using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusTestLanguage.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for TestLanguage objects.
	/// </summary>
	public class BusTestLanguage
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-TestLanguage";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DESCRIPTION = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_VISIBLE_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusTestLanguage constructor takes SqlConnection object</summary>
		public BusTestLanguage()
		{
		}
		/// <summary>BusTestLanguage constructor takes SqlConnection object</summary>
		public BusTestLanguage(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusTestLanguage constructor takes SqlConnection object and Config Object</summary>
		public BusTestLanguage(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all TestLanguage objects
	///     <remarks>   
	///         No parameters. Returns all TestLanguage objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestLanguage objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

	 /// <summary>
	///     Gets all TestLanguage objects
	///     <remarks>   
	///         No parameters. Returns all TestLanguage objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestLanguage objects</retvalue>
	/// </summary>
	public ArrayList Get(long lTestLanguageID)
	{
		return (Get(lTestLanguageID , new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

        /// <summary>
        ///     Gets all TestLanguage objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestLanguage to be returned</param>
        ///     <retvalue>ArrayList containing TestLanguage object</retvalue>
        /// </summary>
	public ArrayList Get(TestLanguage o)
	{	
		return (Get( o.TestLanguageID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.Code, o.Description, o.VisibleCode	));
	}

        /// <summary>
        ///     Gets all TestLanguage objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestLanguage to be returned</param>
        ///     <retvalue>ArrayList containing TestLanguage object</retvalue>
        /// </summary>
	public ArrayList Get(EnumTestLanguage o)
	{	
		return (Get( o.TestLanguageID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.Code, o.Description, o.VisibleCode	));
	}

		/// <summary>
		///     Gets all TestLanguage objects
		///     <remarks>   
		///         Returns TestLanguage objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing TestLanguage object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngTestLanguageID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrCode, string pStrDescription, string pStrVisibleCode)
		{
			TestLanguage data = null;
			_arrlstEntities = new ArrayList();
			EnumTestLanguage enumTestLanguage = new EnumTestLanguage(_conn, _config);
			enumTestLanguage.TestLanguageID = pLngTestLanguageID;
			enumTestLanguage.BeginDateCreated = pDtBeginDateCreated;
			enumTestLanguage.EndDateCreated = pDtEndDateCreated;
			enumTestLanguage.BeginDateModified = pDtBeginDateModified;
			enumTestLanguage.EndDateModified = pDtEndDateModified;
			enumTestLanguage.Code = pStrCode;
			enumTestLanguage.Description = pStrDescription;
			enumTestLanguage.VisibleCode = pStrVisibleCode;
			enumTestLanguage.EnumData();
			_log("Get", enumTestLanguage.ToString());
			while (enumTestLanguage.hasMoreElements())
			{
				data = (TestLanguage) enumTestLanguage.nextElement();
				_arrlstEntities.Add(data);
			}
			enumTestLanguage = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves TestLanguage object to database
        ///     <param name="o">TestLanguage to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(TestLanguage o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestLanguage object to database
		///     <param name="o">TestLanguage to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(TestLanguage o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestLanguage object to database
		///     <param name="o">TestLanguage to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(TestLanguage o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestLanguage object to database
		///     <param name="o">TestLanguage to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(TestLanguage o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist TestLanguage object to database
		///     <param name="o">TestLanguage to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(TestLanguage o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing TestLanguage objects</summary>
		public ArrayList TestLanguages 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					TestLanguage data = null;
					_arrlstEntities = new ArrayList();
					EnumTestLanguage enumTestLanguage = new EnumTestLanguage(_conn, _config);
					enumTestLanguage.EnumData();
					while (enumTestLanguage.hasMoreElements())
					{
						data = (TestLanguage) enumTestLanguage.nextElement();
						_arrlstEntities.Add(data);
					}
					enumTestLanguage = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(TestLanguage pRefTestLanguage)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidTestLanguageID(pRefTestLanguage.TestLanguageID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefTestLanguage.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefTestLanguage.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefTestLanguage.Code);
			if (!isValidTmp && pRefTestLanguage.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDescription(pRefTestLanguage.Description);
			if (!isValidTmp && pRefTestLanguage.Description != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidVisibleCode(pRefTestLanguage.VisibleCode);
			if (!isValidTmp && pRefTestLanguage.VisibleCode != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestLanguageID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDescription(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DESCRIPTION)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_DESCRIPTION;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidVisibleCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_VISIBLE_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestLanguage.DB_FIELD_VISIBLE_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, TestLanguage pRefTestLanguage)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(TestLanguage.TAG_ID + ":  ");
					try
					{
						pRefTestLanguage.TestLanguageID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefTestLanguage.TestLanguageID = 0;
					}
				}

				try
				{
					Console.WriteLine(TestLanguage.TAG_DATE_CREATED + ":  ");
					pRefTestLanguage.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestLanguage.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestLanguage.TAG_DATE_MODIFIED + ":  ");
					pRefTestLanguage.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestLanguage.DateModified = new DateTime();
				}

				Console.WriteLine(TestLanguage.TAG_CODE + ":  ");
				pRefTestLanguage.Code = Console.ReadLine();

				Console.WriteLine(TestLanguage.TAG_DESCRIPTION + ":  ");
				pRefTestLanguage.Description = Console.ReadLine();

				Console.WriteLine(TestLanguage.TAG_VISIBLE_CODE + ":  ");
				pRefTestLanguage.VisibleCode = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							TestLanguage o = null;
							o = new TestLanguage(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList TestLanguage = null;
							TestLanguage = Get();
							Console.WriteLine("TestLanguage count:  " + TestLanguages.Count.ToString() );
							foreach (TestLanguage item in TestLanguages)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
