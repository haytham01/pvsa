using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusAddress.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for Address objects.
	/// </summary>
	public class BusAddress
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-Address";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_STATE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_COUNTRY = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_STREET_NUMBER_1 = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_STREET_NUMBER_2 = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CITY = "";
		private const String REGEXP_ISVALID_ZIP = "";


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusAddress constructor takes SqlConnection object</summary>
		public BusAddress()
		{
		}
		/// <summary>BusAddress constructor takes SqlConnection object</summary>
		public BusAddress(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusAddress constructor takes SqlConnection object and Config Object</summary>
		public BusAddress(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all Address objects
	///     <remarks>   
	///         No parameters. Returns all Address objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Address objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, null, null, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null));
	}

	 /// <summary>
	///     Gets all Address objects
	///     <remarks>   
	///         No parameters. Returns all Address objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Address objects</retvalue>
	/// </summary>
	public ArrayList Get(long lAddressID)
	{
		return (Get(lAddressID , null, null, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null));
	}

        /// <summary>
        ///     Gets all Address objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Address to be returned</param>
        ///     <retvalue>ArrayList containing Address object</retvalue>
        /// </summary>
	public ArrayList Get(Address o)
	{	
		return (Get( o.AddressID, o.State, o.Country, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.StreetNumber1, o.StreetNumber2, o.City, o.Zip	));
	}

        /// <summary>
        ///     Gets all Address objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Address to be returned</param>
        ///     <retvalue>ArrayList containing Address object</retvalue>
        /// </summary>
	public ArrayList Get(EnumAddress o)
	{	
		return (Get( o.AddressID, o.State, o.Country, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.StreetNumber1, o.StreetNumber2, o.City, o.Zip	));
	}

		/// <summary>
		///     Gets all Address objects
		///     <remarks>   
		///         Returns Address objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing Address object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngAddressID, string pStrState, string pStrCountry, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrStreetNumber1, string pStrStreetNumber2, string pStrCity, string pStrZip)
		{
			Address data = null;
			_arrlstEntities = new ArrayList();
			EnumAddress enumAddress = new EnumAddress(_conn, _config);
			enumAddress.AddressID = pLngAddressID;
			enumAddress.State = pStrState;
			enumAddress.Country = pStrCountry;
			enumAddress.BeginDateCreated = pDtBeginDateCreated;
			enumAddress.EndDateCreated = pDtEndDateCreated;
			enumAddress.BeginDateModified = pDtBeginDateModified;
			enumAddress.EndDateModified = pDtEndDateModified;
			enumAddress.StreetNumber1 = pStrStreetNumber1;
			enumAddress.StreetNumber2 = pStrStreetNumber2;
			enumAddress.City = pStrCity;
			enumAddress.Zip = pStrZip;
			enumAddress.EnumData();
			_log("Get", enumAddress.ToString());
			while (enumAddress.hasMoreElements())
			{
				data = (Address) enumAddress.nextElement();
				_arrlstEntities.Add(data);
			}
			enumAddress = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves Address object to database
        ///     <param name="o">Address to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(Address o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Address object to database
		///     <param name="o">Address to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(Address o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Address object to database
		///     <param name="o">Address to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(Address o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Address object to database
		///     <param name="o">Address to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(Address o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist Address object to database
		///     <param name="o">Address to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(Address o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing Address objects</summary>
		public ArrayList Addresss 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					Address data = null;
					_arrlstEntities = new ArrayList();
					EnumAddress enumAddress = new EnumAddress(_conn, _config);
					enumAddress.EnumData();
					while (enumAddress.hasMoreElements())
					{
						data = (Address) enumAddress.nextElement();
						_arrlstEntities.Add(data);
					}
					enumAddress = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(Address pRefAddress)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidAddressID(pRefAddress.AddressID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidState(pRefAddress.State);
			if (!isValidTmp && pRefAddress.State != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidCountry(pRefAddress.Country);
			if (!isValidTmp && pRefAddress.Country != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefAddress.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefAddress.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidStreetNumber1(pRefAddress.StreetNumber1);
			if (!isValidTmp && pRefAddress.StreetNumber1 != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidStreetNumber2(pRefAddress.StreetNumber2);
			if (!isValidTmp && pRefAddress.StreetNumber2 != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidCity(pRefAddress.City);
			if (!isValidTmp && pRefAddress.City != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidZip(pRefAddress.Zip);
			if (!isValidTmp && pRefAddress.Zip != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidAddressID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidState(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_STATE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_STATE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCountry(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_COUNTRY)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_COUNTRY;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidStreetNumber1(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_STREET_NUMBER_1)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_STREET_NUMBER_1;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidStreetNumber2(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_STREET_NUMBER_2)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_STREET_NUMBER_2;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCity(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CITY)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_CITY;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidZip(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_ZIP)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Address.DB_FIELD_ZIP;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, Address pRefAddress)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(Address.TAG_ID + ":  ");
					try
					{
						pRefAddress.AddressID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefAddress.AddressID = 0;
					}
				}


				Console.WriteLine(Address.TAG_STATE + ":  ");
				pRefAddress.State = Console.ReadLine();

				Console.WriteLine(Address.TAG_COUNTRY + ":  ");
				pRefAddress.Country = Console.ReadLine();
				try
				{
					Console.WriteLine(Address.TAG_DATE_CREATED + ":  ");
					pRefAddress.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefAddress.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Address.TAG_DATE_MODIFIED + ":  ");
					pRefAddress.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefAddress.DateModified = new DateTime();
				}

				Console.WriteLine(Address.TAG_STREET_NUMBER_1 + ":  ");
				pRefAddress.StreetNumber1 = Console.ReadLine();

				Console.WriteLine(Address.TAG_STREET_NUMBER_2 + ":  ");
				pRefAddress.StreetNumber2 = Console.ReadLine();

				Console.WriteLine(Address.TAG_CITY + ":  ");
				pRefAddress.City = Console.ReadLine();

				Console.WriteLine(Address.TAG_ZIP + ":  ");
				pRefAddress.Zip = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							Address o = null;
							o = new Address(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList Address = null;
							Address = Get();
							Console.WriteLine("Address count:  " + Addresss.Count.ToString() );
							foreach (Address item in Addresss)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
