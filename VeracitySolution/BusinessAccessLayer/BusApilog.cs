using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  BusApilog.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	1/31/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Business Class for Apilog objects.
    /// </summary>
    public class BusApilog
    {
        private SqlConnection _conn = null;
        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "BusinessAccessLayer-Bus-Apilog";
        private bool _hasError = false;
        private bool _hasInvalid = false;

        private ArrayList _arrlstEntities = null;
        private ArrayList _arrlstColumnErrors = new ArrayList();

        private const String REGEXP_ISVALID_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_APIKEY_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_REF_NUM = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_DATE_CREATED = "";
        private const String REGEXP_ISVALID_MSGSOURCE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_TRACE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_IS_SUCCESS = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_IN_PROGRESS = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_HTTP_STATUS_STR = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_HTTP_STATUS_NUM = BusValidationExpressions.REGEX_TYPE_PATTERN_INT;
        private const String REGEXP_ISVALID_MSGTXT = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
        private const String REGEXP_ISVALID_REQTXT = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
        private const String REGEXP_ISVALID_RESPTXT = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>BusApilog constructor takes SqlConnection object</summary>
        public BusApilog()
        {
        }
        /// <summary>BusApilog constructor takes SqlConnection object</summary>
        public BusApilog(SqlConnection conn)
        {
            _conn = conn;
        }
        /// <summary>BusApilog constructor takes SqlConnection object and Config Object</summary>
        public BusApilog(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        /// <summary>
        ///     Gets all Apilog objects
        ///     <remarks>   
        ///         No parameters. Returns all Apilog objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Apilog objects</retvalue>
        /// </summary>
        public ArrayList Get()
        {
            return (Get(0, 0, 0, new DateTime(), new DateTime(), null, null, false, false, null, 0, null, null, null));
        }

        /// <summary>
        ///     Gets all Apilog objects
        ///     <remarks>   
        ///         No parameters. Returns all Apilog objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Apilog objects</retvalue>
        /// </summary>
        public ArrayList Get(long lApilogID)
        {
            return (Get(lApilogID, 0, 0, new DateTime(), new DateTime(), null, null, false, false, null, 0, null, null, null));
        }

        /// <summary>
        ///     Gets all Apilog objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Apilog to be returned</param>
        ///     <retvalue>ArrayList containing Apilog object</retvalue>
        /// </summary>
        public ArrayList Get(Apilog o)
        {
            return (Get(o.ApilogID, o.ApikeyID, o.RefNum, o.DateCreated, o.DateCreated, o.Msgsource, o.Trace, o.IsSuccess, o.InProgress, o.HttpStatusStr, o.HttpStatusNum, o.Msgtxt, o.Reqtxt, o.Resptxt));
        }

        /// <summary>
        ///     Gets all Apilog objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Apilog to be returned</param>
        ///     <retvalue>ArrayList containing Apilog object</retvalue>
        /// </summary>
        public ArrayList Get(EnumApilog o)
        {
            return (Get(o.ApilogID, o.ApikeyID, o.RefNum, o.BeginDateCreated, o.EndDateCreated, o.Msgsource, o.Trace, o.IsSuccess, o.InProgress, o.HttpStatusStr, o.HttpStatusNum, o.Msgtxt, o.Reqtxt, o.Resptxt));
        }

        /// <summary>
        ///     Gets all Apilog objects
        ///     <remarks>   
        ///         Returns Apilog objects in an array list 
        ///         using the given criteria 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing Apilog object</retvalue>
        /// </summary>
        public ArrayList Get(long pLngApilogID, long pLngApikeyID, long pLngRefNum, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, string pStrMsgsource, string pStrTrace, bool? pBolIsSuccess, bool? pBolInProgress, string pStrHttpStatusStr, long pLngHttpStatusNum, string pStrMsgtxt, string pStrReqtxt, string pStrResptxt)
        {
            Apilog data = null;
            _arrlstEntities = new ArrayList();
            EnumApilog enumApilog = new EnumApilog(_conn, _config);
            enumApilog.ApilogID = pLngApilogID;
            enumApilog.ApikeyID = pLngApikeyID;
            enumApilog.RefNum = pLngRefNum;
            enumApilog.BeginDateCreated = pDtBeginDateCreated;
            enumApilog.EndDateCreated = pDtEndDateCreated;
            enumApilog.Msgsource = pStrMsgsource;
            enumApilog.Trace = pStrTrace;
            enumApilog.IsSuccess = pBolIsSuccess;
            enumApilog.InProgress = pBolInProgress;
            enumApilog.HttpStatusStr = pStrHttpStatusStr;
            enumApilog.HttpStatusNum = pLngHttpStatusNum;
            enumApilog.Msgtxt = pStrMsgtxt;
            enumApilog.Reqtxt = pStrReqtxt;
            enumApilog.Resptxt = pStrResptxt;
            enumApilog.EnumData();
            _log("Get", enumApilog.ToString());
            while (enumApilog.hasMoreElements())
            {
                data = (Apilog)enumApilog.nextElement();
                _arrlstEntities.Add(data);
            }
            enumApilog = null;
            ArrayList.ReadOnly(_arrlstEntities);
            return _arrlstEntities;
        }

        /// <summary>
        ///     Saves Apilog object to database
        ///     <param name="o">Apilog to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Save(Apilog o)
        {
            if (o != null)
            {
                _log("SAVING", o.ToString());
                o.Save(_conn);
                if (o.HasError)
                {
                    _log("ERROR SAVING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apilog object to database
        ///     <param name="o">Apilog to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Update(Apilog o)
        {
            if (o != null)
            {
                _log("UPDATING", o.ToString());
                o.Update(_conn);
                if (o.HasError)
                {
                    _log("ERROR UPDATING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apilog object to database
        ///     <param name="o">Apilog to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Load(Apilog o)
        {
            if (o != null)
            {
                _log("LOADING", o.ToString());
                o.Load(_conn);
                if (o.HasError)
                {
                    _log("ERROR LOADING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apilog object to database
        ///     <param name="o">Apilog to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Delete(Apilog o)
        {
            if (o != null)
            {
                _log("DELETING", o.ToString());
                o.Delete(_conn);
                if (o.HasError)
                {
                    _log("ERROR DELETING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Exist Apilog object to database
        ///     <param name="o">Apilog to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public bool Exist(Apilog o)
        {
            bool bExist = false;
            if (o != null)
            {
                _log("EXIST", o.ToString());
                bExist = o.Exist(_conn);
                if (o.HasError)
                {
                    _log("ERROR EXIST", o.ToString());
                    _hasError = true;
                }
            }

            return bExist;
        }
        /// <summary>Property as to whether or not the object has an Error</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Property as to whether or not the object has invalid columns</summary>
        public bool HasInvalid
        {
            get { return _hasInvalid; }
        }
        /// <summary>Property which returns all column error in an ArrayList</summary>
        public ArrayList ColumnErrors
        {
            get { return _arrlstColumnErrors; }
        }
        /// <summary>Property returns an ArrayList containing Apilog objects</summary>
        public ArrayList Apilogs
        {
            get
            {
                if (_arrlstEntities == null)
                {
                    Apilog data = null;
                    _arrlstEntities = new ArrayList();
                    EnumApilog enumApilog = new EnumApilog(_conn, _config);
                    enumApilog.EnumData();
                    while (enumApilog.hasMoreElements())
                    {
                        data = (Apilog)enumApilog.nextElement();
                        _arrlstEntities.Add(data);
                    }
                    enumApilog = null;
                    ArrayList.ReadOnly(_arrlstEntities);
                }
                return _arrlstEntities;
            }
        }

        /// <summary>
        ///     Checks to make sure all values are valid
        ///     <remarks>   
        ///         Calls "IsValid" method for each property in ocject
        ///     </remarks>   
        ///     <retvalue>true if object has valid entries, false otherwise</retvalue>
        /// </summary>
        public bool IsValid(Apilog pRefApilog)
        {
            bool isValid = true;
            bool isValidTmp = true;

            _arrlstColumnErrors = null;
            _arrlstColumnErrors = new ArrayList();

            isValidTmp = IsValidApilogID(pRefApilog.ApilogID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidApikeyID(pRefApilog.ApikeyID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidRefNum(pRefApilog.RefNum);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateCreated(pRefApilog.DateCreated);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidMsgsource(pRefApilog.Msgsource);
            if (!isValidTmp && pRefApilog.Msgsource != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidTrace(pRefApilog.Trace);
            if (!isValidTmp && pRefApilog.Trace != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsSuccess(pRefApilog.IsSuccess);
            if (!isValidTmp && pRefApilog.IsSuccess != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidInProgress(pRefApilog.InProgress);
            if (!isValidTmp && pRefApilog.InProgress != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidHttpStatusStr(pRefApilog.HttpStatusStr);
            if (!isValidTmp && pRefApilog.HttpStatusStr != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidHttpStatusNum(pRefApilog.HttpStatusNum);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidMsgtxt(pRefApilog.Msgtxt);
            if (!isValidTmp && pRefApilog.Msgtxt != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidReqtxt(pRefApilog.Reqtxt);
            if (!isValidTmp && pRefApilog.Reqtxt != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidResptxt(pRefApilog.Resptxt);
            if (!isValidTmp && pRefApilog.Resptxt != null)
            {
                isValid = false;
            }

            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidApilogID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidApikeyID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_APIKEY_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_APIKEY_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidRefNum(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_REF_NUM)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_REF_NUM;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateCreated(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_DATE_CREATED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidMsgsource(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_MSGSOURCE)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_MSGSOURCE;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidTrace(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_TRACE)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_TRACE;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsSuccess(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_SUCCESS)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_IS_SUCCESS;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidInProgress(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IN_PROGRESS)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_IN_PROGRESS;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidHttpStatusStr(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_HTTP_STATUS_STR)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_HTTP_STATUS_STR;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidHttpStatusNum(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_HTTP_STATUS_NUM)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_HTTP_STATUS_NUM;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidMsgtxt(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_MSGTXT)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_MSGTXT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidReqtxt(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_REQTXT)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_REQTXT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidResptxt(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_RESPTXT)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apilog.DB_FIELD_RESPTXT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Prompt(bool GetIdendity, Apilog pRefApilog)
        {
            try
            {
                GetIdendity = true;
                if (GetIdendity)
                {
                    Console.WriteLine(Apilog.TAG_ID + ":  ");
                    try
                    {
                        pRefApilog.ApilogID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        pRefApilog.ApilogID = 0;
                    }
                }


                Console.WriteLine(Apilog.TAG_APIKEY_ID + ":  ");
                pRefApilog.ApikeyID = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(Apilog.TAG_REF_NUM + ":  ");
                pRefApilog.RefNum = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(Apilog.TAG_DATE_CREATED + ":  ");
                    pRefApilog.DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefApilog.DateCreated = new DateTime();
                }

                Console.WriteLine(Apilog.TAG_MSGSOURCE + ":  ");
                pRefApilog.Msgsource = Console.ReadLine();

                Console.WriteLine(Apilog.TAG_TRACE + ":  ");
                pRefApilog.Trace = Console.ReadLine();

                Console.WriteLine(Apilog.TAG_IS_SUCCESS + ":  ");
                pRefApilog.IsSuccess = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Apilog.TAG_IN_PROGRESS + ":  ");
                pRefApilog.InProgress = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Apilog.TAG_HTTP_STATUS_STR + ":  ");
                pRefApilog.HttpStatusStr = Console.ReadLine();

                Console.WriteLine(Apilog.TAG_HTTP_STATUS_NUM + ":  ");
                pRefApilog.HttpStatusNum = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(Apilog.TAG_MSGTXT + ":  ");
                pRefApilog.Msgtxt = Console.ReadLine();

                Console.WriteLine(Apilog.TAG_REQTXT + ":  ");
                pRefApilog.Reqtxt = Console.ReadLine();

                Console.WriteLine(Apilog.TAG_RESPTXT + ":  ");
                pRefApilog.Resptxt = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
            }
        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test()
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Get All.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // save
                            Apilog o = null;
                            o = new Apilog(_config);
                            Console.WriteLine("Save:  ");
                            Prompt(true, o);
                            Save(o);
                            Console.WriteLine("Has error:  " + HasError.ToString());
                            Console.WriteLine("Has invalid:  " + HasInvalid.ToString());
                            Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString());
                            if (ColumnErrors.Count > 0)
                            {
                                foreach (Column item in ColumnErrors)
                                {
                                    Console.WriteLine("Column Errors Count:  " + item.ToString());
                                }
                            }
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            ArrayList Apilog = null;
                            Apilog = Get();
                            Console.WriteLine("Apilog count:  " + Apilogs.Count.ToString());
                            foreach (Apilog item in Apilogs)
                            {
                                Console.WriteLine("-------\n");
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

    }
}
// END OF CLASS FILE