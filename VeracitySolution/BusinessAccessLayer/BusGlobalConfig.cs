using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusGlobalConfig.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for GlobalConfig objects.
	/// </summary>
	public class BusGlobalConfig
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-GlobalConfig";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DESCRIPTION = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_VISIBLE_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CONFIG_VALUE = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusGlobalConfig constructor takes SqlConnection object</summary>
		public BusGlobalConfig()
		{
		}
		/// <summary>BusGlobalConfig constructor takes SqlConnection object</summary>
		public BusGlobalConfig(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusGlobalConfig constructor takes SqlConnection object and Config Object</summary>
		public BusGlobalConfig(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all GlobalConfig objects
	///     <remarks>   
	///         No parameters. Returns all GlobalConfig objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all GlobalConfig objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null));
	}

	 /// <summary>
	///     Gets all GlobalConfig objects
	///     <remarks>   
	///         No parameters. Returns all GlobalConfig objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all GlobalConfig objects</retvalue>
	/// </summary>
	public ArrayList Get(long lGlobalConfigID)
	{
		return (Get(lGlobalConfigID , new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null));
	}

        /// <summary>
        ///     Gets all GlobalConfig objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">GlobalConfig to be returned</param>
        ///     <retvalue>ArrayList containing GlobalConfig object</retvalue>
        /// </summary>
	public ArrayList Get(GlobalConfig o)
	{	
		return (Get( o.GlobalConfigID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.Code, o.Description, o.VisibleCode, o.ConfigValue	));
	}

        /// <summary>
        ///     Gets all GlobalConfig objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">GlobalConfig to be returned</param>
        ///     <retvalue>ArrayList containing GlobalConfig object</retvalue>
        /// </summary>
	public ArrayList Get(EnumGlobalConfig o)
	{	
		return (Get( o.GlobalConfigID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.Code, o.Description, o.VisibleCode, o.ConfigValue	));
	}

		/// <summary>
		///     Gets all GlobalConfig objects
		///     <remarks>   
		///         Returns GlobalConfig objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing GlobalConfig object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngGlobalConfigID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrCode, string pStrDescription, string pStrVisibleCode, string pStrConfigValue)
		{
			GlobalConfig data = null;
			_arrlstEntities = new ArrayList();
			EnumGlobalConfig enumGlobalConfig = new EnumGlobalConfig(_conn, _config);
			enumGlobalConfig.GlobalConfigID = pLngGlobalConfigID;
			enumGlobalConfig.BeginDateCreated = pDtBeginDateCreated;
			enumGlobalConfig.EndDateCreated = pDtEndDateCreated;
			enumGlobalConfig.BeginDateModified = pDtBeginDateModified;
			enumGlobalConfig.EndDateModified = pDtEndDateModified;
			enumGlobalConfig.Code = pStrCode;
			enumGlobalConfig.Description = pStrDescription;
			enumGlobalConfig.VisibleCode = pStrVisibleCode;
			enumGlobalConfig.ConfigValue = pStrConfigValue;
			enumGlobalConfig.EnumData();
			_log("Get", enumGlobalConfig.ToString());
			while (enumGlobalConfig.hasMoreElements())
			{
				data = (GlobalConfig) enumGlobalConfig.nextElement();
				_arrlstEntities.Add(data);
			}
			enumGlobalConfig = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves GlobalConfig object to database
        ///     <param name="o">GlobalConfig to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(GlobalConfig o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify GlobalConfig object to database
		///     <param name="o">GlobalConfig to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(GlobalConfig o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify GlobalConfig object to database
		///     <param name="o">GlobalConfig to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(GlobalConfig o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify GlobalConfig object to database
		///     <param name="o">GlobalConfig to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(GlobalConfig o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist GlobalConfig object to database
		///     <param name="o">GlobalConfig to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(GlobalConfig o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing GlobalConfig objects</summary>
		public ArrayList GlobalConfigs 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					GlobalConfig data = null;
					_arrlstEntities = new ArrayList();
					EnumGlobalConfig enumGlobalConfig = new EnumGlobalConfig(_conn, _config);
					enumGlobalConfig.EnumData();
					while (enumGlobalConfig.hasMoreElements())
					{
						data = (GlobalConfig) enumGlobalConfig.nextElement();
						_arrlstEntities.Add(data);
					}
					enumGlobalConfig = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(GlobalConfig pRefGlobalConfig)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidGlobalConfigID(pRefGlobalConfig.GlobalConfigID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefGlobalConfig.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefGlobalConfig.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefGlobalConfig.Code);
			if (!isValidTmp && pRefGlobalConfig.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDescription(pRefGlobalConfig.Description);
			if (!isValidTmp && pRefGlobalConfig.Description != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidVisibleCode(pRefGlobalConfig.VisibleCode);
			if (!isValidTmp && pRefGlobalConfig.VisibleCode != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidConfigValue(pRefGlobalConfig.ConfigValue);
			if (!isValidTmp && pRefGlobalConfig.ConfigValue != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidGlobalConfigID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDescription(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DESCRIPTION)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_DESCRIPTION;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidVisibleCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_VISIBLE_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_VISIBLE_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidConfigValue(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CONFIG_VALUE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = GlobalConfig.DB_FIELD_CONFIG_VALUE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, GlobalConfig pRefGlobalConfig)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(GlobalConfig.TAG_ID + ":  ");
					try
					{
						pRefGlobalConfig.GlobalConfigID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefGlobalConfig.GlobalConfigID = 0;
					}
				}

				try
				{
					Console.WriteLine(GlobalConfig.TAG_DATE_CREATED + ":  ");
					pRefGlobalConfig.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefGlobalConfig.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(GlobalConfig.TAG_DATE_MODIFIED + ":  ");
					pRefGlobalConfig.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefGlobalConfig.DateModified = new DateTime();
				}

				Console.WriteLine(GlobalConfig.TAG_CODE + ":  ");
				pRefGlobalConfig.Code = Console.ReadLine();

				Console.WriteLine(GlobalConfig.TAG_DESCRIPTION + ":  ");
				pRefGlobalConfig.Description = Console.ReadLine();

				Console.WriteLine(GlobalConfig.TAG_VISIBLE_CODE + ":  ");
				pRefGlobalConfig.VisibleCode = Console.ReadLine();

				Console.WriteLine(GlobalConfig.TAG_CONFIG_VALUE + ":  ");
				pRefGlobalConfig.ConfigValue = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							GlobalConfig o = null;
							o = new GlobalConfig(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList GlobalConfig = null;
							GlobalConfig = Get();
							Console.WriteLine("GlobalConfig count:  " + GlobalConfigs.Count.ToString() );
							foreach (GlobalConfig item in GlobalConfigs)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
