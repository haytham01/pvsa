using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusTestManager.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for TestManager objects.
	/// </summary>
	public class BusTestManager
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-TestManager";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_TEST_TEMPLATE_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_TEST_LANGUAGE_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_TEST_SUBJECT_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_SCREEN_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_DATE_BEGIN_TEST = "";
		private const String REGEXP_ISVALID_DATE_END_TEST = "";
		private const String REGEXP_ISVALID_REPORT_DATA = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
		private const String REGEXP_ISVALID_NOTES = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusTestManager constructor takes SqlConnection object</summary>
		public BusTestManager()
		{
		}
		/// <summary>BusTestManager constructor takes SqlConnection object</summary>
		public BusTestManager(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusTestManager constructor takes SqlConnection object and Config Object</summary>
		public BusTestManager(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all TestManager objects
	///     <remarks>   
	///         No parameters. Returns all TestManager objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestManager objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, 0, 0, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null));
	}

	 /// <summary>
	///     Gets all TestManager objects
	///     <remarks>   
	///         No parameters. Returns all TestManager objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestManager objects</retvalue>
	/// </summary>
	public ArrayList Get(long lTestManagerID)
	{
		return (Get(lTestManagerID , 0, 0, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null));
	}

        /// <summary>
        ///     Gets all TestManager objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestManager to be returned</param>
        ///     <retvalue>ArrayList containing TestManager object</retvalue>
        /// </summary>
	public ArrayList Get(TestManager o)
	{	
		return (Get( o.TestManagerID, o.TestTemplateID, o.TestLanguageID, o.TestSubjectID, o.ScreenID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.DateBeginTest, o.DateBeginTest, o.DateEndTest, o.DateEndTest, o.ReportData, o.Notes	));
	}

        /// <summary>
        ///     Gets all TestManager objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestManager to be returned</param>
        ///     <retvalue>ArrayList containing TestManager object</retvalue>
        /// </summary>
	public ArrayList Get(EnumTestManager o)
	{	
		return (Get( o.TestManagerID, o.TestTemplateID, o.TestLanguageID, o.TestSubjectID, o.ScreenID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.BeginDateBeginTest, o.EndDateBeginTest, o.BeginDateEndTest, o.EndDateEndTest, o.ReportData, o.Notes	));
	}

		/// <summary>
		///     Gets all TestManager objects
		///     <remarks>   
		///         Returns TestManager objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing TestManager object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngTestManagerID, long pLngTestTemplateID, long pLngTestLanguageID, long pLngTestSubjectID, long pLngScreenID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, DateTime pDtBeginDateBeginTest, DateTime pDtEndDateBeginTest, DateTime pDtBeginDateEndTest, DateTime pDtEndDateEndTest, string pStrReportData, string pStrNotes)
		{
			TestManager data = null;
			_arrlstEntities = new ArrayList();
			EnumTestManager enumTestManager = new EnumTestManager(_conn, _config);
			enumTestManager.TestManagerID = pLngTestManagerID;
			enumTestManager.TestTemplateID = pLngTestTemplateID;
			enumTestManager.TestLanguageID = pLngTestLanguageID;
			enumTestManager.TestSubjectID = pLngTestSubjectID;
			enumTestManager.ScreenID = pLngScreenID;
			enumTestManager.BeginDateCreated = pDtBeginDateCreated;
			enumTestManager.EndDateCreated = pDtEndDateCreated;
			enumTestManager.BeginDateModified = pDtBeginDateModified;
			enumTestManager.EndDateModified = pDtEndDateModified;
			enumTestManager.BeginDateBeginTest = pDtBeginDateBeginTest;
			enumTestManager.EndDateBeginTest = pDtEndDateBeginTest;
			enumTestManager.BeginDateEndTest = pDtBeginDateEndTest;
			enumTestManager.EndDateEndTest = pDtEndDateEndTest;
			enumTestManager.ReportData = pStrReportData;
			enumTestManager.Notes = pStrNotes;
			enumTestManager.EnumData();
			_log("Get", enumTestManager.ToString());
			while (enumTestManager.hasMoreElements())
			{
				data = (TestManager) enumTestManager.nextElement();
				_arrlstEntities.Add(data);
			}
			enumTestManager = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves TestManager object to database
        ///     <param name="o">TestManager to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(TestManager o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestManager object to database
		///     <param name="o">TestManager to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(TestManager o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestManager object to database
		///     <param name="o">TestManager to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(TestManager o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestManager object to database
		///     <param name="o">TestManager to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(TestManager o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist TestManager object to database
		///     <param name="o">TestManager to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(TestManager o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing TestManager objects</summary>
		public ArrayList TestManagers 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					TestManager data = null;
					_arrlstEntities = new ArrayList();
					EnumTestManager enumTestManager = new EnumTestManager(_conn, _config);
					enumTestManager.EnumData();
					while (enumTestManager.hasMoreElements())
					{
						data = (TestManager) enumTestManager.nextElement();
						_arrlstEntities.Add(data);
					}
					enumTestManager = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(TestManager pRefTestManager)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidTestManagerID(pRefTestManager.TestManagerID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidTestTemplateID(pRefTestManager.TestTemplateID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidTestLanguageID(pRefTestManager.TestLanguageID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidTestSubjectID(pRefTestManager.TestSubjectID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidScreenID(pRefTestManager.ScreenID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefTestManager.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefTestManager.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateBeginTest(pRefTestManager.DateBeginTest);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateEndTest(pRefTestManager.DateEndTest);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidReportData(pRefTestManager.ReportData);
			if (!isValidTmp && pRefTestManager.ReportData != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidNotes(pRefTestManager.Notes);
			if (!isValidTmp && pRefTestManager.Notes != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestManagerID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestTemplateID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_TEST_TEMPLATE_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_TEST_TEMPLATE_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestLanguageID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_TEST_LANGUAGE_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_TEST_LANGUAGE_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestSubjectID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_TEST_SUBJECT_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_TEST_SUBJECT_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidScreenID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_SCREEN_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_SCREEN_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateBeginTest(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_BEGIN_TEST)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_DATE_BEGIN_TEST;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateEndTest(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_END_TEST)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_DATE_END_TEST;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidReportData(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_REPORT_DATA)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_REPORT_DATA;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidNotes(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_NOTES)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestManager.DB_FIELD_NOTES;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, TestManager pRefTestManager)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(TestManager.TAG_ID + ":  ");
					try
					{
						pRefTestManager.TestManagerID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefTestManager.TestManagerID = 0;
					}
				}


				Console.WriteLine(TestManager.TAG_TEST_TEMPLATE_ID + ":  ");
				pRefTestManager.TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_TEST_LANGUAGE_ID + ":  ");
				pRefTestManager.TestLanguageID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_TEST_SUBJECT_ID + ":  ");
				pRefTestManager.TestSubjectID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_SCREEN_ID + ":  ");
				pRefTestManager.ScreenID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_CREATED + ":  ");
					pRefTestManager.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestManager.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_MODIFIED + ":  ");
					pRefTestManager.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestManager.DateModified = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_BEGIN_TEST + ":  ");
					pRefTestManager.DateBeginTest = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestManager.DateBeginTest = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_END_TEST + ":  ");
					pRefTestManager.DateEndTest = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestManager.DateEndTest = new DateTime();
				}

				Console.WriteLine(TestManager.TAG_REPORT_DATA + ":  ");
				pRefTestManager.ReportData = Console.ReadLine();

				Console.WriteLine(TestManager.TAG_NOTES + ":  ");
				pRefTestManager.Notes = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							TestManager o = null;
							o = new TestManager(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList TestManager = null;
							TestManager = Get();
							Console.WriteLine("TestManager count:  " + TestManagers.Count.ToString() );
							foreach (TestManager item in TestManagers)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
