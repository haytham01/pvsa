using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusTestType.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for TestType objects.
	/// </summary>
	public class BusTestType
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-TestType";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DESCRIPTION = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_VISIBLE_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusTestType constructor takes SqlConnection object</summary>
		public BusTestType()
		{
		}
		/// <summary>BusTestType constructor takes SqlConnection object</summary>
		public BusTestType(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusTestType constructor takes SqlConnection object and Config Object</summary>
		public BusTestType(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all TestType objects
	///     <remarks>   
	///         No parameters. Returns all TestType objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestType objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

	 /// <summary>
	///     Gets all TestType objects
	///     <remarks>   
	///         No parameters. Returns all TestType objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestType objects</retvalue>
	/// </summary>
	public ArrayList Get(long lTestTypeID)
	{
		return (Get(lTestTypeID , new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

        /// <summary>
        ///     Gets all TestType objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestType to be returned</param>
        ///     <retvalue>ArrayList containing TestType object</retvalue>
        /// </summary>
	public ArrayList Get(TestType o)
	{	
		return (Get( o.TestTypeID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.Code, o.Description, o.VisibleCode	));
	}

        /// <summary>
        ///     Gets all TestType objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestType to be returned</param>
        ///     <retvalue>ArrayList containing TestType object</retvalue>
        /// </summary>
	public ArrayList Get(EnumTestType o)
	{	
		return (Get( o.TestTypeID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.Code, o.Description, o.VisibleCode	));
	}

		/// <summary>
		///     Gets all TestType objects
		///     <remarks>   
		///         Returns TestType objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing TestType object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngTestTypeID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrCode, string pStrDescription, string pStrVisibleCode)
		{
			TestType data = null;
			_arrlstEntities = new ArrayList();
			EnumTestType enumTestType = new EnumTestType(_conn, _config);
			enumTestType.TestTypeID = pLngTestTypeID;
			enumTestType.BeginDateCreated = pDtBeginDateCreated;
			enumTestType.EndDateCreated = pDtEndDateCreated;
			enumTestType.BeginDateModified = pDtBeginDateModified;
			enumTestType.EndDateModified = pDtEndDateModified;
			enumTestType.Code = pStrCode;
			enumTestType.Description = pStrDescription;
			enumTestType.VisibleCode = pStrVisibleCode;
			enumTestType.EnumData();
			_log("Get", enumTestType.ToString());
			while (enumTestType.hasMoreElements())
			{
				data = (TestType) enumTestType.nextElement();
				_arrlstEntities.Add(data);
			}
			enumTestType = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves TestType object to database
        ///     <param name="o">TestType to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(TestType o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestType object to database
		///     <param name="o">TestType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(TestType o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestType object to database
		///     <param name="o">TestType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(TestType o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestType object to database
		///     <param name="o">TestType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(TestType o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist TestType object to database
		///     <param name="o">TestType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(TestType o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing TestType objects</summary>
		public ArrayList TestTypes 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					TestType data = null;
					_arrlstEntities = new ArrayList();
					EnumTestType enumTestType = new EnumTestType(_conn, _config);
					enumTestType.EnumData();
					while (enumTestType.hasMoreElements())
					{
						data = (TestType) enumTestType.nextElement();
						_arrlstEntities.Add(data);
					}
					enumTestType = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(TestType pRefTestType)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidTestTypeID(pRefTestType.TestTypeID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefTestType.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefTestType.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefTestType.Code);
			if (!isValidTmp && pRefTestType.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDescription(pRefTestType.Description);
			if (!isValidTmp && pRefTestType.Description != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidVisibleCode(pRefTestType.VisibleCode);
			if (!isValidTmp && pRefTestType.VisibleCode != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestTypeID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDescription(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DESCRIPTION)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_DESCRIPTION;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidVisibleCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_VISIBLE_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestType.DB_FIELD_VISIBLE_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, TestType pRefTestType)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(TestType.TAG_ID + ":  ");
					try
					{
						pRefTestType.TestTypeID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefTestType.TestTypeID = 0;
					}
				}

				try
				{
					Console.WriteLine(TestType.TAG_DATE_CREATED + ":  ");
					pRefTestType.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestType.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestType.TAG_DATE_MODIFIED + ":  ");
					pRefTestType.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestType.DateModified = new DateTime();
				}

				Console.WriteLine(TestType.TAG_CODE + ":  ");
				pRefTestType.Code = Console.ReadLine();

				Console.WriteLine(TestType.TAG_DESCRIPTION + ":  ");
				pRefTestType.Description = Console.ReadLine();

				Console.WriteLine(TestType.TAG_VISIBLE_CODE + ":  ");
				pRefTestType.VisibleCode = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							TestType o = null;
							o = new TestType(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList TestType = null;
							TestType = Get();
							Console.WriteLine("TestType count:  " + TestTypes.Count.ToString() );
							foreach (TestType item in TestTypes)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
