using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  BusApikey.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	1/31/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Business Class for Apikey objects.
    /// </summary>
    public class BusApikey
    {
        private SqlConnection _conn = null;
        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "BusinessAccessLayer-Bus-Apikey";
        private bool _hasError = false;
        private bool _hasInvalid = false;

        private ArrayList _arrlstEntities = null;
        private ArrayList _arrlstColumnErrors = new ArrayList();

        private const String REGEXP_ISVALID_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_CLIENT_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_DATE_CREATED = "";
        private const String REGEXP_ISVALID_DATE_MODIFIED = "";
        private const String REGEXP_ISVALID_DATE_EXPIRATION = "";
        private const String REGEXP_ISVALID_IS_DISABLED = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_GUID_STR = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_AUTH_KEY = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_NOTES = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>BusApikey constructor takes SqlConnection object</summary>
        public BusApikey()
        {
        }
        /// <summary>BusApikey constructor takes SqlConnection object</summary>
        public BusApikey(SqlConnection conn)
        {
            _conn = conn;
        }
        /// <summary>BusApikey constructor takes SqlConnection object and Config Object</summary>
        public BusApikey(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        /// <summary>
        ///     Gets all Apikey objects
        ///     <remarks>   
        ///         No parameters. Returns all Apikey objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Apikey objects</retvalue>
        /// </summary>
        public ArrayList Get()
        {
            return (Get(0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), false, null, null, null));
        }

        /// <summary>
        ///     Gets all Apikey objects
        ///     <remarks>   
        ///         No parameters. Returns all Apikey objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Apikey objects</retvalue>
        /// </summary>
        public ArrayList Get(long lApikeyID)
        {
            return (Get(lApikeyID, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), false, null, null, null));
        }

        /// <summary>
        ///     Gets all Apikey objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Apikey to be returned</param>
        ///     <retvalue>ArrayList containing Apikey object</retvalue>
        /// </summary>
        public ArrayList Get(Apikey o)
        {
            return (Get(o.ApikeyID, o.ClientID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.DateExpiration, o.DateExpiration, o.IsDisabled, o.GuidStr, o.AuthKey, o.Notes));
        }

        /// <summary>
        ///     Gets all Apikey objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Apikey to be returned</param>
        ///     <retvalue>ArrayList containing Apikey object</retvalue>
        /// </summary>
        public ArrayList Get(EnumApikey o)
        {
            return (Get(o.ApikeyID, o.ClientID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.BeginDateExpiration, o.EndDateExpiration, o.IsDisabled, o.GuidStr, o.AuthKey, o.Notes));
        }

        /// <summary>
        ///     Gets all Apikey objects
        ///     <remarks>   
        ///         Returns Apikey objects in an array list 
        ///         using the given criteria 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing Apikey object</retvalue>
        /// </summary>
        public ArrayList Get(long pLngApikeyID, long pLngClientID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, DateTime pDtBeginDateExpiration, DateTime pDtEndDateExpiration, bool? pBolIsDisabled, string pStrGuidStr, string pStrAuthKey, string pStrNotes)
        {
            Apikey data = null;
            _arrlstEntities = new ArrayList();
            EnumApikey enumApikey = new EnumApikey(_conn, _config);
            enumApikey.ApikeyID = pLngApikeyID;
            enumApikey.ClientID = pLngClientID;
            enumApikey.BeginDateCreated = pDtBeginDateCreated;
            enumApikey.EndDateCreated = pDtEndDateCreated;
            enumApikey.BeginDateModified = pDtBeginDateModified;
            enumApikey.EndDateModified = pDtEndDateModified;
            enumApikey.BeginDateExpiration = pDtBeginDateExpiration;
            enumApikey.EndDateExpiration = pDtEndDateExpiration;
            enumApikey.IsDisabled = pBolIsDisabled;
            enumApikey.GuidStr = pStrGuidStr;
            enumApikey.AuthKey = pStrAuthKey;
            enumApikey.Notes = pStrNotes;
            enumApikey.EnumData();
            _log("Get", enumApikey.ToString());
            while (enumApikey.hasMoreElements())
            {
                data = (Apikey)enumApikey.nextElement();
                _arrlstEntities.Add(data);
            }
            enumApikey = null;
            ArrayList.ReadOnly(_arrlstEntities);
            return _arrlstEntities;
        }

        /// <summary>
        ///     Saves Apikey object to database
        ///     <param name="o">Apikey to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Save(Apikey o)
        {
            if (o != null)
            {
                _log("SAVING", o.ToString());
                o.Save(_conn);
                if (o.HasError)
                {
                    _log("ERROR SAVING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apikey object to database
        ///     <param name="o">Apikey to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Update(Apikey o)
        {
            if (o != null)
            {
                _log("UPDATING", o.ToString());
                o.Update(_conn);
                if (o.HasError)
                {
                    _log("ERROR UPDATING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apikey object to database
        ///     <param name="o">Apikey to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Load(Apikey o)
        {
            if (o != null)
            {
                _log("LOADING", o.ToString());
                o.Load(_conn);
                if (o.HasError)
                {
                    _log("ERROR LOADING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Apikey object to database
        ///     <param name="o">Apikey to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Delete(Apikey o)
        {
            if (o != null)
            {
                _log("DELETING", o.ToString());
                o.Delete(_conn);
                if (o.HasError)
                {
                    _log("ERROR DELETING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Exist Apikey object to database
        ///     <param name="o">Apikey to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public bool Exist(Apikey o)
        {
            bool bExist = false;
            if (o != null)
            {
                _log("EXIST", o.ToString());
                bExist = o.Exist(_conn);
                if (o.HasError)
                {
                    _log("ERROR EXIST", o.ToString());
                    _hasError = true;
                }
            }

            return bExist;
        }
        /// <summary>Property as to whether or not the object has an Error</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Property as to whether or not the object has invalid columns</summary>
        public bool HasInvalid
        {
            get { return _hasInvalid; }
        }
        /// <summary>Property which returns all column error in an ArrayList</summary>
        public ArrayList ColumnErrors
        {
            get { return _arrlstColumnErrors; }
        }
        /// <summary>Property returns an ArrayList containing Apikey objects</summary>
        public ArrayList Apikeys
        {
            get
            {
                if (_arrlstEntities == null)
                {
                    Apikey data = null;
                    _arrlstEntities = new ArrayList();
                    EnumApikey enumApikey = new EnumApikey(_conn, _config);
                    enumApikey.EnumData();
                    while (enumApikey.hasMoreElements())
                    {
                        data = (Apikey)enumApikey.nextElement();
                        _arrlstEntities.Add(data);
                    }
                    enumApikey = null;
                    ArrayList.ReadOnly(_arrlstEntities);
                }
                return _arrlstEntities;
            }
        }

        /// <summary>
        ///     Checks to make sure all values are valid
        ///     <remarks>   
        ///         Calls "IsValid" method for each property in ocject
        ///     </remarks>   
        ///     <retvalue>true if object has valid entries, false otherwise</retvalue>
        /// </summary>
        public bool IsValid(Apikey pRefApikey)
        {
            bool isValid = true;
            bool isValidTmp = true;

            _arrlstColumnErrors = null;
            _arrlstColumnErrors = new ArrayList();

            isValidTmp = IsValidApikeyID(pRefApikey.ApikeyID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidClientID(pRefApikey.ClientID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateCreated(pRefApikey.DateCreated);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateModified(pRefApikey.DateModified);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateExpiration(pRefApikey.DateExpiration);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsDisabled(pRefApikey.IsDisabled);
            if (!isValidTmp && pRefApikey.IsDisabled != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidGuidStr(pRefApikey.GuidStr);
            if (!isValidTmp && pRefApikey.GuidStr != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidAuthKey(pRefApikey.AuthKey);
            if (!isValidTmp && pRefApikey.AuthKey != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidNotes(pRefApikey.Notes);
            if (!isValidTmp && pRefApikey.Notes != null)
            {
                isValid = false;
            }

            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidApikeyID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidClientID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_CLIENT_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_CLIENT_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateCreated(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_DATE_CREATED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateModified(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_DATE_MODIFIED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateExpiration(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_EXPIRATION)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_DATE_EXPIRATION;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsDisabled(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_DISABLED)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_IS_DISABLED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidGuidStr(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_GUID_STR)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_GUID_STR;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidAuthKey(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_AUTH_KEY)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_AUTH_KEY;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidNotes(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_NOTES)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Apikey.DB_FIELD_NOTES;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Prompt(bool GetIdendity, Apikey pRefApikey)
        {
            try
            {
                GetIdendity = true;
                if (GetIdendity)
                {
                    Console.WriteLine(Apikey.TAG_ID + ":  ");
                    try
                    {
                        pRefApikey.ApikeyID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        pRefApikey.ApikeyID = 0;
                    }
                }


                Console.WriteLine(Apikey.TAG_CLIENT_ID + ":  ");
                pRefApikey.ClientID = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(Apikey.TAG_DATE_CREATED + ":  ");
                    pRefApikey.DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefApikey.DateCreated = new DateTime();
                }
                try
                {
                    Console.WriteLine(Apikey.TAG_DATE_MODIFIED + ":  ");
                    pRefApikey.DateModified = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefApikey.DateModified = new DateTime();
                }
                try
                {
                    Console.WriteLine(Apikey.TAG_DATE_EXPIRATION + ":  ");
                    pRefApikey.DateExpiration = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefApikey.DateExpiration = new DateTime();
                }

                Console.WriteLine(Apikey.TAG_IS_DISABLED + ":  ");
                pRefApikey.IsDisabled = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Apikey.TAG_GUID_STR + ":  ");
                pRefApikey.GuidStr = Console.ReadLine();

                Console.WriteLine(Apikey.TAG_AUTH_KEY + ":  ");
                pRefApikey.AuthKey = Console.ReadLine();

                Console.WriteLine(Apikey.TAG_NOTES + ":  ");
                pRefApikey.Notes = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
            }
        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test()
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Get All.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // save
                            Apikey o = null;
                            o = new Apikey(_config);
                            Console.WriteLine("Save:  ");
                            Prompt(true, o);
                            Save(o);
                            Console.WriteLine("Has error:  " + HasError.ToString());
                            Console.WriteLine("Has invalid:  " + HasInvalid.ToString());
                            Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString());
                            if (ColumnErrors.Count > 0)
                            {
                                foreach (Column item in ColumnErrors)
                                {
                                    Console.WriteLine("Column Errors Count:  " + item.ToString());
                                }
                            }
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            ArrayList Apikey = null;
                            Apikey = Get();
                            Console.WriteLine("Apikey count:  " + Apikeys.Count.ToString());
                            foreach (Apikey item in Apikeys)
                            {
                                Console.WriteLine("-------\n");
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

    }
}
// END OF CLASS FILE