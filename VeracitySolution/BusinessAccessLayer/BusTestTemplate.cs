using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  BusTestTemplate.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/7/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Business Class for TestTemplate objects.
    /// </summary>
    public class BusTestTemplate
    {
        private SqlConnection _conn = null;
        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "BusinessAccessLayer-Bus-TestTemplate";
        private bool _hasError = false;
        private bool _hasInvalid = false;

        private ArrayList _arrlstEntities = null;
        private ArrayList _arrlstColumnErrors = new ArrayList();

        private const String REGEXP_ISVALID_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_TEST_TYPE_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_DO_PRINT_REPORT = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_DO_LICENSE_IDENTIFICATION = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_DO_NAME_IDENTIFICATION = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_PRINT_TEMPLATE_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_IS_KIOSK_MODE = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_IS_SYNCHRONIZED = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_ADMIN_PASSWORD = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_MANAGER_PASSWORD = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_RESET_IN_SECONDS = BusValidationExpressions.REGEX_TYPE_PATTERN_INT;
        private const String REGEXP_ISVALID_DATE_CREATED = "";
        private const String REGEXP_ISVALID_DATE_MODIFIED = "";


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>BusTestTemplate constructor takes SqlConnection object</summary>
        public BusTestTemplate()
        {
        }
        /// <summary>BusTestTemplate constructor takes SqlConnection object</summary>
        public BusTestTemplate(SqlConnection conn)
        {
            _conn = conn;
        }
        /// <summary>BusTestTemplate constructor takes SqlConnection object and Config Object</summary>
        public BusTestTemplate(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        /// <summary>
        ///     Gets all TestTemplate objects
        ///     <remarks>   
        ///         No parameters. Returns all TestTemplate objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all TestTemplate objects</retvalue>
        /// </summary>
        public ArrayList Get()
        {
            return (Get(0, 0, false, false, false, null, false, false, null, null, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime()));
        }

        /// <summary>
        ///     Gets all TestTemplate objects
        ///     <remarks>   
        ///         No parameters. Returns all TestTemplate objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all TestTemplate objects</retvalue>
        /// </summary>
        public ArrayList Get(long lTestTemplateID)
        {
            return (Get(lTestTemplateID, 0, false, false, false, null, false, false, null, null, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime()));
        }

        /// <summary>
        ///     Gets all TestTemplate objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestTemplate to be returned</param>
        ///     <retvalue>ArrayList containing TestTemplate object</retvalue>
        /// </summary>
        public ArrayList Get(TestTemplate o)
        {
            return (Get(o.TestTemplateID, o.TestTypeID, o.DoPrintReport, o.DoLicenseIdentification, o.DoNameIdentification, o.PrintTemplateName, o.IsKioskMode, o.IsSynchronized, o.AdminPassword, o.ManagerPassword, o.ResetInSeconds, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified));
        }

        /// <summary>
        ///     Gets all TestTemplate objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestTemplate to be returned</param>
        ///     <retvalue>ArrayList containing TestTemplate object</retvalue>
        /// </summary>
        public ArrayList Get(EnumTestTemplate o)
        {
            return (Get(o.TestTemplateID, o.TestTypeID, o.DoPrintReport, o.DoLicenseIdentification, o.DoNameIdentification, o.PrintTemplateName, o.IsKioskMode, o.IsSynchronized, o.AdminPassword, o.ManagerPassword, o.ResetInSeconds, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified));
        }

        /// <summary>
        ///     Gets all TestTemplate objects
        ///     <remarks>   
        ///         Returns TestTemplate objects in an array list 
        ///         using the given criteria 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing TestTemplate object</retvalue>
        /// </summary>
        public ArrayList Get(long pLngTestTemplateID, long pLngTestTypeID, bool? pBolDoPrintReport, bool? pBolDoLicenseIdentification, bool? pBolDoNameIdentification, string pStrPrintTemplateName, bool? pBolIsKioskMode, bool? pBolIsSynchronized, string pStrAdminPassword, string pStrManagerPassword, long pLngResetInSeconds, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified)
        {
            TestTemplate data = null;
            _arrlstEntities = new ArrayList();
            EnumTestTemplate enumTestTemplate = new EnumTestTemplate(_conn, _config);
            enumTestTemplate.TestTemplateID = pLngTestTemplateID;
            enumTestTemplate.TestTypeID = pLngTestTypeID;
            enumTestTemplate.DoPrintReport = pBolDoPrintReport;
            enumTestTemplate.DoLicenseIdentification = pBolDoLicenseIdentification;
            enumTestTemplate.DoNameIdentification = pBolDoNameIdentification;
            enumTestTemplate.PrintTemplateName = pStrPrintTemplateName;
            enumTestTemplate.IsKioskMode = pBolIsKioskMode;
            enumTestTemplate.IsSynchronized = pBolIsSynchronized;
            enumTestTemplate.AdminPassword = pStrAdminPassword;
            enumTestTemplate.ManagerPassword = pStrManagerPassword;
            enumTestTemplate.ResetInSeconds = pLngResetInSeconds;
            enumTestTemplate.BeginDateCreated = pDtBeginDateCreated;
            enumTestTemplate.EndDateCreated = pDtEndDateCreated;
            enumTestTemplate.BeginDateModified = pDtBeginDateModified;
            enumTestTemplate.EndDateModified = pDtEndDateModified;
            enumTestTemplate.EnumData();
            _log("Get", enumTestTemplate.ToString());
            while (enumTestTemplate.hasMoreElements())
            {
                data = (TestTemplate)enumTestTemplate.nextElement();
                _arrlstEntities.Add(data);
            }
            enumTestTemplate = null;
            ArrayList.ReadOnly(_arrlstEntities);
            return _arrlstEntities;
        }

        /// <summary>
        ///     Saves TestTemplate object to database
        ///     <param name="o">TestTemplate to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Save(TestTemplate o)
        {
            if (o != null)
            {
                _log("SAVING", o.ToString());
                o.Save(_conn);
                if (o.HasError)
                {
                    _log("ERROR SAVING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify TestTemplate object to database
        ///     <param name="o">TestTemplate to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Update(TestTemplate o)
        {
            if (o != null)
            {
                _log("UPDATING", o.ToString());
                o.Update(_conn);
                if (o.HasError)
                {
                    _log("ERROR UPDATING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify TestTemplate object to database
        ///     <param name="o">TestTemplate to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Load(TestTemplate o)
        {
            if (o != null)
            {
                _log("LOADING", o.ToString());
                o.Load(_conn);
                if (o.HasError)
                {
                    _log("ERROR LOADING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify TestTemplate object to database
        ///     <param name="o">TestTemplate to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Delete(TestTemplate o)
        {
            if (o != null)
            {
                _log("DELETING", o.ToString());
                o.Delete(_conn);
                if (o.HasError)
                {
                    _log("ERROR DELETING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Exist TestTemplate object to database
        ///     <param name="o">TestTemplate to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public bool Exist(TestTemplate o)
        {
            bool bExist = false;
            if (o != null)
            {
                _log("EXIST", o.ToString());
                bExist = o.Exist(_conn);
                if (o.HasError)
                {
                    _log("ERROR EXIST", o.ToString());
                    _hasError = true;
                }
            }

            return bExist;
        }
        /// <summary>Property as to whether or not the object has an Error</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Property as to whether or not the object has invalid columns</summary>
        public bool HasInvalid
        {
            get { return _hasInvalid; }
        }
        /// <summary>Property which returns all column error in an ArrayList</summary>
        public ArrayList ColumnErrors
        {
            get { return _arrlstColumnErrors; }
        }
        /// <summary>Property returns an ArrayList containing TestTemplate objects</summary>
        public ArrayList TestTemplates
        {
            get
            {
                if (_arrlstEntities == null)
                {
                    TestTemplate data = null;
                    _arrlstEntities = new ArrayList();
                    EnumTestTemplate enumTestTemplate = new EnumTestTemplate(_conn, _config);
                    enumTestTemplate.EnumData();
                    while (enumTestTemplate.hasMoreElements())
                    {
                        data = (TestTemplate)enumTestTemplate.nextElement();
                        _arrlstEntities.Add(data);
                    }
                    enumTestTemplate = null;
                    ArrayList.ReadOnly(_arrlstEntities);
                }
                return _arrlstEntities;
            }
        }

        /// <summary>
        ///     Checks to make sure all values are valid
        ///     <remarks>   
        ///         Calls "IsValid" method for each property in ocject
        ///     </remarks>   
        ///     <retvalue>true if object has valid entries, false otherwise</retvalue>
        /// </summary>
        public bool IsValid(TestTemplate pRefTestTemplate)
        {
            bool isValid = true;
            bool isValidTmp = true;

            _arrlstColumnErrors = null;
            _arrlstColumnErrors = new ArrayList();

            isValidTmp = IsValidTestTemplateID(pRefTestTemplate.TestTemplateID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidTestTypeID(pRefTestTemplate.TestTypeID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDoPrintReport(pRefTestTemplate.DoPrintReport);
            if (!isValidTmp && pRefTestTemplate.DoPrintReport != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidDoLicenseIdentification(pRefTestTemplate.DoLicenseIdentification);
            if (!isValidTmp && pRefTestTemplate.DoLicenseIdentification != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidDoNameIdentification(pRefTestTemplate.DoNameIdentification);
            if (!isValidTmp && pRefTestTemplate.DoNameIdentification != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidPrintTemplateName(pRefTestTemplate.PrintTemplateName);
            if (!isValidTmp && pRefTestTemplate.PrintTemplateName != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsKioskMode(pRefTestTemplate.IsKioskMode);
            if (!isValidTmp && pRefTestTemplate.IsKioskMode != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsSynchronized(pRefTestTemplate.IsSynchronized);
            if (!isValidTmp && pRefTestTemplate.IsSynchronized != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidAdminPassword(pRefTestTemplate.AdminPassword);
            if (!isValidTmp && pRefTestTemplate.AdminPassword != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidManagerPassword(pRefTestTemplate.ManagerPassword);
            if (!isValidTmp && pRefTestTemplate.ManagerPassword != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidResetInSeconds(pRefTestTemplate.ResetInSeconds);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateCreated(pRefTestTemplate.DateCreated);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateModified(pRefTestTemplate.DateModified);
            if (!isValidTmp)
            {
                isValid = false;
            }

            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidTestTemplateID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidTestTypeID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_TEST_TYPE_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_TEST_TYPE_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDoPrintReport(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DO_PRINT_REPORT)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_DO_PRINT_REPORT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDoLicenseIdentification(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DO_LICENSE_IDENTIFICATION)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_DO_LICENSE_IDENTIFICATION;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDoNameIdentification(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DO_NAME_IDENTIFICATION)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_DO_NAME_IDENTIFICATION;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidPrintTemplateName(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_PRINT_TEMPLATE_NAME)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_PRINT_TEMPLATE_NAME;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsKioskMode(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_KIOSK_MODE)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_IS_KIOSK_MODE;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsSynchronized(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_SYNCHRONIZED)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_IS_SYNCHRONIZED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidAdminPassword(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_ADMIN_PASSWORD)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_ADMIN_PASSWORD;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidManagerPassword(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_MANAGER_PASSWORD)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_MANAGER_PASSWORD;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidResetInSeconds(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_RESET_IN_SECONDS)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_RESET_IN_SECONDS;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateCreated(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_DATE_CREATED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateModified(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = TestTemplate.DB_FIELD_DATE_MODIFIED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Prompt(bool GetIdendity, TestTemplate pRefTestTemplate)
        {
            try
            {
                GetIdendity = true;
                if (GetIdendity)
                {
                    Console.WriteLine(TestTemplate.TAG_ID + ":  ");
                    try
                    {
                        pRefTestTemplate.TestTemplateID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        pRefTestTemplate.TestTemplateID = 0;
                    }
                }


                Console.WriteLine(TestTemplate.TAG_TEST_TYPE_ID + ":  ");
                pRefTestTemplate.TestTypeID = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_PRINT_REPORT + ":  ");
                pRefTestTemplate.DoPrintReport = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_LICENSE_IDENTIFICATION + ":  ");
                pRefTestTemplate.DoLicenseIdentification = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_NAME_IDENTIFICATION + ":  ");
                pRefTestTemplate.DoNameIdentification = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_PRINT_TEMPLATE_NAME + ":  ");
                pRefTestTemplate.PrintTemplateName = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_IS_KIOSK_MODE + ":  ");
                pRefTestTemplate.IsKioskMode = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_IS_SYNCHRONIZED + ":  ");
                pRefTestTemplate.IsSynchronized = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_ADMIN_PASSWORD + ":  ");
                pRefTestTemplate.AdminPassword = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_MANAGER_PASSWORD + ":  ");
                pRefTestTemplate.ManagerPassword = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_RESET_IN_SECONDS + ":  ");
                pRefTestTemplate.ResetInSeconds = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(TestTemplate.TAG_DATE_CREATED + ":  ");
                    pRefTestTemplate.DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefTestTemplate.DateCreated = new DateTime();
                }
                try
                {
                    Console.WriteLine(TestTemplate.TAG_DATE_MODIFIED + ":  ");
                    pRefTestTemplate.DateModified = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefTestTemplate.DateModified = new DateTime();
                }

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
            }
        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test()
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Get All.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // save
                            TestTemplate o = null;
                            o = new TestTemplate(_config);
                            Console.WriteLine("Save:  ");
                            Prompt(true, o);
                            Save(o);
                            Console.WriteLine("Has error:  " + HasError.ToString());
                            Console.WriteLine("Has invalid:  " + HasInvalid.ToString());
                            Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString());
                            if (ColumnErrors.Count > 0)
                            {
                                foreach (Column item in ColumnErrors)
                                {
                                    Console.WriteLine("Column Errors Count:  " + item.ToString());
                                }
                            }
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            ArrayList TestTemplate = null;
                            TestTemplate = Get();
                            Console.WriteLine("TestTemplate count:  " + TestTemplates.Count.ToString());
                            foreach (TestTemplate item in TestTemplates)
                            {
                                Console.WriteLine("-------\n");
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

    }
}
// END OF CLASS FILE