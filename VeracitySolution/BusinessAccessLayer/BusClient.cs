using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusClient.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for Client objects.
	/// </summary>
	public class BusClient
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-Client";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_DATE_HEARTBEAT = "";
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DESCRIPTION = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_VISIBLE_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_TEST_IN_PROGRESS = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
		private const String REGEXP_ISVALID_APP_META_DATA = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusClient constructor takes SqlConnection object</summary>
		public BusClient()
		{
		}
		/// <summary>BusClient constructor takes SqlConnection object</summary>
		public BusClient(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusClient constructor takes SqlConnection object and Config Object</summary>
		public BusClient(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all Client objects
	///     <remarks>   
	///         No parameters. Returns all Client objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Client objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, false, null));
	}

	 /// <summary>
	///     Gets all Client objects
	///     <remarks>   
	///         No parameters. Returns all Client objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Client objects</retvalue>
	/// </summary>
	public ArrayList Get(long lClientID)
	{
		return (Get(lClientID , new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, false, null));
	}

        /// <summary>
        ///     Gets all Client objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Client to be returned</param>
        ///     <retvalue>ArrayList containing Client object</retvalue>
        /// </summary>
	public ArrayList Get(Client o)
	{	
		return (Get( o.ClientID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.DateHeartbeat, o.DateHeartbeat, o.Code, o.Description, o.VisibleCode, o.TestInProgress, o.AppMetaData	));
	}

        /// <summary>
        ///     Gets all Client objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Client to be returned</param>
        ///     <retvalue>ArrayList containing Client object</retvalue>
        /// </summary>
	public ArrayList Get(EnumClient o)
	{	
		return (Get( o.ClientID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.BeginDateHeartbeat, o.EndDateHeartbeat, o.Code, o.Description, o.VisibleCode, o.TestInProgress, o.AppMetaData	));
	}

		/// <summary>
		///     Gets all Client objects
		///     <remarks>   
		///         Returns Client objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing Client object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngClientID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, DateTime pDtBeginDateHeartbeat, DateTime pDtEndDateHeartbeat, string pStrCode, string pStrDescription, string pStrVisibleCode, bool? pBolTestInProgress, string pStrAppMetaData)
		{
			Client data = null;
			_arrlstEntities = new ArrayList();
			EnumClient enumClient = new EnumClient(_conn, _config);
			enumClient.ClientID = pLngClientID;
			enumClient.BeginDateCreated = pDtBeginDateCreated;
			enumClient.EndDateCreated = pDtEndDateCreated;
			enumClient.BeginDateModified = pDtBeginDateModified;
			enumClient.EndDateModified = pDtEndDateModified;
			enumClient.BeginDateHeartbeat = pDtBeginDateHeartbeat;
			enumClient.EndDateHeartbeat = pDtEndDateHeartbeat;
			enumClient.Code = pStrCode;
			enumClient.Description = pStrDescription;
			enumClient.VisibleCode = pStrVisibleCode;
			enumClient.TestInProgress = pBolTestInProgress;
			enumClient.AppMetaData = pStrAppMetaData;
			enumClient.EnumData();
			_log("Get", enumClient.ToString());
			while (enumClient.hasMoreElements())
			{
				data = (Client) enumClient.nextElement();
				_arrlstEntities.Add(data);
			}
			enumClient = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves Client object to database
        ///     <param name="o">Client to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(Client o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Client object to database
		///     <param name="o">Client to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(Client o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Client object to database
		///     <param name="o">Client to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(Client o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Client object to database
		///     <param name="o">Client to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(Client o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist Client object to database
		///     <param name="o">Client to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(Client o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing Client objects</summary>
		public ArrayList Clients 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					Client data = null;
					_arrlstEntities = new ArrayList();
					EnumClient enumClient = new EnumClient(_conn, _config);
					enumClient.EnumData();
					while (enumClient.hasMoreElements())
					{
						data = (Client) enumClient.nextElement();
						_arrlstEntities.Add(data);
					}
					enumClient = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(Client pRefClient)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidClientID(pRefClient.ClientID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefClient.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefClient.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateHeartbeat(pRefClient.DateHeartbeat);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefClient.Code);
			if (!isValidTmp && pRefClient.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDescription(pRefClient.Description);
			if (!isValidTmp && pRefClient.Description != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidVisibleCode(pRefClient.VisibleCode);
			if (!isValidTmp && pRefClient.VisibleCode != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidTestInProgress(pRefClient.TestInProgress);
			if (!isValidTmp && pRefClient.TestInProgress != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidAppMetaData(pRefClient.AppMetaData);
			if (!isValidTmp && pRefClient.AppMetaData != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidClientID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateHeartbeat(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_HEARTBEAT)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_DATE_HEARTBEAT;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDescription(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DESCRIPTION)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_DESCRIPTION;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidVisibleCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_VISIBLE_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_VISIBLE_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestInProgress(bool? pBolData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_TEST_IN_PROGRESS)).IsMatch(pBolData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_TEST_IN_PROGRESS;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidAppMetaData(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_APP_META_DATA)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Client.DB_FIELD_APP_META_DATA;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, Client pRefClient)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(Client.TAG_ID + ":  ");
					try
					{
						pRefClient.ClientID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefClient.ClientID = 0;
					}
				}

				try
				{
					Console.WriteLine(Client.TAG_DATE_CREATED + ":  ");
					pRefClient.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefClient.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Client.TAG_DATE_MODIFIED + ":  ");
					pRefClient.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefClient.DateModified = new DateTime();
				}
				try
				{
					Console.WriteLine(Client.TAG_DATE_HEARTBEAT + ":  ");
					pRefClient.DateHeartbeat = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefClient.DateHeartbeat = new DateTime();
				}

				Console.WriteLine(Client.TAG_CODE + ":  ");
				pRefClient.Code = Console.ReadLine();

				Console.WriteLine(Client.TAG_DESCRIPTION + ":  ");
				pRefClient.Description = Console.ReadLine();

				Console.WriteLine(Client.TAG_VISIBLE_CODE + ":  ");
				pRefClient.VisibleCode = Console.ReadLine();

				Console.WriteLine(Client.TAG_TEST_IN_PROGRESS + ":  ");
				pRefClient.TestInProgress = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Client.TAG_APP_META_DATA + ":  ");
				pRefClient.AppMetaData = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							Client o = null;
							o = new Client(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList Client = null;
							Client = Get();
							Console.WriteLine("Client count:  " + Clients.Count.ToString() );
							foreach (Client item in Clients)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
