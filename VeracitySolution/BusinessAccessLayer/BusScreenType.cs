using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusScreenType.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for ScreenType objects.
	/// </summary>
	public class BusScreenType
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-ScreenType";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_DESCRIPTION = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_VISIBLE_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusScreenType constructor takes SqlConnection object</summary>
		public BusScreenType()
		{
		}
		/// <summary>BusScreenType constructor takes SqlConnection object</summary>
		public BusScreenType(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusScreenType constructor takes SqlConnection object and Config Object</summary>
		public BusScreenType(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all ScreenType objects
	///     <remarks>   
	///         No parameters. Returns all ScreenType objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all ScreenType objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

	 /// <summary>
	///     Gets all ScreenType objects
	///     <remarks>   
	///         No parameters. Returns all ScreenType objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all ScreenType objects</retvalue>
	/// </summary>
	public ArrayList Get(long lScreenTypeID)
	{
		return (Get(lScreenTypeID , new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null));
	}

        /// <summary>
        ///     Gets all ScreenType objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">ScreenType to be returned</param>
        ///     <retvalue>ArrayList containing ScreenType object</retvalue>
        /// </summary>
	public ArrayList Get(ScreenType o)
	{	
		return (Get( o.ScreenTypeID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.Code, o.Description, o.VisibleCode	));
	}

        /// <summary>
        ///     Gets all ScreenType objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">ScreenType to be returned</param>
        ///     <retvalue>ArrayList containing ScreenType object</retvalue>
        /// </summary>
	public ArrayList Get(EnumScreenType o)
	{	
		return (Get( o.ScreenTypeID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.Code, o.Description, o.VisibleCode	));
	}

		/// <summary>
		///     Gets all ScreenType objects
		///     <remarks>   
		///         Returns ScreenType objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing ScreenType object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngScreenTypeID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrCode, string pStrDescription, string pStrVisibleCode)
		{
			ScreenType data = null;
			_arrlstEntities = new ArrayList();
			EnumScreenType enumScreenType = new EnumScreenType(_conn, _config);
			enumScreenType.ScreenTypeID = pLngScreenTypeID;
			enumScreenType.BeginDateCreated = pDtBeginDateCreated;
			enumScreenType.EndDateCreated = pDtEndDateCreated;
			enumScreenType.BeginDateModified = pDtBeginDateModified;
			enumScreenType.EndDateModified = pDtEndDateModified;
			enumScreenType.Code = pStrCode;
			enumScreenType.Description = pStrDescription;
			enumScreenType.VisibleCode = pStrVisibleCode;
			enumScreenType.EnumData();
			_log("Get", enumScreenType.ToString());
			while (enumScreenType.hasMoreElements())
			{
				data = (ScreenType) enumScreenType.nextElement();
				_arrlstEntities.Add(data);
			}
			enumScreenType = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves ScreenType object to database
        ///     <param name="o">ScreenType to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(ScreenType o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify ScreenType object to database
		///     <param name="o">ScreenType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(ScreenType o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify ScreenType object to database
		///     <param name="o">ScreenType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(ScreenType o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify ScreenType object to database
		///     <param name="o">ScreenType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(ScreenType o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist ScreenType object to database
		///     <param name="o">ScreenType to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(ScreenType o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing ScreenType objects</summary>
		public ArrayList ScreenTypes 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					ScreenType data = null;
					_arrlstEntities = new ArrayList();
					EnumScreenType enumScreenType = new EnumScreenType(_conn, _config);
					enumScreenType.EnumData();
					while (enumScreenType.hasMoreElements())
					{
						data = (ScreenType) enumScreenType.nextElement();
						_arrlstEntities.Add(data);
					}
					enumScreenType = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(ScreenType pRefScreenType)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidScreenTypeID(pRefScreenType.ScreenTypeID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefScreenType.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefScreenType.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefScreenType.Code);
			if (!isValidTmp && pRefScreenType.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidDescription(pRefScreenType.Description);
			if (!isValidTmp && pRefScreenType.Description != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidVisibleCode(pRefScreenType.VisibleCode);
			if (!isValidTmp && pRefScreenType.VisibleCode != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidScreenTypeID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDescription(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DESCRIPTION)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_DESCRIPTION;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidVisibleCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_VISIBLE_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = ScreenType.DB_FIELD_VISIBLE_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, ScreenType pRefScreenType)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(ScreenType.TAG_ID + ":  ");
					try
					{
						pRefScreenType.ScreenTypeID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefScreenType.ScreenTypeID = 0;
					}
				}

				try
				{
					Console.WriteLine(ScreenType.TAG_DATE_CREATED + ":  ");
					pRefScreenType.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefScreenType.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(ScreenType.TAG_DATE_MODIFIED + ":  ");
					pRefScreenType.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefScreenType.DateModified = new DateTime();
				}

				Console.WriteLine(ScreenType.TAG_CODE + ":  ");
				pRefScreenType.Code = Console.ReadLine();

				Console.WriteLine(ScreenType.TAG_DESCRIPTION + ":  ");
				pRefScreenType.Description = Console.ReadLine();

				Console.WriteLine(ScreenType.TAG_VISIBLE_CODE + ":  ");
				pRefScreenType.VisibleCode = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							ScreenType o = null;
							o = new ScreenType(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList ScreenType = null;
							ScreenType = Get();
							Console.WriteLine("ScreenType count:  " + ScreenTypes.Count.ToString() );
							foreach (ScreenType item in ScreenTypes)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
