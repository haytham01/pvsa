using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusTestSubject.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for TestSubject objects.
	/// </summary>
	public class BusTestSubject
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-TestSubject";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_ADDRESS_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_DRIVER_LICENSE_NUMBER = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_FIRST_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_MIDDLE_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_LAST_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_PRIMARY_PHONE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_EMAIL_ADDRESS = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CODE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_IDENTIFICATION_RAW_DATA = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
		private const String REGEXP_ISVALID_NOTES = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusTestSubject constructor takes SqlConnection object</summary>
		public BusTestSubject()
		{
		}
		/// <summary>BusTestSubject constructor takes SqlConnection object</summary>
		public BusTestSubject(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusTestSubject constructor takes SqlConnection object and Config Object</summary>
		public BusTestSubject(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all TestSubject objects
	///     <remarks>   
	///         No parameters. Returns all TestSubject objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestSubject objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null, null, null, null, null, null));
	}

	 /// <summary>
	///     Gets all TestSubject objects
	///     <remarks>   
	///         No parameters. Returns all TestSubject objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all TestSubject objects</retvalue>
	/// </summary>
	public ArrayList Get(long lTestSubjectID)
	{
		return (Get(lTestSubjectID , 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), null, null, null, null, null, null, null, null, null));
	}

        /// <summary>
        ///     Gets all TestSubject objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestSubject to be returned</param>
        ///     <retvalue>ArrayList containing TestSubject object</retvalue>
        /// </summary>
	public ArrayList Get(TestSubject o)
	{	
		return (Get( o.TestSubjectID, o.AddressID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.DriverLicenseNumber, o.FirstName, o.MiddleName, o.LastName, o.PrimaryPhone, o.EmailAddress, o.Code, o.IdentificationRawData, o.Notes	));
	}

        /// <summary>
        ///     Gets all TestSubject objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">TestSubject to be returned</param>
        ///     <retvalue>ArrayList containing TestSubject object</retvalue>
        /// </summary>
	public ArrayList Get(EnumTestSubject o)
	{	
		return (Get( o.TestSubjectID, o.AddressID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.DriverLicenseNumber, o.FirstName, o.MiddleName, o.LastName, o.PrimaryPhone, o.EmailAddress, o.Code, o.IdentificationRawData, o.Notes	));
	}

		/// <summary>
		///     Gets all TestSubject objects
		///     <remarks>   
		///         Returns TestSubject objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing TestSubject object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngTestSubjectID, long pLngAddressID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, string pStrDriverLicenseNumber, string pStrFirstName, string pStrMiddleName, string pStrLastName, string pStrPrimaryPhone, string pStrEmailAddress, string pStrCode, string pStrIdentificationRawData, string pStrNotes)
		{
			TestSubject data = null;
			_arrlstEntities = new ArrayList();
			EnumTestSubject enumTestSubject = new EnumTestSubject(_conn, _config);
			enumTestSubject.TestSubjectID = pLngTestSubjectID;
			enumTestSubject.AddressID = pLngAddressID;
			enumTestSubject.BeginDateCreated = pDtBeginDateCreated;
			enumTestSubject.EndDateCreated = pDtEndDateCreated;
			enumTestSubject.BeginDateModified = pDtBeginDateModified;
			enumTestSubject.EndDateModified = pDtEndDateModified;
			enumTestSubject.DriverLicenseNumber = pStrDriverLicenseNumber;
			enumTestSubject.FirstName = pStrFirstName;
			enumTestSubject.MiddleName = pStrMiddleName;
			enumTestSubject.LastName = pStrLastName;
			enumTestSubject.PrimaryPhone = pStrPrimaryPhone;
			enumTestSubject.EmailAddress = pStrEmailAddress;
			enumTestSubject.Code = pStrCode;
			enumTestSubject.IdentificationRawData = pStrIdentificationRawData;
			enumTestSubject.Notes = pStrNotes;
			enumTestSubject.EnumData();
			_log("Get", enumTestSubject.ToString());
			while (enumTestSubject.hasMoreElements())
			{
				data = (TestSubject) enumTestSubject.nextElement();
				_arrlstEntities.Add(data);
			}
			enumTestSubject = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves TestSubject object to database
        ///     <param name="o">TestSubject to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(TestSubject o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestSubject object to database
		///     <param name="o">TestSubject to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(TestSubject o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestSubject object to database
		///     <param name="o">TestSubject to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(TestSubject o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify TestSubject object to database
		///     <param name="o">TestSubject to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(TestSubject o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist TestSubject object to database
		///     <param name="o">TestSubject to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(TestSubject o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing TestSubject objects</summary>
		public ArrayList TestSubjects 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					TestSubject data = null;
					_arrlstEntities = new ArrayList();
					EnumTestSubject enumTestSubject = new EnumTestSubject(_conn, _config);
					enumTestSubject.EnumData();
					while (enumTestSubject.hasMoreElements())
					{
						data = (TestSubject) enumTestSubject.nextElement();
						_arrlstEntities.Add(data);
					}
					enumTestSubject = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(TestSubject pRefTestSubject)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidTestSubjectID(pRefTestSubject.TestSubjectID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidAddressID(pRefTestSubject.AddressID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefTestSubject.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefTestSubject.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDriverLicenseNumber(pRefTestSubject.DriverLicenseNumber);
			if (!isValidTmp && pRefTestSubject.DriverLicenseNumber != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidFirstName(pRefTestSubject.FirstName);
			if (!isValidTmp && pRefTestSubject.FirstName != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidMiddleName(pRefTestSubject.MiddleName);
			if (!isValidTmp && pRefTestSubject.MiddleName != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidLastName(pRefTestSubject.LastName);
			if (!isValidTmp && pRefTestSubject.LastName != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidPrimaryPhone(pRefTestSubject.PrimaryPhone);
			if (!isValidTmp && pRefTestSubject.PrimaryPhone != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidEmailAddress(pRefTestSubject.EmailAddress);
			if (!isValidTmp && pRefTestSubject.EmailAddress != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidCode(pRefTestSubject.Code);
			if (!isValidTmp && pRefTestSubject.Code != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidIdentificationRawData(pRefTestSubject.IdentificationRawData);
			if (!isValidTmp && pRefTestSubject.IdentificationRawData != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidNotes(pRefTestSubject.Notes);
			if (!isValidTmp && pRefTestSubject.Notes != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidTestSubjectID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidAddressID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ADDRESS_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_ADDRESS_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDriverLicenseNumber(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_DRIVER_LICENSE_NUMBER)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_DRIVER_LICENSE_NUMBER;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidFirstName(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_FIRST_NAME)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_FIRST_NAME;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidMiddleName(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_MIDDLE_NAME)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_MIDDLE_NAME;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidLastName(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_LAST_NAME)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_LAST_NAME;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidPrimaryPhone(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_PRIMARY_PHONE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_PRIMARY_PHONE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidEmailAddress(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_EMAIL_ADDRESS)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_EMAIL_ADDRESS;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidCode(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CODE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_CODE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidIdentificationRawData(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_IDENTIFICATION_RAW_DATA)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_IDENTIFICATION_RAW_DATA;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidNotes(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_NOTES)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = TestSubject.DB_FIELD_NOTES;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, TestSubject pRefTestSubject)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(TestSubject.TAG_ID + ":  ");
					try
					{
						pRefTestSubject.TestSubjectID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefTestSubject.TestSubjectID = 0;
					}
				}


				Console.WriteLine(TestSubject.TAG_ADDRESS_ID + ":  ");
				pRefTestSubject.AddressID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(TestSubject.TAG_DATE_CREATED + ":  ");
					pRefTestSubject.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestSubject.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestSubject.TAG_DATE_MODIFIED + ":  ");
					pRefTestSubject.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefTestSubject.DateModified = new DateTime();
				}

				Console.WriteLine(TestSubject.TAG_DRIVER_LICENSE_NUMBER + ":  ");
				pRefTestSubject.DriverLicenseNumber = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_FIRST_NAME + ":  ");
				pRefTestSubject.FirstName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_MIDDLE_NAME + ":  ");
				pRefTestSubject.MiddleName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_LAST_NAME + ":  ");
				pRefTestSubject.LastName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_PRIMARY_PHONE + ":  ");
				pRefTestSubject.PrimaryPhone = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_EMAIL_ADDRESS + ":  ");
				pRefTestSubject.EmailAddress = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_CODE + ":  ");
				pRefTestSubject.Code = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_IDENTIFICATION_RAW_DATA + ":  ");
				pRefTestSubject.IdentificationRawData = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_NOTES + ":  ");
				pRefTestSubject.Notes = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							TestSubject o = null;
							o = new TestSubject(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList TestSubject = null;
							TestSubject = Get();
							Console.WriteLine("TestSubject count:  " + TestSubjects.Count.ToString() );
							foreach (TestSubject item in TestSubjects)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
