using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  BusMerchant.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	12/27/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Business Class for Merchant objects.
	/// </summary>
	public class BusMerchant
	{
		private SqlConnection _conn = null;
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "BusinessAccessLayer-Bus-Merchant";
		private bool _hasError = false;
		private bool _hasInvalid = false;

		private ArrayList _arrlstEntities = null;
		private ArrayList _arrlstColumnErrors = new ArrayList();

		private const String REGEXP_ISVALID_ID= BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_ADDRESS_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
		private const String REGEXP_ISVALID_DATE_CREATED = "";
		private const String REGEXP_ISVALID_DATE_MODIFIED = "";
		private const String REGEXP_ISVALID_IS_DEACTIVATED = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
		private const String REGEXP_ISVALID_PHONE_PRIMARY = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_PHONE_FAX = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_MERCHANT_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_EMAIL_ADDRESS = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_WEBSITE_LINK = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CONTACT_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CONTACT_PHONE = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
		private const String REGEXP_ISVALID_CONTACT_EMAIL = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>BusMerchant constructor takes SqlConnection object</summary>
		public BusMerchant()
		{
		}
		/// <summary>BusMerchant constructor takes SqlConnection object</summary>
		public BusMerchant(SqlConnection conn)
		{
			_conn = conn;
		}
		/// <summary>BusMerchant constructor takes SqlConnection object and Config Object</summary>
		public BusMerchant(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

	 /// <summary>
	///     Gets all Merchant objects
	///     <remarks>   
	///         No parameters. Returns all Merchant objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Merchant objects</retvalue>
	/// </summary>
	public ArrayList Get()
	{
		return (Get(0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), false, null, null, null, null, null, null, null, null));
	}

	 /// <summary>
	///     Gets all Merchant objects
	///     <remarks>   
	///         No parameters. Returns all Merchant objects 
	///     </remarks>   
	///     <retvalue>ArrayList containing all Merchant objects</retvalue>
	/// </summary>
	public ArrayList Get(long lMerchantID)
	{
		return (Get(lMerchantID , 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), false, null, null, null, null, null, null, null, null));
	}

        /// <summary>
        ///     Gets all Merchant objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Merchant to be returned</param>
        ///     <retvalue>ArrayList containing Merchant object</retvalue>
        /// </summary>
	public ArrayList Get(Merchant o)
	{	
		return (Get( o.MerchantID, o.AddressID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.IsDeactivated, o.PhonePrimary, o.PhoneFax, o.MerchantName, o.EmailAddress, o.WebsiteLink, o.ContactName, o.ContactPhone, o.ContactEmail	));
	}

        /// <summary>
        ///     Gets all Merchant objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Merchant to be returned</param>
        ///     <retvalue>ArrayList containing Merchant object</retvalue>
        /// </summary>
	public ArrayList Get(EnumMerchant o)
	{	
		return (Get( o.MerchantID, o.AddressID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.IsDeactivated, o.PhonePrimary, o.PhoneFax, o.MerchantName, o.EmailAddress, o.WebsiteLink, o.ContactName, o.ContactPhone, o.ContactEmail	));
	}

		/// <summary>
		///     Gets all Merchant objects
		///     <remarks>   
		///         Returns Merchant objects in an array list 
		///         using the given criteria 
		///     </remarks>   
		///     <retvalue>ArrayList containing Merchant object</retvalue>
		/// </summary>
		public ArrayList Get( long pLngMerchantID, long pLngAddressID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, bool? pBolIsDeactivated, string pStrPhonePrimary, string pStrPhoneFax, string pStrMerchantName, string pStrEmailAddress, string pStrWebsiteLink, string pStrContactName, string pStrContactPhone, string pStrContactEmail)
		{
			Merchant data = null;
			_arrlstEntities = new ArrayList();
			EnumMerchant enumMerchant = new EnumMerchant(_conn, _config);
			enumMerchant.MerchantID = pLngMerchantID;
			enumMerchant.AddressID = pLngAddressID;
			enumMerchant.BeginDateCreated = pDtBeginDateCreated;
			enumMerchant.EndDateCreated = pDtEndDateCreated;
			enumMerchant.BeginDateModified = pDtBeginDateModified;
			enumMerchant.EndDateModified = pDtEndDateModified;
			enumMerchant.IsDeactivated = pBolIsDeactivated;
			enumMerchant.PhonePrimary = pStrPhonePrimary;
			enumMerchant.PhoneFax = pStrPhoneFax;
			enumMerchant.MerchantName = pStrMerchantName;
			enumMerchant.EmailAddress = pStrEmailAddress;
			enumMerchant.WebsiteLink = pStrWebsiteLink;
			enumMerchant.ContactName = pStrContactName;
			enumMerchant.ContactPhone = pStrContactPhone;
			enumMerchant.ContactEmail = pStrContactEmail;
			enumMerchant.EnumData();
			_log("Get", enumMerchant.ToString());
			while (enumMerchant.hasMoreElements())
			{
				data = (Merchant) enumMerchant.nextElement();
				_arrlstEntities.Add(data);
			}
			enumMerchant = null;
			ArrayList.ReadOnly(_arrlstEntities);
			return _arrlstEntities;
		}

        /// <summary>
        ///     Saves Merchant object to database
        ///     <param name="o">Merchant to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Save(Merchant o)
		{
			if ( o != null )
			{
				_log("SAVING", o.ToString());
				o.Save(_conn);
				if ( o.HasError )
				{
					_log("ERROR SAVING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Merchant object to database
		///     <param name="o">Merchant to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Update(Merchant o)
		{
			if ( o != null )
			{
				_log("UPDATING", o.ToString());
				o.Update(_conn);
				if ( o.HasError )
				{
					_log("ERROR UPDATING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Merchant object to database
		///     <param name="o">Merchant to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Load(Merchant o)
		{
			if ( o != null )
			{
				_log("LOADING", o.ToString());
				o.Load(_conn);
				if ( o.HasError )
				{
					_log("ERROR LOADING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Modify Merchant object to database
		///     <param name="o">Merchant to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public void Delete(Merchant o)
		{
			if ( o != null )
			{
				_log("DELETING", o.ToString());
				o.Delete(_conn);
				if ( o.HasError )
				{
					_log("ERROR DELETING", o.ToString());
					_hasError = true;
				}
			}
		}

		/// <summary>
		///     Exist Merchant object to database
		///     <param name="o">Merchant to be modified.</param>
		///     <retvalue>void</retvalue>
		/// </summary>
		public bool Exist(Merchant o)
		{
			bool bExist = false;
			if ( o != null )
			{
				_log("EXIST", o.ToString());
				bExist = o.Exist(_conn);
				if ( o.HasError )
				{
					_log("ERROR EXIST", o.ToString());
					_hasError = true;
				}
			}

			return bExist;
		}
		/// <summary>Property as to whether or not the object has an Error</summary>
		public bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Property as to whether or not the object has invalid columns</summary>
		public bool HasInvalid 
		{
			get{return _hasInvalid;}
		}
		/// <summary>Property which returns all column error in an ArrayList</summary>
		public ArrayList ColumnErrors
		{
			get{return _arrlstColumnErrors;}
		}
		/// <summary>Property returns an ArrayList containing Merchant objects</summary>
		public ArrayList Merchants 
		{
			get
			{
				if ( _arrlstEntities == null )
				{
					Merchant data = null;
					_arrlstEntities = new ArrayList();
					EnumMerchant enumMerchant = new EnumMerchant(_conn, _config);
					enumMerchant.EnumData();
					while (enumMerchant.hasMoreElements())
					{
						data = (Merchant) enumMerchant.nextElement();
						_arrlstEntities.Add(data);
					}
					enumMerchant = null;
					ArrayList.ReadOnly(_arrlstEntities);
				}
				return _arrlstEntities;
			}
		}

		/// <summary>
		///     Checks to make sure all values are valid
		///     <remarks>   
		///         Calls "IsValid" method for each property in ocject
		///     </remarks>   
		///     <retvalue>true if object has valid entries, false otherwise</retvalue>
		/// </summary>
		public bool IsValid(Merchant pRefMerchant)
		{
			bool isValid = true;
			bool isValidTmp = true;
            
			_arrlstColumnErrors = null;
			_arrlstColumnErrors = new ArrayList();

			isValidTmp = IsValidMerchantID(pRefMerchant.MerchantID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidAddressID(pRefMerchant.AddressID);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateCreated(pRefMerchant.DateCreated);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidDateModified(pRefMerchant.DateModified);
			if (!isValidTmp)
			{
				isValid = false;
			}
			isValidTmp = IsValidIsDeactivated(pRefMerchant.IsDeactivated);
			if (!isValidTmp && pRefMerchant.IsDeactivated != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidPhonePrimary(pRefMerchant.PhonePrimary);
			if (!isValidTmp && pRefMerchant.PhonePrimary != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidPhoneFax(pRefMerchant.PhoneFax);
			if (!isValidTmp && pRefMerchant.PhoneFax != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidMerchantName(pRefMerchant.MerchantName);
			if (!isValidTmp && pRefMerchant.MerchantName != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidEmailAddress(pRefMerchant.EmailAddress);
			if (!isValidTmp && pRefMerchant.EmailAddress != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidWebsiteLink(pRefMerchant.WebsiteLink);
			if (!isValidTmp && pRefMerchant.WebsiteLink != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidContactName(pRefMerchant.ContactName);
			if (!isValidTmp && pRefMerchant.ContactName != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidContactPhone(pRefMerchant.ContactPhone);
			if (!isValidTmp && pRefMerchant.ContactPhone != null)
			{
				isValid = false;
			}
			isValidTmp = IsValidContactEmail(pRefMerchant.ContactEmail);
			if (!isValidTmp && pRefMerchant.ContactEmail != null)
			{
				isValid = false;
			}

			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidMerchantID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidAddressID(long pLngData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_ADDRESS_ID)).IsMatch(pLngData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_ADDRESS_ID;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateCreated(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_DATE_CREATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidDateModified(DateTime pDtData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_DATE_MODIFIED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidIsDeactivated(bool? pBolData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = (new Regex(REGEXP_ISVALID_IS_DEACTIVATED)).IsMatch(pBolData.ToString());
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_IS_DEACTIVATED;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidPhonePrimary(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_PHONE_PRIMARY)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_PHONE_PRIMARY;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidPhoneFax(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_PHONE_FAX)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_PHONE_FAX;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidMerchantName(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_MERCHANT_NAME)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_MERCHANT_NAME;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidEmailAddress(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_EMAIL_ADDRESS)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_EMAIL_ADDRESS;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidWebsiteLink(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_WEBSITE_LINK)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_WEBSITE_LINK;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidContactName(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CONTACT_NAME)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_CONTACT_NAME;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidContactPhone(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CONTACT_PHONE)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_CONTACT_PHONE;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
		/// <summary>
		///     Checks to make sure value is valid
		///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
		/// </summary>
		public bool IsValidContactEmail(string pStrData)
		{
			bool isValid = true;
            
			// do some validation
			isValid = !(new Regex(REGEXP_ISVALID_CONTACT_EMAIL)).IsMatch(pStrData);
			if ( !isValid )
			{
				Column clm = null;
				clm = new Column();
				clm.ColumnName = Merchant.DB_FIELD_CONTACT_EMAIL;
				clm.HasError = true;
				_arrlstColumnErrors.Add(clm);
				_hasInvalid = true;
			}
			return isValid;
		}
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
		public void Prompt(bool GetIdendity, Merchant pRefMerchant)
		{
			try 
			{
				GetIdendity = true;				
				if (GetIdendity)
				{
					Console.WriteLine(Merchant.TAG_ID + ":  ");
					try
					{
						pRefMerchant.MerchantID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						pRefMerchant.MerchantID = 0;
					}
				}


				Console.WriteLine(Merchant.TAG_ADDRESS_ID + ":  ");
				pRefMerchant.AddressID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(Merchant.TAG_DATE_CREATED + ":  ");
					pRefMerchant.DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefMerchant.DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Merchant.TAG_DATE_MODIFIED + ":  ");
					pRefMerchant.DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					pRefMerchant.DateModified = new DateTime();
				}

				Console.WriteLine(Merchant.TAG_IS_DEACTIVATED + ":  ");
				pRefMerchant.IsDeactivated = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Merchant.TAG_PHONE_PRIMARY + ":  ");
				pRefMerchant.PhonePrimary = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_PHONE_FAX + ":  ");
				pRefMerchant.PhoneFax = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_MERCHANT_NAME + ":  ");
				pRefMerchant.MerchantName = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_EMAIL_ADDRESS + ":  ");
				pRefMerchant.EmailAddress = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_WEBSITE_LINK + ":  ");
				pRefMerchant.WebsiteLink = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_NAME + ":  ");
				pRefMerchant.ContactName = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_PHONE + ":  ");
				pRefMerchant.ContactPhone = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_EMAIL + ":  ");
				pRefMerchant.ContactEmail = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
			}
		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test()
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Get All.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// save
							Merchant o = null;
							o = new Merchant(_config);
							Console.WriteLine("Save:  ");
							Prompt(true, o);
							Save(o);
							Console.WriteLine("Has error:  " + HasError.ToString() );
							Console.WriteLine("Has invalid:  " + HasInvalid.ToString() );
							Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString() );
							if ( ColumnErrors.Count > 0 )
							{
								foreach (Column item in ColumnErrors)
								{
									Console.WriteLine("Column Errors Count:  " + item.ToString() );
								}
							}
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							ArrayList Merchant = null;
							Merchant = Get();
							Console.WriteLine("Merchant count:  " + Merchants.Count.ToString() );
							foreach (Merchant item in Merchants)
							{
								Console.WriteLine("-------\n");
								Console.WriteLine(item.ToString() );
							}
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}
		}

	}
}
 // END OF CLASS FILE
