using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  BusScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/25/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Business Class for Screen objects.
    /// </summary>
    public class BusScreen
    {
        private SqlConnection _conn = null;
        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "BusinessAccessLayer-Bus-Screen";
        private bool _hasError = false;
        private bool _hasInvalid = false;

        private ArrayList _arrlstEntities = null;
        private ArrayList _arrlstColumnErrors = new ArrayList();

        private const String REGEXP_ISVALID_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_SCREEN_TYPE_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_DATE_CREATED = "";
        private const String REGEXP_ISVALID_SCREEN_TEXT = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
        private const String REGEXP_ISVALID_SCREEN_IMAGE = "";
        private const String REGEXP_ISVALID_AUDIO_DATA = "";
        private const String REGEXP_ISVALID_IS_TEXT = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_IS_AUDIO = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_SCREEN_TEXT_SHORT = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>BusScreen constructor takes SqlConnection object</summary>
        public BusScreen()
        {
        }
        /// <summary>BusScreen constructor takes SqlConnection object</summary>
        public BusScreen(SqlConnection conn)
        {
            _conn = conn;
        }
        /// <summary>BusScreen constructor takes SqlConnection object and Config Object</summary>
        public BusScreen(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        /// <summary>
        ///     Gets all Screen objects
        ///     <remarks>   
        ///         No parameters. Returns all Screen objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Screen objects</retvalue>
        /// </summary>
        public ArrayList Get()
        {
            return (Get(0, 0, new DateTime(), new DateTime(), null, null, null, false, false, null));
        }

        /// <summary>
        ///     Gets all Screen objects
        ///     <remarks>   
        ///         No parameters. Returns all Screen objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all Screen objects</retvalue>
        /// </summary>
        public ArrayList Get(long lScreenID)
        {
            return (Get(lScreenID, 0, new DateTime(), new DateTime(), null, null, null, false, false, null));
        }

        /// <summary>
        ///     Gets all Screen objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Screen to be returned</param>
        ///     <retvalue>ArrayList containing Screen object</retvalue>
        /// </summary>
        public ArrayList Get(Screen o)
        {
            return (Get(o.ScreenID, o.ScreenTypeID, o.DateCreated, o.DateCreated, o.ScreenText, o.ScreenImage, o.AudioData, o.IsText, o.IsAudio, o.ScreenTextShort));
        }

        /// <summary>
        ///     Gets all Screen objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">Screen to be returned</param>
        ///     <retvalue>ArrayList containing Screen object</retvalue>
        /// </summary>
        public ArrayList Get(EnumScreen o)
        {
            return (Get(o.ScreenID, o.ScreenTypeID, o.BeginDateCreated, o.EndDateCreated, o.ScreenText, o.ScreenImage, o.AudioData, o.IsText, o.IsAudio, o.ScreenTextShort));
        }

        /// <summary>
        ///     Gets all Screen objects
        ///     <remarks>   
        ///         Returns Screen objects in an array list 
        ///         using the given criteria 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing Screen object</retvalue>
        /// </summary>
        public ArrayList Get(long pLngScreenID, long pLngScreenTypeID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, string pStrScreenText, byte[] pBytScreenImage, byte[] pBytAudioData, bool? pBolIsText, bool? pBolIsAudio, string pStrScreenTextShort)
        {
            Screen data = null;
            _arrlstEntities = new ArrayList();
            EnumScreen enumScreen = new EnumScreen(_conn, _config);
            enumScreen.ScreenID = pLngScreenID;
            enumScreen.ScreenTypeID = pLngScreenTypeID;
            enumScreen.BeginDateCreated = pDtBeginDateCreated;
            enumScreen.EndDateCreated = pDtEndDateCreated;
            enumScreen.ScreenText = pStrScreenText;
            enumScreen.ScreenImage = pBytScreenImage;
            enumScreen.AudioData = pBytAudioData;
            enumScreen.IsText = pBolIsText;
            enumScreen.IsAudio = pBolIsAudio;
            enumScreen.ScreenTextShort = pStrScreenTextShort;
            enumScreen.EnumData();
            _log("Get", enumScreen.ToString());
            while (enumScreen.hasMoreElements())
            {
                data = (Screen)enumScreen.nextElement();
                _arrlstEntities.Add(data);
            }
            enumScreen = null;
            ArrayList.ReadOnly(_arrlstEntities);
            return _arrlstEntities;
        }

        /// <summary>
        ///     Saves Screen object to database
        ///     <param name="o">Screen to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Save(Screen o)
        {
            if (o != null)
            {
                _log("SAVING", o.ToString());
                o.Save(_conn);
                if (o.HasError)
                {
                    _log("ERROR SAVING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Screen object to database
        ///     <param name="o">Screen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Update(Screen o)
        {
            if (o != null)
            {
                _log("UPDATING", o.ToString());
                o.Update(_conn);
                if (o.HasError)
                {
                    _log("ERROR UPDATING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Screen object to database
        ///     <param name="o">Screen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Load(Screen o)
        {
            if (o != null)
            {
                _log("LOADING", o.ToString());
                o.Load(_conn);
                if (o.HasError)
                {
                    _log("ERROR LOADING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify Screen object to database
        ///     <param name="o">Screen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Delete(Screen o)
        {
            if (o != null)
            {
                _log("DELETING", o.ToString());
                o.Delete(_conn);
                if (o.HasError)
                {
                    _log("ERROR DELETING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Exist Screen object to database
        ///     <param name="o">Screen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public bool Exist(Screen o)
        {
            bool bExist = false;
            if (o != null)
            {
                _log("EXIST", o.ToString());
                bExist = o.Exist(_conn);
                if (o.HasError)
                {
                    _log("ERROR EXIST", o.ToString());
                    _hasError = true;
                }
            }

            return bExist;
        }
        /// <summary>Property as to whether or not the object has an Error</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Property as to whether or not the object has invalid columns</summary>
        public bool HasInvalid
        {
            get { return _hasInvalid; }
        }
        /// <summary>Property which returns all column error in an ArrayList</summary>
        public ArrayList ColumnErrors
        {
            get { return _arrlstColumnErrors; }
        }
        /// <summary>Property returns an ArrayList containing Screen objects</summary>
        public ArrayList Screens
        {
            get
            {
                if (_arrlstEntities == null)
                {
                    Screen data = null;
                    _arrlstEntities = new ArrayList();
                    EnumScreen enumScreen = new EnumScreen(_conn, _config);
                    enumScreen.EnumData();
                    while (enumScreen.hasMoreElements())
                    {
                        data = (Screen)enumScreen.nextElement();
                        _arrlstEntities.Add(data);
                    }
                    enumScreen = null;
                    ArrayList.ReadOnly(_arrlstEntities);
                }
                return _arrlstEntities;
            }
        }

        /// <summary>
        ///     Checks to make sure all values are valid
        ///     <remarks>   
        ///         Calls "IsValid" method for each property in ocject
        ///     </remarks>   
        ///     <retvalue>true if object has valid entries, false otherwise</retvalue>
        /// </summary>
        public bool IsValid(Screen pRefScreen)
        {
            bool isValid = true;
            bool isValidTmp = true;

            _arrlstColumnErrors = null;
            _arrlstColumnErrors = new ArrayList();

            isValidTmp = IsValidScreenID(pRefScreen.ScreenID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidScreenTypeID(pRefScreen.ScreenTypeID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateCreated(pRefScreen.DateCreated);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidScreenText(pRefScreen.ScreenText);
            if (!isValidTmp && pRefScreen.ScreenText != null)
            {
                isValid = false;
            }
            //Cannot validate type byte[].
            //Cannot validate type byte[].
            isValidTmp = IsValidIsText(pRefScreen.IsText);
            if (!isValidTmp && pRefScreen.IsText != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsAudio(pRefScreen.IsAudio);
            if (!isValidTmp && pRefScreen.IsAudio != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidScreenTextShort(pRefScreen.ScreenTextShort);
            if (!isValidTmp && pRefScreen.ScreenTextShort != null)
            {
                isValid = false;
            }

            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenTypeID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_SCREEN_TYPE_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_SCREEN_TYPE_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateCreated(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_DATE_CREATED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenText(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_SCREEN_TEXT)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_SCREEN_TEXT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenImage(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_SCREEN_IMAGE)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_SCREEN_IMAGE;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidAudioData(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_AUDIO_DATA)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_AUDIO_DATA;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsText(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_TEXT)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_IS_TEXT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsAudio(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_AUDIO)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_IS_AUDIO;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenTextShort(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_SCREEN_TEXT_SHORT)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = Screen.DB_FIELD_SCREEN_TEXT_SHORT;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Prompt(bool GetIdendity, Screen pRefScreen)
        {
            try
            {
                GetIdendity = true;
                if (GetIdendity)
                {
                    Console.WriteLine(Screen.TAG_ID + ":  ");
                    try
                    {
                        pRefScreen.ScreenID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        pRefScreen.ScreenID = 0;
                    }
                }


                Console.WriteLine(Screen.TAG_SCREEN_TYPE_ID + ":  ");
                pRefScreen.ScreenTypeID = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(Screen.TAG_DATE_CREATED + ":  ");
                    pRefScreen.DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefScreen.DateCreated = new DateTime();
                }

                Console.WriteLine(Screen.TAG_SCREEN_TEXT + ":  ");
                pRefScreen.ScreenText = Console.ReadLine();
                // Cannot reliably convert byte[] to string.
                // Cannot reliably convert byte[] to string.

                Console.WriteLine(Screen.TAG_IS_TEXT + ":  ");
                pRefScreen.IsText = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Screen.TAG_IS_AUDIO + ":  ");
                pRefScreen.IsAudio = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Screen.TAG_SCREEN_TEXT_SHORT + ":  ");
                pRefScreen.ScreenTextShort = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
            }
        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test()
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Get All.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // save
                            Screen o = null;
                            o = new Screen(_config);
                            Console.WriteLine("Save:  ");
                            Prompt(true, o);
                            Save(o);
                            Console.WriteLine("Has error:  " + HasError.ToString());
                            Console.WriteLine("Has invalid:  " + HasInvalid.ToString());
                            Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString());
                            if (ColumnErrors.Count > 0)
                            {
                                foreach (Column item in ColumnErrors)
                                {
                                    Console.WriteLine("Column Errors Count:  " + item.ToString());
                                }
                            }
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            ArrayList Screen = null;
                            Screen = Get();
                            Console.WriteLine("Screen count:  " + Screens.Count.ToString());
                            foreach (Screen item in Screens)
                            {
                                Console.WriteLine("-------\n");
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

    }
}
// END OF CLASS FILE