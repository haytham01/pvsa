using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.DataAccessLayer.Enumeration;
using Veracity.DataAccessLayer.Data;

namespace Veracity.BusinessAccessLayer
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  BusLnkTestTemplateScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/14/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Business Class for LnkTestTemplateScreen objects.
    /// </summary>
    public class BusLnkTestTemplateScreen
    {
        private SqlConnection _conn = null;
        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "BusinessAccessLayer-Bus-LnkTestTemplateScreen";
        private bool _hasError = false;
        private bool _hasInvalid = false;

        private ArrayList _arrlstEntities = null;
        private ArrayList _arrlstColumnErrors = new ArrayList();

        private const String REGEXP_ISVALID_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_TEST_TEMPLATE_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_SCREEN_ID = BusValidationExpressions.REGEX_TYPE_PATTERN_NUMERIC10;
        private const String REGEXP_ISVALID_DATE_CREATED = "";
        private const String REGEXP_ISVALID_DATE_MODIFIED = "";
        private const String REGEXP_ISVALID_FLOW_ORDER_INDEX = BusValidationExpressions.REGEX_TYPE_PATTERN_INT;
        private const String REGEXP_ISVALID_TRANSITION_DELAY_IN_MS = BusValidationExpressions.REGEX_TYPE_PATTERN_INT;
        private const String REGEXP_ISVALID_IS_IGNORED = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_IS_REPORTED = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_NOTES = BusValidationExpressions.REGEX_TYPE_PATTERN_TEXT;
        private const String REGEXP_ISVALID_IMAGE_FILE_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;
        private const String REGEXP_ISVALID_HAS_AUDIO = BusValidationExpressions.REGEX_TYPE_PATTERN_BIT;
        private const String REGEXP_ISVALID_AUDIO_FILE_NAME = BusValidationExpressions.REGEX_TYPE_PATTERN_NVARCHAR255;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>BusLnkTestTemplateScreen constructor takes SqlConnection object</summary>
        public BusLnkTestTemplateScreen()
        {
        }
        /// <summary>BusLnkTestTemplateScreen constructor takes SqlConnection object</summary>
        public BusLnkTestTemplateScreen(SqlConnection conn)
        {
            _conn = conn;
        }
        /// <summary>BusLnkTestTemplateScreen constructor takes SqlConnection object and Config Object</summary>
        public BusLnkTestTemplateScreen(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        /// <summary>
        ///     Gets all LnkTestTemplateScreen objects
        ///     <remarks>   
        ///         No parameters. Returns all LnkTestTemplateScreen objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all LnkTestTemplateScreen objects</retvalue>
        /// </summary>
        public ArrayList Get()
        {
            return (Get(0, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), 0, 0, false, false, null, null, false, null));
        }

        /// <summary>
        ///     Gets all LnkTestTemplateScreen objects
        ///     <remarks>   
        ///         No parameters. Returns all LnkTestTemplateScreen objects 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing all LnkTestTemplateScreen objects</retvalue>
        /// </summary>
        public ArrayList Get(long lLnkTestTemplateScreenID)
        {
            return (Get(lLnkTestTemplateScreenID, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime(), 0, 0, false, false, null, null, false, null));
        }

        /// <summary>
        ///     Gets all LnkTestTemplateScreen objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">LnkTestTemplateScreen to be returned</param>
        ///     <retvalue>ArrayList containing LnkTestTemplateScreen object</retvalue>
        /// </summary>
        public ArrayList Get(LnkTestTemplateScreen o)
        {
            return (Get(o.LnkTestTemplateScreenID, o.TestTemplateID, o.ScreenID, o.DateCreated, o.DateCreated, o.DateModified, o.DateModified, o.FlowOrderIndex, o.TransitionDelayInMs, o.IsIgnored, o.IsReported, o.Notes, o.ImageFileName, o.HasAudio, o.AudioFileName));
        }

        /// <summary>
        ///     Gets all LnkTestTemplateScreen objects
        ///     <remarks>   
        ///         Returns ArrayList containing object passed in 
        ///     </remarks>   
        ///     <param name="o">LnkTestTemplateScreen to be returned</param>
        ///     <retvalue>ArrayList containing LnkTestTemplateScreen object</retvalue>
        /// </summary>
        public ArrayList Get(EnumLnkTestTemplateScreen o)
        {
            return (Get(o.LnkTestTemplateScreenID, o.TestTemplateID, o.ScreenID, o.BeginDateCreated, o.EndDateCreated, o.BeginDateModified, o.EndDateModified, o.FlowOrderIndex, o.TransitionDelayInMs, o.IsIgnored, o.IsReported, o.Notes, o.ImageFileName, o.HasAudio, o.AudioFileName));
        }

        /// <summary>
        ///     Gets all LnkTestTemplateScreen objects
        ///     <remarks>   
        ///         Returns LnkTestTemplateScreen objects in an array list 
        ///         using the given criteria 
        ///     </remarks>   
        ///     <retvalue>ArrayList containing LnkTestTemplateScreen object</retvalue>
        /// </summary>
        public ArrayList Get(long pLngLnkTestTemplateScreenID, long pLngTestTemplateID, long pLngScreenID, DateTime pDtBeginDateCreated, DateTime pDtEndDateCreated, DateTime pDtBeginDateModified, DateTime pDtEndDateModified, long pLngFlowOrderIndex, long pLngTransitionDelayInMs, bool? pBolIsIgnored, bool? pBolIsReported, string pStrNotes, string pStrImageFileName, bool? pBolHasAudio, string pStrAudioFileName)
        {
            LnkTestTemplateScreen data = null;
            _arrlstEntities = new ArrayList();
            EnumLnkTestTemplateScreen enumLnkTestTemplateScreen = new EnumLnkTestTemplateScreen(_conn, _config);
            enumLnkTestTemplateScreen.LnkTestTemplateScreenID = pLngLnkTestTemplateScreenID;
            enumLnkTestTemplateScreen.TestTemplateID = pLngTestTemplateID;
            enumLnkTestTemplateScreen.ScreenID = pLngScreenID;
            enumLnkTestTemplateScreen.BeginDateCreated = pDtBeginDateCreated;
            enumLnkTestTemplateScreen.EndDateCreated = pDtEndDateCreated;
            enumLnkTestTemplateScreen.BeginDateModified = pDtBeginDateModified;
            enumLnkTestTemplateScreen.EndDateModified = pDtEndDateModified;
            enumLnkTestTemplateScreen.FlowOrderIndex = pLngFlowOrderIndex;
            enumLnkTestTemplateScreen.TransitionDelayInMs = pLngTransitionDelayInMs;
            enumLnkTestTemplateScreen.IsIgnored = pBolIsIgnored;
            enumLnkTestTemplateScreen.IsReported = pBolIsReported;
            enumLnkTestTemplateScreen.Notes = pStrNotes;
            enumLnkTestTemplateScreen.ImageFileName = pStrImageFileName;
            enumLnkTestTemplateScreen.HasAudio = pBolHasAudio;
            enumLnkTestTemplateScreen.AudioFileName = pStrAudioFileName;
            enumLnkTestTemplateScreen.EnumData();
            _log("Get", enumLnkTestTemplateScreen.ToString());
            while (enumLnkTestTemplateScreen.hasMoreElements())
            {
                data = (LnkTestTemplateScreen)enumLnkTestTemplateScreen.nextElement();
                _arrlstEntities.Add(data);
            }
            enumLnkTestTemplateScreen = null;
            ArrayList.ReadOnly(_arrlstEntities);
            return _arrlstEntities;
        }

        /// <summary>
        ///     Saves LnkTestTemplateScreen object to database
        ///     <param name="o">LnkTestTemplateScreen to be saved.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Save(LnkTestTemplateScreen o)
        {
            if (o != null)
            {
                _log("SAVING", o.ToString());
                o.Save(_conn);
                if (o.HasError)
                {
                    _log("ERROR SAVING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify LnkTestTemplateScreen object to database
        ///     <param name="o">LnkTestTemplateScreen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Update(LnkTestTemplateScreen o)
        {
            if (o != null)
            {
                _log("UPDATING", o.ToString());
                o.Update(_conn);
                if (o.HasError)
                {
                    _log("ERROR UPDATING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify LnkTestTemplateScreen object to database
        ///     <param name="o">LnkTestTemplateScreen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Load(LnkTestTemplateScreen o)
        {
            if (o != null)
            {
                _log("LOADING", o.ToString());
                o.Load(_conn);
                if (o.HasError)
                {
                    _log("ERROR LOADING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Modify LnkTestTemplateScreen object to database
        ///     <param name="o">LnkTestTemplateScreen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Delete(LnkTestTemplateScreen o)
        {
            if (o != null)
            {
                _log("DELETING", o.ToString());
                o.Delete(_conn);
                if (o.HasError)
                {
                    _log("ERROR DELETING", o.ToString());
                    _hasError = true;
                }
            }
        }

        /// <summary>
        ///     Exist LnkTestTemplateScreen object to database
        ///     <param name="o">LnkTestTemplateScreen to be modified.</param>
        ///     <retvalue>void</retvalue>
        /// </summary>
        public bool Exist(LnkTestTemplateScreen o)
        {
            bool bExist = false;
            if (o != null)
            {
                _log("EXIST", o.ToString());
                bExist = o.Exist(_conn);
                if (o.HasError)
                {
                    _log("ERROR EXIST", o.ToString());
                    _hasError = true;
                }
            }

            return bExist;
        }
        /// <summary>Property as to whether or not the object has an Error</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Property as to whether or not the object has invalid columns</summary>
        public bool HasInvalid
        {
            get { return _hasInvalid; }
        }
        /// <summary>Property which returns all column error in an ArrayList</summary>
        public ArrayList ColumnErrors
        {
            get { return _arrlstColumnErrors; }
        }
        /// <summary>Property returns an ArrayList containing LnkTestTemplateScreen objects</summary>
        public ArrayList LnkTestTemplateScreens
        {
            get
            {
                if (_arrlstEntities == null)
                {
                    LnkTestTemplateScreen data = null;
                    _arrlstEntities = new ArrayList();
                    EnumLnkTestTemplateScreen enumLnkTestTemplateScreen = new EnumLnkTestTemplateScreen(_conn, _config);
                    enumLnkTestTemplateScreen.EnumData();
                    while (enumLnkTestTemplateScreen.hasMoreElements())
                    {
                        data = (LnkTestTemplateScreen)enumLnkTestTemplateScreen.nextElement();
                        _arrlstEntities.Add(data);
                    }
                    enumLnkTestTemplateScreen = null;
                    ArrayList.ReadOnly(_arrlstEntities);
                }
                return _arrlstEntities;
            }
        }

        /// <summary>
        ///     Checks to make sure all values are valid
        ///     <remarks>   
        ///         Calls "IsValid" method for each property in ocject
        ///     </remarks>   
        ///     <retvalue>true if object has valid entries, false otherwise</retvalue>
        /// </summary>
        public bool IsValid(LnkTestTemplateScreen pRefLnkTestTemplateScreen)
        {
            bool isValid = true;
            bool isValidTmp = true;

            _arrlstColumnErrors = null;
            _arrlstColumnErrors = new ArrayList();

            isValidTmp = IsValidLnkTestTemplateScreenID(pRefLnkTestTemplateScreen.LnkTestTemplateScreenID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidTestTemplateID(pRefLnkTestTemplateScreen.TestTemplateID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidScreenID(pRefLnkTestTemplateScreen.ScreenID);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateCreated(pRefLnkTestTemplateScreen.DateCreated);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidDateModified(pRefLnkTestTemplateScreen.DateModified);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidFlowOrderIndex(pRefLnkTestTemplateScreen.FlowOrderIndex);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidTransitionDelayInMs(pRefLnkTestTemplateScreen.TransitionDelayInMs);
            if (!isValidTmp)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsIgnored(pRefLnkTestTemplateScreen.IsIgnored);
            if (!isValidTmp && pRefLnkTestTemplateScreen.IsIgnored != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidIsReported(pRefLnkTestTemplateScreen.IsReported);
            if (!isValidTmp && pRefLnkTestTemplateScreen.IsReported != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidNotes(pRefLnkTestTemplateScreen.Notes);
            if (!isValidTmp && pRefLnkTestTemplateScreen.Notes != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidImageFileName(pRefLnkTestTemplateScreen.ImageFileName);
            if (!isValidTmp && pRefLnkTestTemplateScreen.ImageFileName != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidHasAudio(pRefLnkTestTemplateScreen.HasAudio);
            if (!isValidTmp && pRefLnkTestTemplateScreen.HasAudio != null)
            {
                isValid = false;
            }
            isValidTmp = IsValidAudioFileName(pRefLnkTestTemplateScreen.AudioFileName);
            if (!isValidTmp && pRefLnkTestTemplateScreen.AudioFileName != null)
            {
                isValid = false;
            }

            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidLnkTestTemplateScreenID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidTestTemplateID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_TEST_TEMPLATE_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_TEST_TEMPLATE_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidScreenID(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_SCREEN_ID)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_SCREEN_ID;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateCreated(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_CREATED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_DATE_CREATED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidDateModified(DateTime pDtData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_DATE_MODIFIED)).IsMatch(pDtData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_DATE_MODIFIED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidFlowOrderIndex(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_FLOW_ORDER_INDEX)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_FLOW_ORDER_INDEX;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidTransitionDelayInMs(long pLngData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_TRANSITION_DELAY_IN_MS)).IsMatch(pLngData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_TRANSITION_DELAY_IN_MS;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsIgnored(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_IGNORED)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_IS_IGNORED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidIsReported(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_IS_REPORTED)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_IS_REPORTED;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidNotes(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_NOTES)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_NOTES;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidImageFileName(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_IMAGE_FILE_NAME)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_IMAGE_FILE_NAME;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidHasAudio(bool? pBolData)
        {
            bool isValid = true;

            // do some validation
            isValid = (new Regex(REGEXP_ISVALID_HAS_AUDIO)).IsMatch(pBolData.ToString());
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_HAS_AUDIO;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Checks to make sure value is valid
        ///     <retvalue>true if object has a valid entry, false otherwise</retvalue>
        /// </summary>
        public bool IsValidAudioFileName(string pStrData)
        {
            bool isValid = true;

            // do some validation
            isValid = !(new Regex(REGEXP_ISVALID_AUDIO_FILE_NAME)).IsMatch(pStrData);
            if (!isValid)
            {
                Column clm = null;
                clm = new Column();
                clm.ColumnName = LnkTestTemplateScreen.DB_FIELD_AUDIO_FILE_NAME;
                clm.HasError = true;
                _arrlstColumnErrors.Add(clm);
                _hasInvalid = true;
            }
            return isValid;
        }
        /// <summary>
        ///     Standard Error Logging
        ///     <retvalue>void</retvalue>
        /// </summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>
        ///     Command Line Prompts to get values
        ///     <retvalue>void</retvalue>
        /// </summary>
        public void Prompt(bool GetIdendity, LnkTestTemplateScreen pRefLnkTestTemplateScreen)
        {
            try
            {
                GetIdendity = true;
                if (GetIdendity)
                {
                    Console.WriteLine(LnkTestTemplateScreen.TAG_ID + ":  ");
                    try
                    {
                        pRefLnkTestTemplateScreen.LnkTestTemplateScreenID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        pRefLnkTestTemplateScreen.LnkTestTemplateScreenID = 0;
                    }
                }


                Console.WriteLine(LnkTestTemplateScreen.TAG_TEST_TEMPLATE_ID + ":  ");
                pRefLnkTestTemplateScreen.TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_SCREEN_ID + ":  ");
                pRefLnkTestTemplateScreen.ScreenID = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(LnkTestTemplateScreen.TAG_DATE_CREATED + ":  ");
                    pRefLnkTestTemplateScreen.DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefLnkTestTemplateScreen.DateCreated = new DateTime();
                }
                try
                {
                    Console.WriteLine(LnkTestTemplateScreen.TAG_DATE_MODIFIED + ":  ");
                    pRefLnkTestTemplateScreen.DateModified = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    pRefLnkTestTemplateScreen.DateModified = new DateTime();
                }

                Console.WriteLine(LnkTestTemplateScreen.TAG_FLOW_ORDER_INDEX + ":  ");
                pRefLnkTestTemplateScreen.FlowOrderIndex = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_TRANSITION_DELAY_IN_MS + ":  ");
                pRefLnkTestTemplateScreen.TransitionDelayInMs = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_IS_IGNORED + ":  ");
                pRefLnkTestTemplateScreen.IsIgnored = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_IS_REPORTED + ":  ");
                pRefLnkTestTemplateScreen.IsReported = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_NOTES + ":  ");
                pRefLnkTestTemplateScreen.Notes = Console.ReadLine();

                Console.WriteLine(LnkTestTemplateScreen.TAG_IMAGE_FILE_NAME + ":  ");
                pRefLnkTestTemplateScreen.ImageFileName = Console.ReadLine();

                Console.WriteLine(LnkTestTemplateScreen.TAG_HAS_AUDIO + ":  ");
                pRefLnkTestTemplateScreen.HasAudio = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_AUDIO_FILE_NAME + ":  ");
                pRefLnkTestTemplateScreen.AudioFileName = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
            }
        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test()
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Get All.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // save
                            LnkTestTemplateScreen o = null;
                            o = new LnkTestTemplateScreen(_config);
                            Console.WriteLine("Save:  ");
                            Prompt(true, o);
                            Save(o);
                            Console.WriteLine("Has error:  " + HasError.ToString());
                            Console.WriteLine("Has invalid:  " + HasInvalid.ToString());
                            Console.WriteLine("Column Errors Count:  " + ColumnErrors.Count.ToString());
                            if (ColumnErrors.Count > 0)
                            {
                                foreach (Column item in ColumnErrors)
                                {
                                    Console.WriteLine("Column Errors Count:  " + item.ToString());
                                }
                            }
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            ArrayList LnkTestTemplateScreen = null;
                            LnkTestTemplateScreen = Get();
                            Console.WriteLine("LnkTestTemplateScreen count:  " + LnkTestTemplateScreens.Count.ToString());
                            foreach (LnkTestTemplateScreen item in LnkTestTemplateScreens)
                            {
                                Console.WriteLine("-------\n");
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

    }
}
// END OF CLASS FILE