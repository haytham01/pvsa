﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Veracity.BusinessFacadeLayer;
using Veracity.BusinessAccessLayer;
using Veracity.DataAccessLayer.Data;
using Veracity.DataAccessLayer.Enumeration;
using System.Collections;

namespace DataManagerUtil
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void screenButton_Click(object sender, RoutedEventArgs e)
        {
            ScreenWindow w = new ScreenWindow();
            w.ShowDialog();
        }

        private void createAppControllerButton_Click(object sender, RoutedEventArgs e)
        {
            BusFacCore busFacCore = new BusFacCore();

            Veracity.Client.Proxy.AppController appcontroller = new Veracity.Client.Proxy.AppController();

            // cache all test templates
            List<Veracity.Client.Proxy.TestTemplate> lstTestTemplate = getAllTestTemplate();
            List<Veracity.Client.Proxy.TestLanguage> lstTestLanguage = getAllTestLanguage();
            List<Veracity.Client.Proxy.TestType> lstTestType = getAllTestType();

            // create the test
            Veracity.Client.Proxy.Test test = null;

            // set up test 1
            //test = new Veracity.Client.Proxy.Test();
            //test.objTestTemplate = lstTestTemplate[0];
            //test.objTestType = lstTestType[(int) test.objTestTemplate.TestTypeID - 1];
            //test.lstTestLanguage.Add(lstTestLanguage[0]);
            ////test.lstTestLanguage.Add(lstTestLanguage[1]);
            //test.lstLnkTestTemplateScreen = getLnkTestTemplateScreenList(1);
            //test.lstScreen = getScreenList(test.lstLnkTestTemplateScreen);
            //appcontroller.lstTest.Add(test);

            //// set up test 2
            //test = new Veracity.Client.Proxy.Test();
            //test.objTestTemplate = lstTestTemplate[1];
            //test.objTestType = lstTestType[(int)test.objTestTemplate.TestTypeID - 1];
            //test.lstTestLanguage.Add(lstTestLanguage[0]);
            //test.lstTestLanguage.Add(lstTestLanguage[1]);
            //test.lstTestLanguage.Add(lstTestLanguage[2]);
            //test.lstLnkTestTemplateScreen = getLnkTestTemplateScreenList(2);
            //test.lstScreen = getScreenList(test.lstLnkTestTemplateScreen);
            //appcontroller.lstTest.Add(test);

            //// set up test 3
            //test = new Veracity.Client.Proxy.Test();
            //test.objTestTemplate = lstTestTemplate[2];
            //test.objTestType = lstTestType[(int)test.objTestTemplate.TestTypeID - 1];
            //test.lstTestLanguage.Add(lstTestLanguage[0]);
            //test.lstTestLanguage.Add(lstTestLanguage[1]);
            //test.lstTestLanguage.Add(lstTestLanguage[2]);
            //test.lstTestLanguage.Add(lstTestLanguage[3]);
            //test.lstLnkTestTemplateScreen = getLnkTestTemplateScreenList(3);
            //test.lstScreen = getScreenList(test.lstLnkTestTemplateScreen);
            //appcontroller.lstTest.Add(test);

            // set up test
            long testTemplateID = (long) Convert.ToInt32(this.templateIDTextBox.Text);
            test = new Veracity.Client.Proxy.Test();
            test.objTestTemplate = getTestTemplate(testTemplateID, lstTestTemplate);

            // set the host config
            test.objHostConfig.IsKioskMode = true;
            test.objHostConfig.ShowNavigationControls = false;
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.WELCOME);
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE);
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION);
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.IDENTIFICATION_PHOTO);
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_FIRST_NAME);
            test.objHostConfig.lstClientShellScreen.Add(Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_LAST_NAME);

            test.objTestType = getTestType(testTemplateID);
            test.lstTestLanguage.Add(lstTestLanguage[0]);
            //test.lstTestLanguage.Add(lstTestLanguage[1]);
            test.lstLnkTestTemplateScreen = getLnkTestTemplateScreenList(testTemplateID);
            test.lstScreen = getScreenList(test.lstLnkTestTemplateScreen);
            appcontroller.lstTest.Add(test);


            this.mainTextBox.Text = appcontroller.ToXml();
            appcontroller.Serialize();
        }

       
        private Veracity.Client.Proxy.TestTemplate getTestTemplate(long pLngTestTemplateID, List<Veracity.Client.Proxy.TestTemplate> pLstTestTemplate)
        {
            Veracity.Client.Proxy.TestTemplate testTemplate = null;
            foreach (Veracity.Client.Proxy.TestTemplate t in pLstTestTemplate)
            {
                if (t.TestTemplateID == pLngTestTemplateID)
                {
                    testTemplate = t;
                    break;
                }
            }
            return testTemplate;
        }

        private List<Veracity.Client.Proxy.TestTemplate> getAllTestTemplate()
        {
            List<Veracity.Client.Proxy.TestTemplate> lstTestTemplate = new List<Veracity.Client.Proxy.TestTemplate>();

            BusFacCore busFacCore = new BusFacCore();
            EnumTestTemplate enumTestTemplate = new EnumTestTemplate();
            ArrayList arTestTemplate = busFacCore.TestTemplateGetList(enumTestTemplate);
            if ((arTestTemplate != null) && (arTestTemplate.Count > 0))
            {
                Veracity.Client.Proxy.TestTemplate testTemplateProxy = null;
                Veracity.DataAccessLayer.Data.TestTemplate testTemplateData = null;
                for (int i = 0; i < arTestTemplate.Count; i++)
                {
                    testTemplateData = (Veracity.DataAccessLayer.Data.TestTemplate)arTestTemplate[i];
                    testTemplateProxy = new Veracity.Client.Proxy.TestTemplate(testTemplateData.ToXml());
                    lstTestTemplate.Add(testTemplateProxy);
                }
            }
            return lstTestTemplate;
        }

        private List<Veracity.Client.Proxy.TestLanguage> getAllTestLanguage()
        {
            List<Veracity.Client.Proxy.TestLanguage> lstTestLanguage = new List<Veracity.Client.Proxy.TestLanguage>();

            BusFacCore busFacCore = new BusFacCore();
            EnumTestLanguage enumTestLanguage = new EnumTestLanguage();
            ArrayList arTestLanguage = busFacCore.TestLanguageGetList(enumTestLanguage);
            if ((arTestLanguage != null) && (arTestLanguage.Count > 0))
            {
                Veracity.Client.Proxy.TestLanguage TestLanguageProxy = null;
                Veracity.DataAccessLayer.Data.TestLanguage TestLanguageData = null;
                for (int i = 0; i < arTestLanguage.Count; i++)
                {
                    TestLanguageData = (Veracity.DataAccessLayer.Data.TestLanguage)arTestLanguage[i];
                    TestLanguageProxy = new Veracity.Client.Proxy.TestLanguage(TestLanguageData.ToXml());
                    lstTestLanguage.Add(TestLanguageProxy);
                }
            }
            return lstTestLanguage;
        }

        private Veracity.Client.Proxy.TestType getTestType(long pLngTestTypeID)
        {
            Veracity.Client.Proxy.TestType testType = new Veracity.Client.Proxy.TestType();

            BusFacCore busFacCore = new BusFacCore();
            EnumTestType enumTestType = new EnumTestType();
            enumTestType.TestTypeID = pLngTestTypeID;
            ArrayList arTestType = busFacCore.TestTypeGetList(enumTestType);
            if (arTestType != null)
            {
                Veracity.DataAccessLayer.Data.TestType lnkTestTemplateScreenData = null;
                for (int i = 0; i < arTestType.Count; i++)
                {
                    lnkTestTemplateScreenData = (Veracity.DataAccessLayer.Data.TestType)arTestType[i];
                    testType = new Veracity.Client.Proxy.TestType(lnkTestTemplateScreenData.ToXml());
                    break;
                }
            }

            return testType;
        }

        private List<Veracity.Client.Proxy.LnkTestTemplateScreen> getLnkTestTemplateScreenList(long pLngTestTemplateID)
        {
            List<Veracity.Client.Proxy.LnkTestTemplateScreen> lstLnkTestTemplateScreen = new List<Veracity.Client.Proxy.LnkTestTemplateScreen>();

            BusFacCore busFacCore = new BusFacCore();
            EnumLnkTestTemplateScreen enumLnkTestTemplateScreen = new EnumLnkTestTemplateScreen();
            enumLnkTestTemplateScreen.TestTemplateID = pLngTestTemplateID;
            ArrayList arLnkTestTemplateScreen = busFacCore.LnkTestTemplateScreenGetList(enumLnkTestTemplateScreen);
            if (arLnkTestTemplateScreen != null)
            {
                Veracity.Client.Proxy.LnkTestTemplateScreen lnkTestTemplateScreenProxy = null;
                Veracity.DataAccessLayer.Data.LnkTestTemplateScreen lnkTestTemplateScreenData = null;
                for (int i = 0; i < arLnkTestTemplateScreen.Count; i++)
                {
                    lnkTestTemplateScreenData = (Veracity.DataAccessLayer.Data.LnkTestTemplateScreen)arLnkTestTemplateScreen[i];
                    lnkTestTemplateScreenProxy = new Veracity.Client.Proxy.LnkTestTemplateScreen(lnkTestTemplateScreenData.ToXml());
                    lstLnkTestTemplateScreen.Add(lnkTestTemplateScreenProxy);
                }
            }

            return lstLnkTestTemplateScreen;
        }

        private List<Veracity.Client.Proxy.Screen> getScreenList(List<Veracity.Client.Proxy.LnkTestTemplateScreen> pLstLnkTestTemplateScreen)
        {
            List<Veracity.Client.Proxy.Screen> lstScreen = new List<Veracity.Client.Proxy.Screen>();

            BusFacCore busFacCore = new BusFacCore();
            if ((pLstLnkTestTemplateScreen != null) && (pLstLnkTestTemplateScreen.Count > 0))
            {
                Veracity.Client.Proxy.Screen screenProxy = null;
                Veracity.DataAccessLayer.Data.Screen screenData = null;

                for (int i = 0; i < pLstLnkTestTemplateScreen.Count; i++)
                {
                    screenData = busFacCore.ScreenGet(pLstLnkTestTemplateScreen[i].ScreenID);
                    screenProxy = new Veracity.Client.Proxy.Screen(screenData.ToXml());
                    lstScreen.Add(screenProxy);
                }
            }

            return lstScreen;
        }

        private List<Veracity.Client.Proxy.TestType> getAllTestType()
        {
            List<Veracity.Client.Proxy.TestType> lstTestType = new List<Veracity.Client.Proxy.TestType>();

            BusFacCore busFacCore = new BusFacCore();
            EnumTestType enumTestType = new EnumTestType();
            ArrayList arTestType = busFacCore.TestTypeGetList(enumTestType);
            if ((arTestType != null) && (arTestType.Count > 0))
            {
                Veracity.Client.Proxy.TestType TestTypeProxy = null;
                Veracity.DataAccessLayer.Data.TestType TestTypeData = null;
                for (int i = 0; i < arTestType.Count; i++)
                {
                    TestTypeData = (Veracity.DataAccessLayer.Data.TestType)arTestType[i];
                    TestTypeProxy = new Veracity.Client.Proxy.TestType(TestTypeData.ToXml());
                    lstTestType.Add(TestTypeProxy);
                }
            }
            return lstTestType;
        }

        private void parseAppControllerButton_Click(object sender, RoutedEventArgs e)
        {
            Veracity.Client.Proxy.AppController appcontroller = new Veracity.Client.Proxy.AppController();
            appcontroller.Parse(this.mainTextBox.Text);
        }

        private void createAppController2Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
