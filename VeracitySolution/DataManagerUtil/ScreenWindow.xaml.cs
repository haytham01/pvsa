﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Veracity.BusinessFacadeLayer;
using Veracity.DataAccessLayer.Data;
using Veracity.DataAccessLayer.Enumeration;

namespace DataManagerUtil
{
    /// <summary>
    /// Interaction logic for ScreenWindow.xaml
    /// </summary>
    public partial class ScreenWindow : Window
    {
        public ScreenWindow()
        {
            InitializeComponent();
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            load();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            save();
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            update();
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            delete();
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            clear();
        }

        private void imageDataBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                this.imageDataPathTextBox.Text = filename;
            }

        }

        private void load()
        {
            long id = 0;
            try
            {
                if ((long.TryParse(this.screenIdTextBox.Text, out id)) && (id > 0))
                {
                    BusFacCore busFacCore = new BusFacCore();
                    Screen screen = null;
                    screen = busFacCore.ScreenGet(id);
                    populate(screen);
                }
                else
                {
                    MessageBox.Show("Invalid Action");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void save()
        {
            long id = 0;
            try
            {
                if ((long.TryParse(this.screenIdTextBox.Text, out id)) && (id > 0))
                {
                    BusFacCore busFacCore = new BusFacCore();
                    Screen screen = busFacCore.ScreenGet(id);
                    if (screen == null)
                    {
                        screen = new Screen();
                        screen.ScreenID = id;
                        saveObjectData(screen);
                        busFacCore.ScreenCreateOrModify(screen);
                        load();
                    }
                    else
                    {
                        MessageBox.Show("Item exists");
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Action");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void update()
        {
            long id = 0;
            try
            {
                if ((long.TryParse(this.screenIdTextBox.Text, out id)) && (id > 0))
                {
                     BusFacCore busFacCore = new BusFacCore();
                     Screen screen = busFacCore.ScreenGet(id);
                     if (screen != null)
                     {
                         screen.ScreenID = id;
                         saveObjectData(screen);
                         busFacCore.ScreenCreateOrModify(screen);
                         load();
                     }
                }
                else
                {
                    MessageBox.Show("Invalid Action");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void saveObjectData(Screen screen)
        {
            screen.ScreenID = screen.ScreenID;
            screen.ScreenText = this.screenTextTextBox.Text;
    
            screen.IsAudio = (bool)this.isAudioCheckBox.IsChecked;
            screen.IsText = (bool)this.isTextCheckBox.IsChecked;
            long tmpScreenTypeID = 0;
            if (long.TryParse(this.screenTypeIdTextBox.Text, out tmpScreenTypeID))
            {
                screen.ScreenTypeID = tmpScreenTypeID;
            }
            
            if (this.imageDataPathTextBox.Text.Trim().Length > 0)
            {
                byte[] imageData = Veracity.Client.Proxy.ImageHelper.byteFromPath(this.imageDataPathTextBox.Text);
                screen.ScreenImage = imageData;
            }

            if (this.audioDataPathTextBox.Text.Trim().Length > 0)
            {
                byte[] audioData = Veracity.Client.Proxy.ImageHelper.byteFromPath(this.audioDataPathTextBox.Text);
                screen.AudioData = audioData;
            }
        }
        private void populate(Screen o)
        {
            try
            {
                clear();
                this.screenIdTextBox.Text = o.ScreenID.ToString();
                this.screenTextTextBox.Text = o.ScreenText.ToString();
                if (o.ScreenImage != null)
                {
                    this.imageDataTextBox.Text = o.ScreenImage.LongLength.ToString();
                    //string imgStr = Veracity.Client.Proxy.ImageHelper.ByteArrayToString(o.ScreenImage);
                    //this.imageDataTextBox.Text = imgStr;
                }
                if (o.AudioData != null)
                {
                    this.audioTextTextBox.Text = o.AudioData.LongLength.ToString();

                    //string imgStr = Veracity.Client.Proxy.ImageHelper.ByteArrayToString(o.AudioData);
                    //this.audioTextTextBox.Text = imgStr;
                    //this.audioTextTextBox.Text = Veracity.Client.Proxy.ImageHelper.ByteArrayToBase64(o.AudioData);
                }
                this.isTextCheckBox.IsChecked = o.IsText;
                this.isAudioCheckBox.IsChecked = o.IsAudio;
                this.screenTypeIdTextBox.Text = o.ScreenTypeID.ToString();
                this.dateCreatedTextBox.Text = o.DateCreated.ToLongDateString() + " " + o.DateCreated.ToLongTimeString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void clear()
        {
            try
            {
                this.screenIdTextBox.Text = string.Empty;
                this.screenTextTextBox.Text = string.Empty;
                this.imageDataTextBox.Text = string.Empty;
                this.audioTextTextBox.Text = string.Empty;
                this.isTextCheckBox.IsChecked = false;
                this.isAudioCheckBox.IsChecked = false;
                this.screenTypeIdTextBox.Text = string.Empty;
                this.dateCreatedTextBox.Text = string.Empty;
                this.imageDataPathTextBox.Text = string.Empty;
                this.audioDataPathTextBox.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void delete()
        {
            long id = 0;
            try
            {
                if ((long.TryParse(this.screenIdTextBox.Text, out id)) && (id > 0))
                {
                    BusFacCore busFacCore = new BusFacCore();
                    Screen screen = busFacCore.ScreenGet(id);
                    if (screen != null)
                    {
                        screen.ScreenID = id;
                        busFacCore.ScreenRemove(id);
                        clear();
                    }
                }
                else
                {
                    MessageBox.Show("Invalid Action");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:  " + ex.StackTrace);
            }
        }

        private void audioBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension

            // Display OpenFileDialog by calling ShowDialog method
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                this.audioDataPathTextBox.Text = filename;
            }
        }

      
    }
}
