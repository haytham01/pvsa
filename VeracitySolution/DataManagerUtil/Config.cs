﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DataManagerUtil
{
    public class Config
    {
        private string _strConnectionString = null;

        private static readonly string KEY_CONNECTION_STRING = "ConnectionString";

        public string ConnectionString
        {
            get { return _strConnectionString; }
            set { _strConnectionString = value; }
        }

        public Config()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();
            try
            {
                try
                {
                    // get the connection string
                    ConnectionString = (string)configurationAppSettings.GetValue(KEY_CONNECTION_STRING, typeof(System.String));
                }
                catch (Exception e)
                {
                    ConnectionString = null;
                }
            }
            catch (Exception e)
            {
            }

        }
    }
}
