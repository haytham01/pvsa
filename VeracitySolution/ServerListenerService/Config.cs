﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ServerListenerService
{
    public class Config
    {
        private string _strServiceName = null;

        private static readonly string KEY_SERVICE_NAME = "ServiceName";

        public string ServiceName
        {
            get { return _strServiceName; }
            set { _strServiceName = value; }
        }

        public Config()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();

            try
            {
                _strServiceName = (string)configurationAppSettings.GetValue(KEY_SERVICE_NAME, typeof(System.String));
            }
            catch
            {
                _strServiceName = null;
            }
        }
    }
}
