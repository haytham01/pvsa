﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.ServiceModel;
using System.IO;
using System.Runtime.Serialization;
using IDocumentLibrary;

namespace ServerListenerService
{
    public partial class mainService : ServiceBase
    {
        private ServiceHost svh = null;

        public mainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            StartWCFService();
        }

        public void StartWCFService()
        {
            try
            {
                NetTcpBinding ntcp = new NetTcpBinding();

                ntcp.MaxBufferPoolSize = 2147483647;
                ntcp.MaxReceivedMessageSize = 2147483647;
                ntcp.MaxBufferSize = 2147483647;
                ntcp.ReaderQuotas.MaxStringContentLength = 2147483647;
                ntcp.ReaderQuotas.MaxDepth = 2147483647;
                ntcp.ReaderQuotas.MaxBytesPerRead = 2147483647;
                ntcp.ReaderQuotas.MaxNameTableCharCount = 2147483647;
                ntcp.ReaderQuotas.MaxArrayLength = 2147483647;
                ntcp.SendTimeout = new TimeSpan(1, 10, 0);
                ntcp.ReceiveTimeout = new TimeSpan(1, 10, 0);
                ntcp.Security.Mode = SecurityMode.None;
                //ntcp.OpenTimeout 
                //ntcp.CloseTimeout 

                svh = new ServiceHost(typeof(DocumentLibraryService));
                ((ServiceBehaviorAttribute)svh.Description.Behaviors[0]).MaxItemsInObjectGraph = 2147483647;
                svh.AddServiceEndpoint(
                    typeof(IDocumentLibraryService),
                    ntcp,
                    "net.tcp://localhost:8000");
                svh.Open();
            }
            catch (Exception e)
            {
                //Trace.WriteLine(e.Message);
            }
        }

        protected override void OnStop()
        {
            try
            {
                svh.Close();
            }
            catch (Exception e)
            {
            }
        }
    }

    public class DocumentLibraryService : IDocumentLibraryService
    {
        public void UploadDocument(string fPath, byte[] data)
        {
            string filePath = fPath;
            FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.Write);
            fs.Write(data, 0, data.Length);
            fs.Close();
        }


        public byte[] DownloadDocument(string fPath)
        {
            string filePath = fPath;

            // read the file and return the byte[
            using (FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, (int)fs.Length);
                return buffer;
            }
        }

        public string GetDirList(string dPath)
        {
            StringBuilder sbReturn = new StringBuilder();
            sbReturn.Append("<DirList>");
            string TAG_FILE_STATUS = "Status";
            string strStatus = "OK";
            try
            {
                string[] fileNames = null;
                string filename = null;

                string TAG_FILE = "File";
                string TAG_FILE_NAME = "Name";
                string TAG_FILE_DATE_MODIFIED = "DateModified";

                //string filePattern = "*";
                fileNames = Directory.GetFiles(dPath);
                foreach (String f in fileNames)
                {
                    filename = System.IO.Path.GetFileName(f);
                    DateTime lastWriteTime = File.GetLastWriteTime(f);
                    sbReturn.Append("<" + TAG_FILE + ">");
                    sbReturn.Append("<" + TAG_FILE_NAME + ">" + filename + "</" + TAG_FILE_NAME + ">");
                    sbReturn.Append("<" + TAG_FILE_DATE_MODIFIED + ">" + lastWriteTime + "</" + TAG_FILE_DATE_MODIFIED + ">");
                    sbReturn.Append("</" + TAG_FILE + ">");
                }
                strStatus = "OK";

            }
            catch (Exception ex)
            {
                strStatus = "ERROR";
            }
            finally
            {
                sbReturn.Append("<" + TAG_FILE_STATUS + ">" + strStatus + "</" + TAG_FILE_STATUS + ">");
                sbReturn.Append("</DirList>");
            }
            return sbReturn.ToString();
        }
    }
}
