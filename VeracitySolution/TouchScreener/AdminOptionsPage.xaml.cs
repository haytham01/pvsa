﻿using System;
using System.Windows;
using System.Windows.Controls;
using Veracity.Client.Proxy;
using System.Diagnostics;
using System.Reflection;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for AdminOptionsPage.xaml
    /// </summary>
    public partial class AdminOptionsPage : Page
    {
        private bool _hasError = false;

        public string Page1ReportTitle = null;
        public string Page2ReportTitle = null;
        public string Page1SubTitle = null;
        public string IssueATitle = null;
        public string IssueBTitle = null;
        public string IssueCTitle = null;
        public string IssueDTitle = null;
        public int Page1LeftMargin = 0;
        public int Page1SubtitleLeftMargin = 0;
        public int Page2LeftMargin = 0;
        public string EmailAddress = null;

        public string Q1ShortText = null;
        public string Q2ShortText = null;
        public string Q3ShortText = null;
        public string Q4ShortText = null;
        public string Q5ShortText = null;
        public string Q6ShortText = null;
        public string Q7ShortText = null;
        public string Q8ShortText = null;
        public string Q9ShortText = null;
        public string Q10ShortText = null;
        public string Q11ShortText = null;
        public string Q12ShortText = null;
        public string Q13ShortText = null;
        public string Q14ShortText = null;
        public string Q15ShortText = null;
        public string Q16ShortText = null;
        public string Q17ShortText = null;
        public string Q18ShortText = null;


        public AdminOptionsPage()
        {
            InitializeComponent();
            loadInfo();
        }
        private void loadInfo()
        {

            // load the Questions just in case the user presses the CANCEL button
            Q1ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[0].ScreenTextShort;
            Q2ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[1].ScreenTextShort;
            Q3ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[2].ScreenTextShort;
            Q4ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[3].ScreenTextShort;
            Q5ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[4].ScreenTextShort;
            Q6ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[5].ScreenTextShort;
            Q7ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[6].ScreenTextShort;
            Q8ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[7].ScreenTextShort;
            Q9ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[8].ScreenTextShort;
            Q10ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[9].ScreenTextShort;
            Q11ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[10].ScreenTextShort;
            Q12ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[11].ScreenTextShort;
            Q13ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[12].ScreenTextShort;
            Q14ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[13].ScreenTextShort;
            Q15ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[14].ScreenTextShort;
            Q16ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[15].ScreenTextShort;
            Q17ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[16].ScreenTextShort;
            Q18ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[17].ScreenTextShort;

            this.titleTextBlock.Text = "CONFIGURING TEST \"" + MyGlobals.gCurrentTest.objHostConfig.TestButtonText + "\" ONLY";

            this.kioskModeCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.IsKioskMode;
            this.doPhotoCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.DoPhoto;
            this.printReportCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.DoReportPrinting;
            this.printPreviewCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.DoReportPreview;
            this.tstAutoAdjustCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.AutoAdjust;
            this.multiplierCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;

            this.isAudioOnlyScreenCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.IsAudioOnly;
            this.showResultsScreenhCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.ShowResultsScreen;
            //this.doGraphScreenCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.DoGraph;
            //this.turnOffSPRWeightCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffExtraSPRWeight;

            this.turnOffSPR1CheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR1;
            this.turnOffSPR2CheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR2;
            this.turnOffSPR3CheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR3;
            this.turnOnPressureCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOnPressureSensor;
            this.turnOffRiskAnalysisCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffRiskAnalysis;
            this.turnOffSPR71115CheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR71115;
            this.turnOffMovingTestAverageCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffMovingTestAverage;
            //this.turnOffIDScreenCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TurnOffIDScreen;
       

            if (MyGlobals.gCurrentTest.objHostConfig.TestButtonText != null)
            {
                this.testNameTextBox.Text = MyGlobals.gCurrentTest.objHostConfig.TestButtonText;
            }
            this.tslFloorOnCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.TslFloorOn;

            //this.printGraphToPDFCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.PrintGraphToPDF;
            //this.printReportToPDFCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF;
            //this.doPrintGraphCheckBox.IsChecked = MyGlobals.gCurrentTest.objHostConfig.PrintGraph;

            int tslPercentage = MyGlobals.gCurrentTest.objHostConfig.TslValue;
            //int tslMinPercentage = MyGlobals.gCurrentTest.objHostConfig.TslMinValue;

            if (tslPercentage == 0)
            {
                tslPercentage = getDefaultTslValue();
            }


            this.tslStackpanel.Visibility = System.Windows.Visibility.Visible;
            this.tslLabel.Visibility = System.Windows.Visibility.Visible;
            this.tslValueTextBox.Text = Convert.ToString(tslPercentage);
            this.tslStopValueTextBox.Text = Convert.ToString(MyGlobals.gCurrentTest.objHostConfig.TslValueLow);

            this.hitsValueTextBox.Text = Convert.ToString(Convert.ToInt32(MyGlobals.gCurrentTest.objHostConfig.HitsValue));
            this.multiplierValueTextBox.Text = Convert.ToString(Convert.ToInt32(MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue));
            doHitsCheck();

            this.multiplierStackpanel.Visibility = System.Windows.Visibility.Visible;
            this.tslFloorStackpanel.Visibility = System.Windows.Visibility.Visible;
            this.multiplierLabel.Visibility = System.Windows.Visibility.Visible;
            this.multiplierValueTextBox.Text = Convert.ToString(Convert.ToInt32(MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue));
            doMultiplierCheck();
            doTslFloorCheck();

            PrintManager printManager = new PrintManager(MyGlobals.translateResourceManager);
            if ((MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle.Length > 0))
            {
                Page1ReportTitle = MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle;
            }
            else
            {
                Page1ReportTitle = printManager.sTitle18Q;
            }

            if ((MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle.Length > 0))
            {
                Page2ReportTitle = MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle;
            }
            else
            {
                Page2ReportTitle = printManager.sTitle18QPage2;
            }

            if ((MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle.Length > 0))
            {
                Page1SubTitle = MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle;
            }
            else
            {
                Page1SubTitle = printManager.sSubTitle18Q;
            }

            EmailAddress = MyGlobals.gCurrentTest.objHostConfig.EmailAddress;

            if ((MyGlobals.gCurrentTest.objHostConfig.IssueATitle != null) && (MyGlobals.gCurrentTest.objHostConfig.IssueATitle.Length > 0))
            {
                IssueATitle = MyGlobals.gCurrentTest.objHostConfig.IssueATitle;
            }
            else
            {
                IssueATitle = printManager.sIssueATitle18Q;
            }

            if ((MyGlobals.gCurrentTest.objHostConfig.IssueBTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.IssueBTitle.Length > 0))
            {
                IssueBTitle = MyGlobals.gCurrentTest.objHostConfig.IssueBTitle;
            }
            else
            {
                IssueBTitle = printManager.sIssueBTitle18Q;
            }

            if ((MyGlobals.gCurrentTest.objHostConfig.IssueCTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.IssueCTitle.Length > 0))
            {
                IssueCTitle = MyGlobals.gCurrentTest.objHostConfig.IssueCTitle;
            }
            else
            {
                IssueCTitle = printManager.sIssueCTitle18Q;
            }

            if ((MyGlobals.gCurrentTest.objHostConfig.IssueDTitle != null) && (MyGlobals.gCurrentTest.objHostConfig.IssueDTitle.Length > 0))
            {
                IssueDTitle = MyGlobals.gCurrentTest.objHostConfig.IssueDTitle;
            }
            else
            {
                IssueDTitle = printManager.sIssueDTitle18Q;
            }

            if (MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin == 0)
            {
                Page1LeftMargin = printManager.iTitle18QLeftMargin;
            }
            else
            {
                Page1LeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin;
            }

            if (MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin == 0)
            {
                Page2LeftMargin = printManager.iTitle18QPage2LeftMargin;
            }
            else
            {
                Page2LeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin;
            }

            if (MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin == 0)
            {
                Page1SubtitleLeftMargin = printManager.iSubTitle18QLeftMargin;
            }
            else
            {
                Page1SubtitleLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin;
            }

            int tslFloor = MyGlobals.gCurrentTest.objHostConfig.TslFloor;
            if (tslFloor == 0)
            {
                Anaylyzer18Questions a = new Anaylyzer18Questions();
                tslFloor = Convert.ToInt32(a.eTSLFloor);
            }

            this.tslFloorValueTextBox.Text = Convert.ToString(tslFloor);


            MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle = Page1ReportTitle;
            MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle = Page2ReportTitle;
            MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle = Page1SubTitle;
            MyGlobals.gCurrentTest.objHostConfig.IssueATitle = IssueATitle;
            MyGlobals.gCurrentTest.objHostConfig.IssueBTitle = IssueBTitle;
            MyGlobals.gCurrentTest.objHostConfig.IssueCTitle = IssueCTitle;
            MyGlobals.gCurrentTest.objHostConfig.IssueDTitle = IssueDTitle;
            MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin = Page1LeftMargin;
            MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin = Page2LeftMargin;
            MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin = Page1SubtitleLeftMargin;

            this.isMECheckBox.IsChecked = MyGlobals.gAppController.IsMissionEssentials;
            this.savePDFResultsCheckBox.IsChecked = MyGlobals.gAppController.SavePDFResults;
            this.doOutputCSVEncryptionCheckBox.IsChecked = MyGlobals.gAppController.DoOutputCSVEncryption;

            this.numTestLeftLabel.Content = MyGlobals.gAppController.TestCurrentNum + " tests left";
            if (MyGlobals.gAppController.TestLimitNum != -1)
            {
                numTestValueTextBox.Text = MyGlobals.gAppController.TestLimitNum.ToString();
                this.limitTestCheckBox.IsChecked = true;
                this.numTestStackpanel.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.limitTestCheckBox.IsChecked = false;
                this.numTestStackpanel.Visibility = System.Windows.Visibility.Collapsed;
            }

            //if (tslPercentage > 0)
            //{
            //}
            //else
            //{
            //    this.tslStackpanel.Visibility = System.Windows.Visibility.Collapsed;
            //    this.tslLabel.Visibility = System.Windows.Visibility.Collapsed;
            //}

            //if (MyGlobals.gTestSession.LastTslValue >= 0)
            //{
            //    this.lastTestTSLLabel.Content = Convert.ToString(MyGlobals.gTestSession.LastTslValue);
            //}
            // shell screens
            //foreach (HostConfig.ClientShellScreen clientShellScreen in MyGlobals.gCurrentTest.objHostConfig.lstClientShellScreen)
            //{
            //    if (clientShellScreen == HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE)
            //    {
            //        this.showDriverLicenseReaderCheckBox.IsChecked = true;
            //    }

            //    if (clientShellScreen == HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION)
            //    {
            //        this.showDriverLicenseInputCheckBox.IsChecked = true;
            //    }

            //    if (clientShellScreen == HostConfig.ClientShellScreen.INPUT_FIRST_NAME)
            //    {
            //        this.showFirstNameScreenCheckBox.IsChecked = true;
            //    }

            //    if (clientShellScreen == HostConfig.ClientShellScreen.INPUT_LAST_NAME)
            //    {
            //        this.showLastNameScreenCheckBox.IsChecked = true;
            //    }
            //}


        }

        //private double getNumHits()
        //{
        //    double hits = 2;
        //    AnalyzerKOTQuestions analyzerKOT = null;
        //    Anaylyzer24Questions analyzer24 = null;
        //    Anaylyzer26Questions analyzer26 = null;
        //    Analyzer15Questions analyzer15 = null;
        //    switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
        //    {
        //        case 26:
        //            analyzer26 = new Anaylyzer26Questions();
        //            hits = analyzer26.eTotalNumReaction;
        //            break;
        //        case 24:
        //            analyzer24 = new Anaylyzer24Questions();
        //            hits = analyzer26.eTotalNumReaction;
        //            break;
        //        case 16:
        //            analyzerKOT = new AnalyzerKOTQuestions();
        //            hits = analyzer26.eTotalNumReaction;
        //            break;
        //        case 15:
        //            analyzer15 = new Analyzer15Questions();
        //            hits = analyzer26.eTotalNumReaction;
        //            break;
        //        default:
        //            break;
        //    }

        //    return hits;
        //}

        private int getDefaultTslValue()
        {
            int tslPercentage = 0;
            AnalyzerKOTQuestions analyzerKOT = null;
            Anaylyzer24Questions analyzer24 = null;
            Analyzer15Questions analyzer15 = null;
            switch (MyGlobals.gCurrentTest.lstScreenQuestionsOnly.Count)
            {
                case 24:
                    analyzer24 = new Anaylyzer24Questions();
                    tslPercentage = analyzer24.TSLPercentage;
                    break;
                case 15:
                    analyzer15 = new Analyzer15Questions();
                    tslPercentage = analyzer15.TSLPercentage;
                    break;
                case 16:
                    analyzerKOT = new AnalyzerKOTQuestions();
                    tslPercentage = analyzerKOT.TSLPercentage;
                    break;
                default:
                    break;
            }
            return tslPercentage;
        }

        private void returnButton_Click(object sender, RoutedEventArgs e)
        {
            object page = new AdminPasswordPage();
            this.NavigationService.Navigate(page);
        }

        private bool isValid()
        {
            bool bValid = true;
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslValueTextBox.Text.Trim());
            }
            catch
            {
                _hasError = true;
                bValid = false;
            }
            return bValid;
        }
        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (isValid())
                {
                    MyGlobals.gCurrentTest.objHostConfig.AutoAdjust = (bool)this.tstAutoAdjustCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.IsKioskMode = (bool)this.kioskModeCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.DoPhoto = (bool)this.doPhotoCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.DoReportPrinting = (bool)this.printReportCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.DoReportPreview = (bool)this.printPreviewCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier = (bool)this.multiplierCheckBox.IsChecked;

                    MyGlobals.gCurrentTest.objHostConfig.IsAudioOnly = (bool)this.isAudioOnlyScreenCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.ShowResultsScreen = (bool)this.showResultsScreenhCheckBox.IsChecked;
                    //MyGlobals.gCurrentTest.objHostConfig.TurnOffExtraSPRWeight = (bool)this.turnOffSPRWeightCheckBox.IsChecked;

                    MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR1 = (bool)this.turnOffSPR1CheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR2 = (bool)this.turnOffSPR2CheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR3 = (bool)this.turnOffSPR3CheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOffRiskAnalysis = (bool)this.turnOffRiskAnalysisCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR71115 = (bool)this.turnOffSPR71115CheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOffMovingTestAverage = (bool)this.turnOffMovingTestAverageCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TurnOnPressureSensor = (bool)this.turnOnPressureCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.TestButtonText = this.testNameTextBox.Text.Trim();
                    //MyGlobals.gCurrentTest.objHostConfig.PrintGraphToPDF = (bool)this.printGraphToPDFCheckBox.IsChecked;
                    //MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF = (bool)this.printReportToPDFCheckBox.IsChecked;
                    MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF = (bool)this.savePDFResultsCheckBox.IsChecked;

                    // 20181112 - if the PrintReportToPDF was truly check, then do NOT delete the XML and PDF file
                    if (( MyGlobals.gCurrentTest.objHostConfig.EmailAddress != null ) && (MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF == false))
                    {
                        MyGlobals.gCurrentTest.objHostConfig.DeletePDFReport = true;
                        MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF = true;
                       
                        //MessageBox.Show("2. Setup to delete PDF files");
                
                    }

                    //MyGlobals.gCurrentTest.objHostConfig.TurnOffIDScreen = (bool)this.turnOffIDScreenCheckBox.IsChecked;

                    try
                    {
                        MyGlobals.gCurrentTest.objHostConfig.TslFloor = Convert.ToInt32(this.tslFloorValueTextBox.Text.Trim());
                    }
                    catch { MyGlobals.gCurrentTest.objHostConfig.TslFloor = 0; }

                    MyGlobals.gCurrentTest.objHostConfig.TslFloorOn = (bool)this.tslFloorOnCheckBox.IsChecked;
                    //MyGlobals.gCurrentTest.objHostConfig.PrintGraph = (bool)this.doPrintGraphCheckBox.IsChecked;

                    int tmpTslValue = Convert.ToInt32(this.tslValueTextBox.Text.Trim());
                    MyGlobals.gCurrentTest.objHostConfig.TslValue = tmpTslValue;

                    int tmpTslStopValue = Convert.ToInt32(this.tslStopValueTextBox.Text.Trim());
                    MyGlobals.gCurrentTest.objHostConfig.TslValueLow = tmpTslStopValue;

                    double tmpHitsValue = Convert.ToInt32(this.hitsValueTextBox.Text.Trim());
                    MyGlobals.gCurrentTest.objHostConfig.HitsValue = tmpHitsValue;

                    double tmpAvgValue = Convert.ToInt32(this.multiplierValueTextBox.Text.Trim());
                    MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue = tmpAvgValue;

                    MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle = Page1ReportTitle;
                    MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle = Page2ReportTitle;
                    MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle = Page1SubTitle;
                    MyGlobals.gCurrentTest.objHostConfig.IssueATitle = IssueATitle;
                    MyGlobals.gCurrentTest.objHostConfig.IssueBTitle = IssueBTitle;
                    MyGlobals.gCurrentTest.objHostConfig.IssueCTitle = IssueCTitle;
                    MyGlobals.gCurrentTest.objHostConfig.IssueDTitle = IssueDTitle;
                    MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin = Page1LeftMargin;
                    MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin = Page2LeftMargin;
                    MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin = Page1SubtitleLeftMargin;
                    MyGlobals.gCurrentTest.objHostConfig.EmailAddress = EmailAddress;

                    MyGlobals.gAppController.IsMissionEssentials = (bool)this.isMECheckBox.IsChecked;
                    MyGlobals.gAppController.SavePDFResults = (bool)this.savePDFResultsCheckBox.IsChecked;
                    MyGlobals.gAppController.DoOutputCSVEncryption = (bool)this.doOutputCSVEncryptionCheckBox.IsChecked;
                    if (MyGlobals.gAppController.DoOutputCSVEncryption)
                    {
                        // encrypt csv file
                    }
                    else
                    {
                        // decrypt csv file
                    }
                    MyGlobals.gAppController.TestLimitNum = -1;
                    if ((bool) this.limitTestCheckBox.IsChecked)
                    {
                        try
                        {
                            MyGlobals.gAppController.TestLimitNum = Convert.ToInt32(this.numTestValueTextBox.Text);
                            if (MyGlobals.gAppController.TestCurrentNum == 0)
                            {
                                MyGlobals.gAppController.TestCurrentNum = MyGlobals.gAppController.TestLimitNum;
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        MyGlobals.gAppController.TestCurrentNum = 0;
                    }
                    
                    
                    int ind = 0;
                    // report questions text save
                    for (int i = 0; i < MyGlobals.gCurrentTest.lstScreen.Count; i++ )
                    {
                        if (MyGlobals.gCurrentTest.lstScreen[i].ScreenTypeID == 3)
                        {
                            ind = i;
                            break;
                        }
                    }
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q1ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q2ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q3ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q4ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q5ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q6ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q7ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q8ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q9ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q10ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q11ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q12ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q13ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q14ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q15ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q16ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q17ShortText;
                    MyGlobals.gCurrentTest.lstScreen[ind++].ScreenTextShort = Q18ShortText;
                    //    MyGlobals.gCurrentTest.lstScreenQuestionsOnly[ind++].ScreenTextShort = Q2ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[2].ScreenTextShort = Q3ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[3].ScreenTextShort = Q4ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[4].ScreenTextShort = Q5ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[5].ScreenTextShort = Q6ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[6].ScreenTextShort = Q7ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[7].ScreenTextShort = Q8ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[8].ScreenTextShort = Q9ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[9].ScreenTextShort = Q10ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[10].ScreenTextShort = Q11ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[11].ScreenTextShort = Q12ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[12].ScreenTextShort = Q13ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[13].ScreenTextShort = Q14ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[14].ScreenTextShort = Q15ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[15].ScreenTextShort = Q16ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[16].ScreenTextShort = Q17ShortText;
                    //MyGlobals.gCurrentTest.lstScreenQuestionsOnly[17].ScreenTextShort = Q18ShortText;

                    //MyGlobals.gCurrentTest.objHostConfig.lstClientShellScreen.Clear();
                    //MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_WELCOME();


                    //if ((MyGlobals.gCurrentTest.lstScreenQuestionsOnly.Count == 26) || (MyGlobals.gCurrentTest.lstScreenQuestionsOnly.Count == 18))
                    //{
                    //    MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_CHOICE_SCREEN();
                    //}
                    //if ((bool)this.showDriverLicenseReaderCheckBox.IsChecked)
                    //{
                    //    MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_IDENTIFICATION_DRIVER_LICENSE();
                    //}
                    //if ((bool)this.showDriverLicenseInputCheckBox.IsChecked)
                    //{
                    //    MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION();
                    //}
                    //if ((bool)this.showFirstNameScreenCheckBox.IsChecked)
                    //{
                    //    MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_INPUT_FIRST_NAME();
                    //}
                    //if ((bool)this.showLastNameScreenCheckBox.IsChecked)
                    //{
                    //    MyGlobals.gCurrentTest.objHostConfig.AddShellScreen_INPUT_LAST_NAME();
                    //}

                    MyGlobals.gAppController.SerializeWorking();
                    if (!MyGlobals.gAppController.HasError)
                    {
                        string controllerFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
                        MyGlobals.SetInitialGlobals(controllerFilePath);
                        //this.NavigationService.Navigate(new WelcomePage());

                        Process.Start(Assembly.GetEntryAssembly().Location);
                        //Application.Current.Shutdown();
                        Process.GetCurrentProcess().Kill();
                    }
                    else
                    {
                        // error has occurred during save features operation
                    }
                }
                else
                {
                    errorTextBlock.Visibility = System.Windows.Visibility.Visible;
                }
            }
            catch { errorTextBlock.Visibility = System.Windows.Visibility.Visible; }


        }

        private void exitAppButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }

        private void minusTSLButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslValueTextBox.Text.Trim());
                if ((nCurrentValue - 1) >= 0)
                {
                    nCurrentValue--;
                    this.tslValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void plusTSLButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslValueTextBox.Text.Trim());
                if ((nCurrentValue + 1) <= 100)
                {
                    nCurrentValue++;
                    this.tslValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }

        }

        private void returnToSessionButton_Click(object sender, RoutedEventArgs e)
        {
            object page = MyGlobals.RouteToCurrentPage();
            this.NavigationService.Navigate(page);
        }

        private void tstAutoAdjustCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void doHitsCheck()
        {
            if ((bool)this.tstAutoAdjustCheckBox.IsChecked)
            {
                this.hitsStackpanel.Visibility = System.Windows.Visibility.Visible;
                this.tslFloorOnCheckBox.IsEnabled = true;
                if ((bool)this.tslFloorOnCheckBox.IsChecked)
                {
                    this.tslFloorStackpanel.Visibility = System.Windows.Visibility.Visible;
                }

            }
            else
            {
                this.hitsStackpanel.Visibility = System.Windows.Visibility.Collapsed;
                this.tslFloorStackpanel.Visibility = System.Windows.Visibility.Collapsed;
                this.tslFloorOnCheckBox.IsChecked = false;
                this.tslFloorOnCheckBox.IsEnabled = false;
            }
        }

        private void doMultiplierCheck()
        {
            if ((bool)this.multiplierCheckBox.IsChecked)
            {
                this.multiplierStackpanel.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.multiplierStackpanel.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void doTestLimitCheck()
        {
            if ((bool)this.limitTestCheckBox.IsChecked)
            {
                this.numTestStackpanel.Visibility = System.Windows.Visibility.Visible;
                this.numTestLeftLabel.Visibility = Visibility.Visible;
            }
            else
            {
                this.numTestStackpanel.Visibility = System.Windows.Visibility.Collapsed;
                this.numTestLeftLabel.Visibility = Visibility.Collapsed;
            }
        }
        private void doTslFloorCheck()
        {
            if (((bool)this.tslFloorOnCheckBox.IsChecked) && ((bool)this.tstAutoAdjustCheckBox.IsChecked))
            {
                this.tslFloorStackpanel.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.tslFloorStackpanel.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void tstAutoAdjustCheckBox_Click(object sender, RoutedEventArgs e)
        {
            doHitsCheck();
        }

        private void minusHitsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.hitsValueTextBox.Text.Trim());
                if ((nCurrentValue - 1) >= 0)
                {
                    nCurrentValue--;
                    this.hitsValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void plusHitsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.hitsValueTextBox.Text.Trim());
                if ((nCurrentValue + 1) <= 100)
                {
                    nCurrentValue++;
                    this.hitsValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void multiplierCheckBox_Click(object sender, RoutedEventArgs e)
        {
            doMultiplierCheck();
        }

        private void minusMultiplierButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.multiplierValueTextBox.Text.Trim());
                if ((nCurrentValue - 1) >= 0)
                {
                    nCurrentValue--;
                    this.multiplierValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void plusMultiplierButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.multiplierValueTextBox.Text.Trim());
                if ((nCurrentValue + 1) <= 100)
                {
                    nCurrentValue++;
                    this.multiplierValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void minusTSLStopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslStopValueTextBox.Text.Trim());
                if ((nCurrentValue - 1) >= 0)
                {
                    nCurrentValue--;
                    this.tslStopValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void plusTSLStopButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslStopValueTextBox.Text.Trim());
                if ((nCurrentValue + 1) <= 100)
                {
                    nCurrentValue++;
                    this.tslStopValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void reportOptionsButton_Click(object sender, RoutedEventArgs e)
        {
            ReportOptionsWindow reportOptionsWindow = new ReportOptionsWindow();
            reportOptionsWindow.Page1ReportTitle = Page1ReportTitle;
            reportOptionsWindow.Page2ReportTitle = Page2ReportTitle;
            reportOptionsWindow.Page1SubTitle = Page1SubTitle;
            reportOptionsWindow.IssueATitle = IssueATitle;
            reportOptionsWindow.IssueBTitle = IssueBTitle;
            reportOptionsWindow.IssueCTitle = IssueCTitle;
            reportOptionsWindow.IssueDTitle = IssueDTitle;
            reportOptionsWindow.Page1LeftMargin = Page1LeftMargin;
            reportOptionsWindow.Page1SubtitleLeftMargin = Page1SubtitleLeftMargin;
            reportOptionsWindow.Page2LeftMargin = Page2LeftMargin;
            reportOptionsWindow.EmailAddress = EmailAddress;
            
            reportOptionsWindow.loadInfo();
            reportOptionsWindow.ShowDialog();
            if (reportOptionsWindow.bOk)
            {
                Page1ReportTitle = reportOptionsWindow.Page1ReportTitle;
                Page2ReportTitle = reportOptionsWindow.Page2ReportTitle;
                Page1SubTitle = reportOptionsWindow.Page1SubTitle;
                IssueATitle = reportOptionsWindow.IssueATitle;
                IssueBTitle = reportOptionsWindow.IssueBTitle;
                IssueCTitle = reportOptionsWindow.IssueCTitle;
                IssueDTitle = reportOptionsWindow.IssueDTitle;
                Page1LeftMargin = reportOptionsWindow.Page1LeftMargin;
                Page1SubtitleLeftMargin = reportOptionsWindow.Page1SubtitleLeftMargin;
                Page2LeftMargin = reportOptionsWindow.Page2LeftMargin;
                EmailAddress = reportOptionsWindow.EmailAddress;

                // This is used for the report screen to make it configurable questions
                
                Q1ShortText = reportOptionsWindow.Q1ShortText;
                Q2ShortText = reportOptionsWindow.Q2ShortText;
                Q3ShortText = reportOptionsWindow.Q3ShortText;
                Q4ShortText = reportOptionsWindow.Q4ShortText;
                Q5ShortText = reportOptionsWindow.Q5ShortText;
                Q6ShortText = reportOptionsWindow.Q6ShortText;
                Q7ShortText = reportOptionsWindow.Q7ShortText;
                Q8ShortText = reportOptionsWindow.Q8ShortText;
                Q9ShortText = reportOptionsWindow.Q9ShortText;
                Q10ShortText = reportOptionsWindow.Q10ShortText;
                Q11ShortText = reportOptionsWindow.Q11ShortText;
                Q12ShortText = reportOptionsWindow.Q12ShortText;
                Q13ShortText = reportOptionsWindow.Q13ShortText;
                Q14ShortText = reportOptionsWindow.Q14ShortText;
                Q15ShortText = reportOptionsWindow.Q15ShortText;
                Q16ShortText = reportOptionsWindow.Q16ShortText;
                Q17ShortText = reportOptionsWindow.Q17ShortText;
                Q18ShortText = reportOptionsWindow.Q18ShortText;

                // 20181112 Add this so that we print the report
                if ((MyGlobals.gCurrentTest.objHostConfig.EmailAddress != null) && (MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF == false))
                {
                    MyGlobals.gCurrentTest.objHostConfig.DeletePDFReport = true;
                    MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF = true;

                    //MessageBox.Show("1 - Setup to delete PDF files");
                }
                //    if ((EmailAddress != null) && (EmailAddress.Length > 0)
                //    && ((!((bool)this.printReportToPDFCheckBox.IsChecked))
                //    || (!((bool)this.printReportCheckBox.IsChecked))))
                //{
                //    MessageBox.Show("[Print Report] and [Print to PDF] options must be ON to send email.");
                //}
            }
        }
        private void tslFloorOneCheckBox_Click_1(object sender, RoutedEventArgs e)
        {
            doTslFloorCheck();
        }

        private void minusTslFloorButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslFloorValueTextBox.Text.Trim());
                if ((nCurrentValue - 1) >= 0)
                {
                    nCurrentValue--;
                    this.tslFloorValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void plusTslFloorButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nCurrentValue = Convert.ToInt32(this.tslFloorValueTextBox.Text.Trim());
                if ((nCurrentValue + 1) <= 100)
                {
                    nCurrentValue++;
                    this.tslFloorValueTextBox.Text = Convert.ToString(nCurrentValue);
                }
            }
            catch { _hasError = true; }
        }

        private void tslFloorOnCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void limitTestCheckBox_Click(object sender, RoutedEventArgs e)
        {
            doTestLimitCheck();
        }

       
    }
}
