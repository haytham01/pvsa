﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace TouchScreener
{
    public class Config
    {
        private string _errorMessage = null;
        private string _errorStacktrace = null;
        private bool _hasError = false;        

        private string _strSmtpServer = null;
        private int _nSmtpServerPort = 25;
        private string _strSmtpServerUsername = null;
        private string _strSmtpServerPassword = null;
        private string _strMailFrom = null;
        private string _strMailSubject = null;
        private string _strApplicationTitle = null;

        private static readonly string KEY_SMTP_SERVER = "CredentialsEmailSmtpServer";
        private static readonly string KEY_SMTP_SERVER_PORT = "CredentialsEmailSmtpServerPort";
        private static readonly string KEY_SMTP_SERVER_USERNAME = "CredentialsEmailSmtpServerUsername";
        private static readonly string KEY_SMTP_SERVER_PASSWORD = "CredentialsEmailSmtpServerPassword";
        private static readonly string KEY_MAIL_FROM = "CredentialsEmailFrom";
        private static readonly string KEY_MAIL_SUBJECT = "CredentialsEmailSubject";
        private static readonly string KEY_APPLICATION_TITLE = "ApplicationTitle";

        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        public string MailSubject
        {
            get { return _strMailSubject; }
            set { _strMailSubject = value; }
        }
        public int SmtpServerPort
        {
            get { return _nSmtpServerPort; }
            set { _nSmtpServerPort = value; }
        }
        public string SmtpServer
        {
            get { return _strSmtpServer; }
            set { _strSmtpServer = value; }
        }
        public string SmtpServerUsername
        {
            get { return _strSmtpServerUsername; }
            set { _strSmtpServerUsername = value; }
        }
        public string SmtpServerPassword
        {
            get { return _strSmtpServerPassword; }
            set { _strSmtpServerPassword = value; }
        }
        public string MailFrom
        {
            get { return _strMailFrom; }
            set { _strMailFrom = value; }
        }
        public string ApplicationTitle
        {
            get { return _strApplicationTitle; }
            set { _strApplicationTitle = value; }
        }
        public Config()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();
            try
            {


                try
                {
                    SmtpServer = (string)configurationAppSettings.GetValue(KEY_SMTP_SERVER, typeof(System.String));
                }
                catch (Exception e)
                {
                    SmtpServer = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    string tmp = null;
                    tmp = (string)configurationAppSettings.GetValue(KEY_SMTP_SERVER_PORT, typeof(System.String));
                    try
                    {
                        SmtpServerPort = (int)Convert.ToInt32(tmp);
                    }
                    catch (Exception ex)
                    {
                        SmtpServerPort = 25;
                        _hasError = true;
                        _errorStacktrace = ex.StackTrace.ToString();
                        _errorMessage = ex.Message;
                    }
                }
                catch (Exception e)
                {
                    SmtpServerPort = 25;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    SmtpServerUsername = (string)configurationAppSettings.GetValue(KEY_SMTP_SERVER_USERNAME, typeof(System.String));
                }
                catch (Exception e)
                {
                    SmtpServerUsername = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    SmtpServerPassword = (string)configurationAppSettings.GetValue(KEY_SMTP_SERVER_PASSWORD, typeof(System.String));
                }
                catch (Exception e)
                {
                    SmtpServerPassword = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    MailFrom = (string)configurationAppSettings.GetValue(KEY_MAIL_FROM, typeof(System.String));
                }
                catch (Exception e)
                {
                    MailFrom = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    MailSubject = (string)configurationAppSettings.GetValue(KEY_MAIL_SUBJECT, typeof(System.String));
                }
                catch (Exception e)
                {
                    MailSubject = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }

                try
                {
                    ApplicationTitle = (string)configurationAppSettings.GetValue(KEY_APPLICATION_TITLE, typeof(System.String));
                }
                catch (Exception e)
                {
                    ApplicationTitle = null;
                    _hasError = true;
                    _errorStacktrace = e.StackTrace.ToString();
                    _errorMessage = e.Message;
                }
            }
            catch (Exception exc)
            {
                _hasError = true;
                _errorStacktrace = exc.StackTrace.ToString();
                _errorMessage = exc.Message;

            }
        }

    }
}
