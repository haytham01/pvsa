﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Media;

using Veracity.Client.Proxy;
using System.Windows.Media;
using System.IO;
using System.Globalization;
using System.Resources;

namespace TouchScreener
{
    public class MyGlobals
    {
        //public static bool bDeletePDFFile = false; // 20181112 added global
        public static TestSession gTestSession = null;
        public static AppController gAppController = null;
        public static string gCurrentExecutablePath = null;
        public static string gCurrentExecutableFolderPath = null;
        public static string gResultsFolderPath = null;
        public static Test gCurrentTest = null;
        public static int gCurrentLnkTestTemplateScreenListIndex = 0;
        public static WaitPage gWaitPage = null;
        public static WaitPage2 gWaitPage2 = null;
        public static WaitPage3 gWaitPage3 = null;
        public static int gResetTimerInSeconds = 0;
        public static string gLogFilePath = null;
        public static int currentTestLinkIndex = 0;
        public static string gLinkIdentification = null;

        public static List<Sensor> gLstSensorAll = null;
        public static List<Sensor> gLstSensorPage= null;

        //MyGlobals.gCurrentExecutableFolderPath
        public static readonly string CONTROLLER_FILENAME = "AppController.xml";
        public static readonly string WORKSPACE_FOLDER_NAME = "Workspace";
        public static readonly string RESULTS_FOLDER_NAME = "Results";
        public static readonly string LOCAL_SENSOR_DATA_FOLDER_NAME = "SensorData";
        public static readonly string WORKSPACE_SENSOR_DATA_FOLDER_NAME = "SensorData";
        public static readonly string REGEX_CA_DRIVER_LICENSE = @"^[A-Z]{1}\d{7}$";

        public static readonly string CURRENT_VERSION = "5.23";
        public static readonly string QUICK_ADMIN_PASSWORD = "1234";
        public static readonly string ADVANCED_ADMIN_PASSWORD = "23088032";

        public static bool isSensorOn = false;

        public static bool bDoForwardTestLink = false;

        public static ResourceManager translateResourceManager = new ResourceManager(typeof(TouchScreener.Properties.Resources));

        public static object RouteToNextPage()
        {
            object page = null;

            try
            {
                HostConfig.ClientShellScreen nextClientShellScreen = gCurrentTest.objHostConfig.getNextClientShellScreen();
                page = GetPage(nextClientShellScreen);
            }
            catch { }

            return page;
        }

        public static object RouteToPreviousPage()
        {
            object page = null;

            try
            {
                HostConfig.ClientShellScreen previousClientShellScreen = gCurrentTest.objHostConfig.getPreviousClientShellScreen();
                page = GetPage(previousClientShellScreen);
            }
            catch { }

            return page;
        }

        public static object RouteToCurrentPage()
        {
            object page = null;

            try
            {
                HostConfig.ClientShellScreen nextClientShellScreen = HostConfig.ClientShellScreen.CHOICE_SCREEN;
                if (gCurrentTest != null)
                {
                    nextClientShellScreen = gCurrentTest.objHostConfig.currentClientShellScreen;
                }
                page = GetPage(nextClientShellScreen);
            }
            catch { page = RouteToInitialPage(); }

            return page;
        }

        public static object RouteToInitialPage()
        {
            object page = null;

            try
            {
                HostConfig.ClientShellScreen nextClientShellScreen = HostConfig.ClientShellScreen.WELCOME;
                page = GetPage(nextClientShellScreen);
            }
            catch { }

            return page;
        }
        public static object GetPage(HostConfig.ClientShellScreen pClientShellScreen)
        {
            object page = null;
            switch (pClientShellScreen)
            {
                case HostConfig.ClientShellScreen.WELCOME:
                    page = new WelcomePage();
                    break;
                case HostConfig.ClientShellScreen.CHOICE_SCREEN:
                    page = new ChooseTestPage();
                    break;
                case HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE:
                    page = new IdentificationPage();
                    break;
                case HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION:
                    page = new InputAlphanumPage();
                    break;
                case HostConfig.ClientShellScreen.IDENTIFICATION_PHOTO:
                    page = new PhotoPage();
                    break;
                case HostConfig.ClientShellScreen.INPUT_FIRST_NAME:
                    page = new InputPage();
                    break;
                case HostConfig.ClientShellScreen.INPUT_LAST_NAME:
                    page = new InputPage();
                    break;
                case HostConfig.ClientShellScreen.SCREEN:
                    page = new ScreenPage();
                    break;
                default:
                    break;
            }
            return page;
        }

        ////////////////////////////////////////////////////////////
        ///
        /// 24 QUESTIONS
        ///
        ////////////////////////////////////////////////////////////

        public static bool Parse24QCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;
           
            try
            {
                string sWatchFile = null;
                sWatchFile = MyGlobals.gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + "PVSA-OUTPUT1.CSV";

                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen

                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 146)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {


                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "ADAPT 1";
                                        screen.ScreenTextShort = "ADAPT 1";
                                        break;
                                    case 2:
                                        screen.ScreenText = "ADAPT 2";
                                        screen.ScreenTextShort = "ADAPT 2";
                                        break;
                                    case 3:
                                        screen.ScreenText = "Had accident";
                                        screen.ScreenTextShort = "Had accident";
                                        break;
                                    case 4:
                                        screen.ScreenText = "Failed to report accident";
                                        screen.ScreenTextShort = "Failed to report accident";
                                        break;
                                    case 5:
                                        screen.ScreenText = "Caused Hit & Run accident";
                                        screen.ScreenTextShort = "Caused Hit & Run accident";
                                        break;
                                    case 6:
                                        screen.ScreenText = "Used M/J in last 12 months";
                                        screen.ScreenTextShort = "Used M/J in last 12 months";
                                        break;
                                    case 7:
                                        screen.ScreenText = "Used Drugs in last 3 mos.";
                                        screen.ScreenTextShort = "Used Drugs in last 3 mos.";
                                        break;
                                    case 8:
                                        screen.ScreenText = "Used Drugs in last 30 days";
                                        screen.ScreenTextShort = "Used Drugs in last 30 days";
                                        break;
                                    case 9:
                                        screen.ScreenText = "Committed Sex Crime";
                                        screen.ScreenTextShort = "Committed Sex Crime";
                                        break;
                                    case 10:
                                        screen.ScreenText = "Convicted of Sex Crime";
                                        screen.ScreenTextShort = "Convicted of Sex Crime";
                                        break;
                                    case 11:
                                        screen.ScreenText = "Registered Sex Offender";
                                        screen.ScreenTextShort = "Registered Sex Offender";
                                        break;
                                    case 12:
                                        screen.ScreenText = "Arrest or Convict Traffic Vio";
                                        screen.ScreenTextShort = "Arrest or Convict Traffic Vio";
                                        break;
                                    case 13:
                                        screen.ScreenText = "Arrest or Convict Misdem";
                                        screen.ScreenTextShort = "Arrest or Convict Misdem";
                                        break;
                                    case 14:
                                        screen.ScreenText = "Arrest or Convict Felony";
                                        screen.ScreenTextShort = "Arrest or Convict Felony";
                                        break;
                                    case 15:
                                        screen.ScreenText = "Smuggling associated";
                                        screen.ScreenTextShort = "Associated or affiliated with anyone involved in committing acts of smuggling?";
                                        break;
                                    case 16:
                                        screen.ScreenText = "Smuggling directly involved";
                                        screen.ScreenTextShort = "Smuggling directly involved";
                                        break;
                                    case 17:
                                        screen.ScreenText = "Smuggling in any manner";
                                        screen.ScreenTextShort = "Smuggling in any manner";
                                        break;
                                    case 18:
                                        screen.ScreenText = "Terrorism associated";
                                        screen.ScreenTextShort = "Terrorism associated";
                                        break;
                                    case 19:
                                        screen.ScreenText = "Terrorism directly involved";
                                        screen.ScreenTextShort = "Terrorism directly involved";
                                        break;
                                    case 20:
                                        screen.ScreenText = "Terrorism in any manner";
                                        screen.ScreenTextShort = "Terrorism in any manner";
                                        break;
                                    case 21:
                                        screen.ScreenText = "Unknown Issue";
                                        screen.ScreenTextShort = "Unknown Issue";
                                        break;
                                    case 22:
                                        screen.ScreenText = "Is there anything in your background that you are very concerned?";
                                        screen.ScreenTextShort = "Is there anything in your background that you are very concerned?";
                                        break;
                                    case 23:
                                        screen.ScreenText = "Did you lie to any question";
                                        screen.ScreenTextShort = "Did you lie to any question";
                                        break;
                                    case 24:
                                        screen.ScreenText = "Taken Test before";
                                        screen.ScreenTextShort = "Taken Test before";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    // screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:

                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    // screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    //MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch (Exception ex)
            {
                //MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false;
            }

            return bFoundFile;
        }

        public static void SetInitialGlobals(string controllerFilePath)
        {
            gAppController.ControllerPath = controllerFilePath;
            string controllerXml = File.ReadAllText(controllerFilePath);
            gAppController.Parse(controllerXml);

            gCurrentExecutableFolderPath = System.IO.Path.GetDirectoryName(gCurrentExecutablePath);
            gResultsFolderPath = gCurrentExecutableFolderPath +
                System.IO.Path.DirectorySeparatorChar + WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + RESULTS_FOLDER_NAME;
            if (!System.IO.Directory.Exists(gResultsFolderPath))
            {
                System.IO.Directory.CreateDirectory(gResultsFolderPath);
            }
            gResetTimerInSeconds = 180;
            gLogFilePath = gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + "TOUCHLOG.txt";
        }

        public static void SetTestChoiceGlobals(int pIntTestChoice)
        {
            gCurrentLnkTestTemplateScreenListIndex = 0;
            gCurrentTest = gAppController.lstTest[pIntTestChoice];
            gCurrentTest.selectedTestLanguage = gCurrentTest.lstTestLanguage[0];
            gTestSession = new Veracity.Client.Proxy.TestSession(gAppController.lstTest[pIntTestChoice].objClient);
            gTestSession.objTestTemplate = gAppController.lstTest[pIntTestChoice].objTestTemplate;
            gTestSession.objCurrentTest = gCurrentTest;
            gTestSession.AutoAdjust = gCurrentTest.objHostConfig.AutoAdjust;
            MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.CHOICE_SCREEN;
        }

        public static void SetTestChoiceGlobalsToEmpty()
        {
            gCurrentTest = null;
            gTestSession = null;
        }

        public static void SetTestGlobals()
        {
            MyGlobals.gTestSession.dtBeginSession = DateTime.Now;
            int tslValue = MyGlobals.gCurrentTest.objHostConfig.TslValue;
            if (tslValue == 0)
            {
                tslValue = getDefaultTslValue();
            }

            MyGlobals.gTestSession.BeginTslValue = tslValue;

            string titleText = string.Empty;
            switch (MyGlobals.gCurrentTest.lstScreenQuestionsOnly.Count)
            {
                case 26:
                    titleText = "26Q, " + tslValue;
                    break;
                case 24:
                    titleText = "Eval 24Q, " + tslValue;
                    break;
                case 15:
                    titleText = "PTT Q15, " + tslValue;
                    break;
                case 16:
                    titleText = "KOT, " + tslValue;
                    break;
                default:
                    break;
            }
        }
   
        private static int getDefaultTslValue()
        {
            int tslPercentage = 0;
            AnalyzerKOTQuestions analyzerKOT = null;
            Anaylyzer24Questions analyzer24 = null;
            Analyzer15Questions analyzer15 = null;
            switch (MyGlobals.gCurrentTest.lstScreenQuestionsOnly.Count)
            {
                case 24:
                    analyzer24 = new Anaylyzer24Questions();
                    tslPercentage = analyzer24.TSLPercentage;
                    break;
                case 15:
                    analyzer15 = new Analyzer15Questions();
                    tslPercentage = analyzer15.TSLPercentage;
                    break;
                case 16:
                    analyzerKOT = new AnalyzerKOTQuestions();
                    tslPercentage = analyzerKOT.TSLPercentage;
                    break;
                default:
                    break;
            }
            return tslPercentage;
        }

        public static bool SendEmail(string pStrBody, string pStrTo, string pStrSubject, 
            MemoryStream pAttachmentMemoryStream1, string pAttachmentMemoryStreamFilename1,
            MemoryStream pAttachmentMemoryStream2, string pAttachmentMemoryStreamFilename2,
            MemoryStream pAttachmentMemoryStream3, string pAttachmentMemoryStreamFilename3)
        {
            bool bSuccess = false;
            Config config = new Config();
            if (!config.HasError)
            {
                // Phil's gmail address
                //ptssareports@gmail.com
                //$ELC2308elc$
                Emailer emailer = new Emailer
                {
                    To = pStrTo,
                    Body = pStrBody,
                    DisplayName = "PTSSA TouchScreener",
                    From = config.MailFrom ?? "phil@pvsa.net",
                    SmtpServerName = config.SmtpServer,
                    SmtpServerPort = config.SmtpServerPort,
                    SmtpUsername = config.SmtpServerUsername,
                    SmtpPassword = config.SmtpServerPassword,
                    Subject = pStrSubject,
                    attachmentMemoryStream1 = pAttachmentMemoryStream1,
                    attachmentMemoryStreamFilename1 = pAttachmentMemoryStreamFilename1,
                    attachmentMemoryStream2 = pAttachmentMemoryStream2,
                    attachmentMemoryStreamFilename2 = pAttachmentMemoryStreamFilename2,
                    attachmentMemoryStream3 = pAttachmentMemoryStream3,
                    attachmentMemoryStreamFilename3 = pAttachmentMemoryStreamFilename3,
                    EnableSSL = true
                };

                emailer.SendMail();
                bSuccess = !emailer.HasError;
            }

            return bSuccess;
        }
    }
}
