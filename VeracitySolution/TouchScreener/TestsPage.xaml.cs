﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for TestsPage.xaml
    /// </summary>
    public partial class TestsPage : Page
    {
        public TestsPage()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            loadButtons();
        }

        private void loadButtons()
        {
            if ((MyGlobals.gAppController.lstTest != null) && (MyGlobals.gAppController.lstTest.Count > 0))
            {
                for (int i = 0; i < MyGlobals.gAppController.lstTest.Count; i++)
                {
                    // create the button
                    Button testButton = new Button();
                    Test test = MyGlobals.gAppController.lstTest[i];
                    testButton.Template = (ControlTemplate)FindResource("testButton");
                    testButton.ApplyTemplate();
                    ControlTemplate ct = testButton.Template;

                    Button button = (Button)ct.FindName("templateButton", testButton);
                    if (button != null)
                    {
                        button.Content = test.objTestType.VisibleCode;

                    }
                    testButton.Tag = i;
                    testButton.Click += new RoutedEventHandler(testButtonClick);
                    this.mainWrapPanel.Children.Add(testButton);

                }
            }
        }

        private void testButtonClick(object sender, EventArgs e)
        {
            Button btnClicked = (Button)sender;
            MyGlobals.gCurrentTest = MyGlobals.gAppController.lstTest[Convert.ToInt32(btnClicked.Tag)];
            this.NavigationService.Navigate(new LanguagesPage());
        }
    }
}
