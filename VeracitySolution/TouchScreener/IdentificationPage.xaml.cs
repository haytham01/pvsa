﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for IdentificationPage.xaml
    /// </summary>
    public partial class IdentificationPage : Page
    {
        private string _input = String.Empty;
        private System.Windows.Threading.DispatcherTimer resetTimer = new System.Windows.Threading.DispatcherTimer();

        private void resetTimer_Tick(object sender, EventArgs e)
        {
            resetTimer.Stop();
            this.NavigationService.Navigate(new WelcomePage());
        }
        private void initializeResetTimer()
        {
            resetTimer.Interval = new TimeSpan(0, 0, 0, MyGlobals.gResetTimerInSeconds, 0);
            resetTimer.Tick += new EventHandler(resetTimer_Tick);
            resetTimer.Start();
        }
        public IdentificationPage()
        {
            InitializeComponent();
            initializeResetTimer();

        }
        private void loadData(bool IsError = false)
        {
            string titleText = String.Empty;
            TextBlock tb = this.titleTextBlock;
            tb.TextWrapping = TextWrapping.Wrap;
            tb.Inlines.Clear();
            this.mainColorAnimation.From = Colors.Black;
            this.mainColorAnimation.To = Colors.Orange;
            if (IsError)
            {
                tb.Inlines.Add("YOU MUST ENTER DATA.  ");
                this.mainColorAnimation.From = Colors.Black;
                this.mainColorAnimation.To = Colors.Red;
            }
            tb.Inlines.Add("PLEASE SWIPE YOUR ");
            tb.Inlines.Add(new Run("CALIFORNIA DRIVER LICENSE.") { FontWeight = FontWeights.Bold, FontSize = 32 });
            tb.Inlines.Add(" HIT NEXT FOR MANUAL INPUT.");

        }
        private void keyboardButton_Click(object sender, RoutedEventArgs e)
        {
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadData();
            this.inputTextBox.Focus();
        }
        private void inputTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                MyGlobals.gTestSession.objTestSubject.IdentificationRawData = _input;
                nextPage();
            }
        }
        private void inputTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            _input += e.Text;
            if (e.Text == "?")
            {
                MyGlobals.gTestSession.objTestSubject.IdentificationRawData = _input;
                nextPage();
            }
        }
        private void nextPage()
        {

            
            MyGlobals.gTestSession.ParseCADriverLicense();
            bool isValid = false;
            if (MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber != null)
            {
                isValid = (new Regex(MyGlobals.REGEX_CA_DRIVER_LICENSE)).IsMatch(MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber);
                if (!isValid)
                {
                    MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber = null;
                }
            }

            object page = null;
            page = MyGlobals.RouteToNextPage();
            this.NavigationService.Navigate(page);
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            resetTimer.Stop();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            nextPage();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            object page = null;
            page = MyGlobals.RouteToPreviousPage();
            this.NavigationService.Navigate(page);
        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            object page = new AdminQuickActionsPage();
            this.NavigationService.Navigate(page);
        }

        
    }
}
