﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for WaitPage.xaml
    /// </summary>
    public partial class WaitPage : Page
    {
        private System.Windows.Threading.DispatcherTimer dispatcherTimer = null;
        public WaitPage()
        {
            InitializeComponent();

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            dispatcherTimer.Tick -= dispatcherTimer_Tick;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
            try
            {
                dispatcherTimer.Stop();
                this.NavigationService.Navigate(new ScreenPage());
            }
            catch { }
        }

        public void StartTimer()
        {
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.TransitionDelayInMs);
            dispatcherTimer.Start();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // serialize current session state to csv
            //MyGlobals.gTestSession.SerializeToCsv(MyGlobals.gResultsFolderPath);

        }
    }
}
