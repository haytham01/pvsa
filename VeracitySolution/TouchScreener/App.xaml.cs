﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Veracity.Common;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            
            try
            {
                if (MyGlobals.gAppController.lstTest[0].objHostConfig.IsKioskMode)
                {
                    // Enable Windows Home button on Surface Pro 3, ignore erros if hardware doesn't exist.
                    DisableHardware.DisableDevice(n => n.ToUpperInvariant().Contains("VEN_MSHW&DEV_0028"), false);
                }
            }
            catch
            { }
        }
    }
}
