﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for ReportQuestionsWindow.xaml
    /// </summary>
    public partial class ReportQuestionsWindow : Window
    {
        public bool bOK = false;

        public ReportQuestionsWindow()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            bOK = true;
            this.Hide();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadInfo();
        }

        public void loadInfo()
        {
           this.q1TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[0].ScreenTextShort;
           this.q2TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[1].ScreenTextShort;
           this.q3TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[2].ScreenTextShort;
           this.q4TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[3].ScreenTextShort;
           this.q5TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[4].ScreenTextShort;
           this.q6TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[5].ScreenTextShort;
           this.q7TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[6].ScreenTextShort;
           this.q8TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[7].ScreenTextShort;
           this.q9TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[8].ScreenTextShort;
           this.q10TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[9].ScreenTextShort;
           this.q11TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[10].ScreenTextShort;
           this.q12TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[11].ScreenTextShort;
           this.q13TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[12].ScreenTextShort;
           this.q14TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[13].ScreenTextShort;
           this.q15TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[14].ScreenTextShort;
           this.q16TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[15].ScreenTextShort;
           this.q17TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[16].ScreenTextShort;
           this.q18TextBox.Text = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[17].ScreenTextShort;
        }
    }
}
