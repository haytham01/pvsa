﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for SelectPdfPage.xaml
    /// </summary>
    public partial class SelectPdfPage : Page
    {
        public SelectPdfPage()
        {
            InitializeComponent();

// Solekai added this which caused differnce between the debug and release builds
// removed so we keep it consistent to one type of build
//#if (DEBUG)
//            DirectoryInfo di = new DirectoryInfo(@"C:\Users\matt\Source\Workspaces\Veracity\VeracitySolution\TouchScreener\bin\Release\Workspace\Results");
//#else
//            DirectoryInfo di = new DirectoryInfo(MyGlobals.gResultsFolderPath);
//#endif
            LoadInfo();

        }

        private void LoadInfo()
        {
            DirectoryInfo di = new DirectoryInfo(MyGlobals.gResultsFolderPath);

            //            DirectoryInfo di = new DirectoryInfo(MyGlobals.gResultsFolderPath);

            FileInfo[] fileInfos = di.GetFiles("*.pdf", SearchOption.TopDirectoryOnly).ToArray();

            List<string> pdfFiles = new List<string>();

            foreach (FileInfo fi in fileInfos)
            {
                pdfFiles.Add(System.IO.Path.GetFileName(fi.FullName));
            }

            this.m_PdfListBox.ItemsSource = pdfFiles;
        }
        private void m_PdfListBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
             string itemText = (string)(sender as ListBox).SelectedItem;
             this.filenameTextbox.Text = itemText;
             string path = System.IO.Path.Combine(MyGlobals.gResultsFolderPath, itemText);
             this.pathLabel.Content = "Full path:  " + path;

//#if (DEBUG)
//             string path = System.IO.Path.Combine(@"C:\Users\matt\Source\Workspaces\Veracity\VeracitySolution\TouchScreener\bin\Release\Workspace\Results", itemText);
//#else
//             string path = System.IO.Path.Combine(MyGlobals.gResultsFolderPath, itemText);
//#endif

//             string path = System.IO.Path.Combine(MyGlobals.gResultsFolderPath, itemText);


             //Frame frame = new Frame();
             //m_WebBrowser.Navigate(new Uri(path));
             //frame.Content = m_WebBrowser;

            //m_WebBrowser.Navigate(@"file:///" + path);

           
        }

        private void Done_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void openPdfReader(string filePath)
        {
            try
            {
                var adobe = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Microsoft").OpenSubKey("Windows").OpenSubKey("CurrentVersion").OpenSubKey("App Paths").OpenSubKey("AcroRd32.exe");
                var pathAdobe = adobe.GetValue("");

                Process myProcess = new Process();
                string exeName = System.IO.Path.GetFileName((string)pathAdobe);
                myProcess.StartInfo.FileName = exeName;
                myProcess.StartInfo.Arguments = "/A \"page=1=OpenActions\" " + filePath;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.Start();

            }
            catch(Exception ex)
            {
                MessageBox.Show("An error occurred trying to open PDF file using Acrobat Reader.  Make sure it is installed.");
            }
        }

        private void openButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.filenameTextbox.Text.Trim()))
            {
                string path = System.IO.Path.Combine(MyGlobals.gResultsFolderPath, this.filenameTextbox.Text.Trim());
                openPdfReader(path);
            }
            

        }
    }
}
