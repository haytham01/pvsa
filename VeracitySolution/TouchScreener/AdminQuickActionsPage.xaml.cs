﻿using System.Diagnostics;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for AdminQuickActionsPage.xaml
    /// </summary>
    public partial class AdminQuickActionsPage : Page
    {
        public bool TurnOnEnglishLanguage = false;
        public bool TurnOnSpanishMXLanguage = false;
        public bool TurnOnArabicUAELanguage = false;
        public bool TurnOnPersianAFLanguage = false;
        public bool TurnOnPashtoAFLanguage = false;
        public bool TurnOnPolishPlLanguage = false;

        public AdminQuickActionsPage()
        {
            InitializeComponent();
            this.versionTextBlock.Text = "Version " + MyGlobals.CURRENT_VERSION;
            if (MyGlobals.gCurrentTest == null)
            {
                this.returnToSessionButton.Visibility = System.Windows.Visibility.Collapsed;
                this.advancedButton.Visibility = System.Windows.Visibility.Collapsed;
                this.reportConfigurationButton.Visibility = System.Windows.Visibility.Collapsed;
            }
            foreach(GlobalLanguage g in MyGlobals.gAppController.lstGlobalLanguage)
            {
                if (g.Code == "ar-AE")
                {
                    TurnOnArabicUAELanguage = true;
                }
                if (g.Code == "en")
                {
                    TurnOnEnglishLanguage = true;
                }
                if (g.Code == "es-MX")
                {
                    TurnOnSpanishMXLanguage = true;
                }
                if (g.Code == "prs-AF")
                {
                    TurnOnPersianAFLanguage = true;
                }
                if (g.Code == "ps-AF")
                {
                    TurnOnPashtoAFLanguage = true;
                }
                if (g.Code == "pl-PL")
                {
                    TurnOnPolishPlLanguage = true;
                }
            }
        }

        private void advancedButton_Click(object sender, RoutedEventArgs e)
        {
            object page = new AdminPasswordPage();
            this.NavigationService.Navigate(page);
        }

        private void returnToSessionButton_Click(object sender, RoutedEventArgs e)
        {
            object page = MyGlobals.RouteToCurrentPage();
            this.NavigationService.Navigate(page);
        }

        private void startOverButton_Click(object sender, RoutedEventArgs e)
        {
            object page = null;
            page = MyGlobals.RouteToInitialPage();
            if ((MyGlobals.gAppController.lstGlobalTestLink != null) && (MyGlobals.gAppController.lstGlobalTestLink.Count > 0))
            {
                int ind = MyGlobals.gAppController.lstGlobalTestLink[0].Index;
                MyGlobals.SetTestChoiceGlobals(ind);
                MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.WELCOME;
                page = MyGlobals.RouteToCurrentPage();
            }

            this.NavigationService.Navigate(page);
        }

        private void aboutButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("PTSSA TouchScreener Version " + MyGlobals.CURRENT_VERSION, "About");
        }

        private void exitAppButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Do you want to exit the application?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Application.Current.MainWindow.Close();
            }
        }

        private void historyButton_Click(object sender, RoutedEventArgs e)
        {
            HistoryWindow historyWindow = new HistoryWindow();
            historyWindow.ShowDialog();
        }

        private void reportConfigurationButton_Click(object sender, RoutedEventArgs e)
        {
            ReportOptionsWindow reportOptionsWindow = new ReportOptionsWindow();
            reportOptionsWindow.okButton.Visibility = System.Windows.Visibility.Hidden;
            reportOptionsWindow.saveButton.Visibility = System.Windows.Visibility.Visible;
            reportOptionsWindow.ShowDialog();
            if (reportOptionsWindow.bOk)
            {

            }
            reportOptionsWindow = null;

        }

        private void languagesButton_Click(object sender, RoutedEventArgs e)
        {
            LanguagesWindow languagesWindow = new LanguagesWindow();
            languagesWindow.englishCheckBox.IsChecked = TurnOnEnglishLanguage;
            languagesWindow.spanishMXCheckBox.IsChecked = TurnOnSpanishMXLanguage;
            languagesWindow.ArabicAECheckBox.IsChecked = TurnOnArabicUAELanguage;
            languagesWindow.PersianAFCheckBox.IsChecked = TurnOnPersianAFLanguage;
            languagesWindow.PashtoAFCheckBox.IsChecked = TurnOnPashtoAFLanguage;
            languagesWindow.PolishPlCheckBox.IsChecked = TurnOnPolishPlLanguage;
            languagesWindow.ShowDialog();
            if (languagesWindow.bOk)
            {
                TurnOnArabicUAELanguage = (bool)languagesWindow.ArabicAECheckBox.IsChecked;
                TurnOnEnglishLanguage = (bool)languagesWindow.englishCheckBox.IsChecked;
                TurnOnSpanishMXLanguage = (bool)languagesWindow.spanishMXCheckBox.IsChecked;
                TurnOnPersianAFLanguage = (bool)languagesWindow.PersianAFCheckBox.IsChecked;
                TurnOnPashtoAFLanguage = (bool)languagesWindow.PashtoAFCheckBox.IsChecked;
                TurnOnPolishPlLanguage = (bool)languagesWindow.PolishPlCheckBox.IsChecked;

                MyGlobals.gAppController.lstGlobalLanguage.Clear();
                if ((!TurnOnArabicUAELanguage) && (!TurnOnEnglishLanguage) && (!TurnOnSpanishMXLanguage))
                {
                    TurnOnEnglishLanguage = true;
                }
                GlobalLanguage globalLanguage = null;
                if (TurnOnArabicUAELanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "ar-AE", Description = "Arabic-UAE" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }
                if (TurnOnEnglishLanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "en", Description = "English" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }
                if (TurnOnSpanishMXLanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "es-MX", Description = "Spanish-Mexican" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }
                if (TurnOnPersianAFLanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "prs-AF", Description = "Persian-AF" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }
                if (TurnOnPashtoAFLanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "ps-AF", Description = "Pashto-AF" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }
                if (TurnOnPolishPlLanguage)
                {
                    globalLanguage = new GlobalLanguage() { Code = "pl-PL", Description = "Polish-PL" };
                    MyGlobals.gAppController.lstGlobalLanguage.Add(globalLanguage);
                }

                MyGlobals.gAppController.SerializeWorking();
                Process.Start(Assembly.GetEntryAssembly().Location);
                Process.GetCurrentProcess().Kill();

//                string controllerFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
//System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
//                MyGlobals.SetInitialGlobals(controllerFilePath);
//                this.NavigationService.Navigate(new WelcomePage());

            }
        }

        private void viewReportsButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new SelectPdfPage());
        }

        private void linksButton_Click(object sender, RoutedEventArgs e)
        {
            LinkSummaryWindow linkSummaryWindow = new LinkSummaryWindow();
            linkSummaryWindow.ShowDialog();
            if (linkSummaryWindow.bOk)
            {
                MyGlobals.gAppController.lstGlobalTestLink = linkSummaryWindow.lstTestLink;
                MyGlobals.gAppController.SerializeWorking();
            }
        }
    }
}
