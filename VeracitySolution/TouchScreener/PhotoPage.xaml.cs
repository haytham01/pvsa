﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AForge.Controls;
using AForge.Video;
using AForge.Video.DirectShow;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using System.Drawing;


namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for PhotoPage.xaml
    /// </summary>
    public partial class PhotoPage : Page
    {

        private bool DeviceExist = false;
        private FilterInfoCollection videoDevices;
        private VideoCaptureDevice videoSource = null;
        Bitmap bitmap;

        public PhotoPage()
        {
          InitializeComponent();
          getCamList();
          startVideo();
        }

        // get the devices name
        private void getCamList()
        {
            try
            {
                videoDevices = new  FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (videoDevices.Count == 0)
                    throw new ApplicationException();

                DeviceExist = true;

            }
            catch (ApplicationException)
            {
                DeviceExist = false;
            }
        }


         //toggle start and stop button
         private void startVideo() // as it's say
         {
                if (DeviceExist)
                {
                    videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString); // the only one webcam
                    videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);
                    CloseVideoSource();
                  //  videoSource.DesiredFrameSize = new Size(160, 120); // deprecated ?
                    //videoSource.DesiredFrameRate = 10;
                    videoSource.Start();

                }
                else
                {
                    // error
                }
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
        System.Drawing.Image imgforms = (System.Drawing.Bitmap)eventArgs.Frame.Clone();
        bitmap = (System.Drawing.Bitmap)eventArgs.Frame.Clone(); // use to save to the hard drive

        BitmapImage bi = new BitmapImage();
        bi.BeginInit();

        MemoryStream ms = new MemoryStream();
        imgforms.Save(ms, ImageFormat.Bmp);
        ms.Seek(0, SeekOrigin.Begin);

        bi.StreamSource = ms;
        bi.EndInit();

        //Using the freeze function to avoid cross thread operations 
        bi.Freeze();

        //Calling the UI thread using the Dispatcher to update the 'Image' WPF control         
        Dispatcher.BeginInvoke(new ThreadStart(delegate
        {
            
            frameholder.Source = bi; /*frameholder is the name of the 'Image' WPF control*/
            
        }));     
    }

        //close the device safely
        private void CloseVideoSource()
        {
            if (!(videoSource == null))
                if (videoSource.IsRunning)
                {
                    videoSource.SignalToStop();
                    videoSource = null;
                }
        }

        //public PhotoPage()
        //{
        //    InitializeComponent();
        //}

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {

        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            CloseVideoSource();
            object page = MyGlobals.RouteToPreviousPage();
            this.NavigationService.Navigate(page);
        }

        private void Image_Loaded(object sender, RoutedEventArgs e)
        {
            // ... Create a new BitmapImage.
            //BitmapImage b = new BitmapImage();
            //b.BeginInit();
            //b.UriSource = new Uri("c:\\plus.png");
            //b.EndInit();

            //// ... Get Image reference from sender.
            //var image = sender as Image;
            //// ... Assign Source.
            //image.Source = b;
        }

        private void TakePicture()
        {
            // 
            ///////////////////////////////////////
            // save the image
            ///////////////////////////////////////
            //
            Bitmap original = (Bitmap)bitmap.Clone();
            //MessageBox.Show("Width=" + original.Width + " Height = " + original.Height);

            int Width = original.Width;
            int Height = original.Height;

            if ((original.Width > 640) && (original.Height > 360))
            {
                // if the image is too large, lets fix the size.  Example Surface Pro 6 are 5 MP camera
                Width = 840;
                Height = 480;

            }

            Bitmap current = new Bitmap(original, new System.Drawing.Size(Width, Height)); // see above fix size
            string filepath = Environment.CurrentDirectory;

            string imagesdir = MyGlobals.gResultsFolderPath + System.IO.Path.DirectorySeparatorChar + "images"; // save it in the results folder
            //string imagesdir = globalResultsFolderPath + System.IO.Path.DirectorySeparatorChar + "images";
            System.IO.Directory.CreateDirectory(imagesdir); // create dir if it does not exist
            //string clientID = Convert.ToString(MyGlobals.gTestSession.objClient.ClientID); // added the client id
            string f2_matt_version = MyGlobals.gTestSession.TestSessionID + "-" + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + ".jpg";
            string fileName = imagesdir + System.IO.Path.DirectorySeparatorChar + f2_matt_version;

            current.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            current.Dispose();
            current = null;


            // 
            /////////////////////////////////////
            // close the vidoe source
            /////////////////////////////////////
            //
            CloseVideoSource();
            object page = MyGlobals.RouteToNextPage();
            this.NavigationService.Navigate(page);

        }
        private void Image_TouchUp(object sender, TouchEventArgs e)
        {
            TakePicture();
        }

        private void frameholder_MouseUp(object sender, MouseButtonEventArgs e)
        {
            TakePicture();
        }
    }
}
