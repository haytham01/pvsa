﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for LinkSummaryWindow.xaml
    /// </summary>
    public partial class LinkSummaryWindow : Window
    {
        public bool bOk = false;
        public List<TestLink> lstTestLink = new List<TestLink>();

        public LinkSummaryWindow()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            bOk = true;
            this.Close();
        }
        private void loadItems()
        {
            AppController appController = MyGlobals.gAppController;
            if ((appController != null) && (appController.lstTest != null) && (appController.lstTest.Count > 0))
            {
                TestLink testLink = null;
                //this.testLinksComboBox.Items.Clear();
                lstTestLink.Clear();
                //testLink = new TestLink() { Code = "New", Description = "Create New Test Link" };
                //lstTestLink.Add(testLink);
                if ((appController.lstGlobalTestLink != null) && (appController.lstGlobalTestLink.Count > 0))
                {
                    for (int i = 0; i < appController.lstGlobalTestLink.Count; i++)
                    {
                        testLink = appController.lstGlobalTestLink[i];
                        lstTestLink.Add(testLink);
                    }
                }
                displayLink();
                loadListview();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadItems();
        }
 
        private void addLinkButton_Click(object sender, RoutedEventArgs e)
        {
            ListViewItem item = (ListViewItem) this.mainListView.SelectedItem;

            TestLink tstLink = new TestLink() { Code = item.Content.ToString(), Index = Convert.ToInt32(item.Tag) };
            lstTestLink.Add(tstLink);
            displayLink();
            loadListview();
        }

        private void deleteLinkButton_Click(object sender, RoutedEventArgs e)
        {
            if (lstTestLink.Count > 0)
            {
                lstTestLink.RemoveAt(lstTestLink.Count - 1);
            }
            displayLink();
            loadListview();

        }
        
        private void displayLink()
        {
            string txt = string.Empty;
            this.newLinkTextBox.Clear();
            if (lstTestLink.Count > 0)
            {
                txt = lstTestLink[0].Code;
                for (int i = 1; i < lstTestLink.Count; i++ )
                {
                    txt = txt + " --> " + lstTestLink[i].Code;
                }

            }
            this.newLinkTextBox.Text = txt;
        }

        private void loadListview()
        {
            AppController appController = MyGlobals.gAppController;
            Test test = null;
            this.mainListView.Items.Clear();
            ListViewItem item = null;
          
            for (int i = 0; i < appController.lstTest.Count; i++)
            {
                test = appController.lstTest[i];
                if ((test.objHostConfig.TestButtonText != null) && (test.objHostConfig.TestButtonText.Length > 0)
                    && (!existInLink(test.objHostConfig.TestButtonText)))
                {
                    if (test.objHostConfig.TestButtonText.Trim().Equals("BLANK", StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    item = new ListViewItem();
                    item.Content = test.objHostConfig.TestButtonText;
                    item.Tag = i;
                    this.mainListView.Items.Add(item);
                }
            }

        }

        private bool existInLink(string tstName)
        {
            bool bExist = false;
            if (lstTestLink.Count > 0)
            {
                for (int i = 0; i < lstTestLink.Count; i++)
                {
                    if (tstName == lstTestLink[i].Code)
                    {
                        bExist = true;
                        break;
                    }
                }

            }
            return bExist;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
