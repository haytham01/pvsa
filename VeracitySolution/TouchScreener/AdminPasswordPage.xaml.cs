﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for AdminPasswordPage.xaml
    /// </summary>
    public partial class AdminPasswordPage : Page
    {
        private string currentText = string.Empty;

        public enum NextPageMode
        {
            ADVANCED,
            MANAGER
        };

        public NextPageMode nextPageMode = NextPageMode.ADVANCED;

        public AdminPasswordPage()
        {
            InitializeComponent();
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            object page = MyGlobals.RouteToCurrentPage();
            this.NavigationService.Navigate(page);

        }

        private void backspaceButton_Click(object sender, RoutedEventArgs e)
        {
            if (currentText.Length > 0)
            {
                currentText = currentText.Remove(currentText.Length - 1);
            }
            if (this.inputTextBox.Text.Length > 0)
            {
                this.inputTextBox.Text = this.inputTextBox.Text.Remove(this.inputTextBox.Text.Length - 1);
            }
            
        }

        private void mainNumericKeyboard_UserKeyPressed(object sender, KeyboardNumeric1.KeyboardEventArgs e)
        {
            currentText += e.KeyboardKeyPressed;
            this.inputTextBox.AppendText("*");
            CheckText();
        }

        private void startOverButton_Click(object sender, RoutedEventArgs e)
        {
            object page = MyGlobals.RouteToInitialPage();
            this.NavigationService.Navigate(page);
        }

        private void CheckText()
        {
            object page = null;

            if (nextPageMode == NextPageMode.ADVANCED)
            {
                if (currentText == MyGlobals.ADVANCED_ADMIN_PASSWORD)
                {
                    page = new AdminOptionsPage();
                    this.NavigationService.Navigate(page);
                }
            }
            else
            {
                if (currentText == MyGlobals.QUICK_ADMIN_PASSWORD)
                {
                    page = new AdminQuickActionsPage();
                    this.NavigationService.Navigate(page);
                }

                if (currentText == MyGlobals.ADVANCED_ADMIN_PASSWORD)
                {
                    if (MyGlobals.gAppController.TestCurrentNum == 0)
                    {
                        MyGlobals.gAppController.TestCurrentNum = 1;
                        MyGlobals.gAppController.SerializeWorking();
                        page = MyGlobals.RouteToInitialPage();
                        this.NavigationService.Navigate(page);
                    }
                }
            }
        }
        private void inputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            object page = null;

            if (nextPageMode == NextPageMode.ADVANCED)
            {
                page = new AdminQuickActionsPage();
            }
            else
            {
                page = MyGlobals.RouteToCurrentPage();
            }
            
            this.NavigationService.Navigate(page);

        }

        private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

      
    }
}
