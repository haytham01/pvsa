﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for MainResultsWindow.xaml
    /// </summary>
    public partial class MainResultsWindow : Window
    {
        public bool bOk = false;
        public string finalRiskAnalysis18Q = null;
        public string finalRiskAnalysis18QRecommend = null;

        // new RISK values
        public string finalIssueA_RiskAnalysis18Q = null;
        public string finalIssueB_RiskAnalysis18Q = null;
        public string finalIssueC_RiskAnalysis18Q = null;
        public string finalQuestionsCount = null;
        public string finalDeviceName = null;
        public string finalTestTime = null;
        public string finalSeconds = null;
        public string finalDriverLicense = null;


        public MainResultsWindow()
        {
            InitializeComponent();
        }

        private void cancelResultsButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            bOk = true;
            this.Hide();
        }

        private void SetResults()
        {


            if (finalIssueA_RiskAnalysis18Q != null)
            {
                this.issueAText.Text = finalIssueA_RiskAnalysis18Q.ToString();
                if (finalIssueA_RiskAnalysis18Q.IndexOf("low", 0, StringComparison.CurrentCultureIgnoreCase) != -1 )
                {
                    this.IssueAEllipse.Fill = new SolidColorBrush(Colors.Green);
                }
                if (finalIssueA_RiskAnalysis18Q.IndexOf("low#", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueAEllipse.Fill = new SolidColorBrush(Colors.Blue);
                }
                if (finalIssueA_RiskAnalysis18Q.IndexOf("caution", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueAEllipse.Fill = new SolidColorBrush(Colors.Yellow);
                }
                if (finalIssueA_RiskAnalysis18Q.IndexOf("elevated", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueAEllipse.Fill = new SolidColorBrush(Colors.Orange);
                }
                if (finalIssueA_RiskAnalysis18Q.IndexOf("high", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueAEllipse.Fill = new SolidColorBrush(Colors.Red);
                }
            }

            if (finalIssueB_RiskAnalysis18Q != null)
            {
                this.issueBText.Text = finalIssueB_RiskAnalysis18Q.ToString();
                if (finalIssueB_RiskAnalysis18Q.IndexOf("low", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueBEllipse.Fill = new SolidColorBrush(Colors.Green);
                }
                if (finalIssueB_RiskAnalysis18Q.IndexOf("low#", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueBEllipse.Fill = new SolidColorBrush(Colors.Blue);
                }
                if (finalIssueB_RiskAnalysis18Q.IndexOf("caution", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueBEllipse.Fill = new SolidColorBrush(Colors.Yellow);
                }
                if (finalIssueB_RiskAnalysis18Q.IndexOf("elevated", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueBEllipse.Fill = new SolidColorBrush(Colors.Orange);
                }
                if (finalIssueB_RiskAnalysis18Q.IndexOf("high", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueBEllipse.Fill = new SolidColorBrush(Colors.Red);
                }
            }

            if (finalIssueC_RiskAnalysis18Q != null)
            {
                this.issueCText.Text = finalIssueC_RiskAnalysis18Q.ToString();
                if (finalIssueC_RiskAnalysis18Q.IndexOf("low", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueCEllipse.Fill = new SolidColorBrush(Colors.Green);
                }
                if (finalIssueC_RiskAnalysis18Q.IndexOf("low#", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueCEllipse.Fill = new SolidColorBrush(Colors.Blue);
                }
                if (finalIssueC_RiskAnalysis18Q.IndexOf("caution", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueCEllipse.Fill = new SolidColorBrush(Colors.Yellow);
                }
                if (finalIssueC_RiskAnalysis18Q.IndexOf("elevated", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueCEllipse.Fill = new SolidColorBrush(Colors.Orange);
                }
                if (finalIssueC_RiskAnalysis18Q.IndexOf("high", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
                {
                    this.IssueCEllipse.Fill = new SolidColorBrush(Colors.Red);
                }
            }

            this.QuestionsText.Text = finalQuestionsCount.ToString();
            this.DeviceName.Text = finalDeviceName.ToString();
            this.TestTime.Text = finalTestTime.ToString();
            this.SecondsText.Text = finalSeconds.ToString();
            this.DriverLicenseText.Text = finalDriverLicense.ToString();


            //if (finalRiskAnalysis18Q != null)
            //{
            //    switch (finalRiskAnalysis18Q)
            //    {
            //        case "LOW":
            //            this.lowEllipse.Fill = new SolidColorBrush(Colors.Green);
            //            break;
            //        case "CAUTION":
            //            this.cautionEllipse.Fill = new SolidColorBrush(Colors.Yellow);
            //            break;
            //        case "HIGH":
            //            this.highEllipse.Fill = new SolidColorBrush(Colors.Red);
            //            break;
            //        default:
            //            break;
            //    }
            //}
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            SetResults();
        }
    }
}
