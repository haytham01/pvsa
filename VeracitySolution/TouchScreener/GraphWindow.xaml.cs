﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for GraphWindow.xaml
    /// </summary>
    public partial class GraphWindow : Window
    {

        public static List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();

        public static List<KeyValuePair<string, int>> valueList2 = new List<KeyValuePair<string, int>>();

        public static List<KeyValuePair<string, int>> valueList3 = new List<KeyValuePair<string, int>>();

        public static List<KeyValuePair<string, int>> valueList4 = new List<KeyValuePair<string, int>>();

        public GraphWindow()
        {
            InitializeComponent();
            showColumnChart();
        }

        private void showColumnChart()
        {

            // This is used for the multiple series
            var dataSourceList = new List<List<KeyValuePair<string, int>>>();

            //Setting data for column chart
            dataSourceList.Add(valueList);
            dataSourceList.Add(valueList2);
            dataSourceList.Add(valueList3);

            columnChart.DataContext = valueList4;


            //Setting data for line chart
            lineChart.DataContext = dataSourceList;


        }
    }
}
