﻿using System;
using System.Windows;
using System.Windows.Controls;
using Veracity.Client.Proxy;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Collections.Generic;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for ChooseTestPage.xaml
    /// </summary>
    public partial class ChooseTestPage : Page
    {
        private int pageIndex = 0;
        private int numTestPerPage = 24;

        public List<TestLink> lstTestLink = new List<TestLink>();

        private System.Windows.Threading.DispatcherTimer resetTimer = new System.Windows.Threading.DispatcherTimer();

        public ChooseTestPage()
        {
            InitializeComponent();
            SetChoiceButtonText();
            initializeResetTimer();            
        }
        private void TurnOffChoiceButtons()
        {
            this.test1Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test2Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test3Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test4Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test5Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test6Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test7Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test8Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test9Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test10Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test11Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test12Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test13Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test14Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test15Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test16Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test17Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test18Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test19Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test20Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test21Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test22Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test23Button.Visibility = System.Windows.Visibility.Collapsed;
            this.test24Button.Visibility = System.Windows.Visibility.Collapsed;
        }
        private void CheckME(Button btn, string buttontxt, int index, TextBlock txtblock)
        {
            CultureInfo currentCulture = Thread.CurrentThread.CurrentUICulture;
            if (MyGlobals.gAppController.IsMissionEssentials)
            {
                btn.Visibility = Visibility.Collapsed;
                if (currentCulture.Name == "ar-AE")
                {
                    if (buttontxt.IndexOf("Arabic") != -1)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Add(new TestLink() { Code = buttontxt, Index = index });
                        MyGlobals.gAppController.SerializeWorking();
                        if (MyGlobals.gAppController.lstGlobalTestLink.Count == 1)
                        {
                            btn.Visibility = Visibility.Visible;
                            txtblock.Text = "Arabic-ME";
                        }
                    }
                }
                if (currentCulture.Name == "en")
                {
                    if (buttontxt.IndexOf("English") != -1)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Add(new TestLink() { Code = buttontxt, Index = index });
                        MyGlobals.gAppController.SerializeWorking();
                        if (MyGlobals.gAppController.lstGlobalTestLink.Count == 1)
                        {
                            btn.Visibility = Visibility.Visible;
                            txtblock.Text = "English-ME";
                        }
                    }
                }
                if (currentCulture.Name == "es-MX")
                {
                    if (buttontxt.IndexOf("Spanish") != -1)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Add(new TestLink() { Code = buttontxt, Index = index });
                        MyGlobals.gAppController.SerializeWorking();
                        if (MyGlobals.gAppController.lstGlobalTestLink.Count == 1)
                        {
                            btn.Visibility = Visibility.Visible;
                            txtblock.Text = "Spanish-ME";
                        }
                    }
                }
                if (currentCulture.Name == "prs-AF")
                {
                    if (buttontxt.IndexOf("Dari") != -1)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Add(new TestLink() { Code = buttontxt, Index = index });
                        MyGlobals.gAppController.SerializeWorking();
                        if (MyGlobals.gAppController.lstGlobalTestLink.Count == 1)
                        {
                            btn.Visibility = Visibility.Visible;
                            txtblock.Text = "Dari-ME";
                        }
                    }
                }
                if (currentCulture.Name == "ps-AF")
                {
                    if (buttontxt.IndexOf("Pashto") != -1)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Add(new TestLink() { Code = buttontxt, Index = index });
                        MyGlobals.gAppController.SerializeWorking();
                        if (MyGlobals.gAppController.lstGlobalTestLink.Count == 1)
                        {
                            btn.Visibility = Visibility.Visible;
                            txtblock.Text = "Pashto-ME";
                        }
                    }
                }
            }
        }

        public string T(string s)
        {
            string ret = null;
            try
            {
                ret = MyGlobals.translateResourceManager.GetString(s);
            }
            catch (Exception ex)
            { }
            return ret;
        }

        private void SetChoiceButtonText()
        {
            try
            {
                if (MyGlobals.gAppController.IsMissionEssentials)
                {
                    if (MyGlobals.gAppController.lstGlobalTestLink != null)
                    {
                        MyGlobals.gAppController.lstGlobalTestLink.Clear();
                    }
                }
                TurnOffChoiceButtons();
                AppController appController = null;
                appController = new Veracity.Client.Proxy.AppController();
                string exepath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string controllerFilePath = System.IO.Path.GetDirectoryName(exepath) +
                    System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
                string controllerXml = File.ReadAllText(controllerFilePath);
                appController.Parse(controllerXml);
                if ((appController != null) && (appController.lstTest != null) && (appController.lstTest.Count > 0))
                {
                    if (appController.lstTest.Count <= numTestPerPage)
                    {
                        this.additionalTestButton.Visibility = System.Windows.Visibility.Hidden;
                    }
                    else
                    {
                        this.additionalTestButton.Visibility = System.Windows.Visibility.Visible;
                    }
                    Test test = null;
                    string buttonText = null;
                    bool maxtestlimitreached = false;
                    if (appController.TestLimitNum != -1)
                    {
                        if (appController.TestCurrentNum > 0)
                        {
                            string testleftof = (!string.IsNullOrEmpty(T("TestsLeftOf"))) ? T("TestsLeftOf") : "tests left of";
                            this.numTestLabel.Content = appController.TestCurrentNum + " " + testleftof + " " + appController.TestLimitNum;
                            this.numTestLabel.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            string ReachedMaximumTestOf = (!string.IsNullOrEmpty(T("ReachedMaximumTestOf"))) ? T("ReachedMaximumTestOf") : "Reached maximum test limit of";
                            this.numTestLabel.Content = ReachedMaximumTestOf + " " + appController.TestLimitNum;
                            maxtestlimitreached = true;
                        }
                    }
                    for (int i = 0; i < numTestPerPage; i++)
                    {
                        if ((i + (pageIndex * numTestPerPage)) >= appController.lstTest.Count)
                        {
                            break;
                        }
                        if (maxtestlimitreached)
                        {
                            break;
                        }
                        test = appController.lstTest[i + (pageIndex * numTestPerPage)];
                        buttonText = "Name";
                        if ((test.objHostConfig.TestButtonText != null) && (test.objHostConfig.TestButtonText.Length > 0))
                        {
                            buttonText = test.objHostConfig.TestButtonText;
                        }

                        switch (i)
                        {
                            case 0:
                                this.test1ButtonTextBlock.Text = buttonText;
                                this.test1Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test1Button, buttonText, i, this.test1ButtonTextBlock);
                                break;
                            case 1:
                                this.test2ButtonTextBlock.Text = buttonText;
                                this.test2Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test2Button, buttonText, i, this.test2ButtonTextBlock);
                                break;
                            case 2:
                                this.test3ButtonTextBlock.Text = buttonText;
                                this.test3Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test3Button, buttonText, i, this.test3ButtonTextBlock);
                                break;
                            case 3:
                                this.test4ButtonTextBlock.Text = buttonText;
                                this.test4Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test4Button, buttonText, i, this.test4ButtonTextBlock);
                                break;
                            case 4:
                                this.test5ButtonTextBlock.Text = buttonText;
                                this.test5Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test5Button, buttonText, i, this.test5ButtonTextBlock);
                                break;
                            case 5:
                                this.test6ButtonTextBlock.Text = buttonText;
                                this.test6Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test6Button, buttonText, i, this.test6ButtonTextBlock);
                                break;
                            case 6:
                                this.test7ButtonTextBlock.Text = buttonText;
                                this.test7Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test7Button, buttonText, i, this.test7ButtonTextBlock);
                                break;
                            case 7:
                                this.test8ButtonTextBlock.Text = buttonText;
                                this.test8Button.Visibility = System.Windows.Visibility.Visible;
                                CheckME(this.test8Button, buttonText, i, this.test8ButtonTextBlock);
                                break;
                            case 8:
                                this.test9ButtonTextBlock.Text = buttonText;
                                this.test9Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 9:
                                this.test10ButtonTextBlock.Text = buttonText;
                                this.test10Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 10:
                                this.test11ButtonTextBlock.Text = buttonText;
                                this.test11Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 11:
                                this.test12ButtonTextBlock.Text = buttonText;
                                this.test12Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 12:
                                this.test13ButtonTextBlock.Text = buttonText;
                                this.test13Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 13:
                                this.test14ButtonTextBlock.Text = buttonText;
                                this.test14Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 14:
                                this.test15ButtonTextBlock.Text = buttonText;
                                this.test15Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 15:
                                this.test16ButtonTextBlock.Text = buttonText;
                                this.test16Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 16:
                                this.test17ButtonTextBlock.Text = buttonText;
                                this.test17Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 17:
                                this.test18ButtonTextBlock.Text = buttonText;
                                this.test18Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 18:
                                this.test19ButtonTextBlock.Text = buttonText;
                                this.test19Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 19:
                                this.test20ButtonTextBlock.Text = buttonText;
                                this.test20Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 20:
                                this.test21ButtonTextBlock.Text = buttonText;
                                this.test21Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 21:
                                this.test22ButtonTextBlock.Text = buttonText;
                                this.test22Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 22:
                                this.test23ButtonTextBlock.Text = buttonText;
                                this.test23Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            case 23:
                                this.test24ButtonTextBlock.Text = buttonText;
                                this.test24Button.Visibility = System.Windows.Visibility.Visible;
                                break;
                            default:
                                break;
                        }

                    }


                }
            }
            catch { }

        }
        private void resetTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                resetTimer.Stop();
                this.NavigationService.Navigate(new WelcomePage());
            }
            catch { }
        }
        private void initializeResetTimer()
        {
            resetTimer.Interval = new TimeSpan(0, 0, 0, MyGlobals.gResetTimerInSeconds, 0);
            resetTimer.Tick += new EventHandler(resetTimer_Tick);
            resetTimer.Start();
        }


        //private void initialAppButton_Click(object sender, RoutedEventArgs e)
        //{
        //    MyGlobals.SetTestChoiceGlobals(1);
        //    MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.CHOICE_SCREEN;
        //    object page = MyGlobals.RouteToNextPage();
        //    this.NavigationService.Navigate(page);
        //}

        //private void renewalAppButton_Click(object sender, RoutedEventArgs e)
        //{
        //    MyGlobals.SetTestChoiceGlobals(0);
        //    MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.CHOICE_SCREEN;
        //    object page = MyGlobals.RouteToNextPage();
        //    this.NavigationService.Navigate(page);
        //}

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new WelcomePage());
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            resetTimer.Stop();
        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            AdminPasswordPage page = new AdminPasswordPage();
            page.nextPageMode = AdminPasswordPage.NextPageMode.MANAGER;
            this.NavigationService.Navigate(page);
        }

        private void setTestChoice(int pIntTestInd)
        {
            MyGlobals.SetTestChoiceGlobals(pIntTestInd);
            MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.CHOICE_SCREEN;
            object page = MyGlobals.RouteToNextPage();
            this.NavigationService.Navigate(page);
        }
        private void test1Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(0 + (pageIndex * numTestPerPage));
        }

        private void test2Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(1 + (pageIndex * numTestPerPage));
        }

        private void test3Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(2 + (pageIndex * numTestPerPage));

        }

        private void test4Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(3 + (pageIndex * numTestPerPage));

        }

        private void test5Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(4 + (pageIndex * numTestPerPage));

        }

        private void test6Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(5 + (pageIndex * numTestPerPage));

        }

        private void test7Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(6 + (pageIndex * numTestPerPage));

        }

        private void test8Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(7 + (pageIndex * numTestPerPage));

        }

        private void test9Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(8 + (pageIndex * numTestPerPage));

        }

        private void test10Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(9 + (pageIndex * numTestPerPage));

        }

        private void test11Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(10 + (pageIndex * numTestPerPage));

        }

        private void test12Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(11 + (pageIndex * numTestPerPage));

        }

        private void test13Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(12 + (pageIndex * numTestPerPage));

        }

        private void test14Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(13 + (pageIndex * numTestPerPage));

        }

        private void test15Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(14 + (pageIndex * numTestPerPage));

        }

        private void test16Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(15 + (pageIndex * numTestPerPage));

        }

        private void test17Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(16 + (pageIndex * numTestPerPage));

        }

        private void test18Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(17 + (pageIndex * numTestPerPage));

        }

        private void test19Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(18 + (pageIndex * numTestPerPage));

        }

        private void test20Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(19 + (pageIndex * numTestPerPage));

        }

        private void test21Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(20 + (pageIndex * numTestPerPage));

        }

        private void test22Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(21 + (pageIndex * numTestPerPage));

        }

        private void test23Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(22 + (pageIndex * numTestPerPage));

        }

        private void test24Button_Click(object sender, RoutedEventArgs e)
        {
            setTestChoice(23 + (pageIndex * numTestPerPage));

        }

        private void additionalTestButton_Click(object sender, RoutedEventArgs e)
        {
            if (pageIndex == 0)
            {
                pageIndex = 1;
                this.additionalButtonTextBlock.Text = "Previous Tests";
            }
            else
            {
                pageIndex = 0;
                this.additionalButtonTextBlock.Text = "Additional Tests";
            }
            SetChoiceButtonText();
        }

    }
}
