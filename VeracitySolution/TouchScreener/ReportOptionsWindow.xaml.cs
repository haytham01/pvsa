﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for ReportOptionsWindow.xaml
    /// </summary>
    public partial class ReportOptionsWindow : Window
    {
        public string Page1ReportTitle = null;
        public string Page2ReportTitle = null;
        public string Page1SubTitle = null;
        public string IssueATitle = null;
        public string IssueBTitle = null;
        public string IssueCTitle = null;
        public string IssueDTitle = null;
        public int Page1LeftMargin = 0;
        public int Page1SubtitleLeftMargin = 0;
        public int Page2LeftMargin = 0;
        public string EmailAddress = null;

        // used for the Configuration questions
        public string Q1ShortText = null;
        public string Q2ShortText = null;
        public string Q3ShortText = null;
        public string Q4ShortText = null;
        public string Q5ShortText = null;
        public string Q6ShortText = null;
        public string Q7ShortText = null;
        public string Q8ShortText = null;
        public string Q9ShortText = null;
        public string Q10ShortText = null;
        public string Q11ShortText = null;
        public string Q12ShortText = null;
        public string Q13ShortText = null;
        public string Q14ShortText = null;
        public string Q15ShortText = null;
        public string Q16ShortText = null;
        public string Q17ShortText = null;
        public string Q18ShortText = null;

        public bool bOk = false;

        public ReportQuestionsWindow reportQuestionsWindow = new ReportQuestionsWindow();

        public ReportOptionsWindow()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {

            saveInfo();
            if (bOk)
            {
                this.Close();
            }
        }

        public void loadInfo()
        {
            this.page1TitleTextBox.Text = Page1ReportTitle;
            this.page2TitleTextBox.Text = Page2ReportTitle;
            this.page1SubTitleTextBox.Text = Page1SubTitle;
            this.issueATextBox.Text = IssueATitle;
            this.issueBTextBox.Text = IssueBTitle;
            this.issueCTextBox.Text = IssueCTitle;
            this.issueDTextBox.Text = IssueDTitle;
            this.issueDTextBox.Text = IssueDTitle;
            this.emailAddressTextBox.Text = EmailAddress;
            this.page1LeftMarginTextBox.Text = Convert.ToString(Page1LeftMargin);
            this.page1SubtitleLeftMarginTextBox.Text = Convert.ToString(Page1SubtitleLeftMargin);
            this.page2LeftMarginTextBox.Text = Convert.ToString(Page2LeftMargin);

            this.errorTextBlock.Visibility = System.Windows.Visibility.Collapsed;

            Q1ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[0].ScreenTextShort;
            Q2ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[1].ScreenTextShort;
            Q3ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[2].ScreenTextShort;
            Q4ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[3].ScreenTextShort;
            Q5ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[4].ScreenTextShort;
            Q6ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[5].ScreenTextShort;
            Q7ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[6].ScreenTextShort;
            Q8ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[7].ScreenTextShort;
            Q9ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[8].ScreenTextShort;
            Q10ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[9].ScreenTextShort;
            Q11ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[10].ScreenTextShort;
            Q12ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[11].ScreenTextShort;
            Q13ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[12].ScreenTextShort;
            Q14ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[13].ScreenTextShort;
            Q15ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[14].ScreenTextShort;
            Q16ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[15].ScreenTextShort;
            Q17ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[16].ScreenTextShort;
            Q18ShortText = MyGlobals.gCurrentTest.lstScreenQuestionsOnly[17].ScreenTextShort;
        }

        private void saveInfo()
        {
            try
            {
                Page1ReportTitle = this.page1TitleTextBox.Text.Trim();
                Page2ReportTitle = this.page2TitleTextBox.Text.Trim();
                Page1SubTitle = this.page1SubTitleTextBox.Text.Trim();
                IssueATitle = this.issueATextBox.Text.Trim();
                IssueBTitle = this.issueBTextBox.Text.Trim();
                IssueCTitle = this.issueCTextBox.Text.Trim();
                IssueDTitle = this.issueDTextBox.Text.Trim();
                EmailAddress = this.emailAddressTextBox.Text.Trim();
                Page1LeftMargin = Convert.ToInt32(this.page1LeftMarginTextBox.Text.Trim());
                Page1SubtitleLeftMargin = Convert.ToInt32(this.page1SubtitleLeftMarginTextBox.Text.Trim());
                Page2LeftMargin = Convert.ToInt32(this.page2LeftMarginTextBox.Text.Trim());
                bOk = true;

            }
            catch (Exception ex)
            {
                this.errorTextBlock.Visibility = System.Windows.Visibility.Visible;
                bOk = false;
            }

        }

        private void testEmailButton_Click(object sender, RoutedEventArgs e)
        {
            Config config = new Config();
            bool bSuccess = false;
            string errMsg = null;
            string toAddress = this.emailAddressTextBox.Text.Trim();
            if ((!config.HasError) && (toAddress.Length > 0))
            {
                Emailer emailer = new Emailer();
                bSuccess = MyGlobals.SendEmail("PTSSA test email", toAddress, "PTSSA TouchScreener Test Email", null, null, null, null, null, null);
                if (!bSuccess)
                {
                    errMsg = "There was an error in sending test email.";
                }
                else
                {
                    errMsg = "Successful in sending test email to " + toAddress;
                }
            }
            else
            {
                errMsg = "Invalid email address or error in application email configuration.";
            }

            this.emailTestStatusTextBlock.Text = errMsg;

        }

        private void configureReportQuestionsButton_Click(object sender, RoutedEventArgs e)
        {

            reportQuestionsWindow.ShowDialog();
            if (reportQuestionsWindow.bOK == true)
            { 
               // save data
               Q1ShortText = reportQuestionsWindow.q1TextBox.Text;
               Q2ShortText = reportQuestionsWindow.q2TextBox.Text;
               Q3ShortText = reportQuestionsWindow.q3TextBox.Text;
               Q4ShortText = reportQuestionsWindow.q4TextBox.Text;
               Q5ShortText = reportQuestionsWindow.q5TextBox.Text;
               Q6ShortText = reportQuestionsWindow.q6TextBox.Text;
               Q7ShortText = reportQuestionsWindow.q7TextBox.Text;
               Q8ShortText = reportQuestionsWindow.q8TextBox.Text;
               Q9ShortText = reportQuestionsWindow.q9TextBox.Text;
               Q10ShortText = reportQuestionsWindow.q10TextBox.Text;
               Q11ShortText = reportQuestionsWindow.q11TextBox.Text;
               Q12ShortText = reportQuestionsWindow.q12TextBox.Text;
               Q13ShortText = reportQuestionsWindow.q13TextBox.Text;
               Q14ShortText = reportQuestionsWindow.q14TextBox.Text;
               Q15ShortText = reportQuestionsWindow.q15TextBox.Text;
               Q16ShortText = reportQuestionsWindow.q16TextBox.Text;
               Q17ShortText = reportQuestionsWindow.q17TextBox.Text;
               Q18ShortText = reportQuestionsWindow.q18TextBox.Text;
               //MessageBox.Show("New 1 question is:  " + reportQuestionsWindow.q1TextBox.Text);
               //MessageBox.Show("New 2 question is:  " + reportQuestionsWindow.q2TextBox.Text);
               //MessageBox.Show("New 3 question is:  " + reportQuestionsWindow.q3TextBox.Text);
               //MessageBox.Show("New 4 question is:  " + reportQuestionsWindow.q4TextBox.Text);
               //MessageBox.Show("New 5 question is:  " + reportQuestionsWindow.q5TextBox.Text);
               //MessageBox.Show("New 6 question is:  " + reportQuestionsWindow.q6TextBox.Text);
               //MessageBox.Show("New 7 question is:  " + reportQuestionsWindow.q7TextBox.Text);
               //MessageBox.Show("New 8 question is:  " + reportQuestionsWindow.q8TextBox.Text);
               //MessageBox.Show("New 9 question is:  " + reportQuestionsWindow.q9TextBox.Text);
               //MessageBox.Show("New 10 question is:  " + reportQuestionsWindow.q10TextBox.Text);
               //MessageBox.Show("New 11 question is:  " + reportQuestionsWindow.q11TextBox.Text);
               //MessageBox.Show("New 12 question is:  " + reportQuestionsWindow.q12TextBox.Text);
               //MessageBox.Show("New 13 question is:  " + reportQuestionsWindow.q13TextBox.Text);
               //MessageBox.Show("New 14 question is:  " + reportQuestionsWindow.q14TextBox.Text);
               //MessageBox.Show("New 15 question is:  " + reportQuestionsWindow.q15TextBox.Text);
               //MessageBox.Show("New 16 question is:  " + reportQuestionsWindow.q16TextBox.Text);
               //MessageBox.Show("New 17 question is:  " + reportQuestionsWindow.q17TextBox.Text);
               //MessageBox.Show("New 18 question is:  " + reportQuestionsWindow.q18TextBox.Text);
            }
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
