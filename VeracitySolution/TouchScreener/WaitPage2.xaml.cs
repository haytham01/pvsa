﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for WaitPage2.xaml
    /// </summary>
    public partial class WaitPage2 : Page
    {
        private System.Windows.Threading.DispatcherTimer dispatcherTimer = null;

        private static int imgCnt = 1;

        public WaitPage2()
        {
            InitializeComponent();

            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);

        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            dispatcherTimer.Tick -= dispatcherTimer_Tick;
        }
     
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            // code goes here
            if (imgCnt > 8)
            {
                imgCnt = 1;
                this.img1.Visibility = System.Windows.Visibility.Hidden;
                this.img2.Visibility = System.Windows.Visibility.Hidden;
                this.img3.Visibility = System.Windows.Visibility.Hidden;
                this.img4.Visibility = System.Windows.Visibility.Hidden;
                this.img5.Visibility = System.Windows.Visibility.Hidden;
                this.img6.Visibility = System.Windows.Visibility.Hidden;
                this.img7.Visibility = System.Windows.Visibility.Hidden;
                this.img8.Visibility = System.Windows.Visibility.Hidden;
                dispatcherTimer.Stop();
                this.NavigationService.Navigate(new ScreenPage());
            }
            else
            {
                switch (imgCnt)
                {
                    case 1:
                        this.img1.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 2:
                        this.img2.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 3:
                        this.img3.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 4:
                        this.img4.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 5:
                        this.img5.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 6:
                        this.img6.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 7:
                        this.img7.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 8:
                        this.img8.Visibility = System.Windows.Visibility.Visible;
                        break;
                    default:
                        break;
                }
                imgCnt++;
            }
        }

        public void StartTimer()
        {
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.TransitionDelayInMs/8);
            dispatcherTimer.Start();
        }


       
    }
}
