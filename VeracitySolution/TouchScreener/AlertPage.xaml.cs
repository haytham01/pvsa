﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for AlertPage.xaml
    /// </summary>
    public partial class AlertPage : Page
    {
        public AlertPage()
        {
            InitializeComponent();
        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            AdminPasswordPage page = new AdminPasswordPage();
            page.nextPageMode = AdminPasswordPage.NextPageMode.MANAGER;
            this.NavigationService.Navigate(page);

        }
    }
}
