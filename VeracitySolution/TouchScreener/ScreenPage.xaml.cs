﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Media;

using Veracity.Client.Proxy;
using System.IO;
using System.Diagnostics;
using System.Printing;
using System.Reflection;

using WintabDN;


namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for ScreenPage.xaml
    /// </summary>
    public partial class ScreenPage : Page
    {
        private Screen _currentWorkingScreen = null;
        private SoundPlayer _soundPlayer = null;
        private System.Windows.Threading.DispatcherTimer endDispatcherTimer = null;
        private System.Windows.Threading.DispatcherTimer audioEndDispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        private System.Windows.Threading.DispatcherTimer resetTimer = new System.Windows.Threading.DispatcherTimer();

        private static DateTime dtNull = new DateTime();

        private Stopwatch _readTimeStopWatch = new Stopwatch();
        private Stopwatch _ansTimeStopWatch = new Stopwatch();

        // PRESSURE SENSING
        private bool _rerouting;
        private static Dictionary<int, IInputElement> capturemap;
        private CWintabContext m_logContext = null;
        private CWintabData m_wtData = null;
        UInt32 m_maxPackets = 10;
        const bool REMOVE = true;
        private string ans = null;

        //private StringBuilder sbMetrics = null;
        //private List<Sensor> lstSensor = new List<Sensor>();

        private string finalRiskAnalysis18Q = null;
        private string finalRiskAnalysis18QRecommend = null;

        // new RISK screen values

        private string finalIssueA_RiskAnalysis18Q = null;
        private string finalIssueB_RiskAnalysis18Q = null;
        private string finalIssueC_RiskAnalysis18Q = null;

        private string finalQuestionsCount = null;
        private string finalDeviceName = null;
        private string finalTestTime = null;
        private string finalSeconds = null;
        private string finalDriverLicense = null;
        

        private void resetTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                resetTimer.Stop();
                MyGlobals.gTestSession.dtEndSession = DateTime.Now;

                object page = null;
                if ((MyGlobals.gAppController.lstGlobalTestLink != null) && (MyGlobals.gAppController.lstGlobalTestLink.Count > 0))
                {
                    int ind = MyGlobals.gAppController.lstGlobalTestLink[0].Index;
                    MyGlobals.SetTestChoiceGlobals(ind);
                    MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.WELCOME;
                    page = MyGlobals.RouteToCurrentPage();
                }
                else
                {
                    page = new WelcomePage();
                }

                this.NavigationService.Navigate(page);

                //this.NavigationService.Navigate(new WelcomePage());
            }
            catch { }
        }
        private void initializeResetTimer()
        {
            resetTimer.Interval = new TimeSpan(0, 0, 0, MyGlobals.gResetTimerInSeconds, 0);
            resetTimer.Tick += new EventHandler(resetTimer_Tick);
            resetTimer.Start();
        }

        public ScreenPage()
        {
            InitializeComponent();
            GC.Collect();
            GC.WaitForPendingFinalizers();

            endDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            endDispatcherTimer.Tick += new EventHandler(endDispatcherTimer_Tick);
            loadCurrentScreenImage();
            initializeResetTimer();

            if ((MyGlobals.gCurrentTest.objHostConfig.TurnOnPressureSensor) && (!MyGlobals.isSensorOn))
            {
                try
                {
                    // Open a Wintab context that does not send Wintab data events.
                    m_logContext = OpenQueryDigitizerContext();

                    // Create a data object.
                    m_wtData = new CWintabData(m_logContext);

                    m_wtData.SetWTPacketEventHandler(MyWTPacketEventHandler);

                    MyGlobals.isSensorOn = true;
                    //TraceMsg("Press \"Test\" and touch pen to tablet.\n");
                }
                catch (Exception ex)
                {
                    //System.Windows.MessageBox.Show(ex.ToString());
                }
            }
        }



        private void loadCurrentScreenImage()
        {
            try
            {
                //this.logoimage.Visibility = System.Windows.Visibility.Visible;
                if (MyGlobals.bDoForwardTestLink)
                {
                    MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber = MyGlobals.gLinkIdentification;
                    MyGlobals.gCurrentLnkTestTemplateScreenListIndex++;
                    MyGlobals.bDoForwardTestLink = false;
                    this.noButton.Visibility = System.Windows.Visibility.Hidden;
                    //System.Threading.Thread.Sleep(15000);
                }
                else
                {
                    this.noButton.Visibility = System.Windows.Visibility.Visible;
                }
                _currentWorkingScreen = MyGlobals.gCurrentTest.lstScreen[MyGlobals.gCurrentLnkTestTemplateScreenListIndex];
                MyGlobals.gTestSession.currentActiveScreen = _currentWorkingScreen;
                MyGlobals.gTestSession.SessionSerializationIndex++;
                // serialize to partial xml
                //MyGlobals.gTestSession.SerializeToPartialXml(MyGlobals.gResultsFolderPath);
                if ((dtNull.Equals(MyGlobals.gTestSession.dtBeginQuestionTest)) && (_currentWorkingScreen.ScreenTypeID == 3))
                {
                    MyGlobals.gTestSession.dtBeginQuestionTest = DateTime.Now;
                }
                if (!MyGlobals.gCurrentTest.objHostConfig.IsAudioOnly)
                {
                    _readTimeStopWatch.Start();
                }
                _currentWorkingScreen.EnterScreenDate = DateTime.Now;

                MyGlobals.gCurrentTest.setCurrentLnkTestTemplateScreen(MyGlobals.gCurrentLnkTestTemplateScreenListIndex);

                string imageFilePathRelative = getImagePath();
                //MessageBox.Show(imageFilePathRelative);
                //Uri imageUri = new Uri(imageFilePathRelative, UriKind.Relative);
                Uri imageUri = new Uri(AppDomain.CurrentDomain.BaseDirectory + imageFilePathRelative, UriKind.Absolute);
                this.mainImage.Source = new BitmapImage(imageUri);
           


                if (_currentWorkingScreen.ScreenTypeID == 2)
                {
                    this.BottomBorder.Visibility = System.Windows.Visibility.Hidden;
                    MyGlobals.gTestSession.dtEndQuestionTest = DateTime.Now;
                    //this.logoimage.Visibility = System.Windows.Visibility.Hidden;
                    StartEndTimer();
                }
            }
            catch { }
        }
        private void FlipData(ref TestSession pTestSession)
        {
            Screen screen = null;
            for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                // flip data
                double tmp = screen.ReadTimeInSeconds;
                screen.ReadTimeInSeconds = screen.DeltaClickTimeInSeconds;
                screen.DeltaClickTimeInSeconds = tmp;
            }

            //foreach (Screen screen in pTestSession.lstScreenAll)
            //{
            //    // flip data
            //    double tmp = screen.ReadTimeInSeconds;
            //    screen.ReadTimeInSeconds = _currentWorkingScreen.DeltaClickTimeInSeconds;
            //    screen.DeltaClickTimeInSeconds = tmp;
            //}
        }

        private void endDispatcherTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                endDispatcherTimer.Stop();
                resetTimer.Stop();
                bool doIncludeIdentityInOutputCSVFile = false;
                string tmp = MyGlobals.gCurrentTest.objHostConfig.getResourceValue("INCLUDE_IDENTITY_IN_OUTPUT_CSV_FILE");
                if (tmp != null)
                {
                    Boolean.TryParse(tmp, out doIncludeIdentityInOutputCSVFile);
                }


                //string csvData = MyGlobals.gTestSession.ToCsv();
                //MyGlobals.gTestSession.csvDataFlipped = csvData;

                FlipData(ref MyGlobals.gTestSession);

                // reload test session
                //MyGlobals.gTestSession.lstScreenQuestionOnly.Clear();
                //MyGlobals.Parse24QCSV(ref MyGlobals.gTestSession);
                //MyGlobals.gTestSession.objClient = MyGlobals.gAppController.lstTest[0].objClient;
                AnalyzerKOTQuestions analyzerKOT = null;
                Anaylyzer24Questions analyzer24 = null;
                Anaylyzer26Questions analyzer26 = null;
                Anaylyzer18Questions analyzer18 = null;
                Analyzer15Questions analyzer15 = null;
                int tslValue = MyGlobals.gCurrentTest.objHostConfig.TslValue;
                switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
                {
                    case 26:
                        analyzer26 = new Anaylyzer26Questions();
                        MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_1;

                        analyzer26.bRunTheTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;
                        if (MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier)
                        {
                            analyzer26.dTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue;
                        }

                        if (MyGlobals.gCurrentTest.objHostConfig.AutoAdjust)
                        {
                            analyzer26.eTotalNumReaction = MyGlobals.gCurrentTest.objHostConfig.HitsValue;
                            if (tslValue > 0)
                            {
                                analyzer26.dTSLAdjust26Q = tslValue;
                                analyzer26.dTSLLow = MyGlobals.gCurrentTest.objHostConfig.TslValueLow;
                            }
                            analyzer26.AnalysisTest26AutoAdjust(ref MyGlobals.gTestSession);
                        }
                        else
                        {
                            if (tslValue > 0)
                            {
                                analyzer26.dTSLAdjust26Q = tslValue;
                            }
                            analyzer26.AnalysisTest26Questions(ref MyGlobals.gTestSession);

                        }

                        //MyGlobals.gTestSession.EndTslValue = analyzer26.TSLPercentage;
                        MyGlobals.gTestSession.BeginTslValue = MyGlobals.gCurrentTest.objHostConfig.TslValue;
                        MyGlobals.gTestSession.EndTslValue = analyzer26.dTSLAdjust26Q;

                        break;
                    case 18:
                        analyzer18 = new Anaylyzer18Questions();
                        MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
                        analyzer18.bTurnOffExtraSPRWeightCalculation = MyGlobals.gCurrentTest.objHostConfig.TurnOffExtraSPRWeight;

                        analyzer18.bTurnOnSPR1 = (!MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR1);
                        analyzer18.bTurnOnSPR2 = (!MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR2);
                        analyzer18.bTurnOnSPR3 = (!MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR3);
                        analyzer18.bTurnOnRiskAnalysis = (MyGlobals.gCurrentTest.objHostConfig.TurnOffRiskAnalysis);
                        analyzer18.bTurnOnSPR71115 = (MyGlobals.gCurrentTest.objHostConfig.TurnOffSPR71115);
                        analyzer18.bTurnOnMovingTestAverage = (MyGlobals.gCurrentTest.objHostConfig.TurnOffMovingTestAverage);

                        analyzer18.bRunTheTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;
                        if (MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier)
                        {
                            analyzer18.dTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue;
                        }

                        if (MyGlobals.gCurrentTest.objHostConfig.AutoAdjust)
                        {
                            analyzer18.eTSLFloor = (double)MyGlobals.gCurrentTest.objHostConfig.TslFloor;
                            analyzer18.eTotalNumReaction = MyGlobals.gCurrentTest.objHostConfig.HitsValue;
                            if (tslValue > 0)
                            {
                                analyzer18.dTSLAdjust26Q = tslValue;
                                analyzer18.dTSLLow = MyGlobals.gCurrentTest.objHostConfig.TslValueLow;
                            }
                            analyzer18.AnalysisTest18AutoAdjust(ref MyGlobals.gTestSession, ref MyGlobals.gResultsFolderPath);
                        }
                        else
                        {
                            if (tslValue > 0)
                            {
                                analyzer18.dTSLAdjust18Q = tslValue;
                            }
                            // March 4th 2016
                            // XX Added the global results path so we can use the pictures in the PDF files
                            analyzer18.AnalysisTest18Questions(ref MyGlobals.gTestSession, ref MyGlobals.gResultsFolderPath);


                        }
                        //DTslAdjust18Q
                        //DTslHi
                        //DtsLow

                        //MyGlobals.gTestSession.EndTslValue = analyzer18.TSLPercentage;
                        MyGlobals.gTestSession.BeginTslValue = MyGlobals.gCurrentTest.objHostConfig.TslValue;
                        MyGlobals.gTestSession.EndTslValue = analyzer18.dTSLAdjust18Q;
                        analyzer18.GetRiskAnalysis18QSummary(ref MyGlobals.gTestSession);
                        finalRiskAnalysis18Q = analyzer18.strRiskAnalysis;
                        finalRiskAnalysis18QRecommend = analyzer18.strRiskAnalysisVeracityRecommend;

                        // new RISK values
                        finalIssueA_RiskAnalysis18Q = analyzer18.strRiskAnalysisIssueA;
                        finalIssueB_RiskAnalysis18Q = analyzer18.strRiskAnalysisIssueB;
                        finalIssueC_RiskAnalysis18Q = analyzer18.strRiskAnalysisIssueC;

                        finalQuestionsCount = MyGlobals.gTestSession.lstScreenQuestionOnly.Count.ToString();
                        finalTestTime = MyGlobals.gTestSession.dtBeginQuestionTest.ToString("yyy-MM-dd hh:mm:ss");
                        finalDeviceName = Convert.ToString(MyGlobals.gTestSession.objClient.ClientID);
                        finalDriverLicense = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber;
                        
                        // calculate the test time
                        TimeSpan DifferenceTime = MyGlobals.gTestSession.dtEndQuestionTest - MyGlobals.gTestSession.dtBeginQuestionTest;
                        finalSeconds = string.Format("{0:D2}:{1:D2}", DifferenceTime.Minutes, DifferenceTime.Seconds);
                        

                        break;
                    case 24:
                        analyzer24 = new Anaylyzer24Questions();
                        MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_24Q_1;
                        analyzer24.bRunTheTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;
                        if (MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier)
                        {
                            analyzer24.dTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue;
                        }
                        if (MyGlobals.gCurrentTest.objHostConfig.AutoAdjust)
                        {
                            analyzer24.eTotalNumReaction = MyGlobals.gCurrentTest.objHostConfig.HitsValue;
                            if (tslValue > 0)
                            {
                                analyzer24.dTSLAdjust24Q = tslValue;
                            }
                            analyzer24.AnalysisTest24AutoAdjust(ref MyGlobals.gTestSession);
                        }
                        else
                        {
                            if (tslValue > 0)
                            {
                                analyzer24.dTSLAdjust24Q = tslValue;
                            }
                            analyzer24.AnalysisTest24Questions(ref MyGlobals.gTestSession);

                        }


                        MyGlobals.gTestSession.EndTslValue = analyzer24.dTSLLow;
                        break;
                    case 15:
                        analyzer15 = new Analyzer15Questions();
                        MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_15_1;
                        analyzer15.bRunTheTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;
                        if (MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier)
                        {
                            analyzer15.dTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue;
                        }
                        if (MyGlobals.gCurrentTest.objHostConfig.AutoAdjust)
                        {
                            analyzer15.eTotalNumReaction = MyGlobals.gCurrentTest.objHostConfig.HitsValue;
                            if (tslValue > 0)
                            {
                                analyzer15.dTSLAdjust24Q = tslValue;
                            }
                            analyzer15.AnalysisTest15AutoAdjust(ref MyGlobals.gTestSession);
                        }
                        else
                        {
                            if (tslValue > 0)
                            {
                                analyzer15.dTSLAdjust24Q = tslValue;
                            }
                            analyzer15.AnalysisTest15Questions(ref MyGlobals.gTestSession);
                        }


                        MyGlobals.gTestSession.EndTslValue = analyzer15.dTSLLow;
                        break;
                    case 16:
                        analyzerKOT = new AnalyzerKOTQuestions();
                        analyzerKOT.eTotalNumReaction = MyGlobals.gCurrentTest.objHostConfig.HitsValue;
                        MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_16KOT_1;
                        if (tslValue > 0)
                        {
                            analyzerKOT.dTSLAdjust24Q = tslValue;
                        }
                        analyzerKOT.AnalysisTestKOTQuestions(ref MyGlobals.gTestSession);
                        analyzerKOT.bRunTheTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier;
                        if (MyGlobals.gCurrentTest.objHostConfig.AvgMultiplier)
                        {
                            analyzerKOT.dTestAvgMultiplierCode = MyGlobals.gCurrentTest.objHostConfig.AvgMultiplierValue;
                        }
                        MyGlobals.gTestSession.EndTslValue = analyzerKOT.dTSLLow;
                        break;
                    default:
                        break;
                }

                if (MyGlobals.gCurrentTest.objHostConfig.ShowResultsScreen)
                {
                    if ((finalRiskAnalysis18Q != null) && (finalRiskAnalysis18QRecommend != null))
                    {
                        MainResultsWindow mainResultsWindow = new MainResultsWindow();
                        mainResultsWindow.finalRiskAnalysis18Q = finalRiskAnalysis18Q;
                        mainResultsWindow.finalRiskAnalysis18QRecommend = finalRiskAnalysis18QRecommend;

                        // new RISK SCREEN
                        mainResultsWindow.finalIssueA_RiskAnalysis18Q = finalIssueA_RiskAnalysis18Q;
                        mainResultsWindow.finalIssueB_RiskAnalysis18Q = finalIssueB_RiskAnalysis18Q;
                        mainResultsWindow.finalIssueC_RiskAnalysis18Q = finalIssueC_RiskAnalysis18Q;

                        mainResultsWindow.finalQuestionsCount = finalQuestionsCount;
                        mainResultsWindow.finalDeviceName = System.Environment.MachineName + " Type: " + finalDeviceName;
                        mainResultsWindow.finalTestTime = finalTestTime;
                        mainResultsWindow.finalSeconds = finalSeconds;
                        mainResultsWindow.finalDriverLicense = finalDriverLicense;

                        

                        mainResultsWindow.ShowDialog();
                        if ((mainResultsWindow.bOk) && (!MyGlobals.gCurrentTest.objHostConfig.DoReportPreview))
                        {
                            MyGlobals.gCurrentTest.objHostConfig.DoReportPreview = true;
                        }

                        mainResultsWindow = null;
                    }
                }
                // temp
                //MyGlobals.gCurrentTest.objHostConfig.DoReportEmail = false;

                if (MyGlobals.gCurrentTest.objHostConfig.DoReportEmail)
                {
                    // folder of the local results
                    //MyGlobals.gResultsFolderPath
                    // save report pdf to local
                    PrintManager printMgr = new PrintManager(MyGlobals.translateResourceManager);
                    //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession);
                    if ((MyGlobals.gTestSession.lstScreenQuestionOnly.Count == 16) && (analyzerKOT != null))
                    {
                        printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzerKOT);
                    }
                    else
                    {
                        switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
                        {
                            case 18:
                                break;
                            case 26:
                                // printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer26);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2;
                                printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer26);

                                // the chart window
                                List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
                                List<KeyValuePair<string, int>> valueList2 = new List<KeyValuePair<string, int>>();
                                List<KeyValuePair<string, int>> valueList3 = new List<KeyValuePair<string, int>>();
                                List<KeyValuePair<string, int>> valueList4 = new List<KeyValuePair<string, int>>();

                                Veracity.Client.Proxy.Screen screen = null;

                                for (int i = 0; i < MyGlobals.gTestSession.lstScreenQuestionOnly.Count(); i++)
                                {
                                    valueList.Add(new KeyValuePair<string, int>(i.ToString(), Convert.ToInt16(Math.Round(analyzer26.dSumEI[i], 2))));

                                    screen = (Veracity.Client.Proxy.Screen)MyGlobals.gTestSession.lstScreenQuestionOnly[i];

                                    // convert seconds to ms for graphing
                                    valueList4.Add(new KeyValuePair<string, int>(i.ToString(), Convert.ToInt16(screen.DeltaClickTimeInSeconds * 100)));
                                    // valueList4.Add(new KeyValuePair<string, int>(i.ToString(), Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2))));
                                    //Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2));

                                }


                                valueList2.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                valueList2.Add(new KeyValuePair<string, int>("25", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                //valueList2.Add(new KeyValuePair<string, int>("0", 40));
                                //valueList2.Add(new KeyValuePair<string, int>("25", 40));

                                valueList3.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));
                                valueList3.Add(new KeyValuePair<string, int>("25", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));
                                //valueList3.Add(new KeyValuePair<string, int>("1", 30));
                                //valueList3.Add(new KeyValuePair<string, int>("25", 30));


                                //valueList4.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                //valueList4.Add(new KeyValuePair<string, int>("25", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));

                                GraphWindow.valueList = valueList;
                                GraphWindow.valueList2 = valueList2;
                                GraphWindow.valueList3 = valueList3;
                                GraphWindow.valueList4 = valueList4;


                                GraphWindow chartWindow = new GraphWindow();




                                //GraphWindow chartWindow = new GraphWindow();
                                chartWindow.Show();

                                PrintDialog printDlg = new PrintDialog();
                                PrintQueue printQueue = LocalPrintServer.GetDefaultPrintQueue();
                                printDlg.PrintQueue = printQueue;
                                //chartWindow.
                                //if (printDlg.ShowDialog() == true)
                                //{
                                //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                //    chartWindow.Close();

                                //}
                                int iCounter = 0;
                                while (iCounter <= 10)
                                {
                                    iCounter++;
                                    System.Threading.Thread.Sleep(1000);
                                    System.Windows.Forms.Application.DoEvents();
                                }

                                //printDlg.PrintDocument = chartWindow;

                                printDlg.PrintVisual(chartWindow, "Window Printing");

                                chartWindow.Close();

                                //if (printDlg.ShowDialog() == true)
                                //{
                                //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                //    chartWindow.Close();

                                //}




                                break;
                            default:
                                break;
                        }
                    }

                }

                if (MyGlobals.gCurrentTest.objHostConfig.DoReportPrinting)
                {
                    PrintManager printMgr = new PrintManager(MyGlobals.translateResourceManager);
                    //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession);
                    if ((MyGlobals.gTestSession.lstScreenQuestionOnly.Count == 16) && (analyzerKOT != null))
                    {
                        printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzerKOT);
                    }
                    else
                    {
                        switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
                        {
                            case 18:
                                // Admin options here
                                printMgr.sTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle;
                                printMgr.sTitle18QPage2 = MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle;
                                printMgr.sSubTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle;
                                printMgr.sIssueATitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueATitle;
                                printMgr.sIssueBTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueBTitle;
                                printMgr.sIssueCTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueCTitle;
                                printMgr.sIssueDTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueDTitle;
                                printMgr.iTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin;
                                printMgr.iTitle18QPage2LeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin;
                                printMgr.iSubTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin;
                                printMgr.bPrintToPDF = MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF;

                                // MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_1;
                                // printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer18);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
                                printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer18, MyGlobals.gResultsFolderPath);


                                // May 31 2015 - force graph to be off, per phil
                                MyGlobals.gCurrentTest.objHostConfig.PrintGraph = false;
                                if (MyGlobals.gCurrentTest.objHostConfig.PrintGraph)
                                {
                                    // the chart window
                                    List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList2 = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList3 = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList4 = new List<KeyValuePair<string, int>>();

                                    Veracity.Client.Proxy.Screen screen = null;

                                    valueList.Add(new KeyValuePair<string, int>("0", 0));
                                    valueList4.Add(new KeyValuePair<string, int>("0", 0));
                                    for (int i = 0; i < MyGlobals.gTestSession.lstScreenQuestionOnly.Count(); i++)
                                    {
                                        valueList.Add(new KeyValuePair<string, int>((i + 1).ToString(), Convert.ToInt16(Math.Round(analyzer18.dSumEI[i], 2))));

                                        screen = (Veracity.Client.Proxy.Screen)MyGlobals.gTestSession.lstScreenQuestionOnly[i];

                                        // convert seconds to ms for graphing
                                        valueList4.Add(new KeyValuePair<string, int>((i + 1).ToString(), Convert.ToInt16(screen.DeltaClickTimeInSeconds * 100)));
                                        // valueList4.Add(new KeyValuePair<string, int>(i.ToString(), Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2))));
                                        //Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2));

                                    }


                                    valueList2.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[12], 2))));
                                    valueList2.Add(new KeyValuePair<string, int>("18", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[12], 2))));
                                    //valueList2.Add(new KeyValuePair<string, int>("0", 40));
                                    //valueList2.Add(new KeyValuePair<string, int>("25", 40));

                                    valueList3.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[13], 2))));
                                    valueList3.Add(new KeyValuePair<string, int>("18", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[13], 2))));
                                    //valueList3.Add(new KeyValuePair<string, int>("1", 30));
                                    //valueList3.Add(new KeyValuePair<string, int>("25", 30));


                                    //valueList4.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[12], 2))));
                                    //valueList4.Add(new KeyValuePair<string, int>("25", Convert.ToInt16(Math.Round(analyzer18.dRDTimeAvg[13], 2))));

                                    GraphWindow.valueList = valueList;
                                    GraphWindow.valueList2 = valueList2;
                                    GraphWindow.valueList3 = valueList3;
                                    GraphWindow.valueList4 = valueList4;


                                    GraphWindow chartWindow = new GraphWindow();




                                    //GraphWindow chartWindow = new GraphWindow();
                                    chartWindow.Show();

                                    PrintDialog printDlg = new PrintDialog();
                                    PrintQueue printQueue = LocalPrintServer.GetDefaultPrintQueue();
                                    printDlg.PrintQueue = printQueue;
                                    //chartWindow.
                                    //if (printDlg.ShowDialog() == true)
                                    //{
                                    //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                    //    chartWindow.Close();

                                    //}
                                    int iCounter = 0;
                                    while (iCounter <= 10)
                                    {
                                        iCounter++;
                                        System.Threading.Thread.Sleep(1000);
                                        System.Windows.Forms.Application.DoEvents();
                                    }

                                    //printDlg.PrintDocument = chartWindow;

                                    printDlg.PrintVisual(chartWindow, "Window Printing");



                                    ///////////////////////////////////////////////////////////////////
                                    //  START Try to print to the PDF file
                                    ///////////////////////////////////////////////////////////////////
                                    bool bPrintGraphToPDF = MyGlobals.gCurrentTest.objHostConfig.PrintGraphToPDF;
                                    // bPrintGraphToPDF = true;
                                    if (bPrintGraphToPDF)
                                    {
                                        try
                                        {
                                            string documentPDFName = MyGlobals.gTestSession.TestSessionID + "_page3";
                                            PrintQueue printQueuePDF = new PrintQueue(new PrintServer(), "PDFCreator");
                                            printDlg.PrintQueue = printQueuePDF;
                                            printDlg.PrintVisual(chartWindow, documentPDFName);
                                        }
                                        catch (Exception exc)
                                        {

                                            MessageBox.Show("Printing error!  Please install PDFCreator.\n" + exc.Message, "Error");
                                        }
                                    }
                                    ///////////////////////////////////////////////////////////////////
                                    // END Try to print to the PDF file
                                    ///////////////////////////////////////////////////////////////////

                                    chartWindow.Close();

                                    //if (printDlg.ShowDialog() == true)
                                    //{
                                    //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                    //    chartWindow.Close();

                                    //}


                                    /////////////////////////////////////////////////////////////////
                                    ////
                                    ////
                                    //// PRINT THE GRAPH TO THE PDF FILE
                                    ////
                                    ////
                                    /////////////////////////////////////////////////////////////////

                                    //bool bPrintGraphToPDF = true;

                                    //if (bPrintGraphToPDF)
                                    //{
                                    //    PrintDialog pDia = new PrintDialog();

                                    //    LocalPrintServer.Equals

                                    //    PrinterSettings ps = new PrinterSettings();
                                    //    pDia.Document = objDoc;
                                    //    pDia.Document.DocumentName = pdfDocumentName;
                                    //    //ps.PrinterName = "CutePDF Writer";
                                    //    ps.PrinterName = "PDFCreator";
                                    //    ps.PrintToFile = false;


                                    //    ps.PrintFileName = "C:\\Veracity\\printgraph.pdf";
                                    //    // Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                                    //    //pDia.Document.OriginAtMargins = true;
                                    //    //ps.DefaultPageSettings.Margins.Left = 2;
                                    //    objDoc.PrinterSettings = ps;

                                    //    try
                                    //    {
                                    //        objDoc.Print();
                                    //    }

                                    //    catch (Exception exc)
                                    //    {
                                    //        MessageBox.Show("Printing error!  Please install PDFCreator.\n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    //    }
                                    //}
                                }
                                break;
                            case 26:
                                //printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer26);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2;
                                printMgr.PrintReportToDefaultPrinter(MyGlobals.gTestSession, analyzer26);

                                if (MyGlobals.gCurrentTest.objHostConfig.PrintGraph)
                                {
                                    // the chart window
                                    List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList2 = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList3 = new List<KeyValuePair<string, int>>();
                                    List<KeyValuePair<string, int>> valueList4 = new List<KeyValuePair<string, int>>();

                                    Veracity.Client.Proxy.Screen screen = null;

                                    valueList.Add(new KeyValuePair<string, int>("0", 0));
                                    valueList4.Add(new KeyValuePair<string, int>("0", 0));
                                    for (int i = 0; i < MyGlobals.gTestSession.lstScreenQuestionOnly.Count(); i++)
                                    {
                                        valueList.Add(new KeyValuePair<string, int>((i + 1).ToString(), Convert.ToInt16(Math.Round(analyzer26.dSumEI[i], 2))));

                                        screen = (Veracity.Client.Proxy.Screen)MyGlobals.gTestSession.lstScreenQuestionOnly[i];

                                        // convert seconds to ms for graphing
                                        valueList4.Add(new KeyValuePair<string, int>((i + 1).ToString(), Convert.ToInt16(screen.DeltaClickTimeInSeconds * 100)));
                                        // valueList4.Add(new KeyValuePair<string, int>(i.ToString(), Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2))));
                                        //Convert.ToInt16(Math.Round(screen.DeltaClickTimeInSeconds, 2));

                                    }


                                    valueList2.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                    valueList2.Add(new KeyValuePair<string, int>("26", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                    //valueList2.Add(new KeyValuePair<string, int>("0", 40));
                                    //valueList2.Add(new KeyValuePair<string, int>("25", 40));

                                    valueList3.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));
                                    valueList3.Add(new KeyValuePair<string, int>("26", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));
                                    //valueList3.Add(new KeyValuePair<string, int>("1", 30));
                                    //valueList3.Add(new KeyValuePair<string, int>("25", 30));


                                    //valueList4.Add(new KeyValuePair<string, int>("0", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[12], 2))));
                                    //valueList4.Add(new KeyValuePair<string, int>("25", Convert.ToInt16(Math.Round(analyzer26.dRDTimeAvg[13], 2))));

                                    GraphWindow.valueList = valueList;
                                    GraphWindow.valueList2 = valueList2;
                                    GraphWindow.valueList3 = valueList3;
                                    GraphWindow.valueList4 = valueList4;


                                    GraphWindow chartWindow = new GraphWindow();




                                    //GraphWindow chartWindow = new GraphWindow();
                                    chartWindow.Show();

                                    PrintDialog printDlg = new PrintDialog();
                                    PrintQueue printQueue = LocalPrintServer.GetDefaultPrintQueue();
                                    printDlg.PrintQueue = printQueue;
                                    //chartWindow.
                                    //if (printDlg.ShowDialog() == true)
                                    //{
                                    //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                    //    chartWindow.Close();

                                    //}
                                    int iCounter = 0;
                                    while (iCounter <= 15)
                                    {
                                        iCounter++;
                                        System.Threading.Thread.Sleep(1000);
                                        System.Windows.Forms.Application.DoEvents();
                                    }

                                    //printDlg.PrintDocument = chartWindow;

                                    printDlg.PrintVisual(chartWindow, "Window Printing");

                                    chartWindow.Close();

                                    //if (printDlg.ShowDialog() == true)
                                    //{
                                    //    printDlg.PrintVisual(chartWindow, "Window Printing");

                                    //    chartWindow.Close();

                                    //}
                                }

                                break;
                            default:
                                break;
                        }
                    }
                }

                if (MyGlobals.gCurrentTest.objHostConfig.DoReportPdf)
                {
                    PrintManager printMgr = new PrintManager(MyGlobals.translateResourceManager);

                    if ((MyGlobals.gTestSession.lstScreenQuestionOnly.Count == 16)
                        && (analyzerKOT != null))
                    {
                        printMgr.PrintReportToPdf(MyGlobals.gTestSession, analyzerKOT);
                    }
                    else
                    {
                        switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
                        {
                            case 18:
                                // Admin options here
                                printMgr.sTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle;
                                printMgr.sTitle18QPage2 = MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle;
                                printMgr.sSubTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle;
                                printMgr.sIssueATitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueATitle;
                                printMgr.sIssueBTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueBTitle;
                                printMgr.sIssueCTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueCTitle;
                                printMgr.sIssueDTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueDTitle;
                                printMgr.iTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin;
                                printMgr.iTitle18QPage2LeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin;
                                printMgr.iSubTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin;
                                printMgr.bPrintToPDF = MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF;

                                //MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_1;
                                //printMgr.PrintReportToPdf(MyGlobals.gTestSession, analyzer18);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
                                if (printMgr.bPrintToPDF)
                                {
                                    printMgr.PrintReportToPdf(MyGlobals.gTestSession, analyzer18);
                                }
                                break;
                            case 26:
                                printMgr.PrintReportToPdf(MyGlobals.gTestSession, analyzer26);
                                //MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2;
                                //printMgr.PrintReportToPdf(MyGlobals.gTestSession, analyzer26);
                                break;
                            default:
                                break;
                        }
                    }
                }

                if (MyGlobals.gCurrentTest.objHostConfig.DoReportPreview)
                {
                    PrintManager printMgr = new PrintManager(MyGlobals.translateResourceManager);

                    // Admin options here
                    printMgr.sTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1ReportTitle;
                    printMgr.sTitle18QPage2 = MyGlobals.gCurrentTest.objHostConfig.Page2ReportTitle;
                    printMgr.sSubTitle18Q = MyGlobals.gCurrentTest.objHostConfig.Page1SubTitle;
                    printMgr.sIssueATitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueATitle;
                    printMgr.sIssueBTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueBTitle;
                    printMgr.sIssueCTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueCTitle;
                    printMgr.sIssueDTitle18Q = MyGlobals.gCurrentTest.objHostConfig.IssueDTitle;
                    printMgr.iTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1LeftMargin;
                    printMgr.iTitle18QPage2LeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page2LeftMargin;
                    printMgr.iSubTitle18QLeftMargin = MyGlobals.gCurrentTest.objHostConfig.Page1SubtitleLeftMargin;
                    printMgr.bPrintToPDF = MyGlobals.gCurrentTest.objHostConfig.PrintReportToPDF;


                    //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession);
                    if ((MyGlobals.gTestSession.lstScreenQuestionOnly.Count == 16) && (analyzerKOT != null))
                    {
                        printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzerKOT);

                    }
                    else
                    {
                        switch (MyGlobals.gTestSession.lstScreenQuestionOnly.Count)
                        {
                            case 26:
                                //MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_1;
                                // printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer26);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2;
                                printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer26);
                                break;
                            case 18:
                                //MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_1;
                                //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer18);
                                MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
                                printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer18, MyGlobals.gResultsFolderPath);

                                //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer18);
                                //MyGlobals.gTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
                                //printMgr.CreateReportType1PrintPreview(MyGlobals.gTestSession, analyzer18);
                                break;
                            default:
                                break;
                        }
                    }
                }

                MyGlobals.gTestSession.SerializeToLocalCsvWithIdentification(MyGlobals.gCurrentExecutableFolderPath, null, MyGlobals.gAppController.DoOutputCSVEncryption);
                MyGlobals.gTestSession.SerializeToLocalCsvWithIdentification(MyGlobals.gCurrentExecutableFolderPath, MyGlobals.gLstSensorAll, MyGlobals.gAppController.DoOutputCSVEncryption);

                //if (doIncludeIdentityInOutputCSVFile)
                //{
                //    MyGlobals.gTestSession.SerializeToLocalCsvWithIdentification(MyGlobals.gCurrentExecutableFolderPath, null);
                //}
                //else
                //{
                //    MyGlobals.gTestSession.SerializeToLocalCsv(MyGlobals.gCurrentExecutableFolderPath, null);
                //}

                if (MyGlobals.gAppController.TestLimitNum != -1)
                {
                    MyGlobals.gAppController.TestCurrentNum--;
                    MyGlobals.gAppController.SerializeWorking();
                }

                // serialize to full xml
                MyGlobals.gTestSession.IsFinished = true;
                MyGlobals.gTestSession.SerializeToXml(MyGlobals.gResultsFolderPath);
                if ((MyGlobals.gTestSession.objCurrentTest.objHostConfig.EmailAddress != null)
                    && (MyGlobals.gTestSession.objCurrentTest.objHostConfig.EmailAddress.Trim().Length > 0)
                    && (MyGlobals.gTestSession.objCurrentTest.objHostConfig.PrintReportToPDF))
                {
                    // send email
                    string reportsDir = @"C:\SafeScreenReports";
                    if (!Directory.Exists(reportsDir))
                    {
                        Directory.CreateDirectory(reportsDir);
                    }

                    string reportsDirMattVersion = MyGlobals.gResultsFolderPath; // kludge to add matt version


                    string f1 = MyGlobals.gTestSession.TestSessionID + "_page1.pdf";
                    string f2 = MyGlobals.gTestSession.TestSessionID + "_page2.pdf";
                    string f3 = MyGlobals.gTestSession.TestSessionID + "_page3.pdf";

                    // 20181028 new reports are in the working directory workspace
                    string imagesdir = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + "Results";
                    string fCurrentXML = MyGlobals.gTestSession.TestSessionID + ".xml";
                    string fileNameXML = imagesdir + System.IO.Path.DirectorySeparatorChar + fCurrentXML;
                    string fCurrentPDF = MyGlobals.gTestSession.TestSessionID + "-" +   MyGlobals.gTestSession.objClient.ClientID + "-" + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + ".pdf";
                    string fileNamePDF = imagesdir + System.IO.Path.DirectorySeparatorChar + fCurrentPDF;


                    DateTime dtNow = DateTime.Now;
                    //string timestamp = dtNow.ToString("yyyy-MM-dd HH:mm:ss");
                    string timestamp = dtNow.ToString("yyyyMMddhhmmss"); // change the format to match Matt's version of the file name
                    string clientID = Convert.ToString(MyGlobals.gTestSession.objClient.ClientID); // added the client id

                    string f2_matt_version = timestamp + "-" + clientID + "-" + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + ".pdf";
                    string p2_matt_version = reportsDirMattVersion + System.IO.Path.DirectorySeparatorChar + f2_matt_version;

                    string subject = "PTSSA Test Results For " + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + " " + timestamp;
                    string body = "TestID " + clientID + ": Attached are the results for PTSSA SafeScreener test for subject " + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + " at date " + timestamp + " for filename " + fCurrentPDF;
                    //string f1_friendly = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + "_" + timestamp + "_page1.pdf";
                    string f1_friendly = fCurrentXML;

                    //string f2_friendly = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + "_" + timestamp + "_page2.pdf";
                    string f2_friendly = fCurrentPDF;
                    string f3_friendly = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + "_" + timestamp + "_page3.pdf";

                    //string p1 = reportsDir + System.IO.Path.DirectorySeparatorChar + f1;
                    //string p2 = reportsDir + System.IO.Path.DirectorySeparatorChar + f2;
                    string p3 = reportsDir + System.IO.Path.DirectorySeparatorChar + f3;
                    string p1 = fileNameXML;
                    string p2 = fileNamePDF;
                    


                    // 20181214 increase time from 15000
                    System.Threading.Thread.Sleep(30000);

                    MemoryStream ms1 = null;
                    MemoryStream ms2 = null;
                    MemoryStream ms3 = null;
                    byte[] data = null;
                    if (File.Exists(p1))
                    {
                        data = File.ReadAllBytes(p1);
                        ms1 = new MemoryStream(data);
                    }
                    if (File.Exists(p2))
                    {
                        data = File.ReadAllBytes(p2);
                        ms2 = new MemoryStream(data);
                    }

                    // we will override above if matt version exists
                    if (File.Exists(p2_matt_version))
                    {
                        data = File.ReadAllBytes(p2_matt_version);
                        ms2 = new MemoryStream(data);
                    }
                    if (File.Exists(p3))
                    {
                        data = File.ReadAllBytes(p3);
                        ms3 = new MemoryStream(data);
                    }

                    Emailer emailer = new Emailer();
                    bool bSuccess = MyGlobals.SendEmail(body, MyGlobals.gTestSession.objCurrentTest.objHostConfig.EmailAddress, subject,
                        ms1, f1_friendly, ms2, f2_friendly, ms3, f3_friendly);
                    if (!bSuccess)
                    {
                        // 20181028 per Phil remove the message box when not able to email out
                        //MessageBox.Show("There was an error in sending email to " + MyGlobals.gTestSession.objCurrentTest.objHostConfig.EmailAddress);
                    } else
                    {
                        //File.Delete(fileNamePDF);
                        //File.Delete(fileNameXML);

                        // 20181112 special condition.  If the email address != null but the SAVE PDF was not check
                        // then lets delete the file.  User most likely did NOT want to save on the local drive
                        //
                        //if (MyGlobals.bDeletePDFFile == true)
                        
                        //{
                        //    //System.Threading.Thread.Sleep(150000);
                        //    File.Delete(fileNamePDF);
                        //    File.Delete(fileNameXML);
                        //}
                    }
                }

                string controllerFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath)
                                            + System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
                MyGlobals.SetInitialGlobals(controllerFilePath);
                //this.NavigationService.Navigate(new WelcomePage());
                //Process.Start(Assembly.GetEntryAssembly().Location);
                //Application.Current.Shutdown();
                //Process.GetCurrentProcess().Kill();

                ClearHistory();

                if ((MyGlobals.gAppController.lstGlobalTestLink != null) && (MyGlobals.gAppController.lstGlobalTestLink.Count > 0))
                {
                    int ind = 0;
                    object page = null;
                    if (MyGlobals.currentTestLinkIndex >= (MyGlobals.gAppController.lstGlobalTestLink.Count - 1))
                    {
                        // need to start from first test link
                        MyGlobals.gLinkIdentification = null;
                        MyGlobals.currentTestLinkIndex = 0;
                        ind = MyGlobals.gAppController.lstGlobalTestLink[0].Index;
                        MyGlobals.bDoForwardTestLink = false;
                        MyGlobals.SetTestChoiceGlobals(ind);
                        MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.WELCOME;
                        page = MyGlobals.RouteToCurrentPage();
                        this.NavigationService.Navigate(page);

                    }
                    else
                    {
                        // go to next test link
                        MyGlobals.currentTestLinkIndex++;
                        ind = MyGlobals.gAppController.lstGlobalTestLink[MyGlobals.currentTestLinkIndex].Index;
                        MyGlobals.bDoForwardTestLink = true;
                        MyGlobals.gLinkIdentification = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber;
                        MyGlobals.SetTestChoiceGlobals(ind);
                        MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.SCREEN;
                        page = MyGlobals.RouteToNextPage();
                        this.NavigationService.Navigate(page);

                    }
                }
                else
                {
                    this.NavigationService.Navigate(new WelcomePage());
                }




                //this.NavigationService.Navigate(new WelcomePage());

            }
            catch (Exception exc)
            {
                MessageBox.Show("The system encountered unexpected error:  " + exc.Message);
                Process.Start(Assembly.GetEntryAssembly().Location);
                //Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
                //                string controllerFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
                //System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
                //                MyGlobals.SetInitialGlobals(controllerFilePath);
                //                this.NavigationService.Navigate(new WelcomePage());
            }
        }

        public void ClearHistory()
        {
            if (!this.NavigationService.CanGoBack && !this.NavigationService.CanGoForward)
            {
                return;
            }

            var entry = this.NavigationService.RemoveBackEntry();
            while (entry != null)
            {
                entry = this.NavigationService.RemoveBackEntry();
            }

            this.NavigationService.Navigate(new PageFunction<string>() { RemoveFromJournal = true });
        }

        public void StartEndTimer()
        {
            endDispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, (int)MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.TransitionDelayInMs);
            endDispatcherTimer.Start();

        }

        private void noButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _currentWorkingScreen.BeginAnsClickDate = DateTime.Now;
            _currentWorkingScreen.Answer = false;
            ans = "N";
            _ansTimeStopWatch.Start();
            logTouch("NO_BUTTON_PREVIEW_MOUSE_DOWN");
        }

        private void doMouseUp()
        {
            //this.mainImage.Focus();
            _readTimeStopWatch.Stop();
            _ansTimeStopWatch.Stop();
            _currentWorkingScreen.EndAnsClickDate = DateTime.Now;
            _currentWorkingScreen.ExitScreenDate = DateTime.Now;

            _currentWorkingScreen.ReadTimeInSeconds = _readTimeStopWatch.Elapsed.TotalSeconds;
            _currentWorkingScreen.DeltaClickTimeInSeconds = _ansTimeStopWatch.Elapsed.TotalSeconds;

            logTouch("[SW ELAPSED TIME IN SEC][" + _currentWorkingScreen.DeltaClickTimeInSeconds + "]");

            // round to 6 places

            _currentWorkingScreen.ReadTimeInSeconds = Math.Round(_currentWorkingScreen.ReadTimeInSeconds, 6);
            _currentWorkingScreen.DeltaClickTimeInSeconds = Math.Round(_currentWorkingScreen.DeltaClickTimeInSeconds, 6);


            _currentWorkingScreen.FlowOrderIndex = MyGlobals.gCurrentLnkTestTemplateScreenListIndex + 1;

            // flip the data.  Only temporary
            //double tmp = _currentWorkingScreen.DeltaClickTimeInSeconds;
            //_currentWorkingScreen.DeltaClickTimeInSeconds = _currentWorkingScreen.ReadTimeInSeconds;
            //_currentWorkingScreen.ReadTimeInSeconds = tmp;

            MyGlobals.gTestSession.lstScreenAll.Add(_currentWorkingScreen);
            if (_currentWorkingScreen.ScreenTypeID == 3)
            {
                MyGlobals.gTestSession.lstScreenQuestionOnly.Add(_currentWorkingScreen);
            }
            MyGlobals.gCurrentLnkTestTemplateScreenListIndex++;
            if (MyGlobals.gCurrentLnkTestTemplateScreenListIndex <= (MyGlobals.gCurrentTest.lstLnkTestTemplateScreen.Count - 1))
            {
                //if (MyGlobals.gWaitPage2 == null)
                //{
                //    MyGlobals.gWaitPage2 = new WaitPage2();
                //}
                //MyGlobals.gWaitPage2.StartTimer();
                //this.NavigationService.Navigate(MyGlobals.gWaitPage2);
                //MyGlobals.gWaitPage2 = null;

                //if (MyGlobals.gWaitPage == null)
                //{
                //    MyGlobals.gWaitPage = new WaitPage();
                //}
                //MyGlobals.gWaitPage.StartTimer();
                //this.NavigationService.Navigate(MyGlobals.gWaitPage);


                if (MyGlobals.gWaitPage3 == null)
                {
                    MyGlobals.gWaitPage3 = new WaitPage3();
                }
                MyGlobals.gWaitPage3.StartTimer();
                this.NavigationService.Navigate(MyGlobals.gWaitPage3);
                //MyGlobals.gWaitPage3 = null;

            }

        }

        private BitmapImage GetBitmapImageFromBytes(byte[] bytes)
        {
            BitmapImage btm;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                btm = new BitmapImage();
                btm.BeginInit();
                btm.StreamSource = ms;
                // Below code for caching is crucial.
                btm.CacheOption = BitmapCacheOption.OnLoad;
                btm.EndInit();
                btm.Freeze();
            }
            return btm;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

            if ((bool)MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.HasAudio)
            {
                string audioFilePath = getAudioPath();
                if (File.Exists(audioFilePath))
                {
                    if (isWaveFile(MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.AudioFileName))
                    {
                        if (MyGlobals.gCurrentTest.objHostConfig.IsAudioOnly)
                        {
                            this.yesButton.IsEnabled = false;
                            this.noButton.IsEnabled = false;
                            this.pressAnsTextBlock.Visibility = System.Windows.Visibility.Hidden;
                            this.yesButton.Opacity = 0.25;
                            this.noButton.Opacity = 0.25;

                            int audioDurationInSec = UtilAudio.AudioDurationInSeconds(audioFilePath);
                            if (audioDurationInSec > 0)
                            {
                                audioEndDispatcherTimer.Tick += new EventHandler(audioEndDispatcherTimer_Tick);
                                audioEndDispatcherTimer.Interval = new TimeSpan(0, 0, audioDurationInSec);
                                audioEndDispatcherTimer.Start();
                            }
                            _soundPlayer = new SoundPlayer(audioFilePath);
                            _soundPlayer.Play();
                        }



                    }
                    //else if (isMp3File(MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.AudioFileName))
                    //{
                    //    _mediaElement = new MediaElement();
                    //    _mediaElement.LoadedBehavior = MediaState.Manual;
                    //    _mediaElement.Source = new Uri(audioFilePath);
                    //    _mediaElement.Play();
                    //}
                }
            }
        }

        private void audioEndDispatcherTimer_Tick(object sender, EventArgs e)
        {
            this.yesButton.Opacity = 1.0;
            this.noButton.Opacity = 1.0;
            this.yesButton.IsEnabled = true;
            this.noButton.IsEnabled = true;
            this.pressAnsTextBlock.Visibility = System.Windows.Visibility.Visible;
            _readTimeStopWatch.Start();
        }

        private string getAudioPath()
        {
            string audioFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
            System.IO.Path.DirectorySeparatorChar + "Workspace" +
            System.IO.Path.DirectorySeparatorChar + "Data" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.objTestTemplate.TestTemplateID +
            System.IO.Path.DirectorySeparatorChar + "Language" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.selectedTestLanguage.Code +
            System.IO.Path.DirectorySeparatorChar + "Audio" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.AudioFileName;
            return audioFilePath;
        }

        private string getImagePath()
        {
            //string imageFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
            string imageFilePath = System.IO.Path.DirectorySeparatorChar + "Workspace" +
            System.IO.Path.DirectorySeparatorChar + "Data" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.objTestTemplate.TestTemplateID +
            System.IO.Path.DirectorySeparatorChar + "Language" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.selectedTestLanguage.Code +
            System.IO.Path.DirectorySeparatorChar + "ScreenImages" +
            System.IO.Path.DirectorySeparatorChar + MyGlobals.gCurrentTest.currentLnkTestTemplateScreen.ImageFileName;
            return imageFilePath;
        }

        private bool isWaveFile(string pStrFile)
        {
            bool b = false;
            if (pStrFile != null)
            {
                if (pStrFile.IndexOf(".wav") != -1)
                {
                    b = true;
                }
            }
            return b;
        }

        private bool isMp3File(string pStrFile)
        {
            bool b = false;
            if (pStrFile != null)
            {
                if (pStrFile.IndexOf(".mp3") != -1)
                {
                    b = true;
                }
            }
            return b;
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            resetTimer.Stop();
            resetTimer.Tick -= resetTimer_Tick;
            endDispatcherTimer.Stop();
            endDispatcherTimer.Tick -= endDispatcherTimer_Tick;
            //  dumpSensorData();
            //if ((sbMetrics != null) && (sbMetrics.Length > 0))
            //{
            //    // output to file
            //    string sensorDataFilepath = MyGlobals.gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + "SensorData.txt";
            //    File.WriteAllText(sensorDataFilepath, sbMetrics.ToString());
            //}
        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _soundPlayer.Stop();
            }
            catch { }
            AdminPasswordPage page = new AdminPasswordPage();
            page.nextPageMode = AdminPasswordPage.NextPageMode.MANAGER;
            this.NavigationService.Navigate(page);
        }

        private void logTouch(string eventName)
        {
            try
            {
                DateTime dtNow = DateTime.Now;
                string timestamp = dtNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
                string msg = timestamp + "," + eventName + "\n";
                System.IO.File.AppendAllText(MyGlobals.gLogFilePath, msg);
            }
            catch { }
        }

        private void yesPreviewStylusDown()
        {

            try
            {
                _soundPlayer.Stop();
            }
            catch { }
            _currentWorkingScreen.BeginAnsClickDate = DateTime.Now;
            _currentWorkingScreen.Answer = true;
            ans = "Y";
            _ansTimeStopWatch.Start();
            logTouch("YES_BUTTON_STYLUS_DOWN");
        }
        private void yesButton_PreviewStylusDown(object sender, StylusDownEventArgs e)
        {
            yesPreviewStylusDown();
        }

        private void yesPreviewStylusUp()
        {
            logTouch("YES_BUTTON_STYLUS_LEAVE");
            doMouseUp();
        }
        private void yesButton_PreviewStylusUp(object sender, StylusEventArgs e)
        {
            yesPreviewStylusUp();
        }

        private void noPreviewStylusDown()
        {
            try
            {
                try
                {
                    if (_soundPlayer != null)
                    {
                        _soundPlayer.Stop();
                    }

                }
                catch { }

            }
            catch { }
            if (_currentWorkingScreen.ScreenTypeID > 1)
            {
                _currentWorkingScreen.BeginAnsClickDate = DateTime.Now;
                _currentWorkingScreen.Answer = false;
                ans = "N";
                _ansTimeStopWatch.Start();
                logTouch("NO_BUTTON_STYLUS_DOWN");
            }
        }
        private void noButton_PreviewStylusDown(object sender, StylusDownEventArgs e)
        {
            noPreviewStylusDown();
        }

        private void noPreviewStylusUp()
        {
            if (_currentWorkingScreen.ScreenTypeID > 1)
            {
                logTouch("NO_BUTTON_STYLUS_LEAVE");
                doMouseUp();
            }
        }
        private void noButton_PreviewStylusUp(object sender, StylusEventArgs e)
        {
            noPreviewStylusUp();
        }

        private void noButton_Click(object sender, RoutedEventArgs e)
        {
            if (_currentWorkingScreen.ScreenTypeID == 1)
            {
                try
                {
                    resetTimer.Stop();
                    endDispatcherTimer.Stop();
                    //this.NavigationService.Navigate(new WelcomePage());
                    this.NavigationService.Navigate(new AlertPage());
                }
                catch { }
            }
            e.Handled = true;
        }

        private void yesButton_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
            //yesPreviewStylusDown();
            //yesPreviewStylusUp();
        }

        //private void yesButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        _soundPlayer.Stop();
        //    }
        //    catch { }
        //}

        //private void noButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (_currentWorkingScreen.ScreenTypeID == 1)
        //        {
        //            try
        //            {
        //                resetTimer.Stop();
        //                endDispatcherTimer.Stop();
        //                this.NavigationService.Navigate(new WelcomePage());
        //            }
        //            catch { }
        //        }
        //        _soundPlayer.Stop();
        //    }
        //    catch { }
        //}

        //private void yesButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    _currentWorkingScreen.BeginAnsClickDate = DateTime.Now;
        //    _currentWorkingScreen.Answer = true;
        //    _ansTimeStopWatch.Start();
        //    logTouch("YES_BUTTON_PREVIEW_MOUSE_DOWN");
        //}

        //private void noButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    _currentWorkingScreen.BeginAnsClickDate = DateTime.Now;
        //    _currentWorkingScreen.Answer = false;
        //    _ansTimeStopWatch.Start();
        //    logTouch("NO_BUTTON_PREVIEW_MOUSE_DOWN");
        //}

        //private void yesButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        //{
        //    logTouch("YES_BUTTON_PREVIEW_MOUSE_UP");
        //    doMouseUp();
        //}

        //private void noButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        //{
        //    logTouch("NO_BUTTON_PREVIEW_MOUSE_UP");
        //    doMouseUp();
        //}

        //private void logTouch(string eventName)
        //{
        //    try
        //    {
        //        DateTime dtNow = DateTime.Now;
        //        string timestamp = dtNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
        //        string msg = timestamp + "," + eventName + "\n";
        //        System.IO.File.AppendAllText(MyGlobals.gLogFilePath, msg);
        //    }
        //    catch { }
        //}

        // pressure readings
        private CWintabContext OpenQueryDigitizerContext()
        {
            bool status = false;
            CWintabContext logContext = null;

            try
            {
                // Get the default digitizing context.  Turn off events.  Control system cursor.
                logContext = CWintabInfo.GetDefaultDigitizingContext(ECTXOptionValues.CXO_SYSTEM);

                logContext.Options |= (uint)ECTXOptionValues.CXO_MESSAGES;
                logContext.Options &= ~(uint)ECTXOptionValues.CXO_SYSTEM;

                if (logContext == null)
                {
                    TraceMsg("OpenQueryDigitizerContext: FAILED to get default digitizing context.\n");
                    //System.Diagnostics.Debug.WriteLine("FAILED to get default digitizing context.");
                    return null;
                }

                // Modify the digitizing region.
                logContext.Name = "WintabDN Query Data Context";

                //WintabAxis tabletX = CWintabInfo.GetTabletAxis(EAxisDimension.AXIS_X);
                //WintabAxis tabletY = CWintabInfo.GetTabletAxis(EAxisDimension.AXIS_Y);

                //// Output X/Y values 1-1 with tablet dimensions.
                // logContext.OutOrgX = logContext.OutOrgY = 0;
                // logContext.OutExtX = tabletX.axMax;
                // logContext.OutExtY = tabletY.axMax;

                logContext.SysOrgX = logContext.SysOrgY = 0;
                logContext.SysExtX = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width;
                logContext.SysExtY = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Height;

                // Open the context, which will also tell Wintab to send data packets.
                status = logContext.Open();

                TraceMsg("Context Open: " + (status ? "PASSED [ctx=" + logContext.HCtx + "]" : "FAILED") + "\n");
                //System.Diagnostics.Debug.WriteLine("Context Open: " + (status ? "PASSED [ctx=" + logContext.HCtx + "]" : "FAILED"));
            }
            catch (Exception ex)
            {
                TraceMsg("OpenQueryDigitizerContext: ERROR : " + ex.ToString());
            }

            return logContext;
        }



        /// <summary>
        /// Responds to pen data by removing or peek/flushing data.
        /// </summary>
        /// <param name="sender_I"></param>
        /// <param name="eventArgs_I"></param>
        public void MyWTPacketEventHandler(Object sender_I, MessageReceivedEventArgs eventArgs_I)
        {
            UInt32 numPkts = 0;

            //System.Diagnostics.Debug.WriteLine("Received WT_PACKET event");
            if (m_wtData == null)
            {
                return;
            }

            bool removeData = true;
            //bool removeData = this.removeRadioButton.Checked;

            try
            {
                // If removeData is true, packets are removed as they are read.
                // If removeData is false, peek at packets only (packets flushed below).
                WintabPacket[] packets = m_wtData.GetDataPackets(m_maxPackets, removeData, ref numPkts);

                if (numPkts > 0)
                {
                    for (int idx = 0; idx < packets.Length; idx++)
                    {
                        TraceMsg(
                            "Context:" + packets[idx].pkContext +
                            " Status:" + packets[idx].pkStatus +
                            " ID:" + packets[idx].pkSerialNumber +
                            " X:" + packets[idx].pkX +
                            " Y:" + packets[idx].pkY +
                            " P:" + packets[idx].pkNormalPressure + "\n");
                        TraceMsgValid(packets[idx].pkStatus, packets[idx].pkSerialNumber, packets[idx].pkX,
                            packets[idx].pkY, packets[idx].pkNormalPressure);
                    }

                    // If the peek button was pressed, then flush the packets we just peeked at.
                    if (!removeData)
                    {
                        TraceMsg("Flushing " + numPkts.ToString() + " pending data packets...\n\n");
                        m_wtData.FlushDataPackets(numPkts);
                    }
                }
            }
            catch (Exception ex)
            {
                //System.Windows.MessageBox.Show("MyWTPacketEventHandler ERROR: " + ex.ToString());
            }
        }

        //    TraceMsgValid(packets[idx].pkStatus, packets[idx].pkSerialNumber, packets[idx].pkX, 
        //packets[idx].pkY, packets[idx].pkNormalPressure + "\n");

        private void TraceMsgValid(uint status, uint id, int x, int y, uint p)
        {
            try
            {
                //lock (MyGlobals.gLstSensor)
                //{
                //    Sensor sensor = new Sensor();
                //    sensor.ContextID = status;
                //    sensor.ID = id;
                //    sensor.X = x;
                //    sensor.Y = y;
                //    sensor.P = p;
                //    sensor.actionDate = DateTime.Now;
                //    sensor.UserID = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber;
                //    sensor.ScreenNum = MyGlobals.gTestSession.SessionSerializationIndex;
                //    sensor.ScreenType = _currentWorkingScreen.ScreenTypeID;
                //    sensor.Answer = (ans == null) ? "?" : ans;
                //    MyGlobals.gLstSensor.Add(sensor);
                //}
                if (MyGlobals.gLstSensorPage != null)
                {
                    lock (MyGlobals.gLstSensorPage)
                    {
                        Sensor sensor = new Sensor();
                        sensor.ContextID = status;
                        sensor.ID = id;
                        sensor.X = x;
                        sensor.Y = y;
                        sensor.P = p;
                        sensor.actionDate = DateTime.Now;
                        sensor.UserID = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber;
                        sensor.ScreenNum = MyGlobals.gTestSession.SessionSerializationIndex;
                        sensor.ScreenType = _currentWorkingScreen.ScreenTypeID;
                        sensor.Answer = ans;
                        if (MyGlobals.gLstSensorPage.Count == 0)
                        {
                            sensor.Ind = 1;
                        }
                        else
                        {
                            sensor.Ind = MyGlobals.gLstSensorPage[MyGlobals.gLstSensorPage.Count - 1].Ind + 1;
                        }
                        MyGlobals.gLstSensorPage.Add(sensor);
                    }
                }

            }
            catch { }
        }

        private void TraceMsg(string msg)
        {
            //sbMetrics.Append(msg + Environment.NewLine);

            //testTextBox.BeginChange();
            //if (testTextBox.Selection.Text != string.Empty)
            //{
            //    testTextBox.Selection.Text = string.Empty;
            //}
            //TextPointer tp = testTextBox.CaretPosition.GetPositionAtOffset(0, LogicalDirection.Forward);
            //testTextBox.CaretPosition.InsertTextInRun(msg);
            //testTextBox.CaretPosition = tp;
            //testTextBox.EndChange();
        }

        //private void dumpSensorDataAll()
        //{
        //    try
        //    {
        //        if ((MyGlobals.gLstSensor != null) && (MyGlobals.gLstSensor.Count > 0))
        //        {
        //            StringBuilder sb = new StringBuilder();
        //            Sensor sensor = null;
        //            sb.Append(Sensor.toCSVHeaders() + Environment.NewLine);
        //            for (int i = 0; i < MyGlobals.gLstSensor.Count; i++)
        //            {
        //                sensor = (Sensor)MyGlobals.gLstSensor[i];
        //                sb.Append(sensor.toCSV() + Environment.NewLine);
        //            }
        //            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
        //            string dirName = "SensorData";
        //            string dirPath = MyGlobals.gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + dirName;
        //            if (!System.IO.Directory.Exists(dirPath))
        //            {
        //                System.IO.Directory.CreateDirectory(dirPath);
        //            }
        //            string filename = MyGlobals.gLstSensor[0].UserID;
        //            string sensorDataFilepath = dirPath + System.IO.Path.DirectorySeparatorChar + "SensorData_" +
        //                MyGlobals.gLstSensor[0].UserID + "_" + timestamp + "_Q" + MyGlobals.gTestSession.SessionSerializationIndex + ".csv";
        //            File.WriteAllText(sensorDataFilepath, sb.ToString());
        //        }
        //    }
        //    catch { }

        //}

        private void dumpSensorPageData()
        {
            try
            {
                lock (MyGlobals.gLstSensorPage)
                {
                    if ((MyGlobals.gLstSensorPage != null) && (MyGlobals.gLstSensorPage.Count > 0))
                    {
                        StringBuilder sb = new StringBuilder();
                        Sensor sensor = null;

                        //condition the hover data
                        int tmpScreen = 0;
                        string tmpAns = null;
                        long tmpScreenType = 0;
                        for (int i = 0; i < MyGlobals.gLstSensorPage.Count; i++)
                        {
                            sensor = (Sensor)MyGlobals.gLstSensorPage[i];
                            tmpScreen = sensor.ScreenNum;
                            tmpScreenType = sensor.ScreenType;
                            tmpAns = sensor.Answer;
                        }
                        // find the tail
                        uint curTailP = 0;
                        int curTailInd = 0;
                        for (int i = MyGlobals.gLstSensorPage.Count - 1; i >= 0; i--)
                        {
                            sensor = (Sensor)MyGlobals.gLstSensorPage[i];
                            if ((sensor.P > curTailP) && (curTailP == 0))
                            {
                                curTailP = sensor.P;
                            }
                            else if (sensor.P == curTailP)
                            {
                                curTailInd = i;
                            }
                            else
                            {
                                break;
                            }
                        }

                        sb.Append(Sensor.toCSVHeaders() + Environment.NewLine);
                        for (int i = 0; i < MyGlobals.gLstSensorPage.Count; i++)
                        {
                            sensor = (Sensor)MyGlobals.gLstSensorPage[i];
                            if (sensor.ScreenNum != tmpScreen)
                            {
                                sensor.ScreenNum = tmpScreen;
                            }
                            if (sensor.P > 0)
                            {
                                sensor.Answer = tmpAns;
                            }
                            else
                            {
                                sensor.Answer = "HVR";
                            }
                            sensor.ScreenType = tmpScreenType;

                            if (i == 0)
                            {
                                //sensor.xPrevCalc = System.Math.Abs(sensor.X);
                                //sensor.yPrevCalc = System.Math.Abs(sensor.Y);
                                sensor.xPrevCalc = 0;
                                sensor.yPrevCalc = 0;
                            }
                            else
                            {
                                sensor.yPrevCalc = System.Math.Abs(MyGlobals.gLstSensorPage[i - 1].Y - sensor.Y);
                                sensor.xPrevCalc = System.Math.Abs(MyGlobals.gLstSensorPage[i - 1].X - sensor.X);
                            }
                            sensor.xPrevCalcMultItself = sensor.xPrevCalc * sensor.xPrevCalc;
                            sensor.yPrevCalcMultItself = sensor.yPrevCalc * sensor.yPrevCalc;
                            sensor.CalcMultItselfAdd = sensor.xPrevCalcMultItself + sensor.yPrevCalcMultItself;
                            sensor.sqrtCalcMultItselfAdd = System.Math.Sqrt(sensor.CalcMultItselfAdd);
                            sensor.TailPressureStartInd = curTailInd;
                            if (i > curTailInd)
                            {
                                sensor.isTailPressure = true;
                            }
                            MyGlobals.gLstSensorAll.Add(sensor);
                            sb.Append(sensor.toCSV() + Environment.NewLine);
                        }
                        string timestamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                        string dirName = "SensorData";
                        string dirPath = MyGlobals.gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + dirName;
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        string filename = MyGlobals.gLstSensorPage[0].UserID;
                        string sensorDataFilepath = dirPath + System.IO.Path.DirectorySeparatorChar + "SensorData_" +
                            MyGlobals.gLstSensorPage[0].UserID + "_" + timestamp + "_Q" + MyGlobals.gTestSession.SessionSerializationIndex + ".csv";
                        File.WriteAllText(sensorDataFilepath, sb.ToString());

                        if ((MyGlobals.gCurrentTest.lstScreen.Count - 1) == MyGlobals.gTestSession.SessionSerializationIndex)
                        {
                            dumpSensorAllData();
                            //MyGlobals.gTestSession.SerializeToLocalCsvWithIdentification(MyGlobals.gCurrentExecutableFolderPath, MyGlobals.gLstSensorAll);
                        }
                        MyGlobals.gLstSensorPage.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }

        private void dumpSensorAllData()
        {
            try
            {
                lock (MyGlobals.gLstSensorAll)
                {
                    if ((MyGlobals.gLstSensorAll != null) && (MyGlobals.gLstSensorAll.Count > 0))
                    {


                        StringBuilder sb = new StringBuilder();
                        Sensor sensor = null;

                        sb.Append(Sensor.toCSVHeaders() + Environment.NewLine);
                        for (int i = 0; i < MyGlobals.gLstSensorAll.Count; i++)
                        {
                            sensor = (Sensor)MyGlobals.gLstSensorAll[i];
                            sb.Append(sensor.toCSV() + Environment.NewLine);
                        }
                        string timestamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
                        string dirName = "SensorData";
                        string dirPath = MyGlobals.gCurrentExecutableFolderPath + System.IO.Path.DirectorySeparatorChar + dirName;
                        if (!System.IO.Directory.Exists(dirPath))
                        {
                            System.IO.Directory.CreateDirectory(dirPath);
                        }
                        string filename = MyGlobals.gLstSensorAll[0].UserID;
                        string sensorDataFilepath = dirPath + System.IO.Path.DirectorySeparatorChar + "SensorData_" +
                            MyGlobals.gLstSensorAll[0].UserID + "_" + timestamp + "_All" + ".csv";
                        File.WriteAllText(sensorDataFilepath, sb.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
            }

        }


    }


}



