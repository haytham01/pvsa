﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for StartScreen.xaml
    /// </summary>
    public partial class StartScreen : Page
    {
        public StartScreen()
        {
            InitializeComponent();
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            WelcomePage w = new WelcomePage();
            this.NavigationService.Navigate(w);
        }

        private void goButton_Click(object sender, RoutedEventArgs e)
        {
            WelcomePage w = new WelcomePage();
            this.NavigationService.Navigate(w);

        }

    }
}
