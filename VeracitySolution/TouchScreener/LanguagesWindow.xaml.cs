﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for LanguagesWindow.xaml
    /// </summary>
    public partial class LanguagesWindow : Window
    {
        public bool bOk = false;

        public LanguagesWindow()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            bOk = true;
            this.Close();
        }
    }
}
