﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for InputPage.xaml
    /// </summary>
    public partial class InputPage : Page
    {
        private System.Windows.Threading.DispatcherTimer resetTimer = new System.Windows.Threading.DispatcherTimer();

        private void resetTimer_Tick(object sender, EventArgs e)
        {
            resetTimer.Stop();
            this.NavigationService.Navigate(new WelcomePage());
        }
        private void initializeResetTimer()
        {
            resetTimer.Interval = new TimeSpan(0, 0, 0, MyGlobals.gResetTimerInSeconds, 0);
            resetTimer.Tick += new EventHandler(resetTimer_Tick);
            resetTimer.Start();
        }
        public InputPage()
        {
            InitializeComponent();
            initializeResetTimer();
        }

        private void mainKeyboard_UserKeyPressed(object sender, KeyboardTextAlpha.KeyboardEventArgs e)
        {
            this.inputTextBox.AppendText(e.KeyboardKeyPressed);
        }

        private void backspaceButton_Click(object sender, RoutedEventArgs e)
        {
            string txt = this.inputTextBox.Text;

            if (txt.Length > 0)
            {
                txt = txt.Remove(txt.Length - 1);
                this.inputTextBox.Text = txt;
            }

        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            loadData();
        }

        private void loadData(bool IsError = false)
        {
            string titleText = String.Empty;
            TextBlock tb = this.titleTextBlock;
            tb.TextWrapping = TextWrapping.Wrap;
            tb.Inlines.Clear();
            switch (MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen)
            {
                case Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_FIRST_NAME:
                    this.mainColorAnimation.From = Colors.Black;
                    this.mainColorAnimation.To = Colors.Orange;
                    if (IsError)
                    {
                        tb.Inlines.Add("YOU MUST ENTER DATA.  ");
                        this.mainColorAnimation.From = Colors.Black;
                        this.mainColorAnimation.To = Colors.Red;
                    }
                    tb.Inlines.Add("PLEASE ENTER YOUR ");
                    tb.Inlines.Add(new Run("FIRST NAME ") { FontWeight = FontWeights.Bold, FontSize = 36 });
                    tb.Inlines.Add(" AND HIT NEXT ARROW");
                    break;
                case Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_LAST_NAME:
                    this.mainColorAnimation.From = Colors.Black;
                    this.mainColorAnimation.To = Colors.Yellow;
                    if (IsError)
                    {
                        tb.Inlines.Add("YOU MUST ENTER DATA.  ");
                        this.mainColorAnimation.From = Colors.Black;
                        this.mainColorAnimation.To = Colors.Red;
                    }
                  
                    tb.Inlines.Add("PLEASE ENTER YOUR ");
                    tb.Inlines.Add(new Run("LAST NAME ") { FontWeight = FontWeights.Bold, FontSize = 40 });
                    tb.Inlines.Add(" AND HIT NEXT ARROW");
                    break;
                default:
                    break;
            }
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            string data = this.inputTextBox.Text.Trim();
            if (data.Length == 0)
            {
                loadData(true);
            }
            else
            {
                switch (MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen)
                {
                    case Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_FIRST_NAME:
                        MyGlobals.gTestSession.objTestSubject.FirstName = data;
                        
                       break;
                    case Veracity.Client.Proxy.HostConfig.ClientShellScreen.INPUT_LAST_NAME:
                        MyGlobals.gTestSession.objTestSubject.LastName = data;
                       break;
                    default:
                        break;
                }
                object nextPage = null;
                nextPage = MyGlobals.RouteToNextPage();
                this.NavigationService.Navigate(nextPage);

            
            }

        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            resetTimer.Stop();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            object prevPage = null;
            prevPage = MyGlobals.RouteToPreviousPage();
            this.NavigationService.Navigate(prevPage);
        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            object page = new AdminQuickActionsPage();
            this.NavigationService.Navigate(page);
        }
    }
}
