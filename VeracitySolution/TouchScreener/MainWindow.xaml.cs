﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

using Veracity.Client.SoftwareLocker;
using Veracity.Client.Proxy;
using System.Diagnostics;
using Veracity.Common;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {

        public MainWindow()
        {
            InitializeComponent();
            //// Get Reference to the current Process
            //Process thisProc = Process.GetCurrentProcess();
            //// Check how many total processes have the same name as the current one
            //if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            //{
            //    int numtries = 15;
            //    for (int i = 0; i < numtries; i++)
            //    {
            //        System.Threading.Thread.Sleep(1000);
            //        thisProc = Process.GetCurrentProcess();
            //        if (Process.GetProcessesByName(thisProc.ProcessName).Length == 1)
            //        {
            //            break;
            //        }
            //    }
            //    if (Process.GetProcessesByName(thisProc.ProcessName).Length > 1)
            //    {
            //        // If ther is more than one, than it is already running.
            //        MessageBox.Show("Veracity application is already running.");
            //        Application.Current.Shutdown();
            //        return;
            //    }
            //}
            init();
        }

        public void init()
        {
            // Set the current user interface culture to the specific culture
            //System.Threading.Thread.CurrentThread.CurrentUICulture =
            //            new System.Globalization.CultureInfo("es-MX");
            System.Threading.Thread.CurrentThread.CurrentUICulture =
                        new System.Globalization.CultureInfo("en");

            //#if (DEBUG)
            //            bool isOkToRun = true;
            //#else
            //            bool isOkToRun = showSecurity();
            //#endif
            bool isOkToRun = showSecurity();
            string controllerFilePath = null;

            //this.Navigate(new SelectPdfPage());
            //return;

            if (isOkToRun)
            {
                MyGlobals.gCurrentExecutablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                controllerFilePath = System.IO.Path.GetDirectoryName(MyGlobals.gCurrentExecutablePath) +
                    System.IO.Path.DirectorySeparatorChar + MyGlobals.WORKSPACE_FOLDER_NAME + System.IO.Path.DirectorySeparatorChar + MyGlobals.CONTROLLER_FILENAME;
                MyGlobals.gAppController = new Veracity.Client.Proxy.AppController();
                if (File.Exists(controllerFilePath))
                {
                    MyGlobals.SetInitialGlobals(controllerFilePath);
                    doHostConfiguration();
                    if ((MyGlobals.gAppController.lstGlobalTestLink != null) && (MyGlobals.gAppController.lstGlobalTestLink.Count > 0) && (!MyGlobals.gAppController.IsMissionEssentials))
                    {
                        int ind = 0;
                        object page = null;
                        ind = MyGlobals.gAppController.lstGlobalTestLink[0].Index;
                        MyGlobals.SetTestChoiceGlobals(ind);
                        MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.CHOICE_SCREEN;
                        page = MyGlobals.RouteToNextPage();
                        this.NavigationService.Navigate(page);
                    }
                    else
                    {
                        StartScreen startScreenPage = new StartScreen();
                        this.Navigate(startScreenPage);
                    }
                }
                else
                {
                    MessageBox.Show("Application controller file does not exist.  Consult your system administrator.");
                }
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        public void doHostConfiguration()
        {
            this.ShowsNavigationUI = MyGlobals.gAppController.lstTest[0].objHostConfig.ShowNavigationControls;
            if (MyGlobals.gAppController.lstTest[0].objHostConfig.IsKioskMode)
            {
                this.WindowState = System.Windows.WindowState.Maximized;
                this.ResizeMode = System.Windows.ResizeMode.NoResize;
                this.WindowStyle = System.Windows.WindowStyle.None;
                this.SizeToContent = System.Windows.SizeToContent.Manual;

                try
                {
                    // Disable Windows Home button on Surface Pro 3, ignore errors if hardware doesn't exist.
                    DisableHardware.DisableDevice(n => n.ToUpperInvariant().Contains("VEN_MSHW&DEV_0028"), true);
                }
                catch
                { }
            }
        }

        private bool showSecurity()
        {
            bool bIsOkToRun = true;

            try
            {
                string securityFileDirPath = "C:\\Temp";
                if (!System.IO.Directory.Exists(securityFileDirPath))
                {
                    System.IO.Directory.CreateDirectory(securityFileDirPath);
                }

                TrialMaker t = new TrialMaker("VERACITY", securityFileDirPath + System.IO.Path.DirectorySeparatorChar + ".v.reg",
                              securityFileDirPath + System.IO.Path.DirectorySeparatorChar + ".v.tra", "Email: info@pvsa.net", 120, 100, "518");

                byte[] MyOwnKey = { 97, 250, 1, 5, 84, 21, 7, 63, 4, 54, 87, 56, 123, 10, 3, 62, 7, 9, 20, 36, 37, 21, 101, 57 };

                t.TripleDESKey = MyOwnKey;

                // if you don't call this part the program will
                //use default key to encryption
                TrialMaker.RunTypes RT = t.ShowDialog();
                bool is_trial = false;

                if (RT != TrialMaker.RunTypes.Expired)
                {
                    if (RT == TrialMaker.RunTypes.Full)
                        is_trial = false;
                    else
                        is_trial = true;
                    bIsOkToRun = true;
                }

            }
            catch (Exception ex)
            {
                bIsOkToRun = false;
            }

            return bIsOkToRun;
        }

        //private void NavigationWindow_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    //Point point = this.PointToScreen(Mouse.GetPosition(this));
        //    _dtBeginAdminClick = DateTime.Now;
        //    //Point point = Mouse.GetPosition(this);
        //    //bool bIsAdminClick = isAdminScreenClick();
        //    //Console.WriteLine("Click position:  " + point.ToString() + ", width: " + this.Width + ", height:  " + this.Height + ", Admin click:  " + bIsTopRightClick);

        //}

        //private bool isAdminScreenClick()
        //{
        //    bool bValid = false;
        //    int nLowerXBound = (int)(((0.5) * this.Width) - 50);
        //    int nUpperXBound = (int)(((0.5) * this.Width) + 50);
        //    Point point = Mouse.GetPosition(this);
        //    if ((point.X >= nLowerXBound) && (point.X <= nUpperXBound) && (point.Y <= 75))
        //    {
        //        bValid = true;
        //    }
        //    return bValid;
        //}

        //private void NavigationWindow_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        //{
        //    _dtEndAdminClick = DateTime.Now;
        //    TimeSpan ts = _dtEndAdminClick - _dtBeginAdminClick;
        //    if ((ts.Seconds >= 5) && (isAdminScreenClick()))
        //    {
        //        object page = new AdminPasswordPage();
        //        this.Navigate(page);
        //        //Console.WriteLine("Admin click ...");
        //        //if (MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen == HostConfig.ClientShellScreen.SCREEN)
        //        //{
        //        //}
        //        //else
        //        //{
        //        //}
        //    }
        //}
    }
}
