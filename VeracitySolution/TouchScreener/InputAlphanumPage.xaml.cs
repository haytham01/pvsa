﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Windows.Media.Animation;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for InputAlphanumPage.xaml
    /// </summary>
    public partial class InputAlphanumPage : Page
    {
        public string titleText = String.Empty;
        public InputMode inputMode = InputMode.DRIVERLICENSE;
        private System.Windows.Threading.DispatcherTimer resetTimer = new System.Windows.Threading.DispatcherTimer();

        private DateTime _dtBeginAdminClick = new DateTime();
        private DateTime _dtEndAdminClick = new DateTime();

        private void resetTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                resetTimer.Stop();
                this.NavigationService.Navigate(new WelcomePage());
            }
            catch { }
        }
        private void initializeResetTimer()
        {
            resetTimer.Interval = new TimeSpan(0, 0, 0, MyGlobals.gResetTimerInSeconds, 0);
            resetTimer.Tick += new EventHandler(resetTimer_Tick);
            resetTimer.Start();
        }
        public enum InputMode
        {
            DRIVERLICENSE
        };
        public InputAlphanumPage()
        {
            InitializeComponent();
            initializeResetTimer();
            MyGlobals.gCurrentTest.objHostConfig.currentClientShellScreen = Veracity.Client.Proxy.HostConfig.ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this.nextButton.Visibility = System.Windows.Visibility.Hidden;

            if ((MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber != null) && (MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber.Length > 0))
            {
                titleText = "PLEASE CONFIRM YOUR DRIVER LICENSE NUMBER";
                if (MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber != null)
                {
                    this.inputTextBox.Text = MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber;
                }
            }
            else
            {
                titleText = "PLEASE ENTER YOUR DRIVER LICENSE NUMBER";
            }

            string tmp = MyGlobals.gCurrentTest.objHostConfig.getResourceValue("IDENTIFICATION_DRIVER_LICENSE_TITLE_MESSAGE");
            if (tmp != null)
            {
                titleText = tmp;
            }
           
            //this.titleTextBlock.Text = titleText;
            this.inputTextBox.Focus();
        }

        private void backspaceButton_Click(object sender, RoutedEventArgs e)
        {
            string txt = this.inputTextBox.Text;

            if (txt.Length > 0)
            {
                txt = txt.Remove(txt.Length - 1);
                this.inputTextBox.Text = txt;
            }
        }

        private void Image_ImageFailed(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void mainKeyboard_UserKeyPressed(object sender, KeyboardTextAlpha.KeyboardEventArgs e)
        {
            this.inputTextBox.AppendText(e.KeyboardKeyPressed);
        }

        private void mainNumericKeyboard_UserKeyPressed(object sender, KeyboardNumeric1.KeyboardEventArgs e)
        {
            this.inputTextBox.AppendText(e.KeyboardKeyPressed);
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            string data = this.inputTextBox.Text.Trim();
            bool isValid = true;
            bool bDoValidation = true;

            if (this.inputTextBox.Text.Trim().Length == 0)
            {
                //this.titleTextBlock.Foreground = Brushes.Red;
                //this.titleTextBlock.Text = "You Must Enter Data.  Please Try Again.";
                //this.inputTextBox.Text = String.Empty;
                return;
            }

            string tmp = MyGlobals.gCurrentTest.objHostConfig.getResourceValue("IDENTIFICATION_DRIVER_LICENSE_DO_VALIDATION");
            if (tmp != null)
            {
                Boolean.TryParse(tmp, out bDoValidation);
            }
            if (bDoValidation)
            {
                // do some validation
                isValid = (new Regex(MyGlobals.REGEX_CA_DRIVER_LICENSE)).IsMatch(data);

                if (!isValid)
                {
                    //this.titleTextBlock.Foreground = Brushes.Red;
                    //this.titleTextBlock.Text = "Your Entered Invalid Data.  Please Try Again.";
                    this.inputTextBox.Text = String.Empty;
                }
                else
                {
                    MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber = data;

                    object page = MyGlobals.RouteToNextPage();
                    this.NavigationService.Navigate(page);

                }
            }
            else
            {
                MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber = data;
                object page = MyGlobals.RouteToNextPage();
                this.NavigationService.Navigate(page);
            }
           
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            resetTimer.Stop();
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            object page = null;
            page = MyGlobals.RouteToPreviousPage();
            this.NavigationService.Navigate(page);
        }

        private void logoimage_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _dtBeginAdminClick = DateTime.Now;
        }

        private void logoimage_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            _dtEndAdminClick = DateTime.Now;
            TimeSpan ts = _dtEndAdminClick - _dtBeginAdminClick;
            //if (ts.Seconds >= 5)
            //{
            //    object page = new AdminPasswordPage();
            //    this.NavigationService.Navigate(page);
            //}

        }

        private void Image_ImageFailed_1(object sender, ExceptionRoutedEventArgs e)
        {

        }

        private void logoimage_Click(object sender, RoutedEventArgs e)
        {
            AdminPasswordPage page = new AdminPasswordPage();
            page.nextPageMode = AdminPasswordPage.NextPageMode.MANAGER;
            this.NavigationService.Navigate(page);

            //object page = new AdminQuickActionsPage();
            //this.NavigationService.Navigate(page);
        }

        private void inputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txtBox = (TextBox)sender;
            if (txtBox.Text.Length > 16)
            {
                txtBox.Text = txtBox.Text.Substring(0, 16);
            }

            if (txtBox.Text.Length >= 1)
            {
                this.nextButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.nextButton.Visibility = System.Windows.Visibility.Hidden;
            }
            //if (txtBox.Text.Length == 1)
            //{
            //    this.nextButton.BeginAnimation(Button.HeightProperty,
            //    new DoubleAnimation(25, 200, new Duration(new TimeSpan(0, 0, 1))));
            //}
        }

        private void titleTextBlock_TextInput(object sender, TextCompositionEventArgs e)
        {
           
        }
     
    }
}
