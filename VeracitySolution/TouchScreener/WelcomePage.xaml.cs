﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Resources;

using Veracity.Client.Proxy;

namespace TouchScreener
{
    /// <summary>
    /// Interaction logic for WelcomePage.xaml
    /// </summary>
    public partial class WelcomePage : Page
    {
        public bool TurnOnEnglishLanguage = false;
        public bool TurnOnSpanishMXLanguage = false;
        public bool TurnOnArabicUAELanguage = false;
        public bool TurnOnPersianAFLanguage = false;
        public bool TurnOnPashtoAFLanguage = false;
        public bool TurnOnPolishPLLanguage = false;

        public WelcomePage()
        {
            InitializeComponent();
        }
        private void _init()
        {
            foreach (GlobalLanguage g in MyGlobals.gAppController.lstGlobalLanguage)
            {
                if (g.Code == "ar-AE")
                {
                    TurnOnArabicUAELanguage = true;
                }
                if (g.Code == "en")
                {
                    TurnOnEnglishLanguage = true;
                }
                if (g.Code == "es-MX")
                {
                    TurnOnSpanishMXLanguage = true;
                }
                if (g.Code == "prs-AF")
                {
                    TurnOnPersianAFLanguage = true;
                }
                if (g.Code == "ps-AF")
                {
                    TurnOnPashtoAFLanguage = true;
                }
                if (g.Code == "pl-PL")
                {
                    TurnOnPolishPLLanguage = true;
                }
            }

            if ((!TurnOnArabicUAELanguage) && (!TurnOnEnglishLanguage) 
                && (!TurnOnSpanishMXLanguage) && (!TurnOnPersianAFLanguage)
                && (!TurnOnPashtoAFLanguage) && (!TurnOnPolishPLLanguage))
            {
                TurnOnEnglishLanguage = true;
            }

            if (TurnOnEnglishLanguage)
            {
                this.englishLangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.englishLangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (TurnOnArabicUAELanguage)
            {
                this.arabicAELangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.arabicAELangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (TurnOnSpanishMXLanguage)
            {
                this.spanishMXLangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.spanishMXLangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (TurnOnPersianAFLanguage)
            {
                this.persianAFLangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.persianAFLangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (TurnOnPashtoAFLanguage)
            {
                this.pashtoAFLangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.pashtoAFLangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

            if (TurnOnPolishPLLanguage)
            {
                this.polishPLLangButton.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                this.polishPLLangButton.Visibility = System.Windows.Visibility.Collapsed;
            }

        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            _init();
        }

        private void doStart()
        {
            ClearSensorData(ref MyGlobals.gLstSensorPage);
            ClearSensorData(ref MyGlobals.gLstSensorAll);
            MyGlobals.gLstSensorAll = new List<Sensor>();
            MyGlobals.gLstSensorPage = new List<Sensor>();
            MyGlobals.SetTestChoiceGlobalsToEmpty();
            this.NavigationService.Navigate(new ChooseTestPage());
        }
        private void ClearSensorData(ref List<Sensor> pLstSensor)
        {
            if ((pLstSensor != null) && (pLstSensor.Count > 0))
            {
                pLstSensor.Clear();
            }
          
        }
        private void englishLangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("en");
            doStart();
        }

        private void spanishMXLangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("es-MX");
             doStart();
        }

        private void arabicAELangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("ar-AE");
            doStart();
        }

        private void setLangAndSubmit(string pStrLang)
        {
            System.Threading.Thread.CurrentThread.CurrentUICulture =
            new System.Globalization.CultureInfo(pStrLang);
        }

        private void persianAFLangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("prs-AF");
            doStart();
        }

        private void pashtoAFLangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("ps-AF");
            doStart();
        }

        private void polishPLLangButton_Click(object sender, RoutedEventArgs e)
        {
            setLangAndSubmit("pl-PL");
            doStart();

        }
    }
}
