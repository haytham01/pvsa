using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  EnumClient.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumClient
	{
		private bool _hasAny = false;
		private bool _hasMore = false;
		private bool _bSetup = false;

		private SqlCommand _cmd = null;
		private SqlDataReader _rdr = null;
		private SqlConnection _conn = null;
		
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Enum-EnumClient";
		private ErrorCode _errorCode = null;
		private bool _hasError = false;
		private int _nCount = 0;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumClient"; //Table name to abstract
		private static DateTime dtNull = new DateTime();
		private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

		private long _lClientID = 0;
		private DateTime _dtBeginDateCreated = new DateTime();
		private DateTime _dtEndDateCreated = new DateTime();
		private DateTime _dtBeginDateModified = new DateTime();
		private DateTime _dtEndDateModified = new DateTime();
		private DateTime _dtBeginDateHeartbeat = new DateTime();
		private DateTime _dtEndDateHeartbeat = new DateTime();
		private string _strCode = null;
		private string _strDescription = null;
		private string _strVisibleCode = null;
		private bool? _bTestInProgress = null;
		private string _strAppMetaData = null;
//		private string _strOrderByEnum = "ASC";
		private string _strOrderByField = DB_FIELD_ID;

		/// <summary>DB_FIELD_ID Attribute type string</summary>
		public static readonly string DB_FIELD_ID = "client_id"; //Table id field name
		/// <summary>ClientID Attribute type string</summary>
		public static readonly string TAG_CLIENT_ID = "ClientID"; //Attribute ClientID  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
		/// <summary>EndDateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
		/// <summary>EndDateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
		/// <summary>DateHeartbeat Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_HEARTBEAT = "BeginDateHeartbeat"; //Attribute DateHeartbeat  name
		/// <summary>EndDateHeartbeat Attribute type string</summary>
		public static readonly string TAG_END_DATE_HEARTBEAT = "EndDateHeartbeat"; //Attribute DateHeartbeat  name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Attribute Code  name
		/// <summary>Description Attribute type string</summary>
		public static readonly string TAG_DESCRIPTION = "Description"; //Attribute Description  name
		/// <summary>VisibleCode Attribute type string</summary>
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Attribute VisibleCode  name
		/// <summary>TestInProgress Attribute type string</summary>
		public static readonly string TAG_TEST_IN_PROGRESS = "TestInProgress"; //Attribute TestInProgress  name
		/// <summary>AppMetaData Attribute type string</summary>
		public static readonly string TAG_APP_META_DATA = "AppMetaData"; //Attribute AppMetaData  name
		// Stored procedure name
		private static readonly string SP_ENUM_NAME = "spClientEnum"; //Enum sp name

		/// <summary>HasError is a Property in the Client Class of type bool</summary>
		public bool HasError 
		{
			get{return _hasError;}
			set{_hasError = value;}
		}
		/// <summary>ClientID is a Property in the Client Class of type long</summary>
		public long ClientID 
		{
			get{return _lClientID;}
			set{_lClientID = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime BeginDateCreated
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime EndDateCreated
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime BeginDateModified
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime EndDateModified
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>Property DateHeartbeat. Type: DateTime</summary>
		public DateTime BeginDateHeartbeat
		{
			get{return _dtBeginDateHeartbeat;}
			set{_dtBeginDateHeartbeat = value;}
		}
		/// <summary>Property DateHeartbeat. Type: DateTime</summary>
		public DateTime EndDateHeartbeat
		{
			get{return _dtEndDateHeartbeat;}
			set{_dtEndDateHeartbeat = value;}
		}
		/// <summary>Code is a Property in the Client Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the Client Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the Client Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>TestInProgress is a Property in the Client Class of type bool</summary>
		public bool? TestInProgress 
		{
			get{return _bTestInProgress;}
			set{_bTestInProgress = value;}
		}
		/// <summary>AppMetaData is a Property in the Client Class of type String</summary>
		public string AppMetaData 
		{
			get{return _strAppMetaData;}
			set{_strAppMetaData = value;}
		}

		/// <summary>Count Property. Type: int</summary>
		public int Count 
		{
			get
			{
				_bSetup = true;
				_log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_setupCountParams();
				_cmd.Connection = _conn;
				_cmd.ExecuteNonQuery();
				try
				{
					string strTmp;
					strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
					_nCount = int.Parse(strTmp);
				}
				catch 
				{
					_nCount = 0;
				}
				return _nCount;			}
		}

		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumClient()
		{
		}
		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumClient(SqlConnection conn)
		{
			_conn = conn;
		}

		/// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
		public EnumClient(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

		// Implementation of IEnumerator
		/// <summary>Property of type Client. Returns the next Client in the list</summary>
		private Client _nextTransaction
		{
			get
			{
				Client o = null;
				
				if (!_bSetup)
				{
					EnumData();
				}
				if (_hasMore)
				{
					o = new Client(_rdr);
					_hasMore = _rdr.Read();
					if (!_hasMore)
					{
						Close();
					}
				}
				return o;
			}
		}

		/// <summary>Enumerates the Data</summary>
		public void EnumData()
		{
			if (!_bSetup)
			{
				_bSetup = true;
				_log("ENUM", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_cmd.Connection = _conn;
				_rdr = _cmd.ExecuteReader();
				_hasAny = _rdr.Read();
				_hasMore = _hasAny;
			}
		}


		/// <summary>returns the next element in the enumeration</summary>
		public object nextElement()
		{
			try
			{
				return _nextTransaction;
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
				return null;
			}
		}

		/// <summary>Returns whether or not more elements exist</summary>
		public bool hasMoreElements()
		{
			try
			{
				if (_bSetup)
				{
					EnumData();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return _hasMore;
		}

		/// <summary>Closes the datareader</summary>
		public void Close()
		{
			try
			{
				if ( _rdr != null )
				{
					_rdr.Close();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
			_rdr = null;
			_cmd = null;
		}

		/// <summary>ToString is overridden to display all properties of the Client Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_CLIENT_ID + ":  " + ClientID.ToString() + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(BeginDateHeartbeat))
			{
				sbReturn.Append(TAG_BEGIN_DATE_HEARTBEAT + ":  " + BeginDateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_HEARTBEAT + ":\n");
			}
			if (!dtNull.Equals(EndDateHeartbeat))
			{
				sbReturn.Append(TAG_END_DATE_HEARTBEAT + ":  " + EndDateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_HEARTBEAT + ":\n");
			}
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TEST_IN_PROGRESS + ":  " + TestInProgress + "\n");
			sbReturn.Append(TAG_APP_META_DATA + ":  " + AppMetaData + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Client</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_CLIENT_ID + ">" + ClientID + "</" + TAG_CLIENT_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(BeginDateHeartbeat))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_HEARTBEAT + ">" + BeginDateHeartbeat.ToString() + "</" + TAG_BEGIN_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_HEARTBEAT + "></" + TAG_BEGIN_DATE_HEARTBEAT + ">\n");
			}
			if (!dtNull.Equals(EndDateHeartbeat))
			{
				sbReturn.Append("<" + TAG_END_DATE_HEARTBEAT + ">" + EndDateHeartbeat.ToString() + "</" + TAG_END_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_HEARTBEAT + "></" + TAG_END_DATE_HEARTBEAT + ">\n");
			}
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TEST_IN_PROGRESS + ">" + TestInProgress + "</" + TAG_TEST_IN_PROGRESS + ">\n");
			sbReturn.Append("<" + TAG_APP_META_DATA + ">" + AppMetaData + "</" + TAG_APP_META_DATA + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse XML string and assign values to object</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				if ( xNodes.Count > 0 )
				{
					Parse(xNodes.Item(0));
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CLIENT_ID);
				strTmp = xResultNode.InnerText;
				ClientID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_HEARTBEAT);
				BeginDateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_HEARTBEAT);
				EndDateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
				if (Code.Trim().Length == 0)
					Code = null;
			}
			catch  
			{
				Code = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
				if (Description.Trim().Length == 0)
					Description = null;
			}
			catch  
			{
				Description = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
				if (VisibleCode.Trim().Length == 0)
					VisibleCode = null;
			}
			catch  
			{
				VisibleCode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_IN_PROGRESS);
				TestInProgress = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			TestInProgress = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_APP_META_DATA);
				AppMetaData = xResultNode.InnerText;
				if (AppMetaData.Trim().Length == 0)
					AppMetaData = null;
			}
			catch  
			{
				AppMetaData = null;
			}
		}
		/// <summary>Prompt for values</summary>
		public void Prompt()
		{
			try 
			{
				Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateCreated = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateCreated = DateTime.Parse(s);
				}
				catch  
				{
					EndDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateModified = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateModified = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateModified = DateTime.Parse(s);
				}
				catch  
				{
					EndDateModified = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_HEARTBEAT + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateHeartbeat = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateHeartbeat = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_HEARTBEAT + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateHeartbeat = DateTime.Parse(s);
				}
				catch  
				{
					EndDateHeartbeat = new DateTime();
				}


				Console.WriteLine(TAG_CODE + ":  ");
				Code = Console.ReadLine();
				if (Code.Length == 0)
				{
					Code = null;
				}

				Console.WriteLine(TAG_DESCRIPTION + ":  ");
				Description = Console.ReadLine();
				if (Description.Length == 0)
				{
					Description = null;
				}

				Console.WriteLine(TAG_VISIBLE_CODE + ":  ");
				VisibleCode = Console.ReadLine();
				if (VisibleCode.Length == 0)
				{
					VisibleCode = null;
				}
				Console.WriteLine(TAG_TEST_IN_PROGRESS + ":  ");
				try
				{
					TestInProgress = Convert.ToBoolean(Console.ReadLine());
				}
				catch 
				{
					TestInProgress = false;
				}


				Console.WriteLine(TAG_APP_META_DATA + ":  ");
				AppMetaData = Console.ReadLine();
				if (AppMetaData.Length == 0)
				{
					AppMetaData = null;
				}

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}

		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}
		private void _setupCountParams()
		{
			SqlParameter paramCount = null;
			paramCount = new SqlParameter();
			paramCount.ParameterName = PARAM_COUNT;
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			_cmd.Parameters.Add(paramCount);
		}
		private void _setupEnumParams()
		{
			System.Text.StringBuilder sbLog = null;
			SqlParameter paramClientID = null;
			SqlParameter paramBeginDateCreated = null;
			SqlParameter paramEndDateCreated = null;
			SqlParameter paramBeginDateModified = null;
			SqlParameter paramEndDateModified = null;
			SqlParameter paramBeginDateHeartbeat = null;
			SqlParameter paramEndDateHeartbeat = null;
			SqlParameter paramCode = null;
			SqlParameter paramDescription = null;
			SqlParameter paramVisibleCode = null;
			SqlParameter paramTestInProgress = null;
			SqlParameter paramAppMetaData = null;
			DateTime dtNull = new DateTime();

			sbLog = new System.Text.StringBuilder();
				paramClientID = new SqlParameter("@" + TAG_CLIENT_ID, ClientID);
				sbLog.Append(TAG_CLIENT_ID + "=" + ClientID + "\n");
				paramClientID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramClientID);

			// Setup the date created param
			if (!dtNull.Equals(BeginDateCreated))
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
				sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
			}
			else
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
			}
			paramBeginDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateCreated);

			if (!dtNull.Equals(EndDateCreated))
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
				sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
			}
			else
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
			}
			paramEndDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateCreated);

			// Setup the date modified param
			if (!dtNull.Equals(BeginDateModified))
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
				sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
			}
			else
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
			}
			paramBeginDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateModified);

			if (!dtNull.Equals(EndDateModified))
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
				sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
			}
			else
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
			}
			paramEndDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateModified);

			// Setup the date heartbeat param
			if (!dtNull.Equals(BeginDateHeartbeat))
			{
				paramBeginDateHeartbeat = new SqlParameter("@" + TAG_BEGIN_DATE_HEARTBEAT, BeginDateHeartbeat);
				sbLog.Append(TAG_BEGIN_DATE_HEARTBEAT + "=" + BeginDateHeartbeat.ToLongDateString());
			}
			else
			{
				paramBeginDateHeartbeat = new SqlParameter("@" + TAG_BEGIN_DATE_HEARTBEAT, DBNull.Value);
			}
			paramBeginDateHeartbeat.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateHeartbeat);

			if (!dtNull.Equals(EndDateHeartbeat))
			{
				paramEndDateHeartbeat = new SqlParameter("@" + TAG_END_DATE_HEARTBEAT, EndDateHeartbeat);
				sbLog.Append(TAG_END_DATE_HEARTBEAT + "=" + EndDateHeartbeat.ToLongDateString());
			}
			else
			{
				paramEndDateHeartbeat = new SqlParameter("@" + TAG_END_DATE_HEARTBEAT, DBNull.Value);
			}
			paramEndDateHeartbeat.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateHeartbeat);

			// Setup the code text param
			if ( Code != null )
			{
				paramCode = new SqlParameter("@" + TAG_CODE, Code);
				sbLog.Append(TAG_CODE + "=" + Code + "\n");
			}
			else
			{
				paramCode = new SqlParameter("@" + TAG_CODE, DBNull.Value);
			}
			paramCode.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramCode);

			// Setup the description text param
			if ( Description != null )
			{
				paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, Description);
				sbLog.Append(TAG_DESCRIPTION + "=" + Description + "\n");
			}
			else
			{
				paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, DBNull.Value);
			}
			paramDescription.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramDescription);

			// Setup the visible code text param
			if ( VisibleCode != null )
			{
				paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, VisibleCode);
				sbLog.Append(TAG_VISIBLE_CODE + "=" + VisibleCode + "\n");
			}
			else
			{
				paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, DBNull.Value);
			}
			paramVisibleCode.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramVisibleCode);

				paramTestInProgress = new SqlParameter("@" + TAG_TEST_IN_PROGRESS, TestInProgress);
				sbLog.Append(TAG_TEST_IN_PROGRESS + "=" + TestInProgress + "\n");
				paramTestInProgress.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestInProgress);
			// Setup the app meta data text param
			if ( AppMetaData != null )
			{
				paramAppMetaData = new SqlParameter("@" + TAG_APP_META_DATA, AppMetaData);
				sbLog.Append(TAG_APP_META_DATA + "=" + AppMetaData + "\n");
			}
			else
			{
				paramAppMetaData = new SqlParameter("@" + TAG_APP_META_DATA, DBNull.Value);
			}
			paramAppMetaData.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramAppMetaData);

				_log("ENUM PARAMS", sbLog.ToString());
		}

		//private
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}
	}
}

