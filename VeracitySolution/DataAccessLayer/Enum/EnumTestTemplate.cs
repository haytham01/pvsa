using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  EnumTestTemplate.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/7/2013	Created
    /// 
    /// ----------------------------------------------------
    /// </summary>
    public class EnumTestTemplate
    {
        private bool _hasAny = false;
        private bool _hasMore = false;
        private bool _bSetup = false;

        private SqlCommand _cmd = null;
        private SqlDataReader _rdr = null;
        private SqlConnection _conn = null;

        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Enum-EnumTestTemplate";
        private ErrorCode _errorCode = null;
        private bool _hasError = false;
        private int _nCount = 0;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "EnumTestTemplate"; //Table name to abstract
        private static DateTime dtNull = new DateTime();
        private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

        private long _lTestTemplateID = 0;
        private long _lTestTypeID = 0;
        private bool? _bDoPrintReport = null;
        private bool? _bDoLicenseIdentification = null;
        private bool? _bDoNameIdentification = null;
        private string _strPrintTemplateName = null;
        private bool? _bIsKioskMode = null;
        private bool? _bIsSynchronized = null;
        private string _strAdminPassword = null;
        private string _strManagerPassword = null;
        private long _lResetInSeconds = 0;
        private DateTime _dtBeginDateCreated = new DateTime();
        private DateTime _dtEndDateCreated = new DateTime();
        private DateTime _dtBeginDateModified = new DateTime();
        private DateTime _dtEndDateModified = new DateTime();
        //		private string _strOrderByEnum = "ASC";
        private string _strOrderByField = DB_FIELD_ID;

        /// <summary>DB_FIELD_ID Attribute type string</summary>
        public static readonly string DB_FIELD_ID = "test_template_id"; //Table id field name
        /// <summary>TestTemplateID Attribute type string</summary>
        public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Attribute TestTemplateID  name
        /// <summary>TestTypeID Attribute type string</summary>
        public static readonly string TAG_TEST_TYPE_ID = "TestTypeID"; //Attribute TestTypeID  name
        /// <summary>DoPrintReport Attribute type string</summary>
        public static readonly string TAG_DO_PRINT_REPORT = "DoPrintReport"; //Attribute DoPrintReport  name
        /// <summary>DoLicenseIdentification Attribute type string</summary>
        public static readonly string TAG_DO_LICENSE_IDENTIFICATION = "DoLicenseIdentification"; //Attribute DoLicenseIdentification  name
        /// <summary>DoNameIdentification Attribute type string</summary>
        public static readonly string TAG_DO_NAME_IDENTIFICATION = "DoNameIdentification"; //Attribute DoNameIdentification  name
        /// <summary>PrintTemplateName Attribute type string</summary>
        public static readonly string TAG_PRINT_TEMPLATE_NAME = "PrintTemplateName"; //Attribute PrintTemplateName  name
        /// <summary>IsKioskMode Attribute type string</summary>
        public static readonly string TAG_IS_KIOSK_MODE = "IsKioskMode"; //Attribute IsKioskMode  name
        /// <summary>IsSynchronized Attribute type string</summary>
        public static readonly string TAG_IS_SYNCHRONIZED = "IsSynchronized"; //Attribute IsSynchronized  name
        /// <summary>AdminPassword Attribute type string</summary>
        public static readonly string TAG_ADMIN_PASSWORD = "AdminPassword"; //Attribute AdminPassword  name
        /// <summary>ManagerPassword Attribute type string</summary>
        public static readonly string TAG_MANAGER_PASSWORD = "ManagerPassword"; //Attribute ManagerPassword  name
        /// <summary>ResetInSeconds Attribute type string</summary>
        public static readonly string TAG_RESET_IN_SECONDS = "ResetInSeconds"; //Attribute ResetInSeconds  name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
        /// <summary>EndDateCreated Attribute type string</summary>
        public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
        /// <summary>EndDateModified Attribute type string</summary>
        public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
        // Stored procedure name
        private static readonly string SP_ENUM_NAME = "spTestTemplateEnum"; //Enum sp name

        /// <summary>HasError is a Property in the TestTemplate Class of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        /// <summary>TestTemplateID is a Property in the TestTemplate Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>TestTypeID is a Property in the TestTemplate Class of type long</summary>
        public long TestTypeID
        {
            get { return _lTestTypeID; }
            set { _lTestTypeID = value; }
        }
        /// <summary>DoPrintReport is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoPrintReport
        {
            get { return _bDoPrintReport; }
            set { _bDoPrintReport = value; }
        }
        /// <summary>DoLicenseIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoLicenseIdentification
        {
            get { return _bDoLicenseIdentification; }
            set { _bDoLicenseIdentification = value; }
        }
        /// <summary>DoNameIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoNameIdentification
        {
            get { return _bDoNameIdentification; }
            set { _bDoNameIdentification = value; }
        }
        /// <summary>PrintTemplateName is a Property in the TestTemplate Class of type String</summary>
        public string PrintTemplateName
        {
            get { return _strPrintTemplateName; }
            set { _strPrintTemplateName = value; }
        }
        /// <summary>IsKioskMode is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsKioskMode
        {
            get { return _bIsKioskMode; }
            set { _bIsKioskMode = value; }
        }
        /// <summary>IsSynchronized is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsSynchronized
        {
            get { return _bIsSynchronized; }
            set { _bIsSynchronized = value; }
        }
        /// <summary>AdminPassword is a Property in the TestTemplate Class of type String</summary>
        public string AdminPassword
        {
            get { return _strAdminPassword; }
            set { _strAdminPassword = value; }
        }
        /// <summary>ManagerPassword is a Property in the TestTemplate Class of type String</summary>
        public string ManagerPassword
        {
            get { return _strManagerPassword; }
            set { _strManagerPassword = value; }
        }
        /// <summary>ResetInSeconds is a Property in the TestTemplate Class of type long</summary>
        public long ResetInSeconds
        {
            get { return _lResetInSeconds; }
            set { _lResetInSeconds = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime BeginDateCreated
        {
            get { return _dtBeginDateCreated; }
            set { _dtBeginDateCreated = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime EndDateCreated
        {
            get { return _dtEndDateCreated; }
            set { _dtEndDateCreated = value; }
        }
        /// <summary>Property DateModified. Type: DateTime</summary>
        public DateTime BeginDateModified
        {
            get { return _dtBeginDateModified; }
            set { _dtBeginDateModified = value; }
        }
        /// <summary>Property DateModified. Type: DateTime</summary>
        public DateTime EndDateModified
        {
            get { return _dtEndDateModified; }
            set { _dtEndDateModified = value; }
        }

        /// <summary>Count Property. Type: int</summary>
        public int Count
        {
            get
            {
                _bSetup = true;
                _log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _setupCountParams();
                _cmd.Connection = _conn;
                _cmd.ExecuteNonQuery();
                try
                {
                    string strTmp;
                    strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
                    _nCount = int.Parse(strTmp);
                }
                catch
                {
                    _nCount = 0;
                }
                return _nCount;
            }
        }

        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumTestTemplate()
        {
        }
        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumTestTemplate(SqlConnection conn)
        {
            _conn = conn;
        }

        /// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
        public EnumTestTemplate(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        // Implementation of IEnumerator
        /// <summary>Property of type TestTemplate. Returns the next TestTemplate in the list</summary>
        private TestTemplate _nextTransaction
        {
            get
            {
                TestTemplate o = null;

                if (!_bSetup)
                {
                    EnumData();
                }
                if (_hasMore)
                {
                    o = new TestTemplate(_rdr);
                    _hasMore = _rdr.Read();
                    if (!_hasMore)
                    {
                        Close();
                    }
                }
                return o;
            }
        }

        /// <summary>Enumerates the Data</summary>
        public void EnumData()
        {
            if (!_bSetup)
            {
                _bSetup = true;
                _log("ENUM", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _cmd.Connection = _conn;
                _rdr = _cmd.ExecuteReader();
                _hasAny = _rdr.Read();
                _hasMore = _hasAny;
            }
        }


        /// <summary>returns the next element in the enumeration</summary>
        public object nextElement()
        {
            try
            {
                return _nextTransaction;
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
                return null;
            }
        }

        /// <summary>Returns whether or not more elements exist</summary>
        public bool hasMoreElements()
        {
            try
            {
                if (_bSetup)
                {
                    EnumData();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return _hasMore;
        }

        /// <summary>Closes the datareader</summary>
        public void Close()
        {
            try
            {
                if (_rdr != null)
                {
                    _rdr.Close();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
            _rdr = null;
            _cmd = null;
        }

        /// <summary>ToString is overridden to display all properties of the TestTemplate Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TYPE_ID + ":  " + TestTypeID + "\n");
            sbReturn.Append(TAG_DO_PRINT_REPORT + ":  " + DoPrintReport + "\n");
            sbReturn.Append(TAG_DO_LICENSE_IDENTIFICATION + ":  " + DoLicenseIdentification + "\n");
            sbReturn.Append(TAG_DO_NAME_IDENTIFICATION + ":  " + DoNameIdentification + "\n");
            sbReturn.Append(TAG_PRINT_TEMPLATE_NAME + ":  " + PrintTemplateName + "\n");
            sbReturn.Append(TAG_IS_KIOSK_MODE + ":  " + IsKioskMode + "\n");
            sbReturn.Append(TAG_IS_SYNCHRONIZED + ":  " + IsSynchronized + "\n");
            sbReturn.Append(TAG_ADMIN_PASSWORD + ":  " + AdminPassword + "\n");
            sbReturn.Append(TAG_MANAGER_PASSWORD + ":  " + ManagerPassword + "\n");
            sbReturn.Append(TAG_RESET_IN_SECONDS + ":  " + ResetInSeconds + "\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(BeginDateModified))
            {
                sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
            }
            if (!dtNull.Equals(EndDateModified))
            {
                sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
            }

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of TestTemplate</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TYPE_ID + ">" + TestTypeID + "</" + TAG_TEST_TYPE_ID + ">\n");
            sbReturn.Append("<" + TAG_DO_PRINT_REPORT + ">" + DoPrintReport + "</" + TAG_DO_PRINT_REPORT + ">\n");
            sbReturn.Append("<" + TAG_DO_LICENSE_IDENTIFICATION + ">" + DoLicenseIdentification + "</" + TAG_DO_LICENSE_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_DO_NAME_IDENTIFICATION + ">" + DoNameIdentification + "</" + TAG_DO_NAME_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_PRINT_TEMPLATE_NAME + ">" + PrintTemplateName + "</" + TAG_PRINT_TEMPLATE_NAME + ">\n");
            sbReturn.Append("<" + TAG_IS_KIOSK_MODE + ">" + IsKioskMode + "</" + TAG_IS_KIOSK_MODE + ">\n");
            sbReturn.Append("<" + TAG_IS_SYNCHRONIZED + ">" + IsSynchronized + "</" + TAG_IS_SYNCHRONIZED + ">\n");
            sbReturn.Append("<" + TAG_ADMIN_PASSWORD + ">" + AdminPassword + "</" + TAG_ADMIN_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_MANAGER_PASSWORD + ">" + ManagerPassword + "</" + TAG_MANAGER_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_RESET_IN_SECONDS + ">" + ResetInSeconds + "</" + TAG_RESET_IN_SECONDS + ">\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(BeginDateModified))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
            }
            if (!dtNull.Equals(EndDateModified))
            {
                sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse XML string and assign values to object</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                if (xNodes.Count > 0)
                {
                    Parse(xNodes.Item(0));
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
                strTmp = xResultNode.InnerText;
                TestTemplateID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TYPE_ID);
                TestTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_PRINT_REPORT);
                DoPrintReport = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoPrintReport = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_LICENSE_IDENTIFICATION);
                DoLicenseIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoLicenseIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_NAME_IDENTIFICATION);
                DoNameIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoNameIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_TEMPLATE_NAME);
                PrintTemplateName = xResultNode.InnerText;
                if (PrintTemplateName.Trim().Length == 0)
                    PrintTemplateName = null;
            }
            catch
            {
                PrintTemplateName = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_KIOSK_MODE);
                IsKioskMode = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsKioskMode = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_SYNCHRONIZED);
                IsSynchronized = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsSynchronized = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ADMIN_PASSWORD);
                AdminPassword = xResultNode.InnerText;
                if (AdminPassword.Trim().Length == 0)
                    AdminPassword = null;
            }
            catch
            {
                AdminPassword = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_MANAGER_PASSWORD);
                ManagerPassword = xResultNode.InnerText;
                if (ManagerPassword.Trim().Length == 0)
                    ManagerPassword = null;
            }
            catch
            {
                ManagerPassword = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_RESET_IN_SECONDS);
                ResetInSeconds = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ResetInSeconds = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
                BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
                EndDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
                BeginDateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
                EndDateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }
        }
        /// <summary>Prompt for values</summary>
        public void Prompt()
        {
            try
            {
                Console.WriteLine(TAG_TEST_TYPE_ID + ":  ");
                try
                {
                    TestTypeID = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    TestTypeID = 0;
                }

                Console.WriteLine(TAG_DO_PRINT_REPORT + ":  ");
                try
                {
                    DoPrintReport = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    DoPrintReport = false;
                }

                Console.WriteLine(TAG_DO_LICENSE_IDENTIFICATION + ":  ");
                try
                {
                    DoLicenseIdentification = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    DoLicenseIdentification = false;
                }

                Console.WriteLine(TAG_DO_NAME_IDENTIFICATION + ":  ");
                try
                {
                    DoNameIdentification = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    DoNameIdentification = false;
                }


                Console.WriteLine(TAG_PRINT_TEMPLATE_NAME + ":  ");
                PrintTemplateName = Console.ReadLine();
                if (PrintTemplateName.Length == 0)
                {
                    PrintTemplateName = null;
                }
                Console.WriteLine(TAG_IS_KIOSK_MODE + ":  ");
                try
                {
                    IsKioskMode = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsKioskMode = false;
                }

                Console.WriteLine(TAG_IS_SYNCHRONIZED + ":  ");
                try
                {
                    IsSynchronized = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsSynchronized = false;
                }


                Console.WriteLine(TAG_ADMIN_PASSWORD + ":  ");
                AdminPassword = Console.ReadLine();
                if (AdminPassword.Length == 0)
                {
                    AdminPassword = null;
                }

                Console.WriteLine(TAG_MANAGER_PASSWORD + ":  ");
                ManagerPassword = Console.ReadLine();
                if (ManagerPassword.Length == 0)
                {
                    ManagerPassword = null;
                }
                Console.WriteLine(TAG_RESET_IN_SECONDS + ":  ");
                try
                {
                    ResetInSeconds = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    ResetInSeconds = 0;
                }

                Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    BeginDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    BeginDateCreated = new DateTime();
                }

                Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    EndDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    EndDateCreated = new DateTime();
                }

                Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    BeginDateModified = DateTime.Parse(s);
                }
                catch
                {
                    BeginDateModified = new DateTime();
                }

                Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    EndDateModified = DateTime.Parse(s);
                }
                catch
                {
                    EndDateModified = new DateTime();
                }


            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }
        private void _setupCountParams()
        {
            SqlParameter paramCount = null;
            paramCount = new SqlParameter();
            paramCount.ParameterName = PARAM_COUNT;
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            _cmd.Parameters.Add(paramCount);
        }
        private void _setupEnumParams()
        {
            System.Text.StringBuilder sbLog = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramTestTypeID = null;
            SqlParameter paramDoPrintReport = null;
            SqlParameter paramDoLicenseIdentification = null;
            SqlParameter paramDoNameIdentification = null;
            SqlParameter paramPrintTemplateName = null;
            SqlParameter paramIsKioskMode = null;
            SqlParameter paramIsSynchronized = null;
            SqlParameter paramAdminPassword = null;
            SqlParameter paramManagerPassword = null;
            SqlParameter paramResetInSeconds = null;
            SqlParameter paramBeginDateCreated = null;
            SqlParameter paramEndDateCreated = null;
            SqlParameter paramBeginDateModified = null;
            SqlParameter paramEndDateModified = null;
            DateTime dtNull = new DateTime();

            sbLog = new System.Text.StringBuilder();
            paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
            sbLog.Append(TAG_TEST_TEMPLATE_ID + "=" + TestTemplateID + "\n");
            paramTestTemplateID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramTestTemplateID);

            paramTestTypeID = new SqlParameter("@" + TAG_TEST_TYPE_ID, TestTypeID);
            sbLog.Append(TAG_TEST_TYPE_ID + "=" + TestTypeID + "\n");
            paramTestTypeID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramTestTypeID);
            paramDoPrintReport = new SqlParameter("@" + TAG_DO_PRINT_REPORT, DoPrintReport);
            sbLog.Append(TAG_DO_PRINT_REPORT + "=" + DoPrintReport + "\n");
            paramDoPrintReport.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramDoPrintReport);
            paramDoLicenseIdentification = new SqlParameter("@" + TAG_DO_LICENSE_IDENTIFICATION, DoLicenseIdentification);
            sbLog.Append(TAG_DO_LICENSE_IDENTIFICATION + "=" + DoLicenseIdentification + "\n");
            paramDoLicenseIdentification.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramDoLicenseIdentification);
            paramDoNameIdentification = new SqlParameter("@" + TAG_DO_NAME_IDENTIFICATION, DoNameIdentification);
            sbLog.Append(TAG_DO_NAME_IDENTIFICATION + "=" + DoNameIdentification + "\n");
            paramDoNameIdentification.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramDoNameIdentification);
            // Setup the print template name text param
            if (PrintTemplateName != null)
            {
                paramPrintTemplateName = new SqlParameter("@" + TAG_PRINT_TEMPLATE_NAME, PrintTemplateName);
                sbLog.Append(TAG_PRINT_TEMPLATE_NAME + "=" + PrintTemplateName + "\n");
            }
            else
            {
                paramPrintTemplateName = new SqlParameter("@" + TAG_PRINT_TEMPLATE_NAME, DBNull.Value);
            }
            paramPrintTemplateName.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramPrintTemplateName);

            paramIsKioskMode = new SqlParameter("@" + TAG_IS_KIOSK_MODE, IsKioskMode);
            sbLog.Append(TAG_IS_KIOSK_MODE + "=" + IsKioskMode + "\n");
            paramIsKioskMode.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsKioskMode);
            paramIsSynchronized = new SqlParameter("@" + TAG_IS_SYNCHRONIZED, IsSynchronized);
            sbLog.Append(TAG_IS_SYNCHRONIZED + "=" + IsSynchronized + "\n");
            paramIsSynchronized.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsSynchronized);
            // Setup the admin password text param
            if (AdminPassword != null)
            {
                paramAdminPassword = new SqlParameter("@" + TAG_ADMIN_PASSWORD, AdminPassword);
                sbLog.Append(TAG_ADMIN_PASSWORD + "=" + AdminPassword + "\n");
            }
            else
            {
                paramAdminPassword = new SqlParameter("@" + TAG_ADMIN_PASSWORD, DBNull.Value);
            }
            paramAdminPassword.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramAdminPassword);

            // Setup the manager password text param
            if (ManagerPassword != null)
            {
                paramManagerPassword = new SqlParameter("@" + TAG_MANAGER_PASSWORD, ManagerPassword);
                sbLog.Append(TAG_MANAGER_PASSWORD + "=" + ManagerPassword + "\n");
            }
            else
            {
                paramManagerPassword = new SqlParameter("@" + TAG_MANAGER_PASSWORD, DBNull.Value);
            }
            paramManagerPassword.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramManagerPassword);

            paramResetInSeconds = new SqlParameter("@" + TAG_RESET_IN_SECONDS, ResetInSeconds);
            sbLog.Append(TAG_RESET_IN_SECONDS + "=" + ResetInSeconds + "\n");
            paramResetInSeconds.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramResetInSeconds);

            // Setup the date created param
            if (!dtNull.Equals(BeginDateCreated))
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
                sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
            }
            else
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
            }
            paramBeginDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramBeginDateCreated);

            if (!dtNull.Equals(EndDateCreated))
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
                sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
            }
            else
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
            }
            paramEndDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramEndDateCreated);

            // Setup the date modified param
            if (!dtNull.Equals(BeginDateModified))
            {
                paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
                sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
            }
            else
            {
                paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
            }
            paramBeginDateModified.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramBeginDateModified);

            if (!dtNull.Equals(EndDateModified))
            {
                paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
                sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
            }
            else
            {
                paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
            }
            paramEndDateModified.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramEndDateModified);

            _log("ENUM PARAMS", sbLog.ToString());
        }

        //private
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }
    }
}
