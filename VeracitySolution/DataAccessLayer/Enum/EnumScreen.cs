using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  EnumScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/25/2013	Created
    /// 
    /// ----------------------------------------------------
    /// </summary>
    public class EnumScreen
    {
        private bool _hasAny = false;
        private bool _hasMore = false;
        private bool _bSetup = false;

        private SqlCommand _cmd = null;
        private SqlDataReader _rdr = null;
        private SqlConnection _conn = null;

        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Enum-EnumScreen";
        private ErrorCode _errorCode = null;
        private bool _hasError = false;
        private int _nCount = 0;


        /*********************** CUSTOM NON-META BEGIN *********************/
        private void _setupEnumParams()
        {
            System.Text.StringBuilder sbLog = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramScreenTypeID = null;
            SqlParameter paramBeginDateCreated = null;
            SqlParameter paramEndDateCreated = null;
            SqlParameter paramScreenText = null;
            SqlParameter paramScreenImage = null;
            SqlParameter paramAudioData = null;
            SqlParameter paramIsText = null;
            SqlParameter paramIsAudio = null;
            SqlParameter paramScreenTextShort = null;
            DateTime dtNull = new DateTime();

            sbLog = new System.Text.StringBuilder();
            paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
            sbLog.Append(TAG_SCREEN_ID + "=" + ScreenID + "\n");
            paramScreenID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramScreenID);

            paramScreenTypeID = new SqlParameter("@" + TAG_SCREEN_TYPE_ID, ScreenTypeID);
            sbLog.Append(TAG_SCREEN_TYPE_ID + "=" + ScreenTypeID + "\n");
            paramScreenTypeID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramScreenTypeID);
            // Setup the date created param
            if (!dtNull.Equals(BeginDateCreated))
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
                sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
            }
            else
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
            }
            paramBeginDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramBeginDateCreated);

            if (!dtNull.Equals(EndDateCreated))
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
                sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
            }
            else
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
            }
            paramEndDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramEndDateCreated);

            // Setup the screen text text param
            if (ScreenText != null)
            {
                paramScreenText = new SqlParameter("@" + TAG_SCREEN_TEXT, ScreenText);
                sbLog.Append(TAG_SCREEN_TEXT + "=" + ScreenText + "\n");
            }
            else
            {
                paramScreenText = new SqlParameter("@" + TAG_SCREEN_TEXT, DBNull.Value);
            }
            paramScreenText.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramScreenText);

            //// Setup the screen image text param
            //if (ScreenImage != null)
            //{
            //    paramScreenImage = new SqlParameter("@" + TAG_SCREEN_IMAGE, ScreenImage);
            //    sbLog.Append(TAG_SCREEN_IMAGE + "=" + ScreenImage + "\n");
            //}
            //else
            //{
            //    paramScreenImage = new SqlParameter("@" + TAG_SCREEN_IMAGE, DBNull.Value);
            //}
            //paramScreenImage.Direction = ParameterDirection.Input;
            //_cmd.Parameters.Add(paramScreenImage);

            //// Setup the audio data text param
            //if (AudioData != null)
            //{
            //    paramAudioData = new SqlParameter("@" + TAG_AUDIO_DATA, AudioData);
            //    sbLog.Append(TAG_AUDIO_DATA + "=" + AudioData + "\n");
            //}
            //else
            //{
            //    paramAudioData = new SqlParameter("@" + TAG_AUDIO_DATA, DBNull.Value);
            //}
            //paramAudioData.Direction = ParameterDirection.Input;
            //_cmd.Parameters.Add(paramAudioData);

            paramIsText = new SqlParameter("@" + TAG_IS_TEXT, IsText);
            sbLog.Append(TAG_IS_TEXT + "=" + IsText + "\n");
            paramIsText.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsText);
            paramIsAudio = new SqlParameter("@" + TAG_IS_AUDIO, IsAudio);
            sbLog.Append(TAG_IS_AUDIO + "=" + IsAudio + "\n");
            paramIsAudio.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsAudio);

            // Setup the screen text short text param
            if (ScreenTextShort != null)
            {
                paramScreenTextShort = new SqlParameter("@" + TAG_SCREEN_TEXT_SHORT, ScreenTextShort);
                sbLog.Append(TAG_SCREEN_TEXT_SHORT + "=" + ScreenTextShort + "\n");
            }
            else
            {
                paramScreenTextShort = new SqlParameter("@" + TAG_SCREEN_TEXT_SHORT, DBNull.Value);
            }
            paramScreenTextShort.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramScreenTextShort);


            _log("ENUM PARAMS", sbLog.ToString());
        }


        /*********************** CUSTOM NON-META END *********************/

        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "EnumScreen"; //Table name to abstract
        private static DateTime dtNull = new DateTime();
        private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

        private long _lScreenID = 0;
        private long _lScreenTypeID = 0;
        private DateTime _dtBeginDateCreated = new DateTime();
        private DateTime _dtEndDateCreated = new DateTime();
        private string _strScreenText = null;
        private byte[] _byteScreenImage = null;
        private byte[] _byteAudioData = null;
        private bool? _bIsText = null;
        private bool? _bIsAudio = null;
        private string _strScreenTextShort = null;
        //		private string _strOrderByEnum = "ASC";
        private string _strOrderByField = DB_FIELD_ID;

        /// <summary>DB_FIELD_ID Attribute type string</summary>
        public static readonly string DB_FIELD_ID = "screen_id"; //Table id field name
        /// <summary>ScreenID Attribute type string</summary>
        public static readonly string TAG_SCREEN_ID = "ScreenID"; //Attribute ScreenID  name
        /// <summary>ScreenTypeID Attribute type string</summary>
        public static readonly string TAG_SCREEN_TYPE_ID = "ScreenTypeID"; //Attribute ScreenTypeID  name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
        /// <summary>EndDateCreated Attribute type string</summary>
        public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
        /// <summary>ScreenText Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT = "ScreenText"; //Attribute ScreenText  name
        /// <summary>ScreenImage Attribute type string</summary>
        public static readonly string TAG_SCREEN_IMAGE = "ScreenImage"; //Attribute ScreenImage  name
        /// <summary>AudioData Attribute type string</summary>
        public static readonly string TAG_AUDIO_DATA = "AudioData"; //Attribute AudioData  name
        /// <summary>IsText Attribute type string</summary>
        public static readonly string TAG_IS_TEXT = "IsText"; //Attribute IsText  name
        /// <summary>IsAudio Attribute type string</summary>
        public static readonly string TAG_IS_AUDIO = "IsAudio"; //Attribute IsAudio  name
        /// <summary>ScreenTextShort Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT_SHORT = "ScreenTextShort"; //Attribute ScreenTextShort  name
        // Stored procedure name
        private static readonly string SP_ENUM_NAME = "spScreenEnum"; //Enum sp name

        /// <summary>HasError is a Property in the Screen Class of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        /// <summary>ScreenID is a Property in the Screen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>ScreenTypeID is a Property in the Screen Class of type long</summary>
        public long ScreenTypeID
        {
            get { return _lScreenTypeID; }
            set { _lScreenTypeID = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime BeginDateCreated
        {
            get { return _dtBeginDateCreated; }
            set { _dtBeginDateCreated = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime EndDateCreated
        {
            get { return _dtEndDateCreated; }
            set { _dtEndDateCreated = value; }
        }
        /// <summary>ScreenText is a Property in the Screen Class of type String</summary>
        public string ScreenText
        {
            get { return _strScreenText; }
            set { _strScreenText = value; }
        }
        /// <summary>ScreenImage is a Property in the Screen Class of type byte[]</summary>
        public byte[] ScreenImage
        {
            get { return _byteScreenImage; }
            set { _byteScreenImage = value; }
        }
        /// <summary>AudioData is a Property in the Screen Class of type byte[]</summary>
        public byte[] AudioData
        {
            get { return _byteAudioData; }
            set { _byteAudioData = value; }
        }
        /// <summary>IsText is a Property in the Screen Class of type bool</summary>
        public bool? IsText
        {
            get { return _bIsText; }
            set { _bIsText = value; }
        }
        /// <summary>IsAudio is a Property in the Screen Class of type bool</summary>
        public bool? IsAudio
        {
            get { return _bIsAudio; }
            set { _bIsAudio = value; }
        }
        /// <summary>ScreenTextShort is a Property in the Screen Class of type String</summary>
        public string ScreenTextShort
        {
            get { return _strScreenTextShort; }
            set { _strScreenTextShort = value; }
        }

        /// <summary>Count Property. Type: int</summary>
        public int Count
        {
            get
            {
                _bSetup = true;
                _log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _setupCountParams();
                _cmd.Connection = _conn;
                _cmd.ExecuteNonQuery();
                try
                {
                    string strTmp;
                    strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
                    _nCount = int.Parse(strTmp);
                }
                catch
                {
                    _nCount = 0;
                }
                return _nCount;
            }
        }

        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumScreen()
        {
        }
        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumScreen(SqlConnection conn)
        {
            _conn = conn;
        }

        /// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
        public EnumScreen(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        // Implementation of IEnumerator
        /// <summary>Property of type Screen. Returns the next Screen in the list</summary>
        private Screen _nextTransaction
        {
            get
            {
                Screen o = null;

                if (!_bSetup)
                {
                    EnumData();
                }
                if (_hasMore)
                {
                    o = new Screen(_rdr);
                    _hasMore = _rdr.Read();
                    if (!_hasMore)
                    {
                        Close();
                    }
                }
                return o;
            }
        }

        /// <summary>Enumerates the Data</summary>
        public void EnumData()
        {
            if (!_bSetup)
            {
                _bSetup = true;
                _log("ENUM", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _cmd.Connection = _conn;
                _rdr = _cmd.ExecuteReader();
                _hasAny = _rdr.Read();
                _hasMore = _hasAny;
            }
        }


        /// <summary>returns the next element in the enumeration</summary>
        public object nextElement()
        {
            try
            {
                return _nextTransaction;
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
                return null;
            }
        }

        /// <summary>Returns whether or not more elements exist</summary>
        public bool hasMoreElements()
        {
            try
            {
                if (_bSetup)
                {
                    EnumData();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return _hasMore;
        }

        /// <summary>Closes the datareader</summary>
        public void Close()
        {
            try
            {
                if (_rdr != null)
                {
                    _rdr.Close();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
            _rdr = null;
            _cmd = null;
        }

        /// <summary>ToString is overridden to display all properties of the Screen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID.ToString() + "\n");
            sbReturn.Append(TAG_SCREEN_TYPE_ID + ":  " + ScreenTypeID + "\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
            }
            sbReturn.Append(TAG_SCREEN_TEXT + ":  " + ScreenText + "\n");
            sbReturn.Append(TAG_SCREEN_IMAGE + ":  " + ScreenImage + "\n");
            sbReturn.Append(TAG_AUDIO_DATA + ":  " + AudioData + "\n");
            sbReturn.Append(TAG_IS_TEXT + ":  " + IsText + "\n");
            sbReturn.Append(TAG_IS_AUDIO + ":  " + IsAudio + "\n");
            sbReturn.Append(TAG_SCREEN_TEXT_SHORT + ":  " + ScreenTextShort + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of Screen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TYPE_ID + ">" + ScreenTypeID + "</" + TAG_SCREEN_TYPE_ID + ">\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
            }
            sbReturn.Append("<" + TAG_SCREEN_TEXT + ">" + ScreenText + "</" + TAG_SCREEN_TEXT + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_IMAGE + ">" + ScreenImage + "</" + TAG_SCREEN_IMAGE + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_DATA + ">" + AudioData + "</" + TAG_AUDIO_DATA + ">\n");
            sbReturn.Append("<" + TAG_IS_TEXT + ">" + IsText + "</" + TAG_IS_TEXT + ">\n");
            sbReturn.Append("<" + TAG_IS_AUDIO + ">" + IsAudio + "</" + TAG_IS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TEXT_SHORT + ">" + ScreenTextShort + "</" + TAG_SCREEN_TEXT_SHORT + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse XML string and assign values to object</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                if (xNodes.Count > 0)
                {
                    Parse(xNodes.Item(0));
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
                strTmp = xResultNode.InnerText;
                ScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TYPE_ID);
                ScreenTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
                BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
                EndDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT);
                ScreenText = xResultNode.InnerText;
                if (ScreenText.Trim().Length == 0)
                    ScreenText = null;
            }
            catch
            {
                ScreenText = null;
            }
            // Cannot reliably convert a byte[] to a string.
            // Cannot reliably convert a byte[] to a string.

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_TEXT);
                IsText = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsText = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_AUDIO);
                IsAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT_SHORT);
                ScreenTextShort = xResultNode.InnerText;
                if (ScreenTextShort.Trim().Length == 0)
                    ScreenTextShort = null;
            }
            catch
            {
                ScreenTextShort = null;
            }
        }
        /// <summary>Prompt for values</summary>
        public void Prompt()
        {
            try
            {
                Console.WriteLine(TAG_SCREEN_TYPE_ID + ":  ");
                try
                {
                    ScreenTypeID = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    ScreenTypeID = 0;
                }

                Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    BeginDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    BeginDateCreated = new DateTime();
                }

                Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    EndDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    EndDateCreated = new DateTime();
                }


                Console.WriteLine(TAG_SCREEN_TEXT + ":  ");
                ScreenText = Console.ReadLine();
                if (ScreenText.Length == 0)
                {
                    ScreenText = null;
                }
                // Cannot reliably convert a byte[] to string.
                // Cannot reliably convert a byte[] to string.
                Console.WriteLine(TAG_IS_TEXT + ":  ");
                try
                {
                    IsText = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsText = false;
                }

                Console.WriteLine(TAG_IS_AUDIO + ":  ");
                try
                {
                    IsAudio = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsAudio = false;
                }


                Console.WriteLine(TAG_SCREEN_TEXT_SHORT + ":  ");
                ScreenTextShort = Console.ReadLine();
                if (ScreenTextShort.Length == 0)
                {
                    ScreenTextShort = null;
                }

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }
        private void _setupCountParams()
        {
            SqlParameter paramCount = null;
            paramCount = new SqlParameter();
            paramCount.ParameterName = PARAM_COUNT;
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            _cmd.Parameters.Add(paramCount);
        }
 

        //private
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }
    }
}
