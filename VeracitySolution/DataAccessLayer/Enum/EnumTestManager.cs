using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  EnumTestManager.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumTestManager
	{
		private bool _hasAny = false;
		private bool _hasMore = false;
		private bool _bSetup = false;

		private SqlCommand _cmd = null;
		private SqlDataReader _rdr = null;
		private SqlConnection _conn = null;
		
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Enum-EnumTestManager";
		private ErrorCode _errorCode = null;
		private bool _hasError = false;
		private int _nCount = 0;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumTestManager"; //Table name to abstract
		private static DateTime dtNull = new DateTime();
		private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

		private long _lTestManagerID = 0;
		private long _lTestTemplateID = 0;
		private long _lTestLanguageID = 0;
		private long _lTestSubjectID = 0;
		private long _lScreenID = 0;
		private DateTime _dtBeginDateCreated = new DateTime();
		private DateTime _dtEndDateCreated = new DateTime();
		private DateTime _dtBeginDateModified = new DateTime();
		private DateTime _dtEndDateModified = new DateTime();
		private DateTime _dtBeginDateBeginTest = new DateTime();
		private DateTime _dtEndDateBeginTest = new DateTime();
		private DateTime _dtBeginDateEndTest = new DateTime();
		private DateTime _dtEndDateEndTest = new DateTime();
		private string _strReportData = null;
		private string _strNotes = null;
//		private string _strOrderByEnum = "ASC";
		private string _strOrderByField = DB_FIELD_ID;

		/// <summary>DB_FIELD_ID Attribute type string</summary>
		public static readonly string DB_FIELD_ID = "test_manager_id"; //Table id field name
		/// <summary>TestManagerID Attribute type string</summary>
		public static readonly string TAG_TEST_MANAGER_ID = "TestManagerID"; //Attribute TestManagerID  name
		/// <summary>TestTemplateID Attribute type string</summary>
		public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Attribute TestTemplateID  name
		/// <summary>TestLanguageID Attribute type string</summary>
		public static readonly string TAG_TEST_LANGUAGE_ID = "TestLanguageID"; //Attribute TestLanguageID  name
		/// <summary>TestSubjectID Attribute type string</summary>
		public static readonly string TAG_TEST_SUBJECT_ID = "TestSubjectID"; //Attribute TestSubjectID  name
		/// <summary>ScreenID Attribute type string</summary>
		public static readonly string TAG_SCREEN_ID = "ScreenID"; //Attribute ScreenID  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
		/// <summary>EndDateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
		/// <summary>EndDateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
		/// <summary>DateBeginTest Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_BEGIN_TEST = "BeginDateBeginTest"; //Attribute DateBeginTest  name
		/// <summary>EndDateBeginTest Attribute type string</summary>
		public static readonly string TAG_END_DATE_BEGIN_TEST = "EndDateBeginTest"; //Attribute DateBeginTest  name
		/// <summary>DateEndTest Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_END_TEST = "BeginDateEndTest"; //Attribute DateEndTest  name
		/// <summary>EndDateEndTest Attribute type string</summary>
		public static readonly string TAG_END_DATE_END_TEST = "EndDateEndTest"; //Attribute DateEndTest  name
		/// <summary>ReportData Attribute type string</summary>
		public static readonly string TAG_REPORT_DATA = "ReportData"; //Attribute ReportData  name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Attribute Notes  name
		// Stored procedure name
		private static readonly string SP_ENUM_NAME = "spTestManagerEnum"; //Enum sp name

		/// <summary>HasError is a Property in the TestManager Class of type bool</summary>
		public bool HasError 
		{
			get{return _hasError;}
			set{_hasError = value;}
		}
		/// <summary>TestManagerID is a Property in the TestManager Class of type long</summary>
		public long TestManagerID 
		{
			get{return _lTestManagerID;}
			set{_lTestManagerID = value;}
		}
		/// <summary>TestTemplateID is a Property in the TestManager Class of type long</summary>
		public long TestTemplateID 
		{
			get{return _lTestTemplateID;}
			set{_lTestTemplateID = value;}
		}
		/// <summary>TestLanguageID is a Property in the TestManager Class of type long</summary>
		public long TestLanguageID 
		{
			get{return _lTestLanguageID;}
			set{_lTestLanguageID = value;}
		}
		/// <summary>TestSubjectID is a Property in the TestManager Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>ScreenID is a Property in the TestManager Class of type long</summary>
		public long ScreenID 
		{
			get{return _lScreenID;}
			set{_lScreenID = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime BeginDateCreated
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime EndDateCreated
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime BeginDateModified
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime EndDateModified
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>Property DateBeginTest. Type: DateTime</summary>
		public DateTime BeginDateBeginTest
		{
			get{return _dtBeginDateBeginTest;}
			set{_dtBeginDateBeginTest = value;}
		}
		/// <summary>Property DateBeginTest. Type: DateTime</summary>
		public DateTime EndDateBeginTest
		{
			get{return _dtEndDateBeginTest;}
			set{_dtEndDateBeginTest = value;}
		}
		/// <summary>Property DateEndTest. Type: DateTime</summary>
		public DateTime BeginDateEndTest
		{
			get{return _dtBeginDateEndTest;}
			set{_dtBeginDateEndTest = value;}
		}
		/// <summary>Property DateEndTest. Type: DateTime</summary>
		public DateTime EndDateEndTest
		{
			get{return _dtEndDateEndTest;}
			set{_dtEndDateEndTest = value;}
		}
		/// <summary>ReportData is a Property in the TestManager Class of type String</summary>
		public string ReportData 
		{
			get{return _strReportData;}
			set{_strReportData = value;}
		}
		/// <summary>Notes is a Property in the TestManager Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}

		/// <summary>Count Property. Type: int</summary>
		public int Count 
		{
			get
			{
				_bSetup = true;
				_log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_setupCountParams();
				_cmd.Connection = _conn;
				_cmd.ExecuteNonQuery();
				try
				{
					string strTmp;
					strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
					_nCount = int.Parse(strTmp);
				}
				catch 
				{
					_nCount = 0;
				}
				return _nCount;			}
		}

		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumTestManager()
		{
		}
		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumTestManager(SqlConnection conn)
		{
			_conn = conn;
		}

		/// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
		public EnumTestManager(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

		// Implementation of IEnumerator
		/// <summary>Property of type TestManager. Returns the next TestManager in the list</summary>
		private TestManager _nextTransaction
		{
			get
			{
				TestManager o = null;
				
				if (!_bSetup)
				{
					EnumData();
				}
				if (_hasMore)
				{
					o = new TestManager(_rdr);
					_hasMore = _rdr.Read();
					if (!_hasMore)
					{
						Close();
					}
				}
				return o;
			}
		}

		/// <summary>Enumerates the Data</summary>
		public void EnumData()
		{
			if (!_bSetup)
			{
				_bSetup = true;
				_log("ENUM", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_cmd.Connection = _conn;
				_rdr = _cmd.ExecuteReader();
				_hasAny = _rdr.Read();
				_hasMore = _hasAny;
			}
		}


		/// <summary>returns the next element in the enumeration</summary>
		public object nextElement()
		{
			try
			{
				return _nextTransaction;
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
				return null;
			}
		}

		/// <summary>Returns whether or not more elements exist</summary>
		public bool hasMoreElements()
		{
			try
			{
				if (_bSetup)
				{
					EnumData();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return _hasMore;
		}

		/// <summary>Closes the datareader</summary>
		public void Close()
		{
			try
			{
				if ( _rdr != null )
				{
					_rdr.Close();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
			_rdr = null;
			_cmd = null;
		}

		/// <summary>ToString is overridden to display all properties of the TestManager Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_TEST_MANAGER_ID + ":  " + TestManagerID.ToString() + "\n");
			sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
			sbReturn.Append(TAG_TEST_LANGUAGE_ID + ":  " + TestLanguageID + "\n");
			sbReturn.Append(TAG_TEST_SUBJECT_ID + ":  " + TestSubjectID + "\n");
			sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(BeginDateBeginTest))
			{
				sbReturn.Append(TAG_BEGIN_DATE_BEGIN_TEST + ":  " + BeginDateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(EndDateBeginTest))
			{
				sbReturn.Append(TAG_END_DATE_BEGIN_TEST + ":  " + EndDateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(BeginDateEndTest))
			{
				sbReturn.Append(TAG_BEGIN_DATE_END_TEST + ":  " + BeginDateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_END_TEST + ":\n");
			}
			if (!dtNull.Equals(EndDateEndTest))
			{
				sbReturn.Append(TAG_END_DATE_END_TEST + ":  " + EndDateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_END_TEST + ":\n");
			}
			sbReturn.Append(TAG_REPORT_DATA + ":  " + ReportData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestManager</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_TEST_MANAGER_ID + ">" + TestManagerID + "</" + TAG_TEST_MANAGER_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_LANGUAGE_ID + ">" + TestLanguageID + "</" + TAG_TEST_LANGUAGE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_SUBJECT_ID + ">" + TestSubjectID + "</" + TAG_TEST_SUBJECT_ID + ">\n");
			sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(BeginDateBeginTest))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_BEGIN_TEST + ">" + BeginDateBeginTest.ToString() + "</" + TAG_BEGIN_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_BEGIN_TEST + "></" + TAG_BEGIN_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(EndDateBeginTest))
			{
				sbReturn.Append("<" + TAG_END_DATE_BEGIN_TEST + ">" + EndDateBeginTest.ToString() + "</" + TAG_END_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_BEGIN_TEST + "></" + TAG_END_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(BeginDateEndTest))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_END_TEST + ">" + BeginDateEndTest.ToString() + "</" + TAG_BEGIN_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_END_TEST + "></" + TAG_BEGIN_DATE_END_TEST + ">\n");
			}
			if (!dtNull.Equals(EndDateEndTest))
			{
				sbReturn.Append("<" + TAG_END_DATE_END_TEST + ">" + EndDateEndTest.ToString() + "</" + TAG_END_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_END_TEST + "></" + TAG_END_DATE_END_TEST + ">\n");
			}
			sbReturn.Append("<" + TAG_REPORT_DATA + ">" + ReportData + "</" + TAG_REPORT_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse XML string and assign values to object</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				if ( xNodes.Count > 0 )
				{
					Parse(xNodes.Item(0));
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_MANAGER_ID);
				strTmp = xResultNode.InnerText;
				TestManagerID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
				TestTemplateID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestTemplateID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_LANGUAGE_ID);
				TestLanguageID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestLanguageID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_SUBJECT_ID);
				TestSubjectID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestSubjectID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
				ScreenID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ScreenID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_BEGIN_TEST);
				BeginDateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_BEGIN_TEST);
				EndDateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_END_TEST);
				BeginDateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_END_TEST);
				EndDateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REPORT_DATA);
				ReportData = xResultNode.InnerText;
				if (ReportData.Trim().Length == 0)
					ReportData = null;
			}
			catch  
			{
				ReportData = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
				if (Notes.Trim().Length == 0)
					Notes = null;
			}
			catch  
			{
				Notes = null;
			}
		}
		/// <summary>Prompt for values</summary>
		public void Prompt()
		{
			try 
			{
				Console.WriteLine(TAG_TEST_TEMPLATE_ID + ":  ");
				try
				{
					TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					TestTemplateID = 0;
				}

				Console.WriteLine(TAG_TEST_LANGUAGE_ID + ":  ");
				try
				{
					TestLanguageID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					TestLanguageID = 0;
				}

				Console.WriteLine(TAG_TEST_SUBJECT_ID + ":  ");
				try
				{
					TestSubjectID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					TestSubjectID = 0;
				}

				Console.WriteLine(TAG_SCREEN_ID + ":  ");
				try
				{
					ScreenID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					ScreenID = 0;
				}

				Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateCreated = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateCreated = DateTime.Parse(s);
				}
				catch  
				{
					EndDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateModified = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateModified = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateModified = DateTime.Parse(s);
				}
				catch  
				{
					EndDateModified = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_BEGIN_TEST + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateBeginTest = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateBeginTest = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_BEGIN_TEST + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateBeginTest = DateTime.Parse(s);
				}
				catch  
				{
					EndDateBeginTest = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_END_TEST + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateEndTest = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateEndTest = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_END_TEST + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateEndTest = DateTime.Parse(s);
				}
				catch  
				{
					EndDateEndTest = new DateTime();
				}


				Console.WriteLine(TAG_REPORT_DATA + ":  ");
				ReportData = Console.ReadLine();
				if (ReportData.Length == 0)
				{
					ReportData = null;
				}

				Console.WriteLine(TAG_NOTES + ":  ");
				Notes = Console.ReadLine();
				if (Notes.Length == 0)
				{
					Notes = null;
				}

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}

		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}
		private void _setupCountParams()
		{
			SqlParameter paramCount = null;
			paramCount = new SqlParameter();
			paramCount.ParameterName = PARAM_COUNT;
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			_cmd.Parameters.Add(paramCount);
		}
		private void _setupEnumParams()
		{
			System.Text.StringBuilder sbLog = null;
			SqlParameter paramTestManagerID = null;
			SqlParameter paramTestTemplateID = null;
			SqlParameter paramTestLanguageID = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramScreenID = null;
			SqlParameter paramBeginDateCreated = null;
			SqlParameter paramEndDateCreated = null;
			SqlParameter paramBeginDateModified = null;
			SqlParameter paramEndDateModified = null;
			SqlParameter paramBeginDateBeginTest = null;
			SqlParameter paramEndDateBeginTest = null;
			SqlParameter paramBeginDateEndTest = null;
			SqlParameter paramEndDateEndTest = null;
			SqlParameter paramReportData = null;
			SqlParameter paramNotes = null;
			DateTime dtNull = new DateTime();

			sbLog = new System.Text.StringBuilder();
				paramTestManagerID = new SqlParameter("@" + TAG_TEST_MANAGER_ID, TestManagerID);
				sbLog.Append(TAG_TEST_MANAGER_ID + "=" + TestManagerID + "\n");
				paramTestManagerID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestManagerID);

				paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
				sbLog.Append(TAG_TEST_TEMPLATE_ID + "=" + TestTemplateID + "\n");
				paramTestTemplateID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestTemplateID);
				paramTestLanguageID = new SqlParameter("@" + TAG_TEST_LANGUAGE_ID, TestLanguageID);
				sbLog.Append(TAG_TEST_LANGUAGE_ID + "=" + TestLanguageID + "\n");
				paramTestLanguageID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestLanguageID);
				paramTestSubjectID = new SqlParameter("@" + TAG_TEST_SUBJECT_ID, TestSubjectID);
				sbLog.Append(TAG_TEST_SUBJECT_ID + "=" + TestSubjectID + "\n");
				paramTestSubjectID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestSubjectID);
				paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
				sbLog.Append(TAG_SCREEN_ID + "=" + ScreenID + "\n");
				paramScreenID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramScreenID);
			// Setup the date created param
			if (!dtNull.Equals(BeginDateCreated))
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
				sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
			}
			else
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
			}
			paramBeginDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateCreated);

			if (!dtNull.Equals(EndDateCreated))
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
				sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
			}
			else
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
			}
			paramEndDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateCreated);

			// Setup the date modified param
			if (!dtNull.Equals(BeginDateModified))
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
				sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
			}
			else
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
			}
			paramBeginDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateModified);

			if (!dtNull.Equals(EndDateModified))
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
				sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
			}
			else
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
			}
			paramEndDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateModified);

			// Setup the date begin test param
			if (!dtNull.Equals(BeginDateBeginTest))
			{
				paramBeginDateBeginTest = new SqlParameter("@" + TAG_BEGIN_DATE_BEGIN_TEST, BeginDateBeginTest);
				sbLog.Append(TAG_BEGIN_DATE_BEGIN_TEST + "=" + BeginDateBeginTest.ToLongDateString());
			}
			else
			{
				paramBeginDateBeginTest = new SqlParameter("@" + TAG_BEGIN_DATE_BEGIN_TEST, DBNull.Value);
			}
			paramBeginDateBeginTest.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateBeginTest);

			if (!dtNull.Equals(EndDateBeginTest))
			{
				paramEndDateBeginTest = new SqlParameter("@" + TAG_END_DATE_BEGIN_TEST, EndDateBeginTest);
				sbLog.Append(TAG_END_DATE_BEGIN_TEST + "=" + EndDateBeginTest.ToLongDateString());
			}
			else
			{
				paramEndDateBeginTest = new SqlParameter("@" + TAG_END_DATE_BEGIN_TEST, DBNull.Value);
			}
			paramEndDateBeginTest.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateBeginTest);

			// Setup the date end test param
			if (!dtNull.Equals(BeginDateEndTest))
			{
				paramBeginDateEndTest = new SqlParameter("@" + TAG_BEGIN_DATE_END_TEST, BeginDateEndTest);
				sbLog.Append(TAG_BEGIN_DATE_END_TEST + "=" + BeginDateEndTest.ToLongDateString());
			}
			else
			{
				paramBeginDateEndTest = new SqlParameter("@" + TAG_BEGIN_DATE_END_TEST, DBNull.Value);
			}
			paramBeginDateEndTest.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateEndTest);

			if (!dtNull.Equals(EndDateEndTest))
			{
				paramEndDateEndTest = new SqlParameter("@" + TAG_END_DATE_END_TEST, EndDateEndTest);
				sbLog.Append(TAG_END_DATE_END_TEST + "=" + EndDateEndTest.ToLongDateString());
			}
			else
			{
				paramEndDateEndTest = new SqlParameter("@" + TAG_END_DATE_END_TEST, DBNull.Value);
			}
			paramEndDateEndTest.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateEndTest);

			// Setup the report data text param
			if ( ReportData != null )
			{
				paramReportData = new SqlParameter("@" + TAG_REPORT_DATA, ReportData);
				sbLog.Append(TAG_REPORT_DATA + "=" + ReportData + "\n");
			}
			else
			{
				paramReportData = new SqlParameter("@" + TAG_REPORT_DATA, DBNull.Value);
			}
			paramReportData.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramReportData);

			// Setup the notes text param
			if ( Notes != null )
			{
				paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
				sbLog.Append(TAG_NOTES + "=" + Notes + "\n");
			}
			else
			{
				paramNotes = new SqlParameter("@" + TAG_NOTES, DBNull.Value);
			}
			paramNotes.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramNotes);

				_log("ENUM PARAMS", sbLog.ToString());
		}

		//private
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}
	}
}

