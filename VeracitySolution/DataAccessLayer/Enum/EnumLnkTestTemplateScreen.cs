using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  EnumLnkTestTemplateScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/14/2013	Created
    /// 
    /// ----------------------------------------------------
    /// </summary>
    public class EnumLnkTestTemplateScreen
    {
        private bool _hasAny = false;
        private bool _hasMore = false;
        private bool _bSetup = false;

        private SqlCommand _cmd = null;
        private SqlDataReader _rdr = null;
        private SqlConnection _conn = null;

        private Config _config = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Enum-EnumLnkTestTemplateScreen";
        private ErrorCode _errorCode = null;
        private bool _hasError = false;
        private int _nCount = 0;


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "EnumLnkTestTemplateScreen"; //Table name to abstract
        private static DateTime dtNull = new DateTime();
        private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

        private long _lLnkTestTemplateScreenID = 0;
        private long _lTestTemplateID = 0;
        private long _lScreenID = 0;
        private DateTime _dtBeginDateCreated = new DateTime();
        private DateTime _dtEndDateCreated = new DateTime();
        private DateTime _dtBeginDateModified = new DateTime();
        private DateTime _dtEndDateModified = new DateTime();
        private long _lFlowOrderIndex = 0;
        private long _lTransitionDelayInMs = 0;
        private bool? _bIsIgnored = null;
        private bool? _bIsReported = null;
        private string _strNotes = null;
        private string _strImageFileName = null;
        private bool? _bHasAudio = null;
        private string _strAudioFileName = null;
        //		private string _strOrderByEnum = "ASC";
        private string _strOrderByField = DB_FIELD_ID;

        /// <summary>DB_FIELD_ID Attribute type string</summary>
        public static readonly string DB_FIELD_ID = "lnk_test_template_screen_id"; //Table id field name
        /// <summary>LnkTestTemplateScreenID Attribute type string</summary>
        public static readonly string TAG_LNK_TEST_TEMPLATE_SCREEN_ID = "LnkTestTemplateScreenID"; //Attribute LnkTestTemplateScreenID  name
        /// <summary>TestTemplateID Attribute type string</summary>
        public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Attribute TestTemplateID  name
        /// <summary>ScreenID Attribute type string</summary>
        public static readonly string TAG_SCREEN_ID = "ScreenID"; //Attribute ScreenID  name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
        /// <summary>EndDateCreated Attribute type string</summary>
        public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
        /// <summary>EndDateModified Attribute type string</summary>
        public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
        /// <summary>FlowOrderIndex Attribute type string</summary>
        public static readonly string TAG_FLOW_ORDER_INDEX = "FlowOrderIndex"; //Attribute FlowOrderIndex  name
        /// <summary>TransitionDelayInMs Attribute type string</summary>
        public static readonly string TAG_TRANSITION_DELAY_IN_MS = "TransitionDelayInMs"; //Attribute TransitionDelayInMs  name
        /// <summary>IsIgnored Attribute type string</summary>
        public static readonly string TAG_IS_IGNORED = "IsIgnored"; //Attribute IsIgnored  name
        /// <summary>IsReported Attribute type string</summary>
        public static readonly string TAG_IS_REPORTED = "IsReported"; //Attribute IsReported  name
        /// <summary>Notes Attribute type string</summary>
        public static readonly string TAG_NOTES = "Notes"; //Attribute Notes  name
        /// <summary>ImageFileName Attribute type string</summary>
        public static readonly string TAG_IMAGE_FILE_NAME = "ImageFileName"; //Attribute ImageFileName  name
        /// <summary>HasAudio Attribute type string</summary>
        public static readonly string TAG_HAS_AUDIO = "HasAudio"; //Attribute HasAudio  name
        /// <summary>AudioFileName Attribute type string</summary>
        public static readonly string TAG_AUDIO_FILE_NAME = "AudioFileName"; //Attribute AudioFileName  name
        // Stored procedure name
        private static readonly string SP_ENUM_NAME = "spLnkTestTemplateScreenEnum"; //Enum sp name

        /// <summary>HasError is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }
        /// <summary>LnkTestTemplateScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long LnkTestTemplateScreenID
        {
            get { return _lLnkTestTemplateScreenID; }
            set { _lLnkTestTemplateScreenID = value; }
        }
        /// <summary>TestTemplateID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>ScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime BeginDateCreated
        {
            get { return _dtBeginDateCreated; }
            set { _dtBeginDateCreated = value; }
        }
        /// <summary>Property DateCreated. Type: DateTime</summary>
        public DateTime EndDateCreated
        {
            get { return _dtEndDateCreated; }
            set { _dtEndDateCreated = value; }
        }
        /// <summary>Property DateModified. Type: DateTime</summary>
        public DateTime BeginDateModified
        {
            get { return _dtBeginDateModified; }
            set { _dtBeginDateModified = value; }
        }
        /// <summary>Property DateModified. Type: DateTime</summary>
        public DateTime EndDateModified
        {
            get { return _dtEndDateModified; }
            set { _dtEndDateModified = value; }
        }
        /// <summary>FlowOrderIndex is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long FlowOrderIndex
        {
            get { return _lFlowOrderIndex; }
            set { _lFlowOrderIndex = value; }
        }
        /// <summary>TransitionDelayInMs is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TransitionDelayInMs
        {
            get { return _lTransitionDelayInMs; }
            set { _lTransitionDelayInMs = value; }
        }
        /// <summary>IsIgnored is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsIgnored
        {
            get { return _bIsIgnored; }
            set { _bIsIgnored = value; }
        }
        /// <summary>IsReported is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsReported
        {
            get { return _bIsReported; }
            set { _bIsReported = value; }
        }
        /// <summary>Notes is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string Notes
        {
            get { return _strNotes; }
            set { _strNotes = value; }
        }
        /// <summary>ImageFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string ImageFileName
        {
            get { return _strImageFileName; }
            set { _strImageFileName = value; }
        }
        /// <summary>HasAudio is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? HasAudio
        {
            get { return _bHasAudio; }
            set { _bHasAudio = value; }
        }
        /// <summary>AudioFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string AudioFileName
        {
            get { return _strAudioFileName; }
            set { _strAudioFileName = value; }
        }

        /// <summary>Count Property. Type: int</summary>
        public int Count
        {
            get
            {
                _bSetup = true;
                _log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _setupCountParams();
                _cmd.Connection = _conn;
                _cmd.ExecuteNonQuery();
                try
                {
                    string strTmp;
                    strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
                    _nCount = int.Parse(strTmp);
                }
                catch
                {
                    _nCount = 0;
                }
                return _nCount;
            }
        }

        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumLnkTestTemplateScreen()
        {
        }
        /// <summary>Contructor takes 1 parameter: SqlConnection</summary>
        public EnumLnkTestTemplateScreen(SqlConnection conn)
        {
            _conn = conn;
        }

        /// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
        public EnumLnkTestTemplateScreen(SqlConnection conn, Config pConfig)
        {
            _conn = conn;
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }

        // Implementation of IEnumerator
        /// <summary>Property of type LnkTestTemplateScreen. Returns the next LnkTestTemplateScreen in the list</summary>
        private LnkTestTemplateScreen _nextTransaction
        {
            get
            {
                LnkTestTemplateScreen o = null;

                if (!_bSetup)
                {
                    EnumData();
                }
                if (_hasMore)
                {
                    o = new LnkTestTemplateScreen(_rdr);
                    _hasMore = _rdr.Read();
                    if (!_hasMore)
                    {
                        Close();
                    }
                }
                return o;
            }
        }

        /// <summary>Enumerates the Data</summary>
        public void EnumData()
        {
            if (!_bSetup)
            {
                _bSetup = true;
                _log("ENUM", "Calling sp " + SP_ENUM_NAME);
                // if necessary, close the old reader
                if ((_cmd != null) || (_rdr != null))
                {
                    Close();
                }
                _cmd = new SqlCommand(SP_ENUM_NAME, _conn);
                _cmd.CommandType = CommandType.StoredProcedure;
                _setupEnumParams();
                _cmd.Connection = _conn;
                _rdr = _cmd.ExecuteReader();
                _hasAny = _rdr.Read();
                _hasMore = _hasAny;
            }
        }


        /// <summary>returns the next element in the enumeration</summary>
        public object nextElement()
        {
            try
            {
                return _nextTransaction;
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
                return null;
            }
        }

        /// <summary>Returns whether or not more elements exist</summary>
        public bool hasMoreElements()
        {
            try
            {
                if (_bSetup)
                {
                    EnumData();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return _hasMore;
        }

        /// <summary>Closes the datareader</summary>
        public void Close()
        {
            try
            {
                if (_rdr != null)
                {
                    _rdr.Close();
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
            _rdr = null;
            _cmd = null;
        }

        /// <summary>ToString is overridden to display all properties of the LnkTestTemplateScreen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_LNK_TEST_TEMPLATE_SCREEN_ID + ":  " + LnkTestTemplateScreenID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
            sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(BeginDateModified))
            {
                sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
            }
            if (!dtNull.Equals(EndDateModified))
            {
                sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
            }
            sbReturn.Append(TAG_FLOW_ORDER_INDEX + ":  " + FlowOrderIndex + "\n");
            sbReturn.Append(TAG_TRANSITION_DELAY_IN_MS + ":  " + TransitionDelayInMs + "\n");
            sbReturn.Append(TAG_IS_IGNORED + ":  " + IsIgnored + "\n");
            sbReturn.Append(TAG_IS_REPORTED + ":  " + IsReported + "\n");
            sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");
            sbReturn.Append(TAG_IMAGE_FILE_NAME + ":  " + ImageFileName + "\n");
            sbReturn.Append(TAG_HAS_AUDIO + ":  " + HasAudio + "\n");
            sbReturn.Append(TAG_AUDIO_FILE_NAME + ":  " + AudioFileName + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of LnkTestTemplateScreen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_LNK_TEST_TEMPLATE_SCREEN_ID + ">" + LnkTestTemplateScreenID + "</" + TAG_LNK_TEST_TEMPLATE_SCREEN_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(BeginDateModified))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
            }
            if (!dtNull.Equals(EndDateModified))
            {
                sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("<" + TAG_FLOW_ORDER_INDEX + ">" + FlowOrderIndex + "</" + TAG_FLOW_ORDER_INDEX + ">\n");
            sbReturn.Append("<" + TAG_TRANSITION_DELAY_IN_MS + ">" + TransitionDelayInMs + "</" + TAG_TRANSITION_DELAY_IN_MS + ">\n");
            sbReturn.Append("<" + TAG_IS_IGNORED + ">" + IsIgnored + "</" + TAG_IS_IGNORED + ">\n");
            sbReturn.Append("<" + TAG_IS_REPORTED + ">" + IsReported + "</" + TAG_IS_REPORTED + ">\n");
            sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
            sbReturn.Append("<" + TAG_IMAGE_FILE_NAME + ">" + ImageFileName + "</" + TAG_IMAGE_FILE_NAME + ">\n");
            sbReturn.Append("<" + TAG_HAS_AUDIO + ">" + HasAudio + "</" + TAG_HAS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_FILE_NAME + ">" + AudioFileName + "</" + TAG_AUDIO_FILE_NAME + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse XML string and assign values to object</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                if (xNodes.Count > 0)
                {
                    Parse(xNodes.Item(0));
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_LNK_TEST_TEMPLATE_SCREEN_ID);
                strTmp = xResultNode.InnerText;
                LnkTestTemplateScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
                TestTemplateID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTemplateID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
                ScreenID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
                BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
                EndDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
                BeginDateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
                EndDateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_FLOW_ORDER_INDEX);
                FlowOrderIndex = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                FlowOrderIndex = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TRANSITION_DELAY_IN_MS);
                TransitionDelayInMs = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TransitionDelayInMs = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_IGNORED);
                IsIgnored = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsIgnored = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_REPORTED);
                IsReported = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsReported = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_NOTES);
                Notes = xResultNode.InnerText;
                if (Notes.Trim().Length == 0)
                    Notes = null;
            }
            catch
            {
                Notes = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IMAGE_FILE_NAME);
                ImageFileName = xResultNode.InnerText;
                if (ImageFileName.Trim().Length == 0)
                    ImageFileName = null;
            }
            catch
            {
                ImageFileName = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_HAS_AUDIO);
                HasAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                HasAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AUDIO_FILE_NAME);
                AudioFileName = xResultNode.InnerText;
                if (AudioFileName.Trim().Length == 0)
                    AudioFileName = null;
            }
            catch
            {
                AudioFileName = null;
            }
        }
        /// <summary>Prompt for values</summary>
        public void Prompt()
        {
            try
            {
                Console.WriteLine(TAG_TEST_TEMPLATE_ID + ":  ");
                try
                {
                    TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    TestTemplateID = 0;
                }

                Console.WriteLine(TAG_SCREEN_ID + ":  ");
                try
                {
                    ScreenID = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    ScreenID = 0;
                }

                Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    BeginDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    BeginDateCreated = new DateTime();
                }

                Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    EndDateCreated = DateTime.Parse(s);
                }
                catch
                {
                    EndDateCreated = new DateTime();
                }

                Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    BeginDateModified = DateTime.Parse(s);
                }
                catch
                {
                    BeginDateModified = new DateTime();
                }

                Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
                try
                {
                    string s = Console.ReadLine();
                    EndDateModified = DateTime.Parse(s);
                }
                catch
                {
                    EndDateModified = new DateTime();
                }

                Console.WriteLine(TAG_FLOW_ORDER_INDEX + ":  ");
                try
                {
                    FlowOrderIndex = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    FlowOrderIndex = 0;
                }

                Console.WriteLine(TAG_TRANSITION_DELAY_IN_MS + ":  ");
                try
                {
                    TransitionDelayInMs = (long)Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    TransitionDelayInMs = 0;
                }

                Console.WriteLine(TAG_IS_IGNORED + ":  ");
                try
                {
                    IsIgnored = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsIgnored = false;
                }

                Console.WriteLine(TAG_IS_REPORTED + ":  ");
                try
                {
                    IsReported = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    IsReported = false;
                }


                Console.WriteLine(TAG_NOTES + ":  ");
                Notes = Console.ReadLine();
                if (Notes.Length == 0)
                {
                    Notes = null;
                }

                Console.WriteLine(TAG_IMAGE_FILE_NAME + ":  ");
                ImageFileName = Console.ReadLine();
                if (ImageFileName.Length == 0)
                {
                    ImageFileName = null;
                }
                Console.WriteLine(TAG_HAS_AUDIO + ":  ");
                try
                {
                    HasAudio = Convert.ToBoolean(Console.ReadLine());
                }
                catch
                {
                    HasAudio = false;
                }


                Console.WriteLine(TAG_AUDIO_FILE_NAME + ":  ");
                AudioFileName = Console.ReadLine();
                if (AudioFileName.Length == 0)
                {
                    AudioFileName = null;
                }

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }
        private void _setupCountParams()
        {
            SqlParameter paramCount = null;
            paramCount = new SqlParameter();
            paramCount.ParameterName = PARAM_COUNT;
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            _cmd.Parameters.Add(paramCount);
        }
        private void _setupEnumParams()
        {
            System.Text.StringBuilder sbLog = null;
            SqlParameter paramLnkTestTemplateScreenID = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramBeginDateCreated = null;
            SqlParameter paramEndDateCreated = null;
            SqlParameter paramBeginDateModified = null;
            SqlParameter paramEndDateModified = null;
            SqlParameter paramFlowOrderIndex = null;
            SqlParameter paramTransitionDelayInMs = null;
            SqlParameter paramIsIgnored = null;
            SqlParameter paramIsReported = null;
            SqlParameter paramNotes = null;
            SqlParameter paramImageFileName = null;
            SqlParameter paramHasAudio = null;
            SqlParameter paramAudioFileName = null;
            DateTime dtNull = new DateTime();

            sbLog = new System.Text.StringBuilder();
            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_LNK_TEST_TEMPLATE_SCREEN_ID, LnkTestTemplateScreenID);
            sbLog.Append(TAG_LNK_TEST_TEMPLATE_SCREEN_ID + "=" + LnkTestTemplateScreenID + "\n");
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramLnkTestTemplateScreenID);

            paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
            sbLog.Append(TAG_TEST_TEMPLATE_ID + "=" + TestTemplateID + "\n");
            paramTestTemplateID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramTestTemplateID);
            paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
            sbLog.Append(TAG_SCREEN_ID + "=" + ScreenID + "\n");
            paramScreenID.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramScreenID);
            // Setup the date created param
            if (!dtNull.Equals(BeginDateCreated))
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
                sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
            }
            else
            {
                paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
            }
            paramBeginDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramBeginDateCreated);

            if (!dtNull.Equals(EndDateCreated))
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
                sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
            }
            else
            {
                paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
            }
            paramEndDateCreated.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramEndDateCreated);

            // Setup the date modified param
            if (!dtNull.Equals(BeginDateModified))
            {
                paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
                sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
            }
            else
            {
                paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
            }
            paramBeginDateModified.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramBeginDateModified);

            if (!dtNull.Equals(EndDateModified))
            {
                paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
                sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
            }
            else
            {
                paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
            }
            paramEndDateModified.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramEndDateModified);

            paramFlowOrderIndex = new SqlParameter("@" + TAG_FLOW_ORDER_INDEX, FlowOrderIndex);
            sbLog.Append(TAG_FLOW_ORDER_INDEX + "=" + FlowOrderIndex + "\n");
            paramFlowOrderIndex.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramFlowOrderIndex);

            paramTransitionDelayInMs = new SqlParameter("@" + TAG_TRANSITION_DELAY_IN_MS, TransitionDelayInMs);
            sbLog.Append(TAG_TRANSITION_DELAY_IN_MS + "=" + TransitionDelayInMs + "\n");
            paramTransitionDelayInMs.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramTransitionDelayInMs);

            paramIsIgnored = new SqlParameter("@" + TAG_IS_IGNORED, IsIgnored);
            sbLog.Append(TAG_IS_IGNORED + "=" + IsIgnored + "\n");
            paramIsIgnored.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsIgnored);
            paramIsReported = new SqlParameter("@" + TAG_IS_REPORTED, IsReported);
            sbLog.Append(TAG_IS_REPORTED + "=" + IsReported + "\n");
            paramIsReported.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramIsReported);
            // Setup the notes text param
            if (Notes != null)
            {
                paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
                sbLog.Append(TAG_NOTES + "=" + Notes + "\n");
            }
            else
            {
                paramNotes = new SqlParameter("@" + TAG_NOTES, DBNull.Value);
            }
            paramNotes.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramNotes);

            // Setup the image file name text param
            if (ImageFileName != null)
            {
                paramImageFileName = new SqlParameter("@" + TAG_IMAGE_FILE_NAME, ImageFileName);
                sbLog.Append(TAG_IMAGE_FILE_NAME + "=" + ImageFileName + "\n");
            }
            else
            {
                paramImageFileName = new SqlParameter("@" + TAG_IMAGE_FILE_NAME, DBNull.Value);
            }
            paramImageFileName.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramImageFileName);

            paramHasAudio = new SqlParameter("@" + TAG_HAS_AUDIO, HasAudio);
            sbLog.Append(TAG_HAS_AUDIO + "=" + HasAudio + "\n");
            paramHasAudio.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramHasAudio);
            // Setup the audio file name text param
            if (AudioFileName != null)
            {
                paramAudioFileName = new SqlParameter("@" + TAG_AUDIO_FILE_NAME, AudioFileName);
                sbLog.Append(TAG_AUDIO_FILE_NAME + "=" + AudioFileName + "\n");
            }
            else
            {
                paramAudioFileName = new SqlParameter("@" + TAG_AUDIO_FILE_NAME, DBNull.Value);
            }
            paramAudioFileName.Direction = ParameterDirection.Input;
            _cmd.Parameters.Add(paramAudioFileName);

            _log("ENUM PARAMS", sbLog.ToString());
        }

        //private
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }
    }
}
