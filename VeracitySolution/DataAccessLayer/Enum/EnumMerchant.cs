using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  EnumMerchant.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	12/27/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumMerchant
	{
		private bool _hasAny = false;
		private bool _hasMore = false;
		private bool _bSetup = false;

		private SqlCommand _cmd = null;
		private SqlDataReader _rdr = null;
		private SqlConnection _conn = null;
		
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Enum-EnumMerchant";
		private ErrorCode _errorCode = null;
		private bool _hasError = false;
		private int _nCount = 0;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumMerchant"; //Table name to abstract
		private static DateTime dtNull = new DateTime();
		private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

		private long _lMerchantID = 0;
		private long _lAddressID = 0;
		private DateTime _dtBeginDateCreated = new DateTime();
		private DateTime _dtEndDateCreated = new DateTime();
		private DateTime _dtBeginDateModified = new DateTime();
		private DateTime _dtEndDateModified = new DateTime();
		private bool? _bIsDeactivated = null;
		private string _strPhonePrimary = null;
		private string _strPhoneFax = null;
		private string _strMerchantName = null;
		private string _strEmailAddress = null;
		private string _strWebsiteLink = null;
		private string _strContactName = null;
		private string _strContactPhone = null;
		private string _strContactEmail = null;
//		private string _strOrderByEnum = "ASC";
		private string _strOrderByField = DB_FIELD_ID;

		/// <summary>DB_FIELD_ID Attribute type string</summary>
		public static readonly string DB_FIELD_ID = "merchant_id"; //Table id field name
		/// <summary>MerchantID Attribute type string</summary>
		public static readonly string TAG_MERCHANT_ID = "MerchantID"; //Attribute MerchantID  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Attribute AddressID  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
		/// <summary>EndDateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
		/// <summary>EndDateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
		/// <summary>IsDeactivated Attribute type string</summary>
		public static readonly string TAG_IS_DEACTIVATED = "IsDeactivated"; //Attribute IsDeactivated  name
		/// <summary>PhonePrimary Attribute type string</summary>
		public static readonly string TAG_PHONE_PRIMARY = "PhonePrimary"; //Attribute PhonePrimary  name
		/// <summary>PhoneFax Attribute type string</summary>
		public static readonly string TAG_PHONE_FAX = "PhoneFax"; //Attribute PhoneFax  name
		/// <summary>MerchantName Attribute type string</summary>
		public static readonly string TAG_MERCHANT_NAME = "MerchantName"; //Attribute MerchantName  name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Attribute EmailAddress  name
		/// <summary>WebsiteLink Attribute type string</summary>
		public static readonly string TAG_WEBSITE_LINK = "WebsiteLink"; //Attribute WebsiteLink  name
		/// <summary>ContactName Attribute type string</summary>
		public static readonly string TAG_CONTACT_NAME = "ContactName"; //Attribute ContactName  name
		/// <summary>ContactPhone Attribute type string</summary>
		public static readonly string TAG_CONTACT_PHONE = "ContactPhone"; //Attribute ContactPhone  name
		/// <summary>ContactEmail Attribute type string</summary>
		public static readonly string TAG_CONTACT_EMAIL = "ContactEmail"; //Attribute ContactEmail  name
		// Stored procedure name
		private static readonly string SP_ENUM_NAME = "spMerchantEnum"; //Enum sp name

		/// <summary>HasError is a Property in the Merchant Class of type bool</summary>
		public bool HasError 
		{
			get{return _hasError;}
			set{_hasError = value;}
		}
		/// <summary>MerchantID is a Property in the Merchant Class of type long</summary>
		public long MerchantID 
		{
			get{return _lMerchantID;}
			set{_lMerchantID = value;}
		}
		/// <summary>AddressID is a Property in the Merchant Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime BeginDateCreated
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime EndDateCreated
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime BeginDateModified
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime EndDateModified
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>IsDeactivated is a Property in the Merchant Class of type bool</summary>
		public bool? IsDeactivated 
		{
			get{return _bIsDeactivated;}
			set{_bIsDeactivated = value;}
		}
		/// <summary>PhonePrimary is a Property in the Merchant Class of type String</summary>
		public string PhonePrimary 
		{
			get{return _strPhonePrimary;}
			set{_strPhonePrimary = value;}
		}
		/// <summary>PhoneFax is a Property in the Merchant Class of type String</summary>
		public string PhoneFax 
		{
			get{return _strPhoneFax;}
			set{_strPhoneFax = value;}
		}
		/// <summary>MerchantName is a Property in the Merchant Class of type String</summary>
		public string MerchantName 
		{
			get{return _strMerchantName;}
			set{_strMerchantName = value;}
		}
		/// <summary>EmailAddress is a Property in the Merchant Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>WebsiteLink is a Property in the Merchant Class of type String</summary>
		public string WebsiteLink 
		{
			get{return _strWebsiteLink;}
			set{_strWebsiteLink = value;}
		}
		/// <summary>ContactName is a Property in the Merchant Class of type String</summary>
		public string ContactName 
		{
			get{return _strContactName;}
			set{_strContactName = value;}
		}
		/// <summary>ContactPhone is a Property in the Merchant Class of type String</summary>
		public string ContactPhone 
		{
			get{return _strContactPhone;}
			set{_strContactPhone = value;}
		}
		/// <summary>ContactEmail is a Property in the Merchant Class of type String</summary>
		public string ContactEmail 
		{
			get{return _strContactEmail;}
			set{_strContactEmail = value;}
		}

		/// <summary>Count Property. Type: int</summary>
		public int Count 
		{
			get
			{
				_bSetup = true;
				_log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_setupCountParams();
				_cmd.Connection = _conn;
				_cmd.ExecuteNonQuery();
				try
				{
					string strTmp;
					strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
					_nCount = int.Parse(strTmp);
				}
				catch 
				{
					_nCount = 0;
				}
				return _nCount;			}
		}

		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumMerchant()
		{
		}
		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumMerchant(SqlConnection conn)
		{
			_conn = conn;
		}

		/// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
		public EnumMerchant(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

		// Implementation of IEnumerator
		/// <summary>Property of type Merchant. Returns the next Merchant in the list</summary>
		private Merchant _nextTransaction
		{
			get
			{
				Merchant o = null;
				
				if (!_bSetup)
				{
					EnumData();
				}
				if (_hasMore)
				{
					o = new Merchant(_rdr);
					_hasMore = _rdr.Read();
					if (!_hasMore)
					{
						Close();
					}
				}
				return o;
			}
		}

		/// <summary>Enumerates the Data</summary>
		public void EnumData()
		{
			if (!_bSetup)
			{
				_bSetup = true;
				_log("ENUM", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_cmd.Connection = _conn;
				_rdr = _cmd.ExecuteReader();
				_hasAny = _rdr.Read();
				_hasMore = _hasAny;
			}
		}


		/// <summary>returns the next element in the enumeration</summary>
		public object nextElement()
		{
			try
			{
				return _nextTransaction;
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
				return null;
			}
		}

		/// <summary>Returns whether or not more elements exist</summary>
		public bool hasMoreElements()
		{
			try
			{
				if (_bSetup)
				{
					EnumData();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return _hasMore;
		}

		/// <summary>Closes the datareader</summary>
		public void Close()
		{
			try
			{
				if ( _rdr != null )
				{
					_rdr.Close();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
			_rdr = null;
			_cmd = null;
		}

		/// <summary>ToString is overridden to display all properties of the Merchant Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_MERCHANT_ID + ":  " + MerchantID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_IS_DEACTIVATED + ":  " + IsDeactivated + "\n");
			sbReturn.Append(TAG_PHONE_PRIMARY + ":  " + PhonePrimary + "\n");
			sbReturn.Append(TAG_PHONE_FAX + ":  " + PhoneFax + "\n");
			sbReturn.Append(TAG_MERCHANT_NAME + ":  " + MerchantName + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_WEBSITE_LINK + ":  " + WebsiteLink + "\n");
			sbReturn.Append(TAG_CONTACT_NAME + ":  " + ContactName + "\n");
			sbReturn.Append(TAG_CONTACT_PHONE + ":  " + ContactPhone + "\n");
			sbReturn.Append(TAG_CONTACT_EMAIL + ":  " + ContactEmail + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Merchant</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_MERCHANT_ID + ">" + MerchantID + "</" + TAG_MERCHANT_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_IS_DEACTIVATED + ">" + IsDeactivated + "</" + TAG_IS_DEACTIVATED + ">\n");
			sbReturn.Append("<" + TAG_PHONE_PRIMARY + ">" + PhonePrimary + "</" + TAG_PHONE_PRIMARY + ">\n");
			sbReturn.Append("<" + TAG_PHONE_FAX + ">" + PhoneFax + "</" + TAG_PHONE_FAX + ">\n");
			sbReturn.Append("<" + TAG_MERCHANT_NAME + ">" + MerchantName + "</" + TAG_MERCHANT_NAME + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_WEBSITE_LINK + ">" + WebsiteLink + "</" + TAG_WEBSITE_LINK + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_NAME + ">" + ContactName + "</" + TAG_CONTACT_NAME + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_PHONE + ">" + ContactPhone + "</" + TAG_CONTACT_PHONE + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_EMAIL + ">" + ContactEmail + "</" + TAG_CONTACT_EMAIL + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse XML string and assign values to object</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				if ( xNodes.Count > 0 )
				{
					Parse(xNodes.Item(0));
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MERCHANT_ID);
				strTmp = xResultNode.InnerText;
				MerchantID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IS_DEACTIVATED);
				IsDeactivated = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			IsDeactivated = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_PRIMARY);
				PhonePrimary = xResultNode.InnerText;
				if (PhonePrimary.Trim().Length == 0)
					PhonePrimary = null;
			}
			catch  
			{
				PhonePrimary = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_FAX);
				PhoneFax = xResultNode.InnerText;
				if (PhoneFax.Trim().Length == 0)
					PhoneFax = null;
			}
			catch  
			{
				PhoneFax = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MERCHANT_NAME);
				MerchantName = xResultNode.InnerText;
				if (MerchantName.Trim().Length == 0)
					MerchantName = null;
			}
			catch  
			{
				MerchantName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
				if (EmailAddress.Trim().Length == 0)
					EmailAddress = null;
			}
			catch  
			{
				EmailAddress = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_WEBSITE_LINK);
				WebsiteLink = xResultNode.InnerText;
				if (WebsiteLink.Trim().Length == 0)
					WebsiteLink = null;
			}
			catch  
			{
				WebsiteLink = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_NAME);
				ContactName = xResultNode.InnerText;
				if (ContactName.Trim().Length == 0)
					ContactName = null;
			}
			catch  
			{
				ContactName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_PHONE);
				ContactPhone = xResultNode.InnerText;
				if (ContactPhone.Trim().Length == 0)
					ContactPhone = null;
			}
			catch  
			{
				ContactPhone = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_EMAIL);
				ContactEmail = xResultNode.InnerText;
				if (ContactEmail.Trim().Length == 0)
					ContactEmail = null;
			}
			catch  
			{
				ContactEmail = null;
			}
		}
		/// <summary>Prompt for values</summary>
		public void Prompt()
		{
			try 
			{
				Console.WriteLine(TAG_ADDRESS_ID + ":  ");
				try
				{
					AddressID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					AddressID = 0;
				}

				Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateCreated = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateCreated = DateTime.Parse(s);
				}
				catch  
				{
					EndDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateModified = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateModified = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateModified = DateTime.Parse(s);
				}
				catch  
				{
					EndDateModified = new DateTime();
				}

				Console.WriteLine(TAG_IS_DEACTIVATED + ":  ");
				try
				{
					IsDeactivated = Convert.ToBoolean(Console.ReadLine());
				}
				catch 
				{
					IsDeactivated = false;
				}


				Console.WriteLine(TAG_PHONE_PRIMARY + ":  ");
				PhonePrimary = Console.ReadLine();
				if (PhonePrimary.Length == 0)
				{
					PhonePrimary = null;
				}

				Console.WriteLine(TAG_PHONE_FAX + ":  ");
				PhoneFax = Console.ReadLine();
				if (PhoneFax.Length == 0)
				{
					PhoneFax = null;
				}

				Console.WriteLine(TAG_MERCHANT_NAME + ":  ");
				MerchantName = Console.ReadLine();
				if (MerchantName.Length == 0)
				{
					MerchantName = null;
				}

				Console.WriteLine(TAG_EMAIL_ADDRESS + ":  ");
				EmailAddress = Console.ReadLine();
				if (EmailAddress.Length == 0)
				{
					EmailAddress = null;
				}

				Console.WriteLine(TAG_WEBSITE_LINK + ":  ");
				WebsiteLink = Console.ReadLine();
				if (WebsiteLink.Length == 0)
				{
					WebsiteLink = null;
				}

				Console.WriteLine(TAG_CONTACT_NAME + ":  ");
				ContactName = Console.ReadLine();
				if (ContactName.Length == 0)
				{
					ContactName = null;
				}

				Console.WriteLine(TAG_CONTACT_PHONE + ":  ");
				ContactPhone = Console.ReadLine();
				if (ContactPhone.Length == 0)
				{
					ContactPhone = null;
				}

				Console.WriteLine(TAG_CONTACT_EMAIL + ":  ");
				ContactEmail = Console.ReadLine();
				if (ContactEmail.Length == 0)
				{
					ContactEmail = null;
				}

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}

		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}
		private void _setupCountParams()
		{
			SqlParameter paramCount = null;
			paramCount = new SqlParameter();
			paramCount.ParameterName = PARAM_COUNT;
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			_cmd.Parameters.Add(paramCount);
		}
		private void _setupEnumParams()
		{
			System.Text.StringBuilder sbLog = null;
			SqlParameter paramMerchantID = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramBeginDateCreated = null;
			SqlParameter paramEndDateCreated = null;
			SqlParameter paramBeginDateModified = null;
			SqlParameter paramEndDateModified = null;
			SqlParameter paramIsDeactivated = null;
			SqlParameter paramPhonePrimary = null;
			SqlParameter paramPhoneFax = null;
			SqlParameter paramMerchantName = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramWebsiteLink = null;
			SqlParameter paramContactName = null;
			SqlParameter paramContactPhone = null;
			SqlParameter paramContactEmail = null;
			DateTime dtNull = new DateTime();

			sbLog = new System.Text.StringBuilder();
				paramMerchantID = new SqlParameter("@" + TAG_MERCHANT_ID, MerchantID);
				sbLog.Append(TAG_MERCHANT_ID + "=" + MerchantID + "\n");
				paramMerchantID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramMerchantID);

				paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
				sbLog.Append(TAG_ADDRESS_ID + "=" + AddressID + "\n");
				paramAddressID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramAddressID);
			// Setup the date created param
			if (!dtNull.Equals(BeginDateCreated))
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
				sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
			}
			else
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
			}
			paramBeginDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateCreated);

			if (!dtNull.Equals(EndDateCreated))
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
				sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
			}
			else
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
			}
			paramEndDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateCreated);

			// Setup the date modified param
			if (!dtNull.Equals(BeginDateModified))
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
				sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
			}
			else
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
			}
			paramBeginDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateModified);

			if (!dtNull.Equals(EndDateModified))
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
				sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
			}
			else
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
			}
			paramEndDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateModified);

				paramIsDeactivated = new SqlParameter("@" + TAG_IS_DEACTIVATED, IsDeactivated);
				sbLog.Append(TAG_IS_DEACTIVATED + "=" + IsDeactivated + "\n");
				paramIsDeactivated.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramIsDeactivated);
			// Setup the phone primary text param
			if ( PhonePrimary != null )
			{
				paramPhonePrimary = new SqlParameter("@" + TAG_PHONE_PRIMARY, PhonePrimary);
				sbLog.Append(TAG_PHONE_PRIMARY + "=" + PhonePrimary + "\n");
			}
			else
			{
				paramPhonePrimary = new SqlParameter("@" + TAG_PHONE_PRIMARY, DBNull.Value);
			}
			paramPhonePrimary.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramPhonePrimary);

			// Setup the phone fax text param
			if ( PhoneFax != null )
			{
				paramPhoneFax = new SqlParameter("@" + TAG_PHONE_FAX, PhoneFax);
				sbLog.Append(TAG_PHONE_FAX + "=" + PhoneFax + "\n");
			}
			else
			{
				paramPhoneFax = new SqlParameter("@" + TAG_PHONE_FAX, DBNull.Value);
			}
			paramPhoneFax.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramPhoneFax);

			// Setup the merchant name text param
			if ( MerchantName != null )
			{
				paramMerchantName = new SqlParameter("@" + TAG_MERCHANT_NAME, MerchantName);
				sbLog.Append(TAG_MERCHANT_NAME + "=" + MerchantName + "\n");
			}
			else
			{
				paramMerchantName = new SqlParameter("@" + TAG_MERCHANT_NAME, DBNull.Value);
			}
			paramMerchantName.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramMerchantName);

			// Setup the email address text param
			if ( EmailAddress != null )
			{
				paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
				sbLog.Append(TAG_EMAIL_ADDRESS + "=" + EmailAddress + "\n");
			}
			else
			{
				paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, DBNull.Value);
			}
			paramEmailAddress.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEmailAddress);

			// Setup the website link text param
			if ( WebsiteLink != null )
			{
				paramWebsiteLink = new SqlParameter("@" + TAG_WEBSITE_LINK, WebsiteLink);
				sbLog.Append(TAG_WEBSITE_LINK + "=" + WebsiteLink + "\n");
			}
			else
			{
				paramWebsiteLink = new SqlParameter("@" + TAG_WEBSITE_LINK, DBNull.Value);
			}
			paramWebsiteLink.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramWebsiteLink);

			// Setup the contact name text param
			if ( ContactName != null )
			{
				paramContactName = new SqlParameter("@" + TAG_CONTACT_NAME, ContactName);
				sbLog.Append(TAG_CONTACT_NAME + "=" + ContactName + "\n");
			}
			else
			{
				paramContactName = new SqlParameter("@" + TAG_CONTACT_NAME, DBNull.Value);
			}
			paramContactName.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramContactName);

			// Setup the contact phone text param
			if ( ContactPhone != null )
			{
				paramContactPhone = new SqlParameter("@" + TAG_CONTACT_PHONE, ContactPhone);
				sbLog.Append(TAG_CONTACT_PHONE + "=" + ContactPhone + "\n");
			}
			else
			{
				paramContactPhone = new SqlParameter("@" + TAG_CONTACT_PHONE, DBNull.Value);
			}
			paramContactPhone.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramContactPhone);

			// Setup the contact email text param
			if ( ContactEmail != null )
			{
				paramContactEmail = new SqlParameter("@" + TAG_CONTACT_EMAIL, ContactEmail);
				sbLog.Append(TAG_CONTACT_EMAIL + "=" + ContactEmail + "\n");
			}
			else
			{
				paramContactEmail = new SqlParameter("@" + TAG_CONTACT_EMAIL, DBNull.Value);
			}
			paramContactEmail.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramContactEmail);

				_log("ENUM PARAMS", sbLog.ToString());
		}

		//private
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}
	}
}

