using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  EnumAddress.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumAddress
	{
		private bool _hasAny = false;
		private bool _hasMore = false;
		private bool _bSetup = false;

		private SqlCommand _cmd = null;
		private SqlDataReader _rdr = null;
		private SqlConnection _conn = null;
		
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Enum-EnumAddress";
		private ErrorCode _errorCode = null;
		private bool _hasError = false;
		private int _nCount = 0;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumAddress"; //Table name to abstract
		private static DateTime dtNull = new DateTime();
		private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

		private long _lAddressID = 0;
		private string _strState = null;
		private string _strCountry = null;
		private DateTime _dtBeginDateCreated = new DateTime();
		private DateTime _dtEndDateCreated = new DateTime();
		private DateTime _dtBeginDateModified = new DateTime();
		private DateTime _dtEndDateModified = new DateTime();
		private string _strStreetNumber1 = null;
		private string _strStreetNumber2 = null;
		private string _strCity = null;
		private string _strZip = null;
//		private string _strOrderByEnum = "ASC";
		private string _strOrderByField = DB_FIELD_ID;

		/// <summary>DB_FIELD_ID Attribute type string</summary>
		public static readonly string DB_FIELD_ID = "address_id"; //Table id field name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Attribute AddressID  name
		/// <summary>State Attribute type string</summary>
		public static readonly string TAG_STATE = "State"; //Attribute State  name
		/// <summary>Country Attribute type string</summary>
		public static readonly string TAG_COUNTRY = "Country"; //Attribute Country  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
		/// <summary>EndDateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
		/// <summary>EndDateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
		/// <summary>StreetNumber1 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_1 = "StreetNumber1"; //Attribute StreetNumber1  name
		/// <summary>StreetNumber2 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_2 = "StreetNumber2"; //Attribute StreetNumber2  name
		/// <summary>City Attribute type string</summary>
		public static readonly string TAG_CITY = "City"; //Attribute City  name
		/// <summary>Zip Attribute type string</summary>
		public static readonly string TAG_ZIP = "Zip"; //Attribute Zip  name
		// Stored procedure name
		private static readonly string SP_ENUM_NAME = "spAddressEnum"; //Enum sp name

		/// <summary>HasError is a Property in the Address Class of type bool</summary>
		public bool HasError 
		{
			get{return _hasError;}
			set{_hasError = value;}
		}
		/// <summary>AddressID is a Property in the Address Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>State is a Property in the Address Class of type String</summary>
		public string State 
		{
			get{return _strState;}
			set{_strState = value;}
		}
		/// <summary>Country is a Property in the Address Class of type String</summary>
		public string Country 
		{
			get{return _strCountry;}
			set{_strCountry = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime BeginDateCreated
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime EndDateCreated
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime BeginDateModified
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime EndDateModified
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>StreetNumber1 is a Property in the Address Class of type String</summary>
		public string StreetNumber1 
		{
			get{return _strStreetNumber1;}
			set{_strStreetNumber1 = value;}
		}
		/// <summary>StreetNumber2 is a Property in the Address Class of type String</summary>
		public string StreetNumber2 
		{
			get{return _strStreetNumber2;}
			set{_strStreetNumber2 = value;}
		}
		/// <summary>City is a Property in the Address Class of type String</summary>
		public string City 
		{
			get{return _strCity;}
			set{_strCity = value;}
		}
		/// <summary>Zip is a Property in the Address Class of type String</summary>
		public string Zip 
		{
			get{return _strZip;}
			set{_strZip = value;}
		}

		/// <summary>Count Property. Type: int</summary>
		public int Count 
		{
			get
			{
				_bSetup = true;
				_log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_setupCountParams();
				_cmd.Connection = _conn;
				_cmd.ExecuteNonQuery();
				try
				{
					string strTmp;
					strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
					_nCount = int.Parse(strTmp);
				}
				catch 
				{
					_nCount = 0;
				}
				return _nCount;			}
		}

		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumAddress()
		{
		}
		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumAddress(SqlConnection conn)
		{
			_conn = conn;
		}

		/// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
		public EnumAddress(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

		// Implementation of IEnumerator
		/// <summary>Property of type Address. Returns the next Address in the list</summary>
		private Address _nextTransaction
		{
			get
			{
				Address o = null;
				
				if (!_bSetup)
				{
					EnumData();
				}
				if (_hasMore)
				{
					o = new Address(_rdr);
					_hasMore = _rdr.Read();
					if (!_hasMore)
					{
						Close();
					}
				}
				return o;
			}
		}

		/// <summary>Enumerates the Data</summary>
		public void EnumData()
		{
			if (!_bSetup)
			{
				_bSetup = true;
				_log("ENUM", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_cmd.Connection = _conn;
				_rdr = _cmd.ExecuteReader();
				_hasAny = _rdr.Read();
				_hasMore = _hasAny;
			}
		}


		/// <summary>returns the next element in the enumeration</summary>
		public object nextElement()
		{
			try
			{
				return _nextTransaction;
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
				return null;
			}
		}

		/// <summary>Returns whether or not more elements exist</summary>
		public bool hasMoreElements()
		{
			try
			{
				if (_bSetup)
				{
					EnumData();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return _hasMore;
		}

		/// <summary>Closes the datareader</summary>
		public void Close()
		{
			try
			{
				if ( _rdr != null )
				{
					_rdr.Close();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
			_rdr = null;
			_cmd = null;
		}

		/// <summary>ToString is overridden to display all properties of the Address Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID.ToString() + "\n");
			sbReturn.Append(TAG_STATE + ":  " + State + "\n");
			sbReturn.Append(TAG_COUNTRY + ":  " + Country + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_STREET_NUMBER_1 + ":  " + StreetNumber1 + "\n");
			sbReturn.Append(TAG_STREET_NUMBER_2 + ":  " + StreetNumber2 + "\n");
			sbReturn.Append(TAG_CITY + ":  " + City + "\n");
			sbReturn.Append(TAG_ZIP + ":  " + Zip + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Address</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			sbReturn.Append("<" + TAG_STATE + ">" + State + "</" + TAG_STATE + ">\n");
			sbReturn.Append("<" + TAG_COUNTRY + ">" + Country + "</" + TAG_COUNTRY + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_STREET_NUMBER_1 + ">" + StreetNumber1 + "</" + TAG_STREET_NUMBER_1 + ">\n");
			sbReturn.Append("<" + TAG_STREET_NUMBER_2 + ">" + StreetNumber2 + "</" + TAG_STREET_NUMBER_2 + ">\n");
			sbReturn.Append("<" + TAG_CITY + ">" + City + "</" + TAG_CITY + ">\n");
			sbReturn.Append("<" + TAG_ZIP + ">" + Zip + "</" + TAG_ZIP + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse XML string and assign values to object</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				if ( xNodes.Count > 0 )
				{
					Parse(xNodes.Item(0));
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				strTmp = xResultNode.InnerText;
				AddressID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STATE);
				State = xResultNode.InnerText;
				if (State.Trim().Length == 0)
					State = null;
			}
			catch  
			{
				State = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_COUNTRY);
				Country = xResultNode.InnerText;
				if (Country.Trim().Length == 0)
					Country = null;
			}
			catch  
			{
				Country = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_1);
				StreetNumber1 = xResultNode.InnerText;
				if (StreetNumber1.Trim().Length == 0)
					StreetNumber1 = null;
			}
			catch  
			{
				StreetNumber1 = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_2);
				StreetNumber2 = xResultNode.InnerText;
				if (StreetNumber2.Trim().Length == 0)
					StreetNumber2 = null;
			}
			catch  
			{
				StreetNumber2 = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CITY);
				City = xResultNode.InnerText;
				if (City.Trim().Length == 0)
					City = null;
			}
			catch  
			{
				City = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ZIP);
				Zip = xResultNode.InnerText;
				if (Zip.Trim().Length == 0)
					Zip = null;
			}
			catch  
			{
				Zip = null;
			}
		}
		/// <summary>Prompt for values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(TAG_STATE + ":  ");
				State = Console.ReadLine();
				if (State.Length == 0)
				{
					State = null;
				}

				Console.WriteLine(TAG_COUNTRY + ":  ");
				Country = Console.ReadLine();
				if (Country.Length == 0)
				{
					Country = null;
				}
				Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateCreated = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateCreated = DateTime.Parse(s);
				}
				catch  
				{
					EndDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateModified = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateModified = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateModified = DateTime.Parse(s);
				}
				catch  
				{
					EndDateModified = new DateTime();
				}


				Console.WriteLine(TAG_STREET_NUMBER_1 + ":  ");
				StreetNumber1 = Console.ReadLine();
				if (StreetNumber1.Length == 0)
				{
					StreetNumber1 = null;
				}

				Console.WriteLine(TAG_STREET_NUMBER_2 + ":  ");
				StreetNumber2 = Console.ReadLine();
				if (StreetNumber2.Length == 0)
				{
					StreetNumber2 = null;
				}

				Console.WriteLine(TAG_CITY + ":  ");
				City = Console.ReadLine();
				if (City.Length == 0)
				{
					City = null;
				}

				Console.WriteLine(TAG_ZIP + ":  ");
				Zip = Console.ReadLine();
				if (Zip.Length == 0)
				{
					Zip = null;
				}

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}

		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}
		private void _setupCountParams()
		{
			SqlParameter paramCount = null;
			paramCount = new SqlParameter();
			paramCount.ParameterName = PARAM_COUNT;
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			_cmd.Parameters.Add(paramCount);
		}
		private void _setupEnumParams()
		{
			System.Text.StringBuilder sbLog = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramState = null;
			SqlParameter paramCountry = null;
			SqlParameter paramBeginDateCreated = null;
			SqlParameter paramEndDateCreated = null;
			SqlParameter paramBeginDateModified = null;
			SqlParameter paramEndDateModified = null;
			SqlParameter paramStreetNumber1 = null;
			SqlParameter paramStreetNumber2 = null;
			SqlParameter paramCity = null;
			SqlParameter paramZip = null;
			DateTime dtNull = new DateTime();

			sbLog = new System.Text.StringBuilder();
				paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
				sbLog.Append(TAG_ADDRESS_ID + "=" + AddressID + "\n");
				paramAddressID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramAddressID);

			// Setup the state text param
			if ( State != null )
			{
				paramState = new SqlParameter("@" + TAG_STATE, State);
				sbLog.Append(TAG_STATE + "=" + State + "\n");
			}
			else
			{
				paramState = new SqlParameter("@" + TAG_STATE, DBNull.Value);
			}
			paramState.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramState);

			// Setup the country text param
			if ( Country != null )
			{
				paramCountry = new SqlParameter("@" + TAG_COUNTRY, Country);
				sbLog.Append(TAG_COUNTRY + "=" + Country + "\n");
			}
			else
			{
				paramCountry = new SqlParameter("@" + TAG_COUNTRY, DBNull.Value);
			}
			paramCountry.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramCountry);

			// Setup the date created param
			if (!dtNull.Equals(BeginDateCreated))
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
				sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
			}
			else
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
			}
			paramBeginDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateCreated);

			if (!dtNull.Equals(EndDateCreated))
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
				sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
			}
			else
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
			}
			paramEndDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateCreated);

			// Setup the date modified param
			if (!dtNull.Equals(BeginDateModified))
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
				sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
			}
			else
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
			}
			paramBeginDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateModified);

			if (!dtNull.Equals(EndDateModified))
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
				sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
			}
			else
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
			}
			paramEndDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateModified);

			// Setup the street number 1 text param
			if ( StreetNumber1 != null )
			{
				paramStreetNumber1 = new SqlParameter("@" + TAG_STREET_NUMBER_1, StreetNumber1);
				sbLog.Append(TAG_STREET_NUMBER_1 + "=" + StreetNumber1 + "\n");
			}
			else
			{
				paramStreetNumber1 = new SqlParameter("@" + TAG_STREET_NUMBER_1, DBNull.Value);
			}
			paramStreetNumber1.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramStreetNumber1);

			// Setup the street number 2 text param
			if ( StreetNumber2 != null )
			{
				paramStreetNumber2 = new SqlParameter("@" + TAG_STREET_NUMBER_2, StreetNumber2);
				sbLog.Append(TAG_STREET_NUMBER_2 + "=" + StreetNumber2 + "\n");
			}
			else
			{
				paramStreetNumber2 = new SqlParameter("@" + TAG_STREET_NUMBER_2, DBNull.Value);
			}
			paramStreetNumber2.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramStreetNumber2);

			// Setup the city text param
			if ( City != null )
			{
				paramCity = new SqlParameter("@" + TAG_CITY, City);
				sbLog.Append(TAG_CITY + "=" + City + "\n");
			}
			else
			{
				paramCity = new SqlParameter("@" + TAG_CITY, DBNull.Value);
			}
			paramCity.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramCity);

			// Setup the zip text param
			if ( Zip != null )
			{
				paramZip = new SqlParameter("@" + TAG_ZIP, Zip);
				sbLog.Append(TAG_ZIP + "=" + Zip + "\n");
			}
			else
			{
				paramZip = new SqlParameter("@" + TAG_ZIP, DBNull.Value);
			}
			paramZip.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramZip);

				_log("ENUM PARAMS", sbLog.ToString());
		}

		//private
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}
	}
}

