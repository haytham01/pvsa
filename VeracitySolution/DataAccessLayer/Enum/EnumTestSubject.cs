using System;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using System.Data;

using Veracity.Common;
using Veracity.DataAccessLayer.Data;

namespace Veracity.DataAccessLayer.Enumeration
{

	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  EnumTestSubject.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumTestSubject
	{
		private bool _hasAny = false;
		private bool _hasMore = false;
		private bool _bSetup = false;

		private SqlCommand _cmd = null;
		private SqlDataReader _rdr = null;
		private SqlConnection _conn = null;
		
		private Config _config = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Enum-EnumTestSubject";
		private ErrorCode _errorCode = null;
		private bool _hasError = false;
		private int _nCount = 0;


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumTestSubject"; //Table name to abstract
		private static DateTime dtNull = new DateTime();
		private static readonly string PARAM_COUNT = "@COUNT"; //Sp count parameter

		private long _lTestSubjectID = 0;
		private long _lAddressID = 0;
		private DateTime _dtBeginDateCreated = new DateTime();
		private DateTime _dtEndDateCreated = new DateTime();
		private DateTime _dtBeginDateModified = new DateTime();
		private DateTime _dtEndDateModified = new DateTime();
		private string _strDriverLicenseNumber = null;
		private string _strFirstName = null;
		private string _strMiddleName = null;
		private string _strLastName = null;
		private string _strPrimaryPhone = null;
		private string _strEmailAddress = null;
		private string _strCode = null;
		private string _strIdentificationRawData = null;
		private string _strNotes = null;
//		private string _strOrderByEnum = "ASC";
		private string _strOrderByField = DB_FIELD_ID;

		/// <summary>DB_FIELD_ID Attribute type string</summary>
		public static readonly string DB_FIELD_ID = "test_subject_id"; //Table id field name
		/// <summary>TestSubjectID Attribute type string</summary>
		public static readonly string TAG_TEST_SUBJECT_ID = "TestSubjectID"; //Attribute TestSubjectID  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Attribute AddressID  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Attribute DateCreated  name
		/// <summary>EndDateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Attribute DateCreated  name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Attribute DateModified  name
		/// <summary>EndDateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Attribute DateModified  name
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		public static readonly string TAG_DRIVER_LICENSE_NUMBER = "DriverLicenseNumber"; //Attribute DriverLicenseNumber  name
		/// <summary>FirstName Attribute type string</summary>
		public static readonly string TAG_FIRST_NAME = "FirstName"; //Attribute FirstName  name
		/// <summary>MiddleName Attribute type string</summary>
		public static readonly string TAG_MIDDLE_NAME = "MiddleName"; //Attribute MiddleName  name
		/// <summary>LastName Attribute type string</summary>
		public static readonly string TAG_LAST_NAME = "LastName"; //Attribute LastName  name
		/// <summary>PrimaryPhone Attribute type string</summary>
		public static readonly string TAG_PRIMARY_PHONE = "PrimaryPhone"; //Attribute PrimaryPhone  name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Attribute EmailAddress  name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Attribute Code  name
		/// <summary>IdentificationRawData Attribute type string</summary>
		public static readonly string TAG_IDENTIFICATION_RAW_DATA = "IdentificationRawData"; //Attribute IdentificationRawData  name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Attribute Notes  name
		// Stored procedure name
		private static readonly string SP_ENUM_NAME = "spTestSubjectEnum"; //Enum sp name

		/// <summary>HasError is a Property in the TestSubject Class of type bool</summary>
		public bool HasError 
		{
			get{return _hasError;}
			set{_hasError = value;}
		}
		/// <summary>TestSubjectID is a Property in the TestSubject Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>AddressID is a Property in the TestSubject Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime BeginDateCreated
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>Property DateCreated. Type: DateTime</summary>
		public DateTime EndDateCreated
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime BeginDateModified
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>Property DateModified. Type: DateTime</summary>
		public DateTime EndDateModified
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>DriverLicenseNumber is a Property in the TestSubject Class of type String</summary>
		public string DriverLicenseNumber 
		{
			get{return _strDriverLicenseNumber;}
			set{_strDriverLicenseNumber = value;}
		}
		/// <summary>FirstName is a Property in the TestSubject Class of type String</summary>
		public string FirstName 
		{
			get{return _strFirstName;}
			set{_strFirstName = value;}
		}
		/// <summary>MiddleName is a Property in the TestSubject Class of type String</summary>
		public string MiddleName 
		{
			get{return _strMiddleName;}
			set{_strMiddleName = value;}
		}
		/// <summary>LastName is a Property in the TestSubject Class of type String</summary>
		public string LastName 
		{
			get{return _strLastName;}
			set{_strLastName = value;}
		}
		/// <summary>PrimaryPhone is a Property in the TestSubject Class of type String</summary>
		public string PrimaryPhone 
		{
			get{return _strPrimaryPhone;}
			set{_strPrimaryPhone = value;}
		}
		/// <summary>EmailAddress is a Property in the TestSubject Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>Code is a Property in the TestSubject Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>IdentificationRawData is a Property in the TestSubject Class of type String</summary>
		public string IdentificationRawData 
		{
			get{return _strIdentificationRawData;}
			set{_strIdentificationRawData = value;}
		}
		/// <summary>Notes is a Property in the TestSubject Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}

		/// <summary>Count Property. Type: int</summary>
		public int Count 
		{
			get
			{
				_bSetup = true;
				_log("ENUM COUNT", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_setupCountParams();
				_cmd.Connection = _conn;
				_cmd.ExecuteNonQuery();
				try
				{
					string strTmp;
					strTmp = _cmd.Parameters[PARAM_COUNT].Value.ToString();
					_nCount = int.Parse(strTmp);
				}
				catch 
				{
					_nCount = 0;
				}
				return _nCount;			}
		}

		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumTestSubject()
		{
		}
		/// <summary>Contructor takes 1 parameter: SqlConnection</summary>
		public EnumTestSubject(SqlConnection conn)
		{
			_conn = conn;
		}

		/// <summary>Constructor takes 2 parameters: SqlConnection and Config</summary>
		public EnumTestSubject(SqlConnection conn, Config pConfig)
		{
			_conn = conn;
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}

		// Implementation of IEnumerator
		/// <summary>Property of type TestSubject. Returns the next TestSubject in the list</summary>
		private TestSubject _nextTransaction
		{
			get
			{
				TestSubject o = null;
				
				if (!_bSetup)
				{
					EnumData();
				}
				if (_hasMore)
				{
					o = new TestSubject(_rdr);
					_hasMore = _rdr.Read();
					if (!_hasMore)
					{
						Close();
					}
				}
				return o;
			}
		}

		/// <summary>Enumerates the Data</summary>
		public void EnumData()
		{
			if (!_bSetup)
			{
				_bSetup = true;
				_log("ENUM", "Calling sp " + SP_ENUM_NAME);
				// if necessary, close the old reader
				if ( (_cmd != null) || (_rdr != null) )
				{
					Close();
				}
				_cmd = new SqlCommand(SP_ENUM_NAME, _conn);
				_cmd.CommandType = CommandType.StoredProcedure;
				_setupEnumParams();
				_cmd.Connection = _conn;
				_rdr = _cmd.ExecuteReader();
				_hasAny = _rdr.Read();
				_hasMore = _hasAny;
			}
		}


		/// <summary>returns the next element in the enumeration</summary>
		public object nextElement()
		{
			try
			{
				return _nextTransaction;
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
				return null;
			}
		}

		/// <summary>Returns whether or not more elements exist</summary>
		public bool hasMoreElements()
		{
			try
			{
				if (_bSetup)
				{
					EnumData();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return _hasMore;
		}

		/// <summary>Closes the datareader</summary>
		public void Close()
		{
			try
			{
				if ( _rdr != null )
				{
					_rdr.Close();
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
			_rdr = null;
			_cmd = null;
		}

		/// <summary>ToString is overridden to display all properties of the TestSubject Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_TEST_SUBJECT_ID + ":  " + TestSubjectID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_DRIVER_LICENSE_NUMBER + ":  " + DriverLicenseNumber + "\n");
			sbReturn.Append(TAG_FIRST_NAME + ":  " + FirstName + "\n");
			sbReturn.Append(TAG_MIDDLE_NAME + ":  " + MiddleName + "\n");
			sbReturn.Append(TAG_LAST_NAME + ":  " + LastName + "\n");
			sbReturn.Append(TAG_PRIMARY_PHONE + ":  " + PrimaryPhone + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_IDENTIFICATION_RAW_DATA + ":  " + IdentificationRawData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestSubject</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_TEST_SUBJECT_ID + ">" + TestSubjectID + "</" + TAG_TEST_SUBJECT_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_DRIVER_LICENSE_NUMBER + ">" + DriverLicenseNumber + "</" + TAG_DRIVER_LICENSE_NUMBER + ">\n");
			sbReturn.Append("<" + TAG_FIRST_NAME + ">" + FirstName + "</" + TAG_FIRST_NAME + ">\n");
			sbReturn.Append("<" + TAG_MIDDLE_NAME + ">" + MiddleName + "</" + TAG_MIDDLE_NAME + ">\n");
			sbReturn.Append("<" + TAG_LAST_NAME + ">" + LastName + "</" + TAG_LAST_NAME + ">\n");
			sbReturn.Append("<" + TAG_PRIMARY_PHONE + ">" + PrimaryPhone + "</" + TAG_PRIMARY_PHONE + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_IDENTIFICATION_RAW_DATA + ">" + IdentificationRawData + "</" + TAG_IDENTIFICATION_RAW_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse XML string and assign values to object</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				if ( xNodes.Count > 0 )
				{
					Parse(xNodes.Item(0));
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_SUBJECT_ID);
				strTmp = xResultNode.InnerText;
				TestSubjectID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DRIVER_LICENSE_NUMBER);
				DriverLicenseNumber = xResultNode.InnerText;
				if (DriverLicenseNumber.Trim().Length == 0)
					DriverLicenseNumber = null;
			}
			catch  
			{
				DriverLicenseNumber = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_FIRST_NAME);
				FirstName = xResultNode.InnerText;
				if (FirstName.Trim().Length == 0)
					FirstName = null;
			}
			catch  
			{
				FirstName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MIDDLE_NAME);
				MiddleName = xResultNode.InnerText;
				if (MiddleName.Trim().Length == 0)
					MiddleName = null;
			}
			catch  
			{
				MiddleName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_LAST_NAME);
				LastName = xResultNode.InnerText;
				if (LastName.Trim().Length == 0)
					LastName = null;
			}
			catch  
			{
				LastName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PRIMARY_PHONE);
				PrimaryPhone = xResultNode.InnerText;
				if (PrimaryPhone.Trim().Length == 0)
					PrimaryPhone = null;
			}
			catch  
			{
				PrimaryPhone = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
				if (EmailAddress.Trim().Length == 0)
					EmailAddress = null;
			}
			catch  
			{
				EmailAddress = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
				if (Code.Trim().Length == 0)
					Code = null;
			}
			catch  
			{
				Code = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IDENTIFICATION_RAW_DATA);
				IdentificationRawData = xResultNode.InnerText;
				if (IdentificationRawData.Trim().Length == 0)
					IdentificationRawData = null;
			}
			catch  
			{
				IdentificationRawData = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
				if (Notes.Trim().Length == 0)
					Notes = null;
			}
			catch  
			{
				Notes = null;
			}
		}
		/// <summary>Prompt for values</summary>
		public void Prompt()
		{
			try 
			{
				Console.WriteLine(TAG_ADDRESS_ID + ":  ");
				try
				{
					AddressID = (long)Convert.ToInt32(Console.ReadLine());
				}
				catch 
				{
					AddressID = 0;
				}

				Console.WriteLine(TAG_BEGIN_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateCreated = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_CREATED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateCreated = DateTime.Parse(s);
				}
				catch  
				{
					EndDateCreated = new DateTime();
				}

				Console.WriteLine(TAG_BEGIN_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					BeginDateModified = DateTime.Parse(s);
				}
				catch 
				{
					BeginDateModified = new DateTime();
				}

				Console.WriteLine(TAG_END_DATE_MODIFIED + ":  ");
				try
				{
					string s = Console.ReadLine();
					EndDateModified = DateTime.Parse(s);
				}
				catch  
				{
					EndDateModified = new DateTime();
				}


				Console.WriteLine(TAG_DRIVER_LICENSE_NUMBER + ":  ");
				DriverLicenseNumber = Console.ReadLine();
				if (DriverLicenseNumber.Length == 0)
				{
					DriverLicenseNumber = null;
				}

				Console.WriteLine(TAG_FIRST_NAME + ":  ");
				FirstName = Console.ReadLine();
				if (FirstName.Length == 0)
				{
					FirstName = null;
				}

				Console.WriteLine(TAG_MIDDLE_NAME + ":  ");
				MiddleName = Console.ReadLine();
				if (MiddleName.Length == 0)
				{
					MiddleName = null;
				}

				Console.WriteLine(TAG_LAST_NAME + ":  ");
				LastName = Console.ReadLine();
				if (LastName.Length == 0)
				{
					LastName = null;
				}

				Console.WriteLine(TAG_PRIMARY_PHONE + ":  ");
				PrimaryPhone = Console.ReadLine();
				if (PrimaryPhone.Length == 0)
				{
					PrimaryPhone = null;
				}

				Console.WriteLine(TAG_EMAIL_ADDRESS + ":  ");
				EmailAddress = Console.ReadLine();
				if (EmailAddress.Length == 0)
				{
					EmailAddress = null;
				}

				Console.WriteLine(TAG_CODE + ":  ");
				Code = Console.ReadLine();
				if (Code.Length == 0)
				{
					Code = null;
				}

				Console.WriteLine(TAG_IDENTIFICATION_RAW_DATA + ":  ");
				IdentificationRawData = Console.ReadLine();
				if (IdentificationRawData.Length == 0)
				{
					IdentificationRawData = null;
				}

				Console.WriteLine(TAG_NOTES + ":  ");
				Notes = Console.ReadLine();
				if (Notes.Length == 0)
				{
					Notes = null;
				}

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}

		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}
		private void _setupCountParams()
		{
			SqlParameter paramCount = null;
			paramCount = new SqlParameter();
			paramCount.ParameterName = PARAM_COUNT;
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			_cmd.Parameters.Add(paramCount);
		}
		private void _setupEnumParams()
		{
			System.Text.StringBuilder sbLog = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramBeginDateCreated = null;
			SqlParameter paramEndDateCreated = null;
			SqlParameter paramBeginDateModified = null;
			SqlParameter paramEndDateModified = null;
			SqlParameter paramDriverLicenseNumber = null;
			SqlParameter paramFirstName = null;
			SqlParameter paramMiddleName = null;
			SqlParameter paramLastName = null;
			SqlParameter paramPrimaryPhone = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramCode = null;
			SqlParameter paramIdentificationRawData = null;
			SqlParameter paramNotes = null;
			DateTime dtNull = new DateTime();

			sbLog = new System.Text.StringBuilder();
				paramTestSubjectID = new SqlParameter("@" + TAG_TEST_SUBJECT_ID, TestSubjectID);
				sbLog.Append(TAG_TEST_SUBJECT_ID + "=" + TestSubjectID + "\n");
				paramTestSubjectID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramTestSubjectID);

				paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
				sbLog.Append(TAG_ADDRESS_ID + "=" + AddressID + "\n");
				paramAddressID.Direction = ParameterDirection.Input;
				_cmd.Parameters.Add(paramAddressID);
			// Setup the date created param
			if (!dtNull.Equals(BeginDateCreated))
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, BeginDateCreated);
				sbLog.Append(TAG_BEGIN_DATE_CREATED + "=" + BeginDateCreated.ToLongDateString());
			}
			else
			{
				paramBeginDateCreated = new SqlParameter("@" + TAG_BEGIN_DATE_CREATED, DBNull.Value);
			}
			paramBeginDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateCreated);

			if (!dtNull.Equals(EndDateCreated))
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, EndDateCreated);
				sbLog.Append(TAG_END_DATE_CREATED + "=" + EndDateCreated.ToLongDateString());
			}
			else
			{
				paramEndDateCreated = new SqlParameter("@" + TAG_END_DATE_CREATED, DBNull.Value);
			}
			paramEndDateCreated.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateCreated);

			// Setup the date modified param
			if (!dtNull.Equals(BeginDateModified))
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, BeginDateModified);
				sbLog.Append(TAG_BEGIN_DATE_MODIFIED + "=" + BeginDateModified.ToLongDateString());
			}
			else
			{
				paramBeginDateModified = new SqlParameter("@" + TAG_BEGIN_DATE_MODIFIED, DBNull.Value);
			}
			paramBeginDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramBeginDateModified);

			if (!dtNull.Equals(EndDateModified))
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, EndDateModified);
				sbLog.Append(TAG_END_DATE_MODIFIED + "=" + EndDateModified.ToLongDateString());
			}
			else
			{
				paramEndDateModified = new SqlParameter("@" + TAG_END_DATE_MODIFIED, DBNull.Value);
			}
			paramEndDateModified.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEndDateModified);

			// Setup the driver license number text param
			if ( DriverLicenseNumber != null )
			{
				paramDriverLicenseNumber = new SqlParameter("@" + TAG_DRIVER_LICENSE_NUMBER, DriverLicenseNumber);
				sbLog.Append(TAG_DRIVER_LICENSE_NUMBER + "=" + DriverLicenseNumber + "\n");
			}
			else
			{
				paramDriverLicenseNumber = new SqlParameter("@" + TAG_DRIVER_LICENSE_NUMBER, DBNull.Value);
			}
			paramDriverLicenseNumber.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramDriverLicenseNumber);

			// Setup the first name text param
			if ( FirstName != null )
			{
				paramFirstName = new SqlParameter("@" + TAG_FIRST_NAME, FirstName);
				sbLog.Append(TAG_FIRST_NAME + "=" + FirstName + "\n");
			}
			else
			{
				paramFirstName = new SqlParameter("@" + TAG_FIRST_NAME, DBNull.Value);
			}
			paramFirstName.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramFirstName);

			// Setup the middle name text param
			if ( MiddleName != null )
			{
				paramMiddleName = new SqlParameter("@" + TAG_MIDDLE_NAME, MiddleName);
				sbLog.Append(TAG_MIDDLE_NAME + "=" + MiddleName + "\n");
			}
			else
			{
				paramMiddleName = new SqlParameter("@" + TAG_MIDDLE_NAME, DBNull.Value);
			}
			paramMiddleName.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramMiddleName);

			// Setup the last name text param
			if ( LastName != null )
			{
				paramLastName = new SqlParameter("@" + TAG_LAST_NAME, LastName);
				sbLog.Append(TAG_LAST_NAME + "=" + LastName + "\n");
			}
			else
			{
				paramLastName = new SqlParameter("@" + TAG_LAST_NAME, DBNull.Value);
			}
			paramLastName.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramLastName);

			// Setup the primary phone text param
			if ( PrimaryPhone != null )
			{
				paramPrimaryPhone = new SqlParameter("@" + TAG_PRIMARY_PHONE, PrimaryPhone);
				sbLog.Append(TAG_PRIMARY_PHONE + "=" + PrimaryPhone + "\n");
			}
			else
			{
				paramPrimaryPhone = new SqlParameter("@" + TAG_PRIMARY_PHONE, DBNull.Value);
			}
			paramPrimaryPhone.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramPrimaryPhone);

			// Setup the email address text param
			if ( EmailAddress != null )
			{
				paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
				sbLog.Append(TAG_EMAIL_ADDRESS + "=" + EmailAddress + "\n");
			}
			else
			{
				paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, DBNull.Value);
			}
			paramEmailAddress.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramEmailAddress);

			// Setup the code text param
			if ( Code != null )
			{
				paramCode = new SqlParameter("@" + TAG_CODE, Code);
				sbLog.Append(TAG_CODE + "=" + Code + "\n");
			}
			else
			{
				paramCode = new SqlParameter("@" + TAG_CODE, DBNull.Value);
			}
			paramCode.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramCode);

			// Setup the identification raw data text param
			if ( IdentificationRawData != null )
			{
				paramIdentificationRawData = new SqlParameter("@" + TAG_IDENTIFICATION_RAW_DATA, IdentificationRawData);
				sbLog.Append(TAG_IDENTIFICATION_RAW_DATA + "=" + IdentificationRawData + "\n");
			}
			else
			{
				paramIdentificationRawData = new SqlParameter("@" + TAG_IDENTIFICATION_RAW_DATA, DBNull.Value);
			}
			paramIdentificationRawData.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramIdentificationRawData);

			// Setup the notes text param
			if ( Notes != null )
			{
				paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
				sbLog.Append(TAG_NOTES + "=" + Notes + "\n");
			}
			else
			{
				paramNotes = new SqlParameter("@" + TAG_NOTES, DBNull.Value);
			}
			paramNotes.Direction = ParameterDirection.Input;
			_cmd.Parameters.Add(paramNotes);

				_log("ENUM PARAMS", sbLog.ToString());
		}

		//private
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}
	}
}

