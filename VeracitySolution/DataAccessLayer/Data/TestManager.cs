using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestManager.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the TestManager database table.
	/// </summary>
	public class TestManager
	{
		//Attributes
		/// <summary>TestManagerID Attribute type String</summary>
		private long _lTestManagerID = 0;
		/// <summary>TestTemplateID Attribute type String</summary>
		private long _lTestTemplateID = 0;
		/// <summary>TestLanguageID Attribute type String</summary>
		private long _lTestLanguageID = 0;
		/// <summary>TestSubjectID Attribute type String</summary>
		private long _lTestSubjectID = 0;
		/// <summary>ScreenID Attribute type String</summary>
		private long _lScreenID = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DateBeginTest Attribute type String</summary>
		private DateTime _dtDateBeginTest = dtNull;
		/// <summary>DateEndTest Attribute type String</summary>
		private DateTime _dtDateEndTest = dtNull;
		/// <summary>ReportData Attribute type String</summary>
		private string _strReportData = null;
		/// <summary>Notes Attribute type String</summary>
		private string _strNotes = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-TestManager";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class TestManager and is of type bool</summary>
		public static readonly string ENTITY_NAME = "TestManager"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "test_manager_id"; //Table id field name
		/// <summary>test_template_id Database field </summary>
		public static readonly string DB_FIELD_TEST_TEMPLATE_ID = "test_template_id"; //Table TestTemplateID field name
		/// <summary>test_language_id Database field </summary>
		public static readonly string DB_FIELD_TEST_LANGUAGE_ID = "test_language_id"; //Table TestLanguageID field name
		/// <summary>test_subject_id Database field </summary>
		public static readonly string DB_FIELD_TEST_SUBJECT_ID = "test_subject_id"; //Table TestSubjectID field name
		/// <summary>screen_id Database field </summary>
		public static readonly string DB_FIELD_SCREEN_ID = "screen_id"; //Table ScreenID field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>date_begin_test Database field </summary>
		public static readonly string DB_FIELD_DATE_BEGIN_TEST = "date_begin_test"; //Table DateBeginTest field name
		/// <summary>date_end_test Database field </summary>
		public static readonly string DB_FIELD_DATE_END_TEST = "date_end_test"; //Table DateEndTest field name
		/// <summary>report_data Database field </summary>
		public static readonly string DB_FIELD_REPORT_DATA = "report_data"; //Table ReportData field name
		/// <summary>notes Database field </summary>
		public static readonly string DB_FIELD_NOTES = "notes"; //Table Notes field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "TestManagerID"; //Attribute id  name
		/// <summary>TestTemplateID Attribute type string</summary>
		public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Table TestTemplateID field name
		/// <summary>TestLanguageID Attribute type string</summary>
		public static readonly string TAG_TEST_LANGUAGE_ID = "TestLanguageID"; //Table TestLanguageID field name
		/// <summary>TestSubjectID Attribute type string</summary>
		public static readonly string TAG_TEST_SUBJECT_ID = "TestSubjectID"; //Table TestSubjectID field name
		/// <summary>ScreenID Attribute type string</summary>
		public static readonly string TAG_SCREEN_ID = "ScreenID"; //Table ScreenID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DateBeginTest Attribute type string</summary>
		public static readonly string TAG_DATE_BEGIN_TEST = "DateBeginTest"; //Table DateBeginTest field name
		/// <summary>DateEndTest Attribute type string</summary>
		public static readonly string TAG_DATE_END_TEST = "DateEndTest"; //Table DateEndTest field name
		/// <summary>ReportData Attribute type string</summary>
		public static readonly string TAG_REPORT_DATA = "ReportData"; //Table ReportData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spTestManagerInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spTestManagerUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spTestManagerDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spTestManagerLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spTestManagerExist"; //Exist sp name

		//properties
		/// <summary>TestManagerID is a Property in the TestManager Class of type long</summary>
		public long TestManagerID 
		{
			get{return _lTestManagerID;}
			set{_lTestManagerID = value;}
		}
		/// <summary>TestTemplateID is a Property in the TestManager Class of type long</summary>
		public long TestTemplateID 
		{
			get{return _lTestTemplateID;}
			set{_lTestTemplateID = value;}
		}
		/// <summary>TestLanguageID is a Property in the TestManager Class of type long</summary>
		public long TestLanguageID 
		{
			get{return _lTestLanguageID;}
			set{_lTestLanguageID = value;}
		}
		/// <summary>TestSubjectID is a Property in the TestManager Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>ScreenID is a Property in the TestManager Class of type long</summary>
		public long ScreenID 
		{
			get{return _lScreenID;}
			set{_lScreenID = value;}
		}
		/// <summary>DateCreated is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DateBeginTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateBeginTest 
		{
			get{return _dtDateBeginTest;}
			set{_dtDateBeginTest = value;}
		}
		/// <summary>DateEndTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateEndTest 
		{
			get{return _dtDateEndTest;}
			set{_dtDateEndTest = value;}
		}
		/// <summary>ReportData is a Property in the TestManager Class of type String</summary>
		public string ReportData 
		{
			get{return _strReportData;}
			set{_strReportData = value;}
		}
		/// <summary>Notes is a Property in the TestManager Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>HasError Property in class TestManager and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestManager and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>TestManager empty constructor</summary>
		public TestManager()
		{
		}
		/// <summary>TestManager constructor takes a Config</summary>
		public TestManager(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>TestManager constructor takes TestManagerID and a SqlConnection</summary>
		public TestManager(long l, SqlConnection conn) 
		{
			TestManagerID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>TestManager Constructor takes pStrData and Config</summary>
		public TestManager(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>TestManager Constructor takes SqlDataReader</summary>
		public TestManager(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestManager Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestManagerID.ToString() + "\n");
			sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
			sbReturn.Append(TAG_TEST_LANGUAGE_ID + ":  " + TestLanguageID + "\n");
			sbReturn.Append(TAG_TEST_SUBJECT_ID + ":  " + TestSubjectID + "\n");
			sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(DateBeginTest))
			{
				sbReturn.Append(TAG_DATE_BEGIN_TEST + ":  " + DateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(DateEndTest))
			{
				sbReturn.Append(TAG_DATE_END_TEST + ":  " + DateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_END_TEST + ":\n");
			}
			sbReturn.Append(TAG_REPORT_DATA + ":  " + ReportData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestManager</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<TestManager>\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestManagerID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_LANGUAGE_ID + ">" + TestLanguageID + "</" + TAG_TEST_LANGUAGE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_SUBJECT_ID + ">" + TestSubjectID + "</" + TAG_TEST_SUBJECT_ID + ">\n");
			sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(DateBeginTest))
			{
				sbReturn.Append("<" + TAG_DATE_BEGIN_TEST + ">" + DateBeginTest.ToString() + "</" + TAG_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_BEGIN_TEST + "></" + TAG_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(DateEndTest))
			{
				sbReturn.Append("<" + TAG_DATE_END_TEST + ">" + DateEndTest.ToString() + "</" + TAG_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_END_TEST + "></" + TAG_DATE_END_TEST + ">\n");
			}
			sbReturn.Append("<" + TAG_REPORT_DATA + ">" + ReportData + "</" + TAG_REPORT_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</TestManager>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestManagerID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
				TestTemplateID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestTemplateID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_LANGUAGE_ID);
				TestLanguageID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestLanguageID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_SUBJECT_ID);
				TestSubjectID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestSubjectID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
				ScreenID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ScreenID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_BEGIN_TEST);
				DateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_END_TEST);
				DateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REPORT_DATA);
				ReportData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with test_manager_id equal to the current object's TestManagerID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where test_manager_id equal to the current object's TestManagerID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where test_manager_id equal to the current object's TestManagerID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(TestManager.TAG_TEST_TEMPLATE_ID + ":  ");
				TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_TEST_LANGUAGE_ID + ":  ");
				TestLanguageID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_TEST_SUBJECT_ID + ":  ");
				TestSubjectID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(TestManager.TAG_SCREEN_ID + ":  ");
				ScreenID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_BEGIN_TEST + ":  ");
					DateBeginTest = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateBeginTest = new DateTime();
				}
				try
				{
					Console.WriteLine(TestManager.TAG_DATE_END_TEST + ":  ");
					DateEndTest = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateEndTest = new DateTime();
				}

				Console.WriteLine(TestManager.TAG_REPORT_DATA + ":  ");
				ReportData = Console.ReadLine();

				Console.WriteLine(TestManager.TAG_NOTES + ":  ");
				Notes = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestTemplateID = null;
			SqlParameter paramTestLanguageID = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramScreenID = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramDateBeginTest = null;
			SqlParameter paramDateEndTest = null;
			SqlParameter paramReportData = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
			paramTestTemplateID.DbType = DbType.Int32;
			paramTestTemplateID.Direction = ParameterDirection.Input;

			paramTestLanguageID = new SqlParameter("@" + TAG_TEST_LANGUAGE_ID, TestLanguageID);
			paramTestLanguageID.DbType = DbType.Int32;
			paramTestLanguageID.Direction = ParameterDirection.Input;

			paramTestSubjectID = new SqlParameter("@" + TAG_TEST_SUBJECT_ID, TestSubjectID);
			paramTestSubjectID.DbType = DbType.Int32;
			paramTestSubjectID.Direction = ParameterDirection.Input;

			paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
			paramScreenID.DbType = DbType.Int32;
			paramScreenID.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			if (!dtNull.Equals(DateBeginTest))
			{
				paramDateBeginTest = new SqlParameter("@" + TAG_DATE_BEGIN_TEST, DateBeginTest);
			}
			else
			{
				paramDateBeginTest = new SqlParameter("@" + TAG_DATE_BEGIN_TEST, DBNull.Value);
			}
			paramDateBeginTest.DbType = DbType.DateTime;
			paramDateBeginTest.Direction = ParameterDirection.Input;

			if (!dtNull.Equals(DateEndTest))
			{
				paramDateEndTest = new SqlParameter("@" + TAG_DATE_END_TEST, DateEndTest);
			}
			else
			{
				paramDateEndTest = new SqlParameter("@" + TAG_DATE_END_TEST, DBNull.Value);
			}
			paramDateEndTest.DbType = DbType.DateTime;
			paramDateEndTest.Direction = ParameterDirection.Input;

			paramReportData = new SqlParameter("@" + TAG_REPORT_DATA, ReportData);
			paramReportData.DbType = DbType.String;
			paramReportData.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramTestTemplateID);
			cmd.Parameters.Add(paramTestLanguageID);
			cmd.Parameters.Add(paramTestSubjectID);
			cmd.Parameters.Add(paramScreenID);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramDateBeginTest);
			cmd.Parameters.Add(paramDateEndTest);
			cmd.Parameters.Add(paramReportData);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			TestManagerID = long.Parse(strTmp);

			// cleanup to help GC
			paramTestTemplateID = null;
			paramTestLanguageID = null;
			paramTestSubjectID = null;
			paramScreenID = null;
			paramDateCreated = null;
			paramDateBeginTest = null;
			paramDateEndTest = null;
			paramReportData = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramTestManagerID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramTestManagerID = new SqlParameter("@" + TAG_ID, TestManagerID);
			paramTestManagerID.Direction = ParameterDirection.Input;
			paramTestManagerID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramTestManagerID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramTestManagerID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestManagerID = null;
			SqlParameter paramTestTemplateID = null;
			SqlParameter paramTestLanguageID = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramScreenID = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramDateBeginTest = null;
			SqlParameter paramDateEndTest = null;
			SqlParameter paramReportData = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramTestManagerID = new SqlParameter("@" + TAG_ID, TestManagerID);
			paramTestManagerID.DbType = DbType.Int32;
			paramTestManagerID.Direction = ParameterDirection.Input;


			paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
			paramTestTemplateID.DbType = DbType.Int32;
			paramTestTemplateID.Direction = ParameterDirection.Input;

			paramTestLanguageID = new SqlParameter("@" + TAG_TEST_LANGUAGE_ID, TestLanguageID);
			paramTestLanguageID.DbType = DbType.Int32;
			paramTestLanguageID.Direction = ParameterDirection.Input;

			paramTestSubjectID = new SqlParameter("@" + TAG_TEST_SUBJECT_ID, TestSubjectID);
			paramTestSubjectID.DbType = DbType.Int32;
			paramTestSubjectID.Direction = ParameterDirection.Input;

			paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
			paramScreenID.DbType = DbType.Int32;
			paramScreenID.Direction = ParameterDirection.Input;


				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			if (!dtNull.Equals(DateBeginTest))
			{
				paramDateBeginTest = new SqlParameter("@" + TAG_DATE_BEGIN_TEST, DateBeginTest);
			}
			else
			{
				paramDateBeginTest = new SqlParameter("@" + TAG_DATE_BEGIN_TEST, DBNull.Value);
			}
			paramDateBeginTest.DbType = DbType.DateTime;
			paramDateBeginTest.Direction = ParameterDirection.Input;

			if (!dtNull.Equals(DateEndTest))
			{
				paramDateEndTest = new SqlParameter("@" + TAG_DATE_END_TEST, DateEndTest);
			}
			else
			{
				paramDateEndTest = new SqlParameter("@" + TAG_DATE_END_TEST, DBNull.Value);
			}
			paramDateEndTest.DbType = DbType.DateTime;
			paramDateEndTest.Direction = ParameterDirection.Input;

			paramReportData = new SqlParameter("@" + TAG_REPORT_DATA, ReportData);
			paramReportData.DbType = DbType.String;
			paramReportData.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramTestManagerID);
			cmd.Parameters.Add(paramTestTemplateID);
			cmd.Parameters.Add(paramTestLanguageID);
			cmd.Parameters.Add(paramTestSubjectID);
			cmd.Parameters.Add(paramScreenID);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramDateBeginTest);
			cmd.Parameters.Add(paramDateEndTest);
			cmd.Parameters.Add(paramReportData);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			TestManagerID = long.Parse(s);

			// cleanup
			paramTestManagerID = null;
			paramTestTemplateID = null;
			paramTestLanguageID = null;
			paramTestSubjectID = null;
			paramScreenID = null;
			paramDateModified = null;
			paramDateBeginTest = null;
			paramDateEndTest = null;
			paramReportData = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestManagerID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramTestManagerID = new SqlParameter("@" + TAG_ID, TestManagerID);
			paramTestManagerID.DbType = DbType.Int32;
			paramTestManagerID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramTestManagerID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramTestManagerID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestManagerID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramTestManagerID = new SqlParameter("@" + TAG_ID, TestManagerID);
			paramTestManagerID.DbType = DbType.Int32;
			paramTestManagerID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramTestManagerID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramTestManagerID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.TestManagerID = long.Parse(rdr[DB_FIELD_ID].ToString());
			try
			{
			this.TestTemplateID = Convert.ToInt32(rdr[DB_FIELD_TEST_TEMPLATE_ID].ToString().Trim());
			}
			catch{}
			try
			{
			this.TestLanguageID = Convert.ToInt32(rdr[DB_FIELD_TEST_LANGUAGE_ID].ToString().Trim());
			}
			catch{}
			try
			{
			this.TestSubjectID = Convert.ToInt32(rdr[DB_FIELD_TEST_SUBJECT_ID].ToString().Trim());
			}
			catch{}
			try
			{
			this.ScreenID = Convert.ToInt32(rdr[DB_FIELD_SCREEN_ID].ToString().Trim());
			}
			catch{}
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateBeginTest = DateTime.Parse(rdr[DB_FIELD_DATE_BEGIN_TEST].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateEndTest = DateTime.Parse(rdr[DB_FIELD_DATE_END_TEST].ToString());
			}
			catch 
			{
			}
			this.ReportData = rdr[DB_FIELD_REPORT_DATA].ToString().Trim();
			this.Notes = rdr[DB_FIELD_NOTES].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestManagerID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestManagerID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + TestManagerID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestManagerID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestManagerID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF TestManager CLASS FILE
