using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Apikey.cs
	/// History
	/// ----------------------------------------------------
	/// 001	HA	1/4/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Apikey database table.
	/// </summary>
	public class Apikey
	{
		//Attributes
		/// <summary>ApikeyID Attribute type String</summary>
		private long _lApikeyID = 0;
		/// <summary>ClientID Attribute type String</summary>
		private long _lClientID = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DateExpiration Attribute type String</summary>
		private DateTime _dtDateExpiration = dtNull;
		/// <summary>IsDisabled Attribute type String</summary>
		private bool? _bIsDisabled = null;
		/// <summary>GuidStr Attribute type String</summary>
		private string _strGuidStr = null;
		/// <summary>AuthKey Attribute type String</summary>
		private string _strAuthKey = null;
		/// <summary>Notes Attribute type String</summary>
		private string _strNotes = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-Apikey";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class Apikey and is of type bool</summary>
		public static readonly string ENTITY_NAME = "Apikey"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "apikey_id"; //Table id field name
		/// <summary>client_id Database field </summary>
		public static readonly string DB_FIELD_CLIENT_ID = "client_id"; //Table ClientID field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>date_expiration Database field </summary>
		public static readonly string DB_FIELD_DATE_EXPIRATION = "date_expiration"; //Table DateExpiration field name
		/// <summary>is_disabled Database field </summary>
		public static readonly string DB_FIELD_IS_DISABLED = "is_disabled"; //Table IsDisabled field name
		/// <summary>guid_str Database field </summary>
		public static readonly string DB_FIELD_GUID_STR = "guid_str"; //Table GuidStr field name
		/// <summary>auth_key Database field </summary>
		public static readonly string DB_FIELD_AUTH_KEY = "auth_key"; //Table AuthKey field name
		/// <summary>notes Database field </summary>
		public static readonly string DB_FIELD_NOTES = "notes"; //Table Notes field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "ApikeyID"; //Attribute id  name
		/// <summary>ClientID Attribute type string</summary>
		public static readonly string TAG_CLIENT_ID = "ClientID"; //Table ClientID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DateExpiration Attribute type string</summary>
		public static readonly string TAG_DATE_EXPIRATION = "DateExpiration"; //Table DateExpiration field name
		/// <summary>IsDisabled Attribute type string</summary>
		public static readonly string TAG_IS_DISABLED = "IsDisabled"; //Table IsDisabled field name
		/// <summary>GuidStr Attribute type string</summary>
		public static readonly string TAG_GUID_STR = "GuidStr"; //Table GuidStr field name
		/// <summary>AuthKey Attribute type string</summary>
		public static readonly string TAG_AUTH_KEY = "AuthKey"; //Table AuthKey field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spApikeyInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spApikeyUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spApikeyDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spApikeyLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spApikeyExist"; //Exist sp name

		//properties
		/// <summary>ApikeyID is a Property in the Apikey Class of type long</summary>
		public long ApikeyID 
		{
			get{return _lApikeyID;}
			set{_lApikeyID = value;}
		}
		/// <summary>ClientID is a Property in the Apikey Class of type long</summary>
		public long ClientID 
		{
			get{return _lClientID;}
			set{_lClientID = value;}
		}
		/// <summary>DateCreated is a Property in the Apikey Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Apikey Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DateExpiration is a Property in the Apikey Class of type DateTime</summary>
		public DateTime DateExpiration 
		{
			get{return _dtDateExpiration;}
			set{_dtDateExpiration = value;}
		}
		/// <summary>IsDisabled is a Property in the Apikey Class of type bool</summary>
		public bool? IsDisabled 
		{
			get{return _bIsDisabled;}
			set{_bIsDisabled = value;}
		}
		/// <summary>GuidStr is a Property in the Apikey Class of type String</summary>
		public string GuidStr 
		{
			get{return _strGuidStr;}
			set{_strGuidStr = value;}
		}
		/// <summary>AuthKey is a Property in the Apikey Class of type String</summary>
		public string AuthKey 
		{
			get{return _strAuthKey;}
			set{_strAuthKey = value;}
		}
		/// <summary>Notes is a Property in the Apikey Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}
		/// <summary>HasError Property in class Apikey and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Apikey and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>Apikey empty constructor</summary>
		public Apikey()
		{
		}
		/// <summary>Apikey constructor takes a Config</summary>
		public Apikey(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>Apikey constructor takes ApikeyID and a SqlConnection</summary>
		public Apikey(long l, SqlConnection conn) 
		{
			ApikeyID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Apikey Constructor takes pStrData and Config</summary>
		public Apikey(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>Apikey Constructor takes SqlDataReader</summary>
		public Apikey(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Apikey Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + ApikeyID.ToString() + "\n");
			sbReturn.Append(TAG_CLIENT_ID + ":  " + ClientID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(DateExpiration))
			{
				sbReturn.Append(TAG_DATE_EXPIRATION + ":  " + DateExpiration.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_EXPIRATION + ":\n");
			}
			sbReturn.Append(TAG_IS_DISABLED + ":  " + IsDisabled + "\n");
			sbReturn.Append(TAG_GUID_STR + ":  " + GuidStr + "\n");
			sbReturn.Append(TAG_AUTH_KEY + ":  " + AuthKey + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Apikey</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Apikey>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ApikeyID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_CLIENT_ID + ">" + ClientID + "</" + TAG_CLIENT_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(DateExpiration))
			{
				sbReturn.Append("<" + TAG_DATE_EXPIRATION + ">" + DateExpiration.ToString() + "</" + TAG_DATE_EXPIRATION + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_EXPIRATION + "></" + TAG_DATE_EXPIRATION + ">\n");
			}
			sbReturn.Append("<" + TAG_IS_DISABLED + ">" + IsDisabled + "</" + TAG_IS_DISABLED + ">\n");
			sbReturn.Append("<" + TAG_GUID_STR + ">" + GuidStr + "</" + TAG_GUID_STR + ">\n");
			sbReturn.Append("<" + TAG_AUTH_KEY + ">" + AuthKey + "</" + TAG_AUTH_KEY + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</Apikey>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ApikeyID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CLIENT_ID);
				ClientID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ClientID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_EXPIRATION);
				DateExpiration = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IS_DISABLED);
				IsDisabled = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			IsDisabled = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_GUID_STR);
				GuidStr = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_AUTH_KEY);
				AuthKey = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with apikey_id equal to the current object's ApikeyID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where apikey_id equal to the current object's ApikeyID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where apikey_id equal to the current object's ApikeyID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(Apikey.TAG_CLIENT_ID + ":  ");
				ClientID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(Apikey.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Apikey.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}
				try
				{
					Console.WriteLine(Apikey.TAG_DATE_EXPIRATION + ":  ");
					DateExpiration = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateExpiration = new DateTime();
				}

				Console.WriteLine(Apikey.TAG_IS_DISABLED + ":  ");
				IsDisabled = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Apikey.TAG_GUID_STR + ":  ");
				GuidStr = Console.ReadLine();

				Console.WriteLine(Apikey.TAG_AUTH_KEY + ":  ");
				AuthKey = Console.ReadLine();

				Console.WriteLine(Apikey.TAG_NOTES + ":  ");
				Notes = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramClientID = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramDateExpiration = null;
			SqlParameter paramIsDisabled = null;
			SqlParameter paramGuidStr = null;
			SqlParameter paramAuthKey = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramClientID = new SqlParameter("@" + TAG_CLIENT_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			if (!dtNull.Equals(DateExpiration))
			{
				paramDateExpiration = new SqlParameter("@" + TAG_DATE_EXPIRATION, DateExpiration);
			}
			else
			{
				paramDateExpiration = new SqlParameter("@" + TAG_DATE_EXPIRATION, DBNull.Value);
			}
			paramDateExpiration.DbType = DbType.DateTime;
			paramDateExpiration.Direction = ParameterDirection.Input;

			paramIsDisabled = new SqlParameter("@" + TAG_IS_DISABLED, IsDisabled);
			paramIsDisabled.DbType = DbType.Boolean;
			paramIsDisabled.Direction = ParameterDirection.Input;

			paramGuidStr = new SqlParameter("@" + TAG_GUID_STR, GuidStr);
			paramGuidStr.DbType = DbType.String;
			paramGuidStr.Size = 255;
			paramGuidStr.Direction = ParameterDirection.Input;

			paramAuthKey = new SqlParameter("@" + TAG_AUTH_KEY, AuthKey);
			paramAuthKey.DbType = DbType.String;
			paramAuthKey.Size = 255;
			paramAuthKey.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramClientID);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramDateExpiration);
			cmd.Parameters.Add(paramIsDisabled);
			cmd.Parameters.Add(paramGuidStr);
			cmd.Parameters.Add(paramAuthKey);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			ApikeyID = long.Parse(strTmp);

			// cleanup to help GC
			paramClientID = null;
			paramDateCreated = null;
			paramDateExpiration = null;
			paramIsDisabled = null;
			paramGuidStr = null;
			paramAuthKey = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramApikeyID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramApikeyID = new SqlParameter("@" + TAG_ID, ApikeyID);
			paramApikeyID.Direction = ParameterDirection.Input;
			paramApikeyID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramApikeyID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramApikeyID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApikeyID = null;
			SqlParameter paramClientID = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramDateExpiration = null;
			SqlParameter paramIsDisabled = null;
			SqlParameter paramGuidStr = null;
			SqlParameter paramAuthKey = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramApikeyID = new SqlParameter("@" + TAG_ID, ApikeyID);
			paramApikeyID.DbType = DbType.Int32;
			paramApikeyID.Direction = ParameterDirection.Input;


			paramClientID = new SqlParameter("@" + TAG_CLIENT_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;


				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			if (!dtNull.Equals(DateExpiration))
			{
				paramDateExpiration = new SqlParameter("@" + TAG_DATE_EXPIRATION, DateExpiration);
			}
			else
			{
				paramDateExpiration = new SqlParameter("@" + TAG_DATE_EXPIRATION, DBNull.Value);
			}
			paramDateExpiration.DbType = DbType.DateTime;
			paramDateExpiration.Direction = ParameterDirection.Input;

			paramIsDisabled = new SqlParameter("@" + TAG_IS_DISABLED, IsDisabled);
			paramIsDisabled.DbType = DbType.Boolean;
			paramIsDisabled.Direction = ParameterDirection.Input;

			paramGuidStr = new SqlParameter("@" + TAG_GUID_STR, GuidStr);
			paramGuidStr.DbType = DbType.String;
			paramGuidStr.Size = 255;
			paramGuidStr.Direction = ParameterDirection.Input;

			paramAuthKey = new SqlParameter("@" + TAG_AUTH_KEY, AuthKey);
			paramAuthKey.DbType = DbType.String;
			paramAuthKey.Size = 255;
			paramAuthKey.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramApikeyID);
			cmd.Parameters.Add(paramClientID);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramDateExpiration);
			cmd.Parameters.Add(paramIsDisabled);
			cmd.Parameters.Add(paramGuidStr);
			cmd.Parameters.Add(paramAuthKey);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			ApikeyID = long.Parse(s);

			// cleanup
			paramApikeyID = null;
			paramClientID = null;
			paramDateModified = null;
			paramDateExpiration = null;
			paramIsDisabled = null;
			paramGuidStr = null;
			paramAuthKey = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApikeyID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramApikeyID = new SqlParameter("@" + TAG_ID, ApikeyID);
			paramApikeyID.DbType = DbType.Int32;
			paramApikeyID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramApikeyID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramApikeyID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApikeyID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramApikeyID = new SqlParameter("@" + TAG_ID, ApikeyID);
			paramApikeyID.DbType = DbType.Int32;
			paramApikeyID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramApikeyID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramApikeyID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.ApikeyID = long.Parse(rdr[DB_FIELD_ID].ToString());
			try
			{
			this.ClientID = Convert.ToInt32(rdr[DB_FIELD_CLIENT_ID].ToString().Trim());
			}
			catch{}
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateExpiration = DateTime.Parse(rdr[DB_FIELD_DATE_EXPIRATION].ToString());
			}
			catch 
			{
			}
			try
			{
			this.IsDisabled = Convert.ToBoolean(rdr[DB_FIELD_IS_DISABLED].ToString().Trim());
			}
			catch{}
			this.GuidStr = rdr[DB_FIELD_GUID_STR].ToString().Trim();
			this.AuthKey = rdr[DB_FIELD_AUTH_KEY].ToString().Trim();
			this.Notes = rdr[DB_FIELD_NOTES].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApikeyID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApikeyID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + ApikeyID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApikeyID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApikeyID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF Apikey CLASS FILE
