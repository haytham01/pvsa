using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Address.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Address database table.
	/// </summary>
	public class Address
	{
		//Attributes
		/// <summary>AddressID Attribute type String</summary>
		private long _lAddressID = 0;
		/// <summary>State Attribute type String</summary>
		private string _strState = null;
		/// <summary>Country Attribute type String</summary>
		private string _strCountry = null;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>StreetNumber1 Attribute type String</summary>
		private string _strStreetNumber1 = null;
		/// <summary>StreetNumber2 Attribute type String</summary>
		private string _strStreetNumber2 = null;
		/// <summary>City Attribute type String</summary>
		private string _strCity = null;
		/// <summary>Zip Attribute type String</summary>
		private string _strZip = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-Address";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class Address and is of type bool</summary>
		public static readonly string ENTITY_NAME = "Address"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "address_id"; //Table id field name
		/// <summary>state Database field </summary>
		public static readonly string DB_FIELD_STATE = "state"; //Table State field name
		/// <summary>country Database field </summary>
		public static readonly string DB_FIELD_COUNTRY = "country"; //Table Country field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>street_number_1 Database field </summary>
		public static readonly string DB_FIELD_STREET_NUMBER_1 = "street_number_1"; //Table StreetNumber1 field name
		/// <summary>street_number_2 Database field </summary>
		public static readonly string DB_FIELD_STREET_NUMBER_2 = "street_number_2"; //Table StreetNumber2 field name
		/// <summary>city Database field </summary>
		public static readonly string DB_FIELD_CITY = "city"; //Table City field name
		/// <summary>zip Database field </summary>
		public static readonly string DB_FIELD_ZIP = "zip"; //Table Zip field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "AddressID"; //Attribute id  name
		/// <summary>State Attribute type string</summary>
		public static readonly string TAG_STATE = "State"; //Table State field name
		/// <summary>Country Attribute type string</summary>
		public static readonly string TAG_COUNTRY = "Country"; //Table Country field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>StreetNumber1 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_1 = "StreetNumber1"; //Table StreetNumber1 field name
		/// <summary>StreetNumber2 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_2 = "StreetNumber2"; //Table StreetNumber2 field name
		/// <summary>City Attribute type string</summary>
		public static readonly string TAG_CITY = "City"; //Table City field name
		/// <summary>Zip Attribute type string</summary>
		public static readonly string TAG_ZIP = "Zip"; //Table Zip field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spAddressInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spAddressUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spAddressDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spAddressLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spAddressExist"; //Exist sp name

		//properties
		/// <summary>AddressID is a Property in the Address Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>State is a Property in the Address Class of type String</summary>
		public string State 
		{
			get{return _strState;}
			set{_strState = value;}
		}
		/// <summary>Country is a Property in the Address Class of type String</summary>
		public string Country 
		{
			get{return _strCountry;}
			set{_strCountry = value;}
		}
		/// <summary>DateCreated is a Property in the Address Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Address Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>StreetNumber1 is a Property in the Address Class of type String</summary>
		public string StreetNumber1 
		{
			get{return _strStreetNumber1;}
			set{_strStreetNumber1 = value;}
		}
		/// <summary>StreetNumber2 is a Property in the Address Class of type String</summary>
		public string StreetNumber2 
		{
			get{return _strStreetNumber2;}
			set{_strStreetNumber2 = value;}
		}
		/// <summary>City is a Property in the Address Class of type String</summary>
		public string City 
		{
			get{return _strCity;}
			set{_strCity = value;}
		}
		/// <summary>Zip is a Property in the Address Class of type String</summary>
		public string Zip 
		{
			get{return _strZip;}
			set{_strZip = value;}
		}


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>HasError Property in class Address and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Address and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>Address empty constructor</summary>
		public Address()
		{
		}
		/// <summary>Address constructor takes a Config</summary>
		public Address(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>Address constructor takes AddressID and a SqlConnection</summary>
		public Address(long l, SqlConnection conn) 
		{
			AddressID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Address Constructor takes pStrData and Config</summary>
		public Address(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>Address Constructor takes SqlDataReader</summary>
		public Address(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Address Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + AddressID.ToString() + "\n");
			sbReturn.Append(TAG_STATE + ":  " + State + "\n");
			sbReturn.Append(TAG_COUNTRY + ":  " + Country + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_STREET_NUMBER_1 + ":  " + StreetNumber1 + "\n");
			sbReturn.Append(TAG_STREET_NUMBER_2 + ":  " + StreetNumber2 + "\n");
			sbReturn.Append(TAG_CITY + ":  " + City + "\n");
			sbReturn.Append(TAG_ZIP + ":  " + Zip + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Address</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Address>\n");
			sbReturn.Append("<" + TAG_ID + ">" + AddressID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_STATE + ">" + State + "</" + TAG_STATE + ">\n");
			sbReturn.Append("<" + TAG_COUNTRY + ">" + Country + "</" + TAG_COUNTRY + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_STREET_NUMBER_1 + ">" + StreetNumber1 + "</" + TAG_STREET_NUMBER_1 + ">\n");
			sbReturn.Append("<" + TAG_STREET_NUMBER_2 + ">" + StreetNumber2 + "</" + TAG_STREET_NUMBER_2 + ">\n");
			sbReturn.Append("<" + TAG_CITY + ">" + City + "</" + TAG_CITY + ">\n");
			sbReturn.Append("<" + TAG_ZIP + ">" + Zip + "</" + TAG_ZIP + ">\n");
			sbReturn.Append("</Address>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				AddressID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STATE);
				State = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_COUNTRY);
				Country = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_1);
				StreetNumber1 = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_2);
				StreetNumber2 = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CITY);
				City = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ZIP);
				Zip = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with address_id equal to the current object's AddressID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where address_id equal to the current object's AddressID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where address_id equal to the current object's AddressID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(Address.TAG_STATE + ":  ");
				State = Console.ReadLine();

				Console.WriteLine(Address.TAG_COUNTRY + ":  ");
				Country = Console.ReadLine();
				try
				{
					Console.WriteLine(Address.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Address.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}

				Console.WriteLine(Address.TAG_STREET_NUMBER_1 + ":  ");
				StreetNumber1 = Console.ReadLine();

				Console.WriteLine(Address.TAG_STREET_NUMBER_2 + ":  ");
				StreetNumber2 = Console.ReadLine();

				Console.WriteLine(Address.TAG_CITY + ":  ");
				City = Console.ReadLine();

				Console.WriteLine(Address.TAG_ZIP + ":  ");
				Zip = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramState = null;
			SqlParameter paramCountry = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramStreetNumber1 = null;
			SqlParameter paramStreetNumber2 = null;
			SqlParameter paramCity = null;
			SqlParameter paramZip = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramState = new SqlParameter("@" + TAG_STATE, State);
			paramState.DbType = DbType.String;
			paramState.Size = 2;
			paramState.Direction = ParameterDirection.Input;

			paramCountry = new SqlParameter("@" + TAG_COUNTRY, Country);
			paramCountry.DbType = DbType.String;
			paramCountry.Size = 255;
			paramCountry.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			paramStreetNumber1 = new SqlParameter("@" + TAG_STREET_NUMBER_1, StreetNumber1);
			paramStreetNumber1.DbType = DbType.String;
			paramStreetNumber1.Size = 255;
			paramStreetNumber1.Direction = ParameterDirection.Input;

			paramStreetNumber2 = new SqlParameter("@" + TAG_STREET_NUMBER_2, StreetNumber2);
			paramStreetNumber2.DbType = DbType.String;
			paramStreetNumber2.Size = 255;
			paramStreetNumber2.Direction = ParameterDirection.Input;

			paramCity = new SqlParameter("@" + TAG_CITY, City);
			paramCity.DbType = DbType.String;
			paramCity.Size = 255;
			paramCity.Direction = ParameterDirection.Input;

			paramZip = new SqlParameter("@" + TAG_ZIP, Zip);
			paramZip.DbType = DbType.String;
			paramZip.Size = 32;
			paramZip.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramState);
			cmd.Parameters.Add(paramCountry);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramStreetNumber1);
			cmd.Parameters.Add(paramStreetNumber2);
			cmd.Parameters.Add(paramCity);
			cmd.Parameters.Add(paramZip);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			AddressID = long.Parse(strTmp);

			// cleanup to help GC
			paramState = null;
			paramCountry = null;
			paramDateCreated = null;
			paramStreetNumber1 = null;
			paramStreetNumber2 = null;
			paramCity = null;
			paramZip = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramAddressID = new SqlParameter("@" + TAG_ID, AddressID);
			paramAddressID.Direction = ParameterDirection.Input;
			paramAddressID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramAddressID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramState = null;
			SqlParameter paramCountry = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramStreetNumber1 = null;
			SqlParameter paramStreetNumber2 = null;
			SqlParameter paramCity = null;
			SqlParameter paramZip = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramAddressID = new SqlParameter("@" + TAG_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;


			paramState = new SqlParameter("@" + TAG_STATE, State);
			paramState.DbType = DbType.String;
			paramState.Size = 2;
			paramState.Direction = ParameterDirection.Input;

			paramCountry = new SqlParameter("@" + TAG_COUNTRY, Country);
			paramCountry.DbType = DbType.String;
			paramCountry.Size = 255;
			paramCountry.Direction = ParameterDirection.Input;


				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			paramStreetNumber1 = new SqlParameter("@" + TAG_STREET_NUMBER_1, StreetNumber1);
			paramStreetNumber1.DbType = DbType.String;
			paramStreetNumber1.Size = 255;
			paramStreetNumber1.Direction = ParameterDirection.Input;

			paramStreetNumber2 = new SqlParameter("@" + TAG_STREET_NUMBER_2, StreetNumber2);
			paramStreetNumber2.DbType = DbType.String;
			paramStreetNumber2.Size = 255;
			paramStreetNumber2.Direction = ParameterDirection.Input;

			paramCity = new SqlParameter("@" + TAG_CITY, City);
			paramCity.DbType = DbType.String;
			paramCity.Size = 255;
			paramCity.Direction = ParameterDirection.Input;

			paramZip = new SqlParameter("@" + TAG_ZIP, Zip);
			paramZip.DbType = DbType.String;
			paramZip.Size = 32;
			paramZip.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramState);
			cmd.Parameters.Add(paramCountry);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramStreetNumber1);
			cmd.Parameters.Add(paramStreetNumber2);
			cmd.Parameters.Add(paramCity);
			cmd.Parameters.Add(paramZip);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			AddressID = long.Parse(s);

			// cleanup
			paramAddressID = null;
			paramState = null;
			paramCountry = null;
			paramDateModified = null;
			paramStreetNumber1 = null;
			paramStreetNumber2 = null;
			paramCity = null;
			paramZip = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramAddressID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramAddressID = new SqlParameter("@" + TAG_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramAddressID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramAddressID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramAddressID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramAddressID = new SqlParameter("@" + TAG_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramAddressID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramAddressID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.AddressID = long.Parse(rdr[DB_FIELD_ID].ToString());
			this.State = rdr[DB_FIELD_STATE].ToString().Trim();
			this.Country = rdr[DB_FIELD_COUNTRY].ToString().Trim();
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
			this.StreetNumber1 = rdr[DB_FIELD_STREET_NUMBER_1].ToString().Trim();
			this.StreetNumber2 = rdr[DB_FIELD_STREET_NUMBER_2].ToString().Trim();
			this.City = rdr[DB_FIELD_CITY].ToString().Trim();
			this.Zip = rdr[DB_FIELD_ZIP].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							AddressID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							AddressID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + AddressID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							AddressID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							AddressID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF Address CLASS FILE
