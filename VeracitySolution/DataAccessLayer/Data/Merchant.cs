using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Merchant.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	12/27/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Merchant database table.
	/// </summary>
	public class Merchant
	{
		//Attributes
		/// <summary>MerchantID Attribute type String</summary>
		private long _lMerchantID = 0;
		/// <summary>AddressID Attribute type String</summary>
		private long _lAddressID = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>IsDeactivated Attribute type String</summary>
		private bool? _bIsDeactivated = null;
		/// <summary>PhonePrimary Attribute type String</summary>
		private string _strPhonePrimary = null;
		/// <summary>PhoneFax Attribute type String</summary>
		private string _strPhoneFax = null;
		/// <summary>MerchantName Attribute type String</summary>
		private string _strMerchantName = null;
		/// <summary>EmailAddress Attribute type String</summary>
		private string _strEmailAddress = null;
		/// <summary>WebsiteLink Attribute type String</summary>
		private string _strWebsiteLink = null;
		/// <summary>ContactName Attribute type String</summary>
		private string _strContactName = null;
		/// <summary>ContactPhone Attribute type String</summary>
		private string _strContactPhone = null;
		/// <summary>ContactEmail Attribute type String</summary>
		private string _strContactEmail = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-Merchant";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class Merchant and is of type bool</summary>
		public static readonly string ENTITY_NAME = "Merchant"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "merchant_id"; //Table id field name
		/// <summary>address_id Database field </summary>
		public static readonly string DB_FIELD_ADDRESS_ID = "address_id"; //Table AddressID field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>is_deactivated Database field </summary>
		public static readonly string DB_FIELD_IS_DEACTIVATED = "is_deactivated"; //Table IsDeactivated field name
		/// <summary>phone_primary Database field </summary>
		public static readonly string DB_FIELD_PHONE_PRIMARY = "phone_primary"; //Table PhonePrimary field name
		/// <summary>phone_fax Database field </summary>
		public static readonly string DB_FIELD_PHONE_FAX = "phone_fax"; //Table PhoneFax field name
		/// <summary>merchant_name Database field </summary>
		public static readonly string DB_FIELD_MERCHANT_NAME = "merchant_name"; //Table MerchantName field name
		/// <summary>email_address Database field </summary>
		public static readonly string DB_FIELD_EMAIL_ADDRESS = "email_address"; //Table EmailAddress field name
		/// <summary>website_link Database field </summary>
		public static readonly string DB_FIELD_WEBSITE_LINK = "website_link"; //Table WebsiteLink field name
		/// <summary>contact_name Database field </summary>
		public static readonly string DB_FIELD_CONTACT_NAME = "contact_name"; //Table ContactName field name
		/// <summary>contact_phone Database field </summary>
		public static readonly string DB_FIELD_CONTACT_PHONE = "contact_phone"; //Table ContactPhone field name
		/// <summary>contact_email Database field </summary>
		public static readonly string DB_FIELD_CONTACT_EMAIL = "contact_email"; //Table ContactEmail field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "MerchantID"; //Attribute id  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Table AddressID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>IsDeactivated Attribute type string</summary>
		public static readonly string TAG_IS_DEACTIVATED = "IsDeactivated"; //Table IsDeactivated field name
		/// <summary>PhonePrimary Attribute type string</summary>
		public static readonly string TAG_PHONE_PRIMARY = "PhonePrimary"; //Table PhonePrimary field name
		/// <summary>PhoneFax Attribute type string</summary>
		public static readonly string TAG_PHONE_FAX = "PhoneFax"; //Table PhoneFax field name
		/// <summary>MerchantName Attribute type string</summary>
		public static readonly string TAG_MERCHANT_NAME = "MerchantName"; //Table MerchantName field name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Table EmailAddress field name
		/// <summary>WebsiteLink Attribute type string</summary>
		public static readonly string TAG_WEBSITE_LINK = "WebsiteLink"; //Table WebsiteLink field name
		/// <summary>ContactName Attribute type string</summary>
		public static readonly string TAG_CONTACT_NAME = "ContactName"; //Table ContactName field name
		/// <summary>ContactPhone Attribute type string</summary>
		public static readonly string TAG_CONTACT_PHONE = "ContactPhone"; //Table ContactPhone field name
		/// <summary>ContactEmail Attribute type string</summary>
		public static readonly string TAG_CONTACT_EMAIL = "ContactEmail"; //Table ContactEmail field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spMerchantInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spMerchantUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spMerchantDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spMerchantLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spMerchantExist"; //Exist sp name

		//properties
		/// <summary>MerchantID is a Property in the Merchant Class of type long</summary>
		public long MerchantID 
		{
			get{return _lMerchantID;}
			set{_lMerchantID = value;}
		}
		/// <summary>AddressID is a Property in the Merchant Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>DateCreated is a Property in the Merchant Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Merchant Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>IsDeactivated is a Property in the Merchant Class of type bool</summary>
		public bool? IsDeactivated 
		{
			get{return _bIsDeactivated;}
			set{_bIsDeactivated = value;}
		}
		/// <summary>PhonePrimary is a Property in the Merchant Class of type String</summary>
		public string PhonePrimary 
		{
			get{return _strPhonePrimary;}
			set{_strPhonePrimary = value;}
		}
		/// <summary>PhoneFax is a Property in the Merchant Class of type String</summary>
		public string PhoneFax 
		{
			get{return _strPhoneFax;}
			set{_strPhoneFax = value;}
		}
		/// <summary>MerchantName is a Property in the Merchant Class of type String</summary>
		public string MerchantName 
		{
			get{return _strMerchantName;}
			set{_strMerchantName = value;}
		}
		/// <summary>EmailAddress is a Property in the Merchant Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>WebsiteLink is a Property in the Merchant Class of type String</summary>
		public string WebsiteLink 
		{
			get{return _strWebsiteLink;}
			set{_strWebsiteLink = value;}
		}
		/// <summary>ContactName is a Property in the Merchant Class of type String</summary>
		public string ContactName 
		{
			get{return _strContactName;}
			set{_strContactName = value;}
		}
		/// <summary>ContactPhone is a Property in the Merchant Class of type String</summary>
		public string ContactPhone 
		{
			get{return _strContactPhone;}
			set{_strContactPhone = value;}
		}
		/// <summary>ContactEmail is a Property in the Merchant Class of type String</summary>
		public string ContactEmail 
		{
			get{return _strContactEmail;}
			set{_strContactEmail = value;}
		}


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>HasError Property in class Merchant and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Merchant and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>Merchant empty constructor</summary>
		public Merchant()
		{
		}
		/// <summary>Merchant constructor takes a Config</summary>
		public Merchant(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>Merchant constructor takes MerchantID and a SqlConnection</summary>
		public Merchant(long l, SqlConnection conn) 
		{
			MerchantID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Merchant Constructor takes pStrData and Config</summary>
		public Merchant(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>Merchant Constructor takes SqlDataReader</summary>
		public Merchant(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Merchant Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + MerchantID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_IS_DEACTIVATED + ":  " + IsDeactivated + "\n");
			sbReturn.Append(TAG_PHONE_PRIMARY + ":  " + PhonePrimary + "\n");
			sbReturn.Append(TAG_PHONE_FAX + ":  " + PhoneFax + "\n");
			sbReturn.Append(TAG_MERCHANT_NAME + ":  " + MerchantName + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_WEBSITE_LINK + ":  " + WebsiteLink + "\n");
			sbReturn.Append(TAG_CONTACT_NAME + ":  " + ContactName + "\n");
			sbReturn.Append(TAG_CONTACT_PHONE + ":  " + ContactPhone + "\n");
			sbReturn.Append(TAG_CONTACT_EMAIL + ":  " + ContactEmail + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Merchant</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Merchant>\n");
			sbReturn.Append("<" + TAG_ID + ">" + MerchantID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_IS_DEACTIVATED + ">" + IsDeactivated + "</" + TAG_IS_DEACTIVATED + ">\n");
			sbReturn.Append("<" + TAG_PHONE_PRIMARY + ">" + PhonePrimary + "</" + TAG_PHONE_PRIMARY + ">\n");
			sbReturn.Append("<" + TAG_PHONE_FAX + ">" + PhoneFax + "</" + TAG_PHONE_FAX + ">\n");
			sbReturn.Append("<" + TAG_MERCHANT_NAME + ">" + MerchantName + "</" + TAG_MERCHANT_NAME + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_WEBSITE_LINK + ">" + WebsiteLink + "</" + TAG_WEBSITE_LINK + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_NAME + ">" + ContactName + "</" + TAG_CONTACT_NAME + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_PHONE + ">" + ContactPhone + "</" + TAG_CONTACT_PHONE + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_EMAIL + ">" + ContactEmail + "</" + TAG_CONTACT_EMAIL + ">\n");
			sbReturn.Append("</Merchant>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				MerchantID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IS_DEACTIVATED);
				IsDeactivated = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			IsDeactivated = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_PRIMARY);
				PhonePrimary = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_FAX);
				PhoneFax = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MERCHANT_NAME);
				MerchantName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_WEBSITE_LINK);
				WebsiteLink = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_NAME);
				ContactName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_PHONE);
				ContactPhone = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_EMAIL);
				ContactEmail = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with merchant_id equal to the current object's MerchantID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where merchant_id equal to the current object's MerchantID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where merchant_id equal to the current object's MerchantID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{
				{
					Console.WriteLine(TAG_ID + ":  ");
					try
					{
						MerchantID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						MerchantID = 0;
					}
				}

				Console.WriteLine(Merchant.TAG_ADDRESS_ID + ":  ");
				AddressID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(Merchant.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Merchant.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}

				Console.WriteLine(Merchant.TAG_IS_DEACTIVATED + ":  ");
				IsDeactivated = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Merchant.TAG_PHONE_PRIMARY + ":  ");
				PhonePrimary = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_PHONE_FAX + ":  ");
				PhoneFax = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_MERCHANT_NAME + ":  ");
				MerchantName = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_EMAIL_ADDRESS + ":  ");
				EmailAddress = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_WEBSITE_LINK + ":  ");
				WebsiteLink = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_NAME + ":  ");
				ContactName = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_PHONE + ":  ");
				ContactPhone = Console.ReadLine();

				Console.WriteLine(Merchant.TAG_CONTACT_EMAIL + ":  ");
				ContactEmail = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramMerchantID = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramIsDeactivated = null;
			SqlParameter paramPhonePrimary = null;
			SqlParameter paramPhoneFax = null;
			SqlParameter paramMerchantName = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramWebsiteLink = null;
			SqlParameter paramContactName = null;
			SqlParameter paramContactPhone = null;
			SqlParameter paramContactEmail = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters
			paramMerchantID = new SqlParameter("@" + TAG_ID, MerchantID);
			paramMerchantID.DbType = DbType.Int32;
			paramMerchantID.Direction = ParameterDirection.Input;

			paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			paramIsDeactivated = new SqlParameter("@" + TAG_IS_DEACTIVATED, IsDeactivated);
			paramIsDeactivated.DbType = DbType.Boolean;
			paramIsDeactivated.Direction = ParameterDirection.Input;

			paramPhonePrimary = new SqlParameter("@" + TAG_PHONE_PRIMARY, PhonePrimary);
			paramPhonePrimary.DbType = DbType.String;
			paramPhonePrimary.Size = 255;
			paramPhonePrimary.Direction = ParameterDirection.Input;

			paramPhoneFax = new SqlParameter("@" + TAG_PHONE_FAX, PhoneFax);
			paramPhoneFax.DbType = DbType.String;
			paramPhoneFax.Size = 255;
			paramPhoneFax.Direction = ParameterDirection.Input;

			paramMerchantName = new SqlParameter("@" + TAG_MERCHANT_NAME, MerchantName);
			paramMerchantName.DbType = DbType.String;
			paramMerchantName.Size = 255;
			paramMerchantName.Direction = ParameterDirection.Input;

			paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
			paramEmailAddress.DbType = DbType.String;
			paramEmailAddress.Size = 255;
			paramEmailAddress.Direction = ParameterDirection.Input;

			paramWebsiteLink = new SqlParameter("@" + TAG_WEBSITE_LINK, WebsiteLink);
			paramWebsiteLink.DbType = DbType.String;
			paramWebsiteLink.Size = 255;
			paramWebsiteLink.Direction = ParameterDirection.Input;

			paramContactName = new SqlParameter("@" + TAG_CONTACT_NAME, ContactName);
			paramContactName.DbType = DbType.String;
			paramContactName.Size = 255;
			paramContactName.Direction = ParameterDirection.Input;

			paramContactPhone = new SqlParameter("@" + TAG_CONTACT_PHONE, ContactPhone);
			paramContactPhone.DbType = DbType.String;
			paramContactPhone.Size = 255;
			paramContactPhone.Direction = ParameterDirection.Input;

			paramContactEmail = new SqlParameter("@" + TAG_CONTACT_EMAIL, ContactEmail);
			paramContactEmail.DbType = DbType.String;
			paramContactEmail.Size = 255;
			paramContactEmail.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramMerchantID);
			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramIsDeactivated);
			cmd.Parameters.Add(paramPhonePrimary);
			cmd.Parameters.Add(paramPhoneFax);
			cmd.Parameters.Add(paramMerchantName);
			cmd.Parameters.Add(paramEmailAddress);
			cmd.Parameters.Add(paramWebsiteLink);
			cmd.Parameters.Add(paramContactName);
			cmd.Parameters.Add(paramContactPhone);
			cmd.Parameters.Add(paramContactEmail);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			MerchantID = long.Parse(strTmp);

			// cleanup to help GC
			paramMerchantID = null;
			paramAddressID = null;
			paramDateCreated = null;
			paramIsDeactivated = null;
			paramPhonePrimary = null;
			paramPhoneFax = null;
			paramMerchantName = null;
			paramEmailAddress = null;
			paramWebsiteLink = null;
			paramContactName = null;
			paramContactPhone = null;
			paramContactEmail = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramMerchantID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramMerchantID = new SqlParameter("@" + TAG_ID, MerchantID);
			paramMerchantID.Direction = ParameterDirection.Input;
			paramMerchantID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramMerchantID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramMerchantID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramMerchantID = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramIsDeactivated = null;
			SqlParameter paramPhonePrimary = null;
			SqlParameter paramPhoneFax = null;
			SqlParameter paramMerchantName = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramWebsiteLink = null;
			SqlParameter paramContactName = null;
			SqlParameter paramContactPhone = null;
			SqlParameter paramContactEmail = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramMerchantID = new SqlParameter("@" + TAG_ID, MerchantID);
			paramMerchantID.DbType = DbType.Int32;
			paramMerchantID.Direction = ParameterDirection.Input;


			paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;


				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			paramIsDeactivated = new SqlParameter("@" + TAG_IS_DEACTIVATED, IsDeactivated);
			paramIsDeactivated.DbType = DbType.Boolean;
			paramIsDeactivated.Direction = ParameterDirection.Input;

			paramPhonePrimary = new SqlParameter("@" + TAG_PHONE_PRIMARY, PhonePrimary);
			paramPhonePrimary.DbType = DbType.String;
			paramPhonePrimary.Size = 255;
			paramPhonePrimary.Direction = ParameterDirection.Input;

			paramPhoneFax = new SqlParameter("@" + TAG_PHONE_FAX, PhoneFax);
			paramPhoneFax.DbType = DbType.String;
			paramPhoneFax.Size = 255;
			paramPhoneFax.Direction = ParameterDirection.Input;

			paramMerchantName = new SqlParameter("@" + TAG_MERCHANT_NAME, MerchantName);
			paramMerchantName.DbType = DbType.String;
			paramMerchantName.Size = 255;
			paramMerchantName.Direction = ParameterDirection.Input;

			paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
			paramEmailAddress.DbType = DbType.String;
			paramEmailAddress.Size = 255;
			paramEmailAddress.Direction = ParameterDirection.Input;

			paramWebsiteLink = new SqlParameter("@" + TAG_WEBSITE_LINK, WebsiteLink);
			paramWebsiteLink.DbType = DbType.String;
			paramWebsiteLink.Size = 255;
			paramWebsiteLink.Direction = ParameterDirection.Input;

			paramContactName = new SqlParameter("@" + TAG_CONTACT_NAME, ContactName);
			paramContactName.DbType = DbType.String;
			paramContactName.Size = 255;
			paramContactName.Direction = ParameterDirection.Input;

			paramContactPhone = new SqlParameter("@" + TAG_CONTACT_PHONE, ContactPhone);
			paramContactPhone.DbType = DbType.String;
			paramContactPhone.Size = 255;
			paramContactPhone.Direction = ParameterDirection.Input;

			paramContactEmail = new SqlParameter("@" + TAG_CONTACT_EMAIL, ContactEmail);
			paramContactEmail.DbType = DbType.String;
			paramContactEmail.Size = 255;
			paramContactEmail.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramMerchantID);
			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramIsDeactivated);
			cmd.Parameters.Add(paramPhonePrimary);
			cmd.Parameters.Add(paramPhoneFax);
			cmd.Parameters.Add(paramMerchantName);
			cmd.Parameters.Add(paramEmailAddress);
			cmd.Parameters.Add(paramWebsiteLink);
			cmd.Parameters.Add(paramContactName);
			cmd.Parameters.Add(paramContactPhone);
			cmd.Parameters.Add(paramContactEmail);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			MerchantID = long.Parse(s);

			// cleanup
			paramMerchantID = null;
			paramAddressID = null;
			paramDateModified = null;
			paramIsDeactivated = null;
			paramPhonePrimary = null;
			paramPhoneFax = null;
			paramMerchantName = null;
			paramEmailAddress = null;
			paramWebsiteLink = null;
			paramContactName = null;
			paramContactPhone = null;
			paramContactEmail = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramMerchantID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramMerchantID = new SqlParameter("@" + TAG_ID, MerchantID);
			paramMerchantID.DbType = DbType.Int32;
			paramMerchantID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramMerchantID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramMerchantID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramMerchantID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramMerchantID = new SqlParameter("@" + TAG_ID, MerchantID);
			paramMerchantID.DbType = DbType.Int32;
			paramMerchantID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramMerchantID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramMerchantID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.MerchantID = long.Parse(rdr[DB_FIELD_ID].ToString());
			try
			{
			this.AddressID = Convert.ToInt32(rdr[DB_FIELD_ADDRESS_ID].ToString().Trim());
			}
			catch{}
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
			try
			{
			this.IsDeactivated = Convert.ToBoolean(rdr[DB_FIELD_IS_DEACTIVATED].ToString().Trim());
			}
			catch{}
			this.PhonePrimary = rdr[DB_FIELD_PHONE_PRIMARY].ToString().Trim();
			this.PhoneFax = rdr[DB_FIELD_PHONE_FAX].ToString().Trim();
			this.MerchantName = rdr[DB_FIELD_MERCHANT_NAME].ToString().Trim();
			this.EmailAddress = rdr[DB_FIELD_EMAIL_ADDRESS].ToString().Trim();
			this.WebsiteLink = rdr[DB_FIELD_WEBSITE_LINK].ToString().Trim();
			this.ContactName = rdr[DB_FIELD_CONTACT_NAME].ToString().Trim();
			this.ContactPhone = rdr[DB_FIELD_CONTACT_PHONE].ToString().Trim();
			this.ContactEmail = rdr[DB_FIELD_CONTACT_EMAIL].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							MerchantID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							MerchantID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + MerchantID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							MerchantID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							MerchantID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF Merchant CLASS FILE
