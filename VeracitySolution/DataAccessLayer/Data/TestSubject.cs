using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestSubject.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the TestSubject database table.
	/// </summary>
	public class TestSubject
	{
		//Attributes
		/// <summary>TestSubjectID Attribute type String</summary>
		private long _lTestSubjectID = 0;
		/// <summary>AddressID Attribute type String</summary>
		private long _lAddressID = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DriverLicenseNumber Attribute type String</summary>
		private string _strDriverLicenseNumber = null;
		/// <summary>FirstName Attribute type String</summary>
		private string _strFirstName = null;
		/// <summary>MiddleName Attribute type String</summary>
		private string _strMiddleName = null;
		/// <summary>LastName Attribute type String</summary>
		private string _strLastName = null;
		/// <summary>PrimaryPhone Attribute type String</summary>
		private string _strPrimaryPhone = null;
		/// <summary>EmailAddress Attribute type String</summary>
		private string _strEmailAddress = null;
		/// <summary>Code Attribute type String</summary>
		private string _strCode = null;
		/// <summary>IdentificationRawData Attribute type String</summary>
		private string _strIdentificationRawData = null;
		/// <summary>Notes Attribute type String</summary>
		private string _strNotes = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-TestSubject";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class TestSubject and is of type bool</summary>
		public static readonly string ENTITY_NAME = "TestSubject"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "test_subject_id"; //Table id field name
		/// <summary>address_id Database field </summary>
		public static readonly string DB_FIELD_ADDRESS_ID = "address_id"; //Table AddressID field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>driver_license_number Database field </summary>
		public static readonly string DB_FIELD_DRIVER_LICENSE_NUMBER = "driver_license_number"; //Table DriverLicenseNumber field name
		/// <summary>first_name Database field </summary>
		public static readonly string DB_FIELD_FIRST_NAME = "first_name"; //Table FirstName field name
		/// <summary>middle_name Database field </summary>
		public static readonly string DB_FIELD_MIDDLE_NAME = "middle_name"; //Table MiddleName field name
		/// <summary>last_name Database field </summary>
		public static readonly string DB_FIELD_LAST_NAME = "last_name"; //Table LastName field name
		/// <summary>primary_phone Database field </summary>
		public static readonly string DB_FIELD_PRIMARY_PHONE = "primary_phone"; //Table PrimaryPhone field name
		/// <summary>email_address Database field </summary>
		public static readonly string DB_FIELD_EMAIL_ADDRESS = "email_address"; //Table EmailAddress field name
		/// <summary>code Database field </summary>
		public static readonly string DB_FIELD_CODE = "code"; //Table Code field name
		/// <summary>identification_raw_data Database field </summary>
		public static readonly string DB_FIELD_IDENTIFICATION_RAW_DATA = "identification_raw_data"; //Table IdentificationRawData field name
		/// <summary>notes Database field </summary>
		public static readonly string DB_FIELD_NOTES = "notes"; //Table Notes field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "TestSubjectID"; //Attribute id  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Table AddressID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		public static readonly string TAG_DRIVER_LICENSE_NUMBER = "DriverLicenseNumber"; //Table DriverLicenseNumber field name
		/// <summary>FirstName Attribute type string</summary>
		public static readonly string TAG_FIRST_NAME = "FirstName"; //Table FirstName field name
		/// <summary>MiddleName Attribute type string</summary>
		public static readonly string TAG_MIDDLE_NAME = "MiddleName"; //Table MiddleName field name
		/// <summary>LastName Attribute type string</summary>
		public static readonly string TAG_LAST_NAME = "LastName"; //Table LastName field name
		/// <summary>PrimaryPhone Attribute type string</summary>
		public static readonly string TAG_PRIMARY_PHONE = "PrimaryPhone"; //Table PrimaryPhone field name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Table EmailAddress field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>IdentificationRawData Attribute type string</summary>
		public static readonly string TAG_IDENTIFICATION_RAW_DATA = "IdentificationRawData"; //Table IdentificationRawData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spTestSubjectInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spTestSubjectUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spTestSubjectDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spTestSubjectLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spTestSubjectExist"; //Exist sp name

		//properties
		/// <summary>TestSubjectID is a Property in the TestSubject Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>AddressID is a Property in the TestSubject Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>DateCreated is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DriverLicenseNumber is a Property in the TestSubject Class of type String</summary>
		public string DriverLicenseNumber 
		{
			get{return _strDriverLicenseNumber;}
			set{_strDriverLicenseNumber = value;}
		}
		/// <summary>FirstName is a Property in the TestSubject Class of type String</summary>
		public string FirstName 
		{
			get{return _strFirstName;}
			set{_strFirstName = value;}
		}
		/// <summary>MiddleName is a Property in the TestSubject Class of type String</summary>
		public string MiddleName 
		{
			get{return _strMiddleName;}
			set{_strMiddleName = value;}
		}
		/// <summary>LastName is a Property in the TestSubject Class of type String</summary>
		public string LastName 
		{
			get{return _strLastName;}
			set{_strLastName = value;}
		}
		/// <summary>PrimaryPhone is a Property in the TestSubject Class of type String</summary>
		public string PrimaryPhone 
		{
			get{return _strPrimaryPhone;}
			set{_strPrimaryPhone = value;}
		}
		/// <summary>EmailAddress is a Property in the TestSubject Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>Code is a Property in the TestSubject Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>IdentificationRawData is a Property in the TestSubject Class of type String</summary>
		public string IdentificationRawData 
		{
			get{return _strIdentificationRawData;}
			set{_strIdentificationRawData = value;}
		}
		/// <summary>Notes is a Property in the TestSubject Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>HasError Property in class TestSubject and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestSubject and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>TestSubject empty constructor</summary>
		public TestSubject()
		{
		}
		/// <summary>TestSubject constructor takes a Config</summary>
		public TestSubject(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>TestSubject constructor takes TestSubjectID and a SqlConnection</summary>
		public TestSubject(long l, SqlConnection conn) 
		{
			TestSubjectID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>TestSubject Constructor takes pStrData and Config</summary>
		public TestSubject(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>TestSubject Constructor takes SqlDataReader</summary>
		public TestSubject(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestSubject Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestSubjectID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_DRIVER_LICENSE_NUMBER + ":  " + DriverLicenseNumber + "\n");
			sbReturn.Append(TAG_FIRST_NAME + ":  " + FirstName + "\n");
			sbReturn.Append(TAG_MIDDLE_NAME + ":  " + MiddleName + "\n");
			sbReturn.Append(TAG_LAST_NAME + ":  " + LastName + "\n");
			sbReturn.Append(TAG_PRIMARY_PHONE + ":  " + PrimaryPhone + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_IDENTIFICATION_RAW_DATA + ":  " + IdentificationRawData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestSubject</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<TestSubject>\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestSubjectID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_DRIVER_LICENSE_NUMBER + ">" + DriverLicenseNumber + "</" + TAG_DRIVER_LICENSE_NUMBER + ">\n");
			sbReturn.Append("<" + TAG_FIRST_NAME + ">" + FirstName + "</" + TAG_FIRST_NAME + ">\n");
			sbReturn.Append("<" + TAG_MIDDLE_NAME + ">" + MiddleName + "</" + TAG_MIDDLE_NAME + ">\n");
			sbReturn.Append("<" + TAG_LAST_NAME + ">" + LastName + "</" + TAG_LAST_NAME + ">\n");
			sbReturn.Append("<" + TAG_PRIMARY_PHONE + ">" + PrimaryPhone + "</" + TAG_PRIMARY_PHONE + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_IDENTIFICATION_RAW_DATA + ">" + IdentificationRawData + "</" + TAG_IDENTIFICATION_RAW_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</TestSubject>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestSubjectID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DRIVER_LICENSE_NUMBER);
				DriverLicenseNumber = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_FIRST_NAME);
				FirstName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MIDDLE_NAME);
				MiddleName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_LAST_NAME);
				LastName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PRIMARY_PHONE);
				PrimaryPhone = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IDENTIFICATION_RAW_DATA);
				IdentificationRawData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with test_subject_id equal to the current object's TestSubjectID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where test_subject_id equal to the current object's TestSubjectID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where test_subject_id equal to the current object's TestSubjectID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(TestSubject.TAG_ADDRESS_ID + ":  ");
				AddressID = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(TestSubject.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(TestSubject.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}

				Console.WriteLine(TestSubject.TAG_DRIVER_LICENSE_NUMBER + ":  ");
				DriverLicenseNumber = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_FIRST_NAME + ":  ");
				FirstName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_MIDDLE_NAME + ":  ");
				MiddleName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_LAST_NAME + ":  ");
				LastName = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_PRIMARY_PHONE + ":  ");
				PrimaryPhone = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_EMAIL_ADDRESS + ":  ");
				EmailAddress = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_CODE + ":  ");
				Code = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_IDENTIFICATION_RAW_DATA + ":  ");
				IdentificationRawData = Console.ReadLine();

				Console.WriteLine(TestSubject.TAG_NOTES + ":  ");
				Notes = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramDriverLicenseNumber = null;
			SqlParameter paramFirstName = null;
			SqlParameter paramMiddleName = null;
			SqlParameter paramLastName = null;
			SqlParameter paramPrimaryPhone = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramCode = null;
			SqlParameter paramIdentificationRawData = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			paramDriverLicenseNumber = new SqlParameter("@" + TAG_DRIVER_LICENSE_NUMBER, DriverLicenseNumber);
			paramDriverLicenseNumber.DbType = DbType.String;
			paramDriverLicenseNumber.Size = 255;
			paramDriverLicenseNumber.Direction = ParameterDirection.Input;

			paramFirstName = new SqlParameter("@" + TAG_FIRST_NAME, FirstName);
			paramFirstName.DbType = DbType.String;
			paramFirstName.Size = 255;
			paramFirstName.Direction = ParameterDirection.Input;

			paramMiddleName = new SqlParameter("@" + TAG_MIDDLE_NAME, MiddleName);
			paramMiddleName.DbType = DbType.String;
			paramMiddleName.Size = 255;
			paramMiddleName.Direction = ParameterDirection.Input;

			paramLastName = new SqlParameter("@" + TAG_LAST_NAME, LastName);
			paramLastName.DbType = DbType.String;
			paramLastName.Size = 255;
			paramLastName.Direction = ParameterDirection.Input;

			paramPrimaryPhone = new SqlParameter("@" + TAG_PRIMARY_PHONE, PrimaryPhone);
			paramPrimaryPhone.DbType = DbType.String;
			paramPrimaryPhone.Size = 255;
			paramPrimaryPhone.Direction = ParameterDirection.Input;

			paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
			paramEmailAddress.DbType = DbType.String;
			paramEmailAddress.Size = 255;
			paramEmailAddress.Direction = ParameterDirection.Input;

			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramIdentificationRawData = new SqlParameter("@" + TAG_IDENTIFICATION_RAW_DATA, IdentificationRawData);
			paramIdentificationRawData.DbType = DbType.String;
			paramIdentificationRawData.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramDriverLicenseNumber);
			cmd.Parameters.Add(paramFirstName);
			cmd.Parameters.Add(paramMiddleName);
			cmd.Parameters.Add(paramLastName);
			cmd.Parameters.Add(paramPrimaryPhone);
			cmd.Parameters.Add(paramEmailAddress);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramIdentificationRawData);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			TestSubjectID = long.Parse(strTmp);

			// cleanup to help GC
			paramAddressID = null;
			paramDateCreated = null;
			paramDriverLicenseNumber = null;
			paramFirstName = null;
			paramMiddleName = null;
			paramLastName = null;
			paramPrimaryPhone = null;
			paramEmailAddress = null;
			paramCode = null;
			paramIdentificationRawData = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramTestSubjectID = new SqlParameter("@" + TAG_ID, TestSubjectID);
			paramTestSubjectID.Direction = ParameterDirection.Input;
			paramTestSubjectID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramTestSubjectID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramTestSubjectID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestSubjectID = null;
			SqlParameter paramAddressID = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramDriverLicenseNumber = null;
			SqlParameter paramFirstName = null;
			SqlParameter paramMiddleName = null;
			SqlParameter paramLastName = null;
			SqlParameter paramPrimaryPhone = null;
			SqlParameter paramEmailAddress = null;
			SqlParameter paramCode = null;
			SqlParameter paramIdentificationRawData = null;
			SqlParameter paramNotes = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramTestSubjectID = new SqlParameter("@" + TAG_ID, TestSubjectID);
			paramTestSubjectID.DbType = DbType.Int32;
			paramTestSubjectID.Direction = ParameterDirection.Input;


			paramAddressID = new SqlParameter("@" + TAG_ADDRESS_ID, AddressID);
			paramAddressID.DbType = DbType.Int32;
			paramAddressID.Direction = ParameterDirection.Input;


				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			paramDriverLicenseNumber = new SqlParameter("@" + TAG_DRIVER_LICENSE_NUMBER, DriverLicenseNumber);
			paramDriverLicenseNumber.DbType = DbType.String;
			paramDriverLicenseNumber.Size = 255;
			paramDriverLicenseNumber.Direction = ParameterDirection.Input;

			paramFirstName = new SqlParameter("@" + TAG_FIRST_NAME, FirstName);
			paramFirstName.DbType = DbType.String;
			paramFirstName.Size = 255;
			paramFirstName.Direction = ParameterDirection.Input;

			paramMiddleName = new SqlParameter("@" + TAG_MIDDLE_NAME, MiddleName);
			paramMiddleName.DbType = DbType.String;
			paramMiddleName.Size = 255;
			paramMiddleName.Direction = ParameterDirection.Input;

			paramLastName = new SqlParameter("@" + TAG_LAST_NAME, LastName);
			paramLastName.DbType = DbType.String;
			paramLastName.Size = 255;
			paramLastName.Direction = ParameterDirection.Input;

			paramPrimaryPhone = new SqlParameter("@" + TAG_PRIMARY_PHONE, PrimaryPhone);
			paramPrimaryPhone.DbType = DbType.String;
			paramPrimaryPhone.Size = 255;
			paramPrimaryPhone.Direction = ParameterDirection.Input;

			paramEmailAddress = new SqlParameter("@" + TAG_EMAIL_ADDRESS, EmailAddress);
			paramEmailAddress.DbType = DbType.String;
			paramEmailAddress.Size = 255;
			paramEmailAddress.Direction = ParameterDirection.Input;

			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramIdentificationRawData = new SqlParameter("@" + TAG_IDENTIFICATION_RAW_DATA, IdentificationRawData);
			paramIdentificationRawData.DbType = DbType.String;
			paramIdentificationRawData.Direction = ParameterDirection.Input;

			paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
			paramNotes.DbType = DbType.String;
			paramNotes.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramTestSubjectID);
			cmd.Parameters.Add(paramAddressID);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramDriverLicenseNumber);
			cmd.Parameters.Add(paramFirstName);
			cmd.Parameters.Add(paramMiddleName);
			cmd.Parameters.Add(paramLastName);
			cmd.Parameters.Add(paramPrimaryPhone);
			cmd.Parameters.Add(paramEmailAddress);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramIdentificationRawData);
			cmd.Parameters.Add(paramNotes);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			TestSubjectID = long.Parse(s);

			// cleanup
			paramTestSubjectID = null;
			paramAddressID = null;
			paramDateModified = null;
			paramDriverLicenseNumber = null;
			paramFirstName = null;
			paramMiddleName = null;
			paramLastName = null;
			paramPrimaryPhone = null;
			paramEmailAddress = null;
			paramCode = null;
			paramIdentificationRawData = null;
			paramNotes = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestSubjectID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramTestSubjectID = new SqlParameter("@" + TAG_ID, TestSubjectID);
			paramTestSubjectID.DbType = DbType.Int32;
			paramTestSubjectID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramTestSubjectID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramTestSubjectID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramTestSubjectID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramTestSubjectID = new SqlParameter("@" + TAG_ID, TestSubjectID);
			paramTestSubjectID.DbType = DbType.Int32;
			paramTestSubjectID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramTestSubjectID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramTestSubjectID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.TestSubjectID = long.Parse(rdr[DB_FIELD_ID].ToString());
			try
			{
			this.AddressID = Convert.ToInt32(rdr[DB_FIELD_ADDRESS_ID].ToString().Trim());
			}
			catch{}
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
			this.DriverLicenseNumber = rdr[DB_FIELD_DRIVER_LICENSE_NUMBER].ToString().Trim();
			this.FirstName = rdr[DB_FIELD_FIRST_NAME].ToString().Trim();
			this.MiddleName = rdr[DB_FIELD_MIDDLE_NAME].ToString().Trim();
			this.LastName = rdr[DB_FIELD_LAST_NAME].ToString().Trim();
			this.PrimaryPhone = rdr[DB_FIELD_PRIMARY_PHONE].ToString().Trim();
			this.EmailAddress = rdr[DB_FIELD_EMAIL_ADDRESS].ToString().Trim();
			this.Code = rdr[DB_FIELD_CODE].ToString().Trim();
			this.IdentificationRawData = rdr[DB_FIELD_IDENTIFICATION_RAW_DATA].ToString().Trim();
			this.Notes = rdr[DB_FIELD_NOTES].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestSubjectID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestSubjectID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + TestSubjectID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestSubjectID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							TestSubjectID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF TestSubject CLASS FILE
