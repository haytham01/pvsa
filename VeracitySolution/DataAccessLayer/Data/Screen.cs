using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  Screen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/25/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the Screen database table.
    /// </summary>
    public class Screen
    {
        //Attributes
        /// <summary>ScreenID Attribute type String</summary>
        private long _lScreenID = 0;
        /// <summary>ScreenTypeID Attribute type String</summary>
        private long _lScreenTypeID = 0;
        /// <summary>DateCreated Attribute type String</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>ScreenText Attribute type String</summary>
        private string _strScreenText = null;
        /// <summary>ScreenImage Attribute type String</summary>
        private byte[] _byteScreenImage = null;
        /// <summary>AudioData Attribute type String</summary>
        private byte[] _byteAudioData = null;
        /// <summary>IsText Attribute type String</summary>
        private bool? _bIsText = null;
        /// <summary>IsAudio Attribute type String</summary>
        private bool? _bIsAudio = null;
        /// <summary>ScreenTextShort Attribute type String</summary>
        private string _strScreenTextShort = null;

        private Config _config = null;
        private ErrorCode _errorCode = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Data-Screen";
        private bool _hasError = false;
        private static DateTime dtNull = new DateTime();

        /// <summary>HasError Property in class Screen and is of type bool</summary>
        public static readonly string ENTITY_NAME = "Screen"; //Table name to abstract

        // DB Field names
        /// <summary>ID Database field</summary>
        public static readonly string DB_FIELD_ID = "screen_id"; //Table id field name
        /// <summary>screen_type_id Database field </summary>
        public static readonly string DB_FIELD_SCREEN_TYPE_ID = "screen_type_id"; //Table ScreenTypeID field name
        /// <summary>date_created Database field </summary>
        public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
        /// <summary>screen_text Database field </summary>
        public static readonly string DB_FIELD_SCREEN_TEXT = "screen_text"; //Table ScreenText field name
        /// <summary>screen_image Database field </summary>
        public static readonly string DB_FIELD_SCREEN_IMAGE = "screen_image"; //Table ScreenImage field name
        /// <summary>audio_data Database field </summary>
        public static readonly string DB_FIELD_AUDIO_DATA = "audio_data"; //Table AudioData field name
        /// <summary>is_text Database field </summary>
        public static readonly string DB_FIELD_IS_TEXT = "is_text"; //Table IsText field name
        /// <summary>is_audio Database field </summary>
        public static readonly string DB_FIELD_IS_AUDIO = "is_audio"; //Table IsAudio field name
        /// <summary>screen_text_short Database field </summary>
        public static readonly string DB_FIELD_SCREEN_TEXT_SHORT = "screen_text_short"; //Table ScreenTextShort field name

        // Attribute variables
        /// <summary>TAG_ID Attribute type string</summary>
        public static readonly string TAG_ID = "ScreenID"; //Attribute id  name
        /// <summary>ScreenTypeID Attribute type string</summary>
        public static readonly string TAG_SCREEN_TYPE_ID = "ScreenTypeID"; //Table ScreenTypeID field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>ScreenText Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT = "ScreenText"; //Table ScreenText field name
        /// <summary>ScreenImage Attribute type string</summary>
        public static readonly string TAG_SCREEN_IMAGE = "ScreenImage"; //Table ScreenImage field name
        /// <summary>AudioData Attribute type string</summary>
        public static readonly string TAG_AUDIO_DATA = "AudioData"; //Table AudioData field name
        /// <summary>IsText Attribute type string</summary>
        public static readonly string TAG_IS_TEXT = "IsText"; //Table IsText field name
        /// <summary>IsAudio Attribute type string</summary>
        public static readonly string TAG_IS_AUDIO = "IsAudio"; //Table IsAudio field name
        /// <summary>ScreenTextShort Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT_SHORT = "ScreenTextShort"; //Table ScreenTextShort field name

        // Stored procedure names
        private static readonly string SP_INSERT_NAME = "spScreenInsert"; //Insert sp name
        private static readonly string SP_UPDATE_NAME = "spScreenUpdate"; //Update sp name
        private static readonly string SP_DELETE_NAME = "spScreenDelete"; //Delete sp name
        private static readonly string SP_LOAD_NAME = "spScreenLoad"; //Load sp name
        private static readonly string SP_EXIST_NAME = "spScreenExist"; //Exist sp name

        //properties
        /// <summary>ScreenID is a Property in the Screen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>ScreenTypeID is a Property in the Screen Class of type long</summary>
        public long ScreenTypeID
        {
            get { return _lScreenTypeID; }
            set { _lScreenTypeID = value; }
        }
        /// <summary>DateCreated is a Property in the Screen Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>ScreenText is a Property in the Screen Class of type String</summary>
        public string ScreenText
        {
            get { return _strScreenText; }
            set { _strScreenText = value; }
        }
        /// <summary>ScreenImage is a Property in the Screen Class of type byte[]</summary>
        public byte[] ScreenImage
        {
            get { return _byteScreenImage; }
            set { _byteScreenImage = value; }
        }
        /// <summary>AudioData is a Property in the Screen Class of type byte[]</summary>
        public byte[] AudioData
        {
            get { return _byteAudioData; }
            set { _byteAudioData = value; }
        }
        /// <summary>IsText is a Property in the Screen Class of type bool</summary>
        public bool? IsText
        {
            get { return _bIsText; }
            set { _bIsText = value; }
        }
        /// <summary>IsAudio is a Property in the Screen Class of type bool</summary>
        public bool? IsAudio
        {
            get { return _bIsAudio; }
            set { _bIsAudio = value; }
        }
        /// <summary>ScreenTextShort is a Property in the Screen Class of type String</summary>
        public string ScreenTextShort
        {
            get { return _strScreenTextShort; }
            set { _strScreenTextShort = value; }
        }


        /*********************** CUSTOM NON-META BEGIN *********************/
        /// <summary>Creates well formatted XML - includes all properties of Screen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<Screen>\n");
            sbReturn.Append("<" + TAG_ID + ">" + ScreenID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TYPE_ID + ">" + ScreenTypeID + "</" + TAG_SCREEN_TYPE_ID + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            //sbReturn.Append("<" + TAG_SCREEN_TEXT + ">" + ScreenText + "</" + TAG_SCREEN_TEXT + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TEXT + "><![CDATA[" + ScreenText + "]]></" + TAG_SCREEN_TEXT + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_IMAGE + "><![CDATA[" + ScreenImage + "]]></" + TAG_SCREEN_IMAGE + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_DATA + "><![CDATA[" + AudioData + "]]></" + TAG_AUDIO_DATA + ">\n");
            //sbReturn.Append("<" + TAG_SCREEN_IMAGE + ">" + ScreenImage + "</" + TAG_SCREEN_IMAGE + ">\n");
            //sbReturn.Append("<" + TAG_AUDIO_DATA + ">" + AudioData + "</" + TAG_AUDIO_DATA + ">\n");
            sbReturn.Append("<" + TAG_IS_TEXT + ">" + IsText + "</" + TAG_IS_TEXT + ">\n");
            sbReturn.Append("<" + TAG_IS_AUDIO + ">" + IsAudio + "</" + TAG_IS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TEXT_SHORT + ">" + ScreenTextShort + "</" + TAG_SCREEN_TEXT_SHORT + ">\n");
            sbReturn.Append("</Screen>" + "\n");

            return sbReturn.ToString();
        }

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>HasError Property in class Screen and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class Screen and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }

        //Constructors
        /// <summary>Screen empty constructor</summary>
        public Screen()
        {
        }
        /// <summary>Screen constructor takes a Config</summary>
        public Screen(Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }
        /// <summary>Screen constructor takes ScreenID and a SqlConnection</summary>
        public Screen(long l, SqlConnection conn)
        {
            ScreenID = l;
            try
            {
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Screen Constructor takes pStrData and Config</summary>
        public Screen(string pStrData, Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
            Parse(pStrData);
        }
        /// <summary>Screen Constructor takes SqlDataReader</summary>
        public Screen(SqlDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the Screen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + ScreenID.ToString() + "\n");
            sbReturn.Append(TAG_SCREEN_TYPE_ID + ":  " + ScreenTypeID + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            sbReturn.Append(TAG_SCREEN_TEXT + ":  " + ScreenText + "\n");
            sbReturn.Append(TAG_SCREEN_IMAGE + ":  " + ScreenImage + "\n");
            sbReturn.Append(TAG_AUDIO_DATA + ":  " + AudioData + "\n");
            sbReturn.Append(TAG_IS_TEXT + ":  " + IsText + "\n");
            sbReturn.Append(TAG_IS_AUDIO + ":  " + IsAudio + "\n");
            sbReturn.Append(TAG_SCREEN_TEXT_SHORT + ":  " + ScreenTextShort + "\n");

            return sbReturn.ToString();
        }
 
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                ScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TYPE_ID);
                ScreenTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT);
                ScreenText = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
            //Cannot reliably convert byte[] to string.
            //Cannot reliably convert byte[] to string.

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_TEXT);
                IsText = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsText = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_AUDIO);
                IsAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT_SHORT);
                ScreenTextShort = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
        }
        /// <summary>Calls sqlLoad() method which gets record from database with screen_id equal to the current object's ScreenID </summary>
        public void Load(SqlConnection conn)
        {
            try
            {
                _log("LOAD", ToString());
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlUpdate() method which record record from database with current object values where screen_id equal to the current object's ScreenID </summary>
        public void Update(SqlConnection conn)
        {
            bool bExist = false;
            try
            {
                _log("UPDATE", ToString());
                bExist = Exist(conn);
                if (bExist)
                {
                    sqlUpdate(conn);
                }
                else
                {
                    _log("NOT_EXIST", ToString());
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
        public void Save(SqlConnection conn)
        {
            try
            {
                bool bExist = false;

                _log("INSERT", ToString());
                bExist = Exist(conn);
                if (!bExist)
                {
                    sqlInsert(conn);
                }
                else
                {
                    _log("ALREADY_EXISTS", ToString());
                    sqlUpdate(conn);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlDelete() method which delete's the record from database where where screen_id equal to the current object's ScreenID </summary>
        public void Delete(SqlConnection conn)
        {
            try
            {
                _log("DELETE", ToString());
                sqlDelete(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
        public bool Exist(SqlConnection conn)
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return bReturn;
        }
        /// <summary>Prompt user to enter Property values</summary>
        public void Prompt()
        {
            try
            {
                {
                    Console.WriteLine(TAG_ID + ":  ");
                    try
                    {
                        ScreenID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        ScreenID = 0;
                    }
                }

                Console.WriteLine(Screen.TAG_SCREEN_TYPE_ID + ":  ");
                ScreenTypeID = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(Screen.TAG_DATE_CREATED + ":  ");
                    DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    DateCreated = new DateTime();
                }

                Console.WriteLine(Screen.TAG_SCREEN_TEXT + ":  ");
                ScreenText = Console.ReadLine();
                //Cannot reliably convert byte[] to string.
                //Cannot reliably convert byte[] to string.

                Console.WriteLine(Screen.TAG_IS_TEXT + ":  ");
                IsText = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Screen.TAG_IS_AUDIO + ":  ");
                IsAudio = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(Screen.TAG_SCREEN_TEXT_SHORT + ":  ");
                ScreenTextShort = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        //protected
        /// <summary>Inserts row of data into the database</summary>
        protected void sqlInsert(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramScreenTypeID = null;
            SqlParameter paramDateCreated = null;
            SqlParameter paramScreenText = null;
            SqlParameter paramScreenImage = null;
            SqlParameter paramAudioData = null;
            SqlParameter paramIsText = null;
            SqlParameter paramIsAudio = null;
            SqlParameter paramScreenTextShort = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_INSERT_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters
            paramScreenID = new SqlParameter("@" + TAG_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;

            paramScreenTypeID = new SqlParameter("@" + TAG_SCREEN_TYPE_ID, ScreenTypeID);
            paramScreenTypeID.DbType = DbType.Int32;
            paramScreenTypeID.Direction = ParameterDirection.Input;

            paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
            paramDateCreated.DbType = DbType.DateTime;
            paramDateCreated.Direction = ParameterDirection.Input;

            paramScreenText = new SqlParameter("@" + TAG_SCREEN_TEXT, ScreenText);
            paramScreenText.DbType = DbType.String;
            paramScreenText.Direction = ParameterDirection.Input;

            paramScreenImage = new SqlParameter("@" + TAG_SCREEN_IMAGE, ScreenImage);
            paramScreenImage.DbType = DbType.Binary;
            paramScreenImage.Size = 2147483647;
            paramScreenImage.Direction = ParameterDirection.Input;

            paramAudioData = new SqlParameter("@" + TAG_AUDIO_DATA, AudioData);
            paramAudioData.DbType = DbType.Binary;
            paramAudioData.Size = 2147483647;
            paramAudioData.Direction = ParameterDirection.Input;

            paramIsText = new SqlParameter("@" + TAG_IS_TEXT, IsText);
            paramIsText.DbType = DbType.Boolean;
            paramIsText.Direction = ParameterDirection.Input;

            paramIsAudio = new SqlParameter("@" + TAG_IS_AUDIO, IsAudio);
            paramIsAudio.DbType = DbType.Boolean;
            paramIsAudio.Direction = ParameterDirection.Input;

            paramScreenTextShort = new SqlParameter("@" + TAG_SCREEN_TEXT_SHORT, ScreenTextShort);
            paramScreenTextShort.DbType = DbType.String;
            paramScreenTextShort.Direction = ParameterDirection.Input;

            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramScreenID);
            cmd.Parameters.Add(paramScreenTypeID);
            cmd.Parameters.Add(paramDateCreated);
            cmd.Parameters.Add(paramScreenText);
            cmd.Parameters.Add(paramScreenImage);
            cmd.Parameters.Add(paramAudioData);
            cmd.Parameters.Add(paramIsText);
            cmd.Parameters.Add(paramIsAudio);
            cmd.Parameters.Add(paramScreenTextShort);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            // assign the primary kiey
            string strTmp;
            strTmp = cmd.Parameters["@PKID"].Value.ToString();
            ScreenID = long.Parse(strTmp);

            // cleanup to help GC
            paramScreenID = null;
            paramScreenTypeID = null;
            paramDateCreated = null;
            paramScreenText = null;
            paramScreenImage = null;
            paramAudioData = null;
            paramIsText = null;
            paramIsAudio = null;
            paramScreenTextShort = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Check to see if the row exists in database</summary>
        protected bool sqlExist(SqlConnection conn)
        {
            bool bExist = false;

            SqlCommand cmd = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramCount = null;

            cmd = new SqlCommand(SP_EXIST_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            paramScreenID = new SqlParameter("@" + TAG_ID, ScreenID);
            paramScreenID.Direction = ParameterDirection.Input;
            paramScreenID.DbType = DbType.Int32;

            paramCount = new SqlParameter();
            paramCount.ParameterName = "@COUNT";
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramScreenID);
            cmd.Parameters.Add(paramCount);
            cmd.ExecuteNonQuery();

            string strTmp;
            int nCount = 0;
            strTmp = cmd.Parameters["@COUNT"].Value.ToString();
            nCount = int.Parse(strTmp);
            if (nCount > 0)
            {
                bExist = true;
            }

            // cleanup
            paramScreenID = null;
            paramCount = null;
            cmd = null;

            return bExist;
        }
        /// <summary>Updates row of data in database</summary>
        protected void sqlUpdate(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramScreenTypeID = null;
            SqlParameter paramScreenText = null;
            SqlParameter paramScreenImage = null;
            SqlParameter paramAudioData = null;
            SqlParameter paramIsText = null;
            SqlParameter paramIsAudio = null;
            SqlParameter paramScreenTextShort = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_UPDATE_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters

            paramScreenID = new SqlParameter("@" + TAG_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;


            paramScreenTypeID = new SqlParameter("@" + TAG_SCREEN_TYPE_ID, ScreenTypeID);
            paramScreenTypeID.DbType = DbType.Int32;
            paramScreenTypeID.Direction = ParameterDirection.Input;


            paramScreenText = new SqlParameter("@" + TAG_SCREEN_TEXT, ScreenText);
            paramScreenText.DbType = DbType.String;
            paramScreenText.Direction = ParameterDirection.Input;

            paramScreenImage = new SqlParameter("@" + TAG_SCREEN_IMAGE, ScreenImage);
            paramScreenImage.DbType = DbType.Binary;
            paramScreenImage.Size = 2147483647;
            paramScreenImage.Direction = ParameterDirection.Input;

            paramAudioData = new SqlParameter("@" + TAG_AUDIO_DATA, AudioData);
            paramAudioData.DbType = DbType.Binary;
            paramAudioData.Size = 2147483647;
            paramAudioData.Direction = ParameterDirection.Input;

            paramIsText = new SqlParameter("@" + TAG_IS_TEXT, IsText);
            paramIsText.DbType = DbType.Boolean;
            paramIsText.Direction = ParameterDirection.Input;

            paramIsAudio = new SqlParameter("@" + TAG_IS_AUDIO, IsAudio);
            paramIsAudio.DbType = DbType.Boolean;
            paramIsAudio.Direction = ParameterDirection.Input;

            paramScreenTextShort = new SqlParameter("@" + TAG_SCREEN_TEXT_SHORT, ScreenTextShort);
            paramScreenTextShort.DbType = DbType.String;
            paramScreenTextShort.Direction = ParameterDirection.Input;

            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramScreenID);
            cmd.Parameters.Add(paramScreenTypeID);
            cmd.Parameters.Add(paramScreenText);
            cmd.Parameters.Add(paramScreenImage);
            cmd.Parameters.Add(paramAudioData);
            cmd.Parameters.Add(paramIsText);
            cmd.Parameters.Add(paramIsAudio);
            cmd.Parameters.Add(paramScreenTextShort);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            string s;
            s = cmd.Parameters["@PKID"].Value.ToString();
            ScreenID = long.Parse(s);

            // cleanup
            paramScreenID = null;
            paramScreenTypeID = null;
            paramScreenText = null;
            paramScreenImage = null;
            paramAudioData = null;
            paramIsText = null;
            paramIsAudio = null;
            paramScreenTextShort = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Deletes row of data in database</summary>
        protected void sqlDelete(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramScreenID = null;

            cmd = new SqlCommand(SP_DELETE_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramScreenID = new SqlParameter("@" + TAG_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramScreenID);
            cmd.ExecuteNonQuery();

            // cleanup to help GC
            paramScreenID = null;
            cmd = null;

        }
        /// <summary>Load row of data from database</summary>
        protected void sqlLoad(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramScreenID = null;
            SqlDataReader rdr = null;

            cmd = new SqlCommand(SP_LOAD_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramScreenID = new SqlParameter("@" + TAG_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramScreenID);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                sqlParseResultSet(rdr);
            }
            // cleanup
            rdr.Close();
            rdr = null;
            paramScreenID = null;
            cmd = null;
        }
        /// <summary>Parse result set</summary>
        protected void sqlParseResultSet(SqlDataReader rdr)
        {
            this.ScreenID = long.Parse(rdr[DB_FIELD_ID].ToString());
            try
            {
                this.ScreenTypeID = Convert.ToInt32(rdr[DB_FIELD_SCREEN_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
            }
            catch
            {
            }
            this.ScreenText = rdr[DB_FIELD_SCREEN_TEXT].ToString().Trim();
            if (rdr[rdr.GetOrdinal(DB_FIELD_SCREEN_IMAGE)] != DBNull.Value)
            {
                this.ScreenImage = (byte[])rdr[rdr.GetOrdinal(DB_FIELD_SCREEN_IMAGE)];
            }
            if (rdr[rdr.GetOrdinal(DB_FIELD_AUDIO_DATA)] != DBNull.Value)
            {
                this.AudioData = (byte[])rdr[rdr.GetOrdinal(DB_FIELD_AUDIO_DATA)];
            }
            try
            {
                this.IsText = Convert.ToBoolean(rdr[DB_FIELD_IS_TEXT].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsAudio = Convert.ToBoolean(rdr[DB_FIELD_IS_AUDIO].ToString().Trim());
            }
            catch { }
            this.ScreenTextShort = rdr[DB_FIELD_SCREEN_TEXT_SHORT].ToString().Trim();
        }

        //private
        /// <summary>Log errors</summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test(SqlConnection conn)
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Delete.");
                Console.WriteLine("3.  Update.");
                Console.WriteLine("4.  Exist.");
                Console.WriteLine("5.  Load.");
                Console.WriteLine("6.  ToXml.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // insert
                            Console.WriteLine("Save:  ");
                            Prompt();
                            Save(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("Delete " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            ScreenID = long.Parse(strAns);
                            Delete(conn);
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 3:
                            Console.WriteLine("Update:  ");
                            Prompt();
                            Update(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 4:
                            Console.WriteLine("Exist " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            ScreenID = long.Parse(strAns);
                            bool bExist = false;
                            bExist = Exist(conn);
                            Console.WriteLine("Record id " + ScreenID + " exist:  " + bExist.ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 5:
                            Console.WriteLine("Load " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            ScreenID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 6:
                            Console.WriteLine("ToXml " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            ScreenID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToXml());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }

        }
    }
}

//END OF Screen CLASS FILE