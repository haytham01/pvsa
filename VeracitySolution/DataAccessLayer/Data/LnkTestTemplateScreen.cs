using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  LnkTestTemplateScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/14/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the LnkTestTemplateScreen database table.
    /// </summary>
    public class LnkTestTemplateScreen
    {
        //Attributes
        /// <summary>LnkTestTemplateScreenID Attribute type String</summary>
        private long _lLnkTestTemplateScreenID = 0;
        /// <summary>TestTemplateID Attribute type String</summary>
        private long _lTestTemplateID = 0;
        /// <summary>ScreenID Attribute type String</summary>
        private long _lScreenID = 0;
        /// <summary>DateCreated Attribute type String</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>DateModified Attribute type String</summary>
        private DateTime _dtDateModified = dtNull;
        /// <summary>FlowOrderIndex Attribute type String</summary>
        private long _lFlowOrderIndex = 0;
        /// <summary>TransitionDelayInMs Attribute type String</summary>
        private long _lTransitionDelayInMs = 0;
        /// <summary>IsIgnored Attribute type String</summary>
        private bool? _bIsIgnored = null;
        /// <summary>IsReported Attribute type String</summary>
        private bool? _bIsReported = null;
        /// <summary>Notes Attribute type String</summary>
        private string _strNotes = null;
        /// <summary>ImageFileName Attribute type String</summary>
        private string _strImageFileName = null;
        /// <summary>HasAudio Attribute type String</summary>
        private bool? _bHasAudio = null;
        /// <summary>AudioFileName Attribute type String</summary>
        private string _strAudioFileName = null;

        private Config _config = null;
        private ErrorCode _errorCode = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Data-LnkTestTemplateScreen";
        private bool _hasError = false;
        private static DateTime dtNull = new DateTime();

        /// <summary>HasError Property in class LnkTestTemplateScreen and is of type bool</summary>
        public static readonly string ENTITY_NAME = "LnkTestTemplateScreen"; //Table name to abstract

        // DB Field names
        /// <summary>ID Database field</summary>
        public static readonly string DB_FIELD_ID = "lnk_test_template_screen_id"; //Table id field name
        /// <summary>test_template_id Database field </summary>
        public static readonly string DB_FIELD_TEST_TEMPLATE_ID = "test_template_id"; //Table TestTemplateID field name
        /// <summary>screen_id Database field </summary>
        public static readonly string DB_FIELD_SCREEN_ID = "screen_id"; //Table ScreenID field name
        /// <summary>date_created Database field </summary>
        public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
        /// <summary>date_modified Database field </summary>
        public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
        /// <summary>flow_order_index Database field </summary>
        public static readonly string DB_FIELD_FLOW_ORDER_INDEX = "flow_order_index"; //Table FlowOrderIndex field name
        /// <summary>transition_delay_in_ms Database field </summary>
        public static readonly string DB_FIELD_TRANSITION_DELAY_IN_MS = "transition_delay_in_ms"; //Table TransitionDelayInMs field name
        /// <summary>is_ignored Database field </summary>
        public static readonly string DB_FIELD_IS_IGNORED = "is_ignored"; //Table IsIgnored field name
        /// <summary>is_reported Database field </summary>
        public static readonly string DB_FIELD_IS_REPORTED = "is_reported"; //Table IsReported field name
        /// <summary>notes Database field </summary>
        public static readonly string DB_FIELD_NOTES = "notes"; //Table Notes field name
        /// <summary>image_file_name Database field </summary>
        public static readonly string DB_FIELD_IMAGE_FILE_NAME = "image_file_name"; //Table ImageFileName field name
        /// <summary>has_audio Database field </summary>
        public static readonly string DB_FIELD_HAS_AUDIO = "has_audio"; //Table HasAudio field name
        /// <summary>audio_file_name Database field </summary>
        public static readonly string DB_FIELD_AUDIO_FILE_NAME = "audio_file_name"; //Table AudioFileName field name

        // Attribute variables
        /// <summary>TAG_ID Attribute type string</summary>
        public static readonly string TAG_ID = "LnkTestTemplateScreenID"; //Attribute id  name
        /// <summary>TestTemplateID Attribute type string</summary>
        public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Table TestTemplateID field name
        /// <summary>ScreenID Attribute type string</summary>
        public static readonly string TAG_SCREEN_ID = "ScreenID"; //Table ScreenID field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
        /// <summary>FlowOrderIndex Attribute type string</summary>
        public static readonly string TAG_FLOW_ORDER_INDEX = "FlowOrderIndex"; //Table FlowOrderIndex field name
        /// <summary>TransitionDelayInMs Attribute type string</summary>
        public static readonly string TAG_TRANSITION_DELAY_IN_MS = "TransitionDelayInMs"; //Table TransitionDelayInMs field name
        /// <summary>IsIgnored Attribute type string</summary>
        public static readonly string TAG_IS_IGNORED = "IsIgnored"; //Table IsIgnored field name
        /// <summary>IsReported Attribute type string</summary>
        public static readonly string TAG_IS_REPORTED = "IsReported"; //Table IsReported field name
        /// <summary>Notes Attribute type string</summary>
        public static readonly string TAG_NOTES = "Notes"; //Table Notes field name
        /// <summary>ImageFileName Attribute type string</summary>
        public static readonly string TAG_IMAGE_FILE_NAME = "ImageFileName"; //Table ImageFileName field name
        /// <summary>HasAudio Attribute type string</summary>
        public static readonly string TAG_HAS_AUDIO = "HasAudio"; //Table HasAudio field name
        /// <summary>AudioFileName Attribute type string</summary>
        public static readonly string TAG_AUDIO_FILE_NAME = "AudioFileName"; //Table AudioFileName field name

        // Stored procedure names
        private static readonly string SP_INSERT_NAME = "spLnkTestTemplateScreenInsert"; //Insert sp name
        private static readonly string SP_UPDATE_NAME = "spLnkTestTemplateScreenUpdate"; //Update sp name
        private static readonly string SP_DELETE_NAME = "spLnkTestTemplateScreenDelete"; //Delete sp name
        private static readonly string SP_LOAD_NAME = "spLnkTestTemplateScreenLoad"; //Load sp name
        private static readonly string SP_EXIST_NAME = "spLnkTestTemplateScreenExist"; //Exist sp name

        //properties
        /// <summary>LnkTestTemplateScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long LnkTestTemplateScreenID
        {
            get { return _lLnkTestTemplateScreenID; }
            set { _lLnkTestTemplateScreenID = value; }
        }
        /// <summary>TestTemplateID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>ScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>DateCreated is a Property in the LnkTestTemplateScreen Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>DateModified is a Property in the LnkTestTemplateScreen Class of type DateTime</summary>
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }
        /// <summary>FlowOrderIndex is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long FlowOrderIndex
        {
            get { return _lFlowOrderIndex; }
            set { _lFlowOrderIndex = value; }
        }
        /// <summary>TransitionDelayInMs is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TransitionDelayInMs
        {
            get { return _lTransitionDelayInMs; }
            set { _lTransitionDelayInMs = value; }
        }
        /// <summary>IsIgnored is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsIgnored
        {
            get { return _bIsIgnored; }
            set { _bIsIgnored = value; }
        }
        /// <summary>IsReported is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsReported
        {
            get { return _bIsReported; }
            set { _bIsReported = value; }
        }
        /// <summary>Notes is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string Notes
        {
            get { return _strNotes; }
            set { _strNotes = value; }
        }
        /// <summary>ImageFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string ImageFileName
        {
            get { return _strImageFileName; }
            set { _strImageFileName = value; }
        }
        /// <summary>HasAudio is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? HasAudio
        {
            get { return _bHasAudio; }
            set { _bHasAudio = value; }
        }
        /// <summary>AudioFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string AudioFileName
        {
            get { return _strAudioFileName; }
            set { _strAudioFileName = value; }
        }


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>HasError Property in class LnkTestTemplateScreen and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class LnkTestTemplateScreen and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }

        //Constructors
        /// <summary>LnkTestTemplateScreen empty constructor</summary>
        public LnkTestTemplateScreen()
        {
        }
        /// <summary>LnkTestTemplateScreen constructor takes a Config</summary>
        public LnkTestTemplateScreen(Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }
        /// <summary>LnkTestTemplateScreen constructor takes LnkTestTemplateScreenID and a SqlConnection</summary>
        public LnkTestTemplateScreen(long l, SqlConnection conn)
        {
            LnkTestTemplateScreenID = l;
            try
            {
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>LnkTestTemplateScreen Constructor takes pStrData and Config</summary>
        public LnkTestTemplateScreen(string pStrData, Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
            Parse(pStrData);
        }
        /// <summary>LnkTestTemplateScreen Constructor takes SqlDataReader</summary>
        public LnkTestTemplateScreen(SqlDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the LnkTestTemplateScreen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + LnkTestTemplateScreenID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
            sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
            }
            sbReturn.Append(TAG_FLOW_ORDER_INDEX + ":  " + FlowOrderIndex + "\n");
            sbReturn.Append(TAG_TRANSITION_DELAY_IN_MS + ":  " + TransitionDelayInMs + "\n");
            sbReturn.Append(TAG_IS_IGNORED + ":  " + IsIgnored + "\n");
            sbReturn.Append(TAG_IS_REPORTED + ":  " + IsReported + "\n");
            sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");
            sbReturn.Append(TAG_IMAGE_FILE_NAME + ":  " + ImageFileName + "\n");
            sbReturn.Append(TAG_HAS_AUDIO + ":  " + HasAudio + "\n");
            sbReturn.Append(TAG_AUDIO_FILE_NAME + ":  " + AudioFileName + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of LnkTestTemplateScreen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<LnkTestTemplateScreen>\n");
            sbReturn.Append("<" + TAG_ID + ">" + LnkTestTemplateScreenID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("<" + TAG_FLOW_ORDER_INDEX + ">" + FlowOrderIndex + "</" + TAG_FLOW_ORDER_INDEX + ">\n");
            sbReturn.Append("<" + TAG_TRANSITION_DELAY_IN_MS + ">" + TransitionDelayInMs + "</" + TAG_TRANSITION_DELAY_IN_MS + ">\n");
            sbReturn.Append("<" + TAG_IS_IGNORED + ">" + IsIgnored + "</" + TAG_IS_IGNORED + ">\n");
            sbReturn.Append("<" + TAG_IS_REPORTED + ">" + IsReported + "</" + TAG_IS_REPORTED + ">\n");
            sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
            sbReturn.Append("<" + TAG_IMAGE_FILE_NAME + ">" + ImageFileName + "</" + TAG_IMAGE_FILE_NAME + ">\n");
            sbReturn.Append("<" + TAG_HAS_AUDIO + ">" + HasAudio + "</" + TAG_HAS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_FILE_NAME + ">" + AudioFileName + "</" + TAG_AUDIO_FILE_NAME + ">\n");
            sbReturn.Append("</LnkTestTemplateScreen>" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                LnkTestTemplateScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
                TestTemplateID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTemplateID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
                ScreenID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
                DateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_FLOW_ORDER_INDEX);
                FlowOrderIndex = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                FlowOrderIndex = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TRANSITION_DELAY_IN_MS);
                TransitionDelayInMs = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TransitionDelayInMs = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_IGNORED);
                IsIgnored = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsIgnored = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_REPORTED);
                IsReported = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsReported = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_NOTES);
                Notes = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IMAGE_FILE_NAME);
                ImageFileName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_HAS_AUDIO);
                HasAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                HasAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AUDIO_FILE_NAME);
                AudioFileName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
        }
        /// <summary>Calls sqlLoad() method which gets record from database with lnk_test_template_screen_id equal to the current object's LnkTestTemplateScreenID </summary>
        public void Load(SqlConnection conn)
        {
            try
            {
                _log("LOAD", ToString());
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlUpdate() method which record record from database with current object values where lnk_test_template_screen_id equal to the current object's LnkTestTemplateScreenID </summary>
        public void Update(SqlConnection conn)
        {
            bool bExist = false;
            try
            {
                _log("UPDATE", ToString());
                bExist = Exist(conn);
                if (bExist)
                {
                    sqlUpdate(conn);
                }
                else
                {
                    _log("NOT_EXIST", ToString());
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
        public void Save(SqlConnection conn)
        {
            try
            {
                bool bExist = false;

                _log("INSERT", ToString());
                bExist = Exist(conn);
                if (!bExist)
                {
                    sqlInsert(conn);
                }
                else
                {
                    _log("ALREADY_EXISTS", ToString());
                    sqlUpdate(conn);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlDelete() method which delete's the record from database where where lnk_test_template_screen_id equal to the current object's LnkTestTemplateScreenID </summary>
        public void Delete(SqlConnection conn)
        {
            try
            {
                _log("DELETE", ToString());
                sqlDelete(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
        public bool Exist(SqlConnection conn)
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return bReturn;
        }
        /// <summary>Prompt user to enter Property values</summary>
        public void Prompt()
        {
            try
            {
                {
                    Console.WriteLine(TAG_ID + ":  ");
                    try
                    {
                        LnkTestTemplateScreenID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        LnkTestTemplateScreenID = 0;
                    }
                }

                Console.WriteLine(LnkTestTemplateScreen.TAG_TEST_TEMPLATE_ID + ":  ");
                TestTemplateID = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_SCREEN_ID + ":  ");
                ScreenID = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(LnkTestTemplateScreen.TAG_DATE_CREATED + ":  ");
                    DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    DateCreated = new DateTime();
                }
                try
                {
                    Console.WriteLine(LnkTestTemplateScreen.TAG_DATE_MODIFIED + ":  ");
                    DateModified = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    DateModified = new DateTime();
                }

                Console.WriteLine(LnkTestTemplateScreen.TAG_FLOW_ORDER_INDEX + ":  ");
                FlowOrderIndex = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_TRANSITION_DELAY_IN_MS + ":  ");
                TransitionDelayInMs = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_IS_IGNORED + ":  ");
                IsIgnored = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_IS_REPORTED + ":  ");
                IsReported = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_NOTES + ":  ");
                Notes = Console.ReadLine();

                Console.WriteLine(LnkTestTemplateScreen.TAG_IMAGE_FILE_NAME + ":  ");
                ImageFileName = Console.ReadLine();

                Console.WriteLine(LnkTestTemplateScreen.TAG_HAS_AUDIO + ":  ");
                HasAudio = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(LnkTestTemplateScreen.TAG_AUDIO_FILE_NAME + ":  ");
                AudioFileName = Console.ReadLine();

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        //protected
        /// <summary>Inserts row of data into the database</summary>
        protected void sqlInsert(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramLnkTestTemplateScreenID = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramDateCreated = null;
            SqlParameter paramFlowOrderIndex = null;
            SqlParameter paramTransitionDelayInMs = null;
            SqlParameter paramIsIgnored = null;
            SqlParameter paramIsReported = null;
            SqlParameter paramNotes = null;
            SqlParameter paramImageFileName = null;
            SqlParameter paramHasAudio = null;
            SqlParameter paramAudioFileName = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_INSERT_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters
            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_ID, LnkTestTemplateScreenID);
            paramLnkTestTemplateScreenID.DbType = DbType.Int32;
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;

            paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;

            paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;

            paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
            paramDateCreated.DbType = DbType.DateTime;
            paramDateCreated.Direction = ParameterDirection.Input;


            paramFlowOrderIndex = new SqlParameter("@" + TAG_FLOW_ORDER_INDEX, FlowOrderIndex);
            paramFlowOrderIndex.DbType = DbType.Int32;
            paramFlowOrderIndex.Direction = ParameterDirection.Input;

            paramTransitionDelayInMs = new SqlParameter("@" + TAG_TRANSITION_DELAY_IN_MS, TransitionDelayInMs);
            paramTransitionDelayInMs.DbType = DbType.Int32;
            paramTransitionDelayInMs.Direction = ParameterDirection.Input;

            paramIsIgnored = new SqlParameter("@" + TAG_IS_IGNORED, IsIgnored);
            paramIsIgnored.DbType = DbType.Boolean;
            paramIsIgnored.Direction = ParameterDirection.Input;

            paramIsReported = new SqlParameter("@" + TAG_IS_REPORTED, IsReported);
            paramIsReported.DbType = DbType.Boolean;
            paramIsReported.Direction = ParameterDirection.Input;

            paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
            paramNotes.DbType = DbType.String;
            paramNotes.Direction = ParameterDirection.Input;

            paramImageFileName = new SqlParameter("@" + TAG_IMAGE_FILE_NAME, ImageFileName);
            paramImageFileName.DbType = DbType.String;
            paramImageFileName.Size = 255;
            paramImageFileName.Direction = ParameterDirection.Input;

            paramHasAudio = new SqlParameter("@" + TAG_HAS_AUDIO, HasAudio);
            paramHasAudio.DbType = DbType.Boolean;
            paramHasAudio.Direction = ParameterDirection.Input;

            paramAudioFileName = new SqlParameter("@" + TAG_AUDIO_FILE_NAME, AudioFileName);
            paramAudioFileName.DbType = DbType.String;
            paramAudioFileName.Size = 255;
            paramAudioFileName.Direction = ParameterDirection.Input;

            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramLnkTestTemplateScreenID);
            cmd.Parameters.Add(paramTestTemplateID);
            cmd.Parameters.Add(paramScreenID);
            cmd.Parameters.Add(paramDateCreated);
            cmd.Parameters.Add(paramFlowOrderIndex);
            cmd.Parameters.Add(paramTransitionDelayInMs);
            cmd.Parameters.Add(paramIsIgnored);
            cmd.Parameters.Add(paramIsReported);
            cmd.Parameters.Add(paramNotes);
            cmd.Parameters.Add(paramImageFileName);
            cmd.Parameters.Add(paramHasAudio);
            cmd.Parameters.Add(paramAudioFileName);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            // assign the primary kiey
            string strTmp;
            strTmp = cmd.Parameters["@PKID"].Value.ToString();
            LnkTestTemplateScreenID = long.Parse(strTmp);

            // cleanup to help GC
            paramLnkTestTemplateScreenID = null;
            paramTestTemplateID = null;
            paramScreenID = null;
            paramDateCreated = null;
            paramFlowOrderIndex = null;
            paramTransitionDelayInMs = null;
            paramIsIgnored = null;
            paramIsReported = null;
            paramNotes = null;
            paramImageFileName = null;
            paramHasAudio = null;
            paramAudioFileName = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Check to see if the row exists in database</summary>
        protected bool sqlExist(SqlConnection conn)
        {
            bool bExist = false;

            SqlCommand cmd = null;
            SqlParameter paramLnkTestTemplateScreenID = null;
            SqlParameter paramCount = null;

            cmd = new SqlCommand(SP_EXIST_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_ID, LnkTestTemplateScreenID);
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;
            paramLnkTestTemplateScreenID.DbType = DbType.Int32;

            paramCount = new SqlParameter();
            paramCount.ParameterName = "@COUNT";
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramLnkTestTemplateScreenID);
            cmd.Parameters.Add(paramCount);
            cmd.ExecuteNonQuery();

            string strTmp;
            int nCount = 0;
            strTmp = cmd.Parameters["@COUNT"].Value.ToString();
            nCount = int.Parse(strTmp);
            if (nCount > 0)
            {
                bExist = true;
            }

            // cleanup
            paramLnkTestTemplateScreenID = null;
            paramCount = null;
            cmd = null;

            return bExist;
        }
        /// <summary>Updates row of data in database</summary>
        protected void sqlUpdate(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramLnkTestTemplateScreenID = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramScreenID = null;
            SqlParameter paramDateModified = null;
            SqlParameter paramFlowOrderIndex = null;
            SqlParameter paramTransitionDelayInMs = null;
            SqlParameter paramIsIgnored = null;
            SqlParameter paramIsReported = null;
            SqlParameter paramNotes = null;
            SqlParameter paramImageFileName = null;
            SqlParameter paramHasAudio = null;
            SqlParameter paramAudioFileName = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_UPDATE_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters

            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_ID, LnkTestTemplateScreenID);
            paramLnkTestTemplateScreenID.DbType = DbType.Int32;
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;


            paramTestTemplateID = new SqlParameter("@" + TAG_TEST_TEMPLATE_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;

            paramScreenID = new SqlParameter("@" + TAG_SCREEN_ID, ScreenID);
            paramScreenID.DbType = DbType.Int32;
            paramScreenID.Direction = ParameterDirection.Input;


            paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
            paramDateModified.DbType = DbType.DateTime;
            paramDateModified.Direction = ParameterDirection.Input;

            paramFlowOrderIndex = new SqlParameter("@" + TAG_FLOW_ORDER_INDEX, FlowOrderIndex);
            paramFlowOrderIndex.DbType = DbType.Int32;
            paramFlowOrderIndex.Direction = ParameterDirection.Input;

            paramTransitionDelayInMs = new SqlParameter("@" + TAG_TRANSITION_DELAY_IN_MS, TransitionDelayInMs);
            paramTransitionDelayInMs.DbType = DbType.Int32;
            paramTransitionDelayInMs.Direction = ParameterDirection.Input;

            paramIsIgnored = new SqlParameter("@" + TAG_IS_IGNORED, IsIgnored);
            paramIsIgnored.DbType = DbType.Boolean;
            paramIsIgnored.Direction = ParameterDirection.Input;

            paramIsReported = new SqlParameter("@" + TAG_IS_REPORTED, IsReported);
            paramIsReported.DbType = DbType.Boolean;
            paramIsReported.Direction = ParameterDirection.Input;

            paramNotes = new SqlParameter("@" + TAG_NOTES, Notes);
            paramNotes.DbType = DbType.String;
            paramNotes.Direction = ParameterDirection.Input;

            paramImageFileName = new SqlParameter("@" + TAG_IMAGE_FILE_NAME, ImageFileName);
            paramImageFileName.DbType = DbType.String;
            paramImageFileName.Size = 255;
            paramImageFileName.Direction = ParameterDirection.Input;

            paramHasAudio = new SqlParameter("@" + TAG_HAS_AUDIO, HasAudio);
            paramHasAudio.DbType = DbType.Boolean;
            paramHasAudio.Direction = ParameterDirection.Input;

            paramAudioFileName = new SqlParameter("@" + TAG_AUDIO_FILE_NAME, AudioFileName);
            paramAudioFileName.DbType = DbType.String;
            paramAudioFileName.Size = 255;
            paramAudioFileName.Direction = ParameterDirection.Input;

            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramLnkTestTemplateScreenID);
            cmd.Parameters.Add(paramTestTemplateID);
            cmd.Parameters.Add(paramScreenID);
            cmd.Parameters.Add(paramDateModified);
            cmd.Parameters.Add(paramFlowOrderIndex);
            cmd.Parameters.Add(paramTransitionDelayInMs);
            cmd.Parameters.Add(paramIsIgnored);
            cmd.Parameters.Add(paramIsReported);
            cmd.Parameters.Add(paramNotes);
            cmd.Parameters.Add(paramImageFileName);
            cmd.Parameters.Add(paramHasAudio);
            cmd.Parameters.Add(paramAudioFileName);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            string s;
            s = cmd.Parameters["@PKID"].Value.ToString();
            LnkTestTemplateScreenID = long.Parse(s);

            // cleanup
            paramLnkTestTemplateScreenID = null;
            paramTestTemplateID = null;
            paramScreenID = null;
            paramDateModified = null;
            paramFlowOrderIndex = null;
            paramTransitionDelayInMs = null;
            paramIsIgnored = null;
            paramIsReported = null;
            paramNotes = null;
            paramImageFileName = null;
            paramHasAudio = null;
            paramAudioFileName = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Deletes row of data in database</summary>
        protected void sqlDelete(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramLnkTestTemplateScreenID = null;

            cmd = new SqlCommand(SP_DELETE_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_ID, LnkTestTemplateScreenID);
            paramLnkTestTemplateScreenID.DbType = DbType.Int32;
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramLnkTestTemplateScreenID);
            cmd.ExecuteNonQuery();

            // cleanup to help GC
            paramLnkTestTemplateScreenID = null;
            cmd = null;

        }
        /// <summary>Load row of data from database</summary>
        protected void sqlLoad(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramLnkTestTemplateScreenID = null;
            SqlDataReader rdr = null;

            cmd = new SqlCommand(SP_LOAD_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramLnkTestTemplateScreenID = new SqlParameter("@" + TAG_ID, LnkTestTemplateScreenID);
            paramLnkTestTemplateScreenID.DbType = DbType.Int32;
            paramLnkTestTemplateScreenID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramLnkTestTemplateScreenID);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                sqlParseResultSet(rdr);
            }
            // cleanup
            rdr.Close();
            rdr = null;
            paramLnkTestTemplateScreenID = null;
            cmd = null;
        }
        /// <summary>Parse result set</summary>
        protected void sqlParseResultSet(SqlDataReader rdr)
        {
            this.LnkTestTemplateScreenID = long.Parse(rdr[DB_FIELD_ID].ToString());
            try
            {
                this.TestTemplateID = Convert.ToInt32(rdr[DB_FIELD_TEST_TEMPLATE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.ScreenID = Convert.ToInt32(rdr[DB_FIELD_SCREEN_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
            }
            catch
            {
            }
            try
            {
                this.FlowOrderIndex = Convert.ToInt32(rdr[DB_FIELD_FLOW_ORDER_INDEX].ToString().Trim());
            }
            catch { }
            try
            {
                this.TransitionDelayInMs = Convert.ToInt32(rdr[DB_FIELD_TRANSITION_DELAY_IN_MS].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsIgnored = Convert.ToBoolean(rdr[DB_FIELD_IS_IGNORED].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsReported = Convert.ToBoolean(rdr[DB_FIELD_IS_REPORTED].ToString().Trim());
            }
            catch { }
            this.Notes = rdr[DB_FIELD_NOTES].ToString().Trim();
            this.ImageFileName = rdr[DB_FIELD_IMAGE_FILE_NAME].ToString().Trim();
            try
            {
                this.HasAudio = Convert.ToBoolean(rdr[DB_FIELD_HAS_AUDIO].ToString().Trim());
            }
            catch { }
            this.AudioFileName = rdr[DB_FIELD_AUDIO_FILE_NAME].ToString().Trim();
        }

        //private
        /// <summary>Log errors</summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test(SqlConnection conn)
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Delete.");
                Console.WriteLine("3.  Update.");
                Console.WriteLine("4.  Exist.");
                Console.WriteLine("5.  Load.");
                Console.WriteLine("6.  ToXml.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // insert
                            Console.WriteLine("Save:  ");
                            Prompt();
                            Save(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("Delete " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            LnkTestTemplateScreenID = long.Parse(strAns);
                            Delete(conn);
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 3:
                            Console.WriteLine("Update:  ");
                            Prompt();
                            Update(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 4:
                            Console.WriteLine("Exist " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            LnkTestTemplateScreenID = long.Parse(strAns);
                            bool bExist = false;
                            bExist = Exist(conn);
                            Console.WriteLine("Record id " + LnkTestTemplateScreenID + " exist:  " + bExist.ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 5:
                            Console.WriteLine("Load " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            LnkTestTemplateScreenID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 6:
                            Console.WriteLine("ToXml " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            LnkTestTemplateScreenID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToXml());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }

        }
    }
}

//END OF LnkTestTemplateScreen CLASS FILE