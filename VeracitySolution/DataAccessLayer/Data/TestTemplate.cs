using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  TestTemplate.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH	7/7/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the TestTemplate database table.
    /// </summary>
    public class TestTemplate
    {
        //Attributes
        /// <summary>TestTemplateID Attribute type String</summary>
        private long _lTestTemplateID = 0;
        /// <summary>TestTypeID Attribute type String</summary>
        private long _lTestTypeID = 0;
        /// <summary>DoPrintReport Attribute type String</summary>
        private bool? _bDoPrintReport = null;
        /// <summary>DoLicenseIdentification Attribute type String</summary>
        private bool? _bDoLicenseIdentification = null;
        /// <summary>DoNameIdentification Attribute type String</summary>
        private bool? _bDoNameIdentification = null;
        /// <summary>PrintTemplateName Attribute type String</summary>
        private string _strPrintTemplateName = null;
        /// <summary>IsKioskMode Attribute type String</summary>
        private bool? _bIsKioskMode = null;
        /// <summary>IsSynchronized Attribute type String</summary>
        private bool? _bIsSynchronized = null;
        /// <summary>AdminPassword Attribute type String</summary>
        private string _strAdminPassword = null;
        /// <summary>ManagerPassword Attribute type String</summary>
        private string _strManagerPassword = null;
        /// <summary>ResetInSeconds Attribute type String</summary>
        private long _lResetInSeconds = 0;
        /// <summary>DateCreated Attribute type String</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>DateModified Attribute type String</summary>
        private DateTime _dtDateModified = dtNull;

        private Config _config = null;
        private ErrorCode _errorCode = null;
        private Logger _oLog = null;
        private string _strLognameText = "DataAccessLayer-Data-TestTemplate";
        private bool _hasError = false;
        private static DateTime dtNull = new DateTime();

        /// <summary>HasError Property in class TestTemplate and is of type bool</summary>
        public static readonly string ENTITY_NAME = "TestTemplate"; //Table name to abstract

        // DB Field names
        /// <summary>ID Database field</summary>
        public static readonly string DB_FIELD_ID = "test_template_id"; //Table id field name
        /// <summary>test_type_id Database field </summary>
        public static readonly string DB_FIELD_TEST_TYPE_ID = "test_type_id"; //Table TestTypeID field name
        /// <summary>do_print_report Database field </summary>
        public static readonly string DB_FIELD_DO_PRINT_REPORT = "do_print_report"; //Table DoPrintReport field name
        /// <summary>do_license_identification Database field </summary>
        public static readonly string DB_FIELD_DO_LICENSE_IDENTIFICATION = "do_license_identification"; //Table DoLicenseIdentification field name
        /// <summary>do_name_identification Database field </summary>
        public static readonly string DB_FIELD_DO_NAME_IDENTIFICATION = "do_name_identification"; //Table DoNameIdentification field name
        /// <summary>print_template_name Database field </summary>
        public static readonly string DB_FIELD_PRINT_TEMPLATE_NAME = "print_template_name"; //Table PrintTemplateName field name
        /// <summary>is_kiosk_mode Database field </summary>
        public static readonly string DB_FIELD_IS_KIOSK_MODE = "is_kiosk_mode"; //Table IsKioskMode field name
        /// <summary>is_synchronized Database field </summary>
        public static readonly string DB_FIELD_IS_SYNCHRONIZED = "is_synchronized"; //Table IsSynchronized field name
        /// <summary>admin_password Database field </summary>
        public static readonly string DB_FIELD_ADMIN_PASSWORD = "admin_password"; //Table AdminPassword field name
        /// <summary>manager_password Database field </summary>
        public static readonly string DB_FIELD_MANAGER_PASSWORD = "manager_password"; //Table ManagerPassword field name
        /// <summary>reset_in_seconds Database field </summary>
        public static readonly string DB_FIELD_RESET_IN_SECONDS = "reset_in_seconds"; //Table ResetInSeconds field name
        /// <summary>date_created Database field </summary>
        public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
        /// <summary>date_modified Database field </summary>
        public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name

        // Attribute variables
        /// <summary>TAG_ID Attribute type string</summary>
        public static readonly string TAG_ID = "TestTemplateID"; //Attribute id  name
        /// <summary>TestTypeID Attribute type string</summary>
        public static readonly string TAG_TEST_TYPE_ID = "TestTypeID"; //Table TestTypeID field name
        /// <summary>DoPrintReport Attribute type string</summary>
        public static readonly string TAG_DO_PRINT_REPORT = "DoPrintReport"; //Table DoPrintReport field name
        /// <summary>DoLicenseIdentification Attribute type string</summary>
        public static readonly string TAG_DO_LICENSE_IDENTIFICATION = "DoLicenseIdentification"; //Table DoLicenseIdentification field name
        /// <summary>DoNameIdentification Attribute type string</summary>
        public static readonly string TAG_DO_NAME_IDENTIFICATION = "DoNameIdentification"; //Table DoNameIdentification field name
        /// <summary>PrintTemplateName Attribute type string</summary>
        public static readonly string TAG_PRINT_TEMPLATE_NAME = "PrintTemplateName"; //Table PrintTemplateName field name
        /// <summary>IsKioskMode Attribute type string</summary>
        public static readonly string TAG_IS_KIOSK_MODE = "IsKioskMode"; //Table IsKioskMode field name
        /// <summary>IsSynchronized Attribute type string</summary>
        public static readonly string TAG_IS_SYNCHRONIZED = "IsSynchronized"; //Table IsSynchronized field name
        /// <summary>AdminPassword Attribute type string</summary>
        public static readonly string TAG_ADMIN_PASSWORD = "AdminPassword"; //Table AdminPassword field name
        /// <summary>ManagerPassword Attribute type string</summary>
        public static readonly string TAG_MANAGER_PASSWORD = "ManagerPassword"; //Table ManagerPassword field name
        /// <summary>ResetInSeconds Attribute type string</summary>
        public static readonly string TAG_RESET_IN_SECONDS = "ResetInSeconds"; //Table ResetInSeconds field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name

        // Stored procedure names
        private static readonly string SP_INSERT_NAME = "spTestTemplateInsert"; //Insert sp name
        private static readonly string SP_UPDATE_NAME = "spTestTemplateUpdate"; //Update sp name
        private static readonly string SP_DELETE_NAME = "spTestTemplateDelete"; //Delete sp name
        private static readonly string SP_LOAD_NAME = "spTestTemplateLoad"; //Load sp name
        private static readonly string SP_EXIST_NAME = "spTestTemplateExist"; //Exist sp name

        //properties
        /// <summary>TestTemplateID is a Property in the TestTemplate Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>TestTypeID is a Property in the TestTemplate Class of type long</summary>
        public long TestTypeID
        {
            get { return _lTestTypeID; }
            set { _lTestTypeID = value; }
        }
        /// <summary>DoPrintReport is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoPrintReport
        {
            get { return _bDoPrintReport; }
            set { _bDoPrintReport = value; }
        }
        /// <summary>DoLicenseIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoLicenseIdentification
        {
            get { return _bDoLicenseIdentification; }
            set { _bDoLicenseIdentification = value; }
        }
        /// <summary>DoNameIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoNameIdentification
        {
            get { return _bDoNameIdentification; }
            set { _bDoNameIdentification = value; }
        }
        /// <summary>PrintTemplateName is a Property in the TestTemplate Class of type String</summary>
        public string PrintTemplateName
        {
            get { return _strPrintTemplateName; }
            set { _strPrintTemplateName = value; }
        }
        /// <summary>IsKioskMode is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsKioskMode
        {
            get { return _bIsKioskMode; }
            set { _bIsKioskMode = value; }
        }
        /// <summary>IsSynchronized is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsSynchronized
        {
            get { return _bIsSynchronized; }
            set { _bIsSynchronized = value; }
        }
        /// <summary>AdminPassword is a Property in the TestTemplate Class of type String</summary>
        public string AdminPassword
        {
            get { return _strAdminPassword; }
            set { _strAdminPassword = value; }
        }
        /// <summary>ManagerPassword is a Property in the TestTemplate Class of type String</summary>
        public string ManagerPassword
        {
            get { return _strManagerPassword; }
            set { _strManagerPassword = value; }
        }
        /// <summary>ResetInSeconds is a Property in the TestTemplate Class of type long</summary>
        public long ResetInSeconds
        {
            get { return _lResetInSeconds; }
            set { _lResetInSeconds = value; }
        }
        /// <summary>DateCreated is a Property in the TestTemplate Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>DateModified is a Property in the TestTemplate Class of type DateTime</summary>
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        /// <summary>HasError Property in class TestTemplate and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class TestTemplate and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }

        //Constructors
        /// <summary>TestTemplate empty constructor</summary>
        public TestTemplate()
        {
        }
        /// <summary>TestTemplate constructor takes a Config</summary>
        public TestTemplate(Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
        }
        /// <summary>TestTemplate constructor takes TestTemplateID and a SqlConnection</summary>
        public TestTemplate(long l, SqlConnection conn)
        {
            TestTemplateID = l;
            try
            {
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>TestTemplate Constructor takes pStrData and Config</summary>
        public TestTemplate(string pStrData, Config pConfig)
        {
            _config = pConfig;
            _oLog = new Logger(_strLognameText);
            Parse(pStrData);
        }
        /// <summary>TestTemplate Constructor takes SqlDataReader</summary>
        public TestTemplate(SqlDataReader rd)
        {
            sqlParseResultSet(rd);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the TestTemplate Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + TestTemplateID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TYPE_ID + ":  " + TestTypeID + "\n");
            sbReturn.Append(TAG_DO_PRINT_REPORT + ":  " + DoPrintReport + "\n");
            sbReturn.Append(TAG_DO_LICENSE_IDENTIFICATION + ":  " + DoLicenseIdentification + "\n");
            sbReturn.Append(TAG_DO_NAME_IDENTIFICATION + ":  " + DoNameIdentification + "\n");
            sbReturn.Append(TAG_PRINT_TEMPLATE_NAME + ":  " + PrintTemplateName + "\n");
            sbReturn.Append(TAG_IS_KIOSK_MODE + ":  " + IsKioskMode + "\n");
            sbReturn.Append(TAG_IS_SYNCHRONIZED + ":  " + IsSynchronized + "\n");
            sbReturn.Append(TAG_ADMIN_PASSWORD + ":  " + AdminPassword + "\n");
            sbReturn.Append(TAG_MANAGER_PASSWORD + ":  " + ManagerPassword + "\n");
            sbReturn.Append(TAG_RESET_IN_SECONDS + ":  " + ResetInSeconds + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
            }

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of TestTemplate</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<TestTemplate>\n");
            sbReturn.Append("<" + TAG_ID + ">" + TestTemplateID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TYPE_ID + ">" + TestTypeID + "</" + TAG_TEST_TYPE_ID + ">\n");
            sbReturn.Append("<" + TAG_DO_PRINT_REPORT + ">" + DoPrintReport + "</" + TAG_DO_PRINT_REPORT + ">\n");
            sbReturn.Append("<" + TAG_DO_LICENSE_IDENTIFICATION + ">" + DoLicenseIdentification + "</" + TAG_DO_LICENSE_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_DO_NAME_IDENTIFICATION + ">" + DoNameIdentification + "</" + TAG_DO_NAME_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_PRINT_TEMPLATE_NAME + ">" + PrintTemplateName + "</" + TAG_PRINT_TEMPLATE_NAME + ">\n");
            sbReturn.Append("<" + TAG_IS_KIOSK_MODE + ">" + IsKioskMode + "</" + TAG_IS_KIOSK_MODE + ">\n");
            sbReturn.Append("<" + TAG_IS_SYNCHRONIZED + ">" + IsSynchronized + "</" + TAG_IS_SYNCHRONIZED + ">\n");
            sbReturn.Append("<" + TAG_ADMIN_PASSWORD + ">" + AdminPassword + "</" + TAG_ADMIN_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_MANAGER_PASSWORD + ">" + ManagerPassword + "</" + TAG_MANAGER_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_RESET_IN_SECONDS + ">" + ResetInSeconds + "</" + TAG_RESET_IN_SECONDS + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("</TestTemplate>" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                TestTemplateID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TYPE_ID);
                TestTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_PRINT_REPORT);
                DoPrintReport = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoPrintReport = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_LICENSE_IDENTIFICATION);
                DoLicenseIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoLicenseIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_NAME_IDENTIFICATION);
                DoNameIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoNameIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_TEMPLATE_NAME);
                PrintTemplateName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_KIOSK_MODE);
                IsKioskMode = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsKioskMode = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_SYNCHRONIZED);
                IsSynchronized = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsSynchronized = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ADMIN_PASSWORD);
                AdminPassword = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_MANAGER_PASSWORD);
                ManagerPassword = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_RESET_IN_SECONDS);
                ResetInSeconds = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ResetInSeconds = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
                DateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }
        }
        /// <summary>Calls sqlLoad() method which gets record from database with test_template_id equal to the current object's TestTemplateID </summary>
        public void Load(SqlConnection conn)
        {
            try
            {
                _log("LOAD", ToString());
                sqlLoad(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlUpdate() method which record record from database with current object values where test_template_id equal to the current object's TestTemplateID </summary>
        public void Update(SqlConnection conn)
        {
            bool bExist = false;
            try
            {
                _log("UPDATE", ToString());
                bExist = Exist(conn);
                if (bExist)
                {
                    sqlUpdate(conn);
                }
                else
                {
                    _log("NOT_EXIST", ToString());
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
        public void Save(SqlConnection conn)
        {
            try
            {
                bool bExist = false;

                _log("INSERT", ToString());
                bExist = Exist(conn);
                if (!bExist)
                {
                    sqlInsert(conn);
                }
                else
                {
                    _log("ALREADY_EXISTS", ToString());
                    sqlUpdate(conn);
                }
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

        }
        /// <summary>Calls sqlDelete() method which delete's the record from database where where test_template_id equal to the current object's TestTemplateID </summary>
        public void Delete(SqlConnection conn)
        {
            try
            {
                _log("DELETE", ToString());
                sqlDelete(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
        public bool Exist(SqlConnection conn)
        {
            bool bReturn = false;
            try
            {
                bReturn = sqlExist(conn);
            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }

            return bReturn;
        }
        /// <summary>Prompt user to enter Property values</summary>
        public void Prompt()
        {
            try
            {
                {
                    Console.WriteLine(TAG_ID + ":  ");
                    try
                    {
                        TestTemplateID = long.Parse(Console.ReadLine());
                    }
                    catch
                    {
                        TestTemplateID = 0;
                    }
                }

                Console.WriteLine(TestTemplate.TAG_TEST_TYPE_ID + ":  ");
                TestTypeID = (long)Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_PRINT_REPORT + ":  ");
                DoPrintReport = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_LICENSE_IDENTIFICATION + ":  ");
                DoLicenseIdentification = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_DO_NAME_IDENTIFICATION + ":  ");
                DoNameIdentification = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_PRINT_TEMPLATE_NAME + ":  ");
                PrintTemplateName = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_IS_KIOSK_MODE + ":  ");
                IsKioskMode = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_IS_SYNCHRONIZED + ":  ");
                IsSynchronized = Convert.ToBoolean(Console.ReadLine());

                Console.WriteLine(TestTemplate.TAG_ADMIN_PASSWORD + ":  ");
                AdminPassword = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_MANAGER_PASSWORD + ":  ");
                ManagerPassword = Console.ReadLine();

                Console.WriteLine(TestTemplate.TAG_RESET_IN_SECONDS + ":  ");
                ResetInSeconds = (long)Convert.ToInt32(Console.ReadLine());
                try
                {
                    Console.WriteLine(TestTemplate.TAG_DATE_CREATED + ":  ");
                    DateCreated = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    DateCreated = new DateTime();
                }
                try
                {
                    Console.WriteLine(TestTemplate.TAG_DATE_MODIFIED + ":  ");
                    DateModified = DateTime.Parse(Console.ReadLine());
                }
                catch
                {
                    DateModified = new DateTime();
                }

            }
            catch (Exception e)
            {
                _log("ERROR", e.ToString() + e.StackTrace.ToString());
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }

        //protected
        /// <summary>Inserts row of data into the database</summary>
        protected void sqlInsert(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramTestTypeID = null;
            SqlParameter paramDoPrintReport = null;
            SqlParameter paramDoLicenseIdentification = null;
            SqlParameter paramDoNameIdentification = null;
            SqlParameter paramPrintTemplateName = null;
            SqlParameter paramIsKioskMode = null;
            SqlParameter paramIsSynchronized = null;
            SqlParameter paramAdminPassword = null;
            SqlParameter paramManagerPassword = null;
            SqlParameter paramResetInSeconds = null;
            SqlParameter paramDateCreated = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_INSERT_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters
            paramTestTemplateID = new SqlParameter("@" + TAG_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;

            paramTestTypeID = new SqlParameter("@" + TAG_TEST_TYPE_ID, TestTypeID);
            paramTestTypeID.DbType = DbType.Int32;
            paramTestTypeID.Direction = ParameterDirection.Input;

            paramDoPrintReport = new SqlParameter("@" + TAG_DO_PRINT_REPORT, DoPrintReport);
            paramDoPrintReport.DbType = DbType.Boolean;
            paramDoPrintReport.Direction = ParameterDirection.Input;

            paramDoLicenseIdentification = new SqlParameter("@" + TAG_DO_LICENSE_IDENTIFICATION, DoLicenseIdentification);
            paramDoLicenseIdentification.DbType = DbType.Boolean;
            paramDoLicenseIdentification.Direction = ParameterDirection.Input;

            paramDoNameIdentification = new SqlParameter("@" + TAG_DO_NAME_IDENTIFICATION, DoNameIdentification);
            paramDoNameIdentification.DbType = DbType.Boolean;
            paramDoNameIdentification.Direction = ParameterDirection.Input;

            paramPrintTemplateName = new SqlParameter("@" + TAG_PRINT_TEMPLATE_NAME, PrintTemplateName);
            paramPrintTemplateName.DbType = DbType.String;
            paramPrintTemplateName.Size = 255;
            paramPrintTemplateName.Direction = ParameterDirection.Input;

            paramIsKioskMode = new SqlParameter("@" + TAG_IS_KIOSK_MODE, IsKioskMode);
            paramIsKioskMode.DbType = DbType.Boolean;
            paramIsKioskMode.Direction = ParameterDirection.Input;

            paramIsSynchronized = new SqlParameter("@" + TAG_IS_SYNCHRONIZED, IsSynchronized);
            paramIsSynchronized.DbType = DbType.Boolean;
            paramIsSynchronized.Direction = ParameterDirection.Input;

            paramAdminPassword = new SqlParameter("@" + TAG_ADMIN_PASSWORD, AdminPassword);
            paramAdminPassword.DbType = DbType.String;
            paramAdminPassword.Size = 255;
            paramAdminPassword.Direction = ParameterDirection.Input;

            paramManagerPassword = new SqlParameter("@" + TAG_MANAGER_PASSWORD, ManagerPassword);
            paramManagerPassword.DbType = DbType.String;
            paramManagerPassword.Size = 255;
            paramManagerPassword.Direction = ParameterDirection.Input;

            paramResetInSeconds = new SqlParameter("@" + TAG_RESET_IN_SECONDS, ResetInSeconds);
            paramResetInSeconds.DbType = DbType.Int32;
            paramResetInSeconds.Direction = ParameterDirection.Input;

            paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
            paramDateCreated.DbType = DbType.DateTime;
            paramDateCreated.Direction = ParameterDirection.Input;


            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramTestTemplateID);
            cmd.Parameters.Add(paramTestTypeID);
            cmd.Parameters.Add(paramDoPrintReport);
            cmd.Parameters.Add(paramDoLicenseIdentification);
            cmd.Parameters.Add(paramDoNameIdentification);
            cmd.Parameters.Add(paramPrintTemplateName);
            cmd.Parameters.Add(paramIsKioskMode);
            cmd.Parameters.Add(paramIsSynchronized);
            cmd.Parameters.Add(paramAdminPassword);
            cmd.Parameters.Add(paramManagerPassword);
            cmd.Parameters.Add(paramResetInSeconds);
            cmd.Parameters.Add(paramDateCreated);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            // assign the primary kiey
            string strTmp;
            strTmp = cmd.Parameters["@PKID"].Value.ToString();
            TestTemplateID = long.Parse(strTmp);

            // cleanup to help GC
            paramTestTemplateID = null;
            paramTestTypeID = null;
            paramDoPrintReport = null;
            paramDoLicenseIdentification = null;
            paramDoNameIdentification = null;
            paramPrintTemplateName = null;
            paramIsKioskMode = null;
            paramIsSynchronized = null;
            paramAdminPassword = null;
            paramManagerPassword = null;
            paramResetInSeconds = null;
            paramDateCreated = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Check to see if the row exists in database</summary>
        protected bool sqlExist(SqlConnection conn)
        {
            bool bExist = false;

            SqlCommand cmd = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramCount = null;

            cmd = new SqlCommand(SP_EXIST_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;

            paramTestTemplateID = new SqlParameter("@" + TAG_ID, TestTemplateID);
            paramTestTemplateID.Direction = ParameterDirection.Input;
            paramTestTemplateID.DbType = DbType.Int32;

            paramCount = new SqlParameter();
            paramCount.ParameterName = "@COUNT";
            paramCount.DbType = DbType.Int32;
            paramCount.Direction = ParameterDirection.Output;

            cmd.Parameters.Add(paramTestTemplateID);
            cmd.Parameters.Add(paramCount);
            cmd.ExecuteNonQuery();

            string strTmp;
            int nCount = 0;
            strTmp = cmd.Parameters["@COUNT"].Value.ToString();
            nCount = int.Parse(strTmp);
            if (nCount > 0)
            {
                bExist = true;
            }

            // cleanup
            paramTestTemplateID = null;
            paramCount = null;
            cmd = null;

            return bExist;
        }
        /// <summary>Updates row of data in database</summary>
        protected void sqlUpdate(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramTestTemplateID = null;
            SqlParameter paramTestTypeID = null;
            SqlParameter paramDoPrintReport = null;
            SqlParameter paramDoLicenseIdentification = null;
            SqlParameter paramDoNameIdentification = null;
            SqlParameter paramPrintTemplateName = null;
            SqlParameter paramIsKioskMode = null;
            SqlParameter paramIsSynchronized = null;
            SqlParameter paramAdminPassword = null;
            SqlParameter paramManagerPassword = null;
            SqlParameter paramResetInSeconds = null;
            SqlParameter paramDateModified = null;
            SqlParameter paramPKID = null;

            //Create a command object identifying
            //the stored procedure	
            cmd = new SqlCommand(SP_UPDATE_NAME, conn);

            //Set the command object so it knows
            //to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // parameters

            paramTestTemplateID = new SqlParameter("@" + TAG_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;


            paramTestTypeID = new SqlParameter("@" + TAG_TEST_TYPE_ID, TestTypeID);
            paramTestTypeID.DbType = DbType.Int32;
            paramTestTypeID.Direction = ParameterDirection.Input;

            paramDoPrintReport = new SqlParameter("@" + TAG_DO_PRINT_REPORT, DoPrintReport);
            paramDoPrintReport.DbType = DbType.Boolean;
            paramDoPrintReport.Direction = ParameterDirection.Input;

            paramDoLicenseIdentification = new SqlParameter("@" + TAG_DO_LICENSE_IDENTIFICATION, DoLicenseIdentification);
            paramDoLicenseIdentification.DbType = DbType.Boolean;
            paramDoLicenseIdentification.Direction = ParameterDirection.Input;

            paramDoNameIdentification = new SqlParameter("@" + TAG_DO_NAME_IDENTIFICATION, DoNameIdentification);
            paramDoNameIdentification.DbType = DbType.Boolean;
            paramDoNameIdentification.Direction = ParameterDirection.Input;

            paramPrintTemplateName = new SqlParameter("@" + TAG_PRINT_TEMPLATE_NAME, PrintTemplateName);
            paramPrintTemplateName.DbType = DbType.String;
            paramPrintTemplateName.Size = 255;
            paramPrintTemplateName.Direction = ParameterDirection.Input;

            paramIsKioskMode = new SqlParameter("@" + TAG_IS_KIOSK_MODE, IsKioskMode);
            paramIsKioskMode.DbType = DbType.Boolean;
            paramIsKioskMode.Direction = ParameterDirection.Input;

            paramIsSynchronized = new SqlParameter("@" + TAG_IS_SYNCHRONIZED, IsSynchronized);
            paramIsSynchronized.DbType = DbType.Boolean;
            paramIsSynchronized.Direction = ParameterDirection.Input;

            paramAdminPassword = new SqlParameter("@" + TAG_ADMIN_PASSWORD, AdminPassword);
            paramAdminPassword.DbType = DbType.String;
            paramAdminPassword.Size = 255;
            paramAdminPassword.Direction = ParameterDirection.Input;

            paramManagerPassword = new SqlParameter("@" + TAG_MANAGER_PASSWORD, ManagerPassword);
            paramManagerPassword.DbType = DbType.String;
            paramManagerPassword.Size = 255;
            paramManagerPassword.Direction = ParameterDirection.Input;

            paramResetInSeconds = new SqlParameter("@" + TAG_RESET_IN_SECONDS, ResetInSeconds);
            paramResetInSeconds.DbType = DbType.Int32;
            paramResetInSeconds.Direction = ParameterDirection.Input;


            paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
            paramDateModified.DbType = DbType.DateTime;
            paramDateModified.Direction = ParameterDirection.Input;

            paramPKID = new SqlParameter();
            paramPKID.ParameterName = "@PKID";
            paramPKID.DbType = DbType.Int32;
            paramPKID.Direction = ParameterDirection.Output;

            //Add parameters to command, which
            //will be passed to the stored procedure
            cmd.Parameters.Add(paramTestTemplateID);
            cmd.Parameters.Add(paramTestTypeID);
            cmd.Parameters.Add(paramDoPrintReport);
            cmd.Parameters.Add(paramDoLicenseIdentification);
            cmd.Parameters.Add(paramDoNameIdentification);
            cmd.Parameters.Add(paramPrintTemplateName);
            cmd.Parameters.Add(paramIsKioskMode);
            cmd.Parameters.Add(paramIsSynchronized);
            cmd.Parameters.Add(paramAdminPassword);
            cmd.Parameters.Add(paramManagerPassword);
            cmd.Parameters.Add(paramResetInSeconds);
            cmd.Parameters.Add(paramDateModified);
            cmd.Parameters.Add(paramPKID);

            // execute the command
            cmd.ExecuteNonQuery();
            string s;
            s = cmd.Parameters["@PKID"].Value.ToString();
            TestTemplateID = long.Parse(s);

            // cleanup
            paramTestTemplateID = null;
            paramTestTypeID = null;
            paramDoPrintReport = null;
            paramDoLicenseIdentification = null;
            paramDoNameIdentification = null;
            paramPrintTemplateName = null;
            paramIsKioskMode = null;
            paramIsSynchronized = null;
            paramAdminPassword = null;
            paramManagerPassword = null;
            paramResetInSeconds = null;
            paramDateModified = null;
            paramPKID = null;
            cmd = null;
        }
        /// <summary>Deletes row of data in database</summary>
        protected void sqlDelete(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramTestTemplateID = null;

            cmd = new SqlCommand(SP_DELETE_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramTestTemplateID = new SqlParameter("@" + TAG_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramTestTemplateID);
            cmd.ExecuteNonQuery();

            // cleanup to help GC
            paramTestTemplateID = null;
            cmd = null;

        }
        /// <summary>Load row of data from database</summary>
        protected void sqlLoad(SqlConnection conn)
        {
            SqlCommand cmd = null;
            SqlParameter paramTestTemplateID = null;
            SqlDataReader rdr = null;

            cmd = new SqlCommand(SP_LOAD_NAME, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            paramTestTemplateID = new SqlParameter("@" + TAG_ID, TestTemplateID);
            paramTestTemplateID.DbType = DbType.Int32;
            paramTestTemplateID.Direction = ParameterDirection.Input;
            cmd.Parameters.Add(paramTestTemplateID);
            rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                sqlParseResultSet(rdr);
            }
            // cleanup
            rdr.Close();
            rdr = null;
            paramTestTemplateID = null;
            cmd = null;
        }
        /// <summary>Parse result set</summary>
        protected void sqlParseResultSet(SqlDataReader rdr)
        {
            this.TestTemplateID = long.Parse(rdr[DB_FIELD_ID].ToString());
            try
            {
                this.TestTypeID = Convert.ToInt32(rdr[DB_FIELD_TEST_TYPE_ID].ToString().Trim());
            }
            catch { }
            try
            {
                this.DoPrintReport = Convert.ToBoolean(rdr[DB_FIELD_DO_PRINT_REPORT].ToString().Trim());
            }
            catch { }
            try
            {
                this.DoLicenseIdentification = Convert.ToBoolean(rdr[DB_FIELD_DO_LICENSE_IDENTIFICATION].ToString().Trim());
            }
            catch { }
            try
            {
                this.DoNameIdentification = Convert.ToBoolean(rdr[DB_FIELD_DO_NAME_IDENTIFICATION].ToString().Trim());
            }
            catch { }
            this.PrintTemplateName = rdr[DB_FIELD_PRINT_TEMPLATE_NAME].ToString().Trim();
            try
            {
                this.IsKioskMode = Convert.ToBoolean(rdr[DB_FIELD_IS_KIOSK_MODE].ToString().Trim());
            }
            catch { }
            try
            {
                this.IsSynchronized = Convert.ToBoolean(rdr[DB_FIELD_IS_SYNCHRONIZED].ToString().Trim());
            }
            catch { }
            this.AdminPassword = rdr[DB_FIELD_ADMIN_PASSWORD].ToString().Trim();
            this.ManagerPassword = rdr[DB_FIELD_MANAGER_PASSWORD].ToString().Trim();
            try
            {
                this.ResetInSeconds = Convert.ToInt32(rdr[DB_FIELD_RESET_IN_SECONDS].ToString().Trim());
            }
            catch { }
            try
            {
                this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
            }
            catch
            {
            }
            try
            {
                this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
            }
            catch
            {
            }
        }

        //private
        /// <summary>Log errors</summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        /// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
        public void Test(SqlConnection conn)
        {
            try
            {
                Console.WriteLine("What would you like to do?");
                Console.WriteLine("1.  Save.");
                Console.WriteLine("2.  Delete.");
                Console.WriteLine("3.  Update.");
                Console.WriteLine("4.  Exist.");
                Console.WriteLine("5.  Load.");
                Console.WriteLine("6.  ToXml.");
                Console.WriteLine("q.  Quit.");

                string strAns = "";

                strAns = Console.ReadLine();
                if (strAns != "q")
                {
                    int nAns = 0;
                    nAns = int.Parse(strAns);
                    switch (nAns)
                    {
                        case 1:
                            // insert
                            Console.WriteLine("Save:  ");
                            Prompt();
                            Save(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 2:
                            Console.WriteLine("Delete " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            TestTemplateID = long.Parse(strAns);
                            Delete(conn);
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 3:
                            Console.WriteLine("Update:  ");
                            Prompt();
                            Update(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 4:
                            Console.WriteLine("Exist " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            TestTemplateID = long.Parse(strAns);
                            bool bExist = false;
                            bExist = Exist(conn);
                            Console.WriteLine("Record id " + TestTemplateID + " exist:  " + bExist.ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 5:
                            Console.WriteLine("Load " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            TestTemplateID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToString());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        case 6:
                            Console.WriteLine("ToXml " + TAG_ID + ":  ");
                            strAns = Console.ReadLine();
                            TestTemplateID = long.Parse(strAns);
                            Load(conn);
                            Console.WriteLine(ToXml());
                            Console.WriteLine(" ");
                            Console.WriteLine("Press ENTER to continue...");
                            Console.ReadLine();
                            break;
                        default:
                            Console.WriteLine("Undefined option.");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }

        }
    }
}

//END OF TestTemplate CLASS FILE