using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Apilog.cs
	/// History
	/// ----------------------------------------------------
	/// 001	HA	1/4/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Apilog database table.
	/// </summary>
	public class Apilog
	{
		//Attributes
		/// <summary>ApilogID Attribute type String</summary>
		private long _lApilogID = 0;
		/// <summary>ApikeyID Attribute type String</summary>
		private long _lApikeyID = 0;
		/// <summary>RefNum Attribute type String</summary>
		private long _lRefNum = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>Msgsource Attribute type String</summary>
		private string _strMsgsource = null;
		/// <summary>Trace Attribute type String</summary>
		private string _strTrace = null;
		/// <summary>IsSuccess Attribute type String</summary>
		private bool? _bIsSuccess = null;
		/// <summary>InProgress Attribute type String</summary>
		private bool? _bInProgress = null;
		/// <summary>HttpStatusStr Attribute type String</summary>
		private string _strHttpStatusStr = null;
		/// <summary>HttpStatusNum Attribute type String</summary>
		private long _lHttpStatusNum = 0;
		/// <summary>Msgtxt Attribute type String</summary>
		private string _strMsgtxt = null;
		/// <summary>Reqtxt Attribute type String</summary>
		private string _strReqtxt = null;
		/// <summary>Resptxt Attribute type String</summary>
		private string _strResptxt = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-Apilog";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class Apilog and is of type bool</summary>
		public static readonly string ENTITY_NAME = "Apilog"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "apilog_id"; //Table id field name
		/// <summary>apikey_id Database field </summary>
		public static readonly string DB_FIELD_APIKEY_ID = "apikey_id"; //Table ApikeyID field name
		/// <summary>ref_num Database field </summary>
		public static readonly string DB_FIELD_REF_NUM = "ref_num"; //Table RefNum field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>msgsource Database field </summary>
		public static readonly string DB_FIELD_MSGSOURCE = "msgsource"; //Table Msgsource field name
		/// <summary>trace Database field </summary>
		public static readonly string DB_FIELD_TRACE = "trace"; //Table Trace field name
		/// <summary>is_success Database field </summary>
		public static readonly string DB_FIELD_IS_SUCCESS = "is_success"; //Table IsSuccess field name
		/// <summary>in_progress Database field </summary>
		public static readonly string DB_FIELD_IN_PROGRESS = "in_progress"; //Table InProgress field name
		/// <summary>http_status_str Database field </summary>
		public static readonly string DB_FIELD_HTTP_STATUS_STR = "http_status_str"; //Table HttpStatusStr field name
		/// <summary>http_status_num Database field </summary>
		public static readonly string DB_FIELD_HTTP_STATUS_NUM = "http_status_num"; //Table HttpStatusNum field name
		/// <summary>msgtxt Database field </summary>
		public static readonly string DB_FIELD_MSGTXT = "msgtxt"; //Table Msgtxt field name
		/// <summary>reqtxt Database field </summary>
		public static readonly string DB_FIELD_REQTXT = "reqtxt"; //Table Reqtxt field name
		/// <summary>resptxt Database field </summary>
		public static readonly string DB_FIELD_RESPTXT = "resptxt"; //Table Resptxt field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "ApilogID"; //Attribute id  name
		/// <summary>ApikeyID Attribute type string</summary>
		public static readonly string TAG_APIKEY_ID = "ApikeyID"; //Table ApikeyID field name
		/// <summary>RefNum Attribute type string</summary>
		public static readonly string TAG_REF_NUM = "RefNum"; //Table RefNum field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>Msgsource Attribute type string</summary>
		public static readonly string TAG_MSGSOURCE = "Msgsource"; //Table Msgsource field name
		/// <summary>Trace Attribute type string</summary>
		public static readonly string TAG_TRACE = "Trace"; //Table Trace field name
		/// <summary>IsSuccess Attribute type string</summary>
		public static readonly string TAG_IS_SUCCESS = "IsSuccess"; //Table IsSuccess field name
		/// <summary>InProgress Attribute type string</summary>
		public static readonly string TAG_IN_PROGRESS = "InProgress"; //Table InProgress field name
		/// <summary>HttpStatusStr Attribute type string</summary>
		public static readonly string TAG_HTTP_STATUS_STR = "HttpStatusStr"; //Table HttpStatusStr field name
		/// <summary>HttpStatusNum Attribute type string</summary>
		public static readonly string TAG_HTTP_STATUS_NUM = "HttpStatusNum"; //Table HttpStatusNum field name
		/// <summary>Msgtxt Attribute type string</summary>
		public static readonly string TAG_MSGTXT = "Msgtxt"; //Table Msgtxt field name
		/// <summary>Reqtxt Attribute type string</summary>
		public static readonly string TAG_REQTXT = "Reqtxt"; //Table Reqtxt field name
		/// <summary>Resptxt Attribute type string</summary>
		public static readonly string TAG_RESPTXT = "Resptxt"; //Table Resptxt field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spApilogInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spApilogUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spApilogDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spApilogLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spApilogExist"; //Exist sp name

		//properties
		/// <summary>ApilogID is a Property in the Apilog Class of type long</summary>
		public long ApilogID 
		{
			get{return _lApilogID;}
			set{_lApilogID = value;}
		}
		/// <summary>ApikeyID is a Property in the Apilog Class of type long</summary>
		public long ApikeyID 
		{
			get{return _lApikeyID;}
			set{_lApikeyID = value;}
		}
		/// <summary>RefNum is a Property in the Apilog Class of type long</summary>
		public long RefNum 
		{
			get{return _lRefNum;}
			set{_lRefNum = value;}
		}
		/// <summary>DateCreated is a Property in the Apilog Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>Msgsource is a Property in the Apilog Class of type String</summary>
		public string Msgsource 
		{
			get{return _strMsgsource;}
			set{_strMsgsource = value;}
		}
		/// <summary>Trace is a Property in the Apilog Class of type String</summary>
		public string Trace 
		{
			get{return _strTrace;}
			set{_strTrace = value;}
		}
		/// <summary>IsSuccess is a Property in the Apilog Class of type bool</summary>
		public bool? IsSuccess 
		{
			get{return _bIsSuccess;}
			set{_bIsSuccess = value;}
		}
		/// <summary>InProgress is a Property in the Apilog Class of type bool</summary>
		public bool? InProgress 
		{
			get{return _bInProgress;}
			set{_bInProgress = value;}
		}
		/// <summary>HttpStatusStr is a Property in the Apilog Class of type String</summary>
		public string HttpStatusStr 
		{
			get{return _strHttpStatusStr;}
			set{_strHttpStatusStr = value;}
		}
		/// <summary>HttpStatusNum is a Property in the Apilog Class of type long</summary>
		public long HttpStatusNum 
		{
			get{return _lHttpStatusNum;}
			set{_lHttpStatusNum = value;}
		}
		/// <summary>Msgtxt is a Property in the Apilog Class of type String</summary>
		public string Msgtxt 
		{
			get{return _strMsgtxt;}
			set{_strMsgtxt = value;}
		}
		/// <summary>Reqtxt is a Property in the Apilog Class of type String</summary>
		public string Reqtxt 
		{
			get{return _strReqtxt;}
			set{_strReqtxt = value;}
		}
		/// <summary>Resptxt is a Property in the Apilog Class of type String</summary>
		public string Resptxt 
		{
			get{return _strResptxt;}
			set{_strResptxt = value;}
		}
		/// <summary>HasError Property in class Apilog and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Apilog and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>Apilog empty constructor</summary>
		public Apilog()
		{
		}
		/// <summary>Apilog constructor takes a Config</summary>
		public Apilog(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>Apilog constructor takes ApilogID and a SqlConnection</summary>
		public Apilog(long l, SqlConnection conn) 
		{
			ApilogID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Apilog Constructor takes pStrData and Config</summary>
		public Apilog(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>Apilog Constructor takes SqlDataReader</summary>
		public Apilog(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Apilog Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + ApilogID.ToString() + "\n");
			sbReturn.Append(TAG_APIKEY_ID + ":  " + ApikeyID + "\n");
			sbReturn.Append(TAG_REF_NUM + ":  " + RefNum + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			sbReturn.Append(TAG_MSGSOURCE + ":  " + Msgsource + "\n");
			sbReturn.Append(TAG_TRACE + ":  " + Trace + "\n");
			sbReturn.Append(TAG_IS_SUCCESS + ":  " + IsSuccess + "\n");
			sbReturn.Append(TAG_IN_PROGRESS + ":  " + InProgress + "\n");
			sbReturn.Append(TAG_HTTP_STATUS_STR + ":  " + HttpStatusStr + "\n");
			sbReturn.Append(TAG_HTTP_STATUS_NUM + ":  " + HttpStatusNum + "\n");
			sbReturn.Append(TAG_MSGTXT + ":  " + Msgtxt + "\n");
			sbReturn.Append(TAG_REQTXT + ":  " + Reqtxt + "\n");
			sbReturn.Append(TAG_RESPTXT + ":  " + Resptxt + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Apilog</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Apilog>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ApilogID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_APIKEY_ID + ">" + ApikeyID + "</" + TAG_APIKEY_ID + ">\n");
			sbReturn.Append("<" + TAG_REF_NUM + ">" + RefNum + "</" + TAG_REF_NUM + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			sbReturn.Append("<" + TAG_MSGSOURCE + ">" + Msgsource + "</" + TAG_MSGSOURCE + ">\n");
			sbReturn.Append("<" + TAG_TRACE + ">" + Trace + "</" + TAG_TRACE + ">\n");
			sbReturn.Append("<" + TAG_IS_SUCCESS + ">" + IsSuccess + "</" + TAG_IS_SUCCESS + ">\n");
			sbReturn.Append("<" + TAG_IN_PROGRESS + ">" + InProgress + "</" + TAG_IN_PROGRESS + ">\n");
			sbReturn.Append("<" + TAG_HTTP_STATUS_STR + ">" + HttpStatusStr + "</" + TAG_HTTP_STATUS_STR + ">\n");
			sbReturn.Append("<" + TAG_HTTP_STATUS_NUM + ">" + HttpStatusNum + "</" + TAG_HTTP_STATUS_NUM + ">\n");
			sbReturn.Append("<" + TAG_MSGTXT + ">" + Msgtxt + "</" + TAG_MSGTXT + ">\n");
			sbReturn.Append("<" + TAG_REQTXT + ">" + Reqtxt + "</" + TAG_REQTXT + ">\n");
			sbReturn.Append("<" + TAG_RESPTXT + ">" + Resptxt + "</" + TAG_RESPTXT + ">\n");
			sbReturn.Append("</Apilog>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ApilogID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_APIKEY_ID);
				ApikeyID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ApikeyID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REF_NUM);
				RefNum = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			RefNum = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MSGSOURCE);
				Msgsource = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TRACE);
				Trace = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IS_SUCCESS);
				IsSuccess = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			IsSuccess = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IN_PROGRESS);
				InProgress = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			InProgress = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_HTTP_STATUS_STR);
				HttpStatusStr = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_HTTP_STATUS_NUM);
				HttpStatusNum = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			HttpStatusNum = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MSGTXT);
				Msgtxt = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REQTXT);
				Reqtxt = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_RESPTXT);
				Resptxt = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with apilog_id equal to the current object's ApilogID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where apilog_id equal to the current object's ApilogID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where apilog_id equal to the current object's ApilogID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(Apilog.TAG_APIKEY_ID + ":  ");
				ApikeyID = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(Apilog.TAG_REF_NUM + ":  ");
				RefNum = (long)Convert.ToInt32(Console.ReadLine());
				try
				{
					Console.WriteLine(Apilog.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}

				Console.WriteLine(Apilog.TAG_MSGSOURCE + ":  ");
				Msgsource = Console.ReadLine();

				Console.WriteLine(Apilog.TAG_TRACE + ":  ");
				Trace = Console.ReadLine();

				Console.WriteLine(Apilog.TAG_IS_SUCCESS + ":  ");
				IsSuccess = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Apilog.TAG_IN_PROGRESS + ":  ");
				InProgress = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Apilog.TAG_HTTP_STATUS_STR + ":  ");
				HttpStatusStr = Console.ReadLine();

				Console.WriteLine(Apilog.TAG_HTTP_STATUS_NUM + ":  ");
				HttpStatusNum = (long)Convert.ToInt32(Console.ReadLine());

				Console.WriteLine(Apilog.TAG_MSGTXT + ":  ");
				Msgtxt = Console.ReadLine();

				Console.WriteLine(Apilog.TAG_REQTXT + ":  ");
				Reqtxt = Console.ReadLine();

				Console.WriteLine(Apilog.TAG_RESPTXT + ":  ");
				Resptxt = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApikeyID = null;
			SqlParameter paramRefNum = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramMsgsource = null;
			SqlParameter paramTrace = null;
			SqlParameter paramIsSuccess = null;
			SqlParameter paramInProgress = null;
			SqlParameter paramHttpStatusStr = null;
			SqlParameter paramHttpStatusNum = null;
			SqlParameter paramMsgtxt = null;
			SqlParameter paramReqtxt = null;
			SqlParameter paramResptxt = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramApikeyID = new SqlParameter("@" + TAG_APIKEY_ID, ApikeyID);
			paramApikeyID.DbType = DbType.Int32;
			paramApikeyID.Direction = ParameterDirection.Input;

			paramRefNum = new SqlParameter("@" + TAG_REF_NUM, RefNum);
			paramRefNum.DbType = DbType.Int32;
			paramRefNum.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;

			paramMsgsource = new SqlParameter("@" + TAG_MSGSOURCE, Msgsource);
			paramMsgsource.DbType = DbType.String;
			paramMsgsource.Size = 255;
			paramMsgsource.Direction = ParameterDirection.Input;

			paramTrace = new SqlParameter("@" + TAG_TRACE, Trace);
			paramTrace.DbType = DbType.String;
			paramTrace.Size = 255;
			paramTrace.Direction = ParameterDirection.Input;

			paramIsSuccess = new SqlParameter("@" + TAG_IS_SUCCESS, IsSuccess);
			paramIsSuccess.DbType = DbType.Boolean;
			paramIsSuccess.Direction = ParameterDirection.Input;

			paramInProgress = new SqlParameter("@" + TAG_IN_PROGRESS, InProgress);
			paramInProgress.DbType = DbType.Boolean;
			paramInProgress.Direction = ParameterDirection.Input;

			paramHttpStatusStr = new SqlParameter("@" + TAG_HTTP_STATUS_STR, HttpStatusStr);
			paramHttpStatusStr.DbType = DbType.String;
			paramHttpStatusStr.Size = 255;
			paramHttpStatusStr.Direction = ParameterDirection.Input;

			paramHttpStatusNum = new SqlParameter("@" + TAG_HTTP_STATUS_NUM, HttpStatusNum);
			paramHttpStatusNum.DbType = DbType.Int32;
			paramHttpStatusNum.Direction = ParameterDirection.Input;

			paramMsgtxt = new SqlParameter("@" + TAG_MSGTXT, Msgtxt);
			paramMsgtxt.DbType = DbType.String;
			paramMsgtxt.Direction = ParameterDirection.Input;

			paramReqtxt = new SqlParameter("@" + TAG_REQTXT, Reqtxt);
			paramReqtxt.DbType = DbType.String;
			paramReqtxt.Direction = ParameterDirection.Input;

			paramResptxt = new SqlParameter("@" + TAG_RESPTXT, Resptxt);
			paramResptxt.DbType = DbType.String;
			paramResptxt.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramApikeyID);
			cmd.Parameters.Add(paramRefNum);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramMsgsource);
			cmd.Parameters.Add(paramTrace);
			cmd.Parameters.Add(paramIsSuccess);
			cmd.Parameters.Add(paramInProgress);
			cmd.Parameters.Add(paramHttpStatusStr);
			cmd.Parameters.Add(paramHttpStatusNum);
			cmd.Parameters.Add(paramMsgtxt);
			cmd.Parameters.Add(paramReqtxt);
			cmd.Parameters.Add(paramResptxt);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			ApilogID = long.Parse(strTmp);

			// cleanup to help GC
			paramApikeyID = null;
			paramRefNum = null;
			paramDateCreated = null;
			paramMsgsource = null;
			paramTrace = null;
			paramIsSuccess = null;
			paramInProgress = null;
			paramHttpStatusStr = null;
			paramHttpStatusNum = null;
			paramMsgtxt = null;
			paramReqtxt = null;
			paramResptxt = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramApilogID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramApilogID = new SqlParameter("@" + TAG_ID, ApilogID);
			paramApilogID.Direction = ParameterDirection.Input;
			paramApilogID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramApilogID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramApilogID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApilogID = null;
			SqlParameter paramApikeyID = null;
			SqlParameter paramRefNum = null;
			SqlParameter paramMsgsource = null;
			SqlParameter paramTrace = null;
			SqlParameter paramIsSuccess = null;
			SqlParameter paramInProgress = null;
			SqlParameter paramHttpStatusStr = null;
			SqlParameter paramHttpStatusNum = null;
			SqlParameter paramMsgtxt = null;
			SqlParameter paramReqtxt = null;
			SqlParameter paramResptxt = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramApilogID = new SqlParameter("@" + TAG_ID, ApilogID);
			paramApilogID.DbType = DbType.Int32;
			paramApilogID.Direction = ParameterDirection.Input;


			paramApikeyID = new SqlParameter("@" + TAG_APIKEY_ID, ApikeyID);
			paramApikeyID.DbType = DbType.Int32;
			paramApikeyID.Direction = ParameterDirection.Input;

			paramRefNum = new SqlParameter("@" + TAG_REF_NUM, RefNum);
			paramRefNum.DbType = DbType.Int32;
			paramRefNum.Direction = ParameterDirection.Input;


			paramMsgsource = new SqlParameter("@" + TAG_MSGSOURCE, Msgsource);
			paramMsgsource.DbType = DbType.String;
			paramMsgsource.Size = 255;
			paramMsgsource.Direction = ParameterDirection.Input;

			paramTrace = new SqlParameter("@" + TAG_TRACE, Trace);
			paramTrace.DbType = DbType.String;
			paramTrace.Size = 255;
			paramTrace.Direction = ParameterDirection.Input;

			paramIsSuccess = new SqlParameter("@" + TAG_IS_SUCCESS, IsSuccess);
			paramIsSuccess.DbType = DbType.Boolean;
			paramIsSuccess.Direction = ParameterDirection.Input;

			paramInProgress = new SqlParameter("@" + TAG_IN_PROGRESS, InProgress);
			paramInProgress.DbType = DbType.Boolean;
			paramInProgress.Direction = ParameterDirection.Input;

			paramHttpStatusStr = new SqlParameter("@" + TAG_HTTP_STATUS_STR, HttpStatusStr);
			paramHttpStatusStr.DbType = DbType.String;
			paramHttpStatusStr.Size = 255;
			paramHttpStatusStr.Direction = ParameterDirection.Input;

			paramHttpStatusNum = new SqlParameter("@" + TAG_HTTP_STATUS_NUM, HttpStatusNum);
			paramHttpStatusNum.DbType = DbType.Int32;
			paramHttpStatusNum.Direction = ParameterDirection.Input;

			paramMsgtxt = new SqlParameter("@" + TAG_MSGTXT, Msgtxt);
			paramMsgtxt.DbType = DbType.String;
			paramMsgtxt.Direction = ParameterDirection.Input;

			paramReqtxt = new SqlParameter("@" + TAG_REQTXT, Reqtxt);
			paramReqtxt.DbType = DbType.String;
			paramReqtxt.Direction = ParameterDirection.Input;

			paramResptxt = new SqlParameter("@" + TAG_RESPTXT, Resptxt);
			paramResptxt.DbType = DbType.String;
			paramResptxt.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramApilogID);
			cmd.Parameters.Add(paramApikeyID);
			cmd.Parameters.Add(paramRefNum);
			cmd.Parameters.Add(paramMsgsource);
			cmd.Parameters.Add(paramTrace);
			cmd.Parameters.Add(paramIsSuccess);
			cmd.Parameters.Add(paramInProgress);
			cmd.Parameters.Add(paramHttpStatusStr);
			cmd.Parameters.Add(paramHttpStatusNum);
			cmd.Parameters.Add(paramMsgtxt);
			cmd.Parameters.Add(paramReqtxt);
			cmd.Parameters.Add(paramResptxt);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			ApilogID = long.Parse(s);

			// cleanup
			paramApilogID = null;
			paramApikeyID = null;
			paramRefNum = null;
			paramMsgsource = null;
			paramTrace = null;
			paramIsSuccess = null;
			paramInProgress = null;
			paramHttpStatusStr = null;
			paramHttpStatusNum = null;
			paramMsgtxt = null;
			paramReqtxt = null;
			paramResptxt = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApilogID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramApilogID = new SqlParameter("@" + TAG_ID, ApilogID);
			paramApilogID.DbType = DbType.Int32;
			paramApilogID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramApilogID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramApilogID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramApilogID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramApilogID = new SqlParameter("@" + TAG_ID, ApilogID);
			paramApilogID.DbType = DbType.Int32;
			paramApilogID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramApilogID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramApilogID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.ApilogID = long.Parse(rdr[DB_FIELD_ID].ToString());
			try
			{
			this.ApikeyID = Convert.ToInt32(rdr[DB_FIELD_APIKEY_ID].ToString().Trim());
			}
			catch{}
			try
			{
			this.RefNum = Convert.ToInt32(rdr[DB_FIELD_REF_NUM].ToString().Trim());
			}
			catch{}
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
			this.Msgsource = rdr[DB_FIELD_MSGSOURCE].ToString().Trim();
			this.Trace = rdr[DB_FIELD_TRACE].ToString().Trim();
			try
			{
			this.IsSuccess = Convert.ToBoolean(rdr[DB_FIELD_IS_SUCCESS].ToString().Trim());
			}
			catch{}
			try
			{
			this.InProgress = Convert.ToBoolean(rdr[DB_FIELD_IN_PROGRESS].ToString().Trim());
			}
			catch{}
			this.HttpStatusStr = rdr[DB_FIELD_HTTP_STATUS_STR].ToString().Trim();
			try
			{
			this.HttpStatusNum = Convert.ToInt32(rdr[DB_FIELD_HTTP_STATUS_NUM].ToString().Trim());
			}
			catch{}
			this.Msgtxt = rdr[DB_FIELD_MSGTXT].ToString().Trim();
			this.Reqtxt = rdr[DB_FIELD_REQTXT].ToString().Trim();
			this.Resptxt = rdr[DB_FIELD_RESPTXT].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApilogID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApilogID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + ApilogID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApilogID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ApilogID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF Apilog CLASS FILE
