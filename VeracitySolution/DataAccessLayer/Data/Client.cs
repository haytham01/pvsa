using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

using Veracity.Common;

namespace Veracity.DataAccessLayer.Data
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Client.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH	7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Client database table.
	/// </summary>
	public class Client
	{
		//Attributes
		/// <summary>ClientID Attribute type String</summary>
		private long _lClientID = 0;
		/// <summary>DateCreated Attribute type String</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type String</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DateHeartbeat Attribute type String</summary>
		private DateTime _dtDateHeartbeat = dtNull;
		/// <summary>Code Attribute type String</summary>
		private string _strCode = null;
		/// <summary>Description Attribute type String</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute type String</summary>
		private string _strVisibleCode = null;
		/// <summary>TestInProgress Attribute type String</summary>
		private bool? _bTestInProgress = null;
		/// <summary>AppMetaData Attribute type String</summary>
		private string _strAppMetaData = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		private string _strLognameText = "DataAccessLayer-Data-Client";
		private bool _hasError = false;
		private static DateTime dtNull = new DateTime();

		/// <summary>HasError Property in class Client and is of type bool</summary>
		public static readonly string ENTITY_NAME = "Client"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "client_id"; //Table id field name
		/// <summary>date_created Database field </summary>
		public static readonly string DB_FIELD_DATE_CREATED = "date_created"; //Table DateCreated field name
		/// <summary>date_modified Database field </summary>
		public static readonly string DB_FIELD_DATE_MODIFIED = "date_modified"; //Table DateModified field name
		/// <summary>date_heartbeat Database field </summary>
		public static readonly string DB_FIELD_DATE_HEARTBEAT = "date_heartbeat"; //Table DateHeartbeat field name
		/// <summary>code Database field </summary>
		public static readonly string DB_FIELD_CODE = "code"; //Table Code field name
		/// <summary>description Database field </summary>
		public static readonly string DB_FIELD_DESCRIPTION = "description"; //Table Description field name
		/// <summary>visible_code Database field </summary>
		public static readonly string DB_FIELD_VISIBLE_CODE = "visible_code"; //Table VisibleCode field name
		/// <summary>test_in_progress Database field </summary>
		public static readonly string DB_FIELD_TEST_IN_PROGRESS = "test_in_progress"; //Table TestInProgress field name
		/// <summary>app_meta_data Database field </summary>
		public static readonly string DB_FIELD_APP_META_DATA = "app_meta_data"; //Table AppMetaData field name

		// Attribute variables
		/// <summary>TAG_ID Attribute type string</summary>
		public static readonly string TAG_ID = "ClientID"; //Attribute id  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DateHeartbeat Attribute type string</summary>
		public static readonly string TAG_DATE_HEARTBEAT = "DateHeartbeat"; //Table DateHeartbeat field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>Description Attribute type string</summary>
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		/// <summary>VisibleCode Attribute type string</summary>
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		/// <summary>TestInProgress Attribute type string</summary>
		public static readonly string TAG_TEST_IN_PROGRESS = "TestInProgress"; //Table TestInProgress field name
		/// <summary>AppMetaData Attribute type string</summary>
		public static readonly string TAG_APP_META_DATA = "AppMetaData"; //Table AppMetaData field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spClientInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spClientUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spClientDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spClientLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spClientExist"; //Exist sp name

		//properties
		/// <summary>ClientID is a Property in the Client Class of type long</summary>
		public long ClientID 
		{
			get{return _lClientID;}
			set{_lClientID = value;}
		}
		/// <summary>DateCreated is a Property in the Client Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Client Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DateHeartbeat is a Property in the Client Class of type DateTime</summary>
		public DateTime DateHeartbeat 
		{
			get{return _dtDateHeartbeat;}
			set{_dtDateHeartbeat = value;}
		}
		/// <summary>Code is a Property in the Client Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the Client Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the Client Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>TestInProgress is a Property in the Client Class of type bool</summary>
		public bool? TestInProgress 
		{
			get{return _bTestInProgress;}
			set{_bTestInProgress = value;}
		}
		/// <summary>AppMetaData is a Property in the Client Class of type String</summary>
		public string AppMetaData 
		{
			get{return _strAppMetaData;}
			set{_strAppMetaData = value;}
		}


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		/// <summary>HasError Property in class Client and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Client and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

//Constructors
		/// <summary>Client empty constructor</summary>
		public Client()
		{
		}
		/// <summary>Client constructor takes a Config</summary>
		public Client(Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
		}
		/// <summary>Client constructor takes ClientID and a SqlConnection</summary>
		public Client(long l, SqlConnection conn) 
		{
			ClientID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Client Constructor takes pStrData and Config</summary>
		public Client(string pStrData, Config pConfig)
		{
			_config = pConfig;
			_oLog = new Logger(_strLognameText);
			Parse(pStrData);
		}
		/// <summary>Client Constructor takes SqlDataReader</summary>
		public Client(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Client Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + ClientID.ToString() + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(DateHeartbeat))
			{
				sbReturn.Append(TAG_DATE_HEARTBEAT + ":  " + DateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_HEARTBEAT + ":\n");
			}
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TEST_IN_PROGRESS + ":  " + TestInProgress + "\n");
			sbReturn.Append(TAG_APP_META_DATA + ":  " + AppMetaData + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Client</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Client>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ClientID + "</" + TAG_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(DateHeartbeat))
			{
				sbReturn.Append("<" + TAG_DATE_HEARTBEAT + ">" + DateHeartbeat.ToString() + "</" + TAG_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_HEARTBEAT + "></" + TAG_DATE_HEARTBEAT + ">\n");
			}
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TEST_IN_PROGRESS + ">" + TestInProgress + "</" + TAG_TEST_IN_PROGRESS + ">\n");
			sbReturn.Append("<" + TAG_APP_META_DATA + ">" + AppMetaData + "</" + TAG_APP_META_DATA + ">\n");
			sbReturn.Append("</Client>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ClientID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_HEARTBEAT);
				DateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_IN_PROGRESS);
				TestInProgress = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			TestInProgress = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_APP_META_DATA);
				AppMetaData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Calls sqlLoad() method which gets record from database with client_id equal to the current object's ClientID </summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlUpdate() method which record record from database with current object values where client_id equal to the current object's ClientID </summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
				_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlInsert() method which inserts a record into the database with current object values</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
				_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Calls sqlDelete() method which delete's the record from database where where client_id equal to the current object's ClientID </summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		/// <summary>Calls sqlExists() returns true if the record exists, false if not </summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{
				{
					Console.WriteLine(TAG_ID + ":  ");
					try
					{
						ClientID = long.Parse(Console.ReadLine());
					}
					catch 
					{
						ClientID = 0;
					}
				}
				try
				{
					Console.WriteLine(Client.TAG_DATE_CREATED + ":  ");
					DateCreated = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateCreated = new DateTime();
				}
				try
				{
					Console.WriteLine(Client.TAG_DATE_MODIFIED + ":  ");
					DateModified = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateModified = new DateTime();
				}
				try
				{
					Console.WriteLine(Client.TAG_DATE_HEARTBEAT + ":  ");
					DateHeartbeat = DateTime.Parse(Console.ReadLine());
				}
				catch 
				{
					DateHeartbeat = new DateTime();
				}

				Console.WriteLine(Client.TAG_CODE + ":  ");
				Code = Console.ReadLine();

				Console.WriteLine(Client.TAG_DESCRIPTION + ":  ");
				Description = Console.ReadLine();

				Console.WriteLine(Client.TAG_VISIBLE_CODE + ":  ");
				VisibleCode = Console.ReadLine();

				Console.WriteLine(Client.TAG_TEST_IN_PROGRESS + ":  ");
				TestInProgress = Convert.ToBoolean(Console.ReadLine());

				Console.WriteLine(Client.TAG_APP_META_DATA + ":  ");
				AppMetaData = Console.ReadLine();

			}
			catch (Exception e) 
			{
				 _log("ERROR", e.ToString() + e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}
		
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramClientID = null;
			SqlParameter paramDateCreated = null;
			SqlParameter paramDateHeartbeat = null;
			SqlParameter paramCode = null;
			SqlParameter paramDescription = null;
			SqlParameter paramVisibleCode = null;
			SqlParameter paramTestInProgress = null;
			SqlParameter paramAppMetaData = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters
			paramClientID = new SqlParameter("@" + TAG_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;

				paramDateCreated = new SqlParameter("@" + TAG_DATE_CREATED, DateTime.Now);
			paramDateCreated.DbType = DbType.DateTime;
			paramDateCreated.Direction = ParameterDirection.Input;


			if (!dtNull.Equals(DateHeartbeat))
			{
				paramDateHeartbeat = new SqlParameter("@" + TAG_DATE_HEARTBEAT, DateHeartbeat);
			}
			else
			{
				paramDateHeartbeat = new SqlParameter("@" + TAG_DATE_HEARTBEAT, DBNull.Value);
			}
			paramDateHeartbeat.DbType = DbType.DateTime;
			paramDateHeartbeat.Direction = ParameterDirection.Input;

			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, Description);
			paramDescription.DbType = DbType.String;
			paramDescription.Size = 255;
			paramDescription.Direction = ParameterDirection.Input;

			paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, VisibleCode);
			paramVisibleCode.DbType = DbType.String;
			paramVisibleCode.Size = 255;
			paramVisibleCode.Direction = ParameterDirection.Input;

			paramTestInProgress = new SqlParameter("@" + TAG_TEST_IN_PROGRESS, TestInProgress);
			paramTestInProgress.DbType = DbType.Boolean;
			paramTestInProgress.Direction = ParameterDirection.Input;

			paramAppMetaData = new SqlParameter("@" + TAG_APP_META_DATA, AppMetaData);
			paramAppMetaData.DbType = DbType.String;
			paramAppMetaData.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramClientID);
			cmd.Parameters.Add(paramDateCreated);
			cmd.Parameters.Add(paramDateHeartbeat);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramDescription);
			cmd.Parameters.Add(paramVisibleCode);
			cmd.Parameters.Add(paramTestInProgress);
			cmd.Parameters.Add(paramAppMetaData);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			ClientID = long.Parse(strTmp);

			// cleanup to help GC
			paramClientID = null;
			paramDateCreated = null;
			paramDateHeartbeat = null;
			paramCode = null;
			paramDescription = null;
			paramVisibleCode = null;
			paramTestInProgress = null;
			paramAppMetaData = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramClientID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramClientID = new SqlParameter("@" + TAG_ID, ClientID);
			paramClientID.Direction = ParameterDirection.Input;
			paramClientID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramClientID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramClientID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramClientID = null;
			SqlParameter paramDateModified = null;
			SqlParameter paramDateHeartbeat = null;
			SqlParameter paramCode = null;
			SqlParameter paramDescription = null;
			SqlParameter paramVisibleCode = null;
			SqlParameter paramTestInProgress = null;
			SqlParameter paramAppMetaData = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure	
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
			
			// parameters

			paramClientID = new SqlParameter("@" + TAG_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;



				paramDateModified = new SqlParameter("@" + TAG_DATE_MODIFIED, DateTime.Now);
			paramDateModified.DbType = DbType.DateTime;
			paramDateModified.Direction = ParameterDirection.Input;

			if (!dtNull.Equals(DateHeartbeat))
			{
				paramDateHeartbeat = new SqlParameter("@" + TAG_DATE_HEARTBEAT, DateHeartbeat);
			}
			else
			{
				paramDateHeartbeat = new SqlParameter("@" + TAG_DATE_HEARTBEAT, DBNull.Value);
			}
			paramDateHeartbeat.DbType = DbType.DateTime;
			paramDateHeartbeat.Direction = ParameterDirection.Input;

			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, Description);
			paramDescription.DbType = DbType.String;
			paramDescription.Size = 255;
			paramDescription.Direction = ParameterDirection.Input;

			paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, VisibleCode);
			paramVisibleCode.DbType = DbType.String;
			paramVisibleCode.Size = 255;
			paramVisibleCode.Direction = ParameterDirection.Input;

			paramTestInProgress = new SqlParameter("@" + TAG_TEST_IN_PROGRESS, TestInProgress);
			paramTestInProgress.DbType = DbType.Boolean;
			paramTestInProgress.Direction = ParameterDirection.Input;

			paramAppMetaData = new SqlParameter("@" + TAG_APP_META_DATA, AppMetaData);
			paramAppMetaData.DbType = DbType.String;
			paramAppMetaData.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramClientID);
			cmd.Parameters.Add(paramDateModified);
			cmd.Parameters.Add(paramDateHeartbeat);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramDescription);
			cmd.Parameters.Add(paramVisibleCode);
			cmd.Parameters.Add(paramTestInProgress);
			cmd.Parameters.Add(paramAppMetaData);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			ClientID = long.Parse(s);

			// cleanup
			paramClientID = null;
			paramDateModified = null;
			paramDateHeartbeat = null;
			paramCode = null;
			paramDescription = null;
			paramVisibleCode = null;
			paramTestInProgress = null;
			paramAppMetaData = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramClientID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramClientID = new SqlParameter("@" + TAG_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramClientID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramClientID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramClientID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramClientID = new SqlParameter("@" + TAG_ID, ClientID);
			paramClientID.DbType = DbType.Int32;
			paramClientID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramClientID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramClientID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.ClientID = long.Parse(rdr[DB_FIELD_ID].ToString());
         try
			{
				this.DateCreated = DateTime.Parse(rdr[DB_FIELD_DATE_CREATED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateModified = DateTime.Parse(rdr[DB_FIELD_DATE_MODIFIED].ToString());
			}
			catch 
			{
			}
         try
			{
				this.DateHeartbeat = DateTime.Parse(rdr[DB_FIELD_DATE_HEARTBEAT].ToString());
			}
			catch 
			{
			}
			this.Code = rdr[DB_FIELD_CODE].ToString().Trim();
			this.Description = rdr[DB_FIELD_DESCRIPTION].ToString().Trim();
			this.VisibleCode = rdr[DB_FIELD_VISIBLE_CODE].ToString().Trim();
			try
			{
			this.TestInProgress = Convert.ToBoolean(rdr[DB_FIELD_TEST_IN_PROGRESS].ToString().Trim());
			}
			catch{}
			this.AppMetaData = rdr[DB_FIELD_APP_META_DATA].ToString().Trim();
		}

		//private
		/// <summary>Log errors</summary>
		private void _log(string pStrAction, string pStrMsgText) 
		{
			if (_config != null )
			{
				if (_config.DoLogInfo)
				{
						_oLog.Log(pStrAction, pStrMsgText);
				}
			}

		}

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
				
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{	
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ClientID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ClientID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + ClientID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ClientID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ClientID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
			}

		}		
	}
}

//END OF Client CLASS FILE
