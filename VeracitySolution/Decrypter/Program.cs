﻿using Veracity.Client.Proxy;
using System.IO;
using System;

namespace Decrypter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string CurrentExecutablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string CurrentExecutableDirPath = System.IO.Path.GetDirectoryName(CurrentExecutablePath);
                string pathToSerialize = CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + TestSession.CSV_FILENAME_ENCRYPTED;
                string pathToSerialize1 = CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + TestSession.CSV_FILENAME1_ENCRYPTED;
                string pathToSerializeSensor = CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + TestSession.CSV_FILENAME_SENSOR_ENCRYPTED;
                string pathToSerialize1Sensor = CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + TestSession.CSV_FILENAME1_SENSOR_ENCRYPTED;
                string dataEncrypted = null;
                string dataDecrypted = null;

                if (File.Exists(pathToSerialize))
                {
                    // decrypt it
                    dataEncrypted = File.ReadAllText(pathToSerialize);
                    dataDecrypted = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                    System.IO.File.WriteAllText(CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + "PVSA-OUTPUT-DECRYPTED.csv", dataDecrypted);
                }

                if (File.Exists(pathToSerialize1))
                {
                    // decrypt it
                    dataEncrypted = File.ReadAllText(pathToSerialize1);
                    dataDecrypted = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                    System.IO.File.WriteAllText(CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + "PVSA-OUTPUT1-DECRYPTED.csv", dataDecrypted);
                }

                if (File.Exists(pathToSerializeSensor))
                {
                    // decrypt it
                    dataEncrypted = File.ReadAllText(pathToSerializeSensor);
                    dataDecrypted = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                    System.IO.File.WriteAllText(CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + "PVSA-SENSOR-OUTPUT-SENSOR-DECRYPTED.CSV", dataDecrypted);
                }

                if (File.Exists(pathToSerialize1Sensor))
                {
                    // decrypt it
                    dataEncrypted = File.ReadAllText(pathToSerialize1Sensor);
                    dataDecrypted = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                    System.IO.File.WriteAllText(CurrentExecutableDirPath + System.IO.Path.DirectorySeparatorChar + "PVSA-SENSOR-OUTPUT1-DECRYPTED.CSV", dataDecrypted);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in decrypting.");
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
