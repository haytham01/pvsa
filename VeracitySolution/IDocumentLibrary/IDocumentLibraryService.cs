﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace IDocumentLibrary
{
    /// <summary>
    /// Interface IDocumentLibraryService
    /// </summary>
    [ServiceContract]
    public interface IDocumentLibraryService
    {
        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="fPath">The file path.</param>
        /// <param name="data">The data.</param>
        [OperationContract]
        void UploadDocument(string fPath, byte[] data);

        /// <summary>
        /// Downloads the document.
        /// </summary>
        /// <param name="fPath">The file path.</param>
        /// <returns>System.Byte[][].</returns>
        [OperationContract]
        byte[] DownloadDocument(string fPath);

        /// <summary>
        /// Gets the dir list.
        /// </summary>
        /// <param name="dPath">The directory path.</param>
        /// <returns>System.String.</returns>
        [OperationContract]
        string GetDirList(string dPath);

    }
}
