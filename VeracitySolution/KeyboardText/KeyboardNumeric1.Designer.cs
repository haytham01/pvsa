namespace KeyboardNumeric1
{
    partial class KeyboardNumeric1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyboardNumeric1));
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.backspaceButton = new System.Windows.Forms.Button();
            this.zeroButton = new System.Windows.Forms.Button();
            this.doubleZeroButton = new System.Windows.Forms.Button();
            this.threeButton = new System.Windows.Forms.Button();
            this.twoButton = new System.Windows.Forms.Button();
            this.oneButton = new System.Windows.Forms.Button();
            this.sixButton = new System.Windows.Forms.Button();
            this.fiveButton = new System.Windows.Forms.Button();
            this.fourButton = new System.Windows.Forms.Button();
            this.nineButton = new System.Windows.Forms.Button();
            this.eightButton = new System.Windows.Forms.Button();
            this.sevenButton = new System.Windows.Forms.Button();
            this.mainTableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.mainTableLayoutPanel.ColumnCount = 3;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.mainTableLayoutPanel.Controls.Add(this.backspaceButton, 2, 3);
            this.mainTableLayoutPanel.Controls.Add(this.zeroButton, 1, 3);
            this.mainTableLayoutPanel.Controls.Add(this.doubleZeroButton, 0, 3);
            this.mainTableLayoutPanel.Controls.Add(this.threeButton, 2, 2);
            this.mainTableLayoutPanel.Controls.Add(this.twoButton, 1, 2);
            this.mainTableLayoutPanel.Controls.Add(this.oneButton, 0, 2);
            this.mainTableLayoutPanel.Controls.Add(this.sixButton, 2, 1);
            this.mainTableLayoutPanel.Controls.Add(this.fiveButton, 1, 1);
            this.mainTableLayoutPanel.Controls.Add(this.fourButton, 0, 1);
            this.mainTableLayoutPanel.Controls.Add(this.nineButton, 2, 0);
            this.mainTableLayoutPanel.Controls.Add(this.eightButton, 1, 0);
            this.mainTableLayoutPanel.Controls.Add(this.sevenButton, 0, 0);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 4;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(224, 145);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // backspaceButton
            // 
            this.backspaceButton.BackColor = System.Drawing.Color.Transparent;
            this.backspaceButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("backspaceButton.BackgroundImage")));
            this.backspaceButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.backspaceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.backspaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backspaceButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backspaceButton.ForeColor = System.Drawing.Color.White;
            this.backspaceButton.Location = new System.Drawing.Point(152, 111);
            this.backspaceButton.Name = "backspaceButton";
            this.backspaceButton.Size = new System.Drawing.Size(69, 31);
            this.backspaceButton.TabIndex = 11;
            this.backspaceButton.UseVisualStyleBackColor = false;
            this.backspaceButton.Click += new System.EventHandler(this.backspaceButton_Click);
            // 
            // zeroButton
            // 
            this.zeroButton.BackColor = System.Drawing.Color.Transparent;
            this.zeroButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("zeroButton.BackgroundImage")));
            this.zeroButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.zeroButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zeroButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zeroButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zeroButton.ForeColor = System.Drawing.Color.White;
            this.zeroButton.Location = new System.Drawing.Point(79, 111);
            this.zeroButton.Name = "zeroButton";
            this.zeroButton.Size = new System.Drawing.Size(67, 31);
            this.zeroButton.TabIndex = 10;
            this.zeroButton.UseVisualStyleBackColor = false;
            this.zeroButton.Click += new System.EventHandler(this.zeroButton_Click);
            // 
            // doubleZeroButton
            // 
            this.doubleZeroButton.BackColor = System.Drawing.Color.Transparent;
            this.doubleZeroButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.doubleZeroButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doubleZeroButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.doubleZeroButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doubleZeroButton.ForeColor = System.Drawing.Color.White;
            this.doubleZeroButton.Location = new System.Drawing.Point(3, 111);
            this.doubleZeroButton.Name = "doubleZeroButton";
            this.doubleZeroButton.Size = new System.Drawing.Size(70, 31);
            this.doubleZeroButton.TabIndex = 9;
            this.doubleZeroButton.UseVisualStyleBackColor = false;
            this.doubleZeroButton.Visible = false;
            this.doubleZeroButton.Click += new System.EventHandler(this.doubleZeroButton_Click);
            // 
            // threeButton
            // 
            this.threeButton.BackColor = System.Drawing.Color.Transparent;
            this.threeButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("threeButton.BackgroundImage")));
            this.threeButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.threeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.threeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.threeButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.threeButton.ForeColor = System.Drawing.Color.White;
            this.threeButton.Location = new System.Drawing.Point(152, 75);
            this.threeButton.Name = "threeButton";
            this.threeButton.Size = new System.Drawing.Size(69, 30);
            this.threeButton.TabIndex = 8;
            this.threeButton.UseVisualStyleBackColor = false;
            this.threeButton.Click += new System.EventHandler(this.threeButton_Click);
            // 
            // twoButton
            // 
            this.twoButton.BackColor = System.Drawing.Color.Transparent;
            this.twoButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("twoButton.BackgroundImage")));
            this.twoButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.twoButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.twoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.twoButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.twoButton.ForeColor = System.Drawing.Color.White;
            this.twoButton.Location = new System.Drawing.Point(79, 75);
            this.twoButton.Name = "twoButton";
            this.twoButton.Size = new System.Drawing.Size(67, 30);
            this.twoButton.TabIndex = 7;
            this.twoButton.UseVisualStyleBackColor = false;
            this.twoButton.Click += new System.EventHandler(this.twoButton_Click);
            // 
            // oneButton
            // 
            this.oneButton.BackColor = System.Drawing.Color.Transparent;
            this.oneButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("oneButton.BackgroundImage")));
            this.oneButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.oneButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oneButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.oneButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oneButton.ForeColor = System.Drawing.Color.White;
            this.oneButton.Location = new System.Drawing.Point(3, 75);
            this.oneButton.Name = "oneButton";
            this.oneButton.Size = new System.Drawing.Size(70, 30);
            this.oneButton.TabIndex = 6;
            this.oneButton.UseVisualStyleBackColor = false;
            this.oneButton.Click += new System.EventHandler(this.oneButton_Click);
            // 
            // sixButton
            // 
            this.sixButton.BackColor = System.Drawing.Color.Transparent;
            this.sixButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sixButton.BackgroundImage")));
            this.sixButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sixButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sixButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sixButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sixButton.ForeColor = System.Drawing.Color.White;
            this.sixButton.Location = new System.Drawing.Point(152, 39);
            this.sixButton.Name = "sixButton";
            this.sixButton.Size = new System.Drawing.Size(69, 30);
            this.sixButton.TabIndex = 5;
            this.sixButton.UseVisualStyleBackColor = false;
            this.sixButton.Click += new System.EventHandler(this.sixButton_Click);
            // 
            // fiveButton
            // 
            this.fiveButton.BackColor = System.Drawing.Color.Transparent;
            this.fiveButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fiveButton.BackgroundImage")));
            this.fiveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fiveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fiveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fiveButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fiveButton.ForeColor = System.Drawing.Color.White;
            this.fiveButton.Location = new System.Drawing.Point(79, 39);
            this.fiveButton.Name = "fiveButton";
            this.fiveButton.Size = new System.Drawing.Size(67, 30);
            this.fiveButton.TabIndex = 4;
            this.fiveButton.UseVisualStyleBackColor = false;
            this.fiveButton.Click += new System.EventHandler(this.fiveButton_Click);
            // 
            // fourButton
            // 
            this.fourButton.BackColor = System.Drawing.Color.Transparent;
            this.fourButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fourButton.BackgroundImage")));
            this.fourButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fourButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fourButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fourButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fourButton.ForeColor = System.Drawing.Color.White;
            this.fourButton.Location = new System.Drawing.Point(3, 39);
            this.fourButton.Name = "fourButton";
            this.fourButton.Size = new System.Drawing.Size(70, 30);
            this.fourButton.TabIndex = 3;
            this.fourButton.UseVisualStyleBackColor = false;
            this.fourButton.Click += new System.EventHandler(this.fourButton_Click);
            // 
            // nineButton
            // 
            this.nineButton.BackColor = System.Drawing.Color.Transparent;
            this.nineButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("nineButton.BackgroundImage")));
            this.nineButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.nineButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nineButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nineButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nineButton.ForeColor = System.Drawing.Color.White;
            this.nineButton.Location = new System.Drawing.Point(152, 3);
            this.nineButton.Name = "nineButton";
            this.nineButton.Size = new System.Drawing.Size(69, 30);
            this.nineButton.TabIndex = 2;
            this.nineButton.UseVisualStyleBackColor = false;
            this.nineButton.Click += new System.EventHandler(this.nineButton_Click);
            // 
            // eightButton
            // 
            this.eightButton.BackColor = System.Drawing.Color.Transparent;
            this.eightButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("eightButton.BackgroundImage")));
            this.eightButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eightButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eightButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eightButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eightButton.ForeColor = System.Drawing.Color.White;
            this.eightButton.Location = new System.Drawing.Point(79, 3);
            this.eightButton.Name = "eightButton";
            this.eightButton.Size = new System.Drawing.Size(67, 30);
            this.eightButton.TabIndex = 1;
            this.eightButton.UseVisualStyleBackColor = false;
            this.eightButton.Click += new System.EventHandler(this.eightButton_Click);
            // 
            // sevenButton
            // 
            this.sevenButton.BackColor = System.Drawing.Color.Transparent;
            this.sevenButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sevenButton.BackgroundImage")));
            this.sevenButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sevenButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sevenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sevenButton.Font = new System.Drawing.Font("Tahoma", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sevenButton.ForeColor = System.Drawing.Color.White;
            this.sevenButton.Location = new System.Drawing.Point(3, 3);
            this.sevenButton.Name = "sevenButton";
            this.sevenButton.Size = new System.Drawing.Size(70, 30);
            this.sevenButton.TabIndex = 0;
            this.sevenButton.UseVisualStyleBackColor = false;
            this.sevenButton.Click += new System.EventHandler(this.sevenButton_Click);
            // 
            // KeyboardNumeric1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Name = "KeyboardNumeric1";
            this.Size = new System.Drawing.Size(224, 145);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.Button sevenButton;
        private System.Windows.Forms.Button backspaceButton;
        private System.Windows.Forms.Button zeroButton;
        private System.Windows.Forms.Button doubleZeroButton;
        private System.Windows.Forms.Button threeButton;
        private System.Windows.Forms.Button twoButton;
        private System.Windows.Forms.Button oneButton;
        private System.Windows.Forms.Button sixButton;
        private System.Windows.Forms.Button fiveButton;
        private System.Windows.Forms.Button fourButton;
        private System.Windows.Forms.Button nineButton;
        private System.Windows.Forms.Button eightButton;
    }
}
