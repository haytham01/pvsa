using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace KeyboardNumeric1
{
    public partial class KeyboardNumeric1 : UserControl
    {
        private String _strKeypPressed = "";
        private bool _bShowBSButton = false;

        public bool ShowBSButton
        {
            get
            {
                return _bShowBSButton;
            }
            set
            {
                _bShowBSButton = value;
                this.backspaceButton.Visible = value;
            }
        }

        public String KeyPressed
        {
            get { return _strKeypPressed; }
        }

        public KeyboardNumeric1()
        {
            InitializeComponent();
            this.backspaceButton.Visible = _bShowBSButton;
        }

        public delegate void KeyboardDelegate(object sender, KeyboardEventArgs e);

        [Category("Mouse"), Description("Return value of mouseclicked key")]
        public event KeyboardDelegate UserKeyPressed;
        protected virtual void OnUserKeyPressed(KeyboardEventArgs e)
        {
            if (UserKeyPressed != null)
                UserKeyPressed(this, e);
        }

        private void sevenButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("7");
            OnUserKeyPressed(dea);
        }

        private void eightButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("8");
            OnUserKeyPressed(dea);
        }

        private void nineButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("9");
            OnUserKeyPressed(dea);
        }

        private void fourButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("4");
            OnUserKeyPressed(dea);
        }

        private void fiveButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("5");
            OnUserKeyPressed(dea);
        }

        private void sixButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("6");
            OnUserKeyPressed(dea);
        }

        private void oneButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("1");
            OnUserKeyPressed(dea);
        }

        private void twoButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("2");
            OnUserKeyPressed(dea);
        }

        private void threeButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("3");
            OnUserKeyPressed(dea);
        }

        private void doubleZeroButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("00");
            OnUserKeyPressed(dea);
        }

        private void zeroButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("0");
            OnUserKeyPressed(dea);
        }

        private void backspaceButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("{BACKSPACE}");
            OnUserKeyPressed(dea);
        }

        //private void decimalButton_Click(object sender, EventArgs e)
        //{
        //    KeyboardEventArgs dea = new KeyboardEventArgs(".");
        //    OnUserKeyPressed(dea);
        //}
    }


    public class KeyboardEventArgs : EventArgs
    {
        private readonly string pvtKeyboardKeyPressed;

        public KeyboardEventArgs(string KeyboardKeyPressed)
        {
            this.pvtKeyboardKeyPressed = KeyboardKeyPressed;
        }

        public string KeyboardKeyPressed
        {
            get
            {
                return pvtKeyboardKeyPressed;
            }
        }
    }
}