﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using AudioPlayerLib;
using WaveResources;

namespace AudioPlayerDemo {
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window {
    private readonly AudioPlayer m_contentAudioPlayer 
      = new AudioPlayer("661__simondsouza__calling_out_longer_12_beat_Dm_90.mp3");

    // Simulate absolute directory by using "GetCurrentDirectory()"
    private readonly AudioPlayer m_absoluteFilePlayer
      = new AudioPlayer(Directory.GetCurrentDirectory() + "/Windows Ding.wav");

    private readonly AudioPlayer m_wavResourcePlayer
      = new AudioPlayer(WaveResourceAssembly.Assembly, "WaveResources", "406__TicTacShutUp__click_1_d.wav", true);

    private readonly AudioPlayer m_mp3ResourcePlayer
      = new AudioPlayer(WaveResourceAssembly.Assembly, "WaveResources", "554__bebeto__Ambient_loop.mp3");

    public MainWindow() {
      InitializeComponent();

      this.m_contentAudioPlayer.PlaybackEnded += (s, e) => {
        this.m_playRelativeFromFileSystemButton.Content = "Play";
      };

      this.m_absoluteFilePlayer.PlaybackEnded += (s, e) => {
        this.m_playAbsoluteFromFileSystemButton.Content = "Play";
      };

      this.m_wavResourcePlayer.PlaybackEnded += (s, e) => {
        this.m_playLoopedWavFromResourceButton.Content = "Play";
      };

      this.m_mp3ResourcePlayer.PlaybackEnded += (s, e) => {
        this.m_playMp3FromResourceButton.Content = "Play";
      };
    }

    private void TogglePlayback(AudioPlayer player, Button playButton) {
      if (player.IsPlaying) {
        player.Stop();
      } else {
        player.Play();
        playButton.Content = "Stop";
      }
    }

    private void PlayRelativeFromFileSystem(object sender, RoutedEventArgs e) {
      TogglePlayback(this.m_contentAudioPlayer, this.m_playRelativeFromFileSystemButton);
    }

    private void PlayAbsoluteFromFileSystem(object sender, RoutedEventArgs e) {
      TogglePlayback(this.m_absoluteFilePlayer, this.m_playAbsoluteFromFileSystemButton);
    }

    private void PlayLoopedWavFromResource(object sender, RoutedEventArgs e) {
      TogglePlayback(this.m_wavResourcePlayer, this.m_playLoopedWavFromResourceButton);
    }

    private void PlayMp3FromResource(object sender, RoutedEventArgs e) {
      TogglePlayback(this.m_mp3ResourcePlayer, this.m_playMp3FromResourceButton);
    }
  }
}
