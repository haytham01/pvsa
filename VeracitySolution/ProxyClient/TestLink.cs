﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Veracity.Client.Proxy
{
    public class TestLink
    {
        private DateTime _dtDateCreated = dtNull;
        private DateTime _dtDateModified = dtNull;
        private string _strCode = null;
        private string _strDescription = null;
        private int _nIndex = 0;

        public bool isDirty = false;

        private bool _hasError = false;

        private static DateTime dtNull = new DateTime();
        public static readonly string ENTITY_NAME = "TestLink";

        public static readonly string TAG_DATE_CREATED = "DateCreated"; 
        public static readonly string TAG_DATE_MODIFIED = "DateModified";
        public static readonly string TAG_CODE = "Code";
        public static readonly string TAG_DESCRIPTION = "Description";
        public static readonly string TAG_INDEX = "Index";

        public int Index
        {
            get { return _nIndex; }
            set { _nIndex = value; }
        }
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }
        public string Code
        {
            get { return _strCode; }
            set { _strCode = value; }
        }
        public string Description
        {
            get { return _strDescription; }
            set { _strDescription = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public TestLink()
		{
		}
        public TestLink(string pStrData)
		{
			Parse(pStrData);
		}
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
            }
         
            sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
            sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
            sbReturn.Append("<" + TAG_INDEX + ">" + Index + "</" + TAG_INDEX + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">\n");

            return sbReturn.ToString();
        }
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch
            {
                _hasError = true;
           }
        }
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
                DateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_CODE);
                Code = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
                Description = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_INDEX);
                Index = Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                Index = 0;
            }

        }
    }
}
