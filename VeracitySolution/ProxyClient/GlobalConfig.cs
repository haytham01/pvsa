using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  GlobalConfig.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the GlobalConfig database table.
	/// </summary>
	public class GlobalConfig
	{
		//attributes
		/// <summary>GlobalConfigID Attribute type long</summary>
		private long _lGlobalConfigID = 0;
		/// <summary>DateCreated Attribute type DateTime</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type DateTime</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>Code Attribute type string</summary>
		private string _strCode = null;
		/// <summary>Description Attribute type string</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute type string</summary>
		private string _strVisibleCode = null;
		/// <summary>ConfigValue Attribute type string</summary>
		private string _strConfigValue = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "GlobalConfig"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "GlobalConfigID"; //Attribute id  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>Description Attribute type string</summary>
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		/// <summary>VisibleCode Attribute type string</summary>
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		/// <summary>ConfigValue Attribute type string</summary>
		public static readonly string TAG_CONFIG_VALUE = "ConfigValue"; //Table ConfigValue field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>GlobalConfigID is a Property in the GlobalConfig Class of type long</summary>
		public long GlobalConfigID 
		{
			get{return _lGlobalConfigID;}
			set{_lGlobalConfigID = value;}
		}
		/// <summary>DateCreated is a Property in the GlobalConfig Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the GlobalConfig Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>Code is a Property in the GlobalConfig Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the GlobalConfig Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the GlobalConfig Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>ConfigValue is a Property in the GlobalConfig Class of type String</summary>
		public string ConfigValue 
		{
			get{return _strConfigValue;}
			set{_strConfigValue = value;}
		}
		/// <summary>HasError Property in class GlobalConfig and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class GlobalConfig and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>GlobalConfig empty constructor</summary>
		public GlobalConfig()
		{
		}
		/// <summary>GlobalConfig Constructor takes string pStrData and Config</summary>
		public GlobalConfig(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the GlobalConfig Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + GlobalConfigID.ToString() + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_CONFIG_VALUE + ":  " + ConfigValue + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of GlobalConfig</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<GlobalConfig>\n");
			sbReturn.Append("<" + TAG_ID + ">" + GlobalConfigID + "</" + TAG_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_CONFIG_VALUE + ">" + ConfigValue + "</" + TAG_CONFIG_VALUE + ">\n");
			sbReturn.Append("</GlobalConfig>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				GlobalConfigID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONFIG_VALUE);
				ConfigValue = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF GlobalConfig CLASS FILE


