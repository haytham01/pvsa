using System;
using System.Xml;
using System.Text;
using System.Collections;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestSubject.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumTestSubject
	{
		//attributes
		/// <summary>TestSubjectID Attribute type long</summary>
		private long _lTestSubjectID = 0;
		/// <summary>AddressID Attribute type long</summary>
		private long _lAddressID = 0;
		/// <summary>BeginDateCreated Attribute type DateTime</summary>
		private DateTime _dtBeginDateCreated = dtNull;
		/// <summary>EndDateCreated Attribute type DateTime</summary>
		private DateTime _dtEndDateCreated = dtNull;
		/// <summary>BeginDateModified Attribute type DateTime</summary>
		private DateTime _dtBeginDateModified = dtNull;
		/// <summary>EndDateModified Attribute type DateTime</summary>
		private DateTime _dtEndDateModified = dtNull;
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		private string _strDriverLicenseNumber = null;
		/// <summary>FirstName Attribute type string</summary>
		private string _strFirstName = null;
		/// <summary>MiddleName Attribute type string</summary>
		private string _strMiddleName = null;
		/// <summary>LastName Attribute type string</summary>
		private string _strLastName = null;
		/// <summary>PrimaryPhone Attribute type string</summary>
		private string _strPrimaryPhone = null;
		/// <summary>EmailAddress Attribute type string</summary>
		private string _strEmailAddress = null;
		/// <summary>Code Attribute type string</summary>
		private string _strCode = null;
		/// <summary>IdentificationRawData Attribute type string</summary>
		private string _strIdentificationRawData = null;
		/// <summary>Notes Attribute type string</summary>
		private string _strNotes = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumTestSubject"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "TestSubjectID"; //Attribute id  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Table AddressID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Table DateCreated field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Table DateModified field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Table DateModified field name
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		public static readonly string TAG_DRIVER_LICENSE_NUMBER = "DriverLicenseNumber"; //Table DriverLicenseNumber field name
		/// <summary>FirstName Attribute type string</summary>
		public static readonly string TAG_FIRST_NAME = "FirstName"; //Table FirstName field name
		/// <summary>MiddleName Attribute type string</summary>
		public static readonly string TAG_MIDDLE_NAME = "MiddleName"; //Table MiddleName field name
		/// <summary>LastName Attribute type string</summary>
		public static readonly string TAG_LAST_NAME = "LastName"; //Table LastName field name
		/// <summary>PrimaryPhone Attribute type string</summary>
		public static readonly string TAG_PRIMARY_PHONE = "PrimaryPhone"; //Table PrimaryPhone field name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Table EmailAddress field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>IdentificationRawData Attribute type string</summary>
		public static readonly string TAG_IDENTIFICATION_RAW_DATA = "IdentificationRawData"; //Table IdentificationRawData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>TestSubjectID is a Property in the TestSubject Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>AddressID is a Property in the TestSubject Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>BeginDateCreated is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime BeginDateCreated 
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>EndDateCreated is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime EndDateCreated 
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>BeginDateModified is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime BeginDateModified 
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>EndDateModified is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime EndDateModified 
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>DriverLicenseNumber is a Property in the TestSubject Class of type String</summary>
		public string DriverLicenseNumber 
		{
			get{return _strDriverLicenseNumber;}
			set{_strDriverLicenseNumber = value;}
		}
		/// <summary>FirstName is a Property in the TestSubject Class of type String</summary>
		public string FirstName 
		{
			get{return _strFirstName;}
			set{_strFirstName = value;}
		}
		/// <summary>MiddleName is a Property in the TestSubject Class of type String</summary>
		public string MiddleName 
		{
			get{return _strMiddleName;}
			set{_strMiddleName = value;}
		}
		/// <summary>LastName is a Property in the TestSubject Class of type String</summary>
		public string LastName 
		{
			get{return _strLastName;}
			set{_strLastName = value;}
		}
		/// <summary>PrimaryPhone is a Property in the TestSubject Class of type String</summary>
		public string PrimaryPhone 
		{
			get{return _strPrimaryPhone;}
			set{_strPrimaryPhone = value;}
		}
		/// <summary>EmailAddress is a Property in the TestSubject Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>Code is a Property in the TestSubject Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>IdentificationRawData is a Property in the TestSubject Class of type String</summary>
		public string IdentificationRawData 
		{
			get{return _strIdentificationRawData;}
			set{_strIdentificationRawData = value;}
		}
		/// <summary>Notes is a Property in the TestSubject Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}
		/// <summary>HasError Property in class TestSubject and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestSubject and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>TestSubject empty constructor</summary>
		public EnumTestSubject()
		{
		}
		/// <summary>TestSubject Constructor takes string pStrData and Config</summary>
		public EnumTestSubject(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestSubject Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestSubjectID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_DRIVER_LICENSE_NUMBER + ":  " + DriverLicenseNumber + "\n");
			sbReturn.Append(TAG_FIRST_NAME + ":  " + FirstName + "\n");
			sbReturn.Append(TAG_MIDDLE_NAME + ":  " + MiddleName + "\n");
			sbReturn.Append(TAG_LAST_NAME + ":  " + LastName + "\n");
			sbReturn.Append(TAG_PRIMARY_PHONE + ":  " + PrimaryPhone + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_IDENTIFICATION_RAW_DATA + ":  " + IdentificationRawData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestSubject</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestSubjectID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_DRIVER_LICENSE_NUMBER + ">" + DriverLicenseNumber + "</" + TAG_DRIVER_LICENSE_NUMBER + ">\n");
			sbReturn.Append("<" + TAG_FIRST_NAME + ">" + FirstName + "</" + TAG_FIRST_NAME + ">\n");
			sbReturn.Append("<" + TAG_MIDDLE_NAME + ">" + MiddleName + "</" + TAG_MIDDLE_NAME + ">\n");
			sbReturn.Append("<" + TAG_LAST_NAME + ">" + LastName + "</" + TAG_LAST_NAME + ">\n");
			sbReturn.Append("<" + TAG_PRIMARY_PHONE + ">" + PrimaryPhone + "</" + TAG_PRIMARY_PHONE + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_IDENTIFICATION_RAW_DATA + ">" + IdentificationRawData + "</" + TAG_IDENTIFICATION_RAW_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
/// <summary>Parses XML, puts in arraylist</summary>
public static void ParseToTestSubjectArrayList(string pStrXml, ArrayList pRefArrayList)
		{
			try
			{
				TestSubject test_subject = null;
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + TestSubject.ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					if (pRefArrayList != null)
					{
						test_subject = new TestSubject();
						test_subject.Parse(xNode);
						pRefArrayList.Add(test_subject);
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestSubjectID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DRIVER_LICENSE_NUMBER);
				DriverLicenseNumber = xResultNode.InnerText;
				if (DriverLicenseNumber.Trim().Length == 0)
					DriverLicenseNumber = null;
			}
			catch  
			{
				DriverLicenseNumber = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_FIRST_NAME);
				FirstName = xResultNode.InnerText;
				if (FirstName.Trim().Length == 0)
					FirstName = null;
			}
			catch  
			{
				FirstName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MIDDLE_NAME);
				MiddleName = xResultNode.InnerText;
				if (MiddleName.Trim().Length == 0)
					MiddleName = null;
			}
			catch  
			{
				MiddleName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_LAST_NAME);
				LastName = xResultNode.InnerText;
				if (LastName.Trim().Length == 0)
					LastName = null;
			}
			catch  
			{
				LastName = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PRIMARY_PHONE);
				PrimaryPhone = xResultNode.InnerText;
				if (PrimaryPhone.Trim().Length == 0)
					PrimaryPhone = null;
			}
			catch  
			{
				PrimaryPhone = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
				if (EmailAddress.Trim().Length == 0)
					EmailAddress = null;
			}
			catch  
			{
				EmailAddress = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
				if (Code.Trim().Length == 0)
					Code = null;
			}
			catch  
			{
				Code = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IDENTIFICATION_RAW_DATA);
				IdentificationRawData = xResultNode.InnerText;
				if (IdentificationRawData.Trim().Length == 0)
					IdentificationRawData = null;
			}
			catch  
			{
				IdentificationRawData = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
				if (Notes.Trim().Length == 0)
					Notes = null;
			}
			catch  
			{
				Notes = null;
			}
		}
	}
}

//END OF TestSubject CLASS FILE


