using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Merchant.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     12/27/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Merchant database table.
	/// </summary>
	public class Merchant
	{
		//attributes
		/// <summary>MerchantID Attribute type long</summary>
		private long _lMerchantID = 0;
		/// <summary>AddressID Attribute type long</summary>
		private long _lAddressID = 0;
		/// <summary>DateCreated Attribute type DateTime</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type DateTime</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>IsDeactivated Attribute type bool</summary>
		private bool? _bIsDeactivated = null;
		/// <summary>PhonePrimary Attribute type string</summary>
		private string _strPhonePrimary = null;
		/// <summary>PhoneFax Attribute type string</summary>
		private string _strPhoneFax = null;
		/// <summary>MerchantName Attribute type string</summary>
		private string _strMerchantName = null;
		/// <summary>EmailAddress Attribute type string</summary>
		private string _strEmailAddress = null;
		/// <summary>WebsiteLink Attribute type string</summary>
		private string _strWebsiteLink = null;
		/// <summary>ContactName Attribute type string</summary>
		private string _strContactName = null;
		/// <summary>ContactPhone Attribute type string</summary>
		private string _strContactPhone = null;
		/// <summary>ContactEmail Attribute type string</summary>
		private string _strContactEmail = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "Merchant"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "MerchantID"; //Attribute id  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Table AddressID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>IsDeactivated Attribute type string</summary>
		public static readonly string TAG_IS_DEACTIVATED = "IsDeactivated"; //Table IsDeactivated field name
		/// <summary>PhonePrimary Attribute type string</summary>
		public static readonly string TAG_PHONE_PRIMARY = "PhonePrimary"; //Table PhonePrimary field name
		/// <summary>PhoneFax Attribute type string</summary>
		public static readonly string TAG_PHONE_FAX = "PhoneFax"; //Table PhoneFax field name
		/// <summary>MerchantName Attribute type string</summary>
		public static readonly string TAG_MERCHANT_NAME = "MerchantName"; //Table MerchantName field name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Table EmailAddress field name
		/// <summary>WebsiteLink Attribute type string</summary>
		public static readonly string TAG_WEBSITE_LINK = "WebsiteLink"; //Table WebsiteLink field name
		/// <summary>ContactName Attribute type string</summary>
		public static readonly string TAG_CONTACT_NAME = "ContactName"; //Table ContactName field name
		/// <summary>ContactPhone Attribute type string</summary>
		public static readonly string TAG_CONTACT_PHONE = "ContactPhone"; //Table ContactPhone field name
		/// <summary>ContactEmail Attribute type string</summary>
		public static readonly string TAG_CONTACT_EMAIL = "ContactEmail"; //Table ContactEmail field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>MerchantID is a Property in the Merchant Class of type long</summary>
		public long MerchantID 
		{
			get{return _lMerchantID;}
			set{_lMerchantID = value;}
		}
		/// <summary>AddressID is a Property in the Merchant Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>DateCreated is a Property in the Merchant Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Merchant Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>IsDeactivated is a Property in the Merchant Class of type bool</summary>
		public bool? IsDeactivated 
		{
			get{return _bIsDeactivated;}
			set{_bIsDeactivated = value;}
		}
		/// <summary>PhonePrimary is a Property in the Merchant Class of type String</summary>
		public string PhonePrimary 
		{
			get{return _strPhonePrimary;}
			set{_strPhonePrimary = value;}
		}
		/// <summary>PhoneFax is a Property in the Merchant Class of type String</summary>
		public string PhoneFax 
		{
			get{return _strPhoneFax;}
			set{_strPhoneFax = value;}
		}
		/// <summary>MerchantName is a Property in the Merchant Class of type String</summary>
		public string MerchantName 
		{
			get{return _strMerchantName;}
			set{_strMerchantName = value;}
		}
		/// <summary>EmailAddress is a Property in the Merchant Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>WebsiteLink is a Property in the Merchant Class of type String</summary>
		public string WebsiteLink 
		{
			get{return _strWebsiteLink;}
			set{_strWebsiteLink = value;}
		}
		/// <summary>ContactName is a Property in the Merchant Class of type String</summary>
		public string ContactName 
		{
			get{return _strContactName;}
			set{_strContactName = value;}
		}
		/// <summary>ContactPhone is a Property in the Merchant Class of type String</summary>
		public string ContactPhone 
		{
			get{return _strContactPhone;}
			set{_strContactPhone = value;}
		}
		/// <summary>ContactEmail is a Property in the Merchant Class of type String</summary>
		public string ContactEmail 
		{
			get{return _strContactEmail;}
			set{_strContactEmail = value;}
		}
		/// <summary>HasError Property in class Merchant and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Merchant and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>Merchant empty constructor</summary>
		public Merchant()
		{
		}
		/// <summary>Merchant Constructor takes string pStrData and Config</summary>
		public Merchant(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Merchant Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + MerchantID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_IS_DEACTIVATED + ":  " + IsDeactivated + "\n");
			sbReturn.Append(TAG_PHONE_PRIMARY + ":  " + PhonePrimary + "\n");
			sbReturn.Append(TAG_PHONE_FAX + ":  " + PhoneFax + "\n");
			sbReturn.Append(TAG_MERCHANT_NAME + ":  " + MerchantName + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_WEBSITE_LINK + ":  " + WebsiteLink + "\n");
			sbReturn.Append(TAG_CONTACT_NAME + ":  " + ContactName + "\n");
			sbReturn.Append(TAG_CONTACT_PHONE + ":  " + ContactPhone + "\n");
			sbReturn.Append(TAG_CONTACT_EMAIL + ":  " + ContactEmail + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Merchant</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Merchant>\n");
			sbReturn.Append("<" + TAG_ID + ">" + MerchantID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_IS_DEACTIVATED + ">" + IsDeactivated + "</" + TAG_IS_DEACTIVATED + ">\n");
			sbReturn.Append("<" + TAG_PHONE_PRIMARY + ">" + PhonePrimary + "</" + TAG_PHONE_PRIMARY + ">\n");
			sbReturn.Append("<" + TAG_PHONE_FAX + ">" + PhoneFax + "</" + TAG_PHONE_FAX + ">\n");
			sbReturn.Append("<" + TAG_MERCHANT_NAME + ">" + MerchantName + "</" + TAG_MERCHANT_NAME + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_WEBSITE_LINK + ">" + WebsiteLink + "</" + TAG_WEBSITE_LINK + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_NAME + ">" + ContactName + "</" + TAG_CONTACT_NAME + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_PHONE + ">" + ContactPhone + "</" + TAG_CONTACT_PHONE + ">\n");
			sbReturn.Append("<" + TAG_CONTACT_EMAIL + ">" + ContactEmail + "</" + TAG_CONTACT_EMAIL + ">\n");
			sbReturn.Append("</Merchant>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				MerchantID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IS_DEACTIVATED);
				IsDeactivated = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			IsDeactivated = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_PRIMARY);
				PhonePrimary = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PHONE_FAX);
				PhoneFax = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MERCHANT_NAME);
				MerchantName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_WEBSITE_LINK);
				WebsiteLink = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_NAME);
				ContactName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_PHONE);
				ContactPhone = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CONTACT_EMAIL);
				ContactEmail = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF Merchant CLASS FILE


