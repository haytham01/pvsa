using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestSubject.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the TestSubject database table.
	/// </summary>
	public class TestSubject
	{
		//attributes
		/// <summary>TestSubjectID Attribute type long</summary>
		private long _lTestSubjectID = 0;
		/// <summary>AddressID Attribute type long</summary>
		private long _lAddressID = 0;
		/// <summary>DateCreated Attribute type DateTime</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type DateTime</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		private string _strDriverLicenseNumber = null;
		/// <summary>FirstName Attribute type string</summary>
		private string _strFirstName = null;
		/// <summary>MiddleName Attribute type string</summary>
		private string _strMiddleName = null;
		/// <summary>LastName Attribute type string</summary>
		private string _strLastName = null;
		/// <summary>PrimaryPhone Attribute type string</summary>
		private string _strPrimaryPhone = null;
		/// <summary>EmailAddress Attribute type string</summary>
		private string _strEmailAddress = null;
		/// <summary>Code Attribute type string</summary>
		private string _strCode = null;
		/// <summary>IdentificationRawData Attribute type string</summary>
		private string _strIdentificationRawData = null;
		/// <summary>Notes Attribute type string</summary>
		private string _strNotes = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "TestSubject"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "TestSubjectID"; //Attribute id  name
		/// <summary>AddressID Attribute type string</summary>
		public static readonly string TAG_ADDRESS_ID = "AddressID"; //Table AddressID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DriverLicenseNumber Attribute type string</summary>
		public static readonly string TAG_DRIVER_LICENSE_NUMBER = "DriverLicenseNumber"; //Table DriverLicenseNumber field name
		/// <summary>FirstName Attribute type string</summary>
		public static readonly string TAG_FIRST_NAME = "FirstName"; //Table FirstName field name
		/// <summary>MiddleName Attribute type string</summary>
		public static readonly string TAG_MIDDLE_NAME = "MiddleName"; //Table MiddleName field name
		/// <summary>LastName Attribute type string</summary>
		public static readonly string TAG_LAST_NAME = "LastName"; //Table LastName field name
		/// <summary>PrimaryPhone Attribute type string</summary>
		public static readonly string TAG_PRIMARY_PHONE = "PrimaryPhone"; //Table PrimaryPhone field name
		/// <summary>EmailAddress Attribute type string</summary>
		public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress"; //Table EmailAddress field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>IdentificationRawData Attribute type string</summary>
		public static readonly string TAG_IDENTIFICATION_RAW_DATA = "IdentificationRawData"; //Table IdentificationRawData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>TestSubjectID is a Property in the TestSubject Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>AddressID is a Property in the TestSubject Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>DateCreated is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the TestSubject Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DriverLicenseNumber is a Property in the TestSubject Class of type String</summary>
		public string DriverLicenseNumber 
		{
			get{return _strDriverLicenseNumber;}
			set{_strDriverLicenseNumber = value;}
		}
		/// <summary>FirstName is a Property in the TestSubject Class of type String</summary>
		public string FirstName 
		{
			get{return _strFirstName;}
			set{_strFirstName = value;}
		}
		/// <summary>MiddleName is a Property in the TestSubject Class of type String</summary>
		public string MiddleName 
		{
			get{return _strMiddleName;}
			set{_strMiddleName = value;}
		}
		/// <summary>LastName is a Property in the TestSubject Class of type String</summary>
		public string LastName 
		{
			get{return _strLastName;}
			set{_strLastName = value;}
		}
		/// <summary>PrimaryPhone is a Property in the TestSubject Class of type String</summary>
		public string PrimaryPhone 
		{
			get{return _strPrimaryPhone;}
			set{_strPrimaryPhone = value;}
		}
		/// <summary>EmailAddress is a Property in the TestSubject Class of type String</summary>
		public string EmailAddress 
		{
			get{return _strEmailAddress;}
			set{_strEmailAddress = value;}
		}
		/// <summary>Code is a Property in the TestSubject Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>IdentificationRawData is a Property in the TestSubject Class of type String</summary>
		public string IdentificationRawData 
		{
			get{return _strIdentificationRawData;}
			set{_strIdentificationRawData = value;}
		}
		/// <summary>Notes is a Property in the TestSubject Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}
		/// <summary>HasError Property in class TestSubject and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestSubject and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>TestSubject empty constructor</summary>
		public TestSubject()
		{
		}
		/// <summary>TestSubject Constructor takes string pStrData and Config</summary>
		public TestSubject(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestSubject Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestSubjectID.ToString() + "\n");
			sbReturn.Append(TAG_ADDRESS_ID + ":  " + AddressID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_DRIVER_LICENSE_NUMBER + ":  " + DriverLicenseNumber + "\n");
			sbReturn.Append(TAG_FIRST_NAME + ":  " + FirstName + "\n");
			sbReturn.Append(TAG_MIDDLE_NAME + ":  " + MiddleName + "\n");
			sbReturn.Append(TAG_LAST_NAME + ":  " + LastName + "\n");
			sbReturn.Append(TAG_PRIMARY_PHONE + ":  " + PrimaryPhone + "\n");
			sbReturn.Append(TAG_EMAIL_ADDRESS + ":  " + EmailAddress + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_IDENTIFICATION_RAW_DATA + ":  " + IdentificationRawData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestSubject</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<TestSubject>\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestSubjectID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_ADDRESS_ID + ">" + AddressID + "</" + TAG_ADDRESS_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_DRIVER_LICENSE_NUMBER + ">" + DriverLicenseNumber + "</" + TAG_DRIVER_LICENSE_NUMBER + ">\n");
			sbReturn.Append("<" + TAG_FIRST_NAME + ">" + FirstName + "</" + TAG_FIRST_NAME + ">\n");
			sbReturn.Append("<" + TAG_MIDDLE_NAME + ">" + MiddleName + "</" + TAG_MIDDLE_NAME + ">\n");
			sbReturn.Append("<" + TAG_LAST_NAME + ">" + LastName + "</" + TAG_LAST_NAME + ">\n");
			sbReturn.Append("<" + TAG_PRIMARY_PHONE + ">" + PrimaryPhone + "</" + TAG_PRIMARY_PHONE + ">\n");
			sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_IDENTIFICATION_RAW_DATA + ">" + IdentificationRawData + "</" + TAG_IDENTIFICATION_RAW_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</TestSubject>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestSubjectID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ADDRESS_ID);
				AddressID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			AddressID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DRIVER_LICENSE_NUMBER);
				DriverLicenseNumber = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_FIRST_NAME);
				FirstName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_MIDDLE_NAME);
				MiddleName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_LAST_NAME);
				LastName = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_PRIMARY_PHONE);
				PrimaryPhone = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
				EmailAddress = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_IDENTIFICATION_RAW_DATA);
				IdentificationRawData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF TestSubject CLASS FILE


