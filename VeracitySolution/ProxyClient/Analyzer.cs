﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Veracity.Client.Proxy
{




    public class Anaylyzer18Questions : BaseAnalyzer
    {

        //////////////////////////////////////////////////////////////////////////////////
        ///////
        ///////  18 Questions Formula
        ///////
        //////////////////////////////////////////////////////////////////////////////////

        // test analysis formula
        public void AnalysisTest18Questions(ref TestSession pTestSession, ref string gResultsFolderPath )
        {
            UpdateTSL18QValues(ref pTestSession, ref gResultsFolderPath);

        }

        public void CalculateRDTimeT18(ref TestSession pTestSession)
        {

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // do your formulas and populate
                    //tmpAvgRDTime = tmpAvgRDTime + ( screen.ReadTimeInSeconds * 100 );
                    dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);
                    listRDTime.Add(dRDTime[i]);
                    //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);
                    // listRDTime.Add(screen.ReadTimeInSeconds * 100);
                }


            }

        }

        public void Load18Questions(ref TestSession pTestSession)
        {
            CalculateRDTimeT18(ref pTestSession); // verified
            CalculateZAxis18(ref pTestSession); // verified
            CalculateSumXPlusZ18(ref pTestSession); // verified  - XXX DIFFERENT VS FORMULA 26
            CalculateAbsXMinusZ18(ref pTestSession); // verified
            CalculateZMinusX18(ref pTestSession); // verified - XXX DIFFERENT VS FORMULA 26
            CalcualteSumDToJColumn18(ref pTestSession); // verified
            CalculateSUMAvgT18(ref pTestSession); // Calculate the Averages for the REACTION DATA

            //
            // 20180515 OVERRIDE THE AVERAGE AND GET THE MEDIUM
            // per phil

            CalculateGetMedianT18(ref pTestSession);

            //bTurnOnVerifyDistortLogic = true;
            if (bTurnOnVerifyDistortLogic)
            {
                VerifyIfWeHaveADistortInReactionData18(ref pTestSession);
            }

            // getStandardDeviation18(); // no longer need this, we use the getStandardDevlist now
            // getStandardDeviationReadTime18(); // no longer need this, we use the getStandardDevlist now
            CalculateMultipleSPRT18(ref pTestSession);
            

            // Lets calcualte the reaction information
            CalculateGreaterTSLHigh18(ref pTestSession);
            CalculateLessTSLLow18(ref pTestSession);
            CalculateWeight18(ref pTestSession);
            CalculateWeightAnswer18(ref pTestSession);
            CalculateWeightTotal18(ref pTestSession);
            CalculateReactionSPR18(ref pTestSession);
            CalculateReadReactionDistortion18(ref pTestSession);
            CalculateZoneWeightTotal18(ref pTestSession);

            CalculateReadTimeAverages18(ref pTestSession);
            CalculateReadTimeAvgT18(ref pTestSession); // Different from above, Calculate the Read Time Averages
        }

        public void UpdateTSL18QValues(ref TestSession pTestSession, ref string gResultsFolderPath )
        {
            // load the configuration into the text box
            dTSLAdjust18Q--;  // grab the % TSLHi and TSLLo

            // now update the TSLHi with the percentage
            dTSLLow = ((100 - dTSLAdjust18Q) / 100);

            // now update the TSLLow with the percentage
            dTSLHIGH = ((100 + dTSLAdjust18Q) / 100);

            TSLPercentage = Convert.ToInt16(dTSLAdjust18Q);

            // Run the Anaylysis and count the number of hits
            Load18Questions(ref pTestSession);

        }



        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  GET RISK ANALYSIS SUMMARY - 18 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////

        public void GetRiskAnalysis18QSummary(ref TestSession testSession)
        {


            // FOR THE NEW RISKS SCREEN and FORMULA
            // category risk declaration
            string strIssueACatogoryRisk = "None";
            string strIssueBCatogoryRisk = "None";
            string strIssueCCatogoryRisk = "None";
            bool IssueACatogoryAdmitsSetup = false;
            bool IssueBCatogoryAdmitsSetup = false;
            bool IssueCCatogoryAdmitsSetup = false;
            bool IssueACatogoryAdmitsSecondary = false;
            bool IssueBCatogoryAdmitsSecondary = false;
            bool IssueCCatogoryAdmitsSecondary = false;
            bool IssueACatogoryAdmitsPrimary = false;
            bool IssueBCatogoryAdmitsPrimary = false;
            bool IssueCCatogoryAdmitsPrimary = false;

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            //string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            //string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            //string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            //string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            //string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            //int ReactionZone6Counter1 = 0;
            //int ReactionZone6Counter2 = 0;
            //int ReactionZone6Counter3 = 0;
            //string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            //int ReactionZone7Counter1 = 0;
            //int ReactionZone7Counter2 = 0;
            //int ReactionZone7Counter3 = 0;
            //string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            //string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            //string strRiskAnalysis = "Low Risk";
            //int iRiskAnalysisTotalAdmits = 0;
            //int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;

           


            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 17:
                        case 20:
                        case 24:
                        case 25:
                            iRiskAnalysisTotalAdmits--;
                            break;
                        default:
                            break;
                    }
                }

                // for the NEW code from PHIL
                // AS OF SEPT 2015
                // this overrides the RISK Analyaiss
                // Will be using the Catogory Admits instead
                if (screen.Answer)
                {
                    switch (i)
                    {
                        // Issue A
                        case 5:
                            IssueACatogoryAdmitsSetup = true;
                            break;
                        case 6:
                            IssueACatogoryAdmitsSecondary = true;
                            break;
                        case 7:
                            IssueACatogoryAdmitsPrimary = true;
                            break;
                        // Issue B
                        case 9:
                            IssueBCatogoryAdmitsSetup = true;
                            break;
                        case 10:
                            IssueBCatogoryAdmitsSecondary = true;
                            break;
                        case 11:
                            IssueBCatogoryAdmitsPrimary = true;
                            break;
                        // Issue C
                        case 13:
                            IssueCCatogoryAdmitsSetup = true;
                            break;
                        case 14:
                            IssueCCatogoryAdmitsSecondary = true;
                            break;
                        case 15:
                            IssueCCatogoryAdmitsPrimary = true;
                            break;
                        default:
                            break;
                    }

                }


                switch (i)
                {

                    case 5: // questions 3,4,5
                    case 6: // questions 3,4,5
                    case 7: // questions 3,4,5
                        if (i == 5)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 9: // questions 6,7,8
                    case 10: // questions 6,7,8
                    case 11: // questions 6,7,8
                        if (i == 9)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 11)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 13: // questions 9,10,11
                    case 14: // questions 9,10,11
                    case 15: // questions 9,10,11
                        if (i == 13)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 14)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 17: // questions 12,13,14
                    case 18: // questions 12,13,14
                    case 19: // questions 12,13,14
                        if (i == 17)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                    case 23: // questions 3,4,5
                        if (i == 21)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 23)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    default:

                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No or low risk detected";
                strRiskAnalysis = "LOW";
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "At least one area of concern";
                    strRiskAnalysis = "CAUTION";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Secondary screening is Necessary";
                strRiskAnalysis = "HIGH";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Secondary screening is Recommended";
                strRiskAnalysis = "HIGH";
            }




            //////////////////////////////////////////////////////////////
            // NEW LOGIC per Phil Sprague 20150830
            // CALCULATE THE RISK per Issue
            //
            // WARNING THIS OVERRIDES THE RISK ANALYSYS
            //
            // Category Risk 
            // NONE
            // CAUTION
            // HIGH
            //
            // defined as are:
            //
            //
            // NONE- if 0 SPR then CategoryRisk is NONE
            // LOW - if 1 SPR in the SETUP question then Category Risk is LOW
            // CAUTION - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is CAUTION
            // CAUTION - if 1 SPR in the SECONDARY question then CAUTION
            // CAUTION - if 1 SPR in the PRIMARY question then CAUTION
            // HIGH - if 1 SPR PRIMARY and ( 1 SPR in SETUP OR 1 SPR in SECONDARY ) then HIGH
            // HIGH - if ALL SPR then HIGH
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            // NOTE AS OF 10/10/2016 - Phil Changed the logic
            // 0 = Low
            // 1 = Low+
            // 2 = Caution
            // 3 = High
            // 4 = High+
            // 5 or greater than = very high
            //
            //
            // 0 - LOW if 0 SPR then CategoryRisk is NONE
            // 1 - LOW - if 1 SPR in the SETUP question then Category Risk is LOW
            // 3 - HIGH - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is HIGH
            // 2- CAUTION - if 1 SPR in the SECONDARY question then CAUTION
            // 4 - HIGH - if 1 SPR in the PRIMARY question then CAUTION
            // 5- VERY HIGH - if 1 SPR PRIMARY and ( 1 SPR in SETUP OR 1 SPR in SECONDARY ) then HIGH
            // 6 - VERY HIGH - if ALL SPR then HIGH
            //
            //
            //
            //
            // NOTE AS OF 04/07/2017 - phil change the logic
            //
            // 0 - LOW if 0 SPR then CategoryRisk is NONE, GREEN
            // 1 - LOW# - if 1 SPR in the SETUP question then Category Risk is LOW, BLUE
            // 3 - CAUTION# - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is CAUTION#, YELLOW
            // 2-  CAUTION - if 1 SPR in the SECONDARY question then CAUTION, YELLOW
            // 3 - CAUTION# - if 1 SPR in the PRIMARY question then CAUTION#, YELLOW
            // 4 - ELEVATED - if 1 SPR PRIMARY and ( 1 SPR in SETUP ) then ELEVATED, ORANGE
            // 5-  HIGH - if 1 SPR PRIMARY and ( 1 SPR in SECONDARY ) then HIGH, RED
            // 6 - HIGH# - if ALL SPR then HIGH, RED
            //
            //
            // ADMITS
            // POI  - SETUP question admit
            // POI# - SECONDARY question admit
            // HIGH# - PRIMARY question admit
            ///////////////////////////////////////////////////////////////

            // lets check for the first category, check for none cases
            if ((ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3) == 0)
            {
                strIssueACatogoryRisk = "LOW";

            }
            // check for low cases,
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "LOW#";

            }
            // check for caution cases, 1st case
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "CAUTION#";

            }
            // check for caution cases, 2nd case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "CAUTION";

            }
            // check for caution cases, third case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "CAUTION#";

            }

            // check for HIGH cases, 1st case
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "ELEVATED";

            }
            // check for HIGH cases, 1st case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "HIGH";

            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "HIGH#";

            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueACatogoryAdmitsSetup == true))
            {

                strIssueACatogoryRisk = strIssueACatogoryRisk + ", POI";
                

            }
            if ( (IssueACatogoryAdmitsSecondary == true))
            {

                strIssueACatogoryRisk = strIssueACatogoryRisk + ", POI#";


            }
            if (IssueACatogoryAdmitsPrimary == true)
            {
                strIssueACatogoryRisk = "HIGH#";

            }



            // lets check for the SECOND category, check for none cases
            if ((ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3) == 0)
            {
                strIssueBCatogoryRisk = "LOW";

            }
            // check for low cases,
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "LOW#";

            }
            // check for caution cases, 1st case
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "CAUTION#";

            }
            // check for caution cases, 2nd case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "CAUTION";

            }
            // check for caution cases, third case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "CAUTION#";

            }

            // check for HIGH cases, 1st case
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "ELEVATED";

            }
            // check for HIGH cases, 1st case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "HIGH";

            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "HIGH#";

            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueBCatogoryAdmitsSetup == true))
            {

                strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", POI";

            }
            if ((IssueBCatogoryAdmitsSecondary == true))
            {

                strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", POI#";

            }
            if (IssueBCatogoryAdmitsPrimary == true)
            {
                strIssueBCatogoryRisk = "HIGH#";

            }




            // lets check for the THIRD category, check for none cases

            if ((ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3) == 0)
            {
                strIssueCCatogoryRisk = "LOW";

            }
            // check for low cases,
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "LOW#";

            }
            // check for caution cases, 1st case
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "CAUTION#";

            }
            // check for caution cases, 2nd case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "CAUTION";

            }
            // check for caution cases, third case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "CAUTION#";

            }

            // check for HIGH cases, 1st case
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "ELEVATED";

            }
            // check for HIGH cases, 1st case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "HIGH";

            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "HIGH#";

            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueCCatogoryAdmitsSetup == true))
            {
                strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", POI";

            }
            if ((IssueCCatogoryAdmitsSecondary == true))
            {
                strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", POI#";

            }
            if (IssueCCatogoryAdmitsPrimary == true)
            {
                strIssueCCatogoryRisk = "HIGH#";

            }


            // send the data to the RISK SCREEN
            strRiskAnalysisIssueA = strIssueACatogoryRisk;
            // send the data to the RISK SCREEN
            strRiskAnalysisIssueB = strIssueBCatogoryRisk;
            // send the data to the RISK SCREEN
            strRiskAnalysisIssueC = strIssueCCatogoryRisk;


        }




        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  FUZZY LOGIC - 18 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        public void AnalysisTest18AutoAdjust(ref TestSession pTestSession, ref string gResultsFolderPath)
        {
            Veracity.Client.Proxy.Screen screen = null;
            //ReactionZone1Counter = 0;
            //ReactionZone1Counter1 = 0;
            //ReactionZone1Counter2 = 0;
            //ReactionZone1Counter3 = 0;
            //ReactionZone2Counter = 0;
            //ReactionZone2Counter1 = 0;
            //ReactionZone2Counter2 = 0;
            //ReactionZone2Counter3 = 0;
            //ReactionZone3Counter = 0;
            //ReactionZone3Counter1 = 0;
            //ReactionZone3Counter2 = 0;
            //ReactionZone3Counter3 = 0;
            //ReactionZone4Counter = 0;
            //ReactionZone4Counter1 = 0;
            //ReactionZone4Counter2 = 0;
            //ReactionZone4Counter3 = 0;
            //ReactionZone5Counter = 0;
            //ReactionZone5Counter1 = 0;
            //ReactionZone5Counter2 = 0;
            //ReactionZone5Counter3 = 0;
            //ReactionZone6Counter = 0;
            //ReactionZone6Counter1 = 0;
            //ReactionZone6Counter2 = 0;
            //ReactionZone6Counter3 = 0;
            //ReactionZone7Counter = 0;
            //ReactionZone7Counter1 = 0;
            //ReactionZone7Counter2 = 0;
            //ReactionZone7Counter3 = 0;

            for (int j = 0; j != 100; j++)
            {

                /////////////////////////////////////////////////////
                // There are two ways to determine TO DO THE AUTO ADJUST
                //
                // NOTE: AS OF JUNE 2014, if we STOP WITH NO SPRS, then continue until TSL MINIMUM
                //
                // When there two 2 SPR and not counting ADMIT, we stop
                // OR
                // STOP have at least 1 SPR in each catagory/sections
                //
                // 18 questions to determine to leave the loop
                //
                // NOTE: AS OF JUNE 2014, if we STOP WITH NO SPRS, then continue until TSL MINIMUM
                // NOTE: AS OF AUG 20 2014, we create a FLOOR????
                /////////////////////////////////////////////////////


                // quit if we have the number of SPRS has been meet
                if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) >= eTotalNumReaction)
                {
                    j = 100;
                    break;
                }

                // quit if we meet the minimum
                // NEW CONDITION:
                //     If we meet the minimum but the number of SPRS has not been meet, then continue to the floor.]
                //bTSLFloorOn = true;
                if (bTSLFloorOn == false)
                {
                    if (dTSLAdjust18Q == eTSLMinimum)
                    {
                        j = 100;
                        break;
                    }
                }
                else
                {

                    if (dTSLAdjust18Q >= eTSLMinimum)
                    {
                        if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) < eTotalNumReaction)
                        {
                            // lets continue on if the number of SPRS is still not meet
                        }
                        else
                        {
                            j = 100;
                            break;
                        }
                    }

                    // Third condition:
                    // Only goes here if the minimum has not been meet and the number of SPRs has not been meet
                    // but definitly stop here
                    if (dTSLAdjust18Q == eTSLFloor)
                    {
                        j = 100;
                        break;
                    }
                }

                UpdateTSL18QValues(ref pTestSession, ref gResultsFolderPath ); // try the next value for the TSLAdjust



                //////////////////////////////////////////////////////////////////
                //////  We updated the calculations and check if we got any hits
                //////////////////////////////////////////////////////////////////
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Veracity.Client.Proxy.Screen)pTestSession.lstScreenQuestionOnly[i];

                    switch (i)
                    {

                        case 5: // questions 5,6,7
                        case 6: // questions 5,6,7
                        case 7: // questions 5,6,7
                            if (i == 5)
                            {
                                if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter1++;
                                }
                            }
                            if (i == 6)
                            {
                                if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter2++;
                                }
                            }
                            if (i == 7)
                            {
                                if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter3++;
                                }
                            }

                            ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                            break;
                        case 9: // questions 6,7,8
                        case 10: // questions 6,7,8
                        case 11: // questions 6,7,8
                            if (i == 9)
                            {
                                if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter1++;
                                }
                            }
                            if (i == 10)
                            {
                                if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter2++;
                                }
                            }
                            if (i == 11)
                            {
                                if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter3++;
                                }
                            }

                            ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                            break;
                        case 13: // questions 9,10,11
                        case 14: // questions 9,10,11
                        case 15: // questions 9,10,11
                            if (i == 13)
                            {
                                if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter1++;
                                }
                            }
                            if (i == 14)
                            {
                                if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter2++;
                                }
                            }
                            if (i == 15)
                            {
                                if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter3++;
                                }
                            }

                            ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                            break;
                        case 17: // questions 12,13,14
                        case 18: // questions 12,13,14
                        case 19: // questions 12,13,14
                            if (i == 17)
                            {
                                if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter1++;
                                }
                            }
                            if (i == 18)
                            {
                                if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter2++;
                                }
                            }
                            if (i == 19)
                            {
                                if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter3++;
                                }
                            }

                            ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                            break;
                        case 21: // questions 3,4,5
                        case 22: // questions 3,4,5
                        case 23: // questions 3,4,5
                            if (i == 21)
                            {
                                if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter1++;
                                }
                            }
                            if (i == 22)
                            {
                                if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter2++;
                                }
                            }
                            if (i == 23)
                            {
                                if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter3++;
                                }
                            }

                            ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                            break;
                        //case 17: // questions 3,4,5
                        //case 18: // questions 3,4,5
                        //case 19: // questions 3,4,5
                        //    if (i == 17)
                        //    {
                        //        if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter1++;
                        //        }
                        //    }
                        //    if (i == 18)
                        //    {
                        //        if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter2++;
                        //        }
                        //    }
                        //    if (i == 19)
                        //    {
                        //        if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter3++;
                        //        }
                        //    }

                        //    ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                        //    break;
                        //case 20: // questions 3,4,5
                        //case 21: // questions 3,4,5
                        //case 22: // questions 3,4,5
                        //    if (i == 20)
                        //    {
                        //        if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter1++;
                        //        }
                        //    }
                        //    if (i == 21)
                        //    {
                        //        if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter2++;
                        //        }
                        //    }
                        //    if (i == 22)
                        //    {
                        //        if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter3++;
                        //        }
                        //    }

                        //    ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                        //    break;
                        default:
                            break;
                    }


                }
            }
        }

        public void CalculateZAxis18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dZTime[i] = (double)(screen.ZAxisData);
                }
            }

        }

        public void CalculateSumXPlusZ18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    //dSumXZ[i] = (((double)(dRDTime[i] + dZTime[i])) / 2);
                    dSumXZ[i] = (((double)(dRDTime[i] + dZTime[i])));  // # DIFFERENT VS FORMULA 26
                }
            }
        }

        public void CalculateAbsXMinusZ18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dAbsXZ[i] = Math.Abs(dRDTime[i] - dZTime[i]);
                }
            }
        }

        public void CalculateZMinusX18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // dSubZX[i] = (double)(dZTime[i] - dRDTime[i]);
                    dSubZX[i] = (double)(dZTime[i] + dRDTime[i]); // XXX DIFFERENT VS Q26
                }
            }
        }

        public void CalcualteSumDToJColumn18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];

                    // dRDTime = D column
                    // dZTime = Z column
                    // dSumXZ = X + Z column
                    // dAbsXZ = abs(X-Z) column
                    // dSubZX = Z-X column

                    dSumEI[i] = (double)((dRDTime[i] + dZTime[i] + dSumXZ[i] + dAbsXZ[i] + dSubZX[i]));
                    if (i >= 0)  // Dont Skip the first 2 question
                    {
                        listSumEI.Add(dSumEI[i]);  // this is used for the standard deviation for the 18 question
                    }
                }
            }
        }


        public void CalculateReadTimeAverages18(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions 3 to 15
            int j = 0;
            double Sum2 = 0;  // for questions 16 to 25
            int k = 0;

            // Calculate the Read Time Total

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeTotal18 = 0;
                // question 1 to 28
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;

                }
                dReadTimeTotal18 = Sum1;
            }

            // Calculate the Read Time Average
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg18 = 0;
                Sum1 = 0;
                // question 1 to 28
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    j++;
                }
                dReadTimeAvg18 = (double)(Sum1 / (j));
            }

            // Calculate the Read Time Relevant Average
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeRelevantAvg18 = 0;
                Sum1 = 0;
                // question 6,7,8,12,13,14,16,17,18,20,21,22,24,25,26 - 15 questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 5:
                        case 6:
                        case 7:
                        case 11:
                        case 12:
                        case 13:
                        case 15:
                        case 16:
                        case 17:
                        case 19:
                        case 20:
                        case 21:
                        case 23:
                        case 24:
                        case 25:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }

                }
                dReadTimeRelevantAvg18 = (double)(Sum1 / (15));
            }

            // Calculate the Read Time IR Average or BASE questions
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeIRAvg18 = 0;
                Sum1 = 0;
                // question 5,9,13,17,21,25 - 6 questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeIRAvg18 = (double)(Sum1 / (6));
            }



        }


        public void CalculateReadTimeAvgT18(ref TestSession pTestSession)
        {


            int i = 0;
            int iIndex = 0; // use this if we need are 0 based or not
            int iBaseLineAvgIndex = 0; // used for the base line average index
            Screen screen = null;
            int x = 0;
            int y = 0;


            // Calculate TEST Average, questions 5 to 17.  dont include last question
            dReadTimeAvg[0] = 0;
            //i = 0;
            //for (int j = 4; j < (pTestSession.lstScreenQuestionOnly.Count - 1); j++)
            //{
            //    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
            //    dReadTimeAvg[0] += screen.DeltaClickTimeInSeconds;
            //    i++;
            //}

            // 3/8/15 per phil, change the read time average using only the BASELINE
            // Question 6,7,8,10,11,12,14,15,16
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 5: // Q6
                        case 6: // Q7
                        case 7: // Q8
                        case 9: // Q10
                        case 10: // Q11
                        case 11: // Q12
                        case 13: // Q14
                        case 14: // Q15
                        case 15: // Q16
                            dReadTimeAvg[0] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }
                }
             
            }

            dReadTimeAvg[iBaseLineAvgIndex++] = (double)(dReadTimeAvg[0] / 9);



            //
            ////////////////////////////////////////////////////
            //
            // Calculate the BASELINE AVERAGES for each section
            //
            ////////////////////////////////////////////////////
            //

            // Base Average Q5, Q9, Q13, Q17
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8] + dSumEI[iIndex + 12] + dSumEI[iIndex + 16]) / 4;
            //dBaseLineAvg[1] = dReadTimeAvg[1];
            x = 1;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                // question 5,9 - 4 questions
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }




            // Base Average Q5 + Q9
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8]) / 2;
            //dBaseLineAvg[2] = dReadTimeAvg[2];
            x = 2;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                // question 5,9 - 4 questions
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 4:
                        case 8:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }

            // Base Average Q9 + Q13
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 8] + dSumEI[iIndex + 12]) / 2;
            //dBaseLineAvg[3] = dReadTimeAvg[3];
            x = 3;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 8:
                        case 12:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }

            // Base Average Q13 + Q17
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 12] + dSumEI[iIndex + 16]) / 2;
            //dBaseLineAvg[4] = dReadTimeAvg[4];
            x = 4;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 12:
                        case 16:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }

            // Base Average Q17 + Q21
            dReadTimeAvg[5] = 0;


            // Base Average Q21 + Q25
            dReadTimeAvg[6] = 0;


            //
            //////////////////////////////////////
            //
            // Calculate the averages for each section
            //
            //////////////////////////////////////
            //

            // Calculate Day of PROGRAM INTENT Questions 6 to 8 Line 
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 5] + dSumEI[iIndex + 6] + dSumEI[iIndex + 7]) / 3;
            x = 7;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 5:
                        case 6:
                        case 7:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }

            // Calculate ALCOHOL LAST MONTH - Question 10 - 12
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 9] + dSumEI[iIndex + 10] + dSumEI[iIndex + 11]) / 3;
            x = 8;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 9:
                        case 10:
                        case 11:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }

            // Calculate DRUGS LAST MONTH Question 14 - 16
            //dReadTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 13] + dSumEI[iIndex + 14] + dSumEI[iIndex + 15]) / 3;
            x = 9;
            y = 0;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg[x] = 0;
                for (int j = 0; j < pTestSession.lstScreenQuestionOnly.Count; j++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                    switch (j)
                    {
                        case 13:
                        case 14:
                        case 15:
                            y++;
                            dReadTimeAvg[x] += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }


                }
                dReadTimeAvg[x] = (double)(dReadTimeAvg[x] / (y));
            }


            // Calculate Day of Week - Arrest/Convictions - Question 18 - 20
            dReadTimeAvg[10] = 0;

            // Calculate Month of Year - Smuggling - Question 22 - 24
            dReadTimeAvg[11] = 0;

            //// Calculate TSL High
            //dReadTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLHIGH;
            //dTSLHiSPR[0] = dReadTimeAvg[0] * dTSLHIGH;
            dReadTimeAvg[12] = (dReadTimeAvg[0] * dTSLHIGH);

            //// Calculate TSL Low
            //dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLLow;
            //dTSLLowSPR[0] = dReadTimeAvg[0] * dTSLLow;
            dReadTimeAvg[13] = (dReadTimeAvg[0] * dTSLLow);

            // index = 14, Average of the base line 
            dReadTimeAvg[14] = (( dReadTimeAvg[7] + dReadTimeAvg[8] + dReadTimeAvg[9]) / 3 );

            // index = 15, Average of the base line 
            dReadTimeAvg[15] = ((dReadTimeAvg[8] + dReadTimeAvg[9] + dReadTimeAvg[10]) / 3);

            // index = 16, Average of the base line 
            dReadTimeAvg[16] = ((dReadTimeAvg[9] + dReadTimeAvg[10] + dReadTimeAvg[11]) / 3);

        }



        public void CalculateMultipleSPRT18(ref TestSession pTestSession)
        {

            List<double> templistRDTime = new List<double>();

            // Calculate the first SPR2, SPR3, SPR4, SPR5, SPR6, SPR7 for TSL Hi and TSL Low
            for (int i = 1; i <= 6; i++)
            {
                dTSLHiSPR[i] = dBaseLineAvg[i] * dTSLHIGH;
                dTSLLowSPR[i] = dBaseLineAvg[i] * dTSLLow;
            }


            // Calculate STD for SPR 1, Create a list for Q5 to Q26
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[5]);
            templistRDTime.Add(dSumEI[6]);
            templistRDTime.Add(dSumEI[7]);
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[9]);
            templistRDTime.Add(dSumEI[10]);
            templistRDTime.Add(dSumEI[11]);
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[13]);
            templistRDTime.Add(dSumEI[14]);
            templistRDTime.Add(dSumEI[15]);
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[17]);
            templistRDTime.Add(dSumEI[18]);
            templistRDTime.Add(dSumEI[19]);
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[21]);
            templistRDTime.Add(dSumEI[22]);
            templistRDTime.Add(dSumEI[23]);
            templistRDTime.Add(dSumEI[24]);
            templistRDTime.Add(dSumEI[25]);
            dSTDSPR[0] = getStandardDeviationList(templistRDTime);
            StandardDeviation = dSTDSPR[0];

            // Calculate STD for SPR 2, Create a list for Q5, Q9, Q13, Q17, Q21, Q25
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[24]);
            dSTDSPR[1] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 3, Create a list for Q5, Q9
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[8]);
            dSTDSPR[2] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 4, Create a list for Q9, Q13
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[12]);
            dSTDSPR[3] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 5, Create a list for Q13, Q17
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[16]);
            dSTDSPR[4] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 6, Create a list for Q17, Q21
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[20]);
            dSTDSPR[5] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 7, Create a list for Q21, Q25
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[24]);
            dSTDSPR[6] = getStandardDeviationList(templistRDTime);



            // Calculate STD for Read Time, Create a list Q5 to Q17
            templistRDTime.Clear();
            Screen screen = null;
            for (int j = 4; j < (pTestSession.lstScreenQuestionOnly.Count - 1); j++)
            {
                screen = (Screen)pTestSession.lstScreenQuestionOnly[j];
                templistRDTime.Add(screen.DeltaClickTimeInSeconds);
            }
            dSTDSPR[7] = getStandardDeviationList(templistRDTime);
            StandardDeviationReadTime = dSTDSPR[7];




        }


        // 20180515 Carl per phil, instead of the average, calculate the median

        public void CalculateGetMedianT18(ref TestSession pTestSession)
        {
            double[] tempArray = new double[15];
            int x = 0;
            int count = 15; // only 15 questions
            int iIndex = 0;
            double mediumValue = 0.0;

            // array init
            tempArray[x] = dSumEI[iIndex + 3]; x++; // question 4
            tempArray[x] = dSumEI[iIndex + 4]; x++; // question 5
            tempArray[x] = dSumEI[iIndex + 5]; x++; // question 6
            tempArray[x] = dSumEI[iIndex + 6]; x++; // question 7
            tempArray[x] = dSumEI[iIndex + 7]; x++; // question 8
            tempArray[x] = dSumEI[iIndex + 8]; x++; // question 9
            tempArray[x] = dSumEI[iIndex + 9]; x++; // question 10
            tempArray[x] = dSumEI[iIndex + 10]; x++; // question 11
            tempArray[x] = dSumEI[iIndex + 11]; x++; // question 12
            tempArray[x] = dSumEI[iIndex + 12]; x++; // question 13
            tempArray[x] = dSumEI[iIndex + 13]; x++; // question 14
            tempArray[x] = dSumEI[iIndex + 14]; x++; // question 15
            tempArray[x] = dSumEI[iIndex + 15]; x++; // question 16
            tempArray[x] = dSumEI[iIndex + 16]; x++; // question 17
            tempArray[x] = dSumEI[iIndex + 17]; x++; // question 18

            // sort
            Array.Sort(tempArray);

            // calculate the medium if odd number or even number
            if ( count % 2 == 0)
            {
                // count is even, need to get the middle two elements, add them together, then divide by 2
                double middleElement1 = tempArray[(count / 2) - 1];
                double middleElement2 = tempArray[(count / 2)];
                mediumValue = (middleElement1 + middleElement2) / 2;

            }
            else
            {
                // count is odd, simple get the middle element
                mediumValue = tempArray[(count / 2)];
            }


            dRDTimeAvg[0] = mediumValue;



        }


            public void CalculateSUMAvgT18(ref TestSession pTestSession)
        {


            int i = 0;
            int iIndex = 0; // use this if we need are 0 based or not
            int iBaseLineAvgIndex = 0; // used for the base line average index
            double dHighValue = 0;// used for the moving test average, ignore the highest value
            double dLowValue = 100 ; // used for the moving test average, ignore the lowest value


            // Calculate TEST Average, questions 5 to 25.  dont include last question
          
            dRDTimeAvg[0] = 0;
            //for (int j = 4; j < ( pTestSession.lstScreenQuestionOnly.Count - 1); j++)
            //{
            //    i++;
            //    dRDTimeAvg[0] += dSumEI[j];
            //}

            ////////////////////////////////////////////////////////////////////////////////
            // 3/8/15 per Phil, change the TestAverage to be only using the base line avg
            // Use only Q6,Q7, Q8, Q10, Q11, Q12, Q14, Q15, Q16
            //
            // As of 10/21/15 added a check box option
            // per Phil
            if (bTurnOnMovingTestAverage == true)
            {
                i = 9;
                dRDTimeAvg[0] = (dSumEI[iIndex + 5] + dSumEI[iIndex + 6] + dSumEI[iIndex + 7] +
                                 dSumEI[iIndex + 9] + dSumEI[iIndex + 10] + dSumEI[iIndex + 11] +
                                 dSumEI[iIndex + 13] + dSumEI[iIndex + 14] + dSumEI[iIndex + 15]
                                 );
                dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] / i;
            }
            else
            {

                /////////////////////////////////////////////////////////////////////////////////////
                // 10/21/15 per Phil via James.  The new TestAverage is a moving average
                // we will first need to find the Highest and Lowest number in the base line avg
                // remove that from the average and calculate the new average based from the 7 averages

                // first find out which one is the highest and lowest values
                // Use only Q6,Q7, Q8, Q10, Q11, Q12, Q14, Q15, Q16

                for (int xIndex = 5; xIndex <= 15; xIndex++)
                {
                    switch (xIndex)
                    {
                        // these are the base questions
                        case 5:
                        case 6:
                        case 7:
                        case 9:
                        case 10:
                        case 11:
                        case 13:
                        case 14:
                        case 15:
                            if (dSumEI[iIndex + xIndex] > dHighValue)
                            {
                                dHighValue = dSumEI[iIndex + xIndex];
                            }
                            if (dSumEI[iIndex + xIndex] < dLowValue)
                            {
                                dLowValue = dSumEI[iIndex + xIndex];
                            }
                            break;
                        default:
                            break;
                    }

                }

                // now go ahead and get the sum but subtract the high and low

                dRDTimeAvg[0] = (dSumEI[iIndex + 5] + dSumEI[iIndex + 6] + dSumEI[iIndex + 7] +
                                 dSumEI[iIndex + 9] + dSumEI[iIndex + 10] + dSumEI[iIndex + 11] +
                                 dSumEI[iIndex + 13] + dSumEI[iIndex + 14] + dSumEI[iIndex + 15] - dHighValue - dLowValue);

                // now go ahead and divide by 7
                i = 7;
                dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] / i;

                // END OF THE NEW TEST AVERAGE that we called the MOVING AVERAGE
                /////////////////////////////////////////////////////////////////////////////////

                ////
                //// 20180515 OVERRIDE THE AVERAGE AND GET THE MEDIUM
                //// per phil

                //CalculateGetMedianT18(pTestSession);

            }



            //
            ////////////////////////////////////////////////////
            //
            // Calculate the BASELINE AVERAGES for each section
            //
            ////////////////////////////////////////////////////
            //

            // Base Average Q5, Q9, Q13, Q17

            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8] + dSumEI[iIndex + 12] + dSumEI[iIndex + 16] ) / 4;
            dBaseLineAvg[1] = dRDTimeAvg[1];

            // Base Average Q5 + Q9
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8]) / 2;
            dBaseLineAvg[2] = dRDTimeAvg[2];

            // Base Average Q9 + Q13
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 8] + dSumEI[iIndex + 12]) / 2;
            dBaseLineAvg[3] = dRDTimeAvg[3];

            // Base Average Q13 + Q17
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 12] + dSumEI[iIndex + 16]) / 2;
            dBaseLineAvg[4] = dRDTimeAvg[4];

            // Base Average Q17 + Q21
            dRDTimeAvg[iBaseLineAvgIndex++] = 0;
            dBaseLineAvg[5] = dRDTimeAvg[5];

            // Base Average Q21 + Q25
            dRDTimeAvg[iBaseLineAvgIndex++] = 0;
            dBaseLineAvg[6] = dRDTimeAvg[6];

            //
            //////////////////////////////////////
            //
            // Calculate the averages for each section
            //
            //////////////////////////////////////
            //

            // Calculate Day of Week Accidents Reports Questions 6 to 8 Line 
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 5] + dSumEI[iIndex + 6] + dSumEI[iIndex + 7]) / 3;

            // Calculate Month of YEAR - DRUGS Question 10 - 12
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 9] + dSumEI[iIndex + 10] + dSumEI[iIndex + 11]) / 3;

            // Calculate Year - SEX OFFENDER Question 14 - 16
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 13] + dSumEI[iIndex + 14] + dSumEI[iIndex + 15]) / 3;

            // Calculate Day of Week - Arrest/Convictions - Question 18 - 20
            dRDTimeAvg[iBaseLineAvgIndex++] = 0;

            // Calculate Month of Year - Smuggling - Question 22 - 24
            dRDTimeAvg[iBaseLineAvgIndex++] = 0;

            // Calculate TSL High
            dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLHIGH;
            dTSLHiSPR[0] = dRDTimeAvg[0] * dTSLHIGH;

            // Calculate TSL Low
            dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLLow;
            dTSLLowSPR[0] = dRDTimeAvg[0] * dTSLLow;

            // index = 14, Relative Questions Base line Averages
            dRDTimeAvg[iBaseLineAvgIndex++] = ( dRDTimeAvg[7] + dRDTimeAvg[8] + dRDTimeAvg[9] ) /3;

            // index = 15, Relative Questions Base line Averages
            dRDTimeAvg[iBaseLineAvgIndex++] = (dRDTimeAvg[8] + dRDTimeAvg[9] + dRDTimeAvg[10]) / 3;

            // index = 16, Relative Questions Base line Averages
            dRDTimeAvg[iBaseLineAvgIndex++] = (dRDTimeAvg[9] + dRDTimeAvg[10] + dRDTimeAvg[11]) / 3;

        }


        // SPECIAL CASE
        //
        // If we got any data that is higher than the average with configurable multiplier, then we need to put the average
        // based on the zones
        // We have to do this to protect the integrity of the software
        // 
        public void VerifyIfWeHaveADistortInReactionData18(ref TestSession pTestSession)
        {

            double dMultipler = dTestAvgMultiplierCode;
            double dMultiplierAvg = 0;

            int iZone1LastQuestion = 7;  // question 5 to 8
            int iZone2LastQuestion = 11; // question 9 to 12
            int iZone3LastQuestion = 15; // question 13 to 16
            int iZone4LastQuestion = 19; // question 17 to 20
            int iZone5LastQuestion = 23; // question 21 to 24

            int iZone1Average = 2;
            int iZone2Average = 3;
            int iZone3Average = 4;
            int iZone4Average = 5;
            int iZone5Average = 6;

            double dZone1TempAverage = 0;
            double dZone2TempAverage = 0;
            double dZone3TempAverage = 0;
            double dZone4TempAverage = 0;
            double dZone5TempAverage = 0;
            int iCount = 0;

            bool bRunTheLogic = bRunTheTestAvgMultiplierCode;




            int iRecalculateData = 1; // check for any recalculation
            Array.Clear(QuestionThatWasDistortDuringMultipleCodeLogic18, 0, QuestionThatWasDistortDuringMultipleCodeLogic18.Length);

            for (int iNumberOfRecalculate = 0; ((dMultipler > 0) && (bRunTheLogic) && (iNumberOfRecalculate < pTestSession.lstScreenQuestionOnly.Count) && (iRecalculateData != 0)); iNumberOfRecalculate++)
            {
                iRecalculateData = 0; // set it up as no issue, later in the code it MAY find issues!

                // zone 1 - 
                // check each section and see if there is any bad data
                // first calculate the new average WITHOUT THE BAD DATA
                iCount = 0;
                for (int j = 4; ((j != iZone1LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone1Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone1TempAverage += dSumEI[j];
                    }

                }
                dZone1TempAverage = dZone1TempAverage / iCount;

                for (int j = 4; ((j != iZone1LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone1Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone1TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic18[j] = 1;
                    }


                }


                // zone 2 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone2TempAverage += dSumEI[j];
                    }

                }
                dZone2TempAverage = dZone2TempAverage / iCount;

                for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone2TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic18[j] = 1;
                    }

                }
                //for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone2Average];
                //        iRecalculateData = 1;
                //    }

                //}

                // zone 3 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone3TempAverage += dSumEI[j];
                    }

                }
                dZone3TempAverage = dZone3TempAverage / iCount;

                for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone3TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic18[j] = 1;
                    }

                }
                //for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone3Average];
                //        iRecalculateData = 1;
                //    }

                //}

                // zone 4 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone4TempAverage += dSumEI[j];
                    }

                }
                dZone4TempAverage = dZone4TempAverage / iCount;

                for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone4TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic18[j] = 1;
                    }

                }

                //for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone4Average];
                //        iRecalculateData = 1;
                //    }

                //}


                // zone 5 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone5TempAverage += dSumEI[j];
                    }

                }
                dZone5TempAverage = dZone5TempAverage / iCount;

                for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone5TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic18[j] = 1;
                    }

                }
                //for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone5Average];
                //        iRecalculateData = 1;
                //    }

                //}

                // recalculate the data if we need to
                if (iRecalculateData > 0)
                {
                    CalculateSUMAvgT18(ref pTestSession);
                    //
                    // 20180515 OVERRIDE THE AVERAGE AND GET THE MEDIUM
                    // per phil

                    CalculateGetMedianT18(ref pTestSession);
                }

            } // main loop

        }

        public double getStandardDeviationList(List<double> doubleList)
        {
            //double average = doubleList.Average();
            //double sumOfDerivation = 0;
            //foreach (double value in doubleList)
            //{
            //    sumOfDerivation += (value) * (value);
            //}
            //double sumOfDerivationAverage = sumOfDerivation / doubleList.Count;
            //return Math.Sqrt(sumOfDerivationAverage - (average * average));

            double average = doubleList.Average();
            double sumOfDerivation = 0;
            foreach (double value in doubleList)
            {
                sumOfDerivation += Math.Pow(((double)value - average), 2);
            }

            return Math.Sqrt(sumOfDerivation / (doubleList.Count - 1));
        }

        public void getStandardDeviation18()
        {
            double SumOfSqrs = 0;
            double avg = listSumEI[0];

            StandardDeviation = 0;
            for (int i = 4; i < listSumEI.Count(); i++)
            {

                SumOfSqrs += Math.Pow(((double)listSumEI[i] - avg), 2);
            }
            double n = (double)listSumEI.Count();
            StandardDeviation = Math.Sqrt(SumOfSqrs / (n - 1));
            //dSTDSPR[0] = StandardDeviation;


        }

        public void getStandardDeviationReadTime18()
        {
            double SumOfSqrs = 0;
            double avg = listSumEI[0];

            StandardDeviationReadTime = 0;
            for (int i = 4; i < listSumEI.Count(); i++)
            {

                SumOfSqrs += Math.Pow(((double)listSumEI[i] - avg), 2);
            }
            double n = (double)listSumEI.Count();
            StandardDeviationReadTime = Math.Sqrt(SumOfSqrs / (n - 1));
            //dSTDSPR[0] = StandardDeviation;


        }


        public void CalculateGreaterTSLHigh18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // SPR1
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[0]) { bGreaterTSLHighSPR1[i] = 1; }
                    else { bGreaterTSLHighSPR1[i] = 0; }
                }



                // SPR2
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[1]) { bGreaterTSLHighSPR2[i] = 1; }
                    else { bGreaterTSLHighSPR2[i] = 0; }
                }


                // SPR3
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[2]) { bGreaterTSLHighSPR3[i] = 1; }
                    else { bGreaterTSLHighSPR3[i] = 0; }
                }


                // SPR4
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[3]) { bGreaterTSLHighSPR4[i] = 1; }
                    else { bGreaterTSLHighSPR4[i] = 0; }
                }


                // SPR5
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[4]) { bGreaterTSLHighSPR5[i] = 1; }
                    else { bGreaterTSLHighSPR5[i] = 0; }
                }


                // SPR6
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[5]) { bGreaterTSLHighSPR6[i] = 1; }
                    else { bGreaterTSLHighSPR6[i] = 0; }
                }


                // SPR7
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[6]) { bGreaterTSLHighSPR7[i] = 1; }
                    else { bGreaterTSLHighSPR7[i] = 0; }
                }


            }
        }

        public void CalculateLessTSLLow18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // SPR1
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[0]) { bGreaterTSLLowSPR1[i] = 1; }
                    else { bGreaterTSLLowSPR1[i] = 0; }
                }

                // SPR2
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[1]) { bGreaterTSLLowSPR2[i] = 1; }
                    else { bGreaterTSLLowSPR2[i] = 0; }
                }




                // SPR3
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[2]) { bGreaterTSLLowSPR3[i] = 1; }
                    else { bGreaterTSLLowSPR3[i] = 0; }
                }



                // SPR4
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[3]) { bGreaterTSLLowSPR4[i] = 1; }
                    else { bGreaterTSLLowSPR4[i] = 0; }
                }



                // SPR5
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[4]) { bGreaterTSLLowSPR5[i] = 1; }
                    else { bGreaterTSLLowSPR5[i] = 0; }
                }



                // SPR6
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[5]) { bGreaterTSLLowSPR6[i] = 1; }
                    else { bGreaterTSLLowSPR6[i] = 0; }
                }


                // SPR7
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[6]) { bGreaterTSLLowSPR7[i] = 1; }
                    else { bGreaterTSLLowSPR7[i] = 0; }
                }
            }
        }

        public void CalculateWeight18(ref TestSession pTestSession)
        {
            int iWeightTotal = 0;

            // bTurnOffExtraSPRWeightCalculation = true;

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    switch (i)
                    {
                        // question 6, 7, 8
                        case 5:
                        case 6:
                        case 7:
                            if (bTurnOnSPR1 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            }
                            if (bTurnOnSPR2 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i];
                            }
                            if (bTurnOnSPR3 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            }

                            //if (bTurnOffExtraSPRWeightCalculation == false)
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            //}
                            //else
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            //}
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                        // question 10, 11, 12
                        case 9:
                        case 10:
                        case 11:
                            //if (bTurnOffExtraSPRWeightCalculation == false)
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR4[i] + bGreaterTSLLowSPR4[i];
                            //}
                            //else
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            //}
                            if (bTurnOnSPR1 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            }
                            if (bTurnOnSPR2 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i];
                            }
                            if (bTurnOnSPR3 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            }
                             
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                        // question 14, 15, 16

                        case 13:
                        case 14:
                        case 15:
                            //if (bTurnOffExtraSPRWeightCalculation == false)
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR5[i] + bGreaterTSLLowSPR5[i];
                            //}
                            //else
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            //}

                            if (bTurnOnSPR1 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            }
                            if (bTurnOnSPR2 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i];
                            }
                            if (bTurnOnSPR3 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            }
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                        // question 18, 19, 20
                        case 17:
                        case 18:
                        case 19:
                            //if (bTurnOffExtraSPRWeightCalculation == false)
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR6[i] + bGreaterTSLLowSPR6[i];
                            //}
                            //else
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            //}

                            if (bTurnOnSPR1 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            }
                            if (bTurnOnSPR2 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i];
                            }
                            if (bTurnOnSPR3 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            }
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;
                        // question 22, 23, 24
                        case 21:
                        case 22:
                        case 23:
                            //if (bTurnOffExtraSPRWeightCalculation == false)
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR7[i] + bGreaterTSLLowSPR7[i];
                            //}
                            //else
                            //{
                            //    iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            //}

                            if (bTurnOnSPR1 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i];
                            }
                            if (bTurnOnSPR2 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i];
                            }
                            if (bTurnOnSPR3 == true)
                            {
                                iWeightTotal = bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            }
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                        default:
                            break;
                    }

                }
            }
        }

        public void CalculateWeightAnswer18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    if (screen.Answer)
                    {
                        iWeightAnswer[i] = 3;
                    }
                    else
                    {
                        iWeightAnswer[i] = 0;
                    }


                    // thses are the questions that are not suppose to be ADMITS
                    switch (i)
                    {

                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:

                            if (screen.Answer)
                            {
                                iWeightAnswer[i] = 0; // if answer yes, then this is ok
                            }
                            else
                            {
                                iWeightAnswer[i] = 2; // if answer no to these question, maybe be fooling system
                            }
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 25:
                            iWeightAnswer[i] = 0; // we dont care about this answer
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void CalculateWeightTotal18(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    iWeightTotal[i] = Weight[i] + iWeightAnswer[i];

                    // thses are the questions that are not suppose to be ADMITS
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                        case 25:
                        case 18:
                            iWeightTotal[i] = 0;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void CalculateZoneWeightTotal18(ref TestSession pTestSession)
        {

            int iZoneCount = 0;
            int iWeightCount = 2;
            // for Zone 1, Questions 3/5
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 2, Questions 6/8
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 3, Questions 9/11
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 4, Questions 12/14
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 5, Questions 15/17
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 6, Questions 18/20
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 7, Questions 21/23
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];



        }




        public void CalculateReactionSPR18(ref TestSession pTestSession)
        {
            Screen screen = null;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // XXX
                    // TEMPorary Change the Weight Total to Looked at the Reaction Data INSTEAD
                    if (iWeightTotal[i] > 0)
                    {
                        screen.IsHit = true;

                        //sReactionSPR[i] = "SPR";
                    }
                    //if (dRDTime[i] > 0)
                    //{
                    //    screen.IsHit = true;
                    //    //sReactionSPR[i] = "SPR";
                    //}

                    else
                    {
                        screen.IsHit = null;
                        //sReactionSPR[i] = "";
                    }
                }
            }
        }

        public void CalculateReadReactionDistortion18(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions textBoxReadRatio2
            int j = 0;
            double Sum2 = 0;  // for questions textBoxReadRatio3
            int k = 0;

            // Calculate the Read Reactions 
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Calculate the 1st Reactions questions (T7+T11+T15+T19+T23+T27)/6
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 6:
                        case 10:
                        case 14:
                        case 18:
                        case 22:
                        case 26:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            j++;
                            break;
                        default:
                            break;
                    }


                }
                ReadReaction1 = (double)(Sum1 / (j));

                // Calcualte the 2nd Reactions question (T8+T9+T10+T12+T14+T16+T17+T18+T20+T21+T22+T24+T25+T30)/14
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 7:
                        case 8:
                        case 9:
                        case 11:
                        case 13:
                        case 15:
                        case 16:
                        case 17:
                        case 19:
                        case 20:
                        case 21:
                        case 23:
                        case 24:
                        case 29:
                            Sum2 += screen.DeltaClickTimeInSeconds;
                            k++;
                            break;
                        default:
                            break;
                    }
                }
                ReadReaction2 = (double)(Sum2 / k);
            }

            // Calculate the Distortion

            if (ReadReaction1 < (iReadReaction * ReadReaction2))
            {
                Distortion = true;
            }
        }

    }













    public class Anaylyzer26Questions : BaseAnalyzer
    {

        //////////////////////////////////////////////////////////////////////////////////
        ///////
        ///////  26 Questions Formula
        ///////
        //////////////////////////////////////////////////////////////////////////////////

        // test analysis formula
        public void AnalysisTest26Questions(ref TestSession pTestSession)
        {
            UpdateTSL26QValues(ref pTestSession);

        }

        public void CalculateRDTimeT26(ref TestSession pTestSession)
        {

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // do your formulas and populate
                    //tmpAvgRDTime = tmpAvgRDTime + ( screen.ReadTimeInSeconds * 100 );
                    dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);
                    listRDTime.Add(dRDTime[i]);
                    //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);
                    // listRDTime.Add(screen.ReadTimeInSeconds * 100);
                }


            }

        }

        public void Load26Questions(ref TestSession pTestSession)
        {
            CalculateRDTimeT26(ref pTestSession); // verified
            CalculateZAxis26(ref pTestSession); // verified
            CalculateSumXPlusZ26(ref pTestSession); // verified
            CalculateAbsXMinusZ26(ref pTestSession); // verified
            CalculateZMinusX26(ref pTestSession); // verified
            CalcualteSumDToJColumn26(ref pTestSession); // verified
            CalculateSUMAvgT26(ref pTestSession); // Calculate the Averages
            VerifyIfWeHaveADistortInReactionData26(ref pTestSession);
            // getStandardDeviation26(); // no longer need this, we use the getStandardDevlist now
            CalculateMultipleSPRT26(ref pTestSession);

            // Lets calcualte the reaction information
            CalculateGreaterTSLHigh26(ref pTestSession);
            CalculateLessTSLLow26(ref pTestSession);
            CalculateWeight26(ref pTestSession);
            CalculateWeightAnswer26(ref pTestSession);
            CalculateWeightTotal26(ref pTestSession);
            CalculateReactionSPR26(ref pTestSession);
            CalculateReadReactionDistortion26(ref pTestSession);
            CalculateZoneWeightTotal26(ref pTestSession);

            CalculateReadTimeAverages26(ref pTestSession);
        }

        public void UpdateTSL26QValues(ref TestSession pTestSession)
        {
            // load the configuration into the text box
            dTSLAdjust26Q--;  // grab the % TSLHi and TSLLo

            // now update the TSLHi with the percentage
            dTSLLow = ((100 - dTSLAdjust26Q) / 100);

            // now update the TSLLow with the percentage
            dTSLHIGH = ((100 + dTSLAdjust26Q) / 100);

            TSLPercentage = Convert.ToInt16(dTSLAdjust26Q);

            // Run the Anaylysis and count the number of hits
            Load26Questions(ref pTestSession);

        }


        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  FUZZY LOGIC - 26 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        public void AnalysisTest26AutoAdjust(ref TestSession pTestSession)
        {
            Veracity.Client.Proxy.Screen screen = null;
            //ReactionZone1Counter = 0;
            //ReactionZone1Counter1 = 0;
            //ReactionZone1Counter2 = 0;
            //ReactionZone1Counter3 = 0;
            //ReactionZone2Counter = 0;
            //ReactionZone2Counter1 = 0;
            //ReactionZone2Counter2 = 0;
            //ReactionZone2Counter3 = 0;
            //ReactionZone3Counter = 0;
            //ReactionZone3Counter1 = 0;
            //ReactionZone3Counter2 = 0;
            //ReactionZone3Counter3 = 0;
            //ReactionZone4Counter = 0;
            //ReactionZone4Counter1 = 0;
            //ReactionZone4Counter2 = 0;
            //ReactionZone4Counter3 = 0;
            //ReactionZone5Counter = 0;
            //ReactionZone5Counter1 = 0;
            //ReactionZone5Counter2 = 0;
            //ReactionZone5Counter3 = 0;
            //ReactionZone6Counter = 0;
            //ReactionZone6Counter1 = 0;
            //ReactionZone6Counter2 = 0;
            //ReactionZone6Counter3 = 0;
            //ReactionZone7Counter = 0;
            //ReactionZone7Counter1 = 0;
            //ReactionZone7Counter2 = 0;
            //ReactionZone7Counter3 = 0;

            for (int j = 0; j != 100; j++)
            {

                /////////////////////////////////////////////////////
                // There are two ways to determine TO DO THE AUTO ADJUST
                //
                // When there two 2 SPR and not counting ADMIT, we stop
                // OR
                // STOP have at least 1 SPR in each catagory/sections
                //
                // 26 questions to determine to leave the loop
                /////////////////////////////////////////////////////

                if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) >= eTotalNumReaction)
                {
                    j = 100;
                    break;
                }

                if (dTSLAdjust26Q == eTSLMinimum)
                {
                    j = 100;
                    break;
                }

                UpdateTSL26QValues(ref pTestSession); // try the next value for the TSLAdjust



                //////////////////////////////////////////////////////////////////
                //////  We updated the calculations and check if we got any hits
                //////////////////////////////////////////////////////////////////
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Veracity.Client.Proxy.Screen)pTestSession.lstScreenQuestionOnly[i];

                    switch (i)
                    {

                        case 5: // questions 5,6,7
                        case 6: // questions 5,6,7
                        case 7: // questions 5,6,7
                            if (i == 5)
                            {
                                if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter1++;
                                }
                            }
                            if (i == 6)
                            {
                                if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter2++;
                                }
                            }
                            if (i == 7)
                            {
                                if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter3++;
                                }
                            }

                            ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                            break;
                        case 9: // questions 6,7,8
                        case 10: // questions 6,7,8
                        case 11: // questions 6,7,8
                            if (i == 9)
                            {
                                if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter1++;
                                }
                            }
                            if (i == 10)
                            {
                                if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter2++;
                                }
                            }
                            if (i == 11)
                            {
                                if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter3++;
                                }
                            }

                            ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                            break;
                        case 13: // questions 9,10,11
                        case 14: // questions 9,10,11
                        case 15: // questions 9,10,11
                            if (i == 13)
                            {
                                if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter1++;
                                }
                            }
                            if (i == 14)
                            {
                                if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter2++;
                                }
                            }
                            if (i == 15)
                            {
                                if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter3++;
                                }
                            }

                            ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                            break;
                        case 17: // questions 12,13,14
                        case 18: // questions 12,13,14
                        case 19: // questions 12,13,14
                            if (i == 17)
                            {
                                if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter1++;
                                }
                            }
                            if (i == 18)
                            {
                                if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter2++;
                                }
                            }
                            if (i == 19)
                            {
                                if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter3++;
                                }
                            }

                            ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                            break;
                        case 21: // questions 3,4,5
                        case 22: // questions 3,4,5
                        case 23: // questions 3,4,5
                            if (i == 21)
                            {
                                if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter1++;
                                }
                            }
                            if (i == 22)
                            {
                                if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter2++;
                                }
                            }
                            if (i == 23)
                            {
                                if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter3++;
                                }
                            }

                            ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                            break;
                        //case 17: // questions 3,4,5
                        //case 18: // questions 3,4,5
                        //case 19: // questions 3,4,5
                        //    if (i == 17)
                        //    {
                        //        if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter1++;
                        //        }
                        //    }
                        //    if (i == 18)
                        //    {
                        //        if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter2++;
                        //        }
                        //    }
                        //    if (i == 19)
                        //    {
                        //        if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone6Counter3++;
                        //        }
                        //    }

                        //    ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                        //    break;
                        //case 20: // questions 3,4,5
                        //case 21: // questions 3,4,5
                        //case 22: // questions 3,4,5
                        //    if (i == 20)
                        //    {
                        //        if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter1++;
                        //        }
                        //    }
                        //    if (i == 21)
                        //    {
                        //        if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter2++;
                        //        }
                        //    }
                        //    if (i == 22)
                        //    {
                        //        if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                        //        {
                        //            ReactionZone7Counter3++;
                        //        }
                        //    }

                        //    ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                        //    break;
                        default:
                            break;
                    }


                }
            }
        }


        public void CalculateZAxis26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dZTime[i] = (double)(screen.ZAxisData);
                }
            }

        }

        public void CalculateSumXPlusZ26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dSumXZ[i] = (((double)(dRDTime[i] + dZTime[i]))/2);
                }
            }
        }

        public void CalculateAbsXMinusZ26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dAbsXZ[i] = Math.Abs(dRDTime[i] - dZTime[i]);
                }
            }
        }

        public void CalculateZMinusX26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dSubZX[i] = (double)(dZTime[i] - dRDTime[i]);
                }
            }
        }

        public void CalcualteSumDToJColumn26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];

                    // dRDTime = D column
                    // dZTime = Z column
                    // dSumXZ = X + Z column
                    // dAbsXZ = abs(X-Z) column
                    // dSubZX = Z-X column

                    dSumEI[i] = (double)((dRDTime[i] + dZTime[i] + dSumXZ[i] + dAbsXZ[i] + dSubZX[i]));
                    if (i >= 0)  // Dont Skip the first 2 question
                    {
                        listSumEI.Add(dSumEI[i]);  // this is used for the standard deviation for the 26 question
                    }
                }
            }
        }


        public void CalculateReadTimeAverages26(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions 3 to 15
            int j = 0;
            double Sum2 = 0;  // for questions 16 to 25
            int k = 0;

            // Calculate the Read Time Total
            
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeTotal26 = 0;
                // question 1 to 28
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    
                }
                dReadTimeTotal26= Sum1;
            }

            // Calculate the Read Time Average
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeAvg26 = 0;
                Sum1 = 0;
                // question 1 to 28
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    j++;
                }
                dReadTimeAvg26 = (double)(Sum1 / (j));
            }

            // Calculate the Read Time Relevant Average
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeRelevantAvg26 = 0;
                Sum1 = 0;
                // question 6,7,8,12,13,14,16,17,18,20,21,22,24,25,26 - 15 questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 5:
                        case 6:
                        case 7:
                        case 11:
                        case 12:
                        case 13:
                        case 15:
                        case 16:
                        case 17:
                        case 19:
                        case 20:
                        case 21:
                        case 23:
                        case 24:
                        case 25:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }
                    
                }
                dReadTimeRelevantAvg26 = (double)(Sum1 / (15));
            }

            // Calculate the Read Time IR Average or BASE questions
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                dReadTimeIRAvg26 = 0;
                Sum1 = 0;
                // question 5,9,13,17,21,25 - 6 questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            break;
                        default:
                            break;
                    }
                    

                }   
                dReadTimeIRAvg26 = (double)(Sum1 / (6));
            }



        }


        public void CalculateMultipleSPRT26(ref TestSession pTestSession)
        {

            List<double> templistRDTime = new List<double>();

            // Calculate the first SPR2, SPR3, SPR4, SPR5, SPR6, SPR7 for TSL Hi and TSL Low
            for (int i = 1; i <= 6; i++)
            {
                dTSLHiSPR[i] = dBaseLineAvg[i] * dTSLHIGH;
                dTSLLowSPR[i] = dBaseLineAvg[i] * dTSLLow;
            }


            // Calculate STD for SPR 1, Create a list for Q5 to Q26
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[5]);
            templistRDTime.Add(dSumEI[6]);
            templistRDTime.Add(dSumEI[7]);
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[9]);
            templistRDTime.Add(dSumEI[10]);
            templistRDTime.Add(dSumEI[11]);
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[13]);
            templistRDTime.Add(dSumEI[14]);
            templistRDTime.Add(dSumEI[15]);
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[17]);
            templistRDTime.Add(dSumEI[18]);
            templistRDTime.Add(dSumEI[19]);
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[21]);
            templistRDTime.Add(dSumEI[22]);
            templistRDTime.Add(dSumEI[23]);
            templistRDTime.Add(dSumEI[24]);
            templistRDTime.Add(dSumEI[25]);
            dSTDSPR[0] = getStandardDeviationList(templistRDTime);
            StandardDeviation = dSTDSPR[0];

            // Calculate STD for SPR 2, Create a list for Q5, Q9, Q13, Q17, Q21, Q25
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[24]);
            dSTDSPR[1] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 3, Create a list for Q5, Q9
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[4]);
            templistRDTime.Add(dSumEI[8]);
            dSTDSPR[2] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 4, Create a list for Q9, Q13
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[8]);
            templistRDTime.Add(dSumEI[12]);
            dSTDSPR[3] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 5, Create a list for Q13, Q17
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[12]);
            templistRDTime.Add(dSumEI[16]);
            dSTDSPR[4] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 6, Create a list for Q17, Q21
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[16]);
            templistRDTime.Add(dSumEI[20]);
            dSTDSPR[5] = getStandardDeviationList(templistRDTime);

            // Calculate STD for SPR 7, Create a list for Q21, Q25
            templistRDTime.Clear();
            templistRDTime.Add(dSumEI[20]);
            templistRDTime.Add(dSumEI[24]);
            dSTDSPR[6] = getStandardDeviationList(templistRDTime);




        }

        public void CalculateSUMAvgT26(ref TestSession pTestSession)
        {

            
            int i = 0;
            int iIndex = 0 ; // use this if we need are 0 based or not
            int iBaseLineAvgIndex = 0; // used for the base line average index


            // Calculate TEST Average, questions 5 to 25.  dont include last question
            dRDTimeAvg[0] = 0;
            for (int j = 4; j < pTestSession.lstScreenQuestionOnly.Count; j++)
            {
                i++;
                dRDTimeAvg[0] += dSumEI[j];
            }
            dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] / i;



            //
            ////////////////////////////////////////////////////
            //
            // Calculate the BASELINE AVERAGES for each section
            //
            ////////////////////////////////////////////////////
            //

            // Base Average Q5, Q9, Q13, Q17, Q21, Q25
            
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8] + dSumEI[iIndex + 12] + dSumEI[iIndex + 16] + dSumEI[iIndex + 20] + dSumEI[iIndex + 24]) / 6;
            dBaseLineAvg[1] = dRDTimeAvg[1];

            // Base Average Q5 + Q9
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 4] + dSumEI[iIndex + 8]) / 2;
            dBaseLineAvg[2] = dRDTimeAvg[2];

            // Base Average Q9 + Q13
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 8] + dSumEI[iIndex + 12]) / 2;
            dBaseLineAvg[3] = dRDTimeAvg[3];

            // Base Average Q13 + Q17
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 12] + dSumEI[iIndex + 16]) / 2;
            dBaseLineAvg[4] = dRDTimeAvg[4];

            // Base Average Q17 + Q21
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 16] + dSumEI[iIndex + 20]) / 2;
            dBaseLineAvg[5] = dRDTimeAvg[5];

            // Base Average Q21 + Q25
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 20] + dSumEI[iIndex + 24]) / 2;
            dBaseLineAvg[6] = dRDTimeAvg[6];

            //
            //////////////////////////////////////
            //
            // Calculate the averages for each section
            //
            //////////////////////////////////////
            //

            // Calculate Day of Week Accidents Reports Questions 6 to 8 Line 
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 5] + dSumEI[iIndex + 6] + dSumEI[iIndex + 7]) / 3;

            // Calculate Month of YEAR - DRUGS Question 10 - 12
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 9] + dSumEI[iIndex + 10] + dSumEI[iIndex + 11]) / 3;

            // Calculate Year - SEX OFFENDER Question 14 - 16
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 13] + dSumEI[iIndex + 14] + dSumEI[iIndex + 15]) / 3;

            // Calculate Day of Week - Arrest/Convictions - Question 18 - 20
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 17] + dSumEI[iIndex + 18] + dSumEI[iIndex + 19]) / 3;

            // Calculate Month of Year - Smuggling - Question 22 - 24
            dRDTimeAvg[iBaseLineAvgIndex++] = (dSumEI[iIndex + 21] + dSumEI[iIndex + 22] + dSumEI[iIndex + 23]) / 3;

            // Calculate TSL High
            dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLHIGH;
            dTSLHiSPR[0] = dRDTimeAvg[0] * dTSLHIGH;

            // Calculate TSL Low
            dRDTimeAvg[iBaseLineAvgIndex++] = dRDTimeAvg[0] * dTSLLow;
            dTSLLowSPR[0] = dRDTimeAvg[0] * dTSLLow;

        }


        // SPECIAL CASE
        //
        // If we got any data that is higher than the average with configurable multiplier, then we need to put the average
        // based on the zones
        // We have to do this to protect the integrity of the software
        // 
        public void VerifyIfWeHaveADistortInReactionData26(ref TestSession pTestSession)
        {
            
            double dMultipler = dTestAvgMultiplierCode;
            double dMultiplierAvg = 0;

            int iZone1LastQuestion = 7;  // question 5 to 8
            int iZone2LastQuestion = 11; // question 9 to 12
            int iZone3LastQuestion = 15; // question 13 to 16
            int iZone4LastQuestion = 19; // question 17 to 20
            int iZone5LastQuestion = 23; // question 21 to 24

            int iZone1Average = 2;
            int iZone2Average = 3;
            int iZone3Average = 4;
            int iZone4Average = 5;
            int iZone5Average = 6;

            double dZone1TempAverage = 0;
            double dZone2TempAverage = 0;
            double dZone3TempAverage = 0;
            double dZone4TempAverage = 0;
            double dZone5TempAverage = 0;
            int iCount = 0;

            bool bRunTheLogic = bRunTheTestAvgMultiplierCode;

            


            int iRecalculateData = 1; // check for any recalculation
            Array.Clear(QuestionThatWasDistortDuringMultipleCodeLogic26, 0, QuestionThatWasDistortDuringMultipleCodeLogic26.Length);

            for (int iNumberOfRecalculate = 0; ((dMultipler > 0) && (bRunTheLogic) && (iNumberOfRecalculate < pTestSession.lstScreenQuestionOnly.Count) && (iRecalculateData != 0)); iNumberOfRecalculate++)
            {
                iRecalculateData = 0; // set it up as no issue, later in the code it MAY find issues!

                // zone 1 - 
                // check each section and see if there is any bad data
                // first calculate the new average WITHOUT THE BAD DATA
                iCount = 0;
                for ( int j = 4; ((j != iZone1LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone1Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone1TempAverage += dSumEI[j];
                    }

                }
                dZone1TempAverage = dZone1TempAverage / iCount;

                for (int j = 4; ((j != iZone1LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone1Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone1TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic26[j] = 1;
                    }

                    
                }


                // zone 2 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone2TempAverage += dSumEI[j];
                    }

                }
                dZone2TempAverage = dZone2TempAverage / iCount;

                for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone2TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic26[j] = 1;
                    }

                }
                //for (int j = iZone1LastQuestion + 1; ((j != iZone2LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone2Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone2Average];
                //        iRecalculateData = 1;
                //    }
                    
                //}

                // zone 3 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone3TempAverage += dSumEI[j];
                    }

                }
                dZone3TempAverage = dZone3TempAverage / iCount;

                for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone3TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic26[j] = 1;
                    }

                }
                //for (int j = iZone2LastQuestion + 1; ((j != iZone3LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone3Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone3Average];
                //        iRecalculateData = 1;
                //    }
                    
                //}

                // zone 4 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone4TempAverage += dSumEI[j];
                    }

                }
                dZone4TempAverage = dZone4TempAverage / iCount;

                for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone4TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic26[j] = 1;
                    }

                }
                
                //for (int j = iZone3LastQuestion + 1; ((j != iZone4LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone4Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone4Average];
                //        iRecalculateData = 1;
                //    }
                   
                //}


                // zone 5 - 
                // check each section and see if there is any bad data
                iCount = 0;
                for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        // dont include the data as part of the average
                    }
                    else
                    {
                        iCount++;
                        dZone5TempAverage += dSumEI[j];
                    }

                }
                dZone5TempAverage = dZone5TempAverage / iCount;

                for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                {
                    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                    if (dSumEI[j] > (dMultiplierAvg))
                    {
                        dSumEI[j] = dZone5TempAverage;
                        iRecalculateData = 1;
                        QuestionThatWasDistortDuringMultipleCodeLogic26[j] = 1;
                    }

                }
                //for (int j = iZone4LastQuestion + 1; ((j != iZone5LastQuestion)); j++)
                //{
                //    dMultiplierAvg = dRDTimeAvg[iZone5Average] * dMultipler;
                //    if (dSumEI[j] > (dMultiplierAvg))
                //    {
                //        dSumEI[j] = dRDTimeAvg[iZone5Average];
                //        iRecalculateData = 1;
                //    }
                    
                //}

                // recalculate the data if we need to
                if (iRecalculateData > 0)
                {
                    CalculateSUMAvgT26(ref pTestSession);
                }

            } // main loop

        }

        public double getStandardDeviationList(List<double> doubleList)
        {
            //double average = doubleList.Average();
            //double sumOfDerivation = 0;
            //foreach (double value in doubleList)
            //{
            //    sumOfDerivation += (value) * (value);
            //}
            //double sumOfDerivationAverage = sumOfDerivation / doubleList.Count;
            //return Math.Sqrt(sumOfDerivationAverage - (average * average));

            double average = doubleList.Average();
            double sumOfDerivation = 0;
            foreach (double value in doubleList)
            {
                sumOfDerivation += Math.Pow(((double)value - average), 2);
            }
           
            return Math.Sqrt(sumOfDerivation / (doubleList.Count - 1));
        }  

        public void getStandardDeviation26()
        {
            double SumOfSqrs = 0;
            double avg = listSumEI[0];

            StandardDeviation = 0;
            for (int i = 4; i < listSumEI.Count(); i++)
            {

                SumOfSqrs += Math.Pow(((double)listSumEI[i] - avg), 2);
            }
            double n = (double)listSumEI.Count();
            StandardDeviation = Math.Sqrt(SumOfSqrs / (n - 1));
            //dSTDSPR[0] = StandardDeviation;
            

        }


        public void CalculateGreaterTSLHigh26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // SPR1
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[0]) { bGreaterTSLHighSPR1[i] = 1; }
                    else { bGreaterTSLHighSPR1[i] = 0; }
                }



                // SPR2
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[1]) { bGreaterTSLHighSPR2[i] = 1; }
                    else { bGreaterTSLHighSPR2[i] = 0; }
                }


                // SPR3
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[2]) { bGreaterTSLHighSPR3[i] = 1; }
                    else { bGreaterTSLHighSPR3[i] = 0; }
                }


                // SPR4
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[3]) { bGreaterTSLHighSPR4[i] = 1; }
                    else { bGreaterTSLHighSPR4[i] = 0; }
                }


                // SPR5
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[4]) { bGreaterTSLHighSPR5[i] = 1; }
                    else { bGreaterTSLHighSPR5[i] = 0; }
                }


                // SPR6
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[5]) { bGreaterTSLHighSPR6[i] = 1; }
                    else { bGreaterTSLHighSPR6[i] = 0; }
                }


                // SPR7
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dTSLHiSPR[6]) { bGreaterTSLHighSPR7[i] = 1; }
                    else { bGreaterTSLHighSPR7[i] = 0; }
                }


            }
        }

        public void CalculateLessTSLLow26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // SPR1
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[0]) { bGreaterTSLLowSPR1[i] = 1; }
                    else { bGreaterTSLLowSPR1[i] = 0; }
                }

                // SPR2
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[1]) { bGreaterTSLLowSPR2[i] = 1; }
                    else { bGreaterTSLLowSPR2[i] = 0; }
                }




                // SPR3
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[2]) { bGreaterTSLLowSPR3[i] = 1; }
                    else { bGreaterTSLLowSPR3[i] = 0; }
                }



                // SPR4
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[3]) { bGreaterTSLLowSPR4[i] = 1; }
                    else { bGreaterTSLLowSPR4[i] = 0; }
                }



                // SPR5
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[4]) { bGreaterTSLLowSPR5[i] = 1; }
                    else { bGreaterTSLLowSPR5[i] = 0; }
                }



                // SPR6
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[5]) { bGreaterTSLLowSPR6[i] = 1; }
                    else { bGreaterTSLLowSPR6[i] = 0; }
                }


                // SPR7
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dTSLLowSPR[6]) { bGreaterTSLLowSPR7[i] = 1; }
                    else { bGreaterTSLLowSPR7[i] = 0; }
                }
            }
        }

        public void CalculateWeight26(ref TestSession pTestSession)
        {
            int iWeightTotal = 0;

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    switch (i) 
                    {
                        // question 6, 7, 8
                        case 5:
                        case 6:
                        case 7:
                            iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR3[i] + bGreaterTSLLowSPR3[i];
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                            // question 10, 11, 12
                        case 9:
                        case 10:
                        case 11:
                            iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR4[i] + bGreaterTSLLowSPR4[i];
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                            // question 14, 15, 16

                        case 13:
                        case 14:
                        case 15:
                            iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR5[i] + bGreaterTSLLowSPR5[i];
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                            // question 18, 19, 20
                        case 17:
                        case 18:
                        case 19:
                            iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR6[i] + bGreaterTSLLowSPR6[i];
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;
                            // question 22, 23, 24
                        case 21:
                        case 22:
                        case 23:
                            iWeightTotal = bGreaterTSLHighSPR1[i] + bGreaterTSLLowSPR1[i] + bGreaterTSLHighSPR2[i] + bGreaterTSLLowSPR2[i] + bGreaterTSLHighSPR7[i] + bGreaterTSLLowSPR7[i];
                            if (iWeightTotal > 0)
                            {
                                Weight[i] = 1;
                            }
                            else
                            {
                                Weight[i] = 0;
                            }
                            break;

                        default:
                            break;
                    }
                    
                }
            }
        }

        public void CalculateWeightAnswer26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    if (screen.Answer)
                    {
                        iWeightAnswer[i] = 3;
                    }
                    else
                    {
                        iWeightAnswer[i] = 0;
                    }


                    // thses are the questions that are not suppose to be ADMITS
                    switch (i)
                    {

                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:

                            if (screen.Answer)
                            {
                                iWeightAnswer[i] = 0; // if answer yes, then this is ok
                            }
                            else
                            {
                                iWeightAnswer[i] = 2; // if answer no to these question, maybe be fooling system
                            }
                            break;
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 25:
                            iWeightAnswer[i] = 0; // we dont care about this answer
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void CalculateWeightTotal26(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    iWeightTotal[i] = Weight[i] + iWeightAnswer[i];

                    // thses are the questions that are not suppose to be ADMITS
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                        case 25:
                            iWeightTotal[i] = 0;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        public void CalculateZoneWeightTotal26(ref TestSession pTestSession)
        {

            int iZoneCount = 0;
            int iWeightCount = 2;
            // for Zone 1, Questions 3/5
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 2, Questions 6/8
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 3, Questions 9/11
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 4, Questions 12/14
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 5, Questions 15/17
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 6, Questions 18/20
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 7, Questions 21/23
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];



        }




        public void CalculateReactionSPR26(ref TestSession pTestSession)
        {
            Screen screen = null;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count ; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // XXX
                    // TEMPorary Change the Weight Total to Looked at the Reaction Data INSTEAD
                    if (iWeightTotal[i] > 0)
                    {
                        screen.IsHit = true;

                        //sReactionSPR[i] = "SPR";
                    }
                    //if (dRDTime[i] > 0)
                    //{
                    //    screen.IsHit = true;
                    //    //sReactionSPR[i] = "SPR";
                    //}

                    else
                    {
                        screen.IsHit = null;
                        //sReactionSPR[i] = "";
                    }
                }
            }
        }

        public void CalculateReadReactionDistortion26(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions textBoxReadRatio2
            int j = 0;
            double Sum2 = 0;  // for questions textBoxReadRatio3
            int k = 0;

            // Calculate the Read Reactions 
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Calculate the 1st Reactions questions (T7+T11+T15+T19+T23+T27)/6
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 6:
                        case 10:
                        case 14:
                        case 18:
                        case 22:
                        case 26:
                            Sum1 += screen.DeltaClickTimeInSeconds;
                            j++;
                            break;
                        default:
                            break;
                    }
                    
                    
                }
                ReadReaction1 = (double)(Sum1 / (j));

                // Calcualte the 2nd Reactions question (T8+T9+T10+T12+T14+T16+T17+T18+T20+T21+T22+T24+T25+T30)/14
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    switch (i)
                    {
                        case 7:
                        case 8:
                        case 9:
                        case 11:
                        case 13:
                        case 15:
                        case 16:
                        case 17:
                        case 19:
                        case 20:
                        case 21:
                        case 23:
                        case 24:
                        case 29:
                            Sum2 += screen.DeltaClickTimeInSeconds;
                            k++;
                            break;
                        default:
                            break;
                    }
                }
                ReadReaction2 = (double)(Sum2 / k);
            }

            // Calculate the Distortion

            if (ReadReaction1 < (iReadReaction * ReadReaction2))
            {
                Distortion = true;
            }
        }

    }




    public class Anaylyzer24Questions : BaseAnalyzer
    {
      
        //////////////////////////////////////////////////////////////////////////////////
        ///////
        ///////  24 Questions Formula
        ///////
        //////////////////////////////////////////////////////////////////////////////////

        // test analysis formula
        public void AnalysisTest24Questions(ref TestSession pTestSession)
        {
            UpdateTSL24QValues(ref pTestSession);

            //CalculateRDTimeT15(ref pTestSession);
            //CalculateZAxis(ref pTestSession);
            //CalculateSumXPlusZ(ref pTestSession);
            //CalculateAbsXMinusZ(ref pTestSession);
            //CalculateZMinusX(ref pTestSession);
            //CalcualteSumEToIColumn(ref pTestSession);
            //CalculateSUMAvgT24(ref pTestSession); // Calculate the Averages
            //getStandardDeviation24();

            //// Lets calcualte the reaction information
            //CalculateGreaterTSLHigh(ref pTestSession);
            //CalculateLessTSLLow(ref pTestSession);
            //CalculateWeight(ref pTestSession);
            //CalculateWeightAnswer(ref pTestSession);
            //CalculateWeightTotal(ref pTestSession);
            //CalculateReactionSPR(ref pTestSession);
            //CalculateReadReactionDistortion(ref pTestSession);
        }

        public void CalculateRDTimeT24(ref TestSession pTestSession)
        {

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // do your formulas and populate
                    //tmpAvgRDTime = tmpAvgRDTime + ( screen.ReadTimeInSeconds * 100 );
                    dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);
                    listRDTime.Add(dRDTime[i]);
                    //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);
                    // listRDTime.Add(screen.ReadTimeInSeconds * 100);
                }

                //CalculateRDTimeAvgT15(ref pTestSession);

                // tmpAvgRDTime = tmpAvgRDTime / pTestSession.lstScreenQuestionOnly.Count;
                // listRDTime.Add(tmpAvgRDTime);
            }

        }

        public void Load24Questions(ref TestSession pTestSession)
        {
            CalculateRDTimeT24(ref pTestSession);
            CalculateZAxis24(ref pTestSession);
            CalculateSumXPlusZ24(ref pTestSession);
            CalculateAbsXMinusZ24(ref pTestSession);
            CalculateZMinusX24(ref pTestSession);
            CalcualteSumEToIColumn24(ref pTestSession);
            CalculateSUMAvgT24(ref pTestSession); // Calculate the Averages
            getStandardDeviation24();

            // Lets calcualte the reaction information
            CalculateGreaterTSLHigh24(ref pTestSession);
            CalculateLessTSLLow24(ref pTestSession);
            CalculateWeight24(ref pTestSession);
            CalculateWeightAnswer24(ref pTestSession);
            CalculateWeightTotal24(ref pTestSession);
            CalculateReactionSPR24(ref pTestSession);
            CalculateReadReactionDistortion24(ref pTestSession);
            CalculateZoneWeightTotal24(ref pTestSession);
        }

        public void UpdateTSL24QValues(ref TestSession pTestSession)
        {
            // load the configuration into the text box
            dTSLAdjust24Q--;  // grab the % TSLHi and TSLLo

            // now update the TSLHi with the percentage
            dTSLLow = ((100 - dTSLAdjust24Q) / 100);

            // now update the TSLLow with the percentage
            dTSLHIGH = ((100 + dTSLAdjust24Q) / 100);

            TSLPercentage = Convert.ToInt16(dTSLAdjust24Q);

            // Run the Anaylysis and count the number of hits
            Load24Questions(ref pTestSession);

        }


        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  FUZZY LOGIC - 24 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        public void AnalysisTest24AutoAdjust(ref TestSession pTestSession)
        {
            Veracity.Client.Proxy.Screen screen = null;

            for (int j = 0; j != 100; j++)
            {

                /////////////////////////////////////////////////////
                // There are two ways to determine TO DO THE AUTO ADJUST
                //
                // When there two 2 SPR and not counting ADMIT, we stop
                // OR
                // STOP have at least 1 SPR in each catagory/sections
                //
                // 24 questions to determine to leave the loop
                /////////////////////////////////////////////////////
                //if ((ReactionZone1Counter > 0 ) &&
                //    (ReactionZone2Counter > 0) &&
                //    (ReactionZone3Counter > 0) &&
                //    (ReactionZone4Counter > 0) &&
                //    (ReactionZone5Counter > 0) &&
                //    (ReactionZone6Counter > 0) &&
                //    (ReactionZone7Counter > 0))
                //{
                //    j = 100;
                //    break;
                //}
                if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) >= eTotalNumReaction)
                {
                    j = 100;
                    break;
                }

                if (dTSLAdjust24Q == eTSLMinimum)
                {
                    j = 100;
                    break;
                }

                UpdateTSL24QValues(ref pTestSession); // try the next value for the TSLAdjust



                //////////////////////////////////////////////////////////////////
                //////  We updated the calculations and check if we got any hits
                //////////////////////////////////////////////////////////////////
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Veracity.Client.Proxy.Screen)pTestSession.lstScreenQuestionOnly[i];

                    switch (i)
                    {

                        case 2: // questions 3,4,5
                        case 3: // questions 3,4,5
                        case 4: // questions 3,4,5
                            if (i == 2)
                            {
                                if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter1++;
                                }
                            }
                            if (i == 3)
                            {
                                if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter2++;
                                }
                            }
                            if (i == 4)
                            {
                                if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter3++;
                                }
                            }

                            ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                            break;
                        case 5: // questions 6,7,8
                        case 6: // questions 6,7,8
                        case 7: // questions 6,7,8
                            if (i == 5)
                            {
                                if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter1++;
                                }
                            }
                            if (i == 6)
                            {
                                if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter2++;
                                }
                            }
                            if (i == 7)
                            {
                                if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter3++;
                                }
                            }

                            ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                            break;
                        case 8: // questions 9,10,11
                        case 9: // questions 9,10,11
                        case 10: // questions 9,10,11
                            if (i == 8)
                            {
                                if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter1++;
                                }
                            }
                            if (i == 9)
                            {
                                if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter2++;
                                }
                            }
                            if (i == 10)
                            {
                                if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter3++;
                                }
                            }

                            ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                            break;
                        case 11: // questions 12,13,14
                        case 12: // questions 12,13,14
                        case 13: // questions 12,13,14
                            if (i == 11)
                            {
                                if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter1++;
                                }
                            }
                            if (i == 12)
                            {
                                if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter2++;
                                }
                            }
                            if (i == 13)
                            {
                                if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter3++;
                                }
                            }

                            ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                            break;
                        case 14: // questions 3,4,5
                        case 15: // questions 3,4,5
                        case 16: // questions 3,4,5
                            if (i == 14)
                            {
                                if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter1++;
                                }
                            }
                            if (i == 15)
                            {
                                if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter2++;
                                }
                            }
                            if (i == 16)
                            {
                                if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter3++;
                                }
                            }

                            ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                            break;
                        case 17: // questions 3,4,5
                        case 18: // questions 3,4,5
                        case 19: // questions 3,4,5
                            if (i == 17)
                            {
                                if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter1++;
                                }
                            }
                            if (i == 18)
                            {
                                if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter2++;
                                }
                            }
                            if (i == 19)
                            {
                                if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter3++;
                                }
                            }

                            ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                            break;
                        case 20: // questions 3,4,5
                        case 21: // questions 3,4,5
                        case 22: // questions 3,4,5
                            if (i == 20)
                            {
                                if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter1++;
                                }
                            }
                            if (i == 21)
                            {
                                if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter2++;
                                }
                            }
                            if (i == 22)
                            {
                                if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter3++;
                                }
                            }

                            ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                            break;
                        default:
                            break;
                    }

                    //switch (i)
                    //{

                    //    case 2: // questions 3,4,5
                    //    case 3: // questions 3,4,5
                    //    case 4: // questions 3,4,5
                    //        if ((screen.IsHit == true) && ( screen.Answer != true ))
                    //        {
                    //            ReactionZone1Counter++;
                    //        }
                    //        if (ReactionZone1Counter > 3)
                    //        {
                    //            ReactionZone1Counter = 3;
                    //        }
                    //        break;
                    //    case 5: // questions 3,4,5
                    //    case 6: // questions 3,4,5
                    //    case 7: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone2Counter++;
                    //        }
                    //        if (ReactionZone2Counter > 3)
                    //        {
                    //            ReactionZone2Counter = 3;
                    //        }
                    //        break;
                    //    case 8: // questions 3,4,5
                    //    case 9: // questions 3,4,5
                    //    case 10: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone3Counter++;
                    //        }
                    //        if (ReactionZone3Counter > 3)
                    //        {
                    //            ReactionZone3Counter = 3;
                    //        }
                    //        break;
                    //    case 11: // questions 3,4,5
                    //    case 12: // questions 3,4,5
                    //    case 13: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone4Counter++;
                    //        }
                    //        if (ReactionZone4Counter > 3)
                    //        {
                    //            ReactionZone4Counter = 3;
                    //        }
                    //        break;
                    //    case 14: // questions 3,4,5
                    //    case 15: // questions 3,4,5
                    //    case 16: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone5Counter++;
                    //        }
                    //        if (ReactionZone5Counter > 3)
                    //        {
                    //            ReactionZone5Counter = 3;
                    //        }
                    //        break;
                    //    case 17: // questions 3,4,5
                    //    case 18: // questions 3,4,5
                    //    case 19: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone6Counter++;
                    //        }
                    //        if (ReactionZone6Counter > 3)
                    //        {
                    //            ReactionZone6Counter = 3;
                    //        }
                    //        break;
                    //    case 20: // questions 3,4,5
                    //    case 21: // questions 3,4,5
                    //    case 22: // questions 3,4,5
                    //        if ((screen.IsHit == true) && (screen.Answer != true))
                    //        {
                    //            ReactionZone7Counter++;
                    //        }
                    //        if (ReactionZone7Counter > 3)
                    //        {
                    //            ReactionZone7Counter = 3;
                    //        }
                    //        break;
                    //    default:
                    //        break;
                    //}
                }
            }
        }


        public void CalculateZAxis24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dZTime[i] = (double)(screen.ZAxisData);
                }
            }

        }

        public void CalculateSumXPlusZ24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dSumXZ[i] = (double)(dRDTime[i] + dZTime[i]);
                }
            }
        }

        public void CalculateAbsXMinusZ24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dAbsXZ[i] = Math.Abs(dRDTime[i] - dZTime[i]);
                }
            }
        }

        public void CalculateZMinusX24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dSubZX[i] = (double)(dZTime[i] - dRDTime[i]);
                }
            }
        }

        public void CalcualteSumEToIColumn24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dSumEI[i] = (double)((dRDTime[i] + dZTime[i] + dSumXZ[i] + dAbsXZ[i] + dSubZX[i]));
                    if (i >= 0)  // Dont Skip the first 2 question
                    {
                        listSumEI.Add(dSumEI[i]);  // this is used for the standard deviation for the 24 question
                    }
                }
            }
        }


        public void CalculateSUMAvgT24(ref TestSession pTestSession)
        {

            // Calculate TEST Average Line
            int i = 0;
            dRDTimeAvg[0] = 0;
            for (int j = 2; j < pTestSession.lstScreenQuestionOnly.Count; j++)
            {
                i++;
                dRDTimeAvg[0] += dSumEI[j];
            }
            dRDTimeAvg[0] = dRDTimeAvg[0] / i;





            // Calculate Travelling 3/5 Line 
            i = 0;
            dRDTimeAvg[1] = 0;
            for (int j = 2; j <= 4; j++)
            {
                i++;
                dRDTimeAvg[1] += dSumEI[j];
            }
            dRDTimeAvg[1] = dRDTimeAvg[1] / i;


            // Calculate Weapons 6/8 lines
            i = 0;
            dRDTimeAvg[2] = 0;
            for (int j = 5; j <= 7; j++)
            {
                i++;
                dRDTimeAvg[2] += dSumEI[j];
            }
            dRDTimeAvg[2] = dRDTimeAvg[2] / i;


            // Calculate Smuggling 9/11 lines
            i = 0;
            dRDTimeAvg[3] = 0;
            for (int j = 8; j <= 10; j++)
            {
                i++;
                dRDTimeAvg[3] += dSumEI[j];
            }
            dRDTimeAvg[3] = dRDTimeAvg[3] / i;



            // Calculate Terrorism 12/14 lines
            i = 0;
            dRDTimeAvg[4] = 0;
            for (int j = 11; j <= 13; j++)
            {
                i++;
                dRDTimeAvg[4] += dSumEI[j];
            }
            dRDTimeAvg[4] = dRDTimeAvg[4] / i;


            // Calculate ALL Average line
            i = 0;
            dRDTimeAvg[5] = 0;
            for (int j = 5; j <= 23; j++)
            {
                i++;
                dRDTimeAvg[5] += dSumEI[j];
            }
            //dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * Convert.ToDouble(sTSLHigh);
            dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * dTSLHIGH;


            //// Calculate TSL High
            //dRDTimeAvg[6] = 0;
            //for (int j = 1; j <= 4; j++)
            //{
            //    dRDTimeAvg[6] += dRDTimeAvg[j];
            //}
            ////dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * Convert.ToDouble(sTSLHigh);
            //dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * dTSLHIGH;

            //// Calculate TSL Low
            //dRDTimeAvg[7] = 0;
            //for (int j = 1; j <= 4; j++)
            //{
            //    dRDTimeAvg[7] += dRDTimeAvg[j];
            //}
            ////dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * Convert.ToDouble(sTSLLow);
            //dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * dTSLLow;




            // Calculate 15/17 lines
            i = 0;
            dRDTimeAvg[8] = 0;
            for (int j = 14; j <= 16; j++)
            {
                i++;
                dRDTimeAvg[8] += dSumEI[j];
            }
            dRDTimeAvg[8] = dRDTimeAvg[8] / i;


            // Calculate 18/20 lines
            i = 0;
            dRDTimeAvg[9] = 0;
            for (int j = 17; j <= 19; j++)
            {
                i++;
                dRDTimeAvg[9] += dSumEI[j];
            }
            dRDTimeAvg[9] = dRDTimeAvg[9] / i;



            // Calculate 21/23 lines
            i = 0;
            dRDTimeAvg[10] = 0;
            for (int j = 20; j <= 22; j++)
            {
                i++;
                dRDTimeAvg[10] += dSumEI[j];
            }
            dRDTimeAvg[10] = dRDTimeAvg[10] / i;



            // Calculate TSL High
            dRDTimeAvg[6] = 0;
            for (int j = 1; j <= 4; j++)
            {
                dRDTimeAvg[6] += dRDTimeAvg[j];
            }
            for (int j = 8; j <= 10; j++)
            {
                dRDTimeAvg[6] += dRDTimeAvg[j];
            }
            //dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * Convert.ToDouble(sTSLHigh);
            dRDTimeAvg[6] = (dRDTimeAvg[6] / 7) * dTSLHIGH;

            // Calculate TSL Low
            dRDTimeAvg[7] = 0;
            for (int j = 1; j <= 4; j++)
            {
                dRDTimeAvg[7] += dRDTimeAvg[j];
            }
            for (int j = 8; j <= 10; j++)
            {
                dRDTimeAvg[7] += dRDTimeAvg[j];
            }
            //dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * Convert.ToDouble(sTSLLow);
            dRDTimeAvg[7] = (dRDTimeAvg[7] / 7) * dTSLLow;
        }


        public void getStandardDeviation24()
        {
            double SumOfSqrs = 0;
            double avg = listSumEI[0];

            StandardDeviation = 0;
            for (int i = 0; i < listSumEI.Count() ; i++)
            {

                SumOfSqrs += Math.Pow(((double)listSumEI[i] - avg), 2);
            }
            double n = (double)listSumEI.Count();
            StandardDeviation = Math.Sqrt(SumOfSqrs / (n - 1));

        }


        public void CalculateGreaterTSLHigh24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If greater than the TSL High
                    if (dSumEI[i] > dRDTimeAvg[6]) { bGreaterTSLHigh[i] = 1; }
                    else { bGreaterTSLHigh[i] = 0; }
                }
            }
        }

        public void CalculateLessTSLLow24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    // If less than the TSL Low
                    if (dSumEI[i] < dRDTimeAvg[7]) { bGreaterTSLLow[i] = 1; }
                    else { bGreaterTSLLow[i] = 0; }
                }
            }
        }

        public void CalculateWeight24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    Weight[i] = bGreaterTSLHigh[i] + bGreaterTSLLow[i];
                }
            }
        }

        public void CalculateWeightAnswer24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    if (screen.Answer)
                    {
                        iWeightAnswer[i] = 3;
                    }
                    else
                    {
                        iWeightAnswer[i] = 0;
                    }
                }
            }
        }

        public void CalculateWeightTotal24(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    iWeightTotal[i] = Weight[i] + iWeightAnswer[i];
                }
            }
        }

        public void CalculateZoneWeightTotal24(ref TestSession pTestSession)
        {

            int iZoneCount = 0;
            int iWeightCount = 2;
            // for Zone 1, Questions 3/5
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 2, Questions 6/8
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 3, Questions 9/11
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 4, Questions 12/14
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 5, Questions 15/17
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 6, Questions 18/20
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];

            // for Zone 7, Questions 21/23
            iZoneWeightTotal[iZoneCount++] = iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++] + iWeightTotal[iWeightCount++];



        }


        

        public void CalculateReactionSPR24(ref TestSession pTestSession)
        {
            Screen screen = null;
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Go through the all the questions
                for (int i = 2; i < pTestSession.lstScreenQuestionOnly.Count - 1; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // XXX
                    // TEMPorary Change the Weight Total to Looked at the Reaction Data INSTEAD
                    if (iWeightTotal[i] > 0)
                    {
                        screen.IsHit = true;
                        
                        //sReactionSPR[i] = "SPR";
                    }
                    //if (dRDTime[i] > 0)
                    //{
                    //    screen.IsHit = true;
                    //    //sReactionSPR[i] = "SPR";
                    //}

                    else
                    {
                        screen.IsHit = null;
                        //sReactionSPR[i] = "";
                    }
                }
            }
        }

        public void CalculateReadReactionDistortion24(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions 3 to 15
            int j = 0;
            double Sum2 = 0;  // for questions 16 to 25
            int k = 0;

            // Calculate the Read Reactions 
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Calculate the 1st Reactions questions 3 to 15
                for (int i = 2; i <= 14; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    j++;
                }
                ReadReaction1 = (double)(Sum1 / (j));

                // Calcualte the 2nd Reactions question 15 to 25
                for (int i = 14; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum2 += screen.DeltaClickTimeInSeconds;
                    k++;
                }
                ReadReaction2 = (double)(Sum2 / k);
            }

            // Calculate the Distortion

            if (ReadReaction2 < (iReadReaction * ReadReaction1))
            {
                Distortion = true;
            }
        }

    }


    //////////////////////////////////////////////////////////////////////////////////
    ///////
    ///////  KOT Questions Formula
    ///////
    //////////////////////////////////////////////////////////////////////////////////


    public class AnalyzerKOTQuestions : BaseAnalyzer
    {
        //////////////////////////////////////////////////////////////////////////////////
        ///////
        ///////  KOT Questions Formula
        ///////
        //////////////////////////////////////////////////////////////////////////////////

        // test analysis formula
        public void AnalysisTestKOTQuestions(ref TestSession pTestSession)
        {
            UpdateTSLKOTQValues(ref pTestSession);
        }

        public void UpdateTSLKOTQValues(ref TestSession pTestSession)
        {
            // load the configuration into the text box
            //dTSLAdjustKOT--;  // grab the % TSLHi and TSLLo

            // now update the TSLHi with the percentage
            dTSLLow = ((100 - dTSLAdjustKOT) / 100);

            // now update the TSLLow with the percentage
            dTSLHIGH = ((100 + dTSLAdjustKOT) / 100);

            TSLPercentage = Convert.ToInt16(dTSLAdjustKOT);

            // Run the Anaylysis and count the number of hits
            LoadKOTQuestions(ref pTestSession);

        }


        public void LoadKOTQuestions(ref TestSession pTestSession)
        {

            CalculateRDTimeTKOT(ref pTestSession);
            CalculateRDTimeAvgTKOT(ref pTestSession);
            CalculateEKOT(ref pTestSession);
            CalculateIKOT(ref pTestSession);
            CalculateJKOT();
            CalculateKKOT();
            CalculateLKOT();
            CalculateMKOT();
            CalculateNKOT();
            CalculateHiddenGKOT();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenHKOT();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenIKOT();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenJKOT(ref pTestSession);  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenKKOT();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenLKOT(ref pTestSession);
            CalculateResultsKOT();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            //CalculateTSLHighKOT();  // used in the new spread sheet with Graph.
            //CalculateTSLLowKOT();  // used in the new spread sheet with Graph.
            CalculateReactionKOT(ref pTestSession); // used in new spread sheet wiht GRAPH
            CalculateReactionSumKOT(); // used in new spread sheet wiht GRAPH

            CalculateZonesKOT();
            CalculateSurveyAverageKOT(ref pTestSession);
            //CalculateFinalResultsKOT();
            // CalculateReactionResultKOT();
            getStandardDeviationKOT();

            CalculateReadReactionDistortionKOT(ref pTestSession);
        }

        public void CalculateRDTimeTKOT(ref TestSession pTestSession)
        {

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // do your formulas and populate

                    
                    dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);

                    if ((i > 0) && (listRDTime.Count < pTestSession.lstScreenQuestionOnly.Count))
                    {
                        listRDTime.Add(dRDTime[i]);
                    }
                    //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);

                }

                CalculateRDTimeAvgTKOT(ref pTestSession);

                // tmpAvgRDTime = tmpAvgRDTime / pTestSession.lstScreenQuestionOnly.Count;
                // listRDTime.Add(tmpAvgRDTime);
            }

        }
    

        public void CalculateRDTimeAvgTKOT(ref TestSession pTestSession)
        {

            // Calculate Survey Average Line
            int i = 0;
            dRDTimeAvg[0] = 0;
            for (int j = 1; j < 16; j++)
            {
                i++;
                dRDTimeAvg[0] += dRDTime[j];
            }
            dRDTimeAvg[0] = dRDTimeAvg[0] / i;


            // Calculate  (E4+E5+E7+E8) / 4
            i = 0;
            dRDTimeAvg[1] = 0;
            dRDTimeAvg[1] = (dRDTime[1] + dRDTime[2] + dRDTime[4] + dRDTime[5]) / 4;
            //for (int j = 2; j <= 4; j++)
            //{
            //    i++;
            //    dRDTimeAvg[1] += dRDTime[j];
            //}
            //dRDTimeAvg[1] = dRDTimeAvg[1] / i;


            // Calculate (E7+E8+E10+E11) / 4
            i = 0;
            dRDTimeAvg[2] = 0;
            dRDTimeAvg[2] = (dRDTime[4] + dRDTime[5] + dRDTime[7] + dRDTime[8]) / 4;
            //for (int j = 5; j <= 7; j++)
            //{
            //    i++;
            //    dRDTimeAvg[2] += dRDTime[j];
            //}
            //dRDTimeAvg[2] = dRDTimeAvg[2] / i;


            // Calculate (E10+E11+E13+E14) / 4
            i = 0;
            dRDTimeAvg[3] = 0;
            dRDTimeAvg[3] = (dRDTime[7] + dRDTime[8] + dRDTime[10] + dRDTime[11]) / 4;
            //for (int j = 8; j <= 10; j++)
            //{
            //    i++;
            //    dRDTimeAvg[3] += dRDTime[j];
            //}
            //dRDTimeAvg[3] = dRDTimeAvg[3] / i;



            // Calculate (E13+E14+E16+E17) / 4
            i = 0;
            dRDTimeAvg[4] = 0;
            dRDTimeAvg[4] = (dRDTime[10] + dRDTime[11] + dRDTime[13] + dRDTime[14]) / 4;
            //for (int j = 11; j <= 13; j++)
            //{
            //    i++;
            //    dRDTimeAvg[4] += dRDTime[j];
            //}
            //dRDTimeAvg[4] = dRDTimeAvg[4] / i;


            // Calculate ALL Average line

            i = 0;
            dRDTimeAvg[5] = 0;
            for (int j = 5; j <= 13; j++)
            {
                i++;
                dRDTimeAvg[5] += dRDTime[j];
            }
            //dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * Convert.ToDouble(sTSLHigh);
            dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * dTSLHIGH;


            //////////////////////////////////////////////////////////////////////////////
            // Calculate TSL High
            //////////////////////////////////////////////////////////////////////////////
            dRDTimeAvg[6] = 0;
            //for (int j = 1; j <= 4; j++)
            //{
            //    dRDTimeAvg[6] += dRDTimeAvg[j];
            //}
            //dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * Convert.ToDouble(sTSLHigh);
            // dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * dTSLHIGH;
            dRDTimeAvg[6] = dTSLHIGHKOT;

            //////////////////////////////////////////////////////////////////////////////
            // Calculate TSL Low
            //////////////////////////////////////////////////////////////////////////////
            dRDTimeAvg[7] = 0;
            //for (int j = 1; j <= 4; j++)
            //{
            //    dRDTimeAvg[7] += dRDTimeAvg[j];
            //}
            //dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * Convert.ToDouble(sTSLLow);
            // dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * dTSLLow;
            dRDTimeAvg[7] =  dTSLLowKOT;
        }


        public void CalculateEKOT(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dE[i] = (double)(screen.ReadTimeInSeconds * 100);
                }
            }
        }


        // Average the button was pressed
        public void CalculateIKOT(ref TestSession pTestSession)
        {
            // Calculate the first 5 cells
            // Question 3, 4, 5 all RD time or Read Time
            Screen screen3 = null;
            Screen screen4 = null;
            Screen screen5 = null;
            screen3 = (Screen)pTestSession.lstScreenQuestionOnly[2];
            screen4 = (Screen)pTestSession.lstScreenQuestionOnly[3];
            screen5 = (Screen)pTestSession.lstScreenQuestionOnly[4];
            for (int i = 1; i <= 5; i++)
            {
                dI[i] = (((double)screen3.ReadTimeInSeconds) +
                          ((double)screen4.ReadTimeInSeconds) +
                          ((double)screen5.ReadTimeInSeconds)) * 100 / 3;

            }

            // Calculate the next 3 cells
            Screen screen6 = null;
            Screen screen7 = null;
            Screen screen8 = null;
            screen6 = (Screen)pTestSession.lstScreenQuestionOnly[5];
            screen7 = (Screen)pTestSession.lstScreenQuestionOnly[6];
            screen8 = (Screen)pTestSession.lstScreenQuestionOnly[7];
            for (int i = 6; i <= 8; i++)
            {
                dI[i] = (((double)screen6.ReadTimeInSeconds) +
                         ((double)screen7.ReadTimeInSeconds) +
                         ((double)screen8.ReadTimeInSeconds)) * 100 / 3;
            }


            // Calculate the next 3 cells
            Screen screen9 = null;
            Screen screen10 = null;
            Screen screen11 = null;
            screen9 = (Screen)pTestSession.lstScreenQuestionOnly[8];
            screen10 = (Screen)pTestSession.lstScreenQuestionOnly[9];
            screen11 = (Screen)pTestSession.lstScreenQuestionOnly[10];
            for (int i = 9; i <= 11; i++)
            {
                dI[i] = (((double)screen9.ReadTimeInSeconds) +
                         ((double)screen10.ReadTimeInSeconds) +
                         ((double)screen11.ReadTimeInSeconds)) * 100 / 3;
            }

            // Calculate the next 4 cells
            Screen screen12 = null;
            Screen screen13 = null;
            Screen screen14 = null;
            screen12 = (Screen)pTestSession.lstScreenQuestionOnly[11];
            screen13 = (Screen)pTestSession.lstScreenQuestionOnly[12];
            screen14 = (Screen)pTestSession.lstScreenQuestionOnly[13];
            for (int i = 12; i <= pTestSession.lstScreenQuestionOnly.Count; i++)
            {
                dI[i] = (((double)screen12.ReadTimeInSeconds) +
                         ((double)screen13.ReadTimeInSeconds) +
                         ((double)screen14.ReadTimeInSeconds)) * 100 / 3;
            }
        }

        public void CalculateJKOT()
        {
            for (int i = 1; i <= MAXQUESTIONS_KOT; i++)
            {
                dJ[i] = ((dE[i] + dI[i]) / 2);
            }
        }

        public void CalculateKKOT()
        {
            for (int i = 1; i <= MAXQUESTIONS_KOT; i++)
            {
                dK[i] = dI[i] - dE[i];
            }
        }

        public void CalculateLKOT()
        {

            for (int i = 1; i <= MAXQUESTIONS_KOT; i++)
            {
                dL[i] = Math.Abs(dK[i]);
            }
        }

        public void CalculateMKOT()
        {

            for (int i = 1; i <= MAXQUESTIONS_KOT; i++)
            {
                dM[i] = dK[i] + dL[i];
            }
        }

        public void CalculateNKOT()
        {
            for (int i = 1; i <= MAXQUESTIONS_KOT; i++)
            {
                dN[i] = (dJ[i] + dM[i]) / 2;
            }
        }


        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenGKOT()
        {

            iHiddenG[0] = dRDTimeAvg[1] * dRDTimeAvg[6];
            iHiddenG[1] = dRDTimeAvg[2] * dRDTimeAvg[6];
            iHiddenG[2] = dRDTimeAvg[3] * dRDTimeAvg[6];
            iHiddenG[3] = dRDTimeAvg[4] * dRDTimeAvg[6];
            iHiddenG[4] = dRDTimeAvg[4] * dRDTimeAvg[6];

        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenHKOT()
        {
            //iHiddenH[0] = Convert.ToInt32(dRDTimeAvg[1] * dRDTimeAvg[7]);
            //iHiddenH[1] = Convert.ToInt32(dRDTimeAvg[2] * dRDTimeAvg[7]);
            //iHiddenH[2] = Convert.ToInt32(dRDTimeAvg[3] * dRDTimeAvg[7]);
            //iHiddenH[3] = Convert.ToInt32(dRDTimeAvg[4] * dRDTimeAvg[7]);
            //iHiddenH[4] = Convert.ToInt32(dRDTimeAvg[4] * dRDTimeAvg[7]);
            iHiddenH[0] = dRDTimeAvg[1] * dRDTimeAvg[7];
            iHiddenH[1] = dRDTimeAvg[2] * dRDTimeAvg[7];
            iHiddenH[2] = dRDTimeAvg[3] * dRDTimeAvg[7];
            iHiddenH[3] = dRDTimeAvg[4] * dRDTimeAvg[7];
            iHiddenH[4] = dRDTimeAvg[4] * dRDTimeAvg[7];
        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenIKOT()
        {

            iHiddenI[0] = 0;
            if (dRDTime[3] > iHiddenG[0])
            {
                iHiddenI[0] = 1;
            }

            iHiddenI[1] = 0;
            if (dRDTime[6] > iHiddenG[1])
            {
                iHiddenI[1] = 1;
            }

            iHiddenI[2] = 0;
            if (dRDTime[9] > iHiddenG[2])
            {
                iHiddenI[2] = 1;
            }

            iHiddenI[3] = 0;
            if (dRDTime[12] > iHiddenG[3])
            {
                iHiddenI[3] = 1;
            }


            iHiddenI[4] = 0;
            if (dRDTime[15] > iHiddenG[4])
            {
                iHiddenI[4] = 1;
            }

            //MessageBox.Show("iHiddenI = " + iHiddenI[0] + iHiddenI[1] + iHiddenI[2] + iHiddenI[3] + iHiddenI[4]);

        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenJKOT(ref TestSession pTestSession)
        {
            iHiddenJ[0] = 0;
            if (dRDTime[3] < iHiddenH[0])
            {
                iHiddenJ[0] = 1;
            }

            iHiddenJ[1] = 0;
            if (dRDTime[6] < iHiddenH[1])
            {
                iHiddenJ[1] = 1;
            }

            iHiddenJ[2] = 0;
            if (dRDTime[9] < iHiddenH[2])
            {
                iHiddenJ[2] = 1;
            }

            iHiddenJ[3] = 0;
            if (dRDTime[12] < iHiddenH[3])
            {
                iHiddenJ[3] = 1;
            }


            iHiddenJ[4] = 0;
            if (dRDTime[15] < iHiddenH[4])
            {
                iHiddenJ[4] = 1;
            }

            //MessageBox.Show("iHiddenJ = " + iHiddenJ[0] + iHiddenJ[1] + iHiddenJ[2] + iHiddenJ[3] + iHiddenJ[4]);

        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenKKOT()
        {
            int x = 0;

            for (x = 0; x < SIZEOFDATA - 5; x++)
            {
                iHiddenK[x] = iHiddenI[x] + iHiddenJ[x];
                //MessageBox.Show("Hidden K[" + x.ToString() + "]=" + iHiddenK[x].ToString());
            }
        }


        private void CalculateHiddenLKOT(ref TestSession pTestSession)
        {

            iHiddenLAverage = 0;

            if ((pTestSession != null) &&
                (pTestSession.lstScreenQuestionOnly.Count > 0) &&
                (pTestSession.lstScreenQuestionOnly.Count == MAXQUESTIONS_KOT))
            {
                Screen screen = null;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // Question 4, 7, 10, 13 
                    if ((i == 3) || (i == 6) || (i == 9) || (i == 12))
                    {
                        if (screen.Answer)
                            iHiddenL[i] = 10;
                        else
                            iHiddenL[i] = 0;

                    }
                    else
                    {
                        if (screen.Answer == false)
                        {
                            iHiddenL[i] = 10;
                        }
                        else
                        {
                            iHiddenL[i] = 0;
                        }
                    }
                    iHiddenLAverage += iHiddenL[i];
                }
            }

            // Calculate the Valid Test or NOT
            if (iHiddenLAverage > 1)
            {
                sValidTest = "Invalid Test";
            }
            else
            {
                sValidTest = "Valid Test";
            }
            //MessageBox.Show("L average=" + iHiddenLAverage);



        }

        // used in the new spread sheet with Graph.
        public void CalculateReactionKOT(ref TestSession pTestSession)
        {
            int x = 0;
            for (x = 0; x < SIZEOFDATA - 5; x++)
            {
                sReaction[x] = "No Deception";
                if (iHiddenK[x] > 0)
                {
                    sReaction[x] = "DECEPTION";
                }

                if (x == 4)
                {
                    sReaction[x] = "No Reaction";
                    if (iHiddenK[x] > 0)
                    {
                        sReaction[x] = "REACTION";
                    }
                }
                //Console.WriteLine("CARL" + x.ToString());
                //MessageBox.Show("Reaction[" + x.ToString() + "]=" + sReaction[x] + ", " + iHiddenG[x] + ", " + iHiddenH[x] + ", " + iHiddenI[x] + ", " + iHiddenJ[x] + ", " + iHiddenK[x]);
            }
        }


        private void CalculateReactionSumKOT()
        {
            int x = 0;
            iReactionSumWeapons = 0;
            iReactionSumSmuggling = 0;
            iReactionSumTerrorism = 0;

            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                // Calculate the Sum of the Weapons HIT
                if ((x == 0) || (x == 1) || (x == 2))
                {
                    iReactionSumWeapons += iHiddenK[x];
                }

                // Calculate the Sum of the Smuggling HIT
                if ((x == 3) || (x == 4) || (x == 5))
                {
                    iReactionSumSmuggling += iHiddenK[x];
                }

                // Calculate the Sum of the Terrorism HIT
                if ((x == 6) || (x == 7) || (x == 8))
                {
                    iReactionSumTerrorism += iHiddenK[x];
                }
            }

            if (iReactionSumWeapons > 12)
            {
                iReactionSumWeapons = 12;
            }
            if (iReactionSumSmuggling > 12)
            {
                iReactionSumSmuggling = 12;
            }
            if (iReactionSumTerrorism > 12)
            {
                iReactionSumTerrorism = 12;
            }

            //MessageBox.Show("Reaction Weapons=" + iReactionSumWeapons + ", Reaction Smuggling=" + iReactionSumSmuggling + ", Reaction Terrorism=" + iReactionSumTerrorism); 

        }


        private void CalculateResultsKOT()
        {

            int j = 16;
            // Calculate the Questions Results
            for (int i = 1; i < MAX; i++)
            {
                dResultsKOT[i] = dN[i];
            }

            // Calcualte the Average Results for each zones

            // Survey Average
            for (int i = 3; i <= MAXQUESTIONS_KOT; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
            dResultsKOT[j] = dResultsKOT[j] / (MAXQUESTIONS_KOT - 2);


            j++;
            // Average 3/5
            for (int i = 3; i <= 5; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
            dResultsKOT[j] = dResultsKOT[j] / (3);

            j++;
            // Average 6/8
            for (int i = 6; i <= 8; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
            dResultsKOT[j] = dResultsKOT[j] / (3);

            j++;
            // Average 9/11
            for (int i = 9; i <= 11; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
            dResultsKOT[j] = dResultsKOT[j] / (3);

            j++;
            // Average  12/14
            for (int i = 12; i <= 14; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
            dResultsKOT[j] = dResultsKOT[j] / (3);


            j++;
            // Calculate the "Did Subject React to"
            for (int i = 17; i <= 20; i++)
            {
                dResultsKOT[j] = dResultsKOT[j] + dResultsKOT[i];
            }
        
            dResultsKOT[j] = (dResultsKOT[j] / (4)) * dTSLHIGH;


        }


        // skip CalculateFinalResultsKOT()

        private void CalculateZonesKOT()
        {
            dZonesKOT[1] = (dResultsKOT[3] + dResultsKOT[4] + dResultsKOT[5]) / 3;
            dZonesKOT[2] = (dResultsKOT[6] + dResultsKOT[7] + dResultsKOT[8]) / 3;
            dZonesKOT[3] = (dResultsKOT[9] + dResultsKOT[10] + dResultsKOT[11]) / 3;
            dZonesKOT[4] = (dResultsKOT[12] + dResultsKOT[13] + dResultsKOT[14]) / 3;
        }

        private void CalculateSurveyAverageKOT(ref TestSession pTestSession)
        {
            dSurveyAvgKOT = 0;
            for (int i = 3; i <= MAXQUESTIONS_KOT; i++)
            {
                dSurveyAvgKOT += dE[i];
            }
            dSurveyAvgKOT = dSurveyAvgKOT / (MAXQUESTIONS_KOT - 2);



            // Calculate the R/T Average
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dReadTimeAvgKOT += screen.ReadTimeInSeconds;

                }
            }
        }


        public void getStandardDeviationKOT()
        {
            double SumOfSqrs = 0;
            double avg = dRDTimeAvg[0];

            StandardDeviation = 0;
            for (int i = 0; i < listRDTime.Count - 1; i++)
            {

                SumOfSqrs += Math.Pow(((double)listRDTime[i] - avg), 2);
            }
            double n = (double)listRDTime.Count;
            StandardDeviation = Math.Sqrt(SumOfSqrs / (n - 1));
            //MessageBox.Show("STD=" + StandardDeviation.ToString("###.##"));
            //MessageBox.Show("SUMOFSQRS=" + SumOfSqrs.ToString("###.##"));
            //MessageBox.Show("AVG=" + avg.ToString("###.##"));
            //MessageBox.Show("LIST COUNT=" + listRDTime.Count.ToString("###.##"));
        }

        public void CalculateReadReactionDistortionKOT(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions 6, 7, 8
            int j = 0;
            double Sum2 = 0;  // for questions 4,5,6,9 - 15
            int k = 0;

            // Calculate the Read Reactions 
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Calculate the 1st Reactions questions 6, 7, 8
                for (int i = 5; i <= 7; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    j++;
                }
                ReadReaction1 = (double)(Sum1 / (j));

                // Calcualte the 2nd Reactions question 4,5,6, 9 to 15
                for (int i = 3; i <= 5; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum2 += screen.DeltaClickTimeInSeconds;
                    k++;
                }
                for (int i = 8; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum2 += screen.DeltaClickTimeInSeconds;
                    k++;
                }
                ReadReaction2 = (double)(Sum2 / k);
            }

            // Calculate the Distortion
            Distortion = false;
            if (ReadReaction1 < (iReadReaction * ReadReaction2))
            {
                Distortion = true;
            }
        }




    }



    //////////////////////////////////////////////////////////////////////////////////
    ///////
    ///////  15 Questions Formula
    ///////
    //////////////////////////////////////////////////////////////////////////////////


    public class Analyzer15Questions : BaseAnalyzer
    {
        //////////////////////////////////////////////////////////////////////////////////
        ///////
        ///////  15 Questions Formula
        ///////
        //////////////////////////////////////////////////////////////////////////////////

        // test analysis formula
        public void AnalysisTest15Questions(ref TestSession pTestSession)
        {
            UpdateTSL15QValues(ref pTestSession);
        }

        public void UpdateTSL15QValues(ref TestSession pTestSession)
        {
            // load the configuration into the text box
            dTSLAdjust15Q--;  // grab the % TSLHi and TSLLo

            // now update the TSLHi with the percentage
            dTSLLow = ((100 - dTSLAdjust15Q) / 100);

            // now update the TSLLow with the percentage
            dTSLHIGH = ((100 + dTSLAdjust15Q) / 100);

            TSLPercentage = Convert.ToInt16(dTSLAdjust15Q);

            // Run the Anaylysis and count the number of hits
            Load15Questions(ref pTestSession);

        }


        public void Load15Questions(ref TestSession pTestSession)
        {

            CalculateRDTimeT15(ref pTestSession);
            CalculateRDTimeAvgT15(ref pTestSession);
            CalculateE15(ref pTestSession);
            CalculateI15(ref pTestSession);
            CalculateJ15();
            CalculateK15();
            CalculateL15();
            CalculateM15();
            CalculateN15();
            CalculateHiddenG15();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenH15();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenI15();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenJ15(ref pTestSession);  // used in the new spread sheet with Graph.  Does not display on Viewer.
            CalculateHiddenK15();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            //CalculateResults();  // used in the new spread sheet with Graph.  Does not display on Viewer.
            //CalculateTSLHigh();  // used in the new spread sheet with Graph.
            //CalculateTSLLow();  // used in the new spread sheet with Graph.
            CalculateReaction15(ref pTestSession); // used in new spread sheet wiht GRAPH
            getStandardDeviation15();

            CalculateReadReactionDistortion15(ref pTestSession);
        }



        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  FUZZY LOGIC - 15 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        //public void AnalysisTest15AutoAdjust(ref TestSession pTestSession)
        //{
        //    Veracity.Client.Proxy.Screen screen = null;

        //    for (int j = 0; j != 100; j++)
        //    {

        //        /////////////////////////////////////////////////////
        //        // There are two ways to determine TO DO THE AUTO ADJUST
        //        //
        //        // When there two 2 SPR and not counting ADMIT, we stop
        //        // OR
        //        // STOP have at least 1 SPR in each catagory/sections
        //        //
        //        // 24 questions to determine to leave the loop
        //        /////////////////////////////////////////////////////

        //        if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter) >= eTotalNumReaction)
        //        {
        //            j = 100;
        //            break;
        //        }

        //        if (dTSLAdjust == eTSLMinimum)
        //        {
        //            j = 100;
        //            break;
        //        }

        //        UpdateTSL15QValues(ref pTestSession); // try the next value for the TSLAdjust



        //        //////////////////////////////////////////////////////////////////
        //        //////  We updated the calculations and check if we got any hits
        //        //////////////////////////////////////////////////////////////////
        //        for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
        //        {
        //            screen = (Veracity.Client.Proxy.Screen)pTestSession.lstScreenQuestionOnly[i];

        //            switch (i)
        //            {

        //                case 2: // questions 3,4,5
        //                case 3: // questions 3,4,5
        //                case 4: // questions 3,4,5
        //                    if (i == 2)
        //                    {
        //                        if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone1Counter1++;
        //                        }
        //                    }
        //                    if (i == 3)
        //                    {
        //                        if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone1Counter2++;
        //                        }
        //                    }
        //                    if (i == 4)
        //                    {
        //                        if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone1Counter3++;
        //                        }
        //                    }

        //                    ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
        //                    break;
        //                case 5: // questions 6,7,8
        //                case 6: // questions 6,7,8
        //                case 7: // questions 6,7,8
        //                    if (i == 5)
        //                    {
        //                        if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone2Counter1++;
        //                        }
        //                    }
        //                    if (i == 6)
        //                    {
        //                        if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone2Counter2++;
        //                        }
        //                    }
        //                    if (i == 7)
        //                    {
        //                        if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone2Counter3++;
        //                        }
        //                    }

        //                    ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
        //                    break;
        //                case 8: // questions 9,10,11
        //                case 9: // questions 9,10,11
        //                case 10: // questions 9,10,11
        //                    if (i == 8)
        //                    {
        //                        if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone3Counter1++;
        //                        }
        //                    }
        //                    if (i == 9)
        //                    {
        //                        if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone3Counter2++;
        //                        }
        //                    }
        //                    if (i == 10)
        //                    {
        //                        if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone3Counter3++;
        //                        }
        //                    }

        //                    ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
        //                    break;
        //                case 11: // questions 12,13,14
        //                case 12: // questions 12,13,14
        //                case 13: // questions 12,13,14
        //                    if (i == 11)
        //                    {
        //                        if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone4Counter1++;
        //                        }
        //                    }
        //                    if (i == 12)
        //                    {
        //                        if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone4Counter2++;
        //                        }
        //                    }
        //                    if (i == 13)
        //                    {
        //                        if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
        //                        {
        //                            ReactionZone4Counter3++;
        //                        }
        //                    }

        //                    ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
        //                    break;

        //                default:
        //                    break;
        //            }
        //        }
        //    }
        //}





        /// <summary>
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// //////////
        /// //////////  FUZZY LOGIC - 15 QUESTONS
        /// //////////
        /// ////////////////////////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="pTestSession"></param>

        public void AnalysisTest15AutoAdjust(ref TestSession pTestSession)
        {
            Veracity.Client.Proxy.Screen screen = null;

            for (int j = 0; j != 100; j++)
            {


                // 15 questions to determine to leave the loop
                //if ((ReactionZone2Counter > 0) &&
                //    (ReactionZone3Counter > 0) &&
                //    (ReactionZone4Counter > 0))
                //{
                //    j = 100;
                //    break;
                //}

                // 15 questions to determine to leave the loop
                if ((ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter) >= eTotalNumReaction)
                {
                    j = 100;
                    break;
                }
                if (dTSLAdjust15Q == eTSLMinimum)
                {
                    j = 100;
                    break;
                }

                UpdateTSL15QValues(ref pTestSession); // try the next value for the TSLAdjust



                //////////////////////////////////////////////////////////////////
                //////  We updated the calculations and check if we got any hits
                //////////////////////////////////////////////////////////////////
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Veracity.Client.Proxy.Screen)pTestSession.lstScreenQuestionOnly[i];

                    switch (i)
                    {

                        case 2: // questions 3,4,5
                        case 3: // questions 3,4,5
                        case 4: // questions 3,4,5
                            if (i == 2)
                            {
                                if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter1++;
                                }
                            }
                            if (i == 3)
                            {
                                if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter2++;
                                }
                            }
                            if (i == 4)
                            {
                                if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone1Counter3++;
                                }
                            }

                            ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                            break;
                        case 5: // questions 6,7,8
                        case 6: // questions 6,7,8
                        case 7: // questions 6,7,8
                            if (i == 5)
                            {
                                if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter1++;
                                }
                            }
                            if (i == 6)
                            {
                                if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter2++;
                                }
                            }
                            if (i == 7)
                            {
                                if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone2Counter3++;
                                }
                            }

                            ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                            break;
                        case 8: // questions 9,10,11
                        case 9: // questions 9,10,11
                        case 10: // questions 9,10,11
                            if (i == 8)
                            {
                                if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter1++;
                                }
                            }
                            if (i == 9)
                            {
                                if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter2++;
                                }
                            }
                            if (i == 10)
                            {
                                if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone3Counter3++;
                                }
                            }

                            ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                            break;
                        case 11: // questions 12,13,14
                        case 12: // questions 12,13,14
                        case 13: // questions 12,13,14
                            if (i == 11)
                            {
                                if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter1++;
                                }
                            }
                            if (i == 12)
                            {
                                if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter2++;
                                }
                            }
                            if (i == 13)
                            {
                                if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone4Counter3++;
                                }
                            }

                            ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                            break;
                        case 14: // questions 3,4,5
                        case 15: // questions 3,4,5
                        case 16: // questions 3,4,5
                            if (i == 14)
                            {
                                if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter1++;
                                }
                            }
                            if (i == 15)
                            {
                                if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter2++;
                                }
                            }
                            if (i == 16)
                            {
                                if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone5Counter3++;
                                }
                            }

                            ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                            break;
                        case 17: // questions 3,4,5
                        case 18: // questions 3,4,5
                        case 19: // questions 3,4,5
                            if (i == 17)
                            {
                                if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter1++;
                                }
                            }
                            if (i == 18)
                            {
                                if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter2++;
                                }
                            }
                            if (i == 19)
                            {
                                if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone6Counter3++;
                                }
                            }

                            ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                            break;
                        case 20: // questions 3,4,5
                        case 21: // questions 3,4,5
                        case 22: // questions 3,4,5
                            if (i == 20)
                            {
                                if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter1++;
                                }
                            }
                            if (i == 21)
                            {
                                if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter2++;
                                }
                            }
                            if (i == 22)
                            {
                                if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true) && (screen.Answer != true))
                                {
                                    ReactionZone7Counter3++;
                                }
                            }

                            ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
 


        //// Read in the old version of the CSV file
        //private void ParseCSV()
        //{

        //}

        //// Read in the new version of th XML file
        //private void ParseXML()
        //{
        //}



        public void CalculateRDTimeAvgT15(ref TestSession pTestSession)
        {

            // Calculate Survey Average Line
            int i = 0;
            dRDTimeAvg[0] = 0;
            for (int j = 2; j <= 14; j++)
            {
                i++;
                dRDTimeAvg[0] += dRDTime[j];
            }
            dRDTimeAvg[0] = dRDTimeAvg[0] / i;


            // Calculate Travelling 3/5 Line
            // Calculate Travelling 3/5 line 
            i = 0;
            dRDTimeAvg[1] = 0;
            for (int j = 2; j <= 4; j++)
            {
                i++;
                dRDTimeAvg[1] += dRDTime[j];
            }
            dRDTimeAvg[1] = dRDTimeAvg[1] / i;


            // Calculate Weapons 6/8 lines
            i = 0;
            dRDTimeAvg[2] = 0;
            for (int j = 5; j <= 7; j++)
            {
                i++;
                dRDTimeAvg[2] += dRDTime[j];
            }
            dRDTimeAvg[2] = dRDTimeAvg[2] / i;


            // Calculate Smuggling 9/11 lines
            i = 0;
            dRDTimeAvg[3] = 0;
            for (int j = 8; j <= 10; j++)
            {
                i++;
                dRDTimeAvg[3] += dRDTime[j];
            }
            dRDTimeAvg[3] = dRDTimeAvg[3] / i;



            // Calculate Terrorism 12/14 lines
            i = 0;
            dRDTimeAvg[4] = 0;
            for (int j = 11; j <= 13; j++)
            {
                i++;
                dRDTimeAvg[4] += dRDTime[j];
            }
            dRDTimeAvg[4] = dRDTimeAvg[4] / i;


            // Calculate ALL Average line

            i = 0;
            dRDTimeAvg[5] = 0;
            for (int j = 5; j <= 13; j++)
            {
                i++;
                dRDTimeAvg[5] += dRDTime[j];
            }
            //dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * Convert.ToDouble(sTSLHigh);
            dRDTimeAvg[5] = (dRDTimeAvg[5] / i) * dTSLHIGH;


            // Calculate TSL High
            dRDTimeAvg[6] = 0;
            for (int j = 1; j <= 4; j++)
            {
                dRDTimeAvg[6] += dRDTimeAvg[j];
            }
            //dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * Convert.ToDouble(sTSLHigh);
            dRDTimeAvg[6] = (dRDTimeAvg[6] / 4) * dTSLHIGH;

            // Calculate TSL Low
            dRDTimeAvg[7] = 0;
            for (int j = 1; j <= 4; j++)
            {
                dRDTimeAvg[7] += dRDTimeAvg[j];
            }
            //dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * Convert.ToDouble(sTSLLow);
            dRDTimeAvg[7] = (dRDTimeAvg[7] / 4) * dTSLLow;
        }


        public void CalculateRDTimeT15(ref TestSession pTestSession)
        {

            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // double tmpAvgRDTime = 0;

                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    // do your formulas and populate
                    //tmpAvgRDTime = tmpAvgRDTime + ( screen.ReadTimeInSeconds * 100 );
                    dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);
                    listRDTime.Add(dRDTime[i]);
                    //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);
                    // listRDTime.Add(screen.ReadTimeInSeconds * 100);
                }

                //CalculateRDTimeAvgT15(ref pTestSession);

                // tmpAvgRDTime = tmpAvgRDTime / pTestSession.lstScreenQuestionOnly.Count;
                // listRDTime.Add(tmpAvgRDTime);
            }

        }

        public void CalculateE15(ref TestSession pTestSession)
        {
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                Screen screen = null;
                // Go through the all the questions
                for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    dE[i] = (double)(screen.ReadTimeInSeconds * 100);
                }
            }
        }


        // Average the button was pressed
        public void CalculateI15(ref TestSession pTestSession)
        {
            // Calculate the first 5 cells
            // Question 3, 4, 5 all RD time or Read Time
            Screen screen3 = null;
            Screen screen4 = null;
            Screen screen5 = null;
            screen3 = (Screen)pTestSession.lstScreenQuestionOnly[2];
            screen4 = (Screen)pTestSession.lstScreenQuestionOnly[3];
            screen5 = (Screen)pTestSession.lstScreenQuestionOnly[4];
            for (int i = 1; i <= 5; i++)
            {
                dI[i] = (((double)screen3.ReadTimeInSeconds) +
                          ((double)screen4.ReadTimeInSeconds) +
                          ((double)screen5.ReadTimeInSeconds)) * 100 / 3;

                //dI[i] = (((double)System.Convert.ToSingle(sField[17, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 6, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 12, TotalRow - 1]))) * 100 / 3;
            }

            // Calculate the next 3 cells
            Screen screen6 = null;
            Screen screen7 = null;
            Screen screen8 = null;
            screen6 = (Screen)pTestSession.lstScreenQuestionOnly[5];
            screen7 = (Screen)pTestSession.lstScreenQuestionOnly[6];
            screen8 = (Screen)pTestSession.lstScreenQuestionOnly[7];
            for (int i = 6; i <= 8; i++)
            {
                dI[i] = (((double)screen6.ReadTimeInSeconds) +
                         ((double)screen7.ReadTimeInSeconds) +
                         ((double)screen8.ReadTimeInSeconds)) * 100 / 3;
                //dI[i] = (((double)System.Convert.ToSingle(sField[17 + 18, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 24, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 30, TotalRow - 1]))) * 100 / 3;
            }


            // Calculate the next 3 cells
            Screen screen9 = null;
            Screen screen10 = null;
            Screen screen11 = null;
            screen9 = (Screen)pTestSession.lstScreenQuestionOnly[8];
            screen10 = (Screen)pTestSession.lstScreenQuestionOnly[9];
            screen11 = (Screen)pTestSession.lstScreenQuestionOnly[10];
            for (int i = 9; i <= 11; i++)
            {
                dI[i] = (((double)screen9.ReadTimeInSeconds) +
                         ((double)screen10.ReadTimeInSeconds) +
                         ((double)screen11.ReadTimeInSeconds)) * 100 / 3;
                //dI[i] = (((double)System.Convert.ToSingle(sField[17 + 36, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 42, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 48, TotalRow - 1]))) * 100 / 3;
            }

            // Calculate the next 4 cells
            Screen screen12 = null;
            Screen screen13 = null;
            Screen screen14 = null;
            screen12 = (Screen)pTestSession.lstScreenQuestionOnly[11];
            screen13 = (Screen)pTestSession.lstScreenQuestionOnly[12];
            screen14 = (Screen)pTestSession.lstScreenQuestionOnly[13];
            for (int i = 12; i <= pTestSession.lstScreenQuestionOnly.Count; i++)
            {
                dI[i] = (((double)screen12.ReadTimeInSeconds) +
                         ((double)screen13.ReadTimeInSeconds) +
                         ((double)screen14.ReadTimeInSeconds)) * 100 / 3;
                //dI[i] = (((double)System.Convert.ToSingle(sField[17 + 54, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 60, TotalRow - 1])) +
                //    ((double)System.Convert.ToSingle(sField[17 + 66, TotalRow - 1]))) * 100 / 3;
                ////				MessageBox.Show("Average[" + i + "]= "+ dI[i] );
            }
        }

        public void CalculateJ15()
        {
            for (int i = 1; i <= MAXQUESTIONS_15; i++)
            {
                dJ[i] = ((dE[i] + dI[i]) / 2);
            }
        }

        public void CalculateK15()
        {
            for (int i = 1; i <= MAXQUESTIONS_15; i++)
            {
                dK[i] = dI[i] - dE[i];
            }
        }

        public void CalculateL15()
        {

            for (int i = 1; i <= MAXQUESTIONS_15; i++)
            {
                dL[i] = Math.Abs(dK[i]);
            }
        }

        public void CalculateM15()
        {

            for (int i = 1; i <= MAXQUESTIONS_15; i++)
            {
                dM[i] = dK[i] + dL[i];
            }
        }

        public void CalculateN15()
        {
            for (int i = 1; i <= MAXQUESTIONS_15; i++)
            {
                dN[i] = (dJ[i] + dM[i]) / 2;
            }
        }


        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenG15()
        {

            int d = 5;
            int x = 0;
            int y = 3;
            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                if (x == 2) { y = 3; };
                if (x == 5) { y = 3; };
                if (dRDTime[d++] > dRDTimeAvg[6])
                {
                    iHiddenG[x] = y++;
                }
                else
                {
                    iHiddenG[x] = 0;
                }
            }
        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenH15()
        {
            int d = 5;
            int x = 0;
            int y = 3;
            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                if (x == 2) { y = 3; };
                if (x == 5) { y = 3; };
                if (dRDTime[d++] < dRDTimeAvg[7])
                {
                    iHiddenH[x] = y++;
                }
                else
                {
                    iHiddenH[x] = 0;
                }
            }
        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenI15()
        {
            int x = 0;

            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                iHiddenI[x] = Convert.ToInt32(iHiddenG[x] + iHiddenH[x]);
            }
        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenJ15(ref TestSession pTestSession)
        {
            int x = 0;
            int iQuestion = 5; // questions 6 to 14 only
            Screen screen = null;
            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                screen = (Screen)pTestSession.lstScreenQuestionOnly[iQuestion++];
                if (screen.Answer)
                {
                    iHiddenJ[x] = 12;
                }
                else
                {
                    iHiddenJ[x] = 0;
                }
            }
        }

        // used in the new spread sheet with Graph.  Does not display on Viewer.
        public void CalculateHiddenK15()
        {
            int x = 0;

            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                iHiddenK[x] = iHiddenI[x] + iHiddenJ[x];
            }
        }

        // used in the new spread sheet with Graph.
        public void CalculateReaction15(ref TestSession pTestSession)
        {
            int x = 0;
            Screen screen = null;
            int iQuestion = 5; // Question 6 to 14 only
            for (x = 0; x <= SIZEOFDATA - 2; x++)
            {
                screen = (Screen)pTestSession.lstScreenQuestionOnly[iQuestion++];
                if (iHiddenK[x] > 1)
                {
                    screen.IsHit = true;
                    //bReaction[x] = "HIT";
                }
                else
                {
                    screen.IsHit = false;
                    //bReaction[x] = "No";
                }
            }
        }

        public void getStandardDeviation15()
        {
            double SumOfSqrs = 0;
            double avg = dRDTimeAvg[0];

            StandardDeviation = 0;
            for (int i = 0; i < listRDTime.Count - 1; i++)
            {

                SumOfSqrs += Math.Pow(((double)listRDTime[i] - avg), 2);
            }
            double n = (double)listRDTime.Count;
            StandardDeviation = Math.Sqrt(SumOfSqrs / (n - 1));
            //MessageBox.Show("STD=" + StandardDeviation.ToString("###.##"));
            //MessageBox.Show("SUMOFSQRS=" + SumOfSqrs.ToString("###.##"));
            //MessageBox.Show("AVG=" + avg.ToString("###.##"));
            //MessageBox.Show("LIST COUNT=" + listRDTime.Count.ToString("###.##"));
        }

        public void CalculateReadReactionDistortion15(ref TestSession pTestSession)
        {

            Screen screen = null;
            double Sum1 = 0;  // for questions 6, 7, 8
            int j = 0;
            double Sum2 = 0;  // for questions 4,5,6,9 - 15
            int k = 0;

            // Calculate the Read Reactions 
            if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
            {
                // Calculate the 1st Reactions questions 6, 7, 8
                for (int i = 5; i <= 7; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum1 += screen.DeltaClickTimeInSeconds;
                    j++;
                }
                ReadReaction1 = (double)(Sum1 / (j));

                // Calcualte the 2nd Reactions question 4,5,6, 9 to 15
                for (int i = 3; i <= 5; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum2 += screen.DeltaClickTimeInSeconds;
                    k++;
                }
                for (int i = 8; i < pTestSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
                    Sum2 += screen.DeltaClickTimeInSeconds;
                    k++;
                }
                ReadReaction2 = (double)(Sum2 / k);
            }

            // Calculate the Distortion
            Distortion = false;
            if (ReadReaction1 < (iReadReaction * ReadReaction2))
            {
                Distortion = true;
            }
        }

    }


    enum LowTSLValues
    {
        TSL_LOW_ZERO_PRECENT,
        TSL_LOW_ONE_PERCENT,
        TSL_LOW_TWO_PERCENT,
        TSL_LOW_THREE_PERCENT,
        TSL_LOW_FOUR_PERCENT,
        TSL_LOW_FIVE_PERCENT,
        TSL_LOW_SIX_PERCENT,
        TSL_LOW_SEVEN_PERCENT,
        TSL_LOW_EIGHT_PERCENT,
        TSL_LOW_NINE_PERCENT,
        TSL_LOW_TEN_PERCENT,
        TSL_LOW_ELEVEN_PERCENT,
        TSL_LOW_TWELVE_PERCENT,
        TSL_LOW_THIRTEEN_PERCENT,
        TSL_LOW_FOURTEEN_PERCENT,
        TSL_LOW_FIFTHTEEN_PERCENT,
        TSL_LOW_SIXTEEN_PERCENT,
        TSL_LOW_SEVENTEEN_PERCENT,
        TSL_LOW_EIGHTEEN_PERCENT,
        TSL_LOW_NINETEEN_PERCENT,
        TSL_LOW_TWENTY_PERCENT,
        TSL_LOW_MAX = TSL_LOW_TWENTY_PERCENT
    };

    enum NumberReaction
    {
        REACTION_ZERO,
        REACTION_ONE,
        REACTION_TWO,
        REACTION_THREE,
        REACTION_FOUR,
        REACTION_FIVE,
        REACTION_SIX,
        REACTION_SEVEN,
        REACTION_EIGHT,
        REACTION_NINE,
        REACTION_TEN,
        REACTION_MAX = REACTION_TEN
    };

    /// <summary>
    /// //////////////////////////////////////////////////////////////////////////////
    /// ////////
    /// ///////  BASE ANALYZER
    /// ////////
    /// ///////////////////////////////////////////////////////////////////////////////
    /// </summary>
    public class BaseAnalyzer
    {

        protected const int MAXQUESTIONS_15 = 15;
        protected const int MAX = 100;
        protected const int SIZEOFDATA = 10;
        protected const int MAXQUESTIONS_KOT = 16;




        //const int iReadReaction = 1; // Check the Viewer is also hard coded

        //public string sTSLHigh = "1.3";
        //public string sTSLLow = "0.7";

        // this is used for the standard deviation
        protected List<double> _listRDTime = new List<double>();

        public List<double> listRDTime
        {
            get { return _listRDTime; }
            set { _listRDTime = value; }
        }


        // this is the TSL MINIMUM
        // if there are no SPRs when we hit TSL LOW, then keep going until the MINIMUM
        private double _dTSLMinimum = Convert.ToDouble("20.0");
        public double dTSLMinimum
        {
            get { return _dTSLMinimum; }
            set { _dTSLMinimum = value; }
        }


        // this is the TSL LOW
        private double _dTSLLow = Convert.ToDouble("30.0");
        public double dTSLLow
        {
            get { return _dTSLLow; }
            set { _dTSLLow = value; }
        }

        // this is the TSL HIGH
        private double _dTSLHIGH = Convert.ToDouble("1.3");
        public double dTSLHIGH
        {
            get { return _dTSLHIGH; }
            set { _dTSLHIGH = value; }
        }



        // this is the TSL LOW
        private double _dTSLLowReadTime = Convert.ToDouble("30.0");
        public double dTSLLowReadTime
        {
            get { return _dTSLLowReadTime; }
            set { _dTSLLowReadTime = value; }
        }

        // this is the TSL HIGH
        private double _dTSLHIGHReadTime = Convert.ToDouble("1.3");
        public double dTSLHIGHReadTime
        {
            get { return _dTSLHIGHReadTime; }
            set { _dTSLHIGHReadTime = value; }
        }


        // this is the TSL LOW KOT
        private double _dTSLLowKOT = Convert.ToDouble("0.95");
        public double dTSLLowKOT
        {
            get { return _dTSLLowKOT; }
            set { _dTSLLowKOT = value; }
        }

        // this is the TSL HIGH KOT
        private double _dTSLHIGHKOT = Convert.ToDouble("1.05");
        public double dTSLHIGHKOT
        {
            get { return _dTSLHIGHKOT; }
            set { _dTSLHIGHKOT = value; }
        }

        // this is the TSL ADJUST for KOT
        private double _dTSLAdjustKOT = Convert.ToDouble("30.00");
        public double dTSLAdjustKOT
        {
            get { return _dTSLAdjustKOT; }
            set { _dTSLAdjustKOT = value; }
        }

        // this is the TSL ADJUST for 15 Questions
        private double _dTSLAdjust15Q = Convert.ToDouble("30.00");
        public double dTSLAdjust15Q
        {
            get { return _dTSLAdjust15Q; }
            set { _dTSLAdjust15Q = value; }
        }

        // this is the TSL ADJUST for 24 Questions
        private double _dTSLAdjust24Q = Convert.ToDouble("37.00");
        public double dTSLAdjust24Q
        {
            get { return _dTSLAdjust24Q; }
            set { _dTSLAdjust24Q = value; }
        }

        // this is the TSL ADJUST for 26 Questions
        private double _dTSLAdjust26Q = Convert.ToDouble("37.00");
        public double dTSLAdjust26Q
        {
            get { return _dTSLAdjust26Q; }
            set { _dTSLAdjust26Q = value; }
        }

        // this is the RD Time * 100
        private double[] _dRDTime = new double[MAX];

        public double[] dRDTime
        {
            get { return _dRDTime; }
            set { _dRDTime = value; }
        }

        // 24 Questions - Used for the Distortion logic
        private double _Reaction = 0.3;
        public double iReadReaction
        {
            get { return _Reaction; }
            set { _Reaction = value; }
        }

        // 24 Questions - Read Reaction for question 4 to 15
        private double _ReadReaction1 = 0;

        public double ReadReaction1
        {
            get { return _ReadReaction1; }
            set { _ReadReaction1 = value; }
        }

        // 24 Questions - Read Reaction for question 16 to 25
        private double _ReadReaction2 = 0;

        public double ReadReaction2
        {
            get { return _ReadReaction2; }
            set { _ReadReaction2 = value; }
        }

        // 24 Questions - Any Distortion from ReadReaction 1 and 2
        private bool _Distortion = false;

        public bool Distortion
        {
            get { return _Distortion; }
            set { _Distortion = value; }
        }

        // TSL PERCENTAGE
        // If we get more than one hit per zone, then we can assume some caution
        private int _TSLPercentage = 30;
        public int TSLPercentage
        {
            get { return _TSLPercentage; }
            set { _TSLPercentage = value; }
        }

        // Reaction Zone 1 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone1Counter1 = 0;
        public int ReactionZone1Counter1
        {
            get { return _ReactionZone1Counter1; }
            set { _ReactionZone1Counter1 = value; }
        }
        private int _ReactionZone1Counter2 = 0;
        public int ReactionZone1Counter2
        {
            get { return _ReactionZone1Counter2; }
            set { _ReactionZone1Counter2 = value; }
        }
        private int _ReactionZone1Counter3 = 0;
        public int ReactionZone1Counter3
        {
            get { return _ReactionZone1Counter3; }
            set { _ReactionZone1Counter3 = value; }
        }
        private int _ReactionZone1Counter = 0;
        public int ReactionZone1Counter
        {
            get { return _ReactionZone1Counter; }
            set { _ReactionZone1Counter = value; }
        }

        // Reaction Zone 2 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone2Counter1 = 0;
        public int ReactionZone2Counter1
        {
            get { return _ReactionZone2Counter1; }
            set { _ReactionZone2Counter1 = value; }
        }
        private int _ReactionZone2Counter2 = 0;
        public int ReactionZone2Counter2
        {
            get { return _ReactionZone2Counter2; }
            set { _ReactionZone2Counter2 = value; }
        }
        private int _ReactionZone2Counter3 = 0;
        public int ReactionZone2Counter3
        {
            get { return _ReactionZone2Counter3; }
            set { _ReactionZone2Counter3 = value; }
        }
        private int _ReactionZone2Counter = 0;
        public int ReactionZone2Counter
        {
            get { return _ReactionZone2Counter; }
            set { _ReactionZone2Counter = value; }
        }

        // Reaction Zone 3 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone3Counter1 = 0;
        public int ReactionZone3Counter1
        {
            get { return _ReactionZone3Counter1; }
            set { _ReactionZone3Counter1 = value; }
        }
        private int _ReactionZone3Counter2 = 0;
        public int ReactionZone3Counter2
        {
            get { return _ReactionZone3Counter2; }
            set { _ReactionZone3Counter2 = value; }
        }
        private int _ReactionZone3Counter3 = 0;
        public int ReactionZone3Counter3
        {
            get { return _ReactionZone3Counter3; }
            set { _ReactionZone3Counter3 = value; }
        }
        private int _ReactionZone3Counter = 0;
        public int ReactionZone3Counter
        {
            get { return _ReactionZone3Counter; }
            set { _ReactionZone3Counter = value; }
        }

        // Reaction Zone 4 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone4Counter1 = 0;
        public int ReactionZone4Counter1
        {
            get { return _ReactionZone4Counter1; }
            set { _ReactionZone4Counter1 = value; }
        }
        private int _ReactionZone4Counter2 = 0;
        public int ReactionZone4Counter2
        {
            get { return _ReactionZone4Counter2; }
            set { _ReactionZone4Counter2 = value; }
        }
        private int _ReactionZone4Counter3 = 0;
        public int ReactionZone4Counter3
        {
            get { return _ReactionZone4Counter3; }
            set { _ReactionZone4Counter3 = value; }
        }
        private int _ReactionZone4Counter = 0;
        public int ReactionZone4Counter
        {
            get { return _ReactionZone4Counter; }
            set { _ReactionZone4Counter = value; }
        }

        // Reaction Zone 5 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone5Counter1 = 0;
        public int ReactionZone5Counter1
        {
            get { return _ReactionZone5Counter1; }
            set { _ReactionZone5Counter1 = value; }
        }
        private int _ReactionZone5Counter2 = 0;
        public int ReactionZone5Counter2
        {
            get { return _ReactionZone5Counter2; }
            set { _ReactionZone5Counter2 = value; }
        }
        private int _ReactionZone5Counter3 = 0;
        public int ReactionZone5Counter3
        {
            get { return _ReactionZone5Counter3; }
            set { _ReactionZone5Counter3 = value; }
        }
        private int _ReactionZone5Counter = 0;
        public int ReactionZone5Counter
        {
            get { return _ReactionZone5Counter; }
            set { _ReactionZone5Counter = value; }
        }

        // Reaction Zone 6 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone6Counter1 = 0;
        public int ReactionZone6Counter1
        {
            get { return _ReactionZone6Counter1; }
            set { _ReactionZone6Counter1 = value; }
        }
        private int _ReactionZone6Counter2 = 0;
        public int ReactionZone6Counter2
        {
            get { return _ReactionZone6Counter2; }
            set { _ReactionZone6Counter2 = value; }
        }
        private int _ReactionZone6Counter3 = 0;
        public int ReactionZone6Counter3
        {
            get { return _ReactionZone6Counter3; }
            set { _ReactionZone6Counter3 = value; }
        }
        private int _ReactionZone6Counter = 0;
        public int ReactionZone6Counter
        {
            get { return _ReactionZone6Counter; }
            set { _ReactionZone6Counter = value; }
        }

        // Reaction Zone 7 Counter
        // If we get more than one hit per zone, then we can assume some caution
        private int _ReactionZone7Counter1 = 0;
        public int ReactionZone7Counter1
        {
            get { return _ReactionZone7Counter1; }
            set { _ReactionZone7Counter1 = value; }
        }
        private int _ReactionZone7Counter2 = 0;
        public int ReactionZone7Counter2
        {
            get { return _ReactionZone7Counter2; }
            set { _ReactionZone7Counter2 = value; }
        }
        private int _ReactionZone7Counter3 = 0;
        public int ReactionZone7Counter3
        {
            get { return _ReactionZone7Counter3; }
            set { _ReactionZone7Counter3 = value; }
        }
        private int _ReactionZone7Counter = 0;
        public int ReactionZone7Counter
        {
            get { return _ReactionZone7Counter; }
            set { _ReactionZone7Counter = value; }
        }



        // SPR 1
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR1 = new int[MAX];

        public int[] bGreaterTSLHighSPR1
        {
            get { return _GreaterTSLHighSPR1; }
            set { _GreaterTSLHighSPR1 = value; }
        }

        // SPR 1
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR1 = new int[MAX];

        public int[] bGreaterTSLLowSPR1
        {
            get { return _GreaterTSLLowSPR1; }
            set { _GreaterTSLLowSPR1 = value; }
        }






        // SPR2
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR2 = new int[MAX];

        public int[] bGreaterTSLHighSPR2
        {
            get { return _GreaterTSLHighSPR2; }
            set { _GreaterTSLHighSPR2 = value; }
        }

        // SPR2
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR2 = new int[MAX];

        public int[] bGreaterTSLLowSPR2
        {
            get { return _GreaterTSLLowSPR2; }
            set { _GreaterTSLLowSPR2 = value; }
        }




        // SPR3
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR3 = new int[MAX];

        public int[] bGreaterTSLHighSPR3
        {
            get { return _GreaterTSLHighSPR3; }
            set { _GreaterTSLHighSPR3 = value; }
        }

        // SPR3
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR3 = new int[MAX];

        public int[] bGreaterTSLLowSPR3
        {
            get { return _GreaterTSLLowSPR3; }
            set { _GreaterTSLLowSPR3 = value; }
        }






        // SPR4
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR4 = new int[MAX];

        public int[] bGreaterTSLHighSPR4
        {
            get { return _GreaterTSLHighSPR4; }
            set { _GreaterTSLHighSPR4 = value; }
        }

        // SPR4
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR4 = new int[MAX];

        public int[] bGreaterTSLLowSPR4
        {
            get { return _GreaterTSLLowSPR4; }
            set { _GreaterTSLLowSPR4 = value; }
        }





        // SPR5
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR5 = new int[MAX];

        public int[] bGreaterTSLHighSPR5
        {
            get { return _GreaterTSLHighSPR5; }
            set { _GreaterTSLHighSPR5 = value; }
        }

        // SPR5
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR5 = new int[MAX];

        public int[] bGreaterTSLLowSPR5
        {
            get { return _GreaterTSLLowSPR5; }
            set { _GreaterTSLLowSPR5 = value; }
        }





        // SPR6
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR6 = new int[MAX];

        public int[] bGreaterTSLHighSPR6
        {
            get { return _GreaterTSLHighSPR6; }
            set { _GreaterTSLHighSPR6 = value; }
        }

        // SPR6
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR6 = new int[MAX];

        public int[] bGreaterTSLLowSPR6
        {
            get { return _GreaterTSLLowSPR6; }
            set { _GreaterTSLLowSPR6 = value; }
        }





        // SPR7
        // 26 Question - Hideen U - Greater than TSL High
        private int[] _GreaterTSLHighSPR7 = new int[MAX];

        public int[] bGreaterTSLHighSPR7
        {
            get { return _GreaterTSLHighSPR7; }
            set { _GreaterTSLHighSPR7 = value; }
        }

        // SPR7
        // 26 Question - Hidden V - Less than TSL Low
        private int[] _GreaterTSLLowSPR7 = new int[MAX];

        public int[] bGreaterTSLLowSPR7
        {
            get { return _GreaterTSLLowSPR7; }
            set { _GreaterTSLLowSPR7 = value; }
        }





        // Z axis data
        private double[] _dZTime = new double[MAX];

        public double[] dZTime
        {
            get { return _dZTime; }
            set { _dZTime = value; }
        }

        // 24 Question - Sum X and Z
        private double[] _dSumXZ = new double[MAX];

        public double[] dSumXZ
        {
            get { return _dSumXZ; }
            set { _dSumXZ = value; }
        }


        // 24 Question - Abs(X-Z)
        private double[] _dAbsXZ = new double[MAX];

        public double[] dAbsXZ
        {
            get { return _dAbsXZ; }
            set { _dAbsXZ = value; }
        }

        // 24 Question - Sub(Z-X)
        private double[] _dSubZX = new double[MAX];

        public double[] dSubZX
        {
            get { return _dSubZX; }
            set { _dSubZX = value; }
        }


        // 24 Question - SUM(E:I)
        private double[] _dSumEI = new double[MAX];

        public double[] dSumEI
        {
            get { return _dSumEI; }
            set { _dSumEI = value; }
        }

        // 24 Question - this is used for the standard deviation
        private List<double> _listSumEI = new List<double>();

        public List<double> listSumEI
        {
            get { return _listSumEI; }
            set { _listSumEI = value; }
        }

        // 24 Question - Hidden O - Greater than TSL High
        private int[] _GreaterTSLHigh = new int[MAX];

        public int[] bGreaterTSLHigh
        {
            get { return _GreaterTSLHigh; }
            set { _GreaterTSLHigh = value; }
        }

        // 24 Question - Hidden P - Less than TSL Low
        private int[] _GreaterTSLLow = new int[MAX];

        public int[] bGreaterTSLLow
        {
            get { return _GreaterTSLLow; }
            set { _GreaterTSLLow = value; }
        }


        // 24 Question - Hidden Q - weight
        private int[] _Weight = new int[MAX];

        public int[] Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        // 24 Question - Hidden R - weight Answer
        private int[] _iWeightAnswer = new int[MAX];

        public int[] iWeightAnswer
        {
            get { return _iWeightAnswer; }
            set { _iWeightAnswer = value; }
        }

        // 24 Question - Hidden S - weight Total
        private int[] _iWeightTotal = new int[MAX];

        public int[] iWeightTotal
        {
            get { return _iWeightTotal; }
            set { _iWeightTotal = value; }
        }


        // 24 Question - Hidden T - Zone weight
        private int[] _iZoneWeightTotal = new int[7];

        public int[] iZoneWeightTotal
        {
            get { return _iZoneWeightTotal; }
            set { _iZoneWeightTotal = value; }
        }


        // Cell E
        private double[] _dE = new double[MAX];

        public double[] dE
        {
            get { return _dE; }
            set { _dE = value; }
        }

        // Cell I
        private double[] _dI = new double[MAX];

        public double[] dI
        {
            get { return _dI; }
            set { _dI = value; }
        }

        // Cell J
        private double[] _dJ = new double[MAX];

        public double[] dJ
        {
            get { return _dJ; }
            set { _dJ = value; }
        }

        // Cell K
        private double[] _dK = new double[MAX];

        public double[] dK
        {
            get { return _dK; }
            set { _dK = value; }
        }

        // Cell L
        private double[] _dL = new double[MAX];

        public double[] dL
        {
            get { return _dL; }
            set { _dL = value; }
        }


        // Cell M
        private double[] _dM = new double[MAX];

        public double[] dM
        {
            get { return _dM; }
            set { _dM = value; }
        }

        // Cell N
        private double[] _dN = new double[MAX];

        public double[] dN
        {
            get { return _dN; }
            set { _dN = value; }
        }

        // Cell HiddenG
        private double[] _iHiddenG = new double[SIZEOFDATA];

        public double[] iHiddenG
        {
            get { return _iHiddenG; }
            set { _iHiddenG = value; }
        }

        // Cell HiddenH
        private double[] _iHiddenH = new double[SIZEOFDATA];

        public double[] iHiddenH
        {
            get { return _iHiddenH; }
            set { _iHiddenH = value; }
        }


        // Cell HiddenI
        private int[] _iHiddenI = new int[SIZEOFDATA];

        public int[] iHiddenI
        {
            get { return _iHiddenI; }
            set { _iHiddenI = value; }
        }

        // Cell HiddenJ
        private int[] _iHiddenJ = new int[SIZEOFDATA];

        public int[] iHiddenJ
        {
            get { return _iHiddenJ; }
            set { _iHiddenJ = value; }
        }


        // Cell HiddenK
        private int[] _iHiddenK = new int[SIZEOFDATA];

        public int[] iHiddenK
        {
            get { return _iHiddenK; }
            set { _iHiddenK = value; }
        }


        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        // Used for the KOT TESTS
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////
        ////////////////////////////////////////////////

        // Cell HiddenL for KOT
        private int[] _iHiddenL = new int[MAXQUESTIONS_KOT];

        public int[] iHiddenL
        {
            get { return _iHiddenL; }
            set { _iHiddenL = value; }
        }


        // Cell dReadTimeAvgKOT for KOT
        private double _dReadTimeAvgKOT = 0;

        public double dReadTimeAvgKOT
        {
            get { return _dReadTimeAvgKOT; }
            set { _dReadTimeAvgKOT = value; }
        }

        // Cell dSurveyAvgKOT for KOT
        private double _dSurveyAvgKOT = 0;

        public double dSurveyAvgKOT
        {
            get { return _dSurveyAvgKOT; }
            set { _dSurveyAvgKOT = value; }
        }


        // Cell iReactionSumWeapons for KOT
        private int _iReactionSumWeapons = 0;

        public int iReactionSumWeapons
        {
            get { return _iReactionSumWeapons; }
            set { _iReactionSumWeapons = value; }
        }

        // Cell iReactionSumSmuggling for KOT
        private int _iReactionSumSmuggling = 0;

        public int iReactionSumSmuggling
        {
            get { return _iReactionSumSmuggling; }
            set { _iReactionSumSmuggling = value; }
        }

        // Cell iReactionSumTerrorism for KOT
        private int _iReactionSumTerrorism = 0;

        public int iReactionSumTerrorism
        {
            get { return _iReactionSumTerrorism; }
            set { _iReactionSumTerrorism = value; }
        }

        // Cell sReaction for KOT
        private string[] _sReaction = new string[SIZEOFDATA];

        public string[] sReaction
        {
            get { return _sReaction; }
            set { _sReaction = value; }
        }

        // Cell HiddenLAverage for KOT
        private int _iHiddenLAverage = 0;

        public int iHiddenLAverage
        {
            get { return _iHiddenLAverage; }
            set { _iHiddenLAverage = value; }
        }

        public string sValidTest = "Valid Test";


        // Cell dResultsKOT for KOT
        private double[] _dResultsKOT = new double[MAX];

        public double[] dResultsKOT
        {
            get { return _dResultsKOT; }
            set { _dResultsKOT = value; }
        }

        // Cell dZones for KOT
        private double[] _dZonesKOT = new double[MAX];

        public double[] dZonesKOT
        {
            get { return _dZonesKOT; }
            set { _dZonesKOT = value; }
        }

        //
        //
        //
        //
        //
        // 18 Question declararation
        //
        //
        //
        //



        // Risk Analysis Summary
        // Possible Strings are:
        //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
        //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
        //     If more than 2 SPRs, then "Further Investigation Is Necessary"
        //     If ANY Admits, then "Further Investigation Is Necessary"
        private string _strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
        public string strRiskAnalysisVeracityRecommend
        {
            get { return _strRiskAnalysisVeracityRecommend; }
            set { _strRiskAnalysisVeracityRecommend = value; }
        }


        private string _strRiskAnalysis = "Low Risk";
        public string strRiskAnalysis
        {
            get { return _strRiskAnalysis; }
            set { _strRiskAnalysis = value; }
        }

        // new RISK SCREEN values
        private string _strRiskAnalysisIssueA = "Low Risk";
        public string strRiskAnalysisIssueA
        {
            get { return _strRiskAnalysisIssueA; }
            set { _strRiskAnalysisIssueA = value; }
        }

        private string _strRiskAnalysisIssueB = "Low Risk";
        public string strRiskAnalysisIssueB
        {
            get { return _strRiskAnalysisIssueB; }
            set { _strRiskAnalysisIssueB = value; }
        }

        private string _strRiskAnalysisIssueC = "Low Risk";
        public string strRiskAnalysisIssueC
        {
            get { return _strRiskAnalysisIssueC; }
            set { _strRiskAnalysisIssueC = value; }
        }


        private int _iRiskAnalysisTotalAdmits = 0 ;
        public int iRiskAnalysisTotalAdmits
        {
            get { return _iRiskAnalysisTotalAdmits; }
            set { _iRiskAnalysisTotalAdmits = value; }
        }

        private int _iRiskAnalysisTotalCaution = 0;
        public int iRiskAnalysisTotalCaution
        {
            get { return _iRiskAnalysisTotalCaution; }
            set { _iRiskAnalysisTotalCaution = value; }
        }


        // this is the TSL ADJUST for 18 Questions
        private bool _bTurnOnVerifyDistortLogic = false;
        public bool bTurnOnVerifyDistortLogic
        {
            get { return _bTurnOnVerifyDistortLogic; }
            set { _bTurnOnVerifyDistortLogic = value; }
        }




        // this is the TSL ADJUST for 18 Questions
        private double _dTSLAdjust18Q = Convert.ToDouble("37.00");
        public double dTSLAdjust18Q
        {
            get { return _dTSLAdjust18Q; }
            set { _dTSLAdjust18Q = value; }
        }

        // Cell dReadTimeAvg18 for 18 Q
        private double _dReadTimeAvg18 = 0;

        public double dReadTimeAvg18
        {
            get { return _dReadTimeAvg18; }
            set { _dReadTimeAvg18 = value; }
        }

        // Cell dReadTimeTotal18 for 18 Q
        private double _dReadTimeTotal18 = 0;

        public double dReadTimeTotal18
        {
            get { return _dReadTimeTotal18; }
            set { _dReadTimeTotal18 = value; }
        }

        // Cell dReadTimeRelevantAvg18 for 18 Q
        private double _dReadTimeRelevantAvg18 = 0;

        public double dReadTimeRelevantAvg18
        {
            get { return _dReadTimeRelevantAvg18; }
            set { _dReadTimeRelevantAvg18 = value; }
        }


        // Cell dReadTimeIRAvg18 for 18 Q
        private double _dReadTimeIRAvg18 = 0;

        public double dReadTimeIRAvg18
        {
            get { return _dReadTimeIRAvg18; }
            set { _dReadTimeIRAvg18 = value; }
        }

        // 18 Question - Which Questions were DISTORTED or changed during the VerifyIfWeHaveADistortInReactionData26() function
        private int[] _QuestionThatWasDistortDuringMultipleCodeLogic18 = new int[MAX];

        public int[] QuestionThatWasDistortDuringMultipleCodeLogic18
        {
            get { return _QuestionThatWasDistortDuringMultipleCodeLogic18; }
            set { _QuestionThatWasDistortDuringMultipleCodeLogic18 = value; }
        }

        // Used to turn off the extra SPRs for the calculation
        // 18 Q used
        private bool _bTurnOffExtraSPRWeightCalculation = false;

        public bool bTurnOffExtraSPRWeightCalculation
        {
            get { return _bTurnOffExtraSPRWeightCalculation; }
            set { _bTurnOffExtraSPRWeightCalculation = value; }
        }


        // WE NEED TO MAKE THE CONFIGURATION SO WE CAN TURN ON/OFF THE
        // SPRs AT WILL
        private bool _bTurnOnSPR1 = true;

        public bool bTurnOnSPR1
        {
            get { return _bTurnOnSPR1; }
            set { _bTurnOnSPR1 = value; }
        }
        private bool _bTurnOnSPR3 = true;

        public bool bTurnOnSPR3
        {
            get { return _bTurnOnSPR3; }
            set { _bTurnOnSPR3 = value; }
        }
        private bool _bTurnOnSPR2 = true;

        public bool bTurnOnSPR2
        {
            get { return _bTurnOnSPR2; }
            set { _bTurnOnSPR2 = value; }
        }

        private bool _bTurnOnRiskAnalysis = true;

        public bool bTurnOnRiskAnalysis
        {
            get { return _bTurnOnRiskAnalysis; }
            set { _bTurnOnRiskAnalysis = value; }
        }
        private bool _bTurnOnSPR71115 = true;

        public bool bTurnOnSPR71115
        {
            get { return _bTurnOnSPR71115; }
            set { _bTurnOnSPR71115 = value; }
        }

        private bool _bTurnOnMovingTestAverage = true;

        public bool bTurnOnMovingTestAverage
        {
            get { return _bTurnOnMovingTestAverage; }
            set { _bTurnOnMovingTestAverage = value; }
        }
        // index 0 = Survey Average
        // index 1 = Travelling Average lines 3 to 5
        // index 2 = Weapons Average lines 6 to 8
        // index 3 = Smuggling Average lines 9 to 11
        // index 4 = Terrorism Average lines 12 to 14
        // index 5 = ALL Average lines 5 to 13
        // index 6 = TSL High
        // index 7 = TSL Low
        //
        //
        // For the 26 Q sherif report
        // index 0 = Calculate TEST Average Line but SKIP first 4 questions, 5 to 25
        // index 1 = Base Average Q5, Q9, Q13, Q17, Q21, Q25
        // index 2 = Base Average Q5 + Q9
        // index 3 = Base Average Q9 + Q13
        // index 4 = Base Average Q13 + Q17
        // index 5 = Base Average Q17 + Q21
        // index 6 = Base Average Q21 + Q25
        // index 7 = Calculate Day of Week Accidents Reports Questions 6 to 8 Line 
        // index 8 = Calculate Month of YEAR - DRUGS Question 10 - 12
        // index 9 = Calculate Year - SEX OFFENDER Question 14 - 16
        // index 10 = Calculate Day of Week - Arrest/Convictions - Question 18 - 20
        // index 11 = Calculate Month of Year - Smuggling - Question 22 - 24
        // index 12 = Calculate TSL High
        // index 13 = Calculate TSL Low
        // index 14 = Averge of index 7 to 9 - Relevelent Q Base line Averages
        // index 15 = Averge of index 8 to 10 - Relevelent Q Base line Averages
        // index 16 = Averge of index 9 to 11 - Relevelent Q Base line Averages
        //
        // Reaction Data Averages
        private double[] _dRDTimeAvg = new double[MAX];
        public double[] dRDTimeAvg
        {
            get { return _dRDTimeAvg; }
            set { _dRDTimeAvg = value; }
        }

        //
        // READ TIME AVERAGES
        //
        // index 0 = Survey Average
        // index 1 = Travelling Average lines 3 to 5
        // index 2 = Weapons Average lines 6 to 8
        // index 3 = Smuggling Average lines 9 to 11
        // index 4 = Terrorism Average lines 12 to 14
        // index 5 = ALL Average lines 5 to 13
        // index 6 = TSL High
        // index 7 = TSL Low
        //
        //
        // For the 18 Q 
        // index 0 = Calculate TEST Average Line but SKIP first 4 questions, 5 to 25
        // index 1 = Base Average Q5, Q9, Q13, Q17, Q21, Q25
        // index 2 = Base Average Q5 + Q9
        // index 3 = Base Average Q9 + Q13
        // index 4 = Base Average Q13 + Q17
        // index 5 = Base Average Q17 + Q21
        // index 6 = Base Average Q21 + Q25
        // index 7 = Calculate Day of Week Accidents Reports Questions 6 to 8 Line 
        // index 8 = Calculate Month of YEAR - DRUGS Question 10 - 12
        // index 9 = Calculate Year - SEX OFFENDER Question 14 - 16
        // index 10 = Calculate Day of Week - Arrest/Convictions - Question 18 - 20
        // index 11 = Calculate Month of Year - Smuggling - Question 22 - 24
        // index 12 = Calculate TSL High
        // index 13 = Calculate TSL Low
        // index 14 = Averge of index 7 to 9 - Relevelent Q Base line Averages
        // index 15 = Averge of index 8 to 10 - Relevelent Q Base line Averages
        // index 16 = Averge of index 9 to 11 - Relevelent Q Base line Averages
        private double[] _dReadTimeAvg = new double[MAX];
        public double[] dReadTimeAvg
        {
            get { return _dReadTimeAvg; }
            set { _dReadTimeAvg = value; }
        }

        // Cell dReadTimeAvg26 for 26 Q
        private double _dReadTimeAvg26 = 0;

        public double dReadTimeAvg26
        {
            get { return _dReadTimeAvg26; }
            set { _dReadTimeAvg26 = value; }
        }

        // Cell dReadTimeTotal26 for 26 Q
        private double _dReadTimeTotal26 = 0;

        public double dReadTimeTotal26
        {
            get { return _dReadTimeTotal26; }
            set { _dReadTimeTotal26 = value; }
        }

        // Cell dReadTimeRelevantAvg26 for 26 Q
        private double _dReadTimeRelevantAvg26 = 0;

        public double dReadTimeRelevantAvg26
        {
            get { return _dReadTimeRelevantAvg26; }
            set { _dReadTimeRelevantAvg26 = value; }
        }


        // Cell dReadTimeIRAvg26 for 26 Q
        private double _dReadTimeIRAvg26 = 0;

        public double dReadTimeIRAvg26
        {
            get { return _dReadTimeIRAvg26; }
            set { _dReadTimeIRAvg26 = value; }
        }

        // 
        // USED FOR THE 26 Questions
        //
        // index 0 = Baseline Average Q5, Q9, Q13, Q17, Q21, Q25
        // index 1 = Baseline Average Q5, Q9
        // index 2 = Baseline Average Q9, Q13
        // index 3 = Baseline Average Q13, Q17
        // index 4 = Baseline Average Q17, Q21
        // index 5 = Baseline Average Q21, Q25

        private double[] _dBaseLineAvg = new double[MAX];
        public double[] dBaseLineAvg
        {
            get { return _dBaseLineAvg; }
            set { _dBaseLineAvg = value; }
        }

        // 
        // USED FOR THE 26 Questions
        //
        private double[] _dTSLHiSPR = new double[MAX];
        public double[] dTSLHiSPR
        {
            get { return _dTSLHiSPR; }
            set { _dTSLHiSPR = value; }
        }

        // 
        // USED FOR THE 26 Questions
        //
        private double[] _dTSLLowSPR = new double[MAX];
        public double[] dTSLLowSPR
        {
            get { return _dTSLLowSPR; }
            set { _dTSLLowSPR = value; }
        }

        // 
        // USED FOR THE 26 Questions
        //
        private double[] _dSTDSPR = new double[MAX];
        public double[] dSTDSPR
        {
            get { return _dSTDSPR; }
            set { _dSTDSPR = value; }
        }



        private double _StandardDeviation = 0;
        public double StandardDeviation
        {
            get { return _StandardDeviation; }
            set { _StandardDeviation = value; }
        }


        // used for the 18 question

        private double _StandardDeviationReadTime = 0;
        public double StandardDeviationReadTime
        {
            get { return _StandardDeviationReadTime; }
            set { _StandardDeviationReadTime = value; }
        }

        // This is the 1st STOP
        private double _eTSLMinimum = (double ) LowTSLValues.TSL_LOW_TWENTY_PERCENT;
        public double eTSLMinimum
        {
            get { return _eTSLMinimum; }
            set { _eTSLMinimum = value; }
        }

        // this is the FLOOR.  We Must stop at this point
        // only go to this if we have gone to the TSL Minimum and not got the
        // number of SPR meet
        private double _eTSLFloor = (double)LowTSLValues.TSL_LOW_TEN_PERCENT;
        public double eTSLFloor
        {
            get { return _eTSLFloor; }
            set { _eTSLFloor = value; }
        }

        // make the floor logic configurable
        private bool _bTSLFloorOn = false;
        public bool bTSLFloorOn
        {
            get { return _bTSLFloorOn; }
            set { _bTSLFloorOn = value; }
        }

        private double _eTotalNumReaction = (double)NumberReaction.REACTION_TWO;
        public double eTotalNumReaction
        {
            get { return _eTotalNumReaction; }
            set { _eTotalNumReaction = value; }
        }

        // This is used if we need to run the VerifyIfWeHaveADistortInReactionData26() function
        private bool _bRunTheTestAvgMultiplierCode = true;
        public bool bRunTheTestAvgMultiplierCode
        {
            get { return _bRunTheTestAvgMultiplierCode; }
            set { _bRunTheTestAvgMultiplierCode = value; }
        }

        // This is used if we need to run the VerifyIfWeHaveADistortInReactionData26() function
        private double _dTestAvgMultiplierCode = 1.5;
        public double dTestAvgMultiplierCode
        {
            get { return _dTestAvgMultiplierCode; }
            set { _dTestAvgMultiplierCode = value; }
        }

        // 26 Question - Which Questions were DISTORTED or changed during the VerifyIfWeHaveADistortInReactionData26() function
        private int[] _QuestionThatWasDistortDuringMultipleCodeLogic26 = new int[MAX];

        public int[] QuestionThatWasDistortDuringMultipleCodeLogic26
        {
            get { return _QuestionThatWasDistortDuringMultipleCodeLogic26; }
            set { _QuestionThatWasDistortDuringMultipleCodeLogic26 = value; }
        }

        //public void CalculateRDTimeT15(ref TestSession pTestSession)
        //{

        //    if ((pTestSession != null) && (pTestSession.lstScreenQuestionOnly.Count > 0))
        //    {
        //        Screen screen = null;
        //        // double tmpAvgRDTime = 0;

        //        // Go through the all the questions
        //        for (int i = 0; i < pTestSession.lstScreenQuestionOnly.Count; i++)
        //        {
        //            screen = (Screen)pTestSession.lstScreenQuestionOnly[i];
        //            // do your formulas and populate
        //            //tmpAvgRDTime = tmpAvgRDTime + ( screen.ReadTimeInSeconds * 100 );
        //            dRDTime[i] = (double)(screen.ReadTimeInSeconds * 100);
        //            listRDTime.Add(dRDTime[i]);
        //            //MessageBox.Show("dRDTime[" + i + "]=" + dRDTime[i]);
        //            // listRDTime.Add(screen.ReadTimeInSeconds * 100);
        //        }

        //        //CalculateRDTimeAvgT15(ref pTestSession);

        //        // tmpAvgRDTime = tmpAvgRDTime / pTestSession.lstScreenQuestionOnly.Count;
        //        // listRDTime.Add(tmpAvgRDTime);
        //    }

        //}





 

  

        //public string toXML()
        //{
        //}
        
    }
}
