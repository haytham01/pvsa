using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  TestTemplate.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH     7/7/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the TestTemplate database table.
    /// </summary>
    public class TestTemplate
    {
        //attributes
        /// <summary>TestTemplateID Attribute type long</summary>
        private long _lTestTemplateID = 0;
        /// <summary>TestTypeID Attribute type long</summary>
        private long _lTestTypeID = 0;
        /// <summary>DoPrintReport Attribute type bool</summary>
        private bool? _bDoPrintReport = null;
        /// <summary>DoLicenseIdentification Attribute type bool</summary>
        private bool? _bDoLicenseIdentification = null;
        /// <summary>DoNameIdentification Attribute type bool</summary>
        private bool? _bDoNameIdentification = null;
        /// <summary>PrintTemplateName Attribute type string</summary>
        private string _strPrintTemplateName = null;
        /// <summary>IsKioskMode Attribute type bool</summary>
        private bool? _bIsKioskMode = null;
        /// <summary>IsSynchronized Attribute type bool</summary>
        private bool? _bIsSynchronized = null;
        /// <summary>AdminPassword Attribute type string</summary>
        private string _strAdminPassword = null;
        /// <summary>ManagerPassword Attribute type string</summary>
        private string _strManagerPassword = null;
        /// <summary>ResetInSeconds Attribute type long</summary>
        private long _lResetInSeconds = 0;
        /// <summary>DateCreated Attribute type DateTime</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>DateModified Attribute type DateTime</summary>
        private DateTime _dtDateModified = dtNull;

        private static DateTime dtNull = new DateTime();
        private bool _hasError = false;
        private ErrorCode _errorCode = null;

        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "TestTemplate"; //Table name to abstract

        // Attribute variables
        /// <summary>TAG_ID Attribute</summary>
        public static readonly string TAG_ID = "TestTemplateID"; //Attribute id  name
        /// <summary>TestTypeID Attribute type string</summary>
        public static readonly string TAG_TEST_TYPE_ID = "TestTypeID"; //Table TestTypeID field name
        /// <summary>DoPrintReport Attribute type string</summary>
        public static readonly string TAG_DO_PRINT_REPORT = "DoPrintReport"; //Table DoPrintReport field name
        /// <summary>DoLicenseIdentification Attribute type string</summary>
        public static readonly string TAG_DO_LICENSE_IDENTIFICATION = "DoLicenseIdentification"; //Table DoLicenseIdentification field name
        /// <summary>DoNameIdentification Attribute type string</summary>
        public static readonly string TAG_DO_NAME_IDENTIFICATION = "DoNameIdentification"; //Table DoNameIdentification field name
        /// <summary>PrintTemplateName Attribute type string</summary>
        public static readonly string TAG_PRINT_TEMPLATE_NAME = "PrintTemplateName"; //Table PrintTemplateName field name
        /// <summary>IsKioskMode Attribute type string</summary>
        public static readonly string TAG_IS_KIOSK_MODE = "IsKioskMode"; //Table IsKioskMode field name
        /// <summary>IsSynchronized Attribute type string</summary>
        public static readonly string TAG_IS_SYNCHRONIZED = "IsSynchronized"; //Table IsSynchronized field name
        /// <summary>AdminPassword Attribute type string</summary>
        public static readonly string TAG_ADMIN_PASSWORD = "AdminPassword"; //Table AdminPassword field name
        /// <summary>ManagerPassword Attribute type string</summary>
        public static readonly string TAG_MANAGER_PASSWORD = "ManagerPassword"; //Table ManagerPassword field name
        /// <summary>ResetInSeconds Attribute type string</summary>
        public static readonly string TAG_RESET_IN_SECONDS = "ResetInSeconds"; //Table ResetInSeconds field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        //properties
        /// <summary>TestTemplateID is a Property in the TestTemplate Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>TestTypeID is a Property in the TestTemplate Class of type long</summary>
        public long TestTypeID
        {
            get { return _lTestTypeID; }
            set { _lTestTypeID = value; }
        }
        /// <summary>DoPrintReport is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoPrintReport
        {
            get { return _bDoPrintReport; }
            set { _bDoPrintReport = value; }
        }
        /// <summary>DoLicenseIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoLicenseIdentification
        {
            get { return _bDoLicenseIdentification; }
            set { _bDoLicenseIdentification = value; }
        }
        /// <summary>DoNameIdentification is a Property in the TestTemplate Class of type bool</summary>
        public bool? DoNameIdentification
        {
            get { return _bDoNameIdentification; }
            set { _bDoNameIdentification = value; }
        }
        /// <summary>PrintTemplateName is a Property in the TestTemplate Class of type String</summary>
        public string PrintTemplateName
        {
            get { return _strPrintTemplateName; }
            set { _strPrintTemplateName = value; }
        }
        /// <summary>IsKioskMode is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsKioskMode
        {
            get { return _bIsKioskMode; }
            set { _bIsKioskMode = value; }
        }
        /// <summary>IsSynchronized is a Property in the TestTemplate Class of type bool</summary>
        public bool? IsSynchronized
        {
            get { return _bIsSynchronized; }
            set { _bIsSynchronized = value; }
        }
        /// <summary>AdminPassword is a Property in the TestTemplate Class of type String</summary>
        public string AdminPassword
        {
            get { return _strAdminPassword; }
            set { _strAdminPassword = value; }
        }
        /// <summary>ManagerPassword is a Property in the TestTemplate Class of type String</summary>
        public string ManagerPassword
        {
            get { return _strManagerPassword; }
            set { _strManagerPassword = value; }
        }
        /// <summary>ResetInSeconds is a Property in the TestTemplate Class of type long</summary>
        public long ResetInSeconds
        {
            get { return _lResetInSeconds; }
            set { _lResetInSeconds = value; }
        }
        /// <summary>DateCreated is a Property in the TestTemplate Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>DateModified is a Property in the TestTemplate Class of type DateTime</summary>
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }
        /// <summary>HasError Property in class TestTemplate and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class TestTemplate and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }
        //Constructors
        /// <summary>TestTemplate empty constructor</summary>
        public TestTemplate()
        {
        }
        /// <summary>TestTemplate Constructor takes string pStrData and Config</summary>
        public TestTemplate(string pStrData)
        {
            Parse(pStrData);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the TestTemplate Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + TestTemplateID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TYPE_ID + ":  " + TestTypeID + "\n");
            sbReturn.Append(TAG_DO_PRINT_REPORT + ":  " + DoPrintReport + "\n");
            sbReturn.Append(TAG_DO_LICENSE_IDENTIFICATION + ":  " + DoLicenseIdentification + "\n");
            sbReturn.Append(TAG_DO_NAME_IDENTIFICATION + ":  " + DoNameIdentification + "\n");
            sbReturn.Append(TAG_PRINT_TEMPLATE_NAME + ":  " + PrintTemplateName + "\n");
            sbReturn.Append(TAG_IS_KIOSK_MODE + ":  " + IsKioskMode + "\n");
            sbReturn.Append(TAG_IS_SYNCHRONIZED + ":  " + IsSynchronized + "\n");
            sbReturn.Append(TAG_ADMIN_PASSWORD + ":  " + AdminPassword + "\n");
            sbReturn.Append(TAG_MANAGER_PASSWORD + ":  " + ManagerPassword + "\n");
            sbReturn.Append(TAG_RESET_IN_SECONDS + ":  " + ResetInSeconds + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
            }

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of TestTemplate</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<TestTemplate>\n");
            sbReturn.Append("<" + TAG_ID + ">" + TestTemplateID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TYPE_ID + ">" + TestTypeID + "</" + TAG_TEST_TYPE_ID + ">\n");
            sbReturn.Append("<" + TAG_DO_PRINT_REPORT + ">" + DoPrintReport + "</" + TAG_DO_PRINT_REPORT + ">\n");
            sbReturn.Append("<" + TAG_DO_LICENSE_IDENTIFICATION + ">" + DoLicenseIdentification + "</" + TAG_DO_LICENSE_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_DO_NAME_IDENTIFICATION + ">" + DoNameIdentification + "</" + TAG_DO_NAME_IDENTIFICATION + ">\n");
            sbReturn.Append("<" + TAG_PRINT_TEMPLATE_NAME + ">" + PrintTemplateName + "</" + TAG_PRINT_TEMPLATE_NAME + ">\n");
            sbReturn.Append("<" + TAG_IS_KIOSK_MODE + ">" + IsKioskMode + "</" + TAG_IS_KIOSK_MODE + ">\n");
            sbReturn.Append("<" + TAG_IS_SYNCHRONIZED + ">" + IsSynchronized + "</" + TAG_IS_SYNCHRONIZED + ">\n");
            sbReturn.Append("<" + TAG_ADMIN_PASSWORD + ">" + AdminPassword + "</" + TAG_ADMIN_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_MANAGER_PASSWORD + ">" + ManagerPassword + "</" + TAG_MANAGER_PASSWORD + ">\n");
            sbReturn.Append("<" + TAG_RESET_IN_SECONDS + ">" + ResetInSeconds + "</" + TAG_RESET_IN_SECONDS + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("</TestTemplate>" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                TestTemplateID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TYPE_ID);
                TestTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_PRINT_REPORT);
                DoPrintReport = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoPrintReport = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_LICENSE_IDENTIFICATION);
                DoLicenseIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoLicenseIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_NAME_IDENTIFICATION);
                DoNameIdentification = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                DoNameIdentification = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_TEMPLATE_NAME);
                PrintTemplateName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_KIOSK_MODE);
                IsKioskMode = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsKioskMode = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_SYNCHRONIZED);
                IsSynchronized = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsSynchronized = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ADMIN_PASSWORD);
                AdminPassword = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_MANAGER_PASSWORD);
                ManagerPassword = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_RESET_IN_SECONDS);
                ResetInSeconds = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ResetInSeconds = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
                DateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }
        }
    }
}

//END OF TestTemplate CLASS FILE

