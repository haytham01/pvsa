using System;
using System.Xml;
using System.Text;
using System.Collections;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestManager.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumTestManager
	{
		//attributes
		/// <summary>TestManagerID Attribute type long</summary>
		private long _lTestManagerID = 0;
		/// <summary>TestTemplateID Attribute type long</summary>
		private long _lTestTemplateID = 0;
		/// <summary>TestLanguageID Attribute type long</summary>
		private long _lTestLanguageID = 0;
		/// <summary>TestSubjectID Attribute type long</summary>
		private long _lTestSubjectID = 0;
		/// <summary>ScreenID Attribute type long</summary>
		private long _lScreenID = 0;
		/// <summary>BeginDateCreated Attribute type DateTime</summary>
		private DateTime _dtBeginDateCreated = dtNull;
		/// <summary>EndDateCreated Attribute type DateTime</summary>
		private DateTime _dtEndDateCreated = dtNull;
		/// <summary>BeginDateModified Attribute type DateTime</summary>
		private DateTime _dtBeginDateModified = dtNull;
		/// <summary>EndDateModified Attribute type DateTime</summary>
		private DateTime _dtEndDateModified = dtNull;
		/// <summary>BeginDateBeginTest Attribute type DateTime</summary>
		private DateTime _dtBeginDateBeginTest = dtNull;
		/// <summary>EndDateBeginTest Attribute type DateTime</summary>
		private DateTime _dtEndDateBeginTest = dtNull;
		/// <summary>BeginDateEndTest Attribute type DateTime</summary>
		private DateTime _dtBeginDateEndTest = dtNull;
		/// <summary>EndDateEndTest Attribute type DateTime</summary>
		private DateTime _dtEndDateEndTest = dtNull;
		/// <summary>ReportData Attribute type string</summary>
		private string _strReportData = null;
		/// <summary>Notes Attribute type string</summary>
		private string _strNotes = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumTestManager"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "TestManagerID"; //Attribute id  name
		/// <summary>TestTemplateID Attribute type string</summary>
		public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Table TestTemplateID field name
		/// <summary>TestLanguageID Attribute type string</summary>
		public static readonly string TAG_TEST_LANGUAGE_ID = "TestLanguageID"; //Table TestLanguageID field name
		/// <summary>TestSubjectID Attribute type string</summary>
		public static readonly string TAG_TEST_SUBJECT_ID = "TestSubjectID"; //Table TestSubjectID field name
		/// <summary>ScreenID Attribute type string</summary>
		public static readonly string TAG_SCREEN_ID = "ScreenID"; //Table ScreenID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Table DateCreated field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Table DateModified field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Table DateModified field name
		/// <summary>DateBeginTest Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_BEGIN_TEST = "BeginDateBeginTest"; //Table DateBeginTest field name
		/// <summary>DateBeginTest Attribute type string</summary>
		public static readonly string TAG_END_DATE_BEGIN_TEST = "EndDateBeginTest"; //Table DateBeginTest field name
		/// <summary>DateEndTest Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_END_TEST = "BeginDateEndTest"; //Table DateEndTest field name
		/// <summary>DateEndTest Attribute type string</summary>
		public static readonly string TAG_END_DATE_END_TEST = "EndDateEndTest"; //Table DateEndTest field name
		/// <summary>ReportData Attribute type string</summary>
		public static readonly string TAG_REPORT_DATA = "ReportData"; //Table ReportData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>TestManagerID is a Property in the TestManager Class of type long</summary>
		public long TestManagerID 
		{
			get{return _lTestManagerID;}
			set{_lTestManagerID = value;}
		}
		/// <summary>TestTemplateID is a Property in the TestManager Class of type long</summary>
		public long TestTemplateID 
		{
			get{return _lTestTemplateID;}
			set{_lTestTemplateID = value;}
		}
		/// <summary>TestLanguageID is a Property in the TestManager Class of type long</summary>
		public long TestLanguageID 
		{
			get{return _lTestLanguageID;}
			set{_lTestLanguageID = value;}
		}
		/// <summary>TestSubjectID is a Property in the TestManager Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>ScreenID is a Property in the TestManager Class of type long</summary>
		public long ScreenID 
		{
			get{return _lScreenID;}
			set{_lScreenID = value;}
		}
		/// <summary>BeginDateCreated is a Property in the TestManager Class of type DateTime</summary>
		public DateTime BeginDateCreated 
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>EndDateCreated is a Property in the TestManager Class of type DateTime</summary>
		public DateTime EndDateCreated 
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>BeginDateModified is a Property in the TestManager Class of type DateTime</summary>
		public DateTime BeginDateModified 
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>EndDateModified is a Property in the TestManager Class of type DateTime</summary>
		public DateTime EndDateModified 
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>BeginDateBeginTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime BeginDateBeginTest 
		{
			get{return _dtBeginDateBeginTest;}
			set{_dtBeginDateBeginTest = value;}
		}
		/// <summary>EndDateBeginTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime EndDateBeginTest 
		{
			get{return _dtEndDateBeginTest;}
			set{_dtEndDateBeginTest = value;}
		}
		/// <summary>BeginDateEndTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime BeginDateEndTest 
		{
			get{return _dtBeginDateEndTest;}
			set{_dtBeginDateEndTest = value;}
		}
		/// <summary>EndDateEndTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime EndDateEndTest 
		{
			get{return _dtEndDateEndTest;}
			set{_dtEndDateEndTest = value;}
		}
		/// <summary>ReportData is a Property in the TestManager Class of type String</summary>
		public string ReportData 
		{
			get{return _strReportData;}
			set{_strReportData = value;}
		}
		/// <summary>Notes is a Property in the TestManager Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}
		/// <summary>HasError Property in class TestManager and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestManager and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>TestManager empty constructor</summary>
		public EnumTestManager()
		{
		}
		/// <summary>TestManager Constructor takes string pStrData and Config</summary>
		public EnumTestManager(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestManager Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestManagerID.ToString() + "\n");
			sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
			sbReturn.Append(TAG_TEST_LANGUAGE_ID + ":  " + TestLanguageID + "\n");
			sbReturn.Append(TAG_TEST_SUBJECT_ID + ":  " + TestSubjectID + "\n");
			sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(BeginDateBeginTest))
			{
				sbReturn.Append(TAG_BEGIN_DATE_BEGIN_TEST + ":  " + BeginDateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(EndDateBeginTest))
			{
				sbReturn.Append(TAG_END_DATE_BEGIN_TEST + ":  " + EndDateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(BeginDateEndTest))
			{
				sbReturn.Append(TAG_BEGIN_DATE_END_TEST + ":  " + BeginDateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_END_TEST + ":\n");
			}
			if (!dtNull.Equals(EndDateEndTest))
			{
				sbReturn.Append(TAG_END_DATE_END_TEST + ":  " + EndDateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_END_TEST + ":\n");
			}
			sbReturn.Append(TAG_REPORT_DATA + ":  " + ReportData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestManager</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestManagerID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_LANGUAGE_ID + ">" + TestLanguageID + "</" + TAG_TEST_LANGUAGE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_SUBJECT_ID + ">" + TestSubjectID + "</" + TAG_TEST_SUBJECT_ID + ">\n");
			sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(BeginDateBeginTest))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_BEGIN_TEST + ">" + BeginDateBeginTest.ToString() + "</" + TAG_BEGIN_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_BEGIN_TEST + "></" + TAG_BEGIN_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(EndDateBeginTest))
			{
				sbReturn.Append("<" + TAG_END_DATE_BEGIN_TEST + ">" + EndDateBeginTest.ToString() + "</" + TAG_END_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_BEGIN_TEST + "></" + TAG_END_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(BeginDateEndTest))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_END_TEST + ">" + BeginDateEndTest.ToString() + "</" + TAG_BEGIN_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_END_TEST + "></" + TAG_BEGIN_DATE_END_TEST + ">\n");
			}
			if (!dtNull.Equals(EndDateEndTest))
			{
				sbReturn.Append("<" + TAG_END_DATE_END_TEST + ">" + EndDateEndTest.ToString() + "</" + TAG_END_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_END_TEST + "></" + TAG_END_DATE_END_TEST + ">\n");
			}
			sbReturn.Append("<" + TAG_REPORT_DATA + ">" + ReportData + "</" + TAG_REPORT_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
/// <summary>Parses XML, puts in arraylist</summary>
public static void ParseToTestManagerArrayList(string pStrXml, ArrayList pRefArrayList)
		{
			try
			{
				TestManager test_manager = null;
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + TestManager.ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					if (pRefArrayList != null)
					{
						test_manager = new TestManager();
						test_manager.Parse(xNode);
						pRefArrayList.Add(test_manager);
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestManagerID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
				TestTemplateID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestTemplateID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_LANGUAGE_ID);
				TestLanguageID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestLanguageID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_SUBJECT_ID);
				TestSubjectID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestSubjectID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
				ScreenID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ScreenID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_BEGIN_TEST);
				BeginDateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_BEGIN_TEST);
				EndDateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_END_TEST);
				BeginDateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_END_TEST);
				EndDateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REPORT_DATA);
				ReportData = xResultNode.InnerText;
				if (ReportData.Trim().Length == 0)
					ReportData = null;
			}
			catch  
			{
				ReportData = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
				if (Notes.Trim().Length == 0)
					Notes = null;
			}
			catch  
			{
				Notes = null;
			}
		}
	}
}

//END OF TestManager CLASS FILE


