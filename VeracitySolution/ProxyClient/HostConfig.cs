﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Veracity.Client.Proxy
{
    public class HostConfig
    {
        private bool _bIsKioskMode = false;
        private bool _bDoPhoto = false;
        private bool _bShowNavigationControls = false;
        private bool _bDoReportPdf = false;
        private bool _bDoReportPrinting = false;
        private bool _bDoReportPreview = false;
        private bool _bDoReportEmail = false;
        private bool _bDoReportGraph = false;
        private int _nTslValue = 0;
        private int _nTslValueLow = 0;
        private double _dHitsValue = 0;
        private string _strPrinterName = null;
        private string _strReportTemplateName = null;
        private List<ClientShellScreen> _lstClientShellScreen = new List<ClientShellScreen>();
        private ClientShellScreen _currentClientShellScreen = ClientShellScreen.START;
        private List<Resource> _lstResources = new List<Resource>();

        private bool _bAutoAdjust = false;
        //private int _nTslMinValue = 0;

        private bool _bAvgMultiplier = false;
        private double _dAvgMultiplierValue = 0;

        private bool _bIsAudioOnly = false;
        private bool _bShowResultsScreen = false;
        //private bool _bDoGraph = false;
        private bool _bTurnOffExtraSPRWeight = false;
        private bool _bTurnOffSPR1 = false;
        private bool _bTurnOffSPR2 = false;
        private bool _bTurnOffSPR3 = false;
        private bool _bTurnOffRiskAnalysis = false;
        private bool _bTurnOffMovingTestAverage = false;
        private bool _bTurnOffSPR71115 = false;
        private bool _bTurnOnPressureSensor = false;
        
        private string _strTestButtonText = null;

        private bool _bTurnOffIDScreen = false;


        private bool _bPrintReportToPDF = true;
        // 20181112 - determine if we should delete report
        private bool _bDeletePDFReport = false;
        private bool _bTslFloorOn = false;
        private int _nTslFloor = 0;
        private string _strPage1ReportTitle = null;
        private string _strPage2ReportTitle = null;
        private string _strPage1SubTitle = null;
        private string _strIssueATitle = null;
        private string _strIssueBTitle = null;
        private string _strIssueCTitle = null;
        private string _strIssueDTitle = null;
        private string _strEmailAddress = null;
        private int _nPage1LeftMargin = 0;
        private int _nPage1SubtitleLeftMargin = 0;
        private int _nPage2LeftMargin = 0;

        private bool _bPrintGraphToPDF = true;
        private bool _bPrintGraph = true;
        private bool _hasError = false;

        public static readonly string ENTITY_NAME = "HostConfig";
        public static readonly string TAG_CLIENT_SHELL_SCREENS = "ClientShellScreens";
        public static readonly string TAG_IS_KIOSK_MODE = "IsKioskMode";
        public static readonly string TAG_DO_PHOTO = "DoPhoto";
        public static readonly string TAG_SHOW_NAVIGATION_CONTROLS = "ShowNavigationControls";
        public static readonly string TAG_DO_REPORTING_PDF = "DoReportPdf";
        public static readonly string TAG_DO_REPORTING_PRINTING = "DoReportPrinting";
        public static readonly string TAG_DO_REPORTING_PREVIEW = "DoReportPreview";
        public static readonly string TAG_DO_REPORT_GRAPH = "DoReportGraph";
        public static readonly string TAG_DO_REPORT_EMAIL = "DoReportEmail";
        public static readonly string TAG_PRINTER_NAME = "PrinterName";
        public static readonly string TAG_REPORT_TEMPLATE_NAME = "ReportTemplateName";
        public static readonly string TAG_TSL_VALUE = "TSLValue";
        public static readonly string TAG_RESOURCES = "Resources";
        public static readonly string TAG_RESOURCE = "Resource";
        public static readonly string TAG_RESOURCE_KEY = "Key";
        public static readonly string TAG_RESOURCE_VALUE = "Value";
        public static readonly string TAG_AUTO_ADJUST = "AutoAdjust";
        public static readonly string TAG_HITS_VALUE = "HitsValue";
        public static readonly string TAG_TSL_VALUE_LOW = "TSLValueLow";

        public static readonly string TAG_AVG_MULTIPLIER_ON = "AvgMultiplierOn";
        public static readonly string TAG_AVG_MULTIPLIER_VALUE = "AvgMultiplierValue";
        public static readonly string TAG_IS_AUDIO_ONLY = "IsAudioOnly";
        public static readonly string TAG_SHOW_RESULTS_SCREEN = "ShowResultsScreen";
        public static readonly string TAG_TURN_OFF_SPR_WEIGHT = "TurnOffExtraSPRWeight";
        public static readonly string TAG_DO_GRAPH = "DoGraph";
        public static readonly string TAG_TURN_OFF_SPR1 = "TurnOffSPR1";
        public static readonly string TAG_TURN_OFF_SPR2 = "TurnOffSPR2";
        public static readonly string TAG_TURN_OFF_SPR3 = "TurnOffSPR3";
        public static readonly string TAG_TURN_OFF_RISK_ANALYSIS = "TurnOffRiskAnalysis";
        public static readonly string TAG_TURN_OFF_MOVING_TEST_AVERAGE = "TurnOffMovingTestAverage";
        public static readonly string TAG_TURN_OFF_SPR71115 = "TurnOffSPR71115";
        public static readonly string TAG_TURN_ON_PRESSURE_SENSOR = "TurnOnPressureSensor";
        public static readonly string TAG_TEST_BUTTON_TEXT = "TestButtonText";
        public static readonly string TAG_TURN_OFF_ID_SCREEN = "TurnOffIDScreen";
        public static readonly string TAG_TURN_ON_ENGLISH_LANGUAGE = "TurnOnEnglishLanguage";
        public static readonly string TAG_TURN_ON_SPANISH_MX_LANGUAGE = "TurnOnSpanishMXLanguage";
        public static readonly string TAG_TURN_ON_ARABIC_UAE_LANGUAGE = "TurnOnArabicUAELanguage";

        public static readonly string TAG_PAGE1_REPORT_TITLE = "Page1ReportTitle";
        public static readonly string TAG_PAGE2_REPORT_TITLE = "Page2ReportTitle";
        public static readonly string TAG_PAGE1_SUB_TITLE = "Page1SubTitle";
        public static readonly string TAG_ISSUE_A_TITLE = "IssueATitle";
        public static readonly string TAG_ISSUE_B_TITLE = "IssueBTitle";
        public static readonly string TAG_ISSUE_C_TITLE = "IssueCTitle";
        public static readonly string TAG_ISSUE_D_TITLE = "IssueDTitle";
        public static readonly string TAG_EMAIL_ADDRESS = "EmailAddress";
        public static readonly string TAG_PAGE1_LEFT_MARGINE = "Page1LeftMargin";
        public static readonly string TAG_PAGE1_SUBTITLE_LEFT_MARGIN = "Page1SubtitleLeftMargin";
        public static readonly string TAG_PAGE2_LEFT_MARGIN = "Page2LeftMargin";
        public static readonly string TAG_PRINT_GRAPH_TO_PDF = "PrintGraphToPDF";
        public static readonly string TAG_PRINT_REPORT_TO_PDF = "PrintReportToPDF";
        public static readonly string TAG_TSL_FLOOR_ON = "TslFloorOn";
        public static readonly string TAG_TSL_FLOOR = "TslFloor";
        public static readonly string TAG_PRINT_GRAPH = "PrintGraph";

        public enum ClientShellScreen
        {
            START,
            WELCOME,
            CHOICE_SCREEN,
            IDENTIFICATION_DRIVER_LICENSE,
            IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION,
            IDENTIFICATION_PHOTO,
            INPUT_FIRST_NAME,
            INPUT_LAST_NAME,
            SCREEN
        };

        public ClientShellScreen currentClientShellScreen
        {
            get { return _currentClientShellScreen; }
            set { _currentClientShellScreen = value; }
        }
        public string ReportTemplateName
        {
            get { return _strReportTemplateName; }
            set { _strReportTemplateName = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public bool ShowNavigationControls
        {
            get { return _bShowNavigationControls; }
            set { _bShowNavigationControls = value; }
        }
        public List<ClientShellScreen> lstClientShellScreen
        {
            get { return _lstClientShellScreen; }
            set { _lstClientShellScreen = value; }
        }
        public string PrinterName
        {
            get { return _strPrinterName; }
            set { _strPrinterName = value; }
        }
        public bool IsKioskMode
        {
            get { return _bIsKioskMode; }
            set { _bIsKioskMode = value; }
        }
        public bool DoPhoto
        {
            get { return _bDoPhoto; }
            set { 
                _bDoPhoto = value;
                _init();
            }
        }
        public bool DoReportPdf
        {
            get { return _bDoReportPdf; }
            set { _bDoReportPdf = value; }
        }
        public bool DoReportPrinting
        {
            get { return _bDoReportPrinting; }
            set { _bDoReportPrinting = value; }
        }
        public bool DoReportPreview
        {
            get { return _bDoReportPreview; }
            set { _bDoReportPreview = value; }
        }
        public bool DoReportEmail
        {
            get { return _bDoReportEmail; }
            set { _bDoReportEmail = value; }
        }
        public bool DoReportGraph
        {
            get { return _bDoReportGraph; }
            set { _bDoReportGraph = value; }
        }
        public int TslValue
        {
            get { return _nTslValue; }
            set { _nTslValue = value; }
        }
        //public int TslMinValue
        //{
        //    get { return _nTslMinValue; }
        //    set { _nTslMinValue = value; }
        //}
        public double HitsValue
        {
            get { return _dHitsValue; }
            set { _dHitsValue = value; }
        }
        public double AvgMultiplierValue
        {
            get { return _dAvgMultiplierValue; }
            set { _dAvgMultiplierValue = value; }
        }
        public bool AvgMultiplier
        {
            get { return _bAvgMultiplier; }
            set { _bAvgMultiplier = value; }
        }
        public HostConfig()
        {
            _init();
        }
        public HostConfig(string pStrData)
        {
            Parse(pStrData);
            _init();
        }
        private void _init()
        {
            _lstClientShellScreen.Clear();
            _lstClientShellScreen.Add(ClientShellScreen.WELCOME);
            _lstClientShellScreen.Add(ClientShellScreen.CHOICE_SCREEN);
            _lstClientShellScreen.Add(ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION);
            if (_bDoPhoto)
            {
                _lstClientShellScreen.Add(ClientShellScreen.IDENTIFICATION_PHOTO);
            }
        }
        public List<Resource> lstResources
        {
            get { return _lstResources; }
            set { _lstResources = value; }
        }
        public bool AutoAdjust
        {
            get { return _bAutoAdjust; }
            set { _bAutoAdjust = value; }
        }
        public int TslValueLow
        {
            get { return _nTslValueLow; }
            set { _nTslValueLow = value; }
        }
        public bool IsAudioOnly
        {
            get { return _bIsAudioOnly; }
            set { _bIsAudioOnly = value; }
        }
        public bool ShowResultsScreen
        {
            get { return _bShowResultsScreen; }
            set { _bShowResultsScreen = value; }
        }
        public bool TurnOffExtraSPRWeight
        {
            get { return _bTurnOffExtraSPRWeight; }
            set { _bTurnOffExtraSPRWeight = value; }
        }
        //public bool DoGraph
        //{
        //    get { return _bDoGraph; }
        //    set { _bDoGraph = value; }
        //}
        public bool TurnOffSPR1
        {
            get { return _bTurnOffSPR1; }
            set { _bTurnOffSPR1 = value; }
        }
        public bool TurnOffSPR2
        {
            get { return _bTurnOffSPR2; }
            set { _bTurnOffSPR2 = value; }
        }
        public bool TurnOffSPR3
        {
            get { return _bTurnOffSPR3; }
            set { _bTurnOffSPR3 = value; }
        }
        public bool TurnOffRiskAnalysis
        {
            get { return _bTurnOffRiskAnalysis; }
            set { _bTurnOffRiskAnalysis = value; }
        }
        public bool TurnOffSPR71115
        {
            get { return _bTurnOffSPR71115; }
            set { _bTurnOffSPR71115 = value; }
        }
        public bool TurnOffMovingTestAverage
        {
            get { return _bTurnOffMovingTestAverage; }
            set { _bTurnOffMovingTestAverage = value; }
        }
        public bool TurnOnPressureSensor
        {
            get { return _bTurnOnPressureSensor; }
            set { _bTurnOnPressureSensor = value; }
        }
        public bool TurnOffIDScreen
        {
            get { return _bTurnOffIDScreen; }
            set { _bTurnOffIDScreen = value; }
        }
        public string TestButtonText
        {
            get { return _strTestButtonText; }
            set { _strTestButtonText = value; }
        }
        public string Page1ReportTitle
        {
            get { return _strPage1ReportTitle; }
            set { _strPage1ReportTitle = value; }
        }
        public string Page2ReportTitle
        {
            get { return _strPage2ReportTitle; }
            set { _strPage2ReportTitle = value; }
        }
        public string Page1SubTitle
        {
            get { return _strPage1SubTitle; }
            set { _strPage1SubTitle = value; }
        }
        public string IssueATitle
        {
            get { return _strIssueATitle; }
            set { _strIssueATitle = value; }
        }
        public string IssueBTitle
        {
            get { return _strIssueBTitle; }
            set { _strIssueBTitle = value; }
        }
        public string IssueCTitle
        {
            get { return _strIssueCTitle; }
            set { _strIssueCTitle = value; }
        }
        public string IssueDTitle
        {
            get { return _strIssueDTitle; }
            set { _strIssueDTitle = value; }
        }
        public string EmailAddress
        {
            get { return _strEmailAddress; }
            set { _strEmailAddress = value; }
        }
        public int Page1LeftMargin
        {
            get { return _nPage1LeftMargin; }
            set { _nPage1LeftMargin = value; }
        }
        public int Page1SubtitleLeftMargin
        {
            get { return _nPage1SubtitleLeftMargin; }
            set { _nPage1SubtitleLeftMargin = value; }
        }
        public int Page2LeftMargin
        {
            get { return _nPage2LeftMargin; }
            set { _nPage2LeftMargin = value; }
        }
        public bool PrintGraphToPDF
        {
            get { return _bPrintGraphToPDF; }
            set { _bPrintGraphToPDF = value; }
        }
        public bool PrintReportToPDF
        {
            get { return _bPrintReportToPDF; }
            set { _bPrintReportToPDF = value; }
        }
        // 20181112 - determine if we need to delete report
        public bool DeletePDFReport
        {
            get { return _bDeletePDFReport; }
            set { _bDeletePDFReport = value; }
        }
        public bool TslFloorOn
        {
            get { return _bTslFloorOn; }
            set { _bTslFloorOn = value; }
        }
        public bool PrintGraph
        {
            get { return _bPrintGraph; }
            set { _bPrintGraph = value; }
        }
        public int TslFloor
        {
            get { return _nTslFloor; }
            set { _nTslFloor = value; }
        }
        public string getResourceValue(string pStrKey)
        {
            string value = null;
            foreach (Resource r in _lstResources)
            {
                if (r.key.Equals(pStrKey, StringComparison.CurrentCultureIgnoreCase))
                {
                    value = r.value;
                    break;
                }
            }
            return value;
        }

        //public void AddShellScreen_WELCOME()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.WELCOME);
        //}
        //public void AddShellScreen_CHOICE_SCREEN()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.CHOICE_SCREEN);
        //}
        //public void AddShellScreen_IDENTIFICATION_DRIVER_LICENSE()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE);
        //}
        //public void AddShellScreen_IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION);
        //}
        //public void AddShellScreen_INPUT_FIRST_NAME()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.INPUT_FIRST_NAME);
        //}
        //public void AddShellScreen_INPUT_LAST_NAME()
        //{
        //    _lstClientShellScreen.Add(ClientShellScreen.INPUT_LAST_NAME);
        //}
        public ClientShellScreen getNextClientShellScreen()
        {

            if ((_currentClientShellScreen == ClientShellScreen.START) && (lstClientShellScreen.Count > 0))
            {
                _currentClientShellScreen = lstClientShellScreen[0];
            }
            else if ((lstClientShellScreen.Count > 0) && (_currentClientShellScreen == lstClientShellScreen[lstClientShellScreen.Count - 1]))
            {
                _currentClientShellScreen = ClientShellScreen.SCREEN;
            }
            else
            {
                for (int i = 0; i < lstClientShellScreen.Count; i++)
                {
                    if (lstClientShellScreen[i] == _currentClientShellScreen)
                    {
                        if ((i + 1) < lstClientShellScreen.Count)
                        {
                            _currentClientShellScreen = lstClientShellScreen[i + 1];
                            break;
                        }
                    }
                }
            }
            return _currentClientShellScreen;
        }
        public ClientShellScreen getPreviousClientShellScreen()
        {

            if ((_currentClientShellScreen == ClientShellScreen.CHOICE_SCREEN) && (lstClientShellScreen.Count > 0))
            {
                _currentClientShellScreen = lstClientShellScreen[0];
            } 
            if ((_currentClientShellScreen == ClientShellScreen.IDENTIFICATION_DRIVER_LICENSE_KEYBOARD_OPTION) && (lstClientShellScreen.Count > 0))
            {
                if (lstClientShellScreen.Count <= 2)
                {
                    _currentClientShellScreen = lstClientShellScreen[0];
                }
                else
                {
                    _currentClientShellScreen = lstClientShellScreen[1];
                }
            }
            else if ((_currentClientShellScreen == ClientShellScreen.IDENTIFICATION_PHOTO) && (lstClientShellScreen.Count > 0))
            {
                if (lstClientShellScreen.Count <= 3)
                {
                    _currentClientShellScreen = lstClientShellScreen[1];
                }
                else
                {
                    _currentClientShellScreen = lstClientShellScreen[2];
                }
            }
            else if ((lstClientShellScreen.Count > 0) && (_currentClientShellScreen == lstClientShellScreen[lstClientShellScreen.Count - 1]))
            {
                _currentClientShellScreen = ClientShellScreen.SCREEN;
            }
            else
            {
                for (int i = 0; i < lstClientShellScreen.Count; i++)
                {
                    if (lstClientShellScreen[i] == _currentClientShellScreen)
                    {
                        if (i > 0)
                        {
                            _currentClientShellScreen = lstClientShellScreen[i - 1];
                            break;
                        }
                    }
                }
            }
            return _currentClientShellScreen;
        }
        public string ToXml()
        {
            StringBuilder sbReturn = new StringBuilder();

            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_IS_KIOSK_MODE + ">" + IsKioskMode + "</" + TAG_IS_KIOSK_MODE + ">\n");
            sbReturn.Append("<" + TAG_SHOW_NAVIGATION_CONTROLS + ">" + ShowNavigationControls + "</" + TAG_SHOW_NAVIGATION_CONTROLS + ">\n");
            sbReturn.Append("<" + TAG_DO_REPORTING_PDF + ">" + DoReportPdf + "</" + TAG_DO_REPORTING_PDF + ">\n");
            sbReturn.Append("<" + TAG_DO_REPORTING_PRINTING + ">" + DoReportPrinting + "</" + TAG_DO_REPORTING_PRINTING + ">\n");
            sbReturn.Append("<" + TAG_DO_REPORTING_PREVIEW + ">" + DoReportPreview + "</" + TAG_DO_REPORTING_PREVIEW + ">\n");
            sbReturn.Append("<" + TAG_DO_REPORT_EMAIL + ">" + DoReportEmail + "</" + TAG_DO_REPORT_EMAIL + ">\n");
            sbReturn.Append("<" + TAG_DO_REPORT_GRAPH + ">" + DoReportGraph + "</" + TAG_DO_REPORT_GRAPH + ">\n");
            sbReturn.Append("<" + TAG_IS_AUDIO_ONLY + ">" + IsAudioOnly + "</" + TAG_IS_AUDIO_ONLY + ">\n");
            sbReturn.Append("<" + TAG_SHOW_RESULTS_SCREEN + ">" + ShowResultsScreen + "</" + TAG_SHOW_RESULTS_SCREEN + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_SPR_WEIGHT + ">" + TurnOffExtraSPRWeight + "</" + TAG_TURN_OFF_SPR_WEIGHT + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_SPR1 + ">" + TurnOffSPR1 + "</" + TAG_TURN_OFF_SPR1 + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_SPR2 + ">" + TurnOffSPR2 + "</" + TAG_TURN_OFF_SPR2 + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_SPR3 + ">" + TurnOffSPR3 + "</" + TAG_TURN_OFF_SPR3 + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_RISK_ANALYSIS + ">" + TurnOffRiskAnalysis + "</" + TAG_TURN_OFF_RISK_ANALYSIS + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_SPR71115 + ">" + TurnOffSPR71115 + "</" + TAG_TURN_OFF_SPR71115 + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_MOVING_TEST_AVERAGE + ">" + TurnOffMovingTestAverage + "</" + TAG_TURN_OFF_MOVING_TEST_AVERAGE + ">\n");
            sbReturn.Append("<" + TAG_TURN_ON_PRESSURE_SENSOR + ">" + TurnOnPressureSensor + "</" + TAG_TURN_ON_PRESSURE_SENSOR + ">\n");
            sbReturn.Append("<" + TAG_TURN_OFF_ID_SCREEN + ">" + TurnOffIDScreen + "</" + TAG_TURN_OFF_ID_SCREEN + ">\n");
            sbReturn.Append("<" + TAG_TEST_BUTTON_TEXT + ">" + TestButtonText + "</" + TAG_TEST_BUTTON_TEXT + ">\n");
            sbReturn.Append("<" + TAG_PRINTER_NAME + ">" + PrinterName + "</" + TAG_PRINTER_NAME + ">\n");
            sbReturn.Append("<" + TAG_REPORT_TEMPLATE_NAME + ">" + ReportTemplateName + "</" + TAG_REPORT_TEMPLATE_NAME + ">\n");
            sbReturn.Append("<" + TAG_TSL_VALUE + ">" + TslValue + "</" + TAG_TSL_VALUE + ">\n");
            sbReturn.Append("<" + TAG_HITS_VALUE + ">" + _dHitsValue + "</" + TAG_HITS_VALUE + ">\n");
            sbReturn.Append("<" + TAG_AVG_MULTIPLIER_VALUE + ">" + _dAvgMultiplierValue + "</" + TAG_AVG_MULTIPLIER_VALUE + ">\n");
            sbReturn.Append("<" + TAG_AVG_MULTIPLIER_ON + ">" + _bAvgMultiplier + "</" + TAG_AVG_MULTIPLIER_ON + ">\n");
            sbReturn.Append("<" + TAG_TSL_VALUE_LOW + ">" + TslValueLow + "</" + TAG_TSL_VALUE_LOW + ">\n");
            sbReturn.Append("<" + TAG_PAGE1_REPORT_TITLE + ">" + Page1ReportTitle + "</" + TAG_PAGE1_REPORT_TITLE + ">\n");
            sbReturn.Append("<" + TAG_PAGE2_REPORT_TITLE + ">" + Page2ReportTitle + "</" + TAG_PAGE2_REPORT_TITLE + ">\n");
            sbReturn.Append("<" + TAG_PAGE1_SUB_TITLE + ">" + Page1SubTitle + "</" + TAG_PAGE1_SUB_TITLE + ">\n");
            sbReturn.Append("<" + TAG_ISSUE_A_TITLE + ">" + IssueATitle + "</" + TAG_ISSUE_A_TITLE + ">\n");
            sbReturn.Append("<" + TAG_ISSUE_B_TITLE + ">" + IssueBTitle + "</" + TAG_ISSUE_B_TITLE + ">\n");
            sbReturn.Append("<" + TAG_ISSUE_C_TITLE + ">" + IssueCTitle + "</" + TAG_ISSUE_C_TITLE + ">\n");
            sbReturn.Append("<" + TAG_ISSUE_D_TITLE + ">" + IssueDTitle + "</" + TAG_ISSUE_D_TITLE + ">\n");
            sbReturn.Append("<" + TAG_EMAIL_ADDRESS + ">" + EmailAddress + "</" + TAG_EMAIL_ADDRESS + ">\n");
            sbReturn.Append("<" + TAG_PAGE1_LEFT_MARGINE + ">" + Page1LeftMargin + "</" + TAG_PAGE1_LEFT_MARGINE + ">\n");
            sbReturn.Append("<" + TAG_PAGE1_SUBTITLE_LEFT_MARGIN + ">" + Page1SubtitleLeftMargin + "</" + TAG_PAGE1_SUBTITLE_LEFT_MARGIN + ">\n");
            sbReturn.Append("<" + TAG_PAGE2_LEFT_MARGIN + ">" + Page2LeftMargin + "</" + TAG_PAGE2_LEFT_MARGIN + ">\n");
            sbReturn.Append("<" + TAG_PRINT_GRAPH_TO_PDF + ">" + PrintGraphToPDF + "</" + TAG_PRINT_GRAPH_TO_PDF + ">\n");
            sbReturn.Append("<" + TAG_PRINT_REPORT_TO_PDF + ">" + PrintReportToPDF + "</" + TAG_PRINT_REPORT_TO_PDF + ">\n");
            sbReturn.Append("<" + TAG_TSL_FLOOR + ">" + TslFloor + "</" + TAG_TSL_FLOOR + ">\n");
            sbReturn.Append("<" + TAG_TSL_FLOOR_ON + ">" + TslFloorOn + "</" + TAG_TSL_FLOOR_ON + ">\n");
            sbReturn.Append("<" + TAG_PRINT_GRAPH + ">" + PrintGraph + "</" + TAG_PRINT_GRAPH + ">\n");
            sbReturn.Append("<" + TAG_DO_PHOTO + ">" + DoPhoto + "</" + TAG_DO_PHOTO + ">\n");

            sbReturn.Append("<" + TAG_CLIENT_SHELL_SCREENS + ">");
            for (int i = 0; i < _lstClientShellScreen.Count; i++)
            {
                if (i == 0)
                {
                    sbReturn.Append(_lstClientShellScreen[i].ToString());
                }
                else if (i < _lstClientShellScreen.Count)
                {
                    sbReturn.Append("," + _lstClientShellScreen[i].ToString());
                }
            }
            sbReturn.Append("</" + TAG_CLIENT_SHELL_SCREENS + ">\n");

            Resource resource = null;
            sbReturn.Append("<" + TAG_RESOURCES + ">");
            for (int i = 0; i < _lstResources.Count; i++)
            {
                resource = (Resource) _lstResources[i];
                sbReturn.Append("<" + TAG_RESOURCE + ">");
                sbReturn.Append("<" + TAG_RESOURCE_KEY + ">" + resource.key + "</" + TAG_RESOURCE_KEY + ">\n");
                sbReturn.Append("<" + TAG_RESOURCE_VALUE + ">" + resource.value + "</" + TAG_RESOURCE_VALUE + ">\n");
                sbReturn.Append("</" + TAG_RESOURCE + ">\n");
            }
            sbReturn.Append("</" + TAG_RESOURCES + ">\n");
            sbReturn.Append("<" + TAG_AUTO_ADJUST + ">" + AutoAdjust + "</" + TAG_AUTO_ADJUST + ">\n");

            sbReturn.Append("</" + ENTITY_NAME + ">\n");
            return sbReturn.ToString();
        }

        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }

                strXPath = "//" + TAG_RESOURCE;
                xNodes = xmlDoc.SelectNodes(strXPath);
                _lstResources.Clear();
                foreach (XmlNode xNode in xNodes)
                {
                    ParseResources(xNode);
                }
            }
            catch
            {
                _hasError = true;
            }
        }

        public void ParseResources(XmlNode xNode)
        {
            XmlNode xResultNode = null;

            Resource resource = new Resource();
            try
            {
                xResultNode = xNode.SelectSingleNode(Resource.TAG_KEY);
                resource.key = xResultNode.InnerText;

                xResultNode = xNode.SelectSingleNode(Resource.TAG_VALUE);
                resource.value = xResultNode.InnerText;
                _lstResources.Add(resource);
            }
            catch
            {
            }
        }

        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SHOW_NAVIGATION_CONTROLS);
                strTmp = xResultNode.InnerText;
                ShowNavigationControls = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_REPORT_TEMPLATE_NAME);
                ReportTemplateName = xResultNode.InnerText;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINTER_NAME);
                PrinterName = xResultNode.InnerText;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_KIOSK_MODE);
                strTmp = xResultNode.InnerText;
                IsKioskMode = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_PHOTO);
                strTmp = xResultNode.InnerText;
                DoPhoto = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_REPORTING_PDF);
                strTmp = xResultNode.InnerText;
                DoReportPdf = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_REPORTING_PRINTING);
                strTmp = xResultNode.InnerText;
                DoReportPrinting = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_REPORT_GRAPH);
                strTmp = xResultNode.InnerText;
                DoReportGraph = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_REPORT_EMAIL);
                strTmp = xResultNode.InnerText;
                DoReportEmail = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TSL_VALUE);
                strTmp = xResultNode.InnerText;
                _nTslValue = Convert.ToInt32(strTmp);
            }
            catch
            {
                _nTslValue = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DO_REPORTING_PREVIEW);
                strTmp = xResultNode.InnerText;
                DoReportPreview = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_HITS_VALUE);
                strTmp = xResultNode.InnerText;
                _dHitsValue = Convert.ToDouble(strTmp);
            }
            catch
            {
                _dHitsValue = 2;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AVG_MULTIPLIER_VALUE);
                strTmp = xResultNode.InnerText;
                _dAvgMultiplierValue = Convert.ToDouble(strTmp);
            }
            catch
            {
                _dAvgMultiplierValue = 2;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AVG_MULTIPLIER_ON);
                strTmp = xResultNode.InnerText;
                AvgMultiplier = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TSL_VALUE_LOW);
                strTmp = xResultNode.InnerText;
                _nTslValueLow = Convert.ToInt32(strTmp);
            }
            catch
            {
                _nTslValueLow = 10;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_CLIENT_SHELL_SCREENS);
                strTmp = xResultNode.InnerText;
                // comma delimited.  Put in list in order of left to right
                if (strTmp != null)
                {
                    string[] row;
                    char cDelimiter = ',';
                    row = strTmp.Split(cDelimiter);
                    for (int i = 0; i < row.Length; i++)
                    {
                        try
                        {
                            ClientShellScreen shellScreen = (ClientShellScreen)Enum.Parse(typeof(ClientShellScreen), row[i]);
                            _lstClientShellScreen.Add(shellScreen);
                        }
                        catch { }
                    }
                }

            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AUTO_ADJUST);
                strTmp = xResultNode.InnerText;
                AutoAdjust = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_AUDIO_ONLY);
                strTmp = xResultNode.InnerText;
                IsAudioOnly = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SHOW_RESULTS_SCREEN);
                strTmp = xResultNode.InnerText;
                ShowResultsScreen = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }
          
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_SPR_WEIGHT);
                strTmp = xResultNode.InnerText;
                TurnOffExtraSPRWeight = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_SPR1);
                strTmp = xResultNode.InnerText;
                TurnOffSPR1 = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }


            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_SPR2);
                strTmp = xResultNode.InnerText;
                TurnOffSPR2 = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }


            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_SPR3);
                strTmp = xResultNode.InnerText;
                TurnOffSPR3 = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_RISK_ANALYSIS);
                strTmp = xResultNode.InnerText;
                TurnOffRiskAnalysis = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_SPR71115);
                strTmp = xResultNode.InnerText;
                TurnOffSPR71115 = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_MOVING_TEST_AVERAGE);
                strTmp = xResultNode.InnerText;
                TurnOffMovingTestAverage = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_ON_PRESSURE_SENSOR);
                strTmp = xResultNode.InnerText;
                TurnOnPressureSensor = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TURN_OFF_ID_SCREEN);
                strTmp = xResultNode.InnerText;
                TurnOffIDScreen = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_GRAPH_TO_PDF);
                strTmp = xResultNode.InnerText;
                PrintGraphToPDF = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_BUTTON_TEXT);
                strTmp = xResultNode.InnerText;
                TestButtonText = strTmp;
            }
            catch
            {
            }
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE1_REPORT_TITLE);
                strTmp = xResultNode.InnerText;
                Page1ReportTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE2_REPORT_TITLE);
                strTmp = xResultNode.InnerText;
                Page2ReportTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE1_SUB_TITLE);
                strTmp = xResultNode.InnerText;
                Page1SubTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ISSUE_A_TITLE);
                strTmp = xResultNode.InnerText;
                IssueATitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ISSUE_B_TITLE);
                strTmp = xResultNode.InnerText;
                IssueBTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ISSUE_C_TITLE);
                strTmp = xResultNode.InnerText;
                IssueCTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ISSUE_D_TITLE);
                strTmp = xResultNode.InnerText;
                IssueDTitle = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE2_LEFT_MARGIN);
                strTmp = xResultNode.InnerText;
                Page2LeftMargin = Convert.ToInt32(strTmp);
            }
            catch
            {
            }
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE1_SUBTITLE_LEFT_MARGIN);
                strTmp = xResultNode.InnerText;
                Page1SubtitleLeftMargin = Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PAGE1_LEFT_MARGINE);
                strTmp = xResultNode.InnerText;
                Page1LeftMargin = Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_EMAIL_ADDRESS);
                strTmp = xResultNode.InnerText;
                EmailAddress = strTmp;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_GRAPH_TO_PDF);
                strTmp = xResultNode.InnerText;
                PrintGraphToPDF = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_REPORT_TO_PDF);
                strTmp = xResultNode.InnerText;
                PrintReportToPDF = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TSL_FLOOR_ON);
                strTmp = xResultNode.InnerText;
                TslFloorOn = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_PRINT_GRAPH);
                strTmp = xResultNode.InnerText;
                PrintGraph = Convert.ToBoolean(strTmp);
            }
            catch
            {
            }


            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TSL_FLOOR);
                strTmp = xResultNode.InnerText;
                TslFloor = Convert.ToInt32(strTmp);
            }
            catch
            {
            }

        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

    }

    public class Resource
    {
        public string key = null;
        public string value = null;

        public static readonly string TAG_KEY = "Key";
        public static readonly string TAG_VALUE = "Value";
    }
}
