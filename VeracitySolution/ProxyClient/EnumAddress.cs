using System;
using System.Xml;
using System.Text;
using System.Collections;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Address.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumAddress
	{
		//attributes
		/// <summary>AddressID Attribute type long</summary>
		private long _lAddressID = 0;
		/// <summary>State Attribute type string</summary>
		private string _strState = null;
		/// <summary>Country Attribute type string</summary>
		private string _strCountry = null;
		/// <summary>BeginDateCreated Attribute type DateTime</summary>
		private DateTime _dtBeginDateCreated = dtNull;
		/// <summary>EndDateCreated Attribute type DateTime</summary>
		private DateTime _dtEndDateCreated = dtNull;
		/// <summary>BeginDateModified Attribute type DateTime</summary>
		private DateTime _dtBeginDateModified = dtNull;
		/// <summary>EndDateModified Attribute type DateTime</summary>
		private DateTime _dtEndDateModified = dtNull;
		/// <summary>StreetNumber1 Attribute type string</summary>
		private string _strStreetNumber1 = null;
		/// <summary>StreetNumber2 Attribute type string</summary>
		private string _strStreetNumber2 = null;
		/// <summary>City Attribute type string</summary>
		private string _strCity = null;
		/// <summary>Zip Attribute type string</summary>
		private string _strZip = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumAddress"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "AddressID"; //Attribute id  name
		/// <summary>State Attribute type string</summary>
		public static readonly string TAG_STATE = "State"; //Table State field name
		/// <summary>Country Attribute type string</summary>
		public static readonly string TAG_COUNTRY = "Country"; //Table Country field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Table DateCreated field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Table DateModified field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Table DateModified field name
		/// <summary>StreetNumber1 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_1 = "StreetNumber1"; //Table StreetNumber1 field name
		/// <summary>StreetNumber2 Attribute type string</summary>
		public static readonly string TAG_STREET_NUMBER_2 = "StreetNumber2"; //Table StreetNumber2 field name
		/// <summary>City Attribute type string</summary>
		public static readonly string TAG_CITY = "City"; //Table City field name
		/// <summary>Zip Attribute type string</summary>
		public static readonly string TAG_ZIP = "Zip"; //Table Zip field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>AddressID is a Property in the Address Class of type long</summary>
		public long AddressID 
		{
			get{return _lAddressID;}
			set{_lAddressID = value;}
		}
		/// <summary>State is a Property in the Address Class of type String</summary>
		public string State 
		{
			get{return _strState;}
			set{_strState = value;}
		}
		/// <summary>Country is a Property in the Address Class of type String</summary>
		public string Country 
		{
			get{return _strCountry;}
			set{_strCountry = value;}
		}
		/// <summary>BeginDateCreated is a Property in the Address Class of type DateTime</summary>
		public DateTime BeginDateCreated 
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>EndDateCreated is a Property in the Address Class of type DateTime</summary>
		public DateTime EndDateCreated 
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>BeginDateModified is a Property in the Address Class of type DateTime</summary>
		public DateTime BeginDateModified 
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>EndDateModified is a Property in the Address Class of type DateTime</summary>
		public DateTime EndDateModified 
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>StreetNumber1 is a Property in the Address Class of type String</summary>
		public string StreetNumber1 
		{
			get{return _strStreetNumber1;}
			set{_strStreetNumber1 = value;}
		}
		/// <summary>StreetNumber2 is a Property in the Address Class of type String</summary>
		public string StreetNumber2 
		{
			get{return _strStreetNumber2;}
			set{_strStreetNumber2 = value;}
		}
		/// <summary>City is a Property in the Address Class of type String</summary>
		public string City 
		{
			get{return _strCity;}
			set{_strCity = value;}
		}
		/// <summary>Zip is a Property in the Address Class of type String</summary>
		public string Zip 
		{
			get{return _strZip;}
			set{_strZip = value;}
		}
		/// <summary>HasError Property in class Address and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Address and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>Address empty constructor</summary>
		public EnumAddress()
		{
		}
		/// <summary>Address Constructor takes string pStrData and Config</summary>
		public EnumAddress(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Address Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + AddressID.ToString() + "\n");
			sbReturn.Append(TAG_STATE + ":  " + State + "\n");
			sbReturn.Append(TAG_COUNTRY + ":  " + Country + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			sbReturn.Append(TAG_STREET_NUMBER_1 + ":  " + StreetNumber1 + "\n");
			sbReturn.Append(TAG_STREET_NUMBER_2 + ":  " + StreetNumber2 + "\n");
			sbReturn.Append(TAG_CITY + ":  " + City + "\n");
			sbReturn.Append(TAG_ZIP + ":  " + Zip + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Address</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_ID + ">" + AddressID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_STATE + ">" + State + "</" + TAG_STATE + ">\n");
			sbReturn.Append("<" + TAG_COUNTRY + ">" + Country + "</" + TAG_COUNTRY + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			sbReturn.Append("<" + TAG_STREET_NUMBER_1 + ">" + StreetNumber1 + "</" + TAG_STREET_NUMBER_1 + ">\n");
			sbReturn.Append("<" + TAG_STREET_NUMBER_2 + ">" + StreetNumber2 + "</" + TAG_STREET_NUMBER_2 + ">\n");
			sbReturn.Append("<" + TAG_CITY + ">" + City + "</" + TAG_CITY + ">\n");
			sbReturn.Append("<" + TAG_ZIP + ">" + Zip + "</" + TAG_ZIP + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
/// <summary>Parses XML, puts in arraylist</summary>
public static void ParseToAddressArrayList(string pStrXml, ArrayList pRefArrayList)
		{
			try
			{
				Address address = null;
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + Address.ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					if (pRefArrayList != null)
					{
						address = new Address();
						address.Parse(xNode);
						pRefArrayList.Add(address);
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				AddressID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STATE);
				State = xResultNode.InnerText;
				if (State.Trim().Length == 0)
					State = null;
			}
			catch  
			{
				State = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_COUNTRY);
				Country = xResultNode.InnerText;
				if (Country.Trim().Length == 0)
					Country = null;
			}
			catch  
			{
				Country = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_1);
				StreetNumber1 = xResultNode.InnerText;
				if (StreetNumber1.Trim().Length == 0)
					StreetNumber1 = null;
			}
			catch  
			{
				StreetNumber1 = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_STREET_NUMBER_2);
				StreetNumber2 = xResultNode.InnerText;
				if (StreetNumber2.Trim().Length == 0)
					StreetNumber2 = null;
			}
			catch  
			{
				StreetNumber2 = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CITY);
				City = xResultNode.InnerText;
				if (City.Trim().Length == 0)
					City = null;
			}
			catch  
			{
				City = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ZIP);
				Zip = xResultNode.InnerText;
				if (Zip.Trim().Length == 0)
					Zip = null;
			}
			catch  
			{
				Zip = null;
			}
		}
	}
}

//END OF Address CLASS FILE


