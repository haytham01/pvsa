﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace Veracity.Client.Proxy
{
    public class Emailer
    {
         private string smtpServerName = null;
        private int smtpServerPort = 587;
        private string subject = null;
        private string _subjectProcessed = null;
        private string body = null;
        private string _bodyProcessed = null;
        private string from = null;
        private string to = null;
        private string smtpUsername = null;
        private string smtpPassword = null;
        private string displayName = null;
        private bool _bIsBodyHtml = false;
        private string _attachmentPath = null;
        private bool _bEnableSSL = false;
        private MemoryStream _attachmentMemoryStream1 = null;
        private string _attachmentMemoryStreamFilename1 = null;

        private MemoryStream _attachmentMemoryStream2 = null;
        private string _attachmentMemoryStreamFilename2 = null;

        private MemoryStream _attachmentMemoryStream3 = null;
        private string _attachmentMemoryStreamFilename3 = null;

        private bool _hasError = false;
        private ErrorCode _errorCode = null;
        private ArrayList _arrlstSendErrorCodes = new ArrayList();

        public static bool mailSent = false;

        public ArrayList arrlstSendErrorCodes
        {
            get { return _arrlstSendErrorCodes; }
        }
        public bool EnableSSL
        {
            set { _bEnableSSL = value; }
        }
        public string attachmentPath
        {
            set { _attachmentPath = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public bool IsBodyHtml
        {
            set { _bIsBodyHtml = value; }
        }
        public string DisplayName
        {
            set { displayName = value; }
        }
        public int SmtpServerPort
        {
            set { smtpServerPort = value; }
        }
        public string To
        {
            set { to = value; }
        }
        public string From
        {
            set { from = value; }
        }
        public string Body
        {
            set { body = value; }
        }
        public string Subject
        {
            set { subject = value; }
        }
        public string SmtpServerName
        {
            set { smtpServerName = value; }
        }
        public string SmtpUsername
        {
            set { smtpUsername = value; }
        }
        public string SmtpPassword
        {
            set { smtpPassword = value; }
        }
        public MemoryStream attachmentMemoryStream1
        {
            set { _attachmentMemoryStream1 = value; }
        }
        public string attachmentMemoryStreamFilename1
        {
            set { _attachmentMemoryStreamFilename1 = value; }
        }
        public MemoryStream attachmentMemoryStream2
        {
            set { _attachmentMemoryStream2 = value; }
        }
        public string attachmentMemoryStreamFilename2
        {
            set { _attachmentMemoryStreamFilename2 = value; }
        }
        public MemoryStream attachmentMemoryStream3
        {
            set { _attachmentMemoryStream3 = value; }
        }
        public string attachmentMemoryStreamFilename3
        {
            set { _attachmentMemoryStreamFilename3 = value; }
        }
        public Emailer()
        {
        }
        public string SendMail()
        {

            //Builed The MSG
            string retStatus = null;
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.To.Add(to);
            //msg.To.Add("another.reciver@yahoo.com");
            msg.From = new MailAddress(from, displayName, System.Text.Encoding.UTF8);
            _subjectProcessed = ReplaceTags(subject);
            msg.Subject = _subjectProcessed;
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            _bodyProcessed = ReplaceTags(body);
            msg.Body = _bodyProcessed;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = _bIsBodyHtml;
            msg.Priority = MailPriority.Normal;

            if (_attachmentPath != null)
            {
                Attachment attachFile = new Attachment(_attachmentPath);
                msg.Attachments.Add(attachFile);
            }

            if ( (_attachmentMemoryStream1 != null) && (_attachmentMemoryStreamFilename1 != null))
            {
                Attachment attachFile = new Attachment(_attachmentMemoryStream1, _attachmentMemoryStreamFilename1, "application/pdf");
                msg.Attachments.Add(attachFile);
            }
            if ((_attachmentMemoryStream2 != null) && (_attachmentMemoryStreamFilename2 != null))
            {
                Attachment attachFile = new Attachment(_attachmentMemoryStream2, _attachmentMemoryStreamFilename2, "application/pdf");
                msg.Attachments.Add(attachFile);
            }
            if ((_attachmentMemoryStream3 != null) && (_attachmentMemoryStreamFilename3 != null))
            {
                Attachment attachFile = new Attachment(_attachmentMemoryStream3, _attachmentMemoryStreamFilename3, "application/pdf");
                msg.Attachments.Add(attachFile);
            }


            //Add the Creddentials

            SmtpClient client = new SmtpClient();
            //client.UseDefaultCredentials = false;
            if ((smtpUsername != null) && (smtpUsername.Length > 0))
            {
                client.Credentials = new System.Net.NetworkCredential(smtpUsername, smtpPassword);
            }
            else
            {
                client.Credentials = new System.Net.NetworkCredential();
            }
            client.Port = smtpServerPort;//or use 587            

            client.Host = smtpServerName;
            client.EnableSsl = _bEnableSSL;
            client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
            object userState = msg;
            try
            {
                //you can also call client.Send(msg)
                //client.SendAsync(msg, userState);
                client.Send(msg);
            }
            catch (System.Net.Mail.SmtpException e)
            {
                _hasError = true;
                _errorCode = new ErrorCode();
                _errorCode.ErrorCodeID = 100;
                _errorCode.Code = "EMAIL_ERROR";
                _errorCode.Description = "General emailer error.";
                _errorCode.Trace = e.ToString() + Environment.NewLine + e.StackTrace;
                _arrlstSendErrorCodes.Add(_errorCode);
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorCode = new ErrorCode();
                _errorCode.ErrorCodeID = 101;
                _errorCode.Code = "EMAIL_ERROR";
                _errorCode.Description = "General emailer error.";
                _errorCode.Trace = ex.ToString() + Environment.NewLine + ex.StackTrace;
                _arrlstSendErrorCodes.Add(_errorCode);
            }

            return retStatus;
        }

        public void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            ErrorCode err = new ErrorCode();
            MailMessage mail = (MailMessage)e.UserState;
            string subject = mail.Subject;

            if (e.Cancelled)
            {
                _hasError = true;
                string cancelled = string.Format("[{0}] Send canceled.", subject);
                err.ErrorCodeID = 101;
                err.Code = "EMAIL_ERROR_CANCELLED";
                err.Description = cancelled;
                _arrlstSendErrorCodes.Add(err);
                //MessageBox.Show(cancelled);
            }
            if (e.Error != null)
            {
                _hasError = true;
                string error = String.Format("[{0}] {1}", subject, e.Error.ToString());
                err.ErrorCodeID = 102;
                err.Code = "EMAIL_ERROR_CANCELLED";
                err.Description = error;
                _arrlstSendErrorCodes.Add(err);
                //MessageBox.Show(error);
            }
            else
            {
                _hasError = false;
                //MessageBox.Show("Message sent.");
            }
            mailSent = true;
        }

        public string ReplaceTags(string pStrData)
        {
            string strReturn = pStrData;

            try
            {
                if (strReturn != null)
                {
                }
            }
            catch
            {
            }
            return strReturn;
        }
    }
}
