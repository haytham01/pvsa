using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  LnkTestTemplateScreen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH     7/14/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the LnkTestTemplateScreen database table.
    /// </summary>
    public class LnkTestTemplateScreen
    {
        //attributes
        /// <summary>LnkTestTemplateScreenID Attribute type long</summary>
        private long _lLnkTestTemplateScreenID = 0;
        /// <summary>TestTemplateID Attribute type long</summary>
        private long _lTestTemplateID = 0;
        /// <summary>ScreenID Attribute type long</summary>
        private long _lScreenID = 0;
        /// <summary>DateCreated Attribute type DateTime</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>DateModified Attribute type DateTime</summary>
        private DateTime _dtDateModified = dtNull;
        /// <summary>FlowOrderIndex Attribute type long</summary>
        private long _lFlowOrderIndex = 0;
        /// <summary>TransitionDelayInMs Attribute type long</summary>
        private long _lTransitionDelayInMs = 0;
        /// <summary>IsIgnored Attribute type bool</summary>
        private bool? _bIsIgnored = null;
        /// <summary>IsReported Attribute type bool</summary>
        private bool? _bIsReported = null;
        /// <summary>Notes Attribute type string</summary>
        private string _strNotes = null;
        /// <summary>ImageFileName Attribute type string</summary>
        private string _strImageFileName = null;
        /// <summary>HasAudio Attribute type bool</summary>
        private bool? _bHasAudio = null;
        /// <summary>AudioFileName Attribute type string</summary>
        private string _strAudioFileName = null;

        private static DateTime dtNull = new DateTime();
        private bool _hasError = false;
        private ErrorCode _errorCode = null;

        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "LnkTestTemplateScreen"; //Table name to abstract

        // Attribute variables
        /// <summary>TAG_ID Attribute</summary>
        public static readonly string TAG_ID = "LnkTestTemplateScreenID"; //Attribute id  name
        /// <summary>TestTemplateID Attribute type string</summary>
        public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Table TestTemplateID field name
        /// <summary>ScreenID Attribute type string</summary>
        public static readonly string TAG_SCREEN_ID = "ScreenID"; //Table ScreenID field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>DateModified Attribute type string</summary>
        public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
        /// <summary>FlowOrderIndex Attribute type string</summary>
        public static readonly string TAG_FLOW_ORDER_INDEX = "FlowOrderIndex"; //Table FlowOrderIndex field name
        /// <summary>TransitionDelayInMs Attribute type string</summary>
        public static readonly string TAG_TRANSITION_DELAY_IN_MS = "TransitionDelayInMs"; //Table TransitionDelayInMs field name
        /// <summary>IsIgnored Attribute type string</summary>
        public static readonly string TAG_IS_IGNORED = "IsIgnored"; //Table IsIgnored field name
        /// <summary>IsReported Attribute type string</summary>
        public static readonly string TAG_IS_REPORTED = "IsReported"; //Table IsReported field name
        /// <summary>Notes Attribute type string</summary>
        public static readonly string TAG_NOTES = "Notes"; //Table Notes field name
        /// <summary>ImageFileName Attribute type string</summary>
        public static readonly string TAG_IMAGE_FILE_NAME = "ImageFileName"; //Table ImageFileName field name
        /// <summary>HasAudio Attribute type string</summary>
        public static readonly string TAG_HAS_AUDIO = "HasAudio"; //Table HasAudio field name
        /// <summary>AudioFileName Attribute type string</summary>
        public static readonly string TAG_AUDIO_FILE_NAME = "AudioFileName"; //Table AudioFileName field name


        /*********************** CUSTOM NON-META BEGIN *********************/

        /*********************** CUSTOM NON-META END *********************/


        //properties
        /// <summary>LnkTestTemplateScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long LnkTestTemplateScreenID
        {
            get { return _lLnkTestTemplateScreenID; }
            set { _lLnkTestTemplateScreenID = value; }
        }
        /// <summary>TestTemplateID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TestTemplateID
        {
            get { return _lTestTemplateID; }
            set { _lTestTemplateID = value; }
        }
        /// <summary>ScreenID is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>DateCreated is a Property in the LnkTestTemplateScreen Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>DateModified is a Property in the LnkTestTemplateScreen Class of type DateTime</summary>
        public DateTime DateModified
        {
            get { return _dtDateModified; }
            set { _dtDateModified = value; }
        }
        /// <summary>FlowOrderIndex is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long FlowOrderIndex
        {
            get { return _lFlowOrderIndex; }
            set { _lFlowOrderIndex = value; }
        }
        /// <summary>TransitionDelayInMs is a Property in the LnkTestTemplateScreen Class of type long</summary>
        public long TransitionDelayInMs
        {
            get { return _lTransitionDelayInMs; }
            set { _lTransitionDelayInMs = value; }
        }
        /// <summary>IsIgnored is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsIgnored
        {
            get { return _bIsIgnored; }
            set { _bIsIgnored = value; }
        }
        /// <summary>IsReported is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? IsReported
        {
            get { return _bIsReported; }
            set { _bIsReported = value; }
        }
        /// <summary>Notes is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string Notes
        {
            get { return _strNotes; }
            set { _strNotes = value; }
        }
        /// <summary>ImageFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string ImageFileName
        {
            get { return _strImageFileName; }
            set { _strImageFileName = value; }
        }
        /// <summary>HasAudio is a Property in the LnkTestTemplateScreen Class of type bool</summary>
        public bool? HasAudio
        {
            get { return _bHasAudio; }
            set { _bHasAudio = value; }
        }
        /// <summary>AudioFileName is a Property in the LnkTestTemplateScreen Class of type String</summary>
        public string AudioFileName
        {
            get { return _strAudioFileName; }
            set { _strAudioFileName = value; }
        }
        /// <summary>HasError Property in class LnkTestTemplateScreen and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class LnkTestTemplateScreen and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }
        //Constructors
        /// <summary>LnkTestTemplateScreen empty constructor</summary>
        public LnkTestTemplateScreen()
        {
        }
        /// <summary>LnkTestTemplateScreen Constructor takes string pStrData and Config</summary>
        public LnkTestTemplateScreen(string pStrData)
        {
            Parse(pStrData);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the LnkTestTemplateScreen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + LnkTestTemplateScreenID.ToString() + "\n");
            sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
            sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
            }
            sbReturn.Append(TAG_FLOW_ORDER_INDEX + ":  " + FlowOrderIndex + "\n");
            sbReturn.Append(TAG_TRANSITION_DELAY_IN_MS + ":  " + TransitionDelayInMs + "\n");
            sbReturn.Append(TAG_IS_IGNORED + ":  " + IsIgnored + "\n");
            sbReturn.Append(TAG_IS_REPORTED + ":  " + IsReported + "\n");
            sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");
            sbReturn.Append(TAG_IMAGE_FILE_NAME + ":  " + ImageFileName + "\n");
            sbReturn.Append(TAG_HAS_AUDIO + ":  " + HasAudio + "\n");
            sbReturn.Append(TAG_AUDIO_FILE_NAME + ":  " + AudioFileName + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of LnkTestTemplateScreen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<LnkTestTemplateScreen>\n");
            sbReturn.Append("<" + TAG_ID + ">" + LnkTestTemplateScreenID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(DateModified))
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
            }
            sbReturn.Append("<" + TAG_FLOW_ORDER_INDEX + ">" + FlowOrderIndex + "</" + TAG_FLOW_ORDER_INDEX + ">\n");
            sbReturn.Append("<" + TAG_TRANSITION_DELAY_IN_MS + ">" + TransitionDelayInMs + "</" + TAG_TRANSITION_DELAY_IN_MS + ">\n");
            sbReturn.Append("<" + TAG_IS_IGNORED + ">" + IsIgnored + "</" + TAG_IS_IGNORED + ">\n");
            sbReturn.Append("<" + TAG_IS_REPORTED + ">" + IsReported + "</" + TAG_IS_REPORTED + ">\n");
            sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
            sbReturn.Append("<" + TAG_IMAGE_FILE_NAME + ">" + ImageFileName + "</" + TAG_IMAGE_FILE_NAME + ">\n");
            sbReturn.Append("<" + TAG_HAS_AUDIO + ">" + HasAudio + "</" + TAG_HAS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_FILE_NAME + ">" + AudioFileName + "</" + TAG_AUDIO_FILE_NAME + ">\n");
            sbReturn.Append("</LnkTestTemplateScreen>" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                LnkTestTemplateScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
                TestTemplateID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TestTemplateID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
                ScreenID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
                DateModified = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_FLOW_ORDER_INDEX);
                FlowOrderIndex = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                FlowOrderIndex = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_TRANSITION_DELAY_IN_MS);
                TransitionDelayInMs = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                TransitionDelayInMs = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_IGNORED);
                IsIgnored = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsIgnored = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_REPORTED);
                IsReported = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsReported = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_NOTES);
                Notes = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IMAGE_FILE_NAME);
                ImageFileName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_HAS_AUDIO);
                HasAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                HasAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_AUDIO_FILE_NAME);
                AudioFileName = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
        }
    }
}

//END OF LnkTestTemplateScreen CLASS FILE

