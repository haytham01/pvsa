﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

namespace Veracity.Client.Proxy
{
    public class RESTHelper
    {
        private string _strRESTBaseUrl = null;
        private string _strRESTAPIKey = null;
        private string _strRESTAuthKey = null;

        private enum HTTPRequestMethods { GET = 1, POST = 2, PUT = 3, DELETE = 4 };

        private const string PARAM_API_KEY = "apikey";
        private const string PARAM_AUTH_KEY = "authkey";
        private const string PARAM_RESPONSE_FORMAT = "responseformat";
        private const string PARAM_REQUEST_FORMAT_JSON = "JSON";
        private const string PARAM_REQUEST_FORMAT_XML = "XML";
        private const string AND_SYMBOL = "&";
        private const string EQUAL_SYMBOL = "=";
        private const string CDATA_BEGIN = "<![CDATA[";
        private const string CDATA_END = "]]>";

        private bool _hasError = false;

        public static readonly string REST_TEST_SESSION_URI = "/testsessions";
        public static readonly string DATA_PARAM_TEST_SESSION = "testsession";

        public bool HasError
        {
            get { return _hasError; }
        }

        public RESTHelper(string pStrRESTBaseUrl, string pStrAPIKey, string pStrAuthKey)
        {
            _strRESTBaseUrl = pStrRESTBaseUrl;
            _strRESTAPIKey = pStrAPIKey;
            _strRESTAuthKey = pStrAuthKey;
        }

        private HttpWebRequest CreatePostRequest(string url, string postData,
            HTTPRequestMethods method, string contentType)
        {
            HttpWebRequest request = null;
            Uri uri = new Uri(url);
            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = method.ToString();
            request.ContentType = contentType;
            request.ContentLength = postData.Length;
            using (Stream writeStream = request.GetRequestStream())
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytes = encoding.GetBytes(postData);
                writeStream.Write(bytes, 0, bytes.Length);
            }

            return request;
        }

        private HttpWebRequest CreateGetRequest(ref StringBuilder parameters, string url)
        {
            HttpWebRequest request = null;
            try
            {

                request = (HttpWebRequest)WebRequest.Create(url + "?" + parameters.ToString());
            }
            catch (Exception ex)
            {
                request = null;
            }
            return request;
        }


        public void EncodeItems(ref StringBuilder baseRequest, string jsonData, string xmlData,
            string pStrRequestFormat, string pStrParamKey)
        {

            string objectData = null;
            switch (pStrRequestFormat)
            {
                case PARAM_REQUEST_FORMAT_JSON:
                    objectData = jsonData;
                    break;
                case PARAM_REQUEST_FORMAT_XML:
                    objectData = xmlData;
                    break;
                default:
                    objectData = jsonData;
                    break;
            }
            EncodeAndAddItem(ref baseRequest, pStrParamKey, objectData);

        }

        public void EncodeBaseItems(ref StringBuilder baseRequest, string apikey, string authkey, string pStrResponseFormat = null)
        {
            EncodeAndAddItem(ref baseRequest, PARAM_API_KEY, apikey);
            EncodeAndAddItem(ref baseRequest, PARAM_AUTH_KEY, authkey);
            if (pStrResponseFormat != null)
            {
                EncodeAndAddItem(ref baseRequest, PARAM_RESPONSE_FORMAT, pStrResponseFormat);
            }
        }

        /// <summary>
        /// Encodes an item and ads it to the string.
        /// </summary>
        /// <param name="baseRequest">The previously encoded data.</param>
        /// <param name="dataItem">The data to encode.</param>
        /// <returns>A string containing the old data and the previously encoded data.</returns>
        public void EncodeAndAddItem(ref StringBuilder baseRequest, string key, string dataItem)
        {
            if (baseRequest == null)
            {
                baseRequest = new StringBuilder();
            }
            if (baseRequest.Length != 0)
            {
                baseRequest.Append(AND_SYMBOL);
            }

            baseRequest.Append(key);
            baseRequest.Append(EQUAL_SYMBOL);

            // for some reason Syste.Web introduces errors to library
            //baseRequest.Append(System.Web.HttpUtility.UrlEncode(dataItem));
            baseRequest.Append(Uri.EscapeUriString(dataItem));
        }

        public string RemoveCDATA(string s)
        {
            string strReturn = s;

            if (s != null)
            {
                strReturn = strReturn.Replace(CDATA_BEGIN, String.Empty);
                strReturn = strReturn.Replace(CDATA_END, String.Empty);
            }

            return strReturn;
        }

        public HttpWebResponse ExecuteAction(HttpWebRequest request, StringBuilder parameters, ref string pStrResponsBody,
         ref HttpStatusCode pHttpStatusCode)
        {
            HttpWebResponse response = null;
            pHttpStatusCode = HttpStatusCode.BadRequest;
            try
            {
                //request.KeepAlive = false;
                WebResponse ws = request.GetResponse();

                // Get the response stream  
                StreamReader reader = new StreamReader(ws.GetResponseStream());

                // Read the whole contents and return as a string  
                string text = reader.ReadToEnd();
                pStrResponsBody = text;

                response = (HttpWebResponse)request.GetResponse();
                pHttpStatusCode = response.StatusCode;

            }
            catch (Exception ex)
            {
                pStrResponsBody = ex.Message;
                pHttpStatusCode = HttpStatusCode.BadRequest;
            }
            return response;
        }

        private bool CallREST(string pStrURL, string pStrAPIKey, string pStrAuthKey, StringBuilder pSbDataParameters,
            HTTPRequestMethods pMethod, ref string pStrResponseText, ref HttpStatusCode pReturnHttpStatusCode,
            string pStrResponseFormat = PARAM_REQUEST_FORMAT_XML)
        {
            bool bSuccess = false;
            try
            {
                HttpWebRequest request = null;
                EncodeBaseItems(ref pSbDataParameters, pStrAPIKey, pStrAuthKey, pStrResponseFormat);
                if (pMethod == HTTPRequestMethods.GET)
                {
                    request = CreateGetRequest(ref pSbDataParameters, pStrURL);
                }
                else
                {
                    request = CreatePostRequest(pStrURL, pSbDataParameters.ToString(), pMethod, "application/text");
                }

                HttpWebResponse response = ExecuteAction(request, pSbDataParameters, ref pStrResponseText, ref pReturnHttpStatusCode);

                //if (pStrResponseText != null)
                //{
                //    pStrResponseText = conditionRESTResponseXMLText(pStrResponseText);
                //}

                bSuccess = true;
            }
            catch (Exception ex)
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        private string conditionRESTResponseXMLText(string pStrResponseText)
        {
            string strReturn = pStrResponseText;
            string strBegPattern = " xmlns";
            string strEndPattern = ">";
            int bInd = pStrResponseText.IndexOf(strBegPattern);
            int eInd = pStrResponseText.IndexOf(strEndPattern);
            if ((bInd > 0) && (eInd > 0))
            {
                strReturn = pStrResponseText.Remove(bInd, eInd - bInd);
            }
            return strReturn;
        }

        public void SendPostData(string pStrData)
        {
            try
            {
                string strResponseText = null;
                HttpStatusCode returnedHttpStatusCode = HttpStatusCode.BadRequest;
                string strServiceURL = null;
                StringBuilder dataParameters = null;

                HTTPRequestMethods method = RESTHelper.HTTPRequestMethods.POST;
                strServiceURL = _strRESTBaseUrl + REST_TEST_SESSION_URI;
                EncodeAndAddItem(ref dataParameters, DATA_PARAM_TEST_SESSION, pStrData);
                
                bool bSuccess = CallREST(strServiceURL, _strRESTAPIKey, _strRESTAuthKey,
               dataParameters, method, ref strResponseText,
               ref returnedHttpStatusCode, "XML");
                if (returnedHttpStatusCode != HttpStatusCode.OK)
                {
                    _hasError = true;
                }
            }
            catch (Exception ex)
            {
                _hasError = true;
            }
        }

    }
}
