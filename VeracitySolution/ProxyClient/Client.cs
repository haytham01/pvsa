using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Client.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the Client database table.
	/// </summary>
	public class Client
	{
		//attributes
		/// <summary>ClientID Attribute type long</summary>
		private long _lClientID = 0;
		/// <summary>DateCreated Attribute type DateTime</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type DateTime</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DateHeartbeat Attribute type DateTime</summary>
		private DateTime _dtDateHeartbeat = dtNull;
		/// <summary>Code Attribute type string</summary>
		private string _strCode = null;
		/// <summary>Description Attribute type string</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute type string</summary>
		private string _strVisibleCode = null;
		/// <summary>TestInProgress Attribute type bool</summary>
		private bool? _bTestInProgress = null;
		/// <summary>AppMetaData Attribute type string</summary>
		private string _strAppMetaData = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "Client"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "ClientID"; //Attribute id  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DateHeartbeat Attribute type string</summary>
		public static readonly string TAG_DATE_HEARTBEAT = "DateHeartbeat"; //Table DateHeartbeat field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>Description Attribute type string</summary>
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		/// <summary>VisibleCode Attribute type string</summary>
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		/// <summary>TestInProgress Attribute type string</summary>
		public static readonly string TAG_TEST_IN_PROGRESS = "TestInProgress"; //Table TestInProgress field name
		/// <summary>AppMetaData Attribute type string</summary>
		public static readonly string TAG_APP_META_DATA = "AppMetaData"; //Table AppMetaData field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>ClientID is a Property in the Client Class of type long</summary>
		public long ClientID 
		{
			get{return _lClientID;}
			set{_lClientID = value;}
		}
		/// <summary>DateCreated is a Property in the Client Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the Client Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DateHeartbeat is a Property in the Client Class of type DateTime</summary>
		public DateTime DateHeartbeat 
		{
			get{return _dtDateHeartbeat;}
			set{_dtDateHeartbeat = value;}
		}
		/// <summary>Code is a Property in the Client Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the Client Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the Client Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>TestInProgress is a Property in the Client Class of type bool</summary>
		public bool? TestInProgress 
		{
			get{return _bTestInProgress;}
			set{_bTestInProgress = value;}
		}
		/// <summary>AppMetaData is a Property in the Client Class of type String</summary>
		public string AppMetaData 
		{
			get{return _strAppMetaData;}
			set{_strAppMetaData = value;}
		}
		/// <summary>HasError Property in class Client and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Client and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>Client empty constructor</summary>
		public Client()
		{
		}
		/// <summary>Client Constructor takes string pStrData and Config</summary>
		public Client(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Client Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + ClientID.ToString() + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(DateHeartbeat))
			{
				sbReturn.Append(TAG_DATE_HEARTBEAT + ":  " + DateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_HEARTBEAT + ":\n");
			}
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TEST_IN_PROGRESS + ":  " + TestInProgress + "\n");
			sbReturn.Append(TAG_APP_META_DATA + ":  " + AppMetaData + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Client</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<Client>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ClientID + "</" + TAG_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(DateHeartbeat))
			{
				sbReturn.Append("<" + TAG_DATE_HEARTBEAT + ">" + DateHeartbeat.ToString() + "</" + TAG_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_HEARTBEAT + "></" + TAG_DATE_HEARTBEAT + ">\n");
			}
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TEST_IN_PROGRESS + ">" + TestInProgress + "</" + TAG_TEST_IN_PROGRESS + ">\n");
			sbReturn.Append("<" + TAG_APP_META_DATA + ">" + AppMetaData + "</" + TAG_APP_META_DATA + ">\n");
			sbReturn.Append("</Client>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ClientID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_HEARTBEAT);
				DateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_IN_PROGRESS);
				TestInProgress = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			TestInProgress = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_APP_META_DATA);
				AppMetaData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF Client CLASS FILE


