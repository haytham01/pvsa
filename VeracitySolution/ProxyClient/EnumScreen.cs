using System;
using System.Xml;
using System.Text;
using System.Collections;

namespace Veracity.Client.Proxy
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  Screen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH     7/25/2013	Created
    /// 
    /// ----------------------------------------------------
    /// </summary>
    public class EnumScreen
    {
        //attributes
        /// <summary>ScreenID Attribute type long</summary>
        private long _lScreenID = 0;
        /// <summary>ScreenTypeID Attribute type long</summary>
        private long _lScreenTypeID = 0;
        /// <summary>BeginDateCreated Attribute type DateTime</summary>
        private DateTime _dtBeginDateCreated = dtNull;
        /// <summary>EndDateCreated Attribute type DateTime</summary>
        private DateTime _dtEndDateCreated = dtNull;
        /// <summary>ScreenText Attribute type string</summary>
        private string _strScreenText = null;
        /// <summary>ScreenImage Attribute type byte[]</summary>
        private byte[] _byteScreenImage = null;
        /// <summary>AudioData Attribute type byte[]</summary>
        private byte[] _byteAudioData = null;
        /// <summary>IsText Attribute type bool</summary>
        private bool? _bIsText = false;
        /// <summary>IsAudio Attribute type bool</summary>
        private bool? _bIsAudio = false;
        /// <summary>ScreenTextShort Attribute type string</summary>
        private string _strScreenTextShort = null;

        private static DateTime dtNull = new DateTime();
        private bool _hasError = false;
        private ErrorCode _errorCode = null;

        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "EnumScreen"; //Table name to abstract

        // Attribute variables
        /// <summary>TAG_ID Attribute</summary>
        public static readonly string TAG_ID = "ScreenID"; //Attribute id  name
        /// <summary>ScreenTypeID Attribute type string</summary>
        public static readonly string TAG_SCREEN_TYPE_ID = "ScreenTypeID"; //Table ScreenTypeID field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Table DateCreated field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Table DateCreated field name
        /// <summary>ScreenText Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT = "ScreenText"; //Table ScreenText field name
        /// <summary>ScreenImage Attribute type string</summary>
        public static readonly string TAG_SCREEN_IMAGE = "ScreenImage"; //Table ScreenImage field name
        /// <summary>AudioData Attribute type string</summary>
        public static readonly string TAG_AUDIO_DATA = "AudioData"; //Table AudioData field name
        /// <summary>IsText Attribute type string</summary>
        public static readonly string TAG_IS_TEXT = "IsText"; //Table IsText field name
        /// <summary>IsAudio Attribute type string</summary>
        public static readonly string TAG_IS_AUDIO = "IsAudio"; //Table IsAudio field name
        /// <summary>ScreenTextShort Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT_SHORT = "ScreenTextShort"; //Table ScreenTextShort field name


        /*********************** CUSTOM NON-META BEGIN *********************/
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                ScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TYPE_ID);
                ScreenTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
                BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
                EndDateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT);
                ScreenText = xResultNode.InnerText;
                if (ScreenText.Trim().Length == 0)
                    ScreenText = null;
            }
            catch
            {
                ScreenText = null;
            }

            //try
            //{
            //    xResultNode = xNode.SelectSingleNode(TAG_SCREEN_IMAGE);
            //    ScreenImage = xResultNode.InnerText;
            //    if (ScreenImage.Trim().Length == 0)
            //        ScreenImage = null;
            //}
            //catch
            //{
            //    ScreenImage = null;
            //}

            //try
            //{
            //    xResultNode = xNode.SelectSingleNode(TAG_AUDIO_DATA);
            //    AudioData = xResultNode.InnerText;
            //    if (AudioData.Trim().Length == 0)
            //        AudioData = null;
            //}
            //catch
            //{
            //    AudioData = null;
            //}

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_TEXT);
                IsText = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsText = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_AUDIO);
                IsAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT_SHORT);
                ScreenTextShort = xResultNode.InnerText;
                if (ScreenTextShort.Trim().Length == 0)
                    ScreenTextShort = null;
            }
            catch
            {
                ScreenTextShort = null;
            }
        }

        /*********************** CUSTOM NON-META END *********************/


        //properties
        /// <summary>ScreenID is a Property in the Screen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>ScreenTypeID is a Property in the Screen Class of type long</summary>
        public long ScreenTypeID
        {
            get { return _lScreenTypeID; }
            set { _lScreenTypeID = value; }
        }
        /// <summary>BeginDateCreated is a Property in the Screen Class of type DateTime</summary>
        public DateTime BeginDateCreated
        {
            get { return _dtBeginDateCreated; }
            set { _dtBeginDateCreated = value; }
        }
        /// <summary>EndDateCreated is a Property in the Screen Class of type DateTime</summary>
        public DateTime EndDateCreated
        {
            get { return _dtEndDateCreated; }
            set { _dtEndDateCreated = value; }
        }
        /// <summary>ScreenText is a Property in the Screen Class of type String</summary>
        public string ScreenText
        {
            get { return _strScreenText; }
            set { _strScreenText = value; }
        }
        /// <summary>ScreenImage is a Property in the Screen Class of type byte[]</summary>
        public byte[] ScreenImage
        {
            get { return _byteScreenImage; }
            set { _byteScreenImage = value; }
        }
        /// <summary>AudioData is a Property in the Screen Class of type byte[]</summary>
        public byte[] AudioData
        {
            get { return _byteAudioData; }
            set { _byteAudioData = value; }
        }
        /// <summary>IsText is a Property in the Screen Class of type bool</summary>
        public bool? IsText
        {
            get { return _bIsText; }
            set { _bIsText = value; }
        }
        /// <summary>IsAudio is a Property in the Screen Class of type bool</summary>
        public bool? IsAudio
        {
            get { return _bIsAudio; }
            set { _bIsAudio = value; }
        }
        /// <summary>ScreenTextShort is a Property in the Screen Class of type String</summary>
        public string ScreenTextShort
        {
            get { return _strScreenTextShort; }
            set { _strScreenTextShort = value; }
        }
        /// <summary>HasError Property in class Screen and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class Screen and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }
        //Constructors
        /// <summary>Screen empty constructor</summary>
        public EnumScreen()
        {
        }
        /// <summary>Screen Constructor takes string pStrData and Config</summary>
        public EnumScreen(string pStrData)
        {
            Parse(pStrData);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the Screen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + ScreenID.ToString() + "\n");
            sbReturn.Append(TAG_SCREEN_TYPE_ID + ":  " + ScreenTypeID + "\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
            }
            sbReturn.Append(TAG_SCREEN_TEXT + ":  " + ScreenText + "\n");
            sbReturn.Append(TAG_SCREEN_IMAGE + ":  " + ScreenImage + "\n");
            sbReturn.Append(TAG_AUDIO_DATA + ":  " + AudioData + "\n");
            sbReturn.Append(TAG_IS_TEXT + ":  " + IsText + "\n");
            sbReturn.Append(TAG_IS_AUDIO + ":  " + IsAudio + "\n");
            sbReturn.Append(TAG_SCREEN_TEXT_SHORT + ":  " + ScreenTextShort + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of Screen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_ID + ">" + ScreenID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TYPE_ID + ">" + ScreenTypeID + "</" + TAG_SCREEN_TYPE_ID + ">\n");
            if (!dtNull.Equals(BeginDateCreated))
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
            }
            if (!dtNull.Equals(EndDateCreated))
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
            }
            sbReturn.Append("<" + TAG_SCREEN_TEXT + ">" + ScreenText + "</" + TAG_SCREEN_TEXT + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_IMAGE + ">" + ScreenImage + "</" + TAG_SCREEN_IMAGE + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_DATA + ">" + AudioData + "</" + TAG_AUDIO_DATA + ">\n");
            sbReturn.Append("<" + TAG_IS_TEXT + ">" + IsText + "</" + TAG_IS_TEXT + ">\n");
            sbReturn.Append("<" + TAG_IS_AUDIO + ">" + IsAudio + "</" + TAG_IS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TEXT_SHORT + ">" + ScreenTextShort + "</" + TAG_SCREEN_TEXT_SHORT + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parses XML, puts in arraylist</summary>
        public static void ParseToScreenArrayList(string pStrXml, ArrayList pRefArrayList)
        {
            try
            {
                Screen screen = null;
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + Screen.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    if (pRefArrayList != null)
                    {
                        screen = new Screen();
                        screen.Parse(xNode);
                        pRefArrayList.Add(screen);
                    }
                }
            }
            catch (Exception e)
            {
            }
        }
  
    }
}

//END OF Screen CLASS FILE

