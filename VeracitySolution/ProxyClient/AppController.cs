﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace Veracity.Client.Proxy
{
    public class AppController
    {
        private List<GlobalLanguage> _lstGlobalLanguage = new List<GlobalLanguage>();
        private List<TestLink> _lstGlobalTestLink = new List<TestLink>();
        private List<Test> _lstTest = new List<Test>();
        public int TestLimitNum = -1;
        public int TestCurrentNum = 0;
        public bool IsMissionEssentials = false;
        public bool DoOutputCSVEncryption = false;
        public bool SavePDFResults = false;

        private const String XML_DECLARATION = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        public static readonly string ENTITY_NAME = "AppController";
        public static readonly string TAG_GLOBAL_LANGUAGES = "GlobalLanguages";
        public static readonly string TAG_GLOBAL_TEST_LINKS = "GlobalTestLinks";
        public static readonly string TAG_TESTS = "Tests";
        public static readonly string TAG_GLOBAL_TEST_LIMIT = "TestLimit";
        public static readonly string TAG_GLOBAL_TEST_CURRENT = "TestCurrentNum";
        public static readonly string TAG_GLOBAL_IS_MISSION_ESSENTIALS = "IsMissionEssentials";
        public static readonly string TAG_GLOBAL_DO_OUTPUT_CSV_ENCRYPTION = "DoOutputCSVEncryption";
        public static readonly string TAG_GLOBAL_SAVE_PDF_RESULTS = "SavePDFResults";

        public string _strControllerPath = null;

        private bool _hasError = false;

        public string ControllerPath
        {
            get { return _strControllerPath; }
            set { _strControllerPath = value; }
        }
        public List<GlobalLanguage> lstGlobalLanguage
        {
            get { return _lstGlobalLanguage; }
            set { _lstGlobalLanguage = value; }
        }
        public List<TestLink> lstGlobalTestLink
        {
            get { return _lstGlobalTestLink; }
            set { _lstGlobalTestLink = value; }
        }
        public List<Test> lstTest
        {
            get { return _lstTest; }
            set { _lstTest = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }

        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the global languages
                strXPath = "//" + TAG_GLOBAL_LANGUAGES + "/" + GlobalLanguage.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                GlobalLanguage globallanguage = null;
                _lstGlobalLanguage.Clear();
                if (xNodes != null)
                {
                    foreach (XmlNode nd in xNodes)
                    {
                        globallanguage = new GlobalLanguage();
                        globallanguage.Parse(nd);
                        _lstGlobalLanguage.Add(globallanguage);
                    }
                }

                // get the test limit num
                var node = xmlDoc.SelectSingleNode("/AppController/" + TAG_GLOBAL_TEST_LIMIT);
                if (node != null)
                {
                    TestLimitNum = Convert.ToInt32(node.InnerText);
                }
                else
                {
                    TestLimitNum = -1;
                }

                node = xmlDoc.SelectSingleNode("/AppController/" + TAG_GLOBAL_TEST_CURRENT);
                if (node != null)
                {
                    TestCurrentNum = Convert.ToInt32(node.InnerText);
                }
                else
                {
                    TestCurrentNum = 0;
                }

                node = xmlDoc.SelectSingleNode("/AppController/" + TAG_GLOBAL_IS_MISSION_ESSENTIALS);
                if (node != null)
                {
                    IsMissionEssentials = Convert.ToBoolean(node.InnerText);
                }
                else
                {
                    IsMissionEssentials = false;
                }

                node = xmlDoc.SelectSingleNode("/AppController/" + TAG_GLOBAL_SAVE_PDF_RESULTS);
                if (node != null)
                {
                    SavePDFResults = Convert.ToBoolean(node.InnerText);
                }
                else
                {
                    SavePDFResults = false;
                }

                node = xmlDoc.SelectSingleNode("/AppController/" + TAG_GLOBAL_DO_OUTPUT_CSV_ENCRYPTION);
                if (node != null)
                {
                    DoOutputCSVEncryption = Convert.ToBoolean(node.InnerText);
                }
                else
                {
                    DoOutputCSVEncryption = false;
                }
                // get the global test links
                strXPath = "//" + TAG_GLOBAL_TEST_LINKS + "/" + TestLink.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                TestLink testLink = null;
                _lstGlobalTestLink.Clear();
                if (xNodes != null)
                {
                    foreach (XmlNode nd in xNodes)
                    {
                        testLink = new TestLink();
                        testLink.Parse(nd);
                        _lstGlobalTestLink.Add(testLink);
                    }
                }


                // get the element
                strXPath = "//" + ENTITY_NAME + "/" + TAG_TESTS + "/" + Test.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                Test test = null;
                XmlNode xNode = null;
                _lstTest.Clear();
                for (int i = 0; i < xNodes.Count; i++)
                {
                    xNode = xNodes[i];
                    test = new Test();
                    test.TestIndex = i;
                    test.Parse(xNode.OuterXml);
                    _lstTest.Add(test);
                }
            }
            catch (Exception ex)
            {
                _hasError = true;
            }
        }

        public string ToXml()
        {
            StringBuilder sbReturn = new StringBuilder(XML_DECLARATION);

            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_GLOBAL_LANGUAGES + ">\n");
            for (int i = 0; i < _lstGlobalLanguage.Count; i++)
            {
                sbReturn.Append(_lstGlobalLanguage[i].ToXml());
            }
            sbReturn.Append("</" + TAG_GLOBAL_LANGUAGES + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_TEST_LINKS + ">\n");
            for (int i = 0; i < _lstGlobalTestLink.Count; i++)
            {
                sbReturn.Append(_lstGlobalTestLink[i].ToXml());
            }
            sbReturn.Append("</" + TAG_GLOBAL_TEST_LINKS + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_TEST_LIMIT + ">");
            sbReturn.Append(TestLimitNum);
            sbReturn.Append("</" + TAG_GLOBAL_TEST_LIMIT + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_TEST_CURRENT + ">");
            sbReturn.Append(TestCurrentNum);
            sbReturn.Append("</" + TAG_GLOBAL_TEST_CURRENT + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_IS_MISSION_ESSENTIALS + ">");
            sbReturn.Append(IsMissionEssentials);
            sbReturn.Append("</" + TAG_GLOBAL_IS_MISSION_ESSENTIALS + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_SAVE_PDF_RESULTS + ">");
            sbReturn.Append(SavePDFResults);
            sbReturn.Append("</" + TAG_GLOBAL_SAVE_PDF_RESULTS + ">\n");

            sbReturn.Append("<" + TAG_GLOBAL_DO_OUTPUT_CSV_ENCRYPTION + ">");
            sbReturn.Append(DoOutputCSVEncryption);
            sbReturn.Append("</" + TAG_GLOBAL_DO_OUTPUT_CSV_ENCRYPTION + ">\n");

            sbReturn.Append("<" + TAG_TESTS + ">\n");
            for (int i = 0; i < _lstTest.Count; i++)
            {
                sbReturn.Append(_lstTest[i].ToXml());
            }
            sbReturn.Append("</" + TAG_TESTS + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">\n");
            return sbReturn.ToString();
        }

        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        public void Serialize()
        {
            try
            {
                String fullExeNameAndPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                String pathToSerialize = System.IO.Path.GetDirectoryName(fullExeNameAndPath) + System.IO.Path.DirectorySeparatorChar + ENTITY_NAME + ".xml";

                System.IO.File.WriteAllText(pathToSerialize, ToXml());
            }
            catch { _hasError = true; }
        }

        public void SerializeWorking()
        {
            try
            {
                String pathToSerialize = _strControllerPath;
                System.IO.File.WriteAllText(pathToSerialize, ToXml());
            }
            catch { _hasError = true; }
        }
        //public void SaveTest(int pIntTestIndex)
        //{
        //    try
        //    {
        //        XmlTextReader textReader = new XmlTextReader(_strControllerPath);
        //        // Read until end of file
        //        while (textReader.Read())
        //        {
        //            XmlNodeType nType = textReader.NodeType;
        //            if (nType == XmlNodeType.Element)
        //            {
        //                //Console.WriteLine("Element:" + textReader.Name.ToString());
        //            }
        //        }
        //    }
        //    catch { _hasError = true; }
        //}
    }

    public class GlobalLanguage
    {
        public string Code = null;
        public string Description = null;

        public static readonly string ENTITY_NAME = "GlobalLanguage";
        public static readonly string TAG_LANGUAGE_CODE = "Code";
        public static readonly string TAG_LANGUAGE_DESCRIPTION = "Description";

        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_LANGUAGE_CODE);
                Code = xResultNode.InnerText;
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_LANGUAGE_DESCRIPTION);
                Description = xResultNode.InnerText;
            }
            catch
            {
            }
        }

        public string ToXml()
        {
            StringBuilder sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_LANGUAGE_CODE + ">" + Code + "</" + TAG_LANGUAGE_CODE + ">\n");
            sbReturn.Append("<" + TAG_LANGUAGE_DESCRIPTION + ">" + Description + "</" + TAG_LANGUAGE_DESCRIPTION + ">\n");
            sbReturn.Append("</" + ENTITY_NAME + ">\n");
            return sbReturn.ToString();
        }
    }

    public class Test
    {
        private List<TestLanguage> _lstTestLanguage = new List<TestLanguage>();
        private List<Screen> _lstScreen = new List<Screen>();
        private Client _objClient = new Client();
        private List<Screen> _lstScreenQuestionsOnly = new List<Screen>();
        private List<LnkTestTemplateScreen> _lstLnkTestTemplateScreen = new List<LnkTestTemplateScreen>();
        private TestTemplate _objTestTemplate = null;
        private TestType _objTestType = null;
        private HostConfig _objHostConfig = null;
        private TestLanguage _selectedTestLanguage = null;
        private LnkTestTemplateScreen _currentLnkTestTemplateScreen = null;
        private int _nTestIndex = 0;

        public static readonly string ENTITY_NAME = "Test";
        public static readonly string TAG_TEST_LANGUAGES = "TestLanguages";
        public static readonly string TAG_LAST_TSL_VALUE = "LastTslValue";
        public static readonly string TAG_LNK_TEST_TEMPLATE_SCREENS = "LnkTestTemplateScreens";
        public static readonly string TAG_SCREENS = "Screens";

        private bool _hasError = false;

        public Client objClient
        {
            get { return _objClient; }
            set { _objClient = value; }
        }
        public int TestIndex
        {
            get { return _nTestIndex; }
            set { _nTestIndex = value; }
        }
        public List<Screen> lstScreenQuestionsOnly
        {
            get { return _lstScreenQuestionsOnly; }
            set { _lstScreenQuestionsOnly = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public List<LnkTestTemplateScreen> lstLnkTestTemplateScreen
        {
            get { return _lstLnkTestTemplateScreen; }
            set { _lstLnkTestTemplateScreen = value; }
        }
        public List<TestLanguage> lstTestLanguage
        {
            get { return _lstTestLanguage; }
            set { _lstTestLanguage = value; }
        }
        public List<Screen> lstScreen
        {
            get { return _lstScreen; }
            set { _lstScreen = value; }
        }
        public TestTemplate objTestTemplate
        {
            get { return _objTestTemplate; }
            set { _objTestTemplate = value; }
        }
        public TestType objTestType
        {
            get { return _objTestType; }
            set { _objTestType = value; }
        }
        public HostConfig objHostConfig
        {
            get { return _objHostConfig; }
            set { _objHostConfig = value; }
        }
        public TestLanguage selectedTestLanguage
        {
            get { return _selectedTestLanguage; }
            set { _selectedTestLanguage = value; }
        }
        public LnkTestTemplateScreen currentLnkTestTemplateScreen
        {
            get { return _currentLnkTestTemplateScreen; }
            set { _currentLnkTestTemplateScreen = value; }
        }
     
        public Test()
        {
            _objHostConfig = new HostConfig();
        }

        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                //// get the element
                //strXPath = "//" + ENTITY_NAME;
                //xNodes = xmlDoc.SelectNodes(strXPath);
                //foreach (XmlNode xNode in xNodes)
                //{
                //    Parse(xNode);
                //}

                _objHostConfig = new HostConfig(pStrXml);
                _objTestTemplate = new TestTemplate(pStrXml);
                _objTestType = new TestType(pStrXml);
                _objClient = new Client(pStrXml);

                strXPath = "//" + TAG_TEST_LANGUAGES + "/" + TestLanguage.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                TestLanguage testlanguage = null;
                foreach (XmlNode xNode in xNodes)
                {
                    testlanguage = new TestLanguage();
                    testlanguage.Parse(xNode.OuterXml);
                    _lstTestLanguage.Add(testlanguage);
                }

                strXPath = "//" + TAG_SCREENS + "/" + Screen.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                Screen screen = null;
                foreach (XmlNode xNode in xNodes)
                {
                    screen = new Screen();
                    screen.Parse(xNode.OuterXml);
                    _lstScreen.Add(screen);
                    if (screen.ScreenTypeID == 3)
                    {
                        _lstScreenQuestionsOnly.Add(screen);
                    }
                }

                strXPath = "//" + TAG_LNK_TEST_TEMPLATE_SCREENS + "/" + LnkTestTemplateScreen.ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                LnkTestTemplateScreen lnkTestTemplateScreen = null;
                foreach (XmlNode xNode in xNodes)
                {
                    lnkTestTemplateScreen = new LnkTestTemplateScreen();
                    lnkTestTemplateScreen.Parse(xNode.OuterXml);
                    _lstLnkTestTemplateScreen.Add(lnkTestTemplateScreen);
                }

            }
            catch
            {
                _hasError = true;
            }
        }

        //public void Save(string pStrControllerFilePath)
        //{
        //    if ((pStrControllerFilePath != null) && (File.Exists(pStrControllerFilePath)) && (_objHostConfig != null))
        //    {
        //        string controllerXml = File.ReadAllText(pStrControllerFilePath);
        //    }

        //}


        public string ToXml()
        {
            StringBuilder sbReturn = new StringBuilder();

            if (_objClient == null)
            {
                _objClient = new Client();
            }

            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append(_objClient.ToXml());
            sbReturn.Append(_objHostConfig.ToXml());
            sbReturn.Append(_objTestTemplate.ToXml());
            sbReturn.Append(_objTestType.ToXml());

            sbReturn.Append("<" + TAG_LNK_TEST_TEMPLATE_SCREENS + ">\n");
            if (_lstLnkTestTemplateScreen != null)
            {
                for (int i = 0; i < _lstLnkTestTemplateScreen.Count; i++)
                {
                    sbReturn.Append(_lstLnkTestTemplateScreen[i].ToXml());
                }
            }
            sbReturn.Append("</" + TAG_LNK_TEST_TEMPLATE_SCREENS + ">\n");

            sbReturn.Append("<" + TAG_SCREENS + ">\n");
            if (_lstScreen != null)
            {
                for (int i = 0; i < _lstScreen.Count; i++)
                {
                    sbReturn.Append(_lstScreen[i].ToXml());
                }
            }
            sbReturn.Append("</" + TAG_SCREENS + ">\n");


            sbReturn.Append("<" + TAG_TEST_LANGUAGES + ">\n");
            if (_lstTestLanguage != null)
            {
                for (int i = 0; i < _lstTestLanguage.Count; i++)
                {
                    sbReturn.Append(_lstTestLanguage[i].ToXml());
                }
            }
            sbReturn.Append("</" + TAG_TEST_LANGUAGES + ">\n");

            sbReturn.Append("</" + ENTITY_NAME + ">\n");
            return sbReturn.ToString();
        }

        public void setCurrentLnkTestTemplateScreen(int index)
        {
            if ((lstLnkTestTemplateScreen != null) && (index < lstLnkTestTemplateScreen.Count))
            {
                _currentLnkTestTemplateScreen = lstLnkTestTemplateScreen[index];
            }
        }

        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }
    }
}
