﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using PdfFileWriter;
using System.IO;
using System.Resources;

namespace Veracity.Client.Proxy
{
    public class PrintManager
    {
        // hold data in the class scope

        int PageNo = 1;
        PrintPreviewDialog objPrev;
        PrintDocument objDoc;
        Pen pen = new Pen(Color.Black);
        Brush brsh = new SolidBrush(Color.Black);
        TestSession globalTestScreen = new TestSession();
        string globalResultsFolderPath = "C:\\Veracity";
        AnalyzerKOTQuestions globalAnalyzerKOT = new AnalyzerKOTQuestions();
        Anaylyzer26Questions globalAnalyzer26Q = new Anaylyzer26Questions();
        Anaylyzer18Questions globalAnalyzer18Q = new Anaylyzer18Questions();
        public PrintManager() { }

        ResourceManager rm = null;
        public PrintManager(ResourceManager rmtmp)
        {
            // Create a resource manager. 
            rm = rmtmp;
        }

        public string T(string s)
        {
            string ret = null;
            try
            {
                ret = rm.GetString(s);
            }
            catch (Exception ex)
            { }
            return ret;
        }

        // 18 Questions - Title
        private string _sTitle18Q = "Substance Abuse Test";
        public string sTitle18Q
        {
            get { return _sTitle18Q; }
            set { _sTitle18Q = value; }
        }

        private string _sTitle18QPage2 = "Detailed Summary Report Test";
        public string sTitle18QPage2
        {
            get { return _sTitle18QPage2; }
            set { _sTitle18QPage2 = value; }
        }

        private string _sSubTitle18Q = "Verification Behavioral Analysis Report Test";
        public string sSubTitle18Q
        {
            get { return _sSubTitle18Q; }
            set { _sSubTitle18Q = value; }
        }

        private int _iTitle18QPage2LeftMargin = 200;
        public int iTitle18QPage2LeftMargin
        {
            get { return _iTitle18QPage2LeftMargin; }
            set { _iTitle18QPage2LeftMargin = value; }
        }

        private int _iTitle18QLeftMargin = 240;
        public int iTitle18QLeftMargin
        {
            get { return _iTitle18QLeftMargin; }
            set { _iTitle18QLeftMargin = value; }
        }

        private int _iSubTitle18QLeftMargin = 130;
        public int iSubTitle18QLeftMargin
        {
            get { return _iSubTitle18QLeftMargin; }
            set { _iSubTitle18QLeftMargin = value; }
        }


        private string _sIssueATitle18Q = "ISSUE A TEST";
        public string sIssueATitle18Q
        {
            get { return _sIssueATitle18Q; }
            set { _sIssueATitle18Q = value; }
        }

        private string _sIssueBTitle18Q = "ISSUE B TEST";
        public string sIssueBTitle18Q
        {
            get { return _sIssueBTitle18Q; }
            set { _sIssueBTitle18Q = value; }
        }

        private string _sIssueCTitle18Q = "ISSUE C TEST";
        public string sIssueCTitle18Q
        {
            get { return _sIssueCTitle18Q; }
            set { _sIssueCTitle18Q = value; }
        }


        private string _sIssueDTitle18Q = "CONCLUSION";
        public string sIssueDTitle18Q
        {
            get { return _sIssueDTitle18Q; }
            set { _sIssueDTitle18Q = value; }
        }


        private bool _bPrintToPDF = false;
        public bool bPrintToPDF
        {
            get { return _bPrintToPDF; }
            set { _bPrintToPDF = value; }
        }




        // Print preview for the 15 questions report
        public void CreateReportType2PrintPreview(TestSession testSession)
        {
            //objPrev = new PrintPreviewDialog();
            //objDoc = new PrintDocument();

            //globalTestScreen = testSession;

            //objDoc.PrintPage += new PrintPageEventHandler(CreateReportType2);

            //objPrev.Document = objDoc;
            //objPrev.ShowDialog();
        }


        // Report for the 24 questions report - first report
        public void CreateReportType1PrintPreview(TestSession testSession)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;

            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department - San Diego
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department - San Diego
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }

            objPrev.Document = objDoc;
            objPrev.PrintPreviewControl.Zoom = 1.0;
            // show print preview dialog in maximized state
            objPrev.WindowState = FormWindowState.Maximized;
            objPrev.Select();
            objPrev.ShowDialog();
        }

        // Report for the 24 questions report - analyzer KOT report
        public void CreateReportType1PrintPreview(TestSession testSession, AnalyzerKOTQuestions analyzerKOT)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;
            globalAnalyzerKOT = analyzerKOT;



            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // 
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }

            objPrev.Document = objDoc;
            objPrev.PrintPreviewControl.Zoom = 1.0;
            // show print preview dialog in maximized state
            objPrev.WindowState = FormWindowState.Maximized;
            objPrev.Select();
            objPrev.ShowDialog();
        }


        // Report for the 18 questions report -for 18 Analyzer Question report
        public void CreateReportType1PrintPreview(TestSession testSession, Anaylyzer18Questions analyzer, string gResultsFolderPath)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;
            globalAnalyzer18Q = analyzer;
            globalResultsFolderPath = gResultsFolderPath; // have the MyGlobals.gResultsFolderPath

            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department - San Diego
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) //
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }

            objPrev.Document = objDoc;
            objPrev.PrintPreviewControl.Zoom = 1.0;
            // show print preview dialog in maximized state
            objPrev.WindowState = FormWindowState.Maximized;
            objPrev.Select();
            objPrev.ShowDialog();
        }

        // Report for the 26 questions report -for 26 Analyzer Question report
        public void CreateReportType1PrintPreview(TestSession testSession, Anaylyzer26Questions analyzer)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;
            globalAnalyzer26Q = analyzer;

            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department - San Diego
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) //
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }

            objPrev.Document = objDoc;
            objPrev.PrintPreviewControl.Zoom = 1.0;
            // show print preview dialog in maximized state
            objPrev.WindowState = FormWindowState.Maximized;
            objPrev.Select();
            objPrev.ShowDialog();
        }

        public void PrintReportToDefaultPrinter(TestSession testSession)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;

            objDoc = new PrintDocument();
            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }
            objDoc.Print();
        }

        public void PrintReportToPdf(TestSession testSession)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;

            objDoc = new PrintDocument();
            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }
            objDoc.Print();
        }

        public void PrintReportToPdf(TestSession testSession, AnalyzerKOTQuestions analyzerKOT)
        {
            using (PdfDocument pdfDocument = new PdfDocument(PaperType.Letter, false, UnitOfMeasure.Inch, GetPdfFileName(testSession)))
            {
                using (PdfPrintDocument pdfPrintDocument = new PdfPrintDocument(pdfDocument, 300.0))
                {
                    globalTestScreen = testSession;
                    globalAnalyzerKOT = analyzerKOT;

                    if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 18) //
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
                    }

                    pdfPrintDocument.SetMargins(1, 1, 1, 1);
                    pdfPrintDocument.CropRect = new RectangleF(0.0F, 0.0F, 8F, 11F);
                    pdfPrintDocument.AddPagesToPdfDocument();

                    pdfDocument.CreateFile();
                }
            }
        }

        public void PrintReportToDefaultPrinter(TestSession testSession, AnalyzerKOTQuestions analyzerKOT)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;
            globalAnalyzerKOT = analyzerKOT;

            objDoc = new PrintDocument();
            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) //
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }
            objDoc.Print();
        }


        public void PrintReportToDefaultPrinter(TestSession testSession, Anaylyzer18Questions analyzer, string gResultsFolderPath)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();
            string pdfDocumentName = "1234test";

            globalTestScreen = testSession;
            globalAnalyzer18Q = analyzer;
            globalResultsFolderPath = gResultsFolderPath;

            objDoc = new PrintDocument();
            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                    pdfDocumentName = globalTestScreen.TestSessionID + "_page1";
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                    pdfDocumentName = globalTestScreen.TestSessionID + "_page2";
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }
            objDoc.Print();





            // print to PDF file also
            //bPrintToPDF = true;
            if (bPrintToPDF)
            {
                PrintDialog pDia = new PrintDialog();
                PrinterSettings ps = new PrinterSettings();
                pDia.Document = objDoc;
                pDia.Document.DocumentName = pdfDocumentName;
                //ps.PrinterName = "CutePDF Writer";
                ps.PrinterName = "PDFCreator";
                ps.PrintToFile = false;


                ps.PrintFileName = "C:\\SafeScreenReports\\" + pDia.Document.DocumentName + ".pdf";
                // Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                //pDia.Document.OriginAtMargins = true;
                //ps.DefaultPageSettings.Margins.Left = 2;
                objDoc.PrinterSettings = ps;

                try
                {
                    objDoc.Print();
                }

                catch (Exception exc)
                {
                    MessageBox.Show("Printing error!  Please install PDFCreator.\n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void PrintReportToPdf(TestSession testSession, Anaylyzer18Questions analyzer)
        {
            using (PdfDocument pdfDocument = new PdfDocument(PaperType.Letter, false, UnitOfMeasure.Inch, GetPdfFileName(testSession)))
            {
                using (PdfPrintDocument pdfPrintDocument = new PdfPrintDocument(pdfDocument, 300.0))
                {

                    globalTestScreen = testSession;
                    globalAnalyzer18Q = analyzer;

                    if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
                    }

                    pdfPrintDocument.SetMargins(1, 1, 1, 1);
                    pdfPrintDocument.CropRect = new RectangleF(0.0F, 0.0F, 8F, 11F);
                    pdfPrintDocument.AddPagesToPdfDocument();

                    pdfDocument.CreateFile();
                }
            }
        }

        string GetPdfFileName(TestSession testSession)
        {
            string executablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;

            return string.Format(@"{0}\Workspace\Results\{1:yyyyMMddhhmmss}-{2}-{3}.pdf", System.IO.Path.GetDirectoryName(executablePath),
                testSession.TestSessionID,
                testSession.objClient.ClientID,
                string.IsNullOrEmpty(testSession.objTestSubject.DriverLicenseNumber) ? "NA" : testSession.objTestSubject.DriverLicenseNumber);

            // XXX comment out below to put the matching PDF file to the XML file
            // return string.Format(@"{0}\Workspace\Results\{1:yyyyMMddhhmmss}-{2}-{3}.pdf", System.IO.Path.GetDirectoryName(executablePath), DateTime.Now, 
            //    testSession.objClient.ClientID,
            //    string.IsNullOrEmpty(testSession.objTestSubject.DriverLicenseNumber) ? "NA" : testSession.objTestSubject.DriverLicenseNumber);
        }

        public void PrintReportToPdf(TestSession testSession, Anaylyzer26Questions analyzer)
        {
            using (PdfDocument pdfDocument = new PdfDocument(PaperType.Letter, false, UnitOfMeasure.Inch, GetPdfFileName(testSession)))
            {
                using (PdfPrintDocument pdfPrintDocument = new PdfPrintDocument(pdfDocument, 300.0))
                {

                    // the method that will print one page at a time to PrintDocument
                    globalTestScreen = testSession;
                    globalAnalyzer26Q = analyzer;

                    if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
                    {
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                        }
                        if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                        {
                            pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                        }
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
                    }
                    if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
                    {
                        pdfPrintDocument.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
                    }

                    pdfPrintDocument.SetMargins(1, 1, 1, 1);
                    pdfPrintDocument.CropRect = new RectangleF(0.0F, 0.0F, 8F, 11F);
                    pdfPrintDocument.AddPagesToPdfDocument();

                    pdfDocument.CreateFile();
                }
            }
        }

        public void PrintReportToDefaultPrinter(TestSession testSession, Anaylyzer26Questions analyzer)
        {
            objPrev = new PrintPreviewDialog();
            objDoc = new PrintDocument();

            globalTestScreen = testSession;
            globalAnalyzer26Q = analyzer;

            objDoc = new PrintDocument();
            if (testSession.lstScreenQuestionOnly.Count() == 26) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_26Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport26QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 18) // Sheriff Department
            {
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_1)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType1);
                }
                if (testSession.ReportType == TestSession.REPORT_TYPE.REPORT_18Q_2)
                {
                    objDoc.PrintPage += new PrintPageEventHandler(CreateReport18QType2);
                }
            }
            if (testSession.lstScreenQuestionOnly.Count() == 24) // Sheriff Department
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport24QType4);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 15) // Weapons, Smuggling, Terriosm
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReport15QType1);
            }
            if (testSession.lstScreenQuestionOnly.Count() == 16) // KOT 
            {
                objDoc.PrintPage += new PrintPageEventHandler(CreateReportKOTType1);
            }
            objDoc.Print();
        }

        public void PrintReportToDefaultPrinter(string pStrPrinterName)
        {
            // no need to implement
        }


        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  24 Questions Report - Type1 version 1
        /// ///////
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport24QType1(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDriverLicense = null;

            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);


            Veracity.Client.Proxy.Screen screen = null;



            //Horzontal lines
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60));

            // Create the next 30 lines
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 30; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20);

            // Drivers License
            strDriverLicense = "CA DL#: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 270, eve.MarginBounds.Top - 20);

            // Title
            eve.Graphics.DrawString("Taxi Driver Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 70);


            // headers

            int iAccidents = 60;
            int iIllegalDrugs = 140;
            int iSexOffender = 220;
            int iArrest = 300;
            int iSmuggling = 380;
            int iTerrorism = 460;
            int iUnknown = 540;
            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + 3);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + 3);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + 17);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + 31);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + 44);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + 3);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 360, eve.MarginBounds.Top + 3);

            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;

                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMIT";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////
                if ((i >= 2) & (i <= 4))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);
                }

                if ((i >= 5) & (i <= 7))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);
                }

                if ((i >= 8) & (i <= 10))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);
                }
                if ((i >= 11) & (i <= 13))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);
                }
                if ((i >= 14) & (i <= 16))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);
                }
                if ((i >= 17) & (i <= 19))
                {
                    iStartTerrorism += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);
                }
                if ((i >= 20) & (i <= 23))
                {
                    iStartUnknown += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);
                }
            }
        }


        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  24 Questions Report - Type1 version 2
        /// ///////
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport24QType2(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen: ";
            string strDriverLicense = null;

            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";


            Veracity.Client.Proxy.Screen screen = null;



            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown));

            // Create the next 30 lines + 7 lines of spaces
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 37; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + iMoveTableDown),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Date
            strDate += DateTime.Now.ToString("yyyMMdd");
            //strDate += globalTestScreen.objTestSubject.DateCreated;
            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            strTime += DateTime.Now.ToString("HH:mm:ss tt");
            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 40 + iMoveTableDown);
            // TouchScreen
            strTouchScreen += "SDSO_001";
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "CA DL#: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 150;
            int iHeader2 = 110;
            eve.Graphics.DrawString("San Diego County Sheriff Department", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Taxi Driver Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 270, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAccidents = 60 + iMoveTableDown;
            int iIllegalDrugs = 140 + iMoveTableDown + iSpacing;
            int iSexOffender = 240 + iMoveTableDown + iSpacing;
            int iArrest = 340 + iMoveTableDown + iSpacing;
            int iSmuggling = 440 + iMoveTableDown + iSpacing;
            int iTerrorism = 540 + iMoveTableDown + iSpacing;
            int iUnknown = 640 + iMoveTableDown + iSpacing;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing;

            // Footer
            int iFooterYaxis = 880 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown;
            int iSignificant = 3 + iMoveTableDown;
            int iSignificant2 = 17 + iMoveTableDown;
            int iSignificant3 = 31 + iMoveTableDown;
            int iSignificant4 = 44 + iMoveTableDown;
            int iQuestionNumber = 3 + iMoveTableDown;
            int iQuestions = 3 + iMoveTableDown;
            int iQuestions2 = 17 + iMoveTableDown;
            int iQuestions3 = 31 + iMoveTableDown;
            int iRisk = 3 + iMoveTableDown;
            int iRisk2 = 17 + iMoveTableDown;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 380, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("One SPR per issue indicates Caution", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 667, eve.MarginBounds.Top + iRisk2);

            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);

            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////     IF ZONE HAS ONE HIT THEN LIMITED ( YELLOW )
            //////////     IF ZONE HAS MORE THAN ONE HIT THAN HIGH ( RED )
            //////////     ADMITS IS ALWAYS RED
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                switch (i)
                {

                    case 2: // questions 3,4,5
                    case 3: // questions 3,4,5
                    case 4: // questions 3,4,5
                        if (i == 2)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 3)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 4)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 5: // questions 6,7,8
                    case 6: // questions 6,7,8
                    case 7: // questions 6,7,8
                        if (i == 5)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 8: // questions 9,10,11
                    case 9: // questions 9,10,11
                    case 10: // questions 9,10,11
                        if (i == 8)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 9)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 11: // questions 12,13,14
                    case 12: // questions 12,13,14
                    case 13: // questions 12,13,14
                        if (i == 11)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 12)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 13)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 14: // questions 3,4,5
                    case 15: // questions 3,4,5
                    case 16: // questions 3,4,5
                        if (i == 14)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 16)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    case 17: // questions 3,4,5
                    case 18: // questions 3,4,5
                    case 19: // questions 3,4,5
                        if (i == 17)
                        {
                            if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter3++;
                            }
                        }

                        ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                        break;
                    case 20: // questions 3,4,5
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                        if (i == 20)
                        {
                            if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter1++;
                            }
                        }
                        if (i == 21)
                        {
                            if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter2++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter3++;
                            }
                        }

                        ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                        break;
                    default:
                        break;
                }


            }





            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;


                if (ReactionZone1Counter == 1)
                {
                    ReactionZone1Risk = "LIMITED";
                    drawBrushRISKZone1.Color = Color.DarkBlue;
                }
                else if (ReactionZone1Counter >= 2)
                {
                    ReactionZone1Risk = "HIGH";
                    drawBrushRISKZone1.Color = Color.Red;
                }

                if (ReactionZone2Counter == 1)
                {
                    ReactionZone2Risk = "LIMITED";
                    drawBrushRISKZone2.Color = Color.Yellow;
                    drawBrushRISKZone2.Color = Color.DarkBlue;
                }
                else if (ReactionZone2Counter >= 2)
                {
                    ReactionZone2Risk = "HIGH";
                    drawBrushRISKZone2.Color = Color.Red;
                }

                if (ReactionZone3Counter == 1)
                {
                    ReactionZone3Risk = "LIMITED";
                    drawBrushRISKZone3.Color = Color.DarkBlue;
                }
                else if (ReactionZone3Counter >= 2)
                {
                    ReactionZone3Risk = "HIGH";
                    drawBrushRISKZone3.Color = Color.Red;
                }

                if (ReactionZone4Counter == 1)
                {
                    ReactionZone4Risk = "LIMITED";
                    drawBrushRISKZone4.Color = Color.DarkBlue;

                }
                else if (ReactionZone4Counter >= 2)
                {
                    ReactionZone4Risk = "HIGH";
                    drawBrushRISKZone4.Color = Color.Red;
                }


                if (ReactionZone5Counter == 1)
                {
                    ReactionZone5Risk = "LIMITED";
                    drawBrushRISKZone5.Color = Color.Yellow;
                    drawBrushRISKZone5.Color = Color.DarkBlue;
                }
                else if (ReactionZone5Counter >= 2)
                {
                    ReactionZone5Risk = "HIGH";
                    drawBrushRISKZone5.Color = Color.Red;
                }

                if (ReactionZone6Counter == 1)
                {
                    ReactionZone6Risk = "LIMITED";
                    drawBrushRISKZone6.Color = Color.Yellow;
                    drawBrushRISKZone6.Color = Color.DarkBlue;
                }
                else if (ReactionZone6Counter >= 2)
                {
                    ReactionZone6Risk = "HIGH";
                    drawBrushRISKZone6.Color = Color.Red;
                }
                if (ReactionZone7Counter == 1)
                {
                    ReactionZone7Risk = "LIMITED";
                    drawBrushRISKZone7.Color = Color.Yellow;
                    drawBrushRISKZone7.Color = Color.DarkBlue;
                }
                else if (ReactionZone7Counter >= 2)
                {
                    ReactionZone7Risk = "HIGH";
                    drawBrushRISKZone7.Color = Color.Red;
                }





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMIT";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////
                if ((i >= 2) & (i <= 4))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    if (i == 2)
                    {
                        eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 20);
                    }
                }

                if ((i >= 5) & (i <= 7))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    if (i == 5)
                    {
                        eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 20);
                    }
                }

                if ((i >= 8) & (i <= 10))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    if (i == 8)
                    {
                        eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 20);
                    }

                }
                if ((i >= 11) & (i <= 13))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    if (i == 11)
                    {
                        eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 20);
                    }

                }
                if ((i >= 14) & (i <= 16))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                    if (i == 14)
                    {
                        eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 20);
                    }

                }
                if ((i >= 17) & (i <= 19))
                {
                    iStartTerrorism += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                    if (i == 17)
                    {
                        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                    }
                }
                if ((i >= 20) & (i <= 22))
                {
                    iStartUnknown += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                    if (i == 20)
                    {
                        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                    }

                }

                if ((i == 23))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                }
            }

        }






        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  24 Questions Report - Type3 version 1 - ONLY RED AND GREEN COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero or one SPR, the LOW Green
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport24QType3(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen: ";
            string strDriverLicense = null;

            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";


            Veracity.Client.Proxy.Screen screen = null;



            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown));

            // Create the next 30 lines + 7 lines of spaces
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 37; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + iMoveTableDown),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Date
            strDate += DateTime.Now.ToString("yyyMMdd");
            //strDate += globalTestScreen.objTestSubject.DateCreated;
            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            strTime += DateTime.Now.ToString("HH:mm:ss tt");
            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 40 + iMoveTableDown);
            // TouchScreen
            strTouchScreen += "SDSO_001";
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "CA DL#: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 150;
            int iHeader2 = 110;
            eve.Graphics.DrawString("San Diego County Sheriff Department", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Taxi Driver Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 270, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAccidents = 60 + iMoveTableDown;
            int iIllegalDrugs = 140 + iMoveTableDown + iSpacing;
            int iSexOffender = 240 + iMoveTableDown + iSpacing;
            int iArrest = 340 + iMoveTableDown + iSpacing;
            int iSmuggling = 440 + iMoveTableDown + iSpacing;
            int iTerrorism = 540 + iMoveTableDown + iSpacing;
            int iUnknown = 640 + iMoveTableDown + iSpacing;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing;

            // Footer
            int iFooterYaxis = 880 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown;
            int iSignificant = 3 + iMoveTableDown;
            int iSignificant2 = 17 + iMoveTableDown;
            int iSignificant3 = 31 + iMoveTableDown;
            int iSignificant4 = 44 + iMoveTableDown;
            int iQuestionNumber = 3 + iMoveTableDown;
            int iQuestions = 3 + iMoveTableDown;
            int iQuestions2 = 17 + iMoveTableDown;
            int iQuestions3 = 31 + iMoveTableDown;
            int iRisk = 3 + iMoveTableDown;
            int iRisk2 = 17 + iMoveTableDown;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 380, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("One SPR per issue indicates Caution", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 667, eve.MarginBounds.Top + iRisk2);

            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);

            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero or one SPR, the LOW Green
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                switch (i)
                {

                    case 2: // questions 3,4,5
                    case 3: // questions 3,4,5
                    case 4: // questions 3,4,5
                        if (i == 2)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 3)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 4)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 5: // questions 6,7,8
                    case 6: // questions 6,7,8
                    case 7: // questions 6,7,8
                        if (i == 5)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 8: // questions 9,10,11
                    case 9: // questions 9,10,11
                    case 10: // questions 9,10,11
                        if (i == 8)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 9)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 11: // questions 12,13,14
                    case 12: // questions 12,13,14
                    case 13: // questions 12,13,14
                        if (i == 11)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 12)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 13)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 14: // questions 3,4,5
                    case 15: // questions 3,4,5
                    case 16: // questions 3,4,5
                        if (i == 14)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 16)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    case 17: // questions 3,4,5
                    case 18: // questions 3,4,5
                    case 19: // questions 3,4,5
                        if (i == 17)
                        {
                            if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter3++;
                            }
                        }

                        ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                        break;
                    case 20: // questions 3,4,5
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                        if (i == 20)
                        {
                            if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter1++;
                            }
                        }
                        if (i == 21)
                        {
                            if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter2++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter3++;
                            }
                        }

                        ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                        break;
                    default:
                        break;
                }


            }





            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;


                if (ReactionZone1Counter == 1)
                {
                    ReactionZone1Risk = "";
                    drawBrushRISKZone1.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                    }
                }
                else if (ReactionZone1Counter >= 2)
                {
                    ReactionZone1Risk = "HIGH";
                    drawBrushRISKZone1.Color = Color.Red;
                }

                if (ReactionZone2Counter == 1)
                {
                    ReactionZone2Risk = "";
                    drawBrushRISKZone2.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }
                else if (ReactionZone2Counter >= 2)
                {
                    ReactionZone2Risk = "HIGH";
                    drawBrushRISKZone2.Color = Color.Red;
                }

                if (ReactionZone3Counter == 1)
                {
                    ReactionZone3Risk = "";
                    drawBrushRISKZone3.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }
                else if (ReactionZone3Counter >= 2)
                {
                    ReactionZone3Risk = "HIGH";
                    drawBrushRISKZone3.Color = Color.Red;
                }

                if (ReactionZone4Counter == 1)
                {
                    ReactionZone4Risk = "";
                    drawBrushRISKZone4.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }
                else if (ReactionZone4Counter >= 2)
                {
                    ReactionZone4Risk = "HIGH";
                    drawBrushRISKZone4.Color = Color.Red;
                }


                if (ReactionZone5Counter == 1)
                {
                    ReactionZone5Risk = "";
                    drawBrushRISKZone5.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone5Risk = "HIGH";
                        drawBrushRISKZone5.Color = Color.Red;
                    }
                }
                else if (ReactionZone5Counter >= 2)
                {
                    ReactionZone5Risk = "HIGH";
                    drawBrushRISKZone5.Color = Color.Red;
                }

                if (ReactionZone6Counter == 1)
                {
                    ReactionZone6Risk = "";
                    drawBrushRISKZone6.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone6Risk = "HIGH";
                        drawBrushRISKZone6.Color = Color.Red;
                    }
                }
                else if (ReactionZone6Counter >= 2)
                {
                    ReactionZone6Risk = "HIGH";
                    drawBrushRISKZone6.Color = Color.Red;
                }
                if (ReactionZone7Counter == 1)
                {
                    ReactionZone7Risk = "";
                    drawBrushRISKZone7.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone7Risk = "HIGH";
                        drawBrushRISKZone7.Color = Color.Red;
                    }
                }
                else if (ReactionZone7Counter >= 2)
                {
                    ReactionZone7Risk = "HIGH";
                    drawBrushRISKZone7.Color = Color.Red;
                }





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////
                if ((i >= 2) & (i <= 4))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    if (i == 2)
                    {
                        eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 20);
                    }
                }

                if ((i >= 5) & (i <= 7))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    if (i == 5)
                    {
                        eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 20);
                    }
                }

                if ((i >= 8) & (i <= 10))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    if (i == 8)
                    {
                        eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 20);
                    }

                }
                if ((i >= 11) & (i <= 13))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    if (i == 11)
                    {
                        eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 20);
                    }

                }
                if ((i >= 14) & (i <= 16))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                    if (i == 14)
                    {
                        eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 20);
                    }

                }
                if ((i >= 17) & (i <= 19))
                {
                    iStartTerrorism += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                    if (i == 17)
                    {
                        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                    }
                }
                if ((i >= 20) & (i <= 22))
                {
                    iStartUnknown += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                    if (i == 20)
                    {
                        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                    }

                }

                if ((i == 23))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                }
            }

        }






        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  24 Questions Report - Type4 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport24QType4(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen: ";
            string strDriverLicense = null;


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;

            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                }

                switch (i)
                {

                    case 2: // questions 3,4,5
                    case 3: // questions 3,4,5
                    case 4: // questions 3,4,5
                        if (i == 2)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 3)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 4)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 5: // questions 6,7,8
                    case 6: // questions 6,7,8
                    case 7: // questions 6,7,8
                        if (i == 5)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 8: // questions 9,10,11
                    case 9: // questions 9,10,11
                    case 10: // questions 9,10,11
                        if (i == 8)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 9)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 11: // questions 12,13,14
                    case 12: // questions 12,13,14
                    case 13: // questions 12,13,14
                        if (i == 11)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 12)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 13)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 14: // questions 3,4,5
                    case 15: // questions 3,4,5
                    case 16: // questions 3,4,5
                        if (i == 14)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 16)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    case 17: // questions 3,4,5
                    case 18: // questions 3,4,5
                    case 19: // questions 3,4,5
                        if (i == 17)
                        {
                            if ((ReactionZone6Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone6Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone6Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone6Counter3++;
                            }
                        }

                        ReactionZone6Counter = ReactionZone6Counter1 + ReactionZone6Counter2 + ReactionZone6Counter3;
                        break;
                    case 20: // questions 3,4,5
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                        if (i == 20)
                        {
                            if ((ReactionZone7Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter1++;
                            }
                        }
                        if (i == 21)
                        {
                            if ((ReactionZone7Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter2++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone7Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone7Counter3++;
                            }
                        }

                        ReactionZone7Counter = ReactionZone7Counter1 + ReactionZone7Counter2 + ReactionZone7Counter3;
                        break;
                    default:
                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
            }









            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown));

            // Create the next 30 lines + 7 lines of spaces
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 37; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + iMoveTableDown),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Date
            //strDate += DateTime.Now.ToString("yyyMMdd");
            strDate += globalTestScreen.dtBeginQuestionTest.ToString("yyyMMdd");

            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            // strTime += DateTime.Now.ToString("HH:mm:ss tt");
            strTime += globalTestScreen.dtBeginQuestionTest.ToString("HH:mm:ss tt");

            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 40 + iMoveTableDown);
            // TouchScreen
            strTouchScreen += "SDSO_001";
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "CA DL#: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            iHeader1 = 150;
            iHeader2 = 110;
            eve.Graphics.DrawString("San Diego County Sheriff Department", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Taxi Driver Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 270, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAccidents = 60 + iMoveTableDown;
            int iIllegalDrugs = 140 + iMoveTableDown + iSpacing;
            int iSexOffender = 240 + iMoveTableDown + iSpacing;
            int iArrest = 340 + iMoveTableDown + iSpacing;
            int iSmuggling = 440 + iMoveTableDown + iSpacing;
            int iTerrorism = 540 + iMoveTableDown + iSpacing;
            int iUnknown = 640 + iMoveTableDown + iSpacing;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing;

            // Footer
            int iFooterYaxis = 880 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown;
            int iSignificant = 3 + iMoveTableDown;
            int iSignificant2 = 17 + iMoveTableDown;
            int iSignificant3 = 31 + iMoveTableDown;
            int iSignificant4 = 44 + iMoveTableDown;
            int iQuestionNumber = 3 + iMoveTableDown;
            int iQuestions = 3 + iMoveTableDown;
            int iQuestions2 = 17 + iMoveTableDown;
            int iQuestions3 = 31 + iMoveTableDown;
            int iRisk = 3 + iMoveTableDown;
            int iRisk2 = 17 + iMoveTableDown;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 380, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("One SPR per issue indicates Caution", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 667, eve.MarginBounds.Top + iRisk2);

            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);
            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 50 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;


                if (ReactionZone1Counter == 0)
                {
                    ReactionZone1Risk = "";
                    drawBrushRISKZone1.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                    }
                }
                else if (ReactionZone1Counter == 1)
                {
                    ReactionZone1Risk = "LIMITED";
                    drawBrushRISKZone1.Color = Color.Yellow;
                    drawBrushRISKZone1.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                    }
                }
                else if (ReactionZone1Counter >= 2)
                {
                    ReactionZone1Risk = "HIGH";
                    drawBrushRISKZone1.Color = Color.Red;
                }

                if (ReactionZone2Counter == 0)
                {
                    ReactionZone2Risk = "";
                    drawBrushRISKZone2.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }
                else if (ReactionZone2Counter == 1)
                {
                    ReactionZone2Risk = "LIMITED";
                    drawBrushRISKZone2.Color = Color.Yellow;
                    drawBrushRISKZone2.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }
                else if (ReactionZone2Counter >= 2)
                {
                    ReactionZone2Risk = "HIGH";
                    drawBrushRISKZone2.Color = Color.Red;
                }

                if (ReactionZone3Counter == 0)
                {
                    ReactionZone3Risk = "";
                    drawBrushRISKZone3.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }
                else if (ReactionZone3Counter == 1)
                {
                    ReactionZone3Risk = "LIMITED";
                    drawBrushRISKZone3.Color = Color.Yellow;
                    drawBrushRISKZone3.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }
                else if (ReactionZone3Counter >= 2)
                {
                    ReactionZone3Risk = "HIGH";
                    drawBrushRISKZone3.Color = Color.Red;
                }

                if (ReactionZone4Counter == 0)
                {
                    ReactionZone4Risk = "";
                    drawBrushRISKZone4.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }
                else if (ReactionZone4Counter == 1)
                {
                    ReactionZone4Risk = "LIMITED";
                    drawBrushRISKZone4.Color = Color.Yellow;
                    drawBrushRISKZone4.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }
                else if (ReactionZone4Counter >= 2)
                {
                    ReactionZone4Risk = "HIGH";
                    drawBrushRISKZone4.Color = Color.Red;
                }

                if (ReactionZone5Counter == 0)
                {
                    ReactionZone5Risk = "";
                    drawBrushRISKZone5.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone5Risk = "HIGH";
                        drawBrushRISKZone5.Color = Color.Red;
                    }
                }
                else if (ReactionZone5Counter == 1)
                {
                    ReactionZone5Risk = "LIMITED";
                    drawBrushRISKZone5.Color = Color.Yellow;
                    drawBrushRISKZone5.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone5Risk = "HIGH";
                        drawBrushRISKZone5.Color = Color.Red;
                    }
                }
                else if (ReactionZone5Counter >= 2)
                {
                    ReactionZone5Risk = "HIGH";
                    drawBrushRISKZone5.Color = Color.Red;
                }


                if (ReactionZone6Counter == 0)
                {
                    ReactionZone6Risk = "";
                    drawBrushRISKZone6.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone6Risk = "HIGH";
                        drawBrushRISKZone6.Color = Color.Red;
                    }
                }
                else if (ReactionZone6Counter == 1)
                {
                    ReactionZone6Risk = "LIMITED";
                    drawBrushRISKZone6.Color = Color.Yellow;
                    drawBrushRISKZone6.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone6Risk = "HIGH";
                        drawBrushRISKZone6.Color = Color.Red;
                    }
                }
                else if (ReactionZone6Counter >= 2)
                {
                    ReactionZone6Risk = "HIGH";
                    drawBrushRISKZone6.Color = Color.Red;
                }



                if (ReactionZone7Counter == 0)
                {
                    ReactionZone7Risk = "";
                    drawBrushRISKZone7.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone7Risk = "HIGH";
                        drawBrushRISKZone7.Color = Color.Red;
                    }
                }
                else if (ReactionZone7Counter == 1)
                {
                    ReactionZone7Risk = "LIMITED";
                    drawBrushRISKZone7.Color = Color.Yellow;
                    drawBrushRISKZone7.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone7Risk = "HIGH";
                        drawBrushRISKZone7.Color = Color.Red;
                    }
                }
                else if (ReactionZone7Counter >= 2)
                {
                    ReactionZone7Risk = "HIGH";
                    drawBrushRISKZone7.Color = Color.Red;
                }





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////
                if ((i >= 2) & (i <= 4))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    if (i == 2)
                    {
                        eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 20);
                    }
                }

                if ((i >= 5) & (i <= 7))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    if (i == 5)
                    {
                        eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 20);
                    }
                }

                if ((i >= 8) & (i <= 10))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    if (i == 8)
                    {
                        eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 20);
                    }

                }
                if ((i >= 11) & (i <= 13))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    if (i == 11)
                    {
                        eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 20);
                    }

                }
                if ((i >= 14) & (i <= 16))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                    if (i == 14)
                    {
                        eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 20);
                    }

                }
                if ((i >= 17) & (i <= 19))
                {
                    iStartTerrorism += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                    if (i == 17)
                    {
                        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                    }
                }
                if ((i >= 20) & (i <= 22))
                {
                    iStartUnknown += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                    if (i == 20)
                    {
                        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                    }

                }

                if ((i == 23))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                }
            }

        }






        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////  15 Questions Report - Type1 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport15QType1(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen: ";
            string strDriverLicense = null;


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            //int ReactionZone1Counter = 0;
            //int ReactionZone1Counter1 = 0;
            //int ReactionZone1Counter2 = 0;
            //int ReactionZone1Counter3 = 0;
            //string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            //int ReactionZone5Counter = 0;
            //int ReactionZone5Counter1 = 0;
            //int ReactionZone5Counter2 = 0;
            //int ReactionZone5Counter3 = 0;
            //string ReactionZone5Risk = "";

            //int ReactionZone6Counter = 0;
            //int ReactionZone6Counter1 = 0;
            //int ReactionZone6Counter2 = 0;
            //int ReactionZone6Counter3 = 0;
            //string ReactionZone6Risk = "";


            //int ReactionZone7Counter = 0;
            //int ReactionZone7Counter1 = 0;
            //int ReactionZone7Counter2 = 0;
            //int ReactionZone7Counter3 = 0;
            //string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;

            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                // Dont count the question ADAPTS and BASE and EXIT QUESTIONS
                if ((screen.Answer) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                {
                    iRiskAnalysisTotalAdmits++;
                }

                switch (i)
                {


                    case 5: // questions 6,7,8
                    case 6: // questions 6,7,8
                    case 7: // questions 6,7,8
                        if (i == 5)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 8: // questions 9,10,11
                    case 9: // questions 9,10,11
                    case 10: // questions 9,10,11
                        if (i == 8)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 9)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 11: // questions 12,13,14
                    case 12: // questions 12,13,14
                    case 13: // questions 12,13,14
                        if (i == 11)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 12)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 13)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;

                    default:
                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";
            }

            // check for any caution risk
            // 
            if ((ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) && (ReactionZone4Counter < 2))
            {
                if ((ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) || (ReactionZone4Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) || (ReactionZone4Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
            }









            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown));

            // Create the next 30 lines + 7 lines of spaces
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 23; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + iMoveTableDown),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Date
            strDate += DateTime.Now.ToString("yyyMMdd");
            //strDate += globalTestScreen.objTestSubject.DateCreated;
            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            strTime += DateTime.Now.ToString("HH:mm:ss tt");
            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 40 + iMoveTableDown);
            // TouchScreen
            strTouchScreen += "SDSO_001";
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "CA DL#: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            iHeader1 = 150;
            iHeader2 = 110;
            eve.Graphics.DrawString("Denver International Airport", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("WST Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAccidents = 60 + iMoveTableDown;
            int iIllegalDrugs = 140 + iMoveTableDown + iSpacing;
            int iSexOffender = 240 + iMoveTableDown + iSpacing;
            int iArrest = 340 + iMoveTableDown + iSpacing;
            int iSmuggling = 440 + iMoveTableDown + iSpacing;
            int iTerrorism = 540 + iMoveTableDown + iSpacing;
            int iUnknown = 640 + iMoveTableDown + iSpacing;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing;

            // Footer
            int iFooterYaxis = 880 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown;
            int iSignificant = 3 + iMoveTableDown;
            int iSignificant2 = 17 + iMoveTableDown;
            int iSignificant3 = 31 + iMoveTableDown;
            int iSignificant4 = 44 + iMoveTableDown;
            int iQuestionNumber = 3 + iMoveTableDown;
            int iQuestions = 3 + iMoveTableDown;
            int iQuestions2 = 17 + iMoveTableDown;
            int iQuestions3 = 31 + iMoveTableDown;
            int iRisk = 3 + iMoveTableDown;
            int iRisk2 = 17 + iMoveTableDown;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 380, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("One SPR per issue indicates Caution", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 667, eve.MarginBounds.Top + iRisk2);

            eve.Graphics.DrawString("TRAVELING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("WEAPONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            //eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            //eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            //eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);
            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 50 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////


            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;



                if (ReactionZone2Counter == 0)
                {
                    ReactionZone2Risk = "";
                    drawBrushRISKZone2.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }
                else if (ReactionZone2Counter == 1)
                {
                    ReactionZone2Risk = "LIMITED";
                    drawBrushRISKZone2.Color = Color.Yellow;
                    drawBrushRISKZone2.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }
                else if (ReactionZone2Counter >= 2)
                {
                    ReactionZone2Risk = "HIGH";
                    drawBrushRISKZone2.Color = Color.Red;
                }

                if (ReactionZone3Counter == 0)
                {
                    ReactionZone3Risk = "";
                    drawBrushRISKZone3.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }
                else if (ReactionZone3Counter == 1)
                {
                    ReactionZone3Risk = "LIMITED";
                    drawBrushRISKZone3.Color = Color.Yellow;
                    drawBrushRISKZone3.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }
                else if (ReactionZone3Counter >= 2)
                {
                    ReactionZone3Risk = "HIGH";
                    drawBrushRISKZone3.Color = Color.Red;
                }

                if (ReactionZone4Counter == 0)
                {
                    ReactionZone4Risk = "";
                    drawBrushRISKZone4.Color = Color.Green;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }
                else if (ReactionZone4Counter == 1)
                {
                    ReactionZone4Risk = "LIMITED";
                    drawBrushRISKZone4.Color = Color.Yellow;
                    drawBrushRISKZone4.Color = Color.DarkBlue;

                    // If the person ADMITS, then HIGH risk
                    if (screen.Answer)
                    {
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }
                else if (ReactionZone4Counter >= 2)
                {
                    ReactionZone4Risk = "HIGH";
                    drawBrushRISKZone4.Color = Color.Red;
                }







                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }
                // Dont put an ADMIT in the ADAPT, BASE, and EXIT questions
                if ((screen.Answer == true) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if ((screen.IsHit == true) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////
                if ((i >= 2) & (i <= 4))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    //if (i == 2)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 20);
                    //}
                }

                if ((i >= 5) & (i <= 7))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    if (i == 5)
                    {
                        eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 20);
                    }
                }

                if ((i >= 8) & (i <= 10))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    if (i == 8)
                    {
                        eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 20);
                    }

                }
                if ((i >= 11) & (i <= 13))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    if (i == 11)
                    {
                        eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 20);
                    }

                }

                if ((i == 14))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);
                }
                //if ((i >= 14) & (i <= 16))
                //{
                //    iStartSmuggling += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                //    if (i == 14)
                //    {
                //        eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 20);
                //    }

                //}
                //if ((i >= 17) & (i <= 19))
                //{
                //    iStartTerrorism += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                //    if (i == 17)
                //    {
                //        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                //    }
                //}
                //if ((i >= 20) & (i <= 22))
                //{
                //    iStartUnknown += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                //    if (i == 20)
                //    {
                //        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                //    }

                //}

                //if ((i == 23))
                //{
                //    iStartTakenTestBefore += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                //}
            }

        }



        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////  15 Questions Report - Type1 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReportKOTType1(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen: ";
            string strDriverLicense = null;


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            //int ReactionZone1Counter = 0;
            //int ReactionZone1Counter1 = 0;
            //int ReactionZone1Counter2 = 0;
            //int ReactionZone1Counter3 = 0;
            //string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            //int ReactionZone5Counter = 0;
            //int ReactionZone5Counter1 = 0;
            //int ReactionZone5Counter2 = 0;
            //int ReactionZone5Counter3 = 0;
            //string ReactionZone5Risk = "";

            //int ReactionZone6Counter = 0;
            //int ReactionZone6Counter1 = 0;
            //int ReactionZone6Counter2 = 0;
            //int ReactionZone6Counter3 = 0;
            //string ReactionZone6Risk = "";


            //int ReactionZone7Counter = 0;
            //int ReactionZone7Counter1 = 0;
            //int ReactionZone7Counter2 = 0;
            //int ReactionZone7Counter3 = 0;
            //string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;

            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 2; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                // Dont count the question ADAPTS and BASE and EXIT QUESTIONS
                if ((screen.Answer) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                {
                    iRiskAnalysisTotalAdmits++;
                }

                switch (i)
                {


                    case 5: // questions 6,7,8
                    case 6: // questions 6,7,8
                    case 7: // questions 6,7,8
                        if (i == 5)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 8: // questions 9,10,11
                    case 9: // questions 9,10,11
                    case 10: // questions 9,10,11
                        if (i == 8)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 9)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 11: // questions 12,13,14
                    case 12: // questions 12,13,14
                    case 13: // questions 12,13,14
                        if (i == 11)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 12)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 13)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;

                    default:
                        break;
                }
            }



            //// If no SPRs, the no risk
            //if ((ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter) == 0)
            //{
            //    strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            //    strRiskAnalysis = "Low Risk";
            //}

            //// check for any caution risk
            //// 
            //if ((ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) && (ReactionZone4Counter < 2))
            //{
            //    if ((ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) || (ReactionZone4Counter == 1))
            //    {
            //        strRiskAnalysisVeracityRecommend = "Further Investigation Maybe Necessary";
            //        strRiskAnalysis = "Limited Risk";
            //    }

            //}

            //// check for any high risk wih the SPR > 2 counts
            //// 

            //if ((ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) || (ReactionZone4Counter >= 2))
            //{
            //    strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
            //    strRiskAnalysis = "High Risk";
            //}

            //// if any admits, then high risk
            //if (iRiskAnalysisTotalAdmits > 0)
            //{
            //    strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
            //    strRiskAnalysis = "High Risk";
            //}









            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown));

            // Create the next 30 lines + 7 lines of spaces
            int lines = 1;
            int margin = 80;
            for (lines = 1, margin = 80; lines < 12; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 120, eve.MarginBounds.Top + iMoveTableDown),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 120, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Date
            strDate += DateTime.Now.ToString("yyyMMdd");
            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);

            // Time
            strTime += DateTime.Now.ToString("HH:mm:ss tt");
            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 40 + iMoveTableDown);

            // TouchScreen
            strTouchScreen += "Veracity-00125";
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 20 + iMoveTableDown);

            // Blank
            strDriverLicense = " ";
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            //int iHeader1 = 200;
            //int iHeader2 = 180;
            //eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            //eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            int iHeader1 = 150;
            int iHeader2 = 110;
            eve.Graphics.DrawString("Known Outcome Test", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Evaluation", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAccidents = 60 + iMoveTableDown;
            int iIllegalDrugs = 140 + iMoveTableDown + iSpacing;
            int iSexOffender = 240 + iMoveTableDown + iSpacing;
            int iArrest = 340 + iMoveTableDown + iSpacing;
            int iSmuggling = 440 + iMoveTableDown + iSpacing;
            int iTerrorism = 540 + iMoveTableDown + iSpacing;
            int iUnknown = 640 + iMoveTableDown + iSpacing;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing;

            // Footer
            int iFooterYaxis = 880 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown;
            int iSignificant = 3 + iMoveTableDown;
            int iSignificant2 = 17 + iMoveTableDown;
            int iSignificant3 = 31 + iMoveTableDown;
            int iSignificant4 = 44 + iMoveTableDown;
            int iQuestionNumber = 3 + iMoveTableDown;
            int iQuestions = 3 + iMoveTableDown;
            int iQuestions2 = 17 + iMoveTableDown;
            int iQuestions3 = 31 + iMoveTableDown;
            int iRisk = 3 + iMoveTableDown;
            int iRisk2 = 17 + iMoveTableDown;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Answered", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Correctly or", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("Incorrectly", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            //eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 70, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 380, eve.MarginBounds.Top + iQuestions);
            // eve.Graphics.DrawString("Deception is indicated by a DI", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions2);
            // eve.Graphics.DrawString("No Deception is indicated by a NDI", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 300, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Touch", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 650, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 647, eve.MarginBounds.Top + iRisk2);

            //eve.Graphics.DrawString("First Number 4", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            //eve.Graphics.DrawString("Second Number 4", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            //eve.Graphics.DrawString("Third Number 4", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            //eve.Graphics.DrawString("Fourth Number 4", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);

            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 50 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            //SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////


            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                strQuestion = screen.ScreenTextShort;



                //if (ReactionZone2Counter == 0)
                //{
                //    ReactionZone2Risk = "";
                //    drawBrushRISKZone2.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone2Risk = "HIGH";
                //        drawBrushRISKZone2.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone2Counter == 1)
                //{
                //    ReactionZone2Risk = "Caution";
                //    drawBrushRISKZone2.Color = Color.Yellow;
                //    drawBrushRISKZone2.Color = Color.Black;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone2Risk = "HIGH";
                //        drawBrushRISKZone2.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone2Counter >= 2)
                //{
                //    ReactionZone2Risk = "HIGH";
                //    drawBrushRISKZone2.Color = Color.Red;
                //}

                //if (ReactionZone3Counter == 0)
                //{
                //    ReactionZone3Risk = "";
                //    drawBrushRISKZone3.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone3Risk = "HIGH";
                //        drawBrushRISKZone3.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone3Counter == 1)
                //{
                //    ReactionZone3Risk = "Caution";
                //    drawBrushRISKZone3.Color = Color.Yellow;
                //    drawBrushRISKZone3.Color = Color.Black;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone3Risk = "HIGH";
                //        drawBrushRISKZone3.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone3Counter >= 2)
                //{
                //    ReactionZone3Risk = "HIGH";
                //    drawBrushRISKZone3.Color = Color.Red;
                //}

                //if (ReactionZone4Counter == 0)
                //{
                //    ReactionZone4Risk = "";
                //    drawBrushRISKZone4.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone4Risk = "HIGH";
                //        drawBrushRISKZone4.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone4Counter == 1)
                //{
                //    ReactionZone4Risk = "Caution";
                //    drawBrushRISKZone4.Color = Color.Yellow;
                //    drawBrushRISKZone4.Color = Color.Black;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone4Risk = "HIGH";
                //        drawBrushRISKZone4.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone4Counter >= 2)
                //{
                //    ReactionZone4Risk = "HIGH";
                //    drawBrushRISKZone4.Color = Color.Red;
                //}







                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "NO";
                    drawBrush.Color = Color.Black;
                }
                strHit = "";
                drawBrushSPR.Color = Color.Black;
                //// Dont put an ADMIT in the ADAPT, BASE, and EXIT questions
                //if ((screen.Answer == true) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                //{
                //    strHit = "ADMITS";
                //    drawBrushSPR.Color = Color.Red;
                //}
                //else if ((screen.IsHit == true) && ((i != 2) && (i != 3) && (i != 4) && (i != 14)))
                //{
                //    strHit = "SPR";
                //    drawBrushSPR.Color = Color.Red;
                //}


                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////


                if ((i == 3))
                {
                    iStartAccidents += 20;
                    strRiskAnalysis = globalAnalyzerKOT.sReaction[0];
                    strHit = "Correctly";
                    if (globalAnalyzerKOT.iHiddenL[3] > 0)
                    {
                        strHit = "Incorrectly";
                        strRiskAnalysis = "N/A";
                    }
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    // Display the Touch Analysis
                    eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 11, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 620, eve.MarginBounds.Top + iStartAccidents);
                }

                if ((i == 6))
                {
                    iStartAccidents += 40;
                    strRiskAnalysis = globalAnalyzerKOT.sReaction[1];
                    strHit = "Correctly";
                    if (globalAnalyzerKOT.iHiddenL[6] > 0)
                    {
                        strHit = "Incorrectly";
                        strRiskAnalysis = "N/A";
                    }
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    // Display the Touch Analysis
                    eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 11, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 620, eve.MarginBounds.Top + iStartAccidents);
                }

                if ((i == 9))
                {
                    iStartAccidents += 40;
                    strRiskAnalysis = globalAnalyzerKOT.sReaction[2];
                    strHit = "Correctly";
                    if (globalAnalyzerKOT.iHiddenL[9] > 0)
                    {
                        strHit = "Incorrectly";
                        strRiskAnalysis = "N/A";
                    }
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    // Display the Touch Analysis
                    eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 11, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 620, eve.MarginBounds.Top + iStartAccidents);
                }

                if ((i == 12))
                {
                    iStartAccidents += 40;
                    strRiskAnalysis = globalAnalyzerKOT.sReaction[3];
                    strHit = "Correctly";
                    if (globalAnalyzerKOT.iHiddenL[12] > 0)
                    {
                        strHit = "Incorrectly";
                        strRiskAnalysis = "N/A";
                    }
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    // Display the Touch Analysis
                    eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 11, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 620, eve.MarginBounds.Top + iStartAccidents);

                    eve.Graphics.DrawString("Note 1", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents + 20);
                }

                if ((i == 15))
                {
                    iStartAccidents += 40;

                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    // Display the Touch Analysis
                    eve.Graphics.DrawString(globalAnalyzerKOT.sReaction[4], new Font("Arial", 11, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 620, eve.MarginBounds.Top + iStartAccidents);

                    eve.Graphics.DrawString("Note 2", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents + 20);
                }



                //if ((i >= 2) & (i <= 4))
                //{
                //    iStartAccidents += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                //}

                //if ((i >= 5) & (i <= 7))
                //{
                //    iStartIllegal += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                //    if (i == 5)
                //    {
                //        eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 20);
                //    }
                //}

                //if ((i >= 8) & (i <= 10))
                //{
                //    iStartSex += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                //    if (i == 8)
                //    {
                //        eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 20);
                //    }

                //}
                //if ((i >= 11) & (i <= 13))
                //{
                //    iStartArrest += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                //    if (i == 11)
                //    {
                //        eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 20);
                //    }

                //}

                //if ((i == 14))
                //{
                //    iStartSmuggling += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);
                //}



            }

            // NOTE 1
            iStartAccidents += 100;

            eve.Graphics.DrawString("Note 1", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
            iStartAccidents += 20;
            strQuestion = "Because of the benign subject matter (no jeopardy or perceived threat), the Veracity TouchScreener™ must detect significant";
            eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
            strQuestion = "psycho-physiological reaction patterns in 2 out of 4 times concerning the number 4 before determining whether or not it";
            iStartAccidents += 20;
            eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
            strQuestion = "caught you practicing deception.";
            iStartAccidents += 20;
            eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);

            // NOTE 2
            iStartAccidents += 50;
            eve.Graphics.DrawString("Note 2", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
            iStartAccidents += 20;
            strQuestion = "The last question, 16, ‘Did you ever lie to escape deserved punishment’, was used in an attempt to induce you ";
            eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
            strQuestion = "to have an emotional reaction, whether or not you answered truthfully.";
            iStartAccidents += 20;
            eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);

        }






        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  26 Questions Report - Type4 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport26QType1(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen #: ";
            string strDriverLicense = null;
            string GuidSessionID = "12345";
            string strDepartmentPrefix = "SDSO";
            string strTestScreenID = "12345";
            string strDistortQuestion = "";

            // setup the SessionID
            if (String.IsNullOrEmpty(globalTestScreen.TestSessionID))
            {
                // use the default value
            }
            else
            {
                GuidSessionID = globalTestScreen.TestSessionID;
            }

            // setup the client ID
            if ((globalTestScreen.objClient != null) && (globalTestScreen.objClient.ClientID > 0))
            {
                strTestScreenID = Convert.ToString(globalTestScreen.objClient.ClientID);
            }


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;


            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                        case 25:
                            iRiskAnalysisTotalAdmits--;
                            break;
                        default:
                            break;
                    }
                }

                switch (i)
                {

                    case 5: // questions 3,4,5
                    case 6: // questions 3,4,5
                    case 7: // questions 3,4,5
                        if (i == 5)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 9: // questions 6,7,8
                    case 10: // questions 6,7,8
                    case 11: // questions 6,7,8
                        if (i == 9)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 11)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 13: // questions 9,10,11
                    case 14: // questions 9,10,11
                    case 15: // questions 9,10,11
                        if (i == 13)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 14)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 17: // questions 12,13,14
                    case 18: // questions 12,13,14
                    case 19: // questions 12,13,14
                        if (i == 17)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                    case 23: // questions 3,4,5
                        if (i == 21)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 23)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    default:

                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
            }



            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal
            int iAdditionalMoveTableONLY = -40; // this will only move the table



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Create the next 38 lines + 7 lines of spaces
            int lines = 1;
            int TotalLines = 38;
            int margin = 80;
            for (lines = 1, margin = 80; lines < TotalLines; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 180, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 75, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            //strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            //eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Date
            //strDate += DateTime.Now.ToString("yyyMMdd");
            strDate += globalTestScreen.dtBeginQuestionTest.ToString("yyyMMdd");

            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            // strTime += DateTime.Now.ToString("HH:mm:ss tt");
            strTime += globalTestScreen.dtBeginQuestionTest.ToString("HH:mm:ss tt");

            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 60 + iMoveTableDown);

            // TouchScreen
            strTouchScreen += strDepartmentPrefix + "_" + strTestScreenID;
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "Taxi License #: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            iHeader1 = 150;
            iHeader2 = 110;
            eve.Graphics.DrawString("San Diego County Sheriff Department", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Taxi Driver Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 270, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAdaptation = 40 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iAccidents = 160 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iIllegalDrugs = 260 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSexOffender = 380 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iArrest = 500 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSmuggling = 620 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTerrorism = 740 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iUnknown = 840 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;

            // Footer
            int iFooterYaxis = 840 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant3 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant4 = 44 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestionNumber = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions1 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions2 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions3 = 45 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iRisk = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iRisk2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;

            // reaction
            bool ReactionZone1RiskAlreadyHIGH = false;
            bool ReactionZone2RiskAlreadyHIGH = false;
            bool ReactionZone3RiskAlreadyHIGH = false;
            bool ReactionZone4RiskAlreadyHIGH = false;
            bool ReactionZone5RiskAlreadyHIGH = false;
            bool ReactionZone6RiskAlreadyHIGH = false;
            bool ReactionZone7RiskAlreadyHIGH = false;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction(SPR)", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 58, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Issues/Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 370, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("No SPR per issue indicates Low Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions1);
            eve.Graphics.DrawString("One SPR per issue indicates Limited Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 305, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 310, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Analysis", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 667, eve.MarginBounds.Top + iRisk2);

            //eve.Graphics.DrawString("ADOPT", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAdaptation + 2);
            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            //eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            //eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);
            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 30 + iFooterYaxis);
            eve.Graphics.DrawString(GuidSessionID, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 225, eve.MarginBounds.Top - 10 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAdoptation = iAdaptation + 2;
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                if (screen.ScreenTextShort != null)
                {
                    strQuestion = screen.ScreenTextShort;
                }
                else
                {
                    strQuestion = screen.ScreenText;
                }

                // only setup the zones for a particular questions
                //
                if ((i == 5) || (i == 6) || (i == 7))
                {

                    if (ReactionZone1Counter == 0)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LOW";
                            drawBrushRISKZone1.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter == 1)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LIMITED";
                            drawBrushRISKZone1.Color = Color.Yellow;
                            drawBrushRISKZone1.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter >= 2)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                        ReactionZone1RiskAlreadyHIGH = true;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 9) || (i == 10) || (i == 11))
                {

                    if (ReactionZone2Counter == 0)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LOW";
                            drawBrushRISKZone2.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                            ReactionZone2RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone2Counter == 1)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LIMITED";
                            drawBrushRISKZone2.Color = Color.Yellow;
                            drawBrushRISKZone2.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2RiskAlreadyHIGH = true;
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone2Counter >= 2)
                    {
                        ReactionZone2RiskAlreadyHIGH = true;
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 13) || (i == 14) || (i == 15))
                {

                    if (ReactionZone3Counter == 0)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LOW";
                            drawBrushRISKZone3.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                            ReactionZone3RiskAlreadyHIGH = true;
                        }

                    }
                    else if (ReactionZone3Counter == 1)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LIMITED";
                            drawBrushRISKZone3.Color = Color.Yellow;
                            drawBrushRISKZone3.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3RiskAlreadyHIGH = true;
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone3Counter >= 2)
                    {
                        ReactionZone3RiskAlreadyHIGH = true;
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 17) || (i == 18) || (i == 19))
                {

                    if (ReactionZone4Counter == 0)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = "LOW";
                            drawBrushRISKZone4.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = "HIGH";
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter == 1)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = "LIMITED";
                            //ReactionZone4Risk = screen.Answer.ToString() + " " + i.ToString();
                            drawBrushRISKZone4.Color = Color.Yellow;
                            drawBrushRISKZone4.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = "HIGH";
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter >= 2)
                    {
                        ReactionZone4RiskAlreadyHIGH = true;
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }


                // only setup the zones for a particular questions
                //
                if ((i == 21) || (i == 22) || (i == 23))
                {

                    if (ReactionZone5Counter == 0)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = "LOW";
                            drawBrushRISKZone5.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = "HIGH";
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter == 1)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = "LIMITED";
                            drawBrushRISKZone5.Color = Color.Yellow;
                            drawBrushRISKZone5.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = "HIGH";
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter >= 2)
                    {
                        ReactionZone5RiskAlreadyHIGH = true;
                        ReactionZone5Risk = "HIGH";
                        drawBrushRISKZone5.Color = Color.Red;
                    }

                }





                //if (ReactionZone6Counter == 0)
                //{
                //    ReactionZone6Risk = "LOW";
                //    drawBrushRISKZone6.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone6Risk = "HIGH";
                //        drawBrushRISKZone6.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone6Counter == 1)
                //{
                //    ReactionZone6Risk = "LIMITED";
                //    drawBrushRISKZone6.Color = Color.Yellow;
                //    drawBrushRISKZone6.Color = Color.DarkBlue;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone6Risk = "HIGH";
                //        drawBrushRISKZone6.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone6Counter >= 2)
                //{
                //    ReactionZone6Risk = "HIGH";
                //    drawBrushRISKZone6.Color = Color.Red;
                //}



                //if (ReactionZone7Counter == 0)
                //{
                //    ReactionZone7Risk = "LOW";
                //    drawBrushRISKZone7.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone7Risk = "HIGH";
                //        drawBrushRISKZone7.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone7Counter == 1)
                //{
                //    ReactionZone7Risk = "LIMITED";
                //    drawBrushRISKZone7.Color = Color.Yellow;
                //    drawBrushRISKZone7.Color = Color.DarkBlue;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone7Risk = "HIGH";
                //        drawBrushRISKZone7.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone7Counter >= 2)
                //{
                //    ReactionZone7Risk = "HIGH";
                //    drawBrushRISKZone7.Color = Color.Red;
                //}





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "No";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }



                // thses are the questions that are not suppose to be ADMITS
                switch (i)
                {

                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "No";
                            drawBrush.Color = Color.Black;
                        }
                        strHit = "";
                        drawBrushSPR.Color = Color.Black;
                        break;
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                    case 20:
                    case 24:
                        //case 25:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Red;
                            strHit = "Incorrect";
                            drawBrushSPR.Color = Color.Red;
                        }

                        if (screen.Answer)
                        {
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }

                        //ReactionZone1Risk = "";
                        //drawBrushRISKZone1.Color = Color.Black;

                        //ReactionZone2Risk = "";
                        //drawBrushRISKZone2.Color = Color.Black;

                        //ReactionZone3Risk = "";
                        //drawBrushRISKZone3.Color = Color.Black;

                        //ReactionZone4Risk = "";
                        //drawBrushRISKZone4.Color = Color.Black;

                        //ReactionZone5Risk = "";
                        //drawBrushRISKZone5.Color = Color.Black;

                        //ReactionZone6Risk = "";
                        //drawBrushRISKZone6.Color = Color.Black;

                        //ReactionZone7Risk = "";
                        //drawBrushRISKZone7.Color = Color.Black;

                        break;
                    case 25: // did you take this test before should not be RED
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }
                        break;
                    default:
                        break;
                }

                // Determine if this question was DISTORED in the ANALYZER data
                if (globalAnalyzer26Q.QuestionThatWasDistortDuringMultipleCodeLogic26[i] > 0)
                {
                    strDistortQuestion = "#";
                    strHit += strDistortQuestion;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////

                if ((i >= 0) & (i <= 3))
                {
                    iStartAdoptation += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAdoptation);

                    //if (i == 1)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 4) & (i <= 7))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    //if (i == 5)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 8) & (i <= 11))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    //if (i == 9)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 40);
                    //}
                }

                if ((i >= 12) & (i <= 15))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    //if (i == 13)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 40);
                    //}

                }
                if ((i >= 16) & (i <= 19))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    //if ((i == 17))
                    //{
                    //    eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 40);
                    //}

                }
                if ((i >= 20) & (i <= 23))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                    //if (i == 21)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 40);
                    //}

                }
                //if ((i >= 17) & (i <= 19))
                //{
                //    iStartTerrorism += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                //    if (i == 17)
                //    {
                //        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                //    }
                //}
                //if ((i >= 20) & (i <= 22))
                //{
                //    iStartUnknown += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                //    if (i == 20)
                //    {
                //        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                //    }

                //}

                if ((i == 24))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                }
                if ((i == 25))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);
                }
            }

            // Now lets print out the Reaction Data for each ZONES
            eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 80);
            eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 80);
            eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 80);
            eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 80);
            eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 80);


        }


        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  26 Questions Report - Type4 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport26QType2(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen #: ";
            string strDriverLicense = null;
            string GuidSessionID = "12345";
            string strDepartmentPrefix = "SDSO";
            string strTestScreenID = "12345";
            string strDistortQuestion = "";

            // Get the total time
            TimeSpan DifferenceTime = globalTestScreen.dtEndQuestionTest - globalTestScreen.dtEndQuestionTest;

            // setup the SessionID
            if (String.IsNullOrEmpty(globalTestScreen.TestSessionID))
            {
                // use the default value
            }
            else
            {
                GuidSessionID = globalTestScreen.TestSessionID;
            }

            // setup the client ID
            if ((globalTestScreen.objClient != null) && (globalTestScreen.objClient.ClientID > 0))
            {
                strTestScreenID = Convert.ToString(globalTestScreen.objClient.ClientID);
            }


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;
            Anaylyzer26Questions analyzer26Q = null;
            analyzer26Q = globalAnalyzer26Q;


            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                        case 25:
                            iRiskAnalysisTotalAdmits--;
                            break;
                        default:
                            break;
                    }
                }

                switch (i)
                {

                    case 5: // questions 3,4,5
                    case 6: // questions 3,4,5
                    case 7: // questions 3,4,5
                        if (i == 5)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 9: // questions 6,7,8
                    case 10: // questions 6,7,8
                    case 11: // questions 6,7,8
                        if (i == 9)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 11)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 13: // questions 9,10,11
                    case 14: // questions 9,10,11
                    case 15: // questions 9,10,11
                        if (i == 13)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 14)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 17: // questions 12,13,14
                    case 18: // questions 12,13,14
                    case 19: // questions 12,13,14
                        if (i == 17)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                    case 23: // questions 3,4,5
                        if (i == 21)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 23)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    default:

                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
            }



            //Horzontal lines
            int iMoveTableDown = 50; // if we want to move the whole table horizontal
            int iAdditionalMoveTableONLY = -40; // this will only move the table



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Create the next 38 lines + 7 lines of spaces
            int lines = 1;
            int TotalLines = 44;
            int margin = 80;
            for (lines = 1, margin = 80; lines < TotalLines; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 55, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 150, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Q column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 175, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 175, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // SUM column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 100, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 100, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // READ TIME column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 50, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 50, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            //strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            //eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Date
            //strDate += DateTime.Now.ToString("yyyMMdd");
            strDate += globalTestScreen.dtBeginQuestionTest.ToString("yyyMMdd");

            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            // strTime += DateTime.Now.ToString("HH:mm:ss tt");
            strTime += globalTestScreen.dtBeginQuestionTest.ToString("HH:mm:ss tt");

            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 60 + iMoveTableDown);

            // TouchScreen
            strTouchScreen += strDepartmentPrefix + "_" + strTestScreenID;
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "Taxi License #: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            // XXX [removed per Phil] eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            // XXX [removed per Phil] eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            iHeader1 = 150;
            iHeader2 = 110;
            // XXX [removed per Phil] eve.Graphics.DrawString("San Diego County Sheriff Department", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString("Taxi Driver Summary Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAdaptation = 40 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iAccidents = 140 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iIllegalDrugs = 220 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSexOffender = 320 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iArrest = 420 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSmuggling = 520 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTerrorism = 620 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iUnknown = 700 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTakenTestBefore = 600 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iLocationAverages = 660 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iLocationTSL = 700 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;

            // Footer
            int iFooterYaxis = 910 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant3 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant4 = 44 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestionNumber = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions1 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions2 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions3 = 45 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSum = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSum2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iReactionTime = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iReactionTime2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;

            // reaction
            bool ReactionZone1RiskAlreadyHIGH = false;
            bool ReactionZone2RiskAlreadyHIGH = false;
            bool ReactionZone3RiskAlreadyHIGH = false;
            bool ReactionZone4RiskAlreadyHIGH = false;
            bool ReactionZone5RiskAlreadyHIGH = false;
            bool ReactionZone6RiskAlreadyHIGH = false;
            bool ReactionZone7RiskAlreadyHIGH = false;


            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 65, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Psycho-", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 72, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("physiological", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 62, eve.MarginBounds.Top + iSignificant3);
            eve.Graphics.DrawString("Reaction(SPR)", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 58, eve.MarginBounds.Top + iSignificant4);

            eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 154, eve.MarginBounds.Top + iQuestionNumber);

            eve.Graphics.DrawString("Issues/Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 370, eve.MarginBounds.Top + iQuestions);
            eve.Graphics.DrawString("No SPR per issue indicates Low Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions1);
            eve.Graphics.DrawString("One SPR per issue indicates Limited Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 305, eve.MarginBounds.Top + iQuestions2);
            eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 310, eve.MarginBounds.Top + iQuestions3);

            // SUM
            eve.Graphics.DrawString("SUM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 638, eve.MarginBounds.Top + iSum);
            eve.Graphics.DrawString("Data", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 638, eve.MarginBounds.Top + iSum2);

            // read Time
            eve.Graphics.DrawString("Read", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 688, eve.MarginBounds.Top + iReactionTime);
            eve.Graphics.DrawString("Time", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 688, eve.MarginBounds.Top + iReactionTime2);

            //eve.Graphics.DrawString("ADOPT", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAdaptation + 2);
            eve.Graphics.DrawString("ACCIDENTS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString("ILLEGAL DRUGS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString("SEX OFFENDER", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString("ARREST & CONVICTIONS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iArrest + 2);
            eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iSmuggling + 2);
            //eve.Graphics.DrawString("TERRORISM", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iTerrorism + 2);
            //eve.Graphics.DrawString("Unknown Issues in Background", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iUnknown + 2);

            // XXX [removed per Phil] eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);
            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            // XXX [removed per Phil] eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 30 + iFooterYaxis);
            eve.Graphics.DrawString(GuidSessionID, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 225, eve.MarginBounds.Top - 10 + iFooterYaxis);

            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAdoptation = iAdaptation + 2;
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;
            int iStartAverages = iLocationAverages + 2;
            int iStartTSL = iLocationTSL + 2;

            int iReadTimeColumnLeftColumn = 690;
            int iSumDataColumnLeftMargin = 635;
            int iStartAvgLeftMargin = 400;
            int iStartAvgLeftMarginRelative = iReadTimeColumnLeftColumn; // 635; // used for the RELATIVE answers
            int iStartAvgLeftMarginBase = iSumDataColumnLeftMargin; // 580; // used for the BASE answers
            int leftColAnswer = 8; // starting from the left side of the page
            //int iLastColumnLeftMargin = 690;
            //int iReadTimeColumnLeftColumn = 690;
            //int iSumDataColumnLeftMargin = iStartAvgLeftMarginRelative ;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);

            // averages
            SolidBrush drawBrushAverage = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                if (screen.ScreenTextShort != null)
                {
                    strQuestion = screen.ScreenTextShort;
                }
                else
                {
                    strQuestion = screen.ScreenText;
                }

                // only setup the zones for a particular questions
                //
                if ((i == 5) || (i == 6) || (i == 7))
                {

                    if (ReactionZone1Counter == 0)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LOW";
                            drawBrushRISKZone1.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter == 1)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LIMITED";
                            drawBrushRISKZone1.Color = Color.Yellow;
                            drawBrushRISKZone1.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter >= 2)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                        ReactionZone1RiskAlreadyHIGH = true;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 9) || (i == 10) || (i == 11))
                {

                    if (ReactionZone2Counter == 0)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LOW";
                            drawBrushRISKZone2.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                            ReactionZone2RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone2Counter == 1)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LIMITED";
                            drawBrushRISKZone2.Color = Color.Yellow;
                            drawBrushRISKZone2.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2RiskAlreadyHIGH = true;
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone2Counter >= 2)
                    {
                        ReactionZone2RiskAlreadyHIGH = true;
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 13) || (i == 14) || (i == 15))
                {

                    if (ReactionZone3Counter == 0)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LOW";
                            drawBrushRISKZone3.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                            ReactionZone3RiskAlreadyHIGH = true;
                        }

                    }
                    else if (ReactionZone3Counter == 1)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LIMITED";
                            drawBrushRISKZone3.Color = Color.Yellow;
                            drawBrushRISKZone3.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3RiskAlreadyHIGH = true;
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone3Counter >= 2)
                    {
                        ReactionZone3RiskAlreadyHIGH = true;
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 17) || (i == 18) || (i == 19))
                {

                    if (ReactionZone4Counter == 0)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = "LOW";
                            drawBrushRISKZone4.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = "HIGH";
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter == 1)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = "LIMITED";
                            //ReactionZone4Risk = screen.Answer.ToString() + " " + i.ToString();
                            drawBrushRISKZone4.Color = Color.Yellow;
                            drawBrushRISKZone4.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = "HIGH";
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter >= 2)
                    {
                        ReactionZone4RiskAlreadyHIGH = true;
                        ReactionZone4Risk = "HIGH";
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }


                // only setup the zones for a particular questions
                //
                if ((i == 21) || (i == 22) || (i == 23))
                {

                    if (ReactionZone5Counter == 0)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = "LOW";
                            drawBrushRISKZone5.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = "HIGH";
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter == 1)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = "LIMITED";
                            drawBrushRISKZone5.Color = Color.Yellow;
                            drawBrushRISKZone5.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = "HIGH";
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter >= 2)
                    {
                        ReactionZone5RiskAlreadyHIGH = true;
                        ReactionZone5Risk = "HIGH";
                        drawBrushRISKZone5.Color = Color.Red;
                    }

                }





                //if (ReactionZone6Counter == 0)
                //{
                //    ReactionZone6Risk = "LOW";
                //    drawBrushRISKZone6.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone6Risk = "HIGH";
                //        drawBrushRISKZone6.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone6Counter == 1)
                //{
                //    ReactionZone6Risk = "LIMITED";
                //    drawBrushRISKZone6.Color = Color.Yellow;
                //    drawBrushRISKZone6.Color = Color.DarkBlue;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone6Risk = "HIGH";
                //        drawBrushRISKZone6.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone6Counter >= 2)
                //{
                //    ReactionZone6Risk = "HIGH";
                //    drawBrushRISKZone6.Color = Color.Red;
                //}



                //if (ReactionZone7Counter == 0)
                //{
                //    ReactionZone7Risk = "LOW";
                //    drawBrushRISKZone7.Color = Color.Green;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone7Risk = "HIGH";
                //        drawBrushRISKZone7.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone7Counter == 1)
                //{
                //    ReactionZone7Risk = "LIMITED";
                //    drawBrushRISKZone7.Color = Color.Yellow;
                //    drawBrushRISKZone7.Color = Color.DarkBlue;

                //    // If the person ADMITS, then HIGH risk
                //    if (screen.Answer)
                //    {
                //        ReactionZone7Risk = "HIGH";
                //        drawBrushRISKZone7.Color = Color.Red;
                //    }
                //}
                //else if (ReactionZone7Counter >= 2)
                //{
                //    ReactionZone7Risk = "HIGH";
                //    drawBrushRISKZone7.Color = Color.Red;
                //}





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "No";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }



                // thses are the questions that are not suppose to be ADMITS
                switch (i)
                {

                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "No";
                            drawBrush.Color = Color.Black;
                        }
                        strHit = "";
                        drawBrushSPR.Color = Color.Black;
                        break;
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                    case 20:
                    case 24:
                        //case 25:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Red;
                            strHit = "Incorrect";
                            drawBrushSPR.Color = Color.Red;
                        }

                        if (screen.Answer)
                        {
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }

                        //ReactionZone1Risk = "";
                        //drawBrushRISKZone1.Color = Color.Black;

                        //ReactionZone2Risk = "";
                        //drawBrushRISKZone2.Color = Color.Black;

                        //ReactionZone3Risk = "";
                        //drawBrushRISKZone3.Color = Color.Black;

                        //ReactionZone4Risk = "";
                        //drawBrushRISKZone4.Color = Color.Black;

                        //ReactionZone5Risk = "";
                        //drawBrushRISKZone5.Color = Color.Black;

                        //ReactionZone6Risk = "";
                        //drawBrushRISKZone6.Color = Color.Black;

                        //ReactionZone7Risk = "";
                        //drawBrushRISKZone7.Color = Color.Black;

                        break;
                    case 25: // did you take this test before should not be RED
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }
                        break;
                    default:
                        break;
                }

                // Determine if this question was DISTORED in the ANALYZER data
                if (globalAnalyzer26Q.QuestionThatWasDistortDuringMultipleCodeLogic26[i] > 0)
                {
                    strDistortQuestion = "#";
                    strHit += strDistortQuestion;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////

                if ((i >= 0) & (i <= 3))
                {
                    iStartAdoptation += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAdoptation);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartAdoptation);

                    //if (i == 1)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 4) & (i <= 7))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartAccidents);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartAccidents);

                    //if (i == 5)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 8) & (i <= 11))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartIllegal);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartIllegal);

                    //if (i == 9)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 40);
                    //}
                }

                if ((i >= 12) & (i <= 15))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSex);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartSex);

                    //if (i == 13)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 40);
                    //}

                }
                if ((i >= 16) & (i <= 19))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartArrest);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartArrest);

                    //if ((i == 17))
                    //{
                    //    eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 40);
                    //}

                }
                if ((i >= 20) & (i <= 23))
                {
                    iStartSmuggling += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartSmuggling);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartSmuggling);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartSmuggling);

                    //if (i == 21)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 40);
                    //}

                }
                //if ((i >= 17) & (i <= 19))
                //{
                //    iStartTerrorism += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTerrorism);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTerrorism);


                //    if (i == 17)
                //    {
                //        eve.Graphics.DrawString(ReactionZone6Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone6, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartTerrorism - 20);
                //    }
                //}
                //if ((i >= 20) & (i <= 22))
                //{
                //    iStartUnknown += 20;
                //    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + 6, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartUnknown);
                //    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartUnknown);

                //    if (i == 20)
                //    {
                //        eve.Graphics.DrawString(ReactionZone7Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone7, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartUnknown - 20);
                //    }

                //}

                if ((i == 24))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartTakenTestBefore);

                }
                if ((i == 25))
                {
                    iStartTakenTestBefore += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + leftColAnswer, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + 80, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 155, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTakenTestBefore);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer26Q.dSumEI[i], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 635, eve.MarginBounds.Top + iStartTakenTestBefore);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartTakenTestBefore);

                }
            }

            // Now lets print out the Reaction Data for each ZONES
            //eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 80);
            //eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 80);
            //eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 80);
            //eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 80);
            //eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 80);

            // Now lets print out the averages
            drawBrushAverage.Color = Color.Black;

            eve.Graphics.DrawString("Average Test Q:5-25", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[0], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Average Q:5,9,13,17,21,25", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[1], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Base/Relative Avg Q: 6/8", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[2], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[7], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Base/Relative Avg Q: 10/12", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[3], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[8], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Base/Relative Avg Q: 14/16", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[4], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[9], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Base/Relative Avg Q: 18/20", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[5], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[10], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            iStartAverages += 20;
            eve.Graphics.DrawString("Base/Relative Avg Q: 22/25", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[6], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[11], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);

            // Calculate the total read time
            // TimeSpan t = TimeSpan.FromSeconds(Math.Round(analyzer26Q.dReadTimeTotal26, 2));
            //string answer = string.Format("{0:D2}:{1:D2}",t.Minutes,t.Seconds); // convert to have the minutes and seconds
            string answer = string.Format("{0:D2}:{1:D2}", DifferenceTime.Minutes, DifferenceTime.Seconds); // convert to have minutes and seconds
            iStartAverages += 20;

            eve.Graphics.DrawString("Total Read Time", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dReadTimeTotal26, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(answer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            eve.Graphics.DrawString("Read Time Test Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dReadTimeAvg26, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            eve.Graphics.DrawString("Relevant Q's Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dReadTimeRelevantAvg26, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            eve.Graphics.DrawString("Irrelevant Q's Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dReadTimeIRAvg26, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            // Now lets print out the TSL
            eve.Graphics.DrawString("TSL High Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTSL);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[12], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 350, eve.MarginBounds.Top + iStartTSL);
            iStartTSL += 20;
            eve.Graphics.DrawString("TSL Low Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTSL);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dRDTimeAvg[13], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 350, eve.MarginBounds.Top + iStartTSL);


            iStartTSL += 20;
            eve.Graphics.DrawString("STD Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTSL);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.StandardDeviation, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 350, eve.MarginBounds.Top + iStartTSL);
            iStartTSL += 20;
            eve.Graphics.DrawString("Distort", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iStartTSL);
            if (analyzer26Q.Distortion)
            {
                eve.Graphics.DrawString("Likely", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 350, eve.MarginBounds.Top + iStartTSL);
            }
            else
            {
                eve.Graphics.DrawString("NO", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 350, eve.MarginBounds.Top + iStartTSL);

            }
            iStartTSL += 20;


        }








        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  18 Questions Report - Type4 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport18QType1(object obj, PrintPageEventArgs eve)
        {

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strDate = null;
            string strTime = null;
            string strTouchScreen = null;
            string strTestType = null;
            string strDriverLicense = null;
            string GuidSessionID = "12345";
            string strDepartmentPrefix = System.Environment.MachineName;
            string strTestScreenID = "12345";
            string strDistortQuestion = "";
            bool bPrintQuestionNumber = false;


            strDate = T("Date") + ": ";
            strTime = T("Time") + ": ";
            strTouchScreen = T("TouchScreen") + ": ";
            strTestType = T("TestType") + ": ";

            // setup the SessionID
            if (String.IsNullOrEmpty(globalTestScreen.TestSessionID))
            {
                // use the default value
            }
            else
            {
                GuidSessionID = globalTestScreen.TestSessionID;
            }

            // setup the client ID
            if ((globalTestScreen.objClient != null) && (globalTestScreen.objClient.ClientID > 0))
            {
                strTestScreenID = Convert.ToString(globalTestScreen.objClient.ClientID);
            }


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;


            Veracity.Client.Proxy.Screen screen = null;


            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];

                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 17:
                        case 20:
                        case 24:
                        case 25:
                            iRiskAnalysisTotalAdmits--;
                            break;
                        default:
                            break;
                    }
                }

                switch (i)
                {

                    case 5: // questions 3,4,5
                    case 6: // questions 3,4,5
                    case 7: // questions 3,4,5
                        if (i == 5)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 9: // questions 6,7,8
                    case 10: // questions 6,7,8
                    case 11: // questions 6,7,8
                        if (i == 9)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 10)
                        {
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 11)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 13: // questions 9,10,11
                    case 14: // questions 9,10,11
                    case 15: // questions 9,10,11
                        if (i == 13)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }
                        if (i == 14)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 17: // questions 12,13,14
                    case 18: // questions 12,13,14
                    case 19: // questions 12,13,14
                        if (i == 17)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                    case 23: // questions 3,4,5
                        if (i == 21)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 23)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    default:

                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";
                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
            }



            //
            //
            //  Start Drawing the Report at this point
            //
            //


            //Horzontal lines
            int iMoveTableDown = 120; // if we want to move the whole table horizontal
            int iAdditionalMoveTableONLY = -40; // this will only move the table



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Create the next 38 lines + 7 lines of spaces
            int lines = 1;
            int TotalLines = 28;
            int margin = 80;
            for (lines = 1, margin = 80; lines < TotalLines; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Q column
            if (bPrintQuestionNumber)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));
            }


            // Answer column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 500, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 500, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 175, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 175, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));



            // RISK column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 70, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 70, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            //strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            //eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Date
            //strDate += DateTime.Now.ToString("yyyMMdd");
            strDate += globalTestScreen.dtBeginQuestionTest.ToString("yyyMMdd");

            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            // strTime += DateTime.Now.ToString("HH:mm:ss tt");
            strTime += globalTestScreen.dtBeginQuestionTest.ToString("HH:mm:ss tt");

            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top - 60 + iMoveTableDown);

            // TouchScreen
            strTouchScreen += strDepartmentPrefix + " " + strTestType + strTestScreenID;
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "Subject: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            eve.Graphics.DrawString(strRiskAnalysis, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString(strRiskAnalysisVeracityRecommend, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 450, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            iHeader1 = 150;
            iHeader2 = 110;
            eve.Graphics.DrawString(sTitle18Q, new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iTitle18QLeftMargin, eve.MarginBounds.Top - iHeader1 + iMoveTableDown);
            eve.Graphics.DrawString(sSubTitle18Q, new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iSubTitle18QLeftMargin, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);


            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAdaptation = 40 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iAccidents = 160 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iIllegalDrugs = 260 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSexOffender = 380 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iArrest = 500 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSmuggling = 620 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTerrorism = 740 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iUnknown = 840 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTakenTestBefore = 720 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;

            // Footer
            int iFooterYaxis = 840 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant3 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant4 = 44 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestionNumber = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions1 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions2 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions3 = 45 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iRisk = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iRisk2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;

            // reaction
            bool ReactionZone1RiskAlreadyHIGH = false;
            bool ReactionZone2RiskAlreadyHIGH = false;
            bool ReactionZone3RiskAlreadyHIGH = false;
            bool ReactionZone4RiskAlreadyHIGH = false;
            bool ReactionZone5RiskAlreadyHIGH = false;
            bool ReactionZone6RiskAlreadyHIGH = false;
            bool ReactionZone7RiskAlreadyHIGH = false;

            // print out the titles
            //string sDrugTitle = "ISSUE C";
            //string sIntentTitle = "ISSUE A";
            //string sAlcoholTitle = "ISSUE B";
            string sDrugTitle = sIssueCTitle18Q;
            string sIntentTitle = sIssueATitle18Q;
            string sAlcoholTitle = sIssueBTitle18Q;
            int iLeftColumnAnswer = 505; // Print the answers, start from the left side of the paper
            int iLeftColumnHit = 590; // Print the HITS (SPRS), start from the left side
            int iLeftColumnQNumber = 455; // Print the Question numbers
            int iLeftColumnIssue = 8; // Print the Questions
            int iLeftColumnRisk = 665; // Print the RISK

            eve.Graphics.DrawString("Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 500, eve.MarginBounds.Top + iAnswerYAxis);

            eve.Graphics.DrawString("Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 575, eve.MarginBounds.Top + iSignificant);
            eve.Graphics.DrawString("Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top + iSignificant2);
            eve.Graphics.DrawString("Detected", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top + iSignificant3);
            //eve.Graphics.DrawString("Reaction(SPR)", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 58, eve.MarginBounds.Top + iSignificant4);

            if (bPrintQuestionNumber)
            {
                eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 460, eve.MarginBounds.Top + iQuestionNumber);
            }
            eve.Graphics.DrawString("Issues/Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iQuestions);
            //eve.Graphics.DrawString("No SPR per issue indicates Low Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 320, eve.MarginBounds.Top + iQuestions1);
            //eve.Graphics.DrawString("One SPR per issue indicates Limited Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 305, eve.MarginBounds.Top + iQuestions2);
            //eve.Graphics.DrawString("Two SPRs per issue indicates High Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 310, eve.MarginBounds.Top + iQuestions3);

            eve.Graphics.DrawString("Risk", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 680, eve.MarginBounds.Top + iRisk);
            eve.Graphics.DrawString("Detected", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 665, eve.MarginBounds.Top + iRisk2);

            //eve.Graphics.DrawString("ADOPT", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 185, eve.MarginBounds.Top + iAdaptation + 2);
            eve.Graphics.DrawString(sIntentTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString(sAlcoholTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString(sDrugTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString(sIssueDTitle18Q, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iArrest + 2);
            //eve.Graphics.DrawString("SMUGGLING", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iSmuggling + 2);

            eve.Graphics.DrawString("PTSSA™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 70 + iFooterYaxis);
            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - 30 + iFooterYaxis);
            eve.Graphics.DrawString(GuidSessionID, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 225, eve.MarginBounds.Top - 10 + iFooterYaxis);


            // Notes
            int iNoteTopMargin = 280;
            iNoteTopMargin -= 20;
            eve.Graphics.DrawString("NOTE: An SPR indicates subject more than likely withheld information regarding the question and/or issue.  The", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis);

            iNoteTopMargin -= 20;
            eve.Graphics.DrawString("Key Questions for this Verification Behavioral Analysis Report are Q12 and Q16.  Also see Detailed Report.", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis);

            iNoteTopMargin -= 20;
            eve.Graphics.DrawString("Administrator's Comments:", new Font("Arial", 14, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis);





            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAdoptation = iAdaptation + 2;
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushRed = new SolidBrush(Color.Red);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                if (screen.ScreenTextShort != null)
                {
                    strQuestion = screen.ScreenTextShort;
                }
                else
                {
                    strQuestion = screen.ScreenText;
                }

                // only setup the zones for a particular questions
                //
                if ((i == 5) || (i == 6) || (i == 7))
                {

                    if (ReactionZone1Counter == 0)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LOW";
                            drawBrushRISKZone1.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter == 1)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = "LIMITED";
                            drawBrushRISKZone1.Color = Color.Yellow;
                            drawBrushRISKZone1.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = "HIGH";
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter >= 2)
                    {
                        ReactionZone1Risk = "HIGH";
                        drawBrushRISKZone1.Color = Color.Red;
                        ReactionZone1RiskAlreadyHIGH = true;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 9) || (i == 10) || (i == 11))
                {

                    if (ReactionZone2Counter == 0)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LOW";
                            drawBrushRISKZone2.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                            ReactionZone2RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone2Counter == 1)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = "LIMITED";
                            drawBrushRISKZone2.Color = Color.Yellow;
                            drawBrushRISKZone2.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2RiskAlreadyHIGH = true;
                            ReactionZone2Risk = "HIGH";
                            drawBrushRISKZone2.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone2Counter >= 2)
                    {
                        ReactionZone2RiskAlreadyHIGH = true;
                        ReactionZone2Risk = "HIGH";
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 13) || (i == 14) || (i == 15))
                {

                    if (ReactionZone3Counter == 0)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LOW";
                            drawBrushRISKZone3.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                            ReactionZone3RiskAlreadyHIGH = true;
                        }

                    }
                    else if (ReactionZone3Counter == 1)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = "LIMITED";
                            drawBrushRISKZone3.Color = Color.Yellow;
                            drawBrushRISKZone3.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3RiskAlreadyHIGH = true;
                            ReactionZone3Risk = "HIGH";
                            drawBrushRISKZone3.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone3Counter >= 2)
                    {
                        ReactionZone3RiskAlreadyHIGH = true;
                        ReactionZone3Risk = "HIGH";
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                //if ((i == 17) || (i == 18) || (i == 19))
                //{

                //    if (ReactionZone4Counter == 0)
                //    {
                //        if (ReactionZone4RiskAlreadyHIGH == false)
                //        {
                //            ReactionZone4Risk = "LOW";
                //            drawBrushRISKZone4.Color = Color.Green;
                //        }

                //        // If the person ADMITS, then HIGH risk
                //        if (screen.Answer)
                //        {
                //            ReactionZone4RiskAlreadyHIGH = true;
                //            ReactionZone4Risk = "HIGH";
                //            drawBrushRISKZone4.Color = Color.Red;
                //        }
                //    }
                //    else if (ReactionZone4Counter == 1)
                //    {
                //        if (ReactionZone4RiskAlreadyHIGH == false)
                //        {
                //            ReactionZone4Risk = "LIMITED";
                //            //ReactionZone4Risk = screen.Answer.ToString() + " " + i.ToString();
                //            drawBrushRISKZone4.Color = Color.Yellow;
                //            drawBrushRISKZone4.Color = Color.DarkBlue;
                //        }

                //        // If the person ADMITS, then HIGH risk
                //        if (screen.Answer)
                //        {
                //            ReactionZone4RiskAlreadyHIGH = true;
                //            ReactionZone4Risk = "HIGH";
                //            drawBrushRISKZone4.Color = Color.Red;
                //        }
                //    }
                //    else if (ReactionZone4Counter >= 2)
                //    {
                //        ReactionZone4RiskAlreadyHIGH = true;
                //        ReactionZone4Risk = "HIGH";
                //        drawBrushRISKZone4.Color = Color.Red;
                //    }
                //}


                // only setup the zones for a particular questions
                //
                //if ((i == 21) || (i == 22) || (i == 23))
                //{

                //    if (ReactionZone5Counter == 0)
                //    {
                //        if (ReactionZone5RiskAlreadyHIGH == false)
                //        {
                //            ReactionZone5Risk = "LOW";
                //            drawBrushRISKZone5.Color = Color.Green;
                //        }

                //        // If the person ADMITS, then HIGH risk
                //        if (screen.Answer)
                //        {
                //            ReactionZone5RiskAlreadyHIGH = true;
                //            ReactionZone5Risk = "HIGH";
                //            drawBrushRISKZone5.Color = Color.Red;
                //        }
                //    }
                //    else if (ReactionZone5Counter == 1)
                //    {
                //        if (ReactionZone5RiskAlreadyHIGH == false)
                //        {
                //            ReactionZone5Risk = "LIMITED";
                //            drawBrushRISKZone5.Color = Color.Yellow;
                //            drawBrushRISKZone5.Color = Color.DarkBlue;
                //        }

                //        // If the person ADMITS, then HIGH risk
                //        if (screen.Answer)
                //        {
                //            ReactionZone5RiskAlreadyHIGH = true;
                //            ReactionZone5Risk = "HIGH";
                //            drawBrushRISKZone5.Color = Color.Red;
                //        }
                //    }
                //    else if (ReactionZone5Counter >= 2)
                //    {
                //        ReactionZone5RiskAlreadyHIGH = true;
                //        ReactionZone5Risk = "HIGH";
                //        drawBrushRISKZone5.Color = Color.Red;
                //    }

                //}





                if (screen.Answer)
                {

                    strAnswer = "YES";
                    drawBrush.Color = Color.Red;

                }
                else
                {
                    strAnswer = "No";
                    drawBrush.Color = Color.Black;
                }

                if (screen.Answer == true)
                {
                    strHit = "ADMITS";
                    drawBrushSPR.Color = Color.Red;
                }
                else if (screen.IsHit == true)
                {
                    strHit = "SPR";
                    drawBrushSPR.Color = Color.Red;
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }



                // thses are the questions that are not suppose to be ADMITS
                switch (i)
                {

                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "No";
                            drawBrush.Color = Color.Black;
                        }
                        strHit = "";
                        drawBrushSPR.Color = Color.Black;
                        break;
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                        //case 17:
                        //case 20:
                        //case 24:
                        //case 25:
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Red;
                            strHit = "Incorrect";
                            drawBrushSPR.Color = Color.Red;
                        }

                        if (screen.Answer)
                        {
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }

                        //ReactionZone1Risk = "";
                        //drawBrushRISKZone1.Color = Color.Black;

                        //ReactionZone2Risk = "";
                        //drawBrushRISKZone2.Color = Color.Black;

                        //ReactionZone3Risk = "";
                        //drawBrushRISKZone3.Color = Color.Black;

                        //ReactionZone4Risk = "";
                        //drawBrushRISKZone4.Color = Color.Black;

                        //ReactionZone5Risk = "";
                        //drawBrushRISKZone5.Color = Color.Black;

                        //ReactionZone6Risk = "";
                        //drawBrushRISKZone6.Color = Color.Black;

                        //ReactionZone7Risk = "";
                        //drawBrushRISKZone7.Color = Color.Black;

                        break;
                    case 17: // did you take this test before should not be RED
                        if (screen.Answer)
                        {

                            strAnswer = "Yes";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;

                        }
                        else
                        {
                            strAnswer = "NO";
                            drawBrush.Color = Color.Black;
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }
                        break;
                    default:
                        break;
                }

                // Determine if this question was DISTORED in the ANALYZER data
                if (globalAnalyzer18Q.QuestionThatWasDistortDuringMultipleCodeLogic18[i] > 0)
                {
                    strDistortQuestion = "#";
                    strHit += strDistortQuestion;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////

                if ((i >= 0) & (i <= 3))
                {
                    iStartAdoptation += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartAdoptation);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartAdoptation);

                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartAdoptation);

                    //if (i == 1)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 4) & (i <= 7))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartAccidents);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartAccidents);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartAccidents);

                    //if (i == 5)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 8) & (i <= 11))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartIllegal);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartIllegal);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartIllegal);

                    //if (i == 9)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 40);
                    //}
                }

                if ((i >= 12) & (i <= 15))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartSex);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartSex);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartSex);

                    //if (i == 13)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 40);
                    //}

                }
                if ((i == 16))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartArrest);
                    //eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartArrest);


                }
                if ((i == 17))
                {
                    iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartArrest);
                    //eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartArrest);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartArrest);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartArrest);
                }


            }

            // Now lets print out the Reaction Data for each ZONES
            eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnRisk, eve.MarginBounds.Top + iStartAccidents - 80);
            eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnRisk, eve.MarginBounds.Top + iStartIllegal - 80);
            eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnRisk, eve.MarginBounds.Top + iStartSex - 80);
            eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnRisk, eve.MarginBounds.Top + iStartArrest - 80);
            eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnRisk, eve.MarginBounds.Top + iStartSmuggling - 80);






        }

















        /// <summary>
        /// ///////////////////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////  18 Questions Report - Type4 version 1 - ONLY RED, GREEN, YELLOW COLORS
        /// ///////  
        /// ///////  RISK ANALSIS
        /// ///////  If ZONE has zero the LOW Green
        /// ///////  If ZONE has one SPR, the LIMITED YELLOW
        /// ///////  If ZONE has more than one SPR, then HIGH RED
        /// ///////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
        /// ///////  If any ZONE has HIGH, then the TEST is a potential THREAT!
        /// ///////////////////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="eve"></param>

        public void CreateReport18QType2(object obj, PrintPageEventArgs eve)
        {

            bool bPrintSpanish = false;  // This is a kludge for now.  Print the spanish if true

            // This function first draw the lines(Borders),String(Datas) and Image(Logo)
            string strAnswer = null;
            string strHit = null;
            string strQuestion = null;
            string strName = null;
            string strDate = "Date: ";
            string strTime = "Time: ";
            string strTouchScreen = "Touch Screen #: ";
            string strTestType = "Test Type: ";
            string strDriverLicense = null;
            string GuidSessionID = "12345";
            string strDepartmentPrefix = System.Environment.MachineName;
            string strTestScreenID = "12345";
            string strDistortQuestion = "";
            bool bPrintQuestionNumber = false;
            
            strDate = (!string.IsNullOrEmpty(T("Date"))) ? T("Date") + ": " : "Date:  ";
            strTime = (!string.IsNullOrEmpty(T("TimeOfDate"))) ? T("TimeOfDate") + ": " : "Time:  ";
            strTouchScreen = (!string.IsNullOrEmpty(T("TouchScreen"))) ? T("TouchScreen") + ": " : "Touch Screen #: ";
            strTestType = (!string.IsNullOrEmpty(T("TestType"))) ? T("TestType") + ": " : "Test Type:  ";

            if (bPrintSpanish)
            {
                strDate = "Fecha: ";
                strTime = "Hora: ";
                strTouchScreen = "Equipo#: ";
                strTestType = "Evaluation: ";
            }


            // category risk declaration
            string strIssueACatogoryRisk = "None";
            string strIssueBCatogoryRisk = "None";
            string strIssueCCatogoryRisk = "None";
            bool IssueACatogoryAdmitsSetup = false;
            bool IssueBCatogoryAdmitsSetup = false;
            bool IssueCCatogoryAdmitsSetup = false;
            bool IssueACatogoryAdmitsSecondary = false;
            bool IssueBCatogoryAdmitsSecondary = false;
            bool IssueCCatogoryAdmitsSecondary = false;
            bool IssueACatogoryAdmitsPrimary = false;
            bool IssueBCatogoryAdmitsPrimary = false;
            bool IssueCCatogoryAdmitsPrimary = false;
            SolidBrush IssueACatogoryColor = new SolidBrush(Color.Black);
            SolidBrush IssueBCatogoryColor = new SolidBrush(Color.Black);
            SolidBrush IssueCCatogoryColor = new SolidBrush(Color.Black);

            strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("None"))) ? T("None") : "None";
            strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("None"))) ? T("None") : "None";
            strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("None"))) ? T("None") : "None";
            if (bPrintSpanish)
            {
                strIssueACatogoryRisk = "Ninguno";
                strIssueBCatogoryRisk = "Ninguno";
                strIssueCCatogoryRisk = "Ninguno";
            }
            // Get the total time
            TimeSpan DifferenceTime = globalTestScreen.dtEndQuestionTest - globalTestScreen.dtBeginQuestionTest;
            //TimeSpan DifferenceTime = globalTestScreen.dtEndQuestionTest.Subtract(globalTestScreen.dtBeginQuestionTest);

            // setup the SessionID
            if (String.IsNullOrEmpty(globalTestScreen.TestSessionID))
            {
                // use the default value
            }
            else
            {
                GuidSessionID = globalTestScreen.TestSessionID;
            }

            // setup the client ID
            if ((globalTestScreen.objClient != null) && (globalTestScreen.objClient.ClientID > 0))
            {
                strTestScreenID = Convert.ToString(globalTestScreen.objClient.ClientID);
            }


            // configurable
            int leftMarginAdd = -40;  // this will move all the left vertial lines, question, number, etc to the left
            int rightMarginAdd = 40;  // this will move the right vertial to the right only

            //// PrintPreviewDialog objPrev;
            //// PrintDocument objDoc;
            //Pen pen = new Pen(Color.Black);
            //Brush brsh = new SolidBrush(Color.Black);

            // RISKS
            int ReactionZone1Counter = 0;
            int ReactionZone1Counter1 = 0;
            int ReactionZone1Counter2 = 0;
            int ReactionZone1Counter3 = 0;
            string ReactionZone1Risk = "";

            int ReactionZone2Counter = 0;
            int ReactionZone2Counter1 = 0;
            int ReactionZone2Counter2 = 0;
            int ReactionZone2Counter3 = 0;
            string ReactionZone2Risk = "";

            int ReactionZone3Counter = 0;
            int ReactionZone3Counter1 = 0;
            int ReactionZone3Counter2 = 0;
            int ReactionZone3Counter3 = 0;
            string ReactionZone3Risk = "";

            int ReactionZone4Counter = 0;
            int ReactionZone4Counter1 = 0;
            int ReactionZone4Counter2 = 0;
            int ReactionZone4Counter3 = 0;
            string ReactionZone4Risk = "";

            int ReactionZone5Counter = 0;
            int ReactionZone5Counter1 = 0;
            int ReactionZone5Counter2 = 0;
            int ReactionZone5Counter3 = 0;
            string ReactionZone5Risk = "";

            int ReactionZone6Counter = 0;
            int ReactionZone6Counter1 = 0;
            int ReactionZone6Counter2 = 0;
            int ReactionZone6Counter3 = 0;
            string ReactionZone6Risk = "";


            int ReactionZone7Counter = 0;
            int ReactionZone7Counter1 = 0;
            int ReactionZone7Counter2 = 0;
            int ReactionZone7Counter3 = 0;
            string ReactionZone7Risk = "";

            // Risk Analysis Summary
            // Possible Strings are:
            //     If no SPR or ADMITS, then "No Further Investigation is Recommended"
            //     If 1 SPR and no ADMITS, then "Further Investigation Maybe Necessary"
            //     If more than 2 SPRs, then "Further Investigation Is Necessary"
            //     If ANY Admits, then "Further Investigation Is Necessary"
            string strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
            string strRiskAnalysis = "Low Risk";
            int iRiskAnalysisTotalAdmits = 0;
            int iRiskAnalysisTotalCaution = 0;

            strRiskAnalysis = (!string.IsNullOrEmpty(T("LowRisk"))) ? T("LowRisk") : "Low Risk";
            strRiskAnalysisVeracityRecommend = (!string.IsNullOrEmpty(T("strRiskAnalysisVeracityRecommend"))) ? T("strRiskAnalysisVeracityRecommend") : "No Further Investigation is Recommended";
            if (bPrintSpanish)
            {
                strRiskAnalysis = "Bajo";
            }
            Veracity.Client.Proxy.Screen screen = null;
            Anaylyzer18Questions analyzer18Q = null;
            analyzer18Q = globalAnalyzer18Q;


            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// CALCULATE THE RISKS
            //////////  RISK ANALSIS
            //////////  If ZONE has zero the LOW Green
            //////////  If ZONE has one SPR, the LIMITED YELLOW
            //////////  If ZONE has more than one SPR, then HIGH RED
            //////////  If ZONE has an ADMIT, then HIGH.  Regardless of any counts of SPR
            //////////  If any ZONE has HIGH, then the TEST is a potential THREAT!
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {
                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];


                // If the person ADMITS, then HIGH risk
                if (screen.Answer)
                {
                    iRiskAnalysisTotalAdmits++;
                    switch (i)
                    {
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 8:
                        case 12:
                        case 16:
                        case 20:
                        case 24:
                        case 25:
                            iRiskAnalysisTotalAdmits--;
                            break;
                        default:
                            break;
                    }
                }

                // for the NEW code from PHIL
                // AS OF SEPT 2015
                // this overrides the RISK Analyaiss
                // Will be using the Catogory Admits instead
                if (screen.Answer)
                {
                    switch (i)
                    {
                        // Issue A
                        case 5:
                            IssueACatogoryAdmitsSetup = true;
                            break;
                        case 6:
                            IssueACatogoryAdmitsSecondary = true;
                            break;
                        case 7:
                            IssueACatogoryAdmitsPrimary = true;
                            break;
                        // Issue B
                        case 9:
                            IssueBCatogoryAdmitsSetup = true;
                            break;
                        case 10:
                            IssueBCatogoryAdmitsSecondary = true;
                            break;
                        case 11:
                            IssueBCatogoryAdmitsPrimary = true;
                            break;
                        // Issue C
                        case 13:
                            IssueCCatogoryAdmitsSetup = true;
                            break;
                        case 14:
                            IssueCCatogoryAdmitsSecondary = true;
                            break;
                        case 15:
                            IssueCCatogoryAdmitsPrimary = true;
                            break;
                        default:
                            break;
                    }

                }

                switch (i)
                {

                    case 5: // questions 3,4,5
                    case 6: // questions 3,4,5
                    case 7: // questions 3,4,5
                        if (i == 5)
                        {
                            if ((ReactionZone1Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter1++;
                            }
                        }
                        if (i == 6)
                        {
                            ///////////////////////////////////////////////////////// 
                            // AS OF 10/21/15 per Phil SPRAGUE
                            // we will override the SPR for question 7,11,15 using a 
                            // check box called bTurnOffSPR71115 in the admin screen
                            //////////////////////////////////////////////////////////
                            if ((ReactionZone1Counter2 == 0) && (screen.IsHit == true) && (analyzer18Q.bTurnOnSPR71115 == false))
                            {
                                ReactionZone1Counter2++;
                            }
                        }
                        if (i == 7)
                        {
                            if ((ReactionZone1Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone1Counter3++;
                            }
                        }

                        ReactionZone1Counter = ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3;
                        break;
                    case 9: // questions 6,7,8
                    case 10: // questions 6,7,8
                    case 11: // questions 6,7,8
                        if (i == 9)
                        {
                            if ((ReactionZone2Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter1++;
                            }
                        }
                        if (i == 10)
                        {
                            ///////////////////////////////////////////////////////// 
                            // AS OF 10/21/15 per Phil SPRAGUE
                            // we will override the SPR for question 7,11,15 using a 
                            // check box called bTurnOffSPR71115 in the admin screen
                            //////////////////////////////////////////////////////////
                            if ((ReactionZone2Counter2 == 0) && (screen.IsHit == true) && (analyzer18Q.bTurnOnSPR71115 == false))
                            {
                                ReactionZone2Counter2++;
                            }
                        }
                        if (i == 11)
                        {
                            if ((ReactionZone2Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone2Counter3++;
                            }
                        }

                        ReactionZone2Counter = ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3;
                        break;
                    case 13: // questions 9,10,11
                    case 14: // questions 9,10,11
                    case 15: // questions 9,10,11
                        if (i == 13)
                        {
                            if ((ReactionZone3Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter1++;
                            }
                        }

                        ///////////////////////////////////////////////////////// 
                        // AS OF 10/21/15 per Phil SPRAGUE
                        // we will override the SPR for question 7,11,15 using a 
                        // check box called bTurnOffSPR71115 in the admin screen
                        //////////////////////////////////////////////////////////
                        if (i == 14)
                        {
                            if ((ReactionZone3Counter2 == 0) && (screen.IsHit == true) && (analyzer18Q.bTurnOnSPR71115 == false))
                            {
                                ReactionZone3Counter2++;
                            }
                        }
                        if (i == 15)
                        {
                            if ((ReactionZone3Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone3Counter3++;
                            }
                        }

                        ReactionZone3Counter = ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3;
                        break;
                    case 17: // questions 12,13,14
                    case 18: // questions 12,13,14
                    case 19: // questions 12,13,14
                        if (i == 17)
                        {
                            if ((ReactionZone4Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter1++;
                            }
                        }
                        if (i == 18)
                        {
                            if ((ReactionZone4Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter2++;
                            }
                        }
                        if (i == 19)
                        {
                            if ((ReactionZone4Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone4Counter3++;
                            }
                        }

                        ReactionZone4Counter = ReactionZone4Counter1 + ReactionZone4Counter2 + ReactionZone4Counter3;
                        break;
                    case 21: // questions 3,4,5
                    case 22: // questions 3,4,5
                    case 23: // questions 3,4,5
                        if (i == 21)
                        {
                            if ((ReactionZone5Counter1 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter1++;
                            }
                        }
                        if (i == 22)
                        {
                            if ((ReactionZone5Counter2 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter2++;
                            }
                        }
                        if (i == 23)
                        {
                            if ((ReactionZone5Counter3 == 0) && (screen.IsHit == true))
                            {
                                ReactionZone5Counter3++;
                            }
                        }

                        ReactionZone5Counter = ReactionZone5Counter1 + ReactionZone5Counter2 + ReactionZone5Counter3;
                        break;
                    default:

                        break;
                }
            }

            // If no SPRs, the no risk
            if ((ReactionZone1Counter + ReactionZone2Counter + ReactionZone3Counter + ReactionZone4Counter + ReactionZone5Counter + ReactionZone6Counter + ReactionZone7Counter) == 0)
            {
                strRiskAnalysisVeracityRecommend = "No Further Investigation is Recommended";
                strRiskAnalysis = "Low Risk";

                strRiskAnalysis = (!string.IsNullOrEmpty(T("LowRisk"))) ? T("LowRisk") : "Low Risk";
                strRiskAnalysisVeracityRecommend = (!string.IsNullOrEmpty(T("strRiskAnalysisVeracityRecommend"))) ? T("strRiskAnalysisVeracityRecommend") : "No Further Investigation is Recommended";

                if (bPrintSpanish)
                {
                    strRiskAnalysis = "Bajo";
                }
            }

            // check for any caution risk
            // 
            if ((ReactionZone1Counter < 2) && (ReactionZone2Counter < 2) && (ReactionZone3Counter < 2) &&
                 (ReactionZone4Counter < 2) && (ReactionZone5Counter < 2) && (ReactionZone6Counter < 2) &&
                 (ReactionZone7Counter < 2))
            {
                if ((ReactionZone1Counter == 1) || (ReactionZone2Counter == 1) || (ReactionZone3Counter == 1) ||
                     (ReactionZone4Counter == 1) || (ReactionZone5Counter == 1) || (ReactionZone6Counter == 1) ||
                     (ReactionZone7Counter == 1))
                {
                    strRiskAnalysisVeracityRecommend = "Further Investigation May Be Necessary";
                    strRiskAnalysis = "Limited Risk";

                    strRiskAnalysis = (!string.IsNullOrEmpty(T("LimitedRisk"))) ? T("LimitedRisk") : "Limited Risk";
                    strRiskAnalysisVeracityRecommend = (!string.IsNullOrEmpty(T("strRiskAnalysisVeracityRecommend"))) ? T("strRiskAnalysisVeracityRecommend") : "No Further Investigation is Recommended";

                }

            }

            // check for any high risk wih the SPR > 2 counts
            // 

            if ((ReactionZone1Counter >= 2) || (ReactionZone2Counter >= 2) || (ReactionZone3Counter >= 2) ||
                    (ReactionZone4Counter >= 2) || (ReactionZone5Counter >= 2) || (ReactionZone6Counter >= 2) ||
                    (ReactionZone7Counter >= 2))
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation Is Necessary";
                strRiskAnalysis = "High Risk";
                strRiskAnalysis = (!string.IsNullOrEmpty(T("HighRisk"))) ? T("HighRisk") : "High Risk";
                strRiskAnalysisVeracityRecommend = (!string.IsNullOrEmpty(T("strRiskAnalysisVeracityRecommend"))) ? T("strRiskAnalysisVeracityRecommend") : "No Further Investigation is Recommended";

            }

            // if any admits, then high risk
            if (iRiskAnalysisTotalAdmits > 0)
            {
                strRiskAnalysisVeracityRecommend = "Further Investigation is Recommended";
                strRiskAnalysis = "High Risk";
                strRiskAnalysis = (!string.IsNullOrEmpty(T("HighRisk"))) ? T("HighRisk") : "High Risk";
                strRiskAnalysisVeracityRecommend = (!string.IsNullOrEmpty(T("strRiskAnalysisVeracityRecommend"))) ? T("strRiskAnalysisVeracityRecommend") : "No Further Investigation is Recommended";
            }

            //////////////////////////////////////////////////////////////
            // NEW LOGIC per Phil Sprague 20150830
            // CALCULATE THE RISK per Issue
            //
            // WARNING THIS OVERRIDES THE RISK ANALYSYS
            //
            // Category Risk 
            // NONE
            // CAUTION
            // HIGH
            //
            // defined as are:
            //
            //
            // NONE- if 0 SPR then CategoryRisk is NONE, GREEN
            // LOW - if 1 SPR in the SETUP question then Category Risk is LOW, BLUE
            // CAUTION - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is CAUTION, YELLOW
            // CAUTION - if 1 SPR in the SECONDARY question then CAUTION, YELLOW
            // CAUTION# - if 1 SPR in the PRIMARY question then CAUTION#, YELLOW
            // HIGH - if 1 SPR PRIMARY and ( 1 SPR in SETUP OR 1 SPR in SECONDARY ) then HIGH, RED
            // HIGH - if ALL SPR then HIGH, RED
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            // NOTE AS OF 10/10/2016 - Phil Changed the logic
            // 0 = Low
            // 1 = Low+
            // 2 = Caution
            // 3 = High
            // 4 = High+
            // 5 or greater than = very high
            //
            //
            // 0 - LOW if 0 SPR then CategoryRisk is NONE
            // 1 - LOW - if 1 SPR in the SETUP question then Category Risk is LOW
            // 3 - HIGH - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is HIGH
            // 2- CAUTION - if 1 SPR in the SECONDARY question then CAUTION
            // 4 - HIGH - if 1 SPR in the PRIMARY question then CAUTION
            // 5- VERY HIGH - if 1 SPR PRIMARY and ( 1 SPR in SETUP OR 1 SPR in SECONDARY ) then HIGH
            // 6 - VERY HIGH - if ALL SPR then HIGH
            //
            // NOTE AS OF 04/07/2017 - phil change the logic
            //
            // 0 - LOW if 0 SPR then CategoryRisk is NONE, GREEN
            // 1 - LOW# - if 1 SPR in the SETUP question then Category Risk is LOW, BLUE
            // 3 - CAUTION# - if 1 SPR in the SETUP and 1 SPR in the Secondary Question then Category RISK is CAUTION#, YELLOW
            // 2-  CAUTION - if 1 SPR in the SECONDARY question then CAUTION, YELLOW
            // 3 - CAUTION# - if 1 SPR in the PRIMARY question then CAUTION#, YELLOW
            // 4 - ELEVATED - if 1 SPR PRIMARY and ( 1 SPR in SETUP ) then ELEVATED, ORANGE
            // 5-  HIGH - if 1 SPR PRIMARY and ( 1 SPR in SECONDARY ) then HIGH, RED
            // 6 - HIGH# - if ALL SPR then HIGH, RED
            //
            //
            // ADMITS
            // POI  - SETUP question admit
            // POI# - SECONDARY question admit
            // HIGH# - PRIMARY question admit
            //
            //
            // NOTE AS OF 06/04/2018 - phil sprage ask to remove the POI logic for the MISSION ESSENTIALS PROJECT
            //
            // NOTE AS OF 06/04/2018 - phil says need to show any ADMITS as HIGH risk on all Setup, Secondary, and primary questions for
            //     MISSION Essentials project.  The POI will be too difficult to explain to those who have not had extensive training in
            //     reading reports.
            //     Change POI to ADMITS instead
            //
            ///////////////////////////////////////////////////////////////

            // lets check for the first category, check for none cases
            if ((ReactionZone1Counter1 + ReactionZone1Counter2 + ReactionZone1Counter3) == 0)
            {
                strIssueACatogoryRisk = "LOW";
                globalTestScreen.CategoryRiskIssueA = "0";
                IssueACatogoryColor.Color = Color.Green;

                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW";

                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "BAJO";
                }
            }
            // check for low cases,
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "LOW#";
                globalTestScreen.CategoryRiskIssueA = "1";
                IssueACatogoryColor.Color = Color.Blue;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW"): "LOW#";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "BAJO#";
                }
            }
            // check for caution cases, 1st case
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueA = "3";
                IssueACatogoryColor.Color = Color.YellowGreen;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "CUATELA#";
                }
            }
            // check for caution cases, 2nd case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 == 0))
            {
                strIssueACatogoryRisk = "CAUTION";
                globalTestScreen.CategoryRiskIssueA = "2";
                IssueACatogoryColor.Color = Color.YellowGreen;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION") : "CAUTION";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "CUATELA";
                }
            }
            // check for caution cases, third case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueA = "3";
                IssueACatogoryColor.Color = Color.YellowGreen;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "CUATELA#";
                }
            }

            // check for ELEVATED cases
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 == 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "ELEVATED";
                globalTestScreen.CategoryRiskIssueA = "4";
                IssueACatogoryColor.Color = Color.Orange;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("ELEVATED"))) ? T("ELEVATED") : "ELEVATED";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "ELEVADO";
                }
            }
            // check for HIGH cases, 1st case
            if ((ReactionZone1Counter1 == 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "HIGH";
                globalTestScreen.CategoryRiskIssueA = "5";
                IssueACatogoryColor.Color = Color.Red;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "MUY ALTO";
                }
            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone1Counter1 > 0) && (ReactionZone1Counter2 > 0) && (ReactionZone1Counter3 > 0))
            {
                strIssueACatogoryRisk = "HIGH#";
                globalTestScreen.CategoryRiskIssueA = "6";
                IssueACatogoryColor.Color = Color.Red;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH"): "HIGH#";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "MUY ALTO#";
                }
            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueACatogoryAdmitsSetup == true))
            {
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = strIssueACatogoryRisk + ", PDI";
                }
                else
                {
                    //strIssueACatogoryRisk = strIssueACatogoryRisk + ", POI"; // 20180604 no poi per phil sprague
                    strIssueACatogoryRisk = "HIGH";
                    strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                }
                globalTestScreen.CategoryRiskIssueA = "8";
                IssueACatogoryColor.Color = Color.Red;
            }
            if ((IssueACatogoryAdmitsSecondary == true))
            {
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = strIssueACatogoryRisk + ", PDI#";
                }
                else
                {
                    //strIssueACatogoryRisk = strIssueACatogoryRisk + ", POI#"; // 20180604 no poi per phil spreague
                    strIssueACatogoryRisk = "HIGH";
                    strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                }
                globalTestScreen.CategoryRiskIssueA = "8";
                IssueACatogoryColor.Color = Color.Red;
            }
            if (IssueACatogoryAdmitsPrimary == true)
            {
                //strIssueACatogoryRisk = "HIGH#"; // 20180604 all admits as high per phil sprague
                strIssueACatogoryRisk = "HIGH";
                globalTestScreen.CategoryRiskIssueA = "8";
                IssueACatogoryColor.Color = Color.Red;
                strIssueACatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueACatogoryRisk = "MUY ALTO#";
                }
            }


            //
            //
            // 2ND CATEGORY
            // lets check for the 2nd category, check for none cases
            // lets check for the first category, check for none cases
            if ((ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3) == 0)
            {
                strIssueBCatogoryRisk = "LOW";
                globalTestScreen.CategoryRiskIssueB = "0";
                IssueBCatogoryColor.Color = Color.Green;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "BAJO";
                }
            }
            // check for low cases,
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "LOW#";
                globalTestScreen.CategoryRiskIssueB = "1";
                IssueBCatogoryColor.Color = Color.Blue;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW"): "LOW#";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "BAJO#";
                }
            }
            // check for caution cases, 1st case
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueB = "3";
                IssueBCatogoryColor.Color = Color.YellowGreen;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "CUATELA#";
                }
            }
            // check for caution cases, 2nd case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            {
                strIssueBCatogoryRisk = "CAUTION";
                globalTestScreen.CategoryRiskIssueB = "2";
                IssueBCatogoryColor.Color = Color.YellowGreen;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION") : "CAUTION";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "CUATELA";
                }
            }
            // check for caution cases, third case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueB = "3";
                IssueBCatogoryColor.Color = Color.YellowGreen;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "CUATELA#";
                }
            }

            // check for ELEVATED cases
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "ELEVATED";
                globalTestScreen.CategoryRiskIssueB = "4";
                IssueBCatogoryColor.Color = Color.Orange;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("ELEVATED"))) ? T("ELEVATED") : "ELEVATED";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "ELEVADO";
                }
            }
            // check for HIGH cases, 1st case
            if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "HIGH";
                globalTestScreen.CategoryRiskIssueB = "5";
                IssueBCatogoryColor.Color = Color.Red;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "MUY ALTO";
                }
            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            {
                strIssueBCatogoryRisk = "HIGH#";
                globalTestScreen.CategoryRiskIssueB = "6";
                IssueBCatogoryColor.Color = Color.Red;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH"): "HIGH#";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "MUY ALTO#";
                }
            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueBCatogoryAdmitsSetup == true))
            {
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", PDI";
                }
                else
                {
                    //strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", POI";  per phil 20180604
                    strIssueBCatogoryRisk = "HIGH";
                    strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";

                }
                globalTestScreen.CategoryRiskIssueB = "8";
                IssueBCatogoryColor.Color = Color.Red;
            }
            if ((IssueBCatogoryAdmitsSecondary == true))
            {
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", PDI#";
                }
                else
                {
                    //strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", POI#"; per phil 20180604
                    strIssueBCatogoryRisk = "HIGH";
                    strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";

                }
                globalTestScreen.CategoryRiskIssueB = "8";
                IssueBCatogoryColor.Color = Color.Red;
            }
            if (IssueBCatogoryAdmitsPrimary == true)
            {
                strIssueBCatogoryRisk = "HIGH";
                globalTestScreen.CategoryRiskIssueB = "8";
                IssueBCatogoryColor.Color = Color.Red;
                strIssueBCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueBCatogoryRisk = "MUY ALTO#";
                }
            }





            //
            //
            // 3RD CATEGORY
            //
            //
            //
            // lets check for the first category, check for none cases
            if ((ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3) == 0)
            {
                strIssueCCatogoryRisk = "LOW";
                globalTestScreen.CategoryRiskIssueC = "0";
                IssueCCatogoryColor.Color = Color.Green;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "BAJO";
                }
            }
            // check for low cases,
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "LOW#";
                globalTestScreen.CategoryRiskIssueC = "1";
                IssueCCatogoryColor.Color = Color.Blue;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("LOW"))) ? T("LOW"): "LOW#";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "BAJO#";
                }
            }
            // check for caution cases, 1st case
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueC = "3";
                IssueCCatogoryColor.Color = Color.YellowGreen;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "CUATELA#";
                }
            }
            // check for caution cases, 2nd case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            {
                strIssueCCatogoryRisk = "CAUTION";
                globalTestScreen.CategoryRiskIssueC = "2";
                IssueCCatogoryColor.Color = Color.YellowGreen;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION") : "CAUTION";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "CUATELA";
                }
            }
            // check for caution cases, third case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "CAUTION#";
                globalTestScreen.CategoryRiskIssueC = "3";
                IssueCCatogoryColor.Color = Color.YellowGreen;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("CAUTION"))) ? T("CAUTION"): "CAUTION#";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "CUATELA#";
                }
            }

            // check for ELEVATED cases
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "ELEVATED";
                globalTestScreen.CategoryRiskIssueC = "4";
                IssueCCatogoryColor.Color = Color.Orange;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("ELEVATED"))) ? T("ELEVATED") : "ELEVATED";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "ELEVADO";
                }
            }
            // check for HIGH cases, 1st case
            if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "HIGH";
                globalTestScreen.CategoryRiskIssueC = "5";
                IssueCCatogoryColor.Color = Color.Red;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "MUY ALTO";
                }
            }
            // check for HIGH cases, 3rd case
            if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            {
                strIssueCCatogoryRisk = "HIGH#";
                globalTestScreen.CategoryRiskIssueC = "6";
                IssueCCatogoryColor.Color = Color.Red;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH"): "HIGH#";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "MUY ALTO#";
                }
            }
            // ADMITS - this will over everthing ABOVE
            // if ADMIT on Setup or Secondary then its POI, number 7
            // if ADMIT on Primary then VERY HIGH, number 8
            if ((IssueCCatogoryAdmitsSetup == true))
            {
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", PDI";
                }
                else
                {
                    //strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", POI"; per phil sprague
                    strIssueCCatogoryRisk = "HIGH";
                    strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                }
                globalTestScreen.CategoryRiskIssueC = "8";
                IssueCCatogoryColor.Color = Color.Red;
            }
            if ((IssueCCatogoryAdmitsSecondary == true))
            {
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", PDI#";
                }
                else
                {
                    //strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", POI#"; per phil sprague 20180604
                    strIssueCCatogoryRisk = "HIGH";
                    strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                }
                globalTestScreen.CategoryRiskIssueC = "8";
                IssueCCatogoryColor.Color = Color.Red;
            }
            if (IssueCCatogoryAdmitsPrimary == true)
            {
                strIssueCCatogoryRisk = "HIGH"; // per phil sprague 20180604
                globalTestScreen.CategoryRiskIssueC = "8";
                IssueCCatogoryColor.Color = Color.Red;
                strIssueCCatogoryRisk = (!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH";
                if (bPrintSpanish)
                {
                    strIssueCCatogoryRisk = "MUY ALTO#";
                }
            }





            //// lets check for the SECOND category, check for none cases
            //if ((ReactionZone2Counter1 + ReactionZone2Counter2 + ReactionZone2Counter3) == 0)
            //{
            //    strIssueBCatogoryRisk = "LOW";
            //    globalTestScreen.CategoryRiskIssueB = "0";
            //    IssueBCatogoryColor.Color = Color.Blue;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "BAJO";
            //    }
            //}
            //// check for low cases,
            //if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 == 0))
            //{
            //    strIssueBCatogoryRisk = "LOW";
            //    globalTestScreen.CategoryRiskIssueB = "1";
            //    IssueBCatogoryColor.Color = Color.Blue;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "BAJO";
            //    }
            //}
            //// check for caution cases, 1st case
            //if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            //{
            //    strIssueBCatogoryRisk = "HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "3";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "ALTO";
            //    }
            //}
            //// check for caution cases, 2nd case
            //if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 == 0))
            //{
            //    strIssueBCatogoryRisk = "CAUTION";
            //    globalTestScreen.CategoryRiskIssueB = "2";
            //    IssueBCatogoryColor.Color = Color.LightSalmon;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "CUATELA";
            //    }
            //}
            //// check for caution cases, third case
            //if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            //{
            //    strIssueBCatogoryRisk = "HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "4";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "ALTO";
            //    }
            //}

            //// check for HIGH cases, 1st case
            //if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 == 0) && (ReactionZone2Counter3 > 0))
            //{
            //    strIssueBCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "5";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// check for HIGH cases, 1st case
            //if ((ReactionZone2Counter1 == 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            //{
            //    strIssueBCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "5";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// check for HIGH cases, 3rd case
            //if ((ReactionZone2Counter1 > 0) && (ReactionZone2Counter2 > 0) && (ReactionZone2Counter3 > 0))
            //{
            //    strIssueBCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "6";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// ADMITS - this will over everthing ABOVE
            //// if ADMIT on Setup or Secondary then its POI, number 7
            //// if ADMIT on Primary then VERY HIGH, number 8
            //if ((IssueBCatogoryAdmitsSetup == true) || (IssueBCatogoryAdmitsSecondary == true))
            //{
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", PDI";
            //    }
            //    else
            //    {
            //        strIssueBCatogoryRisk = strIssueBCatogoryRisk + ", POI";
            //    }
            //    globalTestScreen.CategoryRiskIssueB = "7";
            //    IssueBCatogoryColor.Color = Color.Red;
            //}
            //if (IssueBCatogoryAdmitsPrimary == true)
            //{
            //    strIssueBCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueB = "8";
            //    IssueBCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueBCatogoryRisk = "MUY ALTO";
            //    }
            //}






            //// lets check for the THIRD category, check for none cases

            //if ((ReactionZone3Counter1 + ReactionZone3Counter2 + ReactionZone3Counter3) == 0)
            //{
            //    strIssueCCatogoryRisk = "LOW";
            //    globalTestScreen.CategoryRiskIssueC = "0";
            //    IssueCCatogoryColor.Color = Color.Blue;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "BAJO";
            //    }
            //}
            //// check for low cases,
            //if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 == 0))
            //{
            //    strIssueCCatogoryRisk = "LOW";
            //    globalTestScreen.CategoryRiskIssueC = "1";
            //    IssueCCatogoryColor.Color = Color.Blue;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "BAJO";
            //    }
            //}
            //// check for caution cases, 1st case
            //if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            //{
            //    strIssueCCatogoryRisk = "HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "3";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "ALTO";
            //    }
            //}
            //// check for caution cases, 2nd case
            //if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 == 0))
            //{
            //    strIssueCCatogoryRisk = "CAUTION";
            //    globalTestScreen.CategoryRiskIssueC = "2";
            //    IssueCCatogoryColor.Color = Color.LightSalmon;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "CUATELA";
            //    }
            //}
            //// check for caution cases, third case
            //if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            //{
            //    strIssueCCatogoryRisk = "HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "4";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "ALTO";
            //    }
            //}

            //// check for HIGH cases, 1st case
            //if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 == 0) && (ReactionZone3Counter3 > 0))
            //{
            //    strIssueCCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "5";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// check for HIGH cases, 1st case
            //if ((ReactionZone3Counter1 == 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            //{
            //    strIssueCCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "5";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// check for HIGH cases, 3rd case
            //if ((ReactionZone3Counter1 > 0) && (ReactionZone3Counter2 > 0) && (ReactionZone3Counter3 > 0))
            //{
            //    strIssueCCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "6";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //// ADMITS - this will over everthing ABOVE
            //// if ADMIT on Setup or Secondary then its POI, number 7
            //// if ADMIT on Primary then VERY HIGH, number 8
            //if ((IssueCCatogoryAdmitsSetup == true) || (IssueCCatogoryAdmitsSecondary == true))
            //{
            //    strIssueCCatogoryRisk = strIssueCCatogoryRisk + ", POI";
            //    globalTestScreen.CategoryRiskIssueC = "7";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "MUY ALTO";
            //    }
            //}
            //if (IssueCCatogoryAdmitsPrimary == true)
            //{
            //    strIssueCCatogoryRisk = "VERY HIGH";
            //    globalTestScreen.CategoryRiskIssueC = "8";
            //    IssueCCatogoryColor.Color = Color.Red;
            //    if (bPrintSpanish)
            //    {
            //        strIssueCCatogoryRisk = "MUY ALTO";
            //    }
            //}

            // send the data to the RISK SCREEN
            // analyzer18Q.strRiskAnalysisIssueC = strIssueCCatogoryRisk;

            // IF THE RISK ANALYSIS CHECK BOX IS ON THEN DONT SHOW ANYTHING
            // per Phil Sprague on 10/16/2015 for the Mexico project
            //
            //
            if (analyzer18Q.bTurnOnRiskAnalysis == true)
            {
                strIssueACatogoryRisk = "";
                IssueACatogoryColor.Color = Color.White;
                strIssueBCatogoryRisk = "";
                IssueBCatogoryColor.Color = Color.White;
                strIssueCCatogoryRisk = "";
                IssueCCatogoryColor.Color = Color.White;
            }


            //Horzontal lines
            int iMoveTableDown = 50; // if we want to move the whole table horizontal
            int iAdditionalMoveTableONLY = -40; // this will only move the table



            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY));

            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY),
               new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + 60 + iMoveTableDown + iAdditionalMoveTableONLY));

            // Create the next 38 lines + 7 lines of spaces
            int lines = 1;
            int TotalLines = 33;
            int margin = 80;
            for (lines = 1, margin = 80; lines < TotalLines; lines++, margin += 20)
            {
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin + iMoveTableDown + iAdditionalMoveTableONLY));
            }

            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left, eve.MarginBounds.Top + 200),
            //  new Point(eve.MarginBounds.Right, eve.MarginBounds.Top + 200));

            //Vertical lines 18Q TYPE 2
            // left border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));





            if (bPrintQuestionNumber)
            {
                //  Issues column
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 395, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Left + leftMarginAdd + 395, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));

                // Q column
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));
            }
            else
            {
                //  Issues column
                eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                    new Point(eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            }


            // Answers column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Left + leftMarginAdd + 475, eve.MarginBounds.Left + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Left + leftMarginAdd + 475, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));




            //// Reaction column
            //eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 100, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
            //    new Point(eve.MarginBounds.Right + rightMarginAdd - 100, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // SPR column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 185, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 185, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // READ TIME column
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd - 50, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
                new Point(eve.MarginBounds.Right + rightMarginAdd - 50, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            // right border
            eve.Graphics.DrawLine(pen, new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + iMoveTableDown + iAdditionalMoveTableONLY),
              new Point(eve.MarginBounds.Right + rightMarginAdd, eve.MarginBounds.Top + margin - 20 + iMoveTableDown + iAdditionalMoveTableONLY));


            //////////////////////////////////////////////////////////////////////
            //////
            ////// HEADERS, DATA, INFO
            //////
            //////////////////////////////////////////////////////////////////////

            // Name
            //strName = globalTestScreen.objTestSubject.FirstName + " " + globalTestScreen.objTestSubject.LastName;
            //eve.Graphics.DrawString(strName, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Date
            //strDate += DateTime.Now.ToString("yyyMMdd");
            strDate += globalTestScreen.dtBeginQuestionTest.ToString("yyyMMdd");

            // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
            //string[] formats = { "yyyyMMdd" };
            //var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
            //startDate = dt.ToString("MM/dd/yyyy");

            eve.Graphics.DrawString(strDate, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 60 + iMoveTableDown);
            // Time
            // strTime += DateTime.Now.ToString("HH:mm:ss tt");
            strTime += globalTestScreen.dtBeginQuestionTest.ToString("HH:mm:ss tt");

            eve.Graphics.DrawString(strTime, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top - 60 + iMoveTableDown);

            // TouchScreen
            // strTouchScreen += strDepartmentPrefix + "_" + strTestScreenID;
            strTouchScreen += strDepartmentPrefix + "     " + strTestType + strTestScreenID;
            eve.Graphics.DrawString(strTouchScreen, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - 80 + iMoveTableDown);

            // Drivers License
            strDriverLicense = "Subject #: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            strDriverLicense = (!string.IsNullOrEmpty(T("Subject"))) ? T("Subject") + " #:  " : "Subject #: ";
            strDriverLicense += globalTestScreen.objTestSubject.DriverLicenseNumber;

            if (bPrintSpanish)
            {
                strDriverLicense = "Sujeto #: " + globalTestScreen.objTestSubject.DriverLicenseNumber;
            }
            eve.Graphics.DrawString(strDriverLicense, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top - 80 + iMoveTableDown);
            // Title
            int iHeader1 = 200;
            int iHeader2 = 180;
            iHeader1 = 150;
            iHeader2 = 110;
            // eve.Graphics.DrawString("Detailed Summary Report", new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 200, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);
            eve.Graphics.DrawString(sTitle18QPage2, new Font("Arial", 25, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iTitle18QPage2LeftMargin, eve.MarginBounds.Top - iHeader2 + iMoveTableDown);



            // headers
            int iSpacing = 20; // this is used to create space between the zones 20 lines = 1 space

            int iAdaptation = 40 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iAccidents = 160 + iMoveTableDown + iAdditionalMoveTableONLY; // IssueA
            int iIllegalDrugs = 240 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY; // Issue B
            int iSexOffender = 340 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY; // Issue C
            int iArrest = 420 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iSmuggling = 520 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTerrorism = 620 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iUnknown = 700 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iTakenTestBefore = 600 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iLocationAverages = 480 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;
            int iLocationTSL = 660 + iMoveTableDown + iSpacing + iAdditionalMoveTableONLY;

            // Footer
            int iFooterYaxis = 935 + iMoveTableDown;

            // header title
            int iAnswerYAxis = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant3 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSignificant4 = 44 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestionNumber = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions1 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions2 = 31 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iQuestions3 = 45 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSum = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iSum2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iReactionTime = 3 + iMoveTableDown + iAdditionalMoveTableONLY;
            int iReactionTime2 = 17 + iMoveTableDown + iAdditionalMoveTableONLY;

            // reaction
            bool ReactionZone1RiskAlreadyHIGH = false;
            bool ReactionZone2RiskAlreadyHIGH = false;
            bool ReactionZone3RiskAlreadyHIGH = false;
            bool ReactionZone4RiskAlreadyHIGH = false;
            bool ReactionZone5RiskAlreadyHIGH = false;
            bool ReactionZone6RiskAlreadyHIGH = false;
            bool ReactionZone7RiskAlreadyHIGH = false;


            // print out the titles
            //string sDrugTitle = "ISSUE C";
            //string sIntentTitle = "ISSUE A";
            //string sAlcoholTitle = "ISSUE B";
            string sDrugTitle = sIssueCTitle18Q;
            string sIntentTitle = sIssueATitle18Q;
            string sAlcoholTitle = sIssueBTitle18Q;
            int iLeftColumnAnswer = 430; // Print the answers, start from the left side of the paper
            int iLeftColumnHit = 550; // Print the HITS (SPRS), start from the left side
            int iLeftColumnQNumber = 400; // Print the Question numbers
            int iLeftColumnIssue = 8; // Print the Questions
            int iLeftColumnRisk = 665; // Print the RISK
            int iLeftColumnReactionData = 495; // Print the RISK
            int iLeftColumnReadTime = 690; // Print the RISK

            if (bPrintSpanish)
            {
                eve.Graphics.DrawString("TEMAS y PREGUNTAS", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iQuestions);
                if (bPrintQuestionNumber)
                {
                    eve.Graphics.DrawString("Q#", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 400, eve.MarginBounds.Top + iQuestionNumber);
                }
                eve.Graphics.DrawString("Si/No", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Top + iAnswerYAxis);

                // SUM
                eve.Graphics.DrawString("Reaccion", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 481, eve.MarginBounds.Top + iSum);
                eve.Graphics.DrawString("Data", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 495, eve.MarginBounds.Top + iSum2);

                eve.Graphics.DrawString("Reaccion", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 565, eve.MarginBounds.Top + iSignificant);
                eve.Graphics.DrawString("Psicofisiologica", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 565, eve.MarginBounds.Top + iSignificant2);
                eve.Graphics.DrawString("Significante", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 565, eve.MarginBounds.Top + iSignificant3);
                // read Time
                eve.Graphics.DrawString("Tiempo", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 678, eve.MarginBounds.Top + iReactionTime);
                eve.Graphics.DrawString("Lectura", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 678, eve.MarginBounds.Top + iReactionTime2);
            }
            else
            {
                

                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("IssuesQuestions"))) ? T("IssuesQuestions") : "Issues/Questions", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 2, eve.MarginBounds.Top + iQuestions);
                if (bPrintQuestionNumber)
                {
                    eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Q"))) ? T("Q") + "#" : "Q", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 400, eve.MarginBounds.Top + iQuestionNumber);
                }
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Answer"))) ? T("Answer") : "Answer", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 425, eve.MarginBounds.Top + iAnswerYAxis);

                // SUM
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Reaction2"))) ? T("Reaction2") : "Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 481, eve.MarginBounds.Top + iSum);
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Data2"))) ? T("Data2") : "Data", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 490, eve.MarginBounds.Top + iSum2);

                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Significant"))) ? T("Significant") : "Significant", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 580, eve.MarginBounds.Top + iSignificant);
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Reaction"))) ? T("Reaction") : "Reaction", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 585, eve.MarginBounds.Top + iSignificant2);
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Detected"))) ? T("Detected") : "Detected", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 585, eve.MarginBounds.Top + iSignificant3);
                // read Time
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Read"))) ? T("Read") : "Read", new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 688, eve.MarginBounds.Top + iReactionTime);
                eve.Graphics.DrawString((!string.IsNullOrEmpty(T("Time"))) ? T("Time") : "Time", new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 688, eve.MarginBounds.Top + iReactionTime2);
            }
            // Issues title
            eve.Graphics.DrawString(sIntentTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iAccidents + 2);
            eve.Graphics.DrawString(sAlcoholTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIllegalDrugs + 2);
            eve.Graphics.DrawString(sDrugTitle, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iSexOffender + 2);
            eve.Graphics.DrawString(sIssueDTitle18Q, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iArrest + 20 + 2);


            // ISSUES Descriptions




            int iIssuesDescription = 510;
            iIssuesDescription += 40;
            // 3/8/15 - per phil, change test average to be ONLY the base line averages
            //string sIssuesDescription = "Test Average is calculated by Baseline (6,7,8,10,11,12,14,15,16)";
            string sIssuesDescription = "Test Average";
            string sAnswerExtraColumn = "A";
            sIssuesDescription = (!string.IsNullOrEmpty(T("TestAverage"))) ? T("TestAverage") : "Test Average";
            if (bPrintSpanish)
            {
                sIssuesDescription = "Promedio Base";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            //sIssuesDescription = "I/R Base Line Avg is calculated by Q5, Q9, Q13, Q17";
            sIssuesDescription = "Irrelevant Base Line Avg";
            sAnswerExtraColumn = "B";
            sIssuesDescription = (!string.IsNullOrEmpty(T("IrrelevantBaseLineAvg"))) ? T("IrrelevantBaseLineAvg") : "Irrelevant Base Line Avg";
            if (bPrintSpanish)
            {
                sIssuesDescription = "Probedio Base Neutro";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            // 2015-09-03 remove all Irrelevant BL per phil
            //sIssuesDescription = "Irrelevant BL Avg is calculated by Q5 and Q9";

            //sAnswerExtraColumn = "C";
            //eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            //eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;


            //sIssuesDescription = "Irrelevant BL Avg is calculated by Q9 and Q13";

            //sAnswerExtraColumn = "D";
            //eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            //eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            //sIssuesDescription = "Irrelevant BL Avg is calculated by Q13 and Q17";

            //sAnswerExtraColumn = "E";
            //eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            //eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            //sIssuesDescription = "REL Base Line Avg is calculated by Q6,7,8: Q10,11,12: Q14,15,16";
            sIssuesDescription = "Relevant Base Line Avg";
            sAnswerExtraColumn = "C";
            sIssuesDescription = (!string.IsNullOrEmpty(T("RelevantBaseLineAvg"))) ? T("RelevantBaseLineAvg") : "Relevant Base Line Avg";
            if (bPrintSpanish)
            {
                sIssuesDescription = "Probedio Base Relevante";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            sIssuesDescription = "ISSUE A Avg is calculated by Q6, Q7, Q8";
            sAnswerExtraColumn = "D";
            sIssuesDescription = (!string.IsNullOrEmpty(T("ISSUEAAvgiscalculatedbyQ6Q7Q8"))) ? T("ISSUEAAvgiscalculatedbyQ6Q7Q8") : "ISSUE A Avg is calculated by Q6, Q7, Q8";
            if (bPrintSpanish)
            {
                sIssuesDescription = "TEMA A: Promedio Base";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            sIssuesDescription = "ISSUE B Avg is calculated by Q10, Q11, Q12";
            sAnswerExtraColumn = "E";
            sIssuesDescription = (!string.IsNullOrEmpty(T("ISSUEBAvgiscalculatedbyQ10Q11Q12"))) ? T("ISSUEBAvgiscalculatedbyQ10Q11Q12") : "ISSUE B Avg is calculated by Q10, Q11, Q12";
            if (bPrintSpanish)
            {
                sIssuesDescription = "TEMA B: Promedio Base";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            sIssuesDescription = "ISSUE C Avg is calculated by Q14, Q15, Q16";
            sAnswerExtraColumn = "F";
            sIssuesDescription = (!string.IsNullOrEmpty(T("ISSUECAvgiscalculatedbyQ14Q15Q16"))) ? T("ISSUECAvgiscalculatedbyQ14Q15Q16") : "ISSUE C Avg is calculated by Q14, Q15, Q16";
            if (bPrintSpanish)
            {
                sIssuesDescription = "TEMA C: Promedio Base";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            //SolidBrush drawBrushRed = new SolidBrush(Color.Red);

            sIssuesDescription = "TSL-Hi: Any REL question above this is considered an SPR";
            sAnswerExtraColumn = "G";
            sIssuesDescription = (!string.IsNullOrEmpty(T("TSLHiAnyRELquestionabovethisisconsideredanSPR"))) ? T("TSLHiAnyRELquestionabovethisisconsideredanSPR") : "TSL-Hi: Any REL question above this is considered an SPR";
            if (bPrintSpanish)
            {
                sIssuesDescription = "Limite Superior";
            }
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;

            sIssuesDescription = "TSL-Lo: Any REL question below this is considered an SPR";
            sAnswerExtraColumn = "H";
            sIssuesDescription = (!string.IsNullOrEmpty(T("TSLLoAnyRELquestionbelowthisisconsideredanSPR"))) ? T("TSLLoAnyRELquestionbelowthisisconsideredanSPR") : "TSL-Lo: Any REL question below this is considered an SPR";
            if (bPrintSpanish)
            {
                sIssuesDescription = "Limite Inferior";
            }
            //eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;
            //sAnswerExtraColumn = "L";
            eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;


            // per Phil, remove the last row from report 3/7/15
            //sIssuesDescription = "Read Distortion.  Read ratio between REL and I/R questions";
            //sAnswerExtraColumn = "M";
            //eve.Graphics.DrawString(sAnswerExtraColumn, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 444, eve.MarginBounds.Top + iIssuesDescription + 2);
            //eve.Graphics.DrawString(sIssuesDescription, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iIssuesDescription + 2); iIssuesDescription += 20;



            // Notes


            int iNoteTopMargin = 180;

            // take out via Phil 20150829

            //string sNoteString = "NOTE: The greater the Reaction Data number from the TSL-Hi or TSL-Lo the more likely the subject did not make full disclosure.";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;
            //sNoteString = "In other words, greater the difference between the TSL factor and the Reaction Data of a Relevant questions, the greater the";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;
            //sNoteString = "probability the subject did not make full disclosure.  Also, take notice of the Read Times for the individual questions and for";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;
            //sNoteString = "ISSUE A Avg, ISSUE B Avg, and ISSUE Avg C.";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 20;
            //sNoteString = "Issue A is a Setup Control-Relevant Issue.  Issue B is a Secondary Relevant issue and also sets up Issue C.  Issue C is a";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;
            //sNoteString = "Primary Relevant issue.  The first question of the issue is the Setup question.  The second question of an issue is the Secondary";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;
            //sNoteString = "question.  The third question of an issue the Primary question";
            //eve.Graphics.DrawString(sNoteString, new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd, eve.MarginBounds.Top - iNoteTopMargin + iFooterYaxis); 
            iNoteTopMargin -= 12;


            // Footer

            // 20180515 Removed via Phil - eve.Graphics.DrawString("PTSSA Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 280, eve.MarginBounds.Top - 70 + iFooterYaxis);
            //eve.Graphics.DrawString("Veracity TouchScreener™ Report", new Font("Arial", 30, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 140, eve.MarginBounds.Top - 90 + iFooterYaxis);
            // - old y axis eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 30 + iFooterYaxis);
            // - old y axis eve.Graphics.DrawString(GuidSessionID, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 225, eve.MarginBounds.Top - 10 + iFooterYaxis);
            eve.Graphics.DrawString("U.S. Patent No. 8,264,364 issued on 9/11/2012 ", new Font("Arial", 15, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 220, eve.MarginBounds.Top - 25 + iFooterYaxis);
            eve.Graphics.DrawString(GuidSessionID, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 225, eve.MarginBounds.Top - 5 + iFooterYaxis);
            /////////////////////////////////////////////////////////////////
            // data - populate the table
            /////////////////////////////////////////////////////////////////
            int iStartAdoptation = iAdaptation + 2;
            int iStartAccidents = iAccidents + 2;
            int iStartIllegal = iIllegalDrugs + 2;
            int iStartSex = iSexOffender + 2;
            int iStartArrest = iArrest + 2;
            int iStartSmuggling = iSmuggling + 2;
            int iStartTerrorism = iTerrorism + 2;
            int iStartUnknown = iUnknown + 2;
            int iStartTakenTestBefore = iTakenTestBefore + 2;
            int iStartAverages = iLocationAverages + 2;
            int iStartTSL = iLocationTSL + 2;

            int iReadTimeColumnLeftColumn = 690;
            int iSumDataColumnLeftMargin = 490;
            int iStartAvgLeftMargin = 550;
            int iStartAvgLeftMarginRelative = iReadTimeColumnLeftColumn; // 635; // used for the RELATIVE answers
            int iStartAvgLeftMarginBase = iSumDataColumnLeftMargin; // 580; // used for the BASE answers
            //int iLastColumnLeftMargin = 690;
            //int iReadTimeColumnLeftColumn = 690;
            //int iSumDataColumnLeftMargin = iStartAvgLeftMarginRelative ;

            SolidBrush drawBrush = new SolidBrush(Color.Black);
            SolidBrush drawBrushBlack = new SolidBrush(Color.Black);
            SolidBrush drawBrushRed = new SolidBrush(Color.Red);
            SolidBrush drawBrushSPR = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone1 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone2 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone3 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone4 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone5 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone6 = new SolidBrush(Color.Black);
            SolidBrush drawBrushRISKZone7 = new SolidBrush(Color.Black);

            // averages
            SolidBrush drawBrushAverage = new SolidBrush(Color.Black);







            ////////////////////////////////////////////////////////////////////////
            //////////
            ////////// PRINT OUT THE REPORT
            //////////
            ////////////////////////////////////////////////////////////////////////
            for (int i = 0; i < globalTestScreen.lstScreenQuestionOnly.Count(); i++)
            {

                screen = (Veracity.Client.Proxy.Screen)globalTestScreen.lstScreenQuestionOnly[i];
                if (screen.ScreenTextShort != null)
                {
                    strQuestion = screen.ScreenTextShort;
                }
                else
                {
                    strQuestion = screen.ScreenText;
                }

                // only setup the zones for a particular questions
                //
                if ((i == 5) || (i == 6) || (i == 7))
                {

                    if (ReactionZone1Counter == 0)
                    {
                        

                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = ((!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW");
                            drawBrushRISKZone1.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter == 1)
                    {
                        if (ReactionZone1RiskAlreadyHIGH == false)
                        {
                            ReactionZone1Risk = ((!string.IsNullOrEmpty(T("LIMITED"))) ? T("LIMITED") : "LIMITED");
                            drawBrushRISKZone1.Color = Color.Yellow;
                            drawBrushRISKZone1.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone1Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone1.Color = Color.Red;
                            ReactionZone1RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone1Counter >= 2)
                    {
                        ReactionZone1Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                        drawBrushRISKZone1.Color = Color.Red;
                        ReactionZone1RiskAlreadyHIGH = true;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 9) || (i == 10) || (i == 11))
                {

                    if (ReactionZone2Counter == 0)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = ((!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW");
                            drawBrushRISKZone2.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone2.Color = Color.Red;
                            ReactionZone2RiskAlreadyHIGH = true;
                        }
                    }
                    else if (ReactionZone2Counter == 1)
                    {
                        if (ReactionZone2RiskAlreadyHIGH == false)
                        {
                            ReactionZone2Risk = ((!string.IsNullOrEmpty(T("LIMITED"))) ? T("LIMITED") : "LIMITED");
                            drawBrushRISKZone2.Color = Color.Yellow;
                            drawBrushRISKZone2.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone2RiskAlreadyHIGH = true;
                            ReactionZone2Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone2.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone2Counter >= 2)
                    {
                        ReactionZone2RiskAlreadyHIGH = true;
                        ReactionZone2Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                        drawBrushRISKZone2.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 13) || (i == 14) || (i == 15))
                {

                    if (ReactionZone3Counter == 0)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = ((!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW");
                            drawBrushRISKZone3.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone3.Color = Color.Red;
                            ReactionZone3RiskAlreadyHIGH = true;
                        }

                    }
                    else if (ReactionZone3Counter == 1)
                    {
                        if (ReactionZone3RiskAlreadyHIGH == false)
                        {
                            ReactionZone3Risk = ((!string.IsNullOrEmpty(T("LIMITED"))) ? T("LIMITED") : "LIMITED");
                            drawBrushRISKZone3.Color = Color.Yellow;
                            drawBrushRISKZone3.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone3RiskAlreadyHIGH = true;
                            ReactionZone3Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone3.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone3Counter >= 2)
                    {
                        ReactionZone3RiskAlreadyHIGH = true;
                        ReactionZone3Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                        drawBrushRISKZone3.Color = Color.Red;
                    }
                }

                // only setup the zones for a particular questions
                //
                if ((i == 17) || (i == 18) || (i == 19))
                {

                    if (ReactionZone4Counter == 0)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = ((!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW");
                            drawBrushRISKZone4.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter == 1)
                    {
                        if (ReactionZone4RiskAlreadyHIGH == false)
                        {
                            ReactionZone4Risk = ((!string.IsNullOrEmpty(T("LIMITED"))) ? T("LIMITED") : "LIMITED");
                            //ReactionZone4Risk = screen.Answer.ToString() + " " + i.ToString();
                            drawBrushRISKZone4.Color = Color.Yellow;
                            drawBrushRISKZone4.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone4RiskAlreadyHIGH = true;
                            ReactionZone4Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone4.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone4Counter >= 2)
                    {
                        ReactionZone4RiskAlreadyHIGH = true;
                        ReactionZone4Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                        drawBrushRISKZone4.Color = Color.Red;
                    }
                }


                // only setup the zones for a particular questions
                //
                if ((i == 21) || (i == 22) || (i == 23))
                {

                    if (ReactionZone5Counter == 0)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = ((!string.IsNullOrEmpty(T("LOW"))) ? T("LOW") : "LOW");
                            drawBrushRISKZone5.Color = Color.Green;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter == 1)
                    {
                        if (ReactionZone5RiskAlreadyHIGH == false)
                        {
                            ReactionZone5Risk = ((!string.IsNullOrEmpty(T("LIMITED"))) ? T("LIMITED") : "LIMITED");
                            drawBrushRISKZone5.Color = Color.Yellow;
                            drawBrushRISKZone5.Color = Color.DarkBlue;
                        }

                        // If the person ADMITS, then HIGH risk
                        if (screen.Answer)
                        {
                            ReactionZone5RiskAlreadyHIGH = true;
                            ReactionZone5Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                            drawBrushRISKZone5.Color = Color.Red;
                        }
                    }
                    else if (ReactionZone5Counter >= 2)
                    {
                        ReactionZone5RiskAlreadyHIGH = true;
                        ReactionZone5Risk = ((!string.IsNullOrEmpty(T("HIGH"))) ? T("HIGH") : "HIGH");
                        drawBrushRISKZone5.Color = Color.Red;
                    }

                }




                if (screen.Answer)
                {

                    strAnswer = ((!string.IsNullOrEmpty(T("YES"))) ? T("YES") : "YES");
                    drawBrush.Color = Color.Red;
                    if (bPrintSpanish)
                    {
                        strAnswer = "SI";
                    }

                }
                else
                {
                    strAnswer = ((!string.IsNullOrEmpty(T("NO"))) ? T("NO") : "No");
                    drawBrush.Color = Color.Black;
                }

                // 10/10/2016 PHIL sprague said to add ADMITS and SPR in same line
                // 01/09/2017 PHIL sprague says to remove the concatenate logic


                if (screen.Answer == true)
                {
                    strHit = ((!string.IsNullOrEmpty(T("ADMITS"))) ? T("ADMITS") : "ADMITS");
                    drawBrushSPR.Color = Color.Red;

                    if (bPrintSpanish)
                    {
                        strHit = "ADMITE";
                    }
                }
                else if (screen.IsHit == true)
                {
                    strHit = ((!string.IsNullOrEmpty(T("SPR"))) ? T("SPR") : "SPR");
                    drawBrushSPR.Color = Color.Red;

                    if (bPrintSpanish)
                    {
                        strHit = ((!string.IsNullOrEmpty(T("RPS"))) ? T("RPS") : "RPS");
                        drawBrushSPR.Color = Color.Red;
                    }
                }
                else
                {
                    strHit = "";
                    drawBrushSPR.Color = Color.Black;
                }

                //
                // concatenate logic
                //
                //strHit = "";
                //drawBrushSPR.Color = Color.Black;

                //if (screen.Answer == true)
                //{
                //    strHit = "ADMITS";
                //    drawBrushSPR.Color = Color.Red;
                //    if (bPrintSpanish)
                //    {
                //        strHit = "ADMITE";
                //    }
                //}
                //if (screen.IsHit == true)
                //{
                //    if (bPrintSpanish)
                //    {
                //        strHit = strHit + " RPS";
                //        drawBrushSPR.Color = Color.Red;
                //    }
                //    else
                //    {
                //        strHit = strHit + " SPR";
                //        drawBrushSPR.Color = Color.Red;
                //    }

                //}

                //if (screen.Answer == true)
                //{
                //    strHit = "ADMITS";
                //    drawBrushSPR.Color = Color.Red;
                //}
                //else if (screen.IsHit == true)
                //{
                //    strHit = "SPR";
                //    drawBrushSPR.Color = Color.Red;
                //}
                //else
                //{
                //    strHit = "";
                //    drawBrushSPR.Color = Color.Black;
                //}


                ///////////////////////////////////////////////////////// 
                // AS OF 10/21/15 per Phil SPRAGUE
                // we will override the SPR for question 7,11,15 using a 
                // check box called bTurnOffSPR71115 in the admin screen
                //////////////////////////////////////////////////////////
                if (analyzer18Q.bTurnOnSPR71115 == true)
                {

                    if (screen.Answer == true)
                    {
                        switch (i)
                        {
                            case 6: // question 7
                            case 10: // question 11
                            case 14: // question 15
                                strHit = "--";
                                drawBrushSPR.Color = Color.Black;
                                break;
                            default:
                                break;
                        }

                    }
                    if (screen.IsHit == true) // we got an SPR but now we will override it
                    {
                        switch (i)
                        {
                            case 6: // question 7
                            case 10: // question 11
                            case 14: // question 15
                                strHit = "-";
                                drawBrushSPR.Color = Color.Black;
                                break;
                            default:
                                break;
                        }

                    }
                }


                // thses are the questions that are not suppose to be ADMITS
                switch (i)
                {

                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        if (screen.Answer)
                        {

                            strAnswer = ((!string.IsNullOrEmpty(T("YES"))) ? T("YES") : "Yes");
                            drawBrush.Color = Color.Black;
                            if (bPrintSpanish)
                            {
                                strAnswer = "Si";
                            }

                        }
                        else
                        {
                            strAnswer = ((!string.IsNullOrEmpty(T("NO"))) ? T("NO") : "No");
                            drawBrush.Color = Color.Black;
                        }
                        strHit = "";
                        drawBrushSPR.Color = Color.Black;
                        break;
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                    case 20:
                    case 24:
                        //case 25:
                        if (screen.Answer)
                        {

                            strAnswer = ((!string.IsNullOrEmpty(T("YES"))) ? T("YES") : "Yes");
                            drawBrush.Color = Color.Black;
                            if (bPrintSpanish)
                            {
                                strAnswer = "Si";
                            }

                        }
                        else
                        {
                            strAnswer = ((!string.IsNullOrEmpty(T("NO"))) ? T("NO") : "No");
                            drawBrush.Color = Color.Red;
                            strHit = ((!string.IsNullOrEmpty(T("Incorrect"))) ? T("Incorrect") : "Incorrect");
                            drawBrushSPR.Color = Color.Red;
                            if (bPrintSpanish)
                            {
                                strHit = "Incorrecto";
                            }
                        }

                        if (screen.Answer)
                        {
                            strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }



                        break;
                    case 17: // did you take this test before should not be RED
                        if (screen.Answer)
                        {

                            strAnswer = ((!string.IsNullOrEmpty(T("YES"))) ? T("YES") : "Yes");
                            drawBrush.Color = Color.Black;
                            // 10/10/2016 - phil wants SPR back strHit = "";
                            drawBrushSPR.Color = Color.Red;
                            if (bPrintSpanish)
                            {
                                strAnswer = "Si";
                            }

                        }
                        else
                        {
                            strAnswer = ((!string.IsNullOrEmpty(T("NO"))) ? T("NO") : "No");
                            drawBrush.Color = Color.Black;
                            // 10/10/2016 - phil wants the SPR back strHit = "";
                            drawBrushSPR.Color = Color.Black;
                        }
                        break;
                    default:
                        break;
                }

                // Determine if this question was DISTORED in the ANALYZER data
                if (globalAnalyzer18Q.QuestionThatWasDistortDuringMultipleCodeLogic18[i] > 0)
                {
                    strDistortQuestion = "#";
                    strHit += strDistortQuestion;
                }

                /////////////////////////////////////////////////////////////////
                // COLUMN DATA
                ////////////////////////////////////////////////////////////////

                if ((i >= 0) & (i <= 4))
                {
                    iStartAdoptation += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartAdoptation);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartAdoptation);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartAdoptation);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartAdoptation);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartAdoptation);

                    //if (i == 1)
                    //{
                    //    eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                    //}
                }

                if ((i >= 5) & (i <= 8))
                {
                    iStartAccidents += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartAccidents);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartAccidents);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartAccidents);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartAccidents);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartAccidents);

                    if (i == 6)
                    {
                        //eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 40);
                        eve.Graphics.DrawString(strIssueACatogoryRisk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), IssueACatogoryColor, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartAccidents - 40);
                        if (bPrintSpanish)
                        {
                            eve.Graphics.DrawString("RIESGO >>", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartAccidents - 40);
                        }
                        else
                        {
                            eve.Graphics.DrawString(((!string.IsNullOrEmpty(T("RISKIS"))) ? T("RISKIS") + "->" : "Risk Is>>"), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartAccidents - 40);
                        }
                    }
                }

                if ((i >= 9) & (i <= 12))
                {
                    iStartIllegal += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartIllegal);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartIllegal);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartIllegal);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartIllegal);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartIllegal);

                    if (i == 10)
                    {
                        // used to be ReactionZone2Risk, drawBrushRISKZone2
                        eve.Graphics.DrawString(strIssueBCatogoryRisk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), IssueBCatogoryColor, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartIllegal - 40);
                        if (bPrintSpanish)
                        {
                            eve.Graphics.DrawString("RIESGO >>", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartIllegal - 40);
                        }
                        else
                        {
                            eve.Graphics.DrawString(((!string.IsNullOrEmpty(T("RISKIS"))) ? T("RISKIS") + "->" : "Risk Is>>"), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartIllegal - 40);
                        }
                    }
                }

                if ((i >= 13) & (i <= 15))
                {
                    iStartSex += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartSex);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartSex);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartSex);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartSex);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartSex);

                    if (i == 14)
                    {
                        // used to be ReactionZone3Risk, drawBrushRISKZone3
                        eve.Graphics.DrawString(strIssueCCatogoryRisk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), IssueCCatogoryColor, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartSex - 40);
                        if (bPrintSpanish)
                        {
                            eve.Graphics.DrawString("RIESGO >>", new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartSex - 40);
                        }
                        else
                        {
                            eve.Graphics.DrawString(((!string.IsNullOrEmpty(T("RISKIS"))) ? T("RISKIS") + "->" : "Risk Is>>"), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData - 20, eve.MarginBounds.Top + iStartSex - 40);
                        }
                    }

                }
                if ((i == 16))
                {
                    //iStartArrest += 20;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartArrest);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartArrest);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartArrest);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartArrest);

                    //if ((i == 17))
                    //{
                    //    eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 40);
                    //}

                }

                if (i == 17)
                {
                    iStartArrest += 40;
                    eve.Graphics.DrawString(strAnswer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrush, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnAnswer, eve.MarginBounds.Top + iStartArrest);
                    // 10/10/2016 - phil wanted the SPR back to last question
                    eve.Graphics.DrawString(strHit, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushSPR, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnHit, eve.MarginBounds.Top + iStartArrest);
                    if (bPrintQuestionNumber)
                    {
                        eve.Graphics.DrawString(Convert.ToString(i + 1), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnQNumber, eve.MarginBounds.Top + iStartArrest);
                    }
                    eve.Graphics.DrawString(strQuestion, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnIssue, eve.MarginBounds.Top + iStartArrest);

                    eve.Graphics.DrawString(Convert.ToString(Math.Round(globalAnalyzer18Q.dSumEI[i], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReactionData, eve.MarginBounds.Top + iStartArrest);
                    eve.Graphics.DrawString(Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushBlack, eve.MarginBounds.Left + leftMarginAdd + iLeftColumnReadTime, eve.MarginBounds.Top + iStartArrest);

                    //if ((i == 17))
                    //{
                    //    eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 40);
                    //}

                }


            }

            // Now lets print out the Reaction Data for each ZONES
            //eve.Graphics.DrawString(ReactionZone1Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone1, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartAccidents - 80);
            //eve.Graphics.DrawString(ReactionZone2Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone2, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartIllegal - 80);
            //eve.Graphics.DrawString(ReactionZone3Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone3, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSex - 80);
            //eve.Graphics.DrawString(ReactionZone4Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone4, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartArrest - 80);
            //eve.Graphics.DrawString(ReactionZone5Risk, new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRISKZone5, eve.MarginBounds.Left + leftMarginAdd + 660, eve.MarginBounds.Top + iStartSmuggling - 80);

            // Now lets print out the averages
            drawBrushAverage.Color = Color.Black;
            iStartAverages += 40;
            if (bPrintSpanish)
            {
                eve.Graphics.DrawString("Tiempo de Lectura", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            }
            else
            {
                eve.Graphics.DrawString(((!string.IsNullOrEmpty(T("TestTime"))) ? T("TestTime") : "Test Time"), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            }
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[0], 1)), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRed, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // this is avg, phil wanted total 
            // eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[0], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            // this is total read time. phil wanted from begin to end,
            // eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeTotal18, 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            // as of 10/9/2016, phil wanted start of the test to the end of the test
            string answer = string.Format("{0:D2}:{1:D2}", DifferenceTime.Minutes, DifferenceTime.Seconds); // convert to have minutes and seconds
            eve.Graphics.DrawString(answer, new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            // 20180515 per Phil
            //if (bPrintSpanish)
            //{
            //    eve.Graphics.DrawString("Base Netura:", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("I/R Base Line Avg", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}

            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[1], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // 20180515 per phil
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[1], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);

            // 20150903 remove the I/R BL per phil
            //iStartAverages += 20;
            //eve.Graphics.DrawString("I/R BL Avg Q 5&9", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[2], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[2], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            //iStartAverages += 20;
            //eve.Graphics.DrawString("I/R BL Avg Q 9&13", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[3], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[3], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            //iStartAverages += 20;
            //eve.Graphics.DrawString("I/R BL Avg Q 13&17", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[4], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[4], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);

            //eve.Graphics.DrawString("Base/Relative Avg Q: 18/20", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[5], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[10], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);
            //iStartAverages += 20;
            //eve.Graphics.DrawString("Base/Relative Avg Q: 22/25", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[6], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[11], 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginRelative, eve.MarginBounds.Top + iStartAverages);

            // Calculate the total read time
            // TimeSpan t = TimeSpan.FromSeconds(Math.Round(analyzer26Q.dReadTimeTotal26, 2));
            //string answer = string.Format("{0:D2}:{1:D2}",t.Minutes,t.Seconds); // convert to have the minutes and seconds
            //string answer = string.Format("{0:D2}:{1:D2}", DifferenceTime.Minutes, DifferenceTime.Seconds); // convert to have minutes and seconds
            iStartAverages += 20;
            // 20180515 per phil
            //if (bPrintSpanish)
            //{
            //    eve.Graphics.DrawString("Base Relevante:", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("REL Base Line Avg", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}

            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer26Q.dReadTimeTotal26, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + 690, eve.MarginBounds.Top + iStartAverages);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[14], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // 20180515 per phil
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[14], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            // 20180515 per Phil
            //if (bPrintSpanish)
            //{
            //    eve.Graphics.DrawString("TEMA A: T/L Pm", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("Issue A Avg", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}

            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[7], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // 20180515 per phil
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[7], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            // 20180515 per phil
            //if (bPrintSpanish)
            //{
            //    eve.Graphics.DrawString("TEMA B: T/L Pm", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("Issue B Avg", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}


            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[8], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // 20180515 per phil
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[8], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            iStartAverages += 20;
            // 20180515 per Phil
            //if (bPrintSpanish)
            //{
            //    eve.Graphics.DrawString("TEMA C: T/L Pm", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("Issue C Avg", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartAverages);
            //}

            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[9], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartAverages);
            // 20180515 per phil
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[9], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartAverages);

            // Now lets print out the TSL
            // 2016-10-10 Phil says remove the it because we dont use it remove TSL High/Low Average
            iStartTSL -= 20;
            //eve.Graphics.DrawString("TSL High Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartTSL);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[12], 1)), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRed, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartTSL);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[12], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartTSL);
            iStartTSL += 20;
            //eve.Graphics.DrawString("TSL Low Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartTSL);
            eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dRDTimeAvg[13], 1)), new Font("Arial", 13, FontStyle.Bold, GraphicsUnit.World), drawBrushRed, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartTSL);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.dReadTimeAvg[13], 1)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartTSL);

            // per Phil, remove the last row from report 3/7/15
            //iStartTSL += 20;
            //eve.Graphics.DrawString("STD Average", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartTSL);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.StandardDeviation, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMarginBase, eve.MarginBounds.Top + iStartTSL);
            //eve.Graphics.DrawString(Convert.ToString(Math.Round(analyzer18Q.StandardDeviationReadTime, 2)), new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartTSL);
            //iStartTSL += 20;
            //eve.Graphics.DrawString("Distort", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iStartAvgLeftMargin, eve.MarginBounds.Top + iStartTSL);
            //if (analyzer18Q.Distortion)
            //{
            //    eve.Graphics.DrawString("Likely", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartTSL);
            //}
            //else
            //{
            //    eve.Graphics.DrawString("NO", new Font("Arial", 13, FontStyle.Regular, GraphicsUnit.World), drawBrushAverage, eve.MarginBounds.Left + leftMarginAdd + iReadTimeColumnLeftColumn, eve.MarginBounds.Top + iStartTSL);

            //}
            ////iStartTSL += 20;



            // print out the picture
            System.Drawing.Image documentImg;

            // this is the hard coded c:\Veracity\images\*.jpg
            //string imagesdir = globalResultsFolderPath + System.IO.Path.DirectorySeparatorChar + "images";

            string imagesdir = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + "Workspace" + System.IO.Path.DirectorySeparatorChar + "Results" + System.IO.Path.DirectorySeparatorChar + "images";
            string fCurrentJPG = globalTestScreen.TestSessionID + "-" + globalTestScreen.objTestSubject.DriverLicenseNumber + ".jpg";
            string fileName = imagesdir + System.IO.Path.DirectorySeparatorChar + fCurrentJPG;

            if (File.Exists(fileName))
            {
                //string imagesdir = MyGlobals.gResultsFolderPath + System.IO.Path.DirectorySeparatorChar  + "images"; // kludge to add matt version
                //System.IO.Directory.CreateDirectory(imagesdir); // create dir if it does not exist
                //string clientID = Convert.ToString(MyGlobals.gTestSession.objClient.ClientID); // added the client id
                //string f2_matt_version = "Photo-" + clientID + "-" + MyGlobals.gTestSession.objTestSubject.DriverLicenseNumber + ".jpg";
                //string fileName = imagesdir + System.IO.Path.DirectorySeparatorChar + f2_matt_version;
                ////Here you have to specify the location of the image
                documentImg = System.Drawing.Image.FromFile(fileName);

                ////Locate the logo image on the location set on new Point
                //eve.Graphics.DrawString(fileName, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left + leftMarginAdd + 250, eve.MarginBounds.Top - 5 + iFooterYaxis);
                //eve.Graphics.DrawString(imagesdir, new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.World), brsh, eve.MarginBounds.Left, eve.MarginBounds.Top - 0 + iFooterYaxis);

                //eve.Graphics.DrawImage(documentImg, new Point(200, 820));
                eve.Graphics.DrawImage(documentImg, new Point(200, 810));
            }


        }

    }






















}



