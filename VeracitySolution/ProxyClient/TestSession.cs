﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;


namespace Veracity.Client.Proxy
{
    public class TestSession
    {
        private List<Screen> _lstScreenQuestionOnly = new List<Screen>();
        private List<Screen> _lstScreenAll = new List<Screen>();
        private TestTemplate _objTestTemplate = null;
        private TestLanguage _chosenTestLanguage = null;
        private TestSubject _objTestSubject = new TestSubject();
        private int _nSessionSerializationIndex = 0;
        private Client _objClient = null;
        private Screen _currentActiveScreen = null;
        private Test _objCurrentTest = null;

        private static DateTime dtNull = new DateTime();
        private DateTime _dtBeginSession = new DateTime();
        private DateTime _dtBeginQuestionTest = new DateTime();
        private DateTime _dtEndSession = new DateTime();
        private DateTime _dtEndQuestionTest = new DateTime();

        // added for category risk analysis for phil 10/9/2016 - add to end of the CSV file
        private string _CategoryRiskIssueA = null;
        private string _CategoryRiskIssueB = null;
        private string _CategoryRiskIssueC = null;

        private string _testSessionID = null;
        private bool _bIsFinished = false;
        //private string _transferFilename = null;
        //private string _csvFilename = null;
        //private string _csvFilenameTmp = null;
        //private string _metaFilename = null;
        //private TimeZone _curTimeZone = TimeZone.CurrentTimeZone;
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStackTrace = null;

        public static readonly string STR_UNIQUE_ID_DATE_FORMAT = "yyyyMMddHHmmssfff";
        
        public static readonly string ENTITY_NAME = "TestSession";
        public static readonly string TAG_TEST_SESSION_ID = "TestSessionID";
        public static readonly string TAG_LOCAL_TIME_ZONE_NAME = "LocalTimeZoneName";
        public static readonly string TAG_BEGIN_SESSION_DATE = "BeginSessionDate";
        public static readonly string TAG_END_SESSION_DATE = "EndSessionDate";
        public static readonly string TAG_BEGIN_QUESTION_DATE = "BeginQuestionDate";
        public static readonly string TAG_END_QUESTION_DATE = "EndQuestionDate";
        public static readonly string TAG_SCREENS_QUESTIONS_ONLY = "ScreensQuestionOnly";
        public static readonly string TAG_BEGIN_TSL_VALUE = "BeginTslValue";
        public static readonly string TAG_END_TSL_VALUE = "EndTslValue";
        public static readonly string TAG_AUTO_ADJUST = "AutoAdjust";
        public static readonly string TAG_SESSION_SERIALIZATION_INDEX = "SessionSerializationIndex";
        public static readonly string TAG_CSV_DATA = "CsvData";
        public static readonly string TAG_CURRENT_ACTIVE_SCREEN = "CurrentActiveScreen";
        public static readonly string TAG_IS_FINISHED = "IsFinished";
        public static readonly string TAG_LOCAL_TIME_NORMAL = "LocalTimeNormal";
        public static readonly string TAG_LOCAL_TIME_UTC = "LocalTimeUTC";
        public static readonly string TAG_CLIENT_CONFIG = "ClientConfig";

        public static readonly string CSV_FILENAME = "PVSA-OUTPUT.CSV";
        public static readonly string CSV_FILENAME1 = "PVSA-OUTPUT1.CSV";
        public static readonly string CSV_FILENAME_ENCRYPTED = "PVSA-OUTPUT.CSV~";
        public static readonly string CSV_FILENAME1_ENCRYPTED = "PVSA-OUTPUT1.CSV~";

        public static readonly string CSV_FILENAME1_SENSOR = "PVSA-SENSOR-OUTPUT1.CSV";
        public static readonly string CSV_FILENAME_SENSOR = "PVSA-SENSOR-OUTPUT-SENSOR.CSV";
        public static readonly string CSV_FILENAME1_SENSOR_ENCRYPTED = "PVSA-SENSOR-OUTPUT1.CSV~";
        public static readonly string CSV_FILENAME_SENSOR_ENCRYPTED = "PVSA-SENSOR-OUTPUT-SENSOR.CSV~";

        //public static readonly string TAG_ = "";
        //public static readonly string TAG_ = "";
        //public static readonly string TAG_ = "";

        private double _dBeginTslValue = -1;
        private double _dEndTslValue = -1;
        private bool _bAutoAdjust = false;

        public enum REPORT_TYPE
        {
            NONE, REPORT_26Q_1, REPORT_26Q_2, REPORT_26Q_3, REPORT_26Q_4, REPORT_26Q_5, REPORT_24Q_1, 
            REPORT_24Q_2, REPORT_16KOT_1, REPORT_16KOT_2, REPORT_15_1, REPORT_15_2,
            REPORT_18Q_1, REPORT_18Q_2, REPORT_18Q_3, REPORT_18Q_4, REPORT18Q_5
        };

        private REPORT_TYPE _reportType = REPORT_TYPE.NONE;

        public TestTemplate objTestTemplate
        {
            get { return _objTestTemplate; }
            set { _objTestTemplate = value; }
        }
        public Client objClient
        {
            get { return _objClient; }
            set { _objClient = value; }
        }
        public string TestSessionID
        {
            get { return _testSessionID; }
        }

        // 10/09/2016 - added to include the Category Risk for each issue into the CSV file
        public string CategoryRiskIssueA
        {
            get { return _CategoryRiskIssueA; }
            set { _CategoryRiskIssueA = value;  }
        }
        public string CategoryRiskIssueB
        {
            get { return _CategoryRiskIssueB; }
            set { _CategoryRiskIssueB = value; }
        }
        public string CategoryRiskIssueC
        {
            get { return _CategoryRiskIssueC; }
            set { _CategoryRiskIssueC = value; }
        }

        public int SessionSerializationIndex
        {
            get { return _nSessionSerializationIndex; }
            set { _nSessionSerializationIndex = value; }
        }
        public REPORT_TYPE ReportType
        {
            get { return _reportType; }
            set { _reportType = value; }
        }
        public double BeginTslValue
        {
            get { return _dBeginTslValue; }
            set { _dBeginTslValue = value; }
        }
        public double EndTslValue
        {
            get { return _dEndTslValue; }
            set { _dEndTslValue = value; }
        }
        public DateTime dtEndQuestionTest
        {
            get { return _dtEndQuestionTest; }
            set { _dtEndQuestionTest = value; }
        }
        public DateTime dtEndSession
        {
            get { return _dtEndSession; }
            set { _dtEndSession = value; }
        }
        public DateTime dtBeginQuestionTest
        {
            get { return _dtBeginQuestionTest; }
            set { _dtBeginQuestionTest = value; }
        }
        public DateTime dtBeginSession
        {
            get { return _dtBeginSession; }
            set { _dtBeginSession = value; }
        }
        public string errorMessage
        {
            get { return _errorMessage; }
        }
        public string errorStackTrace
        {
            get { return _errorStackTrace; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public TestSubject objTestSubject
        {
            get { return _objTestSubject; }
            set { _objTestSubject = value; }
        }
        public TestLanguage chosenTestLanguage
        {
            get { return _chosenTestLanguage; }
            set { _chosenTestLanguage = value; }
        }
        public List<Screen> lstScreenQuestionOnly
        {
            get { return _lstScreenQuestionOnly; }
            set { _lstScreenQuestionOnly = value; }
        }
        public List<Screen> lstScreenAll
        {
            get { return _lstScreenAll; }
            set { _lstScreenAll = value; }
        }
        public Screen currentActiveScreen
        {
            get { return _currentActiveScreen; }
            set { _currentActiveScreen = value; }
        }
        public bool IsFinished
        {
            get { return _bIsFinished; }
            set { _bIsFinished = value; }
        }
        public Test objCurrentTest
        {
            get { return _objCurrentTest; }
            set { _objCurrentTest = value; }
        }
        public bool AutoAdjust
        {
            get { return _bAutoAdjust; }
            set { _bAutoAdjust = value; }
        }
        public TestSession()
        {
            _testSessionID = Guid.NewGuid().ToString("N") + "_" + DateTime.Now.ToString(STR_UNIQUE_ID_DATE_FORMAT);
        }

        public TestSession(Client pObjClient)
        {
            _objClient = pObjClient;
            _testSessionID = _objClient.ClientID + "-" + Guid.NewGuid().ToString("N") + "-" + DateTime.Now.ToString(STR_UNIQUE_ID_DATE_FORMAT);
        }

        public void ParseCADriverLicense()
        {
            try
            {
                if (_objTestSubject.IdentificationRawData != null)
                {
                    int ind = _objTestSubject.IdentificationRawData.IndexOf('=') - 7;
                    if (ind >= 0)
                    {
                        _objTestSubject.DriverLicenseNumber = _objTestSubject.IdentificationRawData.Substring(ind, 7);
                        ind = _objTestSubject.IdentificationRawData.IndexOf('=') - 9;
                        string alpha = _objTestSubject.IdentificationRawData.Substring(ind, 2);
                        int alphaInt = Convert.ToInt32(alpha);
                        char[] alphaSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
                        string alphaStr = string.Empty;
                        alphaStr += alphaSet[alphaInt - 1];
                        _objTestSubject.DriverLicenseNumber = alphaStr + _objTestSubject.DriverLicenseNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }
        }
        private string getContext()
        {
            string context = null;
            if (_objTestSubject.LastName != null)
            {
                context = _objTestSubject.LastName;
            }
            else if (_objTestSubject.DriverLicenseNumber != null)
            {
                context = _objTestSubject.DriverLicenseNumber;
            }
            else if (_objTestSubject.FirstName != null)
            {
                context = _objTestSubject.FirstName;
            }

            return context;
        }
        
        public void SerializeSession(string pStrFolderPath)
        {
            try
            {
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }

        }

        public string ToXml()
        {
            StringBuilder sbReturn = null;
            DateTime dtNow = DateTime.Now;
            DateTime dtNowUTC = DateTime.UtcNow;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_SESSION_SERIALIZATION_INDEX + ">" + _nSessionSerializationIndex + "</" + TAG_SESSION_SERIALIZATION_INDEX + ">\n");
            sbReturn.Append(_objTestTemplate.ToXml());
            sbReturn.Append(_objClient.ToXml());
            sbReturn.Append(_objTestSubject.ToXml());
            sbReturn.Append("<" + TAG_IS_FINISHED + ">" + _bIsFinished + "</" + TAG_IS_FINISHED + ">\n");
            sbReturn.Append("<" + TAG_TEST_SESSION_ID + ">" + _testSessionID + "</" + TAG_TEST_SESSION_ID + ">\n");
            sbReturn.Append("<" + TAG_AUTO_ADJUST + ">" + _bAutoAdjust + "</" + TAG_AUTO_ADJUST + ">\n");
            sbReturn.Append("<" + TAG_BEGIN_TSL_VALUE + ">" + _dBeginTslValue + "</" + TAG_BEGIN_TSL_VALUE + ">\n");
            sbReturn.Append("<" + TAG_END_TSL_VALUE + ">" + _dEndTslValue + "</" + TAG_END_TSL_VALUE + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_ZONE_NAME + ">" + TimeZone.CurrentTimeZone.StandardName + "</" + TAG_LOCAL_TIME_ZONE_NAME + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_NORMAL + ">" + dtNow + "</" + TAG_LOCAL_TIME_NORMAL + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_UTC + ">" + dtNowUTC + "</" + TAG_LOCAL_TIME_UTC + ">\n");
            sbReturn.Append("<" + TAG_CSV_DATA + ">" + ToCsv() + "</" + TAG_CSV_DATA + ">\n");
            if (_currentActiveScreen != null)
            {
                sbReturn.Append("<" + TAG_CURRENT_ACTIVE_SCREEN + ">" + _currentActiveScreen.ToXml() + "</" + TAG_CURRENT_ACTIVE_SCREEN + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_CURRENT_ACTIVE_SCREEN + ">" + "</" + TAG_CURRENT_ACTIVE_SCREEN + ">\n");
            }
            if (!dtNull.Equals(_dtBeginSession))
            {
                sbReturn.Append("<" + TAG_BEGIN_SESSION_DATE + ">" + _dtBeginSession.ToString() + "</" + TAG_BEGIN_SESSION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_SESSION_DATE + "></" + TAG_BEGIN_SESSION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtEndSession))
            {
                sbReturn.Append("<" + TAG_END_SESSION_DATE + ">" + _dtEndSession.ToString() + "</" + TAG_END_SESSION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_SESSION_DATE + "></" + TAG_END_SESSION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtBeginQuestionTest))
            {
                sbReturn.Append("<" + TAG_BEGIN_QUESTION_DATE + ">" + _dtBeginQuestionTest.ToString() + "</" + TAG_BEGIN_QUESTION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_QUESTION_DATE + "></" + TAG_BEGIN_QUESTION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtEndQuestionTest))
            {
                sbReturn.Append("<" + TAG_END_QUESTION_DATE + ">" + _dtEndQuestionTest.ToString() + "</" + TAG_END_QUESTION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_QUESTION_DATE + "></" + TAG_END_QUESTION_DATE + ">\n");
            }

            sbReturn.Append("<" + TAG_SCREENS_QUESTIONS_ONLY + ">\n");
            Screen screen = null;
            for (int i = 0; i < _lstScreenQuestionOnly.Count; i++)
            {
                screen = _lstScreenQuestionOnly[i];

                sbReturn.Append("<" + Screen.ENTITY_NAME + ">\n");
                sbReturn.Append("<" + Screen.TAG_ID + ">" + screen.ScreenID + "</" + Screen.TAG_ID + ">\n");
                sbReturn.Append("<" + Screen.TAG_ANSWER + ">" + screen.Answer + "</" + Screen.TAG_ANSWER + ">\n");
                sbReturn.Append("<" + Screen.TAG_FLOW_ORDER_INDEX + ">" + (i+1) + "</" + Screen.TAG_FLOW_ORDER_INDEX + ">\n");
                sbReturn.Append("<" + Screen.TAG_DELTA_CLICK_TIME_IN_SECONDS + ">" + screen.DeltaClickTimeInSeconds + "</" + Screen.TAG_DELTA_CLICK_TIME_IN_SECONDS + ">\n");
                sbReturn.Append("<" + Screen.TAG_ENTER_SCREEN_DATE + ">" + screen.EnterScreenDate + "</" + Screen.TAG_ENTER_SCREEN_DATE + ">\n");
                sbReturn.Append("<" + Screen.TAG_EXIT_SCREEN_DATE + ">" + screen.ExitScreenDate + "</" + Screen.TAG_EXIT_SCREEN_DATE + ">\n");
                sbReturn.Append("<" + Screen.TAG_READ_TIME_IN_SECONDS + ">" + screen.ReadTimeInSeconds + "</" + Screen.TAG_READ_TIME_IN_SECONDS + ">\n");
                sbReturn.Append("</" + Screen.ENTITY_NAME + ">" + "\n");
            }
            sbReturn.Append("</" + TAG_SCREENS_QUESTIONS_ONLY + ">" + "\n");
            sbReturn.Append("<" + TAG_CLIENT_CONFIG + ">" + _objCurrentTest.ToXml() + "</" + TAG_CLIENT_CONFIG + ">\n");

            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }

        public string ToPartialXml()
        {
            StringBuilder sbReturn = null;
            DateTime dtNow = DateTime.Now;
            DateTime dtNowUTC = DateTime.UtcNow;

            sbReturn = new StringBuilder();
            sbReturn.Append("<" + ENTITY_NAME + ">\n");
            sbReturn.Append("<" + TAG_SESSION_SERIALIZATION_INDEX + ">" + _nSessionSerializationIndex + "</" + TAG_SESSION_SERIALIZATION_INDEX + ">\n");
            sbReturn.Append(_objTestTemplate.ToXml());
            sbReturn.Append(_objClient.ToXml());
            sbReturn.Append(_objTestSubject.ToXml());
            sbReturn.Append("<" + TAG_IS_FINISHED + ">" + _bIsFinished + "</" + TAG_IS_FINISHED + ">\n");
            sbReturn.Append("<" + TAG_TEST_SESSION_ID + ">" + _testSessionID + "</" + TAG_TEST_SESSION_ID + ">\n");
            sbReturn.Append("<" + TAG_AUTO_ADJUST + ">" + _bAutoAdjust + "</" + TAG_AUTO_ADJUST + ">\n");
            sbReturn.Append("<" + TAG_BEGIN_TSL_VALUE + ">" + _dBeginTslValue + "</" + TAG_BEGIN_TSL_VALUE + ">\n");
            sbReturn.Append("<" + TAG_END_TSL_VALUE + ">" + _dEndTslValue + "</" + TAG_END_TSL_VALUE + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_ZONE_NAME + ">" + TimeZone.CurrentTimeZone.StandardName + "</" + TAG_LOCAL_TIME_ZONE_NAME + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_NORMAL + ">" + dtNow + "</" + TAG_LOCAL_TIME_NORMAL + ">\n");
            sbReturn.Append("<" + TAG_LOCAL_TIME_UTC + ">" + dtNowUTC + "</" + TAG_LOCAL_TIME_UTC + ">\n");
            sbReturn.Append("<" + TAG_CSV_DATA + ">" + ToCsv() + "</" + TAG_CSV_DATA + ">\n");
            if (_currentActiveScreen != null)
            {
                sbReturn.Append("<" + TAG_CURRENT_ACTIVE_SCREEN + ">" + _currentActiveScreen.ToXml() + "</" + TAG_CURRENT_ACTIVE_SCREEN + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_CURRENT_ACTIVE_SCREEN + ">" + "</" + TAG_CURRENT_ACTIVE_SCREEN + ">\n");
            }
            if (!dtNull.Equals(_dtBeginSession))
            {
                sbReturn.Append("<" + TAG_BEGIN_SESSION_DATE + ">" + _dtBeginSession.ToString() + "</" + TAG_BEGIN_SESSION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_SESSION_DATE + "></" + TAG_BEGIN_SESSION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtEndSession))
            {
                sbReturn.Append("<" + TAG_END_SESSION_DATE + ">" + _dtEndSession.ToString() + "</" + TAG_END_SESSION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_SESSION_DATE + "></" + TAG_END_SESSION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtBeginQuestionTest))
            {
                sbReturn.Append("<" + TAG_BEGIN_QUESTION_DATE + ">" + _dtBeginQuestionTest.ToString() + "</" + TAG_BEGIN_QUESTION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_BEGIN_QUESTION_DATE + "></" + TAG_BEGIN_QUESTION_DATE + ">\n");
            }

            if (!dtNull.Equals(_dtEndQuestionTest))
            {
                sbReturn.Append("<" + TAG_END_QUESTION_DATE + ">" + _dtEndQuestionTest.ToString() + "</" + TAG_END_QUESTION_DATE + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_END_QUESTION_DATE + "></" + TAG_END_QUESTION_DATE + ">\n");
            }

            sbReturn.Append("<" + TAG_SCREENS_QUESTIONS_ONLY + ">\n");
            Screen screen = null;
            for (int i = 0; i < _lstScreenQuestionOnly.Count; i++)
            {
                screen = _lstScreenQuestionOnly[i];

                sbReturn.Append("<" + Screen.ENTITY_NAME + ">\n");
                sbReturn.Append("<" + Screen.TAG_ID + ">" + screen.ScreenID + "</" + Screen.TAG_ID + ">\n");
                sbReturn.Append("<" + Screen.TAG_ANSWER + ">" + screen.Answer + "</" + Screen.TAG_ANSWER + ">\n");
                sbReturn.Append("<" + Screen.TAG_FLOW_ORDER_INDEX + ">" + (i + 1) + "</" + Screen.TAG_FLOW_ORDER_INDEX + ">\n");
                sbReturn.Append("<" + Screen.TAG_DELTA_CLICK_TIME_IN_SECONDS + ">" + screen.DeltaClickTimeInSeconds + "</" + Screen.TAG_DELTA_CLICK_TIME_IN_SECONDS + ">\n");
                sbReturn.Append("<" + Screen.TAG_ENTER_SCREEN_DATE + ">" + screen.EnterScreenDate + "</" + Screen.TAG_ENTER_SCREEN_DATE + ">\n");
                sbReturn.Append("<" + Screen.TAG_EXIT_SCREEN_DATE + ">" + screen.ExitScreenDate + "</" + Screen.TAG_EXIT_SCREEN_DATE + ">\n");
                sbReturn.Append("<" + Screen.TAG_READ_TIME_IN_SECONDS + ">" + screen.ReadTimeInSeconds + "</" + Screen.TAG_READ_TIME_IN_SECONDS + ">\n");
                sbReturn.Append("</" + Screen.ENTITY_NAME + ">" + "\n");
            }
            sbReturn.Append("</" + TAG_SCREENS_QUESTIONS_ONLY + ">" + "\n");
            //sbReturn.Append("<" + TAG_CLIENT_CONFIG + ">" + _objCurrentTest.ToXml() + "</" + TAG_CLIENT_CONFIG + ">\n");

            sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

            return sbReturn.ToString();
        }

        public string ToCsv(List<Sensor> pLstSensorAll = null)
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(_dtBeginQuestionTest.ToString("yyyyMMdd"));
            sbReturn.Append(", " + _dtBeginQuestionTest.TimeOfDay);
            Screen screen = null;
            string tmpAnswer = null;
            string questions = String.Empty;

           
            for (int i = 0; i < _lstScreenQuestionOnly.Count; i++)
            {
                screen = _lstScreenQuestionOnly[i];
                if (screen.Answer)
                {
                    tmpAnswer = "YES";
                }
                else
                {
                    tmpAnswer = "NO";
                }
                //questions = questions +
                //    ", " + (i + 1) +
                //    ", " + tmpAnswer +
                //    ", " + screen.DeltaClickTimeInSeconds.ToString("0.000000") +
                //    ", " + screen.EnterScreenDate.TimeOfDay +
                //    ", " + screen.ExitScreenDate.TimeOfDay +
                //    ", " + screen.ReadTimeInSeconds.ToString("0.000000");

                 //questions = questions +
                 //   ", " + (i + 1) +
                 //   ", " + tmpAnswer +
                 //   ", " + String.Format("{0:F6}", screen.DeltaClickTimeInSeconds) +
                 //   ", " + screen.EnterScreenDate.TimeOfDay +
                 //   ", " + screen.ExitScreenDate.TimeOfDay +
                 //   ", " + String.Format("{0:F6}", screen.ReadTimeInSeconds);

                questions = questions +
                    ", " + (i + 1) +
                    ", " + tmpAnswer +
                    ", " + String.Format("{0:F6}", screen.DeltaClickTimeInSeconds) +
                    ", " + screen.EnterScreenDate.TimeOfDay +
                    ", " + screen.ExitScreenDate.TimeOfDay +
                    ", " + String.Format("{0:F6}", screen.ReadTimeInSeconds);

                if ( (pLstSensorAll != null) && (pLstSensorAll.Count > 0))
                {
                    uint peakPressure = FindPeakPressure(pLstSensorAll, i + 1);
                    questions = questions + ", " + peakPressure;
                    double avgPressure = FindAvgPressure(pLstSensorAll, i + 1);
                    questions = questions + ", " + avgPressure;
                    double tremor = FindTremorVal(pLstSensorAll, i + 1);
                    questions = questions + ", " + tremor;

                }
            }

            sbReturn.Append(questions);
            sbReturn.Append(", " + _dtEndQuestionTest.ToString("yyyyMMdd"));
            sbReturn.Append(", " + _dtEndQuestionTest.TimeOfDay);
            sbReturn.Append(", " + _dBeginTslValue);
            sbReturn.Append(", " + _dEndTslValue);
            return sbReturn.ToString();
        }
        public uint FindPeakPressure(List<Sensor> pLstSensorAll, int pIntQuestionNum)
        {
            uint p = 0;
            if ((pLstSensorAll != null) && (pLstSensorAll.Count > 0))
            {
                Sensor sensor = null;
                for (int i = 0; i < pLstSensorAll.Count; i++)
                {
                    sensor = (Sensor)pLstSensorAll[i];
                    if ((sensor.ScreenNum == pIntQuestionNum) && (sensor.P > p))
                    {
                        p = sensor.P;
                    }
                }
            }
            return p;
        }
        public double FindAvgPressure(List<Sensor> pLstSensorAll, int pIntQuestionNum)
        {
            double avg = 0;
            uint p = 0;
            int cnt = 0;
            if ((pLstSensorAll != null) && (pLstSensorAll.Count > 0))
            {
                Sensor sensor = null;
                for (int i = 0; i < pLstSensorAll.Count; i++)
                {
                    sensor = (Sensor)pLstSensorAll[i];
                    if ((sensor.ScreenNum == pIntQuestionNum) && (!sensor.isTailPressure))
                    {
                        p += sensor.P;
                        cnt++;
                    }
                }
            }
            if (cnt > 0)
            {
                avg = p / cnt;
            }
            return avg;
        }
        public double FindTremorVal(List<Sensor> pLstSensorAll, int pIntQuestionNum)
        {
            double totalTremor = 0;
            if ((pLstSensorAll != null) && (pLstSensorAll.Count > 0))
            {
                Sensor sensor = null;
                for (int i = 0; i < pLstSensorAll.Count; i++)
                {
                    sensor = (Sensor)pLstSensorAll[i];
                    if (sensor.ScreenNum == pIntQuestionNum)
                    {
                        totalTremor += sensor.sqrtCalcMultItselfAdd;
                    }
                }
            }
            return totalTremor;
        }
        public void SerializeToCsv(string pStrFolderPath)
        {
            try
            {
                string csvData = String.Empty;
                csvData = ToCsv();

                // write to the transfer file
                string pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + _testSessionID + ".csv";
                System.IO.File.WriteAllText(pathToSerialize, csvData);
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }

        }

        //public void SerializeToLocalCsv(string pStrFolderPath, List<Sensor> pLstSensorAll)
        //{
        //    try
        //    {
        //        string csvData = String.Empty;
        //        csvData = ToCsv();

        //        string pathToSerialize = null;
        //        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + "PVSA-OUTPUT1.CSV";
        //        System.IO.File.WriteAllText(pathToSerialize, csvData);
        //        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + "PVSA-OUTPUT.CSV";
        //        System.IO.File.AppendAllText(pathToSerialize, csvData + Environment.NewLine);

        //        csvData = ToCsv(pLstSensorAll);
        //        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + "PVSA-SENSOR-OUTPUT1.CSV";
        //        System.IO.File.WriteAllText(pathToSerialize, csvData);
        //        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + "PVSA-SENSOR-OUTPUT-SENSOR.CSV";
        //        System.IO.File.AppendAllText(pathToSerialize, csvData + Environment.NewLine);
        //    }
        //    catch (Exception ex)
        //    {
        //        _hasError = true;
        //        _errorMessage = ex.Message;
        //        _errorStackTrace = ex.StackTrace;
        //    }

        //}

        public void SerializeToLocalCsvWithIdentification(string pStrFolderPath, List<Sensor> pLstSensorAll, bool doEncryption = false)
        {
            try
            {
                string csvData = String.Empty;
                string pathToSerialize = null;

                string dataEncrypted = string.Empty;
                string data = string.Empty;

                csvData = ToCsv(pLstSensorAll);
                csvData = csvData + "," + _objTestSubject.DriverLicenseNumber + "," + System.Environment.MachineName + "," + _objClient.ClientID;
                csvData = csvData + "," + CategoryRiskIssueA + "," + CategoryRiskIssueB + "," + CategoryRiskIssueC;
                if (pLstSensorAll == null)
                {
                    if (!doEncryption)
                    {
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME1;
                        System.IO.File.WriteAllText(pathToSerialize, csvData);
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME;
                        System.IO.File.AppendAllText(pathToSerialize, csvData + Environment.NewLine);
                    }
                    else
                    {
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME1_ENCRYPTED;
                        dataEncrypted = UtilsSecurity.Encrypt(csvData, UtilsSecurity.DefaultKey);
                        System.IO.File.WriteAllText(pathToSerialize, dataEncrypted);

                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME_ENCRYPTED;
                        if (System.IO.File.Exists(pathToSerialize))
                        {
                            dataEncrypted = System.IO.File.ReadAllText(pathToSerialize);
                            data = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                            data += csvData + Environment.NewLine;
                            dataEncrypted = UtilsSecurity.Encrypt(data, UtilsSecurity.DefaultKey);
                        }
                        else
                        {
                            data = csvData + Environment.NewLine;
                            dataEncrypted = UtilsSecurity.Encrypt(data, UtilsSecurity.DefaultKey);
                        }
                        System.IO.File.WriteAllText(pathToSerialize, dataEncrypted);
                    }
                }
                else
                {
                    if (!doEncryption)
                    {
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME1_SENSOR;
                        System.IO.File.WriteAllText(pathToSerialize, csvData);
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME_SENSOR;
                        System.IO.File.AppendAllText(pathToSerialize, csvData + Environment.NewLine);
                    }
                    else
                    {
                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME1_SENSOR_ENCRYPTED;
                        dataEncrypted = UtilsSecurity.Encrypt(csvData, UtilsSecurity.DefaultKey);
                        System.IO.File.WriteAllText(pathToSerialize, dataEncrypted);

                        pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + CSV_FILENAME_SENSOR_ENCRYPTED;
                        if (System.IO.File.Exists(pathToSerialize))
                        {
                            dataEncrypted = System.IO.File.ReadAllText(pathToSerialize);
                            data = UtilsSecurity.Decrypt(dataEncrypted, UtilsSecurity.DefaultKey);
                            data += csvData + Environment.NewLine;
                            dataEncrypted = UtilsSecurity.Encrypt(data, UtilsSecurity.DefaultKey);
                        }
                        else
                        {
                            data = csvData + Environment.NewLine;
                            dataEncrypted = UtilsSecurity.Encrypt(data, UtilsSecurity.DefaultKey);
                        }
                        System.IO.File.WriteAllText(pathToSerialize, dataEncrypted);

                    }
                }
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }

        }


        public void SerializeToXml(string pStrFolderPath)
        {
            try
            {
                string xmlData = String.Empty;
                xmlData = ToXml();
                string pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + _testSessionID + ".xml";
                System.IO.File.WriteAllText(pathToSerialize, xmlData);
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }

        }

        public void SerializeToPartialXml(string pStrFolderPath)
        {
            try
            {
                string xmlData = String.Empty;
                if (_nSessionSerializationIndex == 1)
                {
                    xmlData = ToXml();
                }
                else
                {
                    xmlData = ToPartialXml();
                }
                string pathToSerialize = pStrFolderPath + System.IO.Path.DirectorySeparatorChar + _testSessionID + "." + _nSessionSerializationIndex + "~";
                System.IO.File.WriteAllText(pathToSerialize, xmlData);
            }
            catch (Exception ex)
            {
                _hasError = true;
                _errorMessage = ex.Message;
                _errorStackTrace = ex.StackTrace;
            }

        }
    }

    public class Sensor
    {
        public DateTime actionDate { get; set; }
        public string UserID { get; set; }
        public int ScreenNum { get; set; }
        public long ScreenType { get; set; }
        public uint ID { get; set; }
        public uint Ind { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public uint P { get; set; }
        public uint ContextID { get; set; }
        public string Answer { get; set; }

        public double xPrevCalc = 0;
        public double yPrevCalc = 0;
        public double xPrevCalcMultItself = 0;
        public double yPrevCalcMultItself = 0;
        public double CalcMultItselfAdd = 0;
        public double sqrtCalcMultItselfAdd = 0;
        public bool isTailPressure = false;
        public int TailPressureStartInd = 0;

        public Sensor()
        {
            Ind = 0;
            ID = 0;
            Answer = null;
        }
        public static string toCSVHeaders()
        {
            CultureInfo ci = CultureInfo.InvariantCulture;
            StringBuilder sb = new StringBuilder();
            sb.Append("Ind");
            sb.Append(",IndCum");
            sb.Append(",Screen");
            sb.Append(",Type");
            sb.Append(",ID");
            sb.Append(",Date");
            sb.Append(",X");
            sb.Append(",Y");
            sb.Append(",P");
            sb.Append(",Ans");
            sb.Append(",PrevX");
            sb.Append(",PrevY");
            sb.Append(",PrevX^2");
            sb.Append(",PrevY^2");
            sb.Append(",Sum");
            sb.Append(",SQRT");
            sb.Append(",TailIndStr");
            sb.Append(",IsTailP");

            return sb.ToString();
        }
        public string toCSV()
        {
            CultureInfo ci = CultureInfo.InvariantCulture;
            StringBuilder sb = new StringBuilder();
            string screentypetext = null;
            switch (ScreenType)
            {
                case 1:
                    screentypetext = "intr";
                    break;
                case 2:
                    screentypetext = "conc";
                    break;
                case 3:
                    screentypetext = "ques";
                    break;
                default:
                    break;
            }
            sb.Append(Ind);
            sb.Append("," + ID);
            sb.Append("," + ScreenNum);
            sb.Append("," + screentypetext);
            sb.Append("," + UserID);
            sb.Append("," + actionDate.ToString("MM-dd-yy hh:mm:ss.FFFFFFF"));
            sb.Append("," + X);
            sb.Append("," + Y);
            sb.Append("," + P);
            sb.Append("," + Answer);
            sb.Append("," + xPrevCalc);
            sb.Append("," + yPrevCalc);
            sb.Append("," + xPrevCalcMultItself);
            sb.Append("," + yPrevCalcMultItself);
            sb.Append("," + CalcMultItselfAdd);
            sb.Append("," + sqrtCalcMultItselfAdd);
            sb.Append("," + TailPressureStartInd);
            sb.Append("," + isTailPressure);

            return sb.ToString();
        }
    }
}
