using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
    /// <summary>
    /// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  Screen.cs
    /// History
    /// ----------------------------------------------------
    /// 001	BH     7/25/2013	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the Screen database table.
    /// </summary>
    public class Screen
    {
        //attributes
        /// <summary>ScreenID Attribute type long</summary>
        private long _lScreenID = 0;
        /// <summary>ScreenTypeID Attribute type long</summary>
        private long _lScreenTypeID = 0;
        /// <summary>DateCreated Attribute type DateTime</summary>
        private DateTime _dtDateCreated = dtNull;
        /// <summary>ScreenText Attribute type string</summary>
        private string _strScreenText = null;
        /// <summary>ScreenImage Attribute type byte[]</summary>
        private byte[] _byteScreenImage = null;
        /// <summary>AudioData Attribute type byte[]</summary>
        private byte[] _byteAudioData = null;
        /// <summary>IsText Attribute type bool</summary>
        private bool? _bIsText = null;
        /// <summary>IsAudio Attribute type bool</summary>
        private bool? _bIsAudio = null;
        /// <summary>ScreenTextShort Attribute type string</summary>
        private string _strScreenTextShort = null;

        private static DateTime dtNull = new DateTime();
        private bool _hasError = false;
        private ErrorCode _errorCode = null;

        /// <summary>Attribute of type string</summary>
        public static readonly string ENTITY_NAME = "Screen"; //Table name to abstract

        // Attribute variables
        /// <summary>TAG_ID Attribute</summary>
        public static readonly string TAG_ID = "ScreenID"; //Attribute id  name
        /// <summary>ScreenTypeID Attribute type string</summary>
        public static readonly string TAG_SCREEN_TYPE_ID = "ScreenTypeID"; //Table ScreenTypeID field name
        /// <summary>DateCreated Attribute type string</summary>
        public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
        /// <summary>ScreenText Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT = "ScreenText"; //Table ScreenText field name
        /// <summary>ScreenImage Attribute type string</summary>
        public static readonly string TAG_SCREEN_IMAGE = "ScreenImage"; //Table ScreenImage field name
        /// <summary>AudioData Attribute type string</summary>
        public static readonly string TAG_AUDIO_DATA = "AudioData"; //Table AudioData field name
        /// <summary>IsText Attribute type string</summary>
        public static readonly string TAG_IS_TEXT = "IsText"; //Table IsText field name
        /// <summary>IsAudio Attribute type string</summary>
        public static readonly string TAG_IS_AUDIO = "IsAudio"; //Table IsAudio field name
        /// <summary>ScreenTextShort Attribute type string</summary>
        public static readonly string TAG_SCREEN_TEXT_SHORT = "ScreenTextShort"; //Table ScreenTextShort field name


        /*********************** CUSTOM NON-META BEGIN *********************/

        private int _nFlowOrderIndex = 0; // Question number
        private bool _bAnswer = false; // Answer YES or NO
        private double _dReadTimeInSeconds = 0; // Time it took to read a question
        private DateTime _dtBeginAnsClickDate; // The time the person pressed the button press
        private DateTime _dtEndAnsClickDate; // The time the persion released the button press
        private double _dDeltaClickTimeInSeconds = 0; // The DELTA click time
        private double _ZaxisData = 0; // Z-Axis Data
        private bool? _bIsHit = null; // DLL populates this.  Reaction to the quetion HIT/Null/NO

        private DateTime _dtEnterScreenDate; // The date entered the screen
        private DateTime _dtExitScreenDate; // The date exited the screen

        public static readonly string TAG_ANSWER = "Answer";
        public static readonly string TAG_READ_TIME_IN_SECONDS = "ReadTimeInSeconds";
        public static readonly string TAG_BEGIN_ANS_CLICK_DATE = "BeginAnsClickDate";
        public static readonly string TAG_END_ANS_CLICK_DATE = "EndAnsClickDate";
        public static readonly string TAG_DELTA_CLICK_TIME_IN_SECONDS = "DeltaClickTimeInSeconds";
        public static readonly string TAG_Z_AXIS_DATA = "ZaxisData";
        public static readonly string TAG_IS_HIT = "IsHit";
        public static readonly string TAG_FLOW_ORDER_INDEX = "FlowOrderIndex";
        public static readonly string TAG_ENTER_SCREEN_DATE = "EnterScreenDate";
        public static readonly string TAG_EXIT_SCREEN_DATE = "ExitScreenDate";

        public double ZAxisData
        {
            get { return _ZaxisData; }
            set { _ZaxisData = value; }
        }

        public bool? IsHit
        {
            get { return _bIsHit; }
            set { _bIsHit = value; }
        }
        public bool Answer
        {
            get { return _bAnswer; }
            set { _bAnswer = value; }
        }
        public DateTime BeginAnsClickDate
        {
            get { return _dtBeginAnsClickDate; }
            set { _dtBeginAnsClickDate = value; }
        }
        public DateTime EndAnsClickDate
        {
            get { return _dtEndAnsClickDate; }
            set { _dtEndAnsClickDate = value; }
        }
        public int FlowOrderIndex
        {
            get { return _nFlowOrderIndex; }
            set { _nFlowOrderIndex = value; }
        }
        public double ReadTimeInSeconds
        {
            get { return _dReadTimeInSeconds; }
            set { _dReadTimeInSeconds = value; }
        }
        public double DeltaClickTimeInSeconds
        {
            get { return _dDeltaClickTimeInSeconds; }
            set { _dDeltaClickTimeInSeconds = value; }
        }
        public DateTime EnterScreenDate
        {
            get { return _dtEnterScreenDate; }
            set { _dtEnterScreenDate = value; }
        }
        public DateTime ExitScreenDate
        {
            get { return _dtExitScreenDate; }
            set { _dtExitScreenDate = value; }
        }
        /*********************** CUSTOM NON-META END *********************/




        //properties
        /// <summary>ScreenID is a Property in the Screen Class of type long</summary>
        public long ScreenID
        {
            get { return _lScreenID; }
            set { _lScreenID = value; }
        }
        /// <summary>ScreenTypeID is a Property in the Screen Class of type long</summary>
        public long ScreenTypeID
        {
            get { return _lScreenTypeID; }
            set { _lScreenTypeID = value; }
        }
        /// <summary>DateCreated is a Property in the Screen Class of type DateTime</summary>
        public DateTime DateCreated
        {
            get { return _dtDateCreated; }
            set { _dtDateCreated = value; }
        }
        /// <summary>ScreenText is a Property in the Screen Class of type String</summary>
        public string ScreenText
        {
            get { return _strScreenText; }
            set { _strScreenText = value; }
        }
        /// <summary>ScreenImage is a Property in the Screen Class of type byte[]</summary>
        public byte[] ScreenImage
        {
            get { return _byteScreenImage; }
            set { _byteScreenImage = value; }
        }
        /// <summary>AudioData is a Property in the Screen Class of type byte[]</summary>
        public byte[] AudioData
        {
            get { return _byteAudioData; }
            set { _byteAudioData = value; }
        }
        /// <summary>IsText is a Property in the Screen Class of type bool</summary>
        public bool? IsText
        {
            get { return _bIsText; }
            set { _bIsText = value; }
        }
        /// <summary>IsAudio is a Property in the Screen Class of type bool</summary>
        public bool? IsAudio
        {
            get { return _bIsAudio; }
            set { _bIsAudio = value; }
        }
        /// <summary>ScreenTextShort is a Property in the Screen Class of type String</summary>
        public string ScreenTextShort
        {
            get { return _strScreenTextShort; }
            set { _strScreenTextShort = value; }
        }
        /// <summary>HasError Property in class Screen and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
        }
        /// <summary>Error Property in class Screen and is of type ErrorCode</summary>
        public ErrorCode Error
        {
            get { return _errorCode; }
        }
        //Constructors
        /// <summary>Screen empty constructor</summary>
        public Screen()
        {
        }
        /// <summary>Screen Constructor takes string pStrData and Config</summary>
        public Screen(string pStrData)
        {
            Parse(pStrData);
        }
        /// <summary>
        ///     Dispose of this object's resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(true); // as a service to those who might inherit from us
        }
        /// <summary>
        ///		Free the instance variables of this object.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return; // we're being collected, so let the GC take care of this object
        }

        // public methods
        /// <summary>ToString is overridden to display all properties of the Screen Class</summary>
        public override string ToString()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append(TAG_ID + ":  " + ScreenID.ToString() + "\n");
            sbReturn.Append(TAG_SCREEN_TYPE_ID + ":  " + ScreenTypeID + "\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
            }
            else
            {
                sbReturn.Append(TAG_DATE_CREATED + ":\n");
            }
            sbReturn.Append(TAG_SCREEN_TEXT + ":  " + ScreenText + "\n");
            sbReturn.Append(TAG_SCREEN_IMAGE + ":  " + ScreenImage + "\n");
            sbReturn.Append(TAG_AUDIO_DATA + ":  " + AudioData + "\n");
            sbReturn.Append(TAG_IS_TEXT + ":  " + IsText + "\n");
            sbReturn.Append(TAG_IS_AUDIO + ":  " + IsAudio + "\n");
            sbReturn.Append(TAG_SCREEN_TEXT_SHORT + ":  " + ScreenTextShort + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Creates well formatted XML - includes all properties of Screen</summary>
        public string ToXml()
        {
            StringBuilder sbReturn = null;

            sbReturn = new StringBuilder();
            sbReturn.Append("<Screen>\n");
            sbReturn.Append("<" + TAG_ID + ">" + ScreenID + "</" + TAG_ID + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TYPE_ID + ">" + ScreenTypeID + "</" + TAG_SCREEN_TYPE_ID + ">\n");
            if (!dtNull.Equals(DateCreated))
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
            }
            else
            {
                sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
            }
            sbReturn.Append("<" + TAG_SCREEN_TEXT + ">" + ScreenText + "</" + TAG_SCREEN_TEXT + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_IMAGE + ">" + ScreenImage + "</" + TAG_SCREEN_IMAGE + ">\n");
            sbReturn.Append("<" + TAG_AUDIO_DATA + ">" + AudioData + "</" + TAG_AUDIO_DATA + ">\n");
            sbReturn.Append("<" + TAG_IS_TEXT + ">" + IsText + "</" + TAG_IS_TEXT + ">\n");
            sbReturn.Append("<" + TAG_IS_AUDIO + ">" + IsAudio + "</" + TAG_IS_AUDIO + ">\n");
            sbReturn.Append("<" + TAG_SCREEN_TEXT_SHORT + ">" + ScreenTextShort + "</" + TAG_SCREEN_TEXT_SHORT + ">\n");
            sbReturn.Append("</Screen>" + "\n");

            return sbReturn.ToString();
        }
        /// <summary>Parse accepts a string in XML format and parses values</summary>
        public void Parse(string pStrXml)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrXml);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                foreach (XmlNode xNode in xNodes)
                {
                    Parse(xNode);
                }
            }
            catch
            {
                _hasError = true;
                _errorCode = new ErrorCode();
            }
        }
        /// <summary>Parse accepts an XmlNode and parses values</summary>
        public void Parse(XmlNode xNode)
        {
            XmlNode xResultNode = null;
            string strTmp = null;

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_ID);
                strTmp = xResultNode.InnerText;
                ScreenID = (long)Convert.ToInt32(strTmp);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TYPE_ID);
                ScreenTypeID = (long)Convert.ToInt32(xResultNode.InnerText);
            }
            catch
            {
                ScreenTypeID = 0;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
                DateCreated = DateTime.Parse(xResultNode.InnerText);
            }
            catch
            {
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT);
                ScreenText = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
            // Cannot reliably convert a byte[] to a string().
            // Cannot reliably convert a byte[] to a string().

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_TEXT);
                IsText = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsText = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_IS_AUDIO);
                IsAudio = Convert.ToBoolean(xResultNode.InnerText);
            }
            catch
            {
                IsAudio = false;
            }

            try
            {
                xResultNode = xNode.SelectSingleNode(TAG_SCREEN_TEXT_SHORT);
                ScreenTextShort = xResultNode.InnerText;
            }
            catch
            {
                xResultNode = null;
            }
        }
    }
}

//END OF Screen CLASS FILE

