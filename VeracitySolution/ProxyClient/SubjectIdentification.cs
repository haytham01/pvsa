﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Veracity.Client.Proxy
{
    public class SubjectIdentification
    {
        private string _strFirstName = null;
        private string _strMiddleName = null;
        private string _strLastName = null;
        private string _strDriverLicenseNumber = null;
        private string _strDriverLicenseExpirationDateStr = null;
        private DateTime _dtBirthDate;

        public string FirstName
        {
            get { return _strFirstName; }
            set { _strFirstName = value; }
        }
        public string MiddleName
        {
            get { return _strMiddleName; }
            set { _strMiddleName = value; }
        }
        public string LastName
        {
            get { return _strLastName; }
            set { _strLastName = value; }
        }
        public DateTime BirthDate
        {
            get { return _dtBirthDate; }
            set { _dtBirthDate = value; }
        }
        public string DriverLicenseNumber
        {
            get { return _strDriverLicenseNumber; }
            set { _strDriverLicenseNumber = value; }
        }
        public string DriverLicenseExpirationDateStr
        {
            get { return _strDriverLicenseExpirationDateStr; }
            set { _strDriverLicenseExpirationDateStr = value; }
        }
        public SubjectIdentification()
        {
        }

    }
}
