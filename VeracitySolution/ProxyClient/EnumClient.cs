using System;
using System.Xml;
using System.Text;
using System.Collections;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  Client.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// </summary>
	public class EnumClient
	{
		//attributes
		/// <summary>ClientID Attribute type long</summary>
		private long _lClientID = 0;
		/// <summary>BeginDateCreated Attribute type DateTime</summary>
		private DateTime _dtBeginDateCreated = dtNull;
		/// <summary>EndDateCreated Attribute type DateTime</summary>
		private DateTime _dtEndDateCreated = dtNull;
		/// <summary>BeginDateModified Attribute type DateTime</summary>
		private DateTime _dtBeginDateModified = dtNull;
		/// <summary>EndDateModified Attribute type DateTime</summary>
		private DateTime _dtEndDateModified = dtNull;
		/// <summary>BeginDateHeartbeat Attribute type DateTime</summary>
		private DateTime _dtBeginDateHeartbeat = dtNull;
		/// <summary>EndDateHeartbeat Attribute type DateTime</summary>
		private DateTime _dtEndDateHeartbeat = dtNull;
		/// <summary>Code Attribute type string</summary>
		private string _strCode = null;
		/// <summary>Description Attribute type string</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute type string</summary>
		private string _strVisibleCode = null;
		/// <summary>TestInProgress Attribute type bool</summary>
		private bool? _bTestInProgress = false;
		/// <summary>AppMetaData Attribute type string</summary>
		private string _strAppMetaData = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "EnumClient"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "ClientID"; //Attribute id  name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_CREATED = "BeginDateCreated"; //Table DateCreated field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_END_DATE_CREATED = "EndDateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_MODIFIED = "BeginDateModified"; //Table DateModified field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_END_DATE_MODIFIED = "EndDateModified"; //Table DateModified field name
		/// <summary>DateHeartbeat Attribute type string</summary>
		public static readonly string TAG_BEGIN_DATE_HEARTBEAT = "BeginDateHeartbeat"; //Table DateHeartbeat field name
		/// <summary>DateHeartbeat Attribute type string</summary>
		public static readonly string TAG_END_DATE_HEARTBEAT = "EndDateHeartbeat"; //Table DateHeartbeat field name
		/// <summary>Code Attribute type string</summary>
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		/// <summary>Description Attribute type string</summary>
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		/// <summary>VisibleCode Attribute type string</summary>
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		/// <summary>TestInProgress Attribute type string</summary>
		public static readonly string TAG_TEST_IN_PROGRESS = "TestInProgress"; //Table TestInProgress field name
		/// <summary>AppMetaData Attribute type string</summary>
		public static readonly string TAG_APP_META_DATA = "AppMetaData"; //Table AppMetaData field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>ClientID is a Property in the Client Class of type long</summary>
		public long ClientID 
		{
			get{return _lClientID;}
			set{_lClientID = value;}
		}
		/// <summary>BeginDateCreated is a Property in the Client Class of type DateTime</summary>
		public DateTime BeginDateCreated 
		{
			get{return _dtBeginDateCreated;}
			set{_dtBeginDateCreated = value;}
		}
		/// <summary>EndDateCreated is a Property in the Client Class of type DateTime</summary>
		public DateTime EndDateCreated 
		{
			get{return _dtEndDateCreated;}
			set{_dtEndDateCreated = value;}
		}
		/// <summary>BeginDateModified is a Property in the Client Class of type DateTime</summary>
		public DateTime BeginDateModified 
		{
			get{return _dtBeginDateModified;}
			set{_dtBeginDateModified = value;}
		}
		/// <summary>EndDateModified is a Property in the Client Class of type DateTime</summary>
		public DateTime EndDateModified 
		{
			get{return _dtEndDateModified;}
			set{_dtEndDateModified = value;}
		}
		/// <summary>BeginDateHeartbeat is a Property in the Client Class of type DateTime</summary>
		public DateTime BeginDateHeartbeat 
		{
			get{return _dtBeginDateHeartbeat;}
			set{_dtBeginDateHeartbeat = value;}
		}
		/// <summary>EndDateHeartbeat is a Property in the Client Class of type DateTime</summary>
		public DateTime EndDateHeartbeat 
		{
			get{return _dtEndDateHeartbeat;}
			set{_dtEndDateHeartbeat = value;}
		}
		/// <summary>Code is a Property in the Client Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the Client Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the Client Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>TestInProgress is a Property in the Client Class of type bool</summary>
		public bool? TestInProgress 
		{
			get{return _bTestInProgress;}
			set{_bTestInProgress = value;}
		}
		/// <summary>AppMetaData is a Property in the Client Class of type String</summary>
		public string AppMetaData 
		{
			get{return _strAppMetaData;}
			set{_strAppMetaData = value;}
		}
		/// <summary>HasError Property in class Client and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class Client and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>Client empty constructor</summary>
		public EnumClient()
		{
		}
		/// <summary>Client Constructor takes string pStrData and Config</summary>
		public EnumClient(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the Client Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + ClientID.ToString() + "\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":  " + BeginDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":  " + EndDateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":  " + BeginDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":  " + EndDateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(BeginDateHeartbeat))
			{
				sbReturn.Append(TAG_BEGIN_DATE_HEARTBEAT + ":  " + BeginDateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_BEGIN_DATE_HEARTBEAT + ":\n");
			}
			if (!dtNull.Equals(EndDateHeartbeat))
			{
				sbReturn.Append(TAG_END_DATE_HEARTBEAT + ":  " + EndDateHeartbeat.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_END_DATE_HEARTBEAT + ":\n");
			}
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TEST_IN_PROGRESS + ":  " + TestInProgress + "\n");
			sbReturn.Append(TAG_APP_META_DATA + ":  " + AppMetaData + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of Client</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<" + ENTITY_NAME + ">\n");
			sbReturn.Append("<" + TAG_ID + ">" + ClientID + "</" + TAG_ID + ">\n");
			if (!dtNull.Equals(BeginDateCreated))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + ">" + BeginDateCreated.ToString() + "</" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_CREATED + "></" + TAG_BEGIN_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(EndDateCreated))
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + ">" + EndDateCreated.ToString() + "</" + TAG_END_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_CREATED + "></" + TAG_END_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(BeginDateModified))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + ">" + BeginDateModified.ToString() + "</" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_MODIFIED + "></" + TAG_BEGIN_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(EndDateModified))
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + ">" + EndDateModified.ToString() + "</" + TAG_END_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_MODIFIED + "></" + TAG_END_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(BeginDateHeartbeat))
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_HEARTBEAT + ">" + BeginDateHeartbeat.ToString() + "</" + TAG_BEGIN_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_BEGIN_DATE_HEARTBEAT + "></" + TAG_BEGIN_DATE_HEARTBEAT + ">\n");
			}
			if (!dtNull.Equals(EndDateHeartbeat))
			{
				sbReturn.Append("<" + TAG_END_DATE_HEARTBEAT + ">" + EndDateHeartbeat.ToString() + "</" + TAG_END_DATE_HEARTBEAT + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_END_DATE_HEARTBEAT + "></" + TAG_END_DATE_HEARTBEAT + ">\n");
			}
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TEST_IN_PROGRESS + ">" + TestInProgress + "</" + TAG_TEST_IN_PROGRESS + ">\n");
			sbReturn.Append("<" + TAG_APP_META_DATA + ">" + AppMetaData + "</" + TAG_APP_META_DATA + ">\n");
			sbReturn.Append("</" + ENTITY_NAME + ">" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
/// <summary>Parses XML, puts in arraylist</summary>
public static void ParseToClientArrayList(string pStrXml, ArrayList pRefArrayList)
		{
			try
			{
				Client client = null;
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + Client.ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					if (pRefArrayList != null)
					{
						client = new Client();
						client.Parse(xNode);
						pRefArrayList.Add(client);
					}
				}
			}
			catch (Exception e)
			{
			}
		}
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ClientID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_CREATED);
				BeginDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_CREATED);
				EndDateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_MODIFIED);
				BeginDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_MODIFIED);
				EndDateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_BEGIN_DATE_HEARTBEAT);
				BeginDateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_END_DATE_HEARTBEAT);
				EndDateHeartbeat = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
				if (Code.Trim().Length == 0)
					Code = null;
			}
			catch  
			{
				Code = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
				if (Description.Trim().Length == 0)
					Description = null;
			}
			catch  
			{
				Description = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
				if (VisibleCode.Trim().Length == 0)
					VisibleCode = null;
			}
			catch  
			{
				VisibleCode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_IN_PROGRESS);
				TestInProgress = Convert.ToBoolean(xResultNode.InnerText);
			}
			catch  
			{
			TestInProgress = false;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_APP_META_DATA);
				AppMetaData = xResultNode.InnerText;
				if (AppMetaData.Trim().Length == 0)
					AppMetaData = null;
			}
			catch  
			{
				AppMetaData = null;
			}
		}
	}
}

//END OF Client CLASS FILE


