using System;
using System.Xml;
using System.Text;


namespace Veracity.Client.Proxy
{
    /// <summary>
    /// Copyright (c) 2007 Haytham Allos.  San Diego, California, USA
    /// All Rights Reserved
    /// 
    /// File:  ErrorCode.cs
    /// History
    /// ----------------------------------------------------
    /// 001	HA     6/19/2007	Created
    /// 
    /// ----------------------------------------------------
    /// Abstracts the Address database table.
    /// </summary>
    /// 
    [Serializable]
	public class ErrorCode
	{
		//Attributes
		/// <summary>ErrorCodeID Attribute</summary>
		private long _lErrorCodeID = 0;
		/// <summary>Code Attribute</summary>
		private string _strCode = "OK";
		/// <summary>Description Attribute</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute</summary>
		private string _strVisibleCode = null;
		/// <summary>Trace Attribute</summary>
		private string _strTrace = null;

		private bool _hasError = false;

		/// <summary>HasError Property in class ErrorCode and is of type bool</summary>
		public static readonly string ENTITY_NAME = "ErrorCode"; //Table name to abstract

		// Attribute variables
		public static readonly string TAG_ID = "ErrorCodeID"; //Attribute id  name
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		public static readonly string TAG_TRACE = "Trace"; //Table Trace field name

		//properties
		/// <summary>ErrorCodeID is a Property in the ErrorCode Class of type long</summary>
		public long ErrorCodeID 
		{
			get{return _lErrorCodeID;}
			set{_lErrorCodeID = value;}
		}
		/// <summary>Code is a Property in the ErrorCode Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the ErrorCode Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the ErrorCode Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>Trace is a Property in the ErrorCode Class of type String</summary>
		public string Trace 
		{
			get{return _strTrace;}
			set{_strTrace = value;}
		}
		/// <summary>HasError Property in class ErrorCode and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		//Constructors
		/// <summary>ErrorCode empty constructor</summary>
		public ErrorCode()
		{
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///                     Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the ErrorCode Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();   
			sbReturn.Append(TAG_ID + ":  " + ErrorCodeID.ToString() + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TRACE + ":  " + Trace + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of ErrorCode</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();   
			sbReturn.Append("<ErrorCode>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ErrorCodeID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TRACE + ">" + Trace + "</" + TAG_TRACE + ">\n");
			sbReturn.Append("</ErrorCode>" + "\n");

			return sbReturn.ToString();
		}   
        
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
				_hasError = true;
			}
		}                       
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ErrorCodeID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TRACE);
				Trace = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF ErrorCode CLASS FILE
