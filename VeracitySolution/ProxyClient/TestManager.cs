using System;
using System.Xml;
using System.Text;

namespace Veracity.Client.Proxy
{
	/// <summary>
	/// Copyright (c) 2013 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  TestManager.cs
	/// History
	/// ----------------------------------------------------
	/// 001	BH     7/2/2013	Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the TestManager database table.
	/// </summary>
	public class TestManager
	{
		//attributes
		/// <summary>TestManagerID Attribute type long</summary>
		private long _lTestManagerID = 0;
		/// <summary>TestTemplateID Attribute type long</summary>
		private long _lTestTemplateID = 0;
		/// <summary>TestLanguageID Attribute type long</summary>
		private long _lTestLanguageID = 0;
		/// <summary>TestSubjectID Attribute type long</summary>
		private long _lTestSubjectID = 0;
		/// <summary>ScreenID Attribute type long</summary>
		private long _lScreenID = 0;
		/// <summary>DateCreated Attribute type DateTime</summary>
		private DateTime _dtDateCreated = dtNull;
		/// <summary>DateModified Attribute type DateTime</summary>
		private DateTime _dtDateModified = dtNull;
		/// <summary>DateBeginTest Attribute type DateTime</summary>
		private DateTime _dtDateBeginTest = dtNull;
		/// <summary>DateEndTest Attribute type DateTime</summary>
		private DateTime _dtDateEndTest = dtNull;
		/// <summary>ReportData Attribute type string</summary>
		private string _strReportData = null;
		/// <summary>Notes Attribute type string</summary>
		private string _strNotes = null;

		private static DateTime dtNull = new DateTime();
		private bool _hasError = false;
		private ErrorCode _errorCode = null;

		/// <summary>Attribute of type string</summary>
		public static readonly string ENTITY_NAME = "TestManager"; //Table name to abstract

		// Attribute variables
		/// <summary>TAG_ID Attribute</summary>
		public static readonly string TAG_ID = "TestManagerID"; //Attribute id  name
		/// <summary>TestTemplateID Attribute type string</summary>
		public static readonly string TAG_TEST_TEMPLATE_ID = "TestTemplateID"; //Table TestTemplateID field name
		/// <summary>TestLanguageID Attribute type string</summary>
		public static readonly string TAG_TEST_LANGUAGE_ID = "TestLanguageID"; //Table TestLanguageID field name
		/// <summary>TestSubjectID Attribute type string</summary>
		public static readonly string TAG_TEST_SUBJECT_ID = "TestSubjectID"; //Table TestSubjectID field name
		/// <summary>ScreenID Attribute type string</summary>
		public static readonly string TAG_SCREEN_ID = "ScreenID"; //Table ScreenID field name
		/// <summary>DateCreated Attribute type string</summary>
		public static readonly string TAG_DATE_CREATED = "DateCreated"; //Table DateCreated field name
		/// <summary>DateModified Attribute type string</summary>
		public static readonly string TAG_DATE_MODIFIED = "DateModified"; //Table DateModified field name
		/// <summary>DateBeginTest Attribute type string</summary>
		public static readonly string TAG_DATE_BEGIN_TEST = "DateBeginTest"; //Table DateBeginTest field name
		/// <summary>DateEndTest Attribute type string</summary>
		public static readonly string TAG_DATE_END_TEST = "DateEndTest"; //Table DateEndTest field name
		/// <summary>ReportData Attribute type string</summary>
		public static readonly string TAG_REPORT_DATA = "ReportData"; //Table ReportData field name
		/// <summary>Notes Attribute type string</summary>
		public static readonly string TAG_NOTES = "Notes"; //Table Notes field name


/*********************** CUSTOM NON-META BEGIN *********************/

/*********************** CUSTOM NON-META END *********************/


		//properties
		/// <summary>TestManagerID is a Property in the TestManager Class of type long</summary>
		public long TestManagerID 
		{
			get{return _lTestManagerID;}
			set{_lTestManagerID = value;}
		}
		/// <summary>TestTemplateID is a Property in the TestManager Class of type long</summary>
		public long TestTemplateID 
		{
			get{return _lTestTemplateID;}
			set{_lTestTemplateID = value;}
		}
		/// <summary>TestLanguageID is a Property in the TestManager Class of type long</summary>
		public long TestLanguageID 
		{
			get{return _lTestLanguageID;}
			set{_lTestLanguageID = value;}
		}
		/// <summary>TestSubjectID is a Property in the TestManager Class of type long</summary>
		public long TestSubjectID 
		{
			get{return _lTestSubjectID;}
			set{_lTestSubjectID = value;}
		}
		/// <summary>ScreenID is a Property in the TestManager Class of type long</summary>
		public long ScreenID 
		{
			get{return _lScreenID;}
			set{_lScreenID = value;}
		}
		/// <summary>DateCreated is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateCreated 
		{
			get{return _dtDateCreated;}
			set{_dtDateCreated = value;}
		}
		/// <summary>DateModified is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateModified 
		{
			get{return _dtDateModified;}
			set{_dtDateModified = value;}
		}
		/// <summary>DateBeginTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateBeginTest 
		{
			get{return _dtDateBeginTest;}
			set{_dtDateBeginTest = value;}
		}
		/// <summary>DateEndTest is a Property in the TestManager Class of type DateTime</summary>
		public DateTime DateEndTest 
		{
			get{return _dtDateEndTest;}
			set{_dtDateEndTest = value;}
		}
		/// <summary>ReportData is a Property in the TestManager Class of type String</summary>
		public string ReportData 
		{
			get{return _strReportData;}
			set{_strReportData = value;}
		}
		/// <summary>Notes is a Property in the TestManager Class of type String</summary>
		public string Notes 
		{
			get{return _strNotes;}
			set{_strNotes = value;}
		}
		/// <summary>HasError Property in class TestManager and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class TestManager and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}
//Constructors
		/// <summary>TestManager empty constructor</summary>
		public TestManager()
		{
		}
		/// <summary>TestManager Constructor takes string pStrData and Config</summary>
		public TestManager(string pStrData)
		{
			Parse(pStrData);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///		Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the TestManager Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append(TAG_ID + ":  " + TestManagerID.ToString() + "\n");
			sbReturn.Append(TAG_TEST_TEMPLATE_ID + ":  " + TestTemplateID + "\n");
			sbReturn.Append(TAG_TEST_LANGUAGE_ID + ":  " + TestLanguageID + "\n");
			sbReturn.Append(TAG_TEST_SUBJECT_ID + ":  " + TestSubjectID + "\n");
			sbReturn.Append(TAG_SCREEN_ID + ":  " + ScreenID + "\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append(TAG_DATE_CREATED + ":  " + DateCreated.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_CREATED + ":\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":  " + DateModified.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_MODIFIED + ":\n");
			}
			if (!dtNull.Equals(DateBeginTest))
			{
				sbReturn.Append(TAG_DATE_BEGIN_TEST + ":  " + DateBeginTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_BEGIN_TEST + ":\n");
			}
			if (!dtNull.Equals(DateEndTest))
			{
				sbReturn.Append(TAG_DATE_END_TEST + ":  " + DateEndTest.ToString() + "\n");
			}
			else
			{
				sbReturn.Append(TAG_DATE_END_TEST + ":\n");
			}
			sbReturn.Append(TAG_REPORT_DATA + ":  " + ReportData + "\n");
			sbReturn.Append(TAG_NOTES + ":  " + Notes + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of TestManager</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();	
			sbReturn.Append("<TestManager>\n");
			sbReturn.Append("<" + TAG_ID + ">" + TestManagerID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_TEMPLATE_ID + ">" + TestTemplateID + "</" + TAG_TEST_TEMPLATE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_LANGUAGE_ID + ">" + TestLanguageID + "</" + TAG_TEST_LANGUAGE_ID + ">\n");
			sbReturn.Append("<" + TAG_TEST_SUBJECT_ID + ">" + TestSubjectID + "</" + TAG_TEST_SUBJECT_ID + ">\n");
			sbReturn.Append("<" + TAG_SCREEN_ID + ">" + ScreenID + "</" + TAG_SCREEN_ID + ">\n");
			if (!dtNull.Equals(DateCreated))
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + ">" + DateCreated.ToString() + "</" + TAG_DATE_CREATED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_CREATED + "></" + TAG_DATE_CREATED + ">\n");
			}
			if (!dtNull.Equals(DateModified))
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + ">" + DateModified.ToString() + "</" + TAG_DATE_MODIFIED + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_MODIFIED + "></" + TAG_DATE_MODIFIED + ">\n");
			}
			if (!dtNull.Equals(DateBeginTest))
			{
				sbReturn.Append("<" + TAG_DATE_BEGIN_TEST + ">" + DateBeginTest.ToString() + "</" + TAG_DATE_BEGIN_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_BEGIN_TEST + "></" + TAG_DATE_BEGIN_TEST + ">\n");
			}
			if (!dtNull.Equals(DateEndTest))
			{
				sbReturn.Append("<" + TAG_DATE_END_TEST + ">" + DateEndTest.ToString() + "</" + TAG_DATE_END_TEST + ">\n");
			}
			else
			{
				sbReturn.Append("<" + TAG_DATE_END_TEST + "></" + TAG_DATE_END_TEST + ">\n");
			}
			sbReturn.Append("<" + TAG_REPORT_DATA + ">" + ReportData + "</" + TAG_REPORT_DATA + ">\n");
			sbReturn.Append("<" + TAG_NOTES + ">" + Notes + "</" + TAG_NOTES + ">\n");
			sbReturn.Append("</TestManager>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch 
			{
				_hasError = true;
				_errorCode = new ErrorCode();
			}
		}		
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				TestManagerID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_TEMPLATE_ID);
				TestTemplateID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestTemplateID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_LANGUAGE_ID);
				TestLanguageID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestLanguageID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TEST_SUBJECT_ID);
				TestSubjectID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			TestSubjectID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_SCREEN_ID);
				ScreenID = (long) Convert.ToInt32(xResultNode.InnerText);
			}
			catch  
			{
			ScreenID = 0;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_CREATED);
				DateCreated = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_MODIFIED);
				DateModified = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_BEGIN_TEST);
				DateBeginTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DATE_END_TEST);
				DateEndTest = DateTime.Parse(xResultNode.InnerText);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_REPORT_DATA);
				ReportData = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_NOTES);
				Notes = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
	}
}

//END OF TestManager CLASS FILE


