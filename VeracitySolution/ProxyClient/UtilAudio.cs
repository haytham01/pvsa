﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using TagLib;


namespace Veracity.Client.Proxy
{
    public class UtilAudio
    {
        public static int AudioDurationInSeconds(string FileFullPath)
        {
            int s_time = 0;
            try
            {
                TagLib.File file = TagLib.File.Create(FileFullPath);
                s_time = (int)file.Properties.Duration.TotalSeconds;
            }
            catch { s_time = 0; }
            return s_time;
        }
    }

   
}
