﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Veracity.Client.Proxy
{
    public class ImageHelper
    {
        public static Image ByteArrayToImage(byte[] myByteArray)
        {
            MemoryStream ms = new MemoryStream(myByteArray, 0, myByteArray.Length);
            ms.Write(myByteArray, 0, myByteArray.Length);
            return Image.FromStream(ms);
        }

        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }



        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();

            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }


        public static Stream ImageToStream(string pStrImagePath)
        {

            Image image = Image.FromFile(pStrImagePath);
            MemoryStream stream = new MemoryStream();
            // Save image to stream.
            image.Save(stream, ImageFormat.Bmp);
            return stream;

        }

        public ImageHelper()
        {
        }

        public static string ResizeImage(Stream pOriginalFileContent, string pStrUploadDirectory, int pIntSize)
        {
            string filename = null;


            // Create a bitmap of the content of the fileUpload control in memory
            Bitmap originalBMP = new Bitmap(pOriginalFileContent);

            int thumbnailSize = pIntSize;
            int newWidth, newHeight;
            if (originalBMP.Width > originalBMP.Height)
            {
                newWidth = thumbnailSize;
                newHeight = originalBMP.Height * thumbnailSize / originalBMP.Width;
            }

            else
            {
                newWidth = originalBMP.Width * thumbnailSize / originalBMP.Height;
                newHeight = thumbnailSize;
            }

            // Create a new bitmap which will hold the previous resized bitmap
            Bitmap newBMP = new Bitmap(originalBMP, newWidth, newWidth);

            // Create a graphic based on the new bitmap
            Graphics oGraphics = Graphics.FromImage(newBMP);
            // Set the properties for the new graphic file
            oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            // Draw the new graphic based on the resized bitmap
            oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newWidth);
            // Save the new graphic file to the server
            Guid g = Guid.NewGuid();
            filename = g.ToString() + "_" + newWidth + "x" + newWidth + ".bmp";

            newBMP.Save(pStrUploadDirectory + Path.DirectorySeparatorChar + filename);

            // Once finished with the bitmap objects, we deallocate them.
            originalBMP.Dispose();
            newBMP.Dispose();
            oGraphics.Dispose();
            return filename;
        }

        public static void ResizeImage(string OrigFile, string NewFile, int NewWidth, int MaxHeight, bool ResizeIfWider)
        {
            System.Drawing.Image FullSizeImage = System.Drawing.Image.FromFile(OrigFile);
            // Ensure the generated thumbnail is not being used by rotating it 360 degrees
            FullSizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            FullSizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
            if (ResizeIfWider)
            {
                if (FullSizeImage.Width <= NewWidth)
                {
                    NewWidth = FullSizeImage.Width;
                }
            }
            int NewHeight = FullSizeImage.Height * NewWidth / FullSizeImage.Width;
            if (NewHeight > MaxHeight) // Height resize if necessary
            {
                NewWidth = FullSizeImage.Width * MaxHeight / FullSizeImage.Height;
                NewHeight = MaxHeight;
            }
            // Create the new image with the sizes we've calculated
            System.Drawing.Image NewImage = FullSizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
            FullSizeImage.Dispose();
            NewImage.Save(NewFile);
        }

        public static string SaveImage(Stream pOriginalFileContent, string pStrUploadDirectory)
        {
            string filename = null;

            // Create a bitmap of the content of the fileUpload control in memory
            Bitmap originalBMP = new Bitmap(pOriginalFileContent);

            // Save the new graphic file to the server
            Guid g = Guid.NewGuid();
            filename = g.ToString() + ".bmp";

            originalBMP.Save(pStrUploadDirectory + Path.DirectorySeparatorChar + filename);

            // Once finished with the bitmap objects, we deallocate them.
            originalBMP.Dispose();
            originalBMP.Dispose();
            return filename;

        }

        public static byte[] byteFromPath(string pStrPath)
        {
            byte[] fileData = null;
            FileStream st = new FileStream(pStrPath, FileMode.Open);
            fileData = new byte[st.Length];
            st.Read(fileData, 0, (int)st.Length);
            st.Close();
            return fileData;
        }

        public static string ByteArrayToBase64(byte[] myByteArray)
        {
            string data = null;
            data = System.Convert.ToBase64String(myByteArray, 0, myByteArray.Length);
            return data;
        }

        public static byte[] Base64ToByteArray(string Base64String)
        {
            byte[] DecodedData = null;
            // Convert the base-64 string back to a byte array
            DecodedData = Convert.FromBase64String(Base64String);
            return DecodedData;
        }

        public static string Base64Decode(string data, string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate,
            FileAccess.Write);
            int sizeOfChunk = 4;
            int startPosition = 0;
            while (startPosition < data.Length)
            {
                string tmp = data.Substring(startPosition, sizeOfChunk);
                startPosition = startPosition + sizeOfChunk;
                byte[] tmpArr = Convert.FromBase64String(tmp);
                fs.Write(tmpArr, 0, tmpArr.Length);
            }
            fs.Close();
            fs.Dispose();
            return fileName;
        }
        public static string ByteArrayToHex(byte[] myByteArray)
        {
            StringBuilder hex = new StringBuilder(myByteArray.Length * 2);
            foreach (byte b in myByteArray)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static string ByteArrayToString(byte[] myByteArray)
        {
            string data = null;
            byte[] buf = Encoding.Convert(Encoding.GetEncoding("iso-8859-1"), Encoding.UTF8, myByteArray);
            data = Encoding.UTF8.GetString(buf, 0, buf.Length);
            return data;
        }

     
    }
}
