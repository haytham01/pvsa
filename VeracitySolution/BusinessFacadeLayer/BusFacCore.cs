﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;

using Veracity.Common;
using Veracity.BusinessAccessLayer;
using Veracity.DataAccessLayer.Data;
using Veracity.DataAccessLayer.Enumeration;

namespace Veracity.BusinessFacadeLayer
{
    public class BusFacCore
    {
        private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

        private PoolConnection _pool = null;
        private Config _config = null;
        private Logger _oLog = null;

        private string _strLognameText = "BusinessFacadeLayer-Core";

        //
        // connection pool name to get a connection from
        //
        private const string CONNECTION_POOL_NAME = "Main";

        public bool HasError
        {
            get { return _hasError; }
        }
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

        public BusFacCore()
        {
            _init();
        }

        private void _init()
        {
            try
            {
                _config = new Config();
                _oLog = new Logger(_strLognameText);

                _pool = PoolConnection.GetInstance(_config, CONNECTION_POOL_NAME);
                _log("INIT", "Successful initialization.");
            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
                _log("ERROR", e.StackTrace.ToString());
            }

        }

        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

        private SqlConnection getDBConnection()
        {
            SqlConnection conn = _pool.GetConnection();
            if (conn != null)
            {
                bool bConn = false;
                bConn = _pool.OpenConnection(conn);
                if (!bConn)
                {
                    // problem in opening the connection
                    // error
                    conn = null;
                    ErrorCode error = new ErrorCode();
                    _hasError = true;
                    _log("ERROR", error.ToString());
                }
            }
            return conn;
        }

        public bool isAuthAPI(string pStrApikeyGuid, string pStrAuthKey, ref Apikey pRefApikey)
        {
            bool bReturn = false;
            Apikey apikey = ApikeyGet(pStrApikeyGuid);
            if (apikey != null)
            {
                if ((apikey.AuthKey.Equals(pStrAuthKey, StringComparison.CurrentCultureIgnoreCase))
                    && (apikey.ClientID > 0))
                {
                    bReturn = true;
                    pRefApikey = apikey;
                }
            }
            return bReturn;
        }

        public Apikey ApikeyGet(string pStrApikeyGuid)
        {
            Apikey apikey = null;
            EnumApikey enumApikey = new EnumApikey();
            enumApikey.GuidStr = pStrApikeyGuid;
            ArrayList arApiKey = ApikeyGetList(enumApikey);
            if ( (arApiKey != null) && (arApiKey.Count == 1))
            {
                apikey = (Apikey)arApiKey[0];
            }
            return apikey;
        }

        public void ApikeysGet(ref ArrayList arItems)
        {
            EnumApikey enumApikey = new EnumApikey();
            arItems = ApikeyGetList(enumApikey);
        }

        public Apilog LogApi(long pLngRefNum, string pStrApikeyGuid, string pStrAuthKey, string pStrMsgSource, string pStrMsgText,
    string pStrTrace, bool pBlnIsSuccess, bool pBlnInProgress, string pStrHttpStatusStr, int pIntHttpStatusNum,
    string pStrReqtxt, string pStrResptxt)
        {
            Apikey apikey = ApikeyGet(pStrApikeyGuid);
            Apilog apilog = new Apilog();
            if (apikey != null)
            {
                apilog.ApikeyID = apikey.ApikeyID;
            }
            apilog.Msgsource = pStrMsgSource;
            apilog.Msgtxt = pStrMsgText;
            apilog.HttpStatusNum = pIntHttpStatusNum;
            apilog.HttpStatusStr = pStrHttpStatusStr;
            apilog.InProgress = pBlnInProgress;
            apilog.IsSuccess = pBlnIsSuccess;
            apilog.RefNum = pLngRefNum;
            apilog.Reqtxt = pStrReqtxt;
            apilog.Resptxt = pStrResptxt;
            apilog.Trace = pStrTrace;
            long ApilogID = ApilogCreateOrModify(apilog);
            return apilog;
        }


 


        /// <summary>
        /// ApikeyCreateOrModify
        /// </summary>
        /// <param name="">pApikey</param>
        /// <returns>long</returns>
        /// 
        public long ApikeyCreateOrModify(Apikey pApikey)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Apikey");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusApikey busApikey = null;
                busApikey = new BusApikey(conn, _config);
                busApikey.Save(pApikey);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pApikey.ApikeyID;
                _hasError = busApikey.HasError;
                if (busApikey.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// ApikeyGetList
        /// </summary>
        /// <param name="">pEnumApikey</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList ApikeyGetList(EnumApikey pEnumApikey)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Apikey");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusApikey busApikey = null;
                busApikey = new BusApikey(conn, _config);
                items = busApikey.Get(pEnumApikey);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busApikey.HasError;
                if (busApikey.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// ApikeyGet
        /// </summary>
        /// <param name="">pLngApikeyID</param>
        /// <returns>Apikey</returns>
        /// 
        public Apikey ApikeyGet(long pLngApikeyID)
        {
            ArrayList items = null;
            Apikey apikey = null;
            _log("GET", "Received Get Request Apikey");
            EnumApikey enumApikey = new EnumApikey();
            enumApikey.ApikeyID = pLngApikeyID;
            items = ApikeyGetList(enumApikey);
            if ((items != null) && (items.Count == 1))
            {
                apikey = (Apikey)items[0];
            }
            return apikey;
        }

        /// <summary>
        /// ApikeyRemove
        /// </summary>
        /// <param name="">pApikeyID</param>
        /// <returns>void</returns>
        /// 
        public void ApikeyRemove(long pApikeyID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Apikey");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Apikey apikey = new Apikey();
                apikey.ApikeyID = pApikeyID;
                BusApikey bus = null;
                bus = new BusApikey(conn, _config);
                bus.Delete(apikey);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }


        /// <summary>
        /// ApilogCreateOrModify
        /// </summary>
        /// <param name="">pApilog</param>
        /// <returns>long</returns>
        /// 
        public long ApilogCreateOrModify(Apilog pApilog)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Apilog");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusApilog busApilog = null;
                busApilog = new BusApilog(conn, _config);
                busApilog.Save(pApilog);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pApilog.ApilogID;
                _hasError = busApilog.HasError;
                if (busApilog.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// ApilogGetList
        /// </summary>
        /// <param name="">pEnumApilog</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList ApilogGetList(EnumApilog pEnumApilog)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Apilog");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusApilog busApilog = null;
                busApilog = new BusApilog(conn, _config);
                items = busApilog.Get(pEnumApilog);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busApilog.HasError;
                if (busApilog.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// ApilogGet
        /// </summary>
        /// <param name="">pLngApilogID</param>
        /// <returns>Apilog</returns>
        /// 
        public Apilog ApilogGet(long pLngApilogID)
        {
            ArrayList items = null;
            Apilog apilog = null;
            _log("GET", "Received Get Request Apilog");
            EnumApilog enumApilog = new EnumApilog();
            enumApilog.ApilogID = pLngApilogID;
            items = ApilogGetList(enumApilog);
            if ((items != null) && (items.Count == 1))
            {
                apilog = (Apilog)items[0];
            }
            return apilog;
        }

        /// <summary>
        /// ApilogRemove
        /// </summary>
        /// <param name="">pApilogID</param>
        /// <returns>void</returns>
        /// 
        public void ApilogRemove(long pApilogID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Apilog");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Apilog apilog = new Apilog();
                apilog.ApilogID = pApilogID;
                BusApilog bus = null;
                bus = new BusApilog(conn, _config);
                bus.Delete(apilog);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// AddressCreateOrModify
        /// </summary>
        /// <param name="">pAddress</param>
        /// <returns>long</returns>
        /// 
        public long AddressCreateOrModify(Address pAddress)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Address");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusAddress busAddress = null;
                busAddress = new BusAddress(conn, _config);
                busAddress.Save(pAddress);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pAddress.AddressID;
                _hasError = busAddress.HasError;
                if (busAddress.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// AddressGetList
        /// </summary>
        /// <param name="">pEnumAddress</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList AddressGetList(EnumAddress pEnumAddress)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Address");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusAddress busAddress = null;
                busAddress = new BusAddress(conn, _config);
                items = busAddress.Get(pEnumAddress);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busAddress.HasError;
                if (busAddress.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// AddressGet
        /// </summary>
        /// <param name="">pLngAddressID</param>
        /// <returns>Address</returns>
        /// 
        public Address AddressGet(long pLngAddressID)
        {
            ArrayList items = null;
            Address address = null;
            _log("GET", "Received Get Request Address");
            EnumAddress enumAddress = new EnumAddress();
            enumAddress.AddressID = pLngAddressID;
            items = AddressGetList(enumAddress);
            if ((items != null) && (items.Count == 1))
            {
                address = (Address)items[0];
            }
            return address;
        }

        /// <summary>
        /// AddressRemove
        /// </summary>
        /// <param name="">pAddressID</param>
        /// <returns>void</returns>
        /// 
        public void AddressRemove(long pAddressID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Address");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Address address = new Address();
                address.AddressID = pAddressID;
                BusAddress bus = null;
                bus = new BusAddress(conn, _config);
                bus.Delete(address);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// ClientCreateOrModify
        /// </summary>
        /// <param name="">pClient</param>
        /// <returns>long</returns>
        /// 
        public long ClientCreateOrModify(Client pClient)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Client");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusClient busClient = null;
                busClient = new BusClient(conn, _config);
                busClient.Save(pClient);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pClient.ClientID;
                _hasError = busClient.HasError;
                if (busClient.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// ClientGetList
        /// </summary>
        /// <param name="">pEnumClient</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList ClientGetList(EnumClient pEnumClient)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Client");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusClient busClient = null;
                busClient = new BusClient(conn, _config);
                items = busClient.Get(pEnumClient);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busClient.HasError;
                if (busClient.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// ClientGet
        /// </summary>
        /// <param name="">pLngClientID</param>
        /// <returns>Client</returns>
        /// 
        public Client ClientGet(long pLngClientID)
        {
            ArrayList items = null;
            Client client = null;
            _log("GET", "Received Get Request Client");
            EnumClient enumClient = new EnumClient();
            enumClient.ClientID = pLngClientID;
            items = ClientGetList(enumClient);
            if ((items != null) && (items.Count == 1))
            {
                client = (Client)items[0];
            }
            return client;
        }

        /// <summary>
        /// ClientRemove
        /// </summary>
        /// <param name="">pClientID</param>
        /// <returns>void</returns>
        /// 
        public void ClientRemove(long pClientID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Client");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Client client = new Client();
                client.ClientID = pClientID;
                BusClient bus = null;
                bus = new BusClient(conn, _config);
                bus.Delete(client);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// GlobalConfigCreateOrModify
        /// </summary>
        /// <param name="">pGlobalConfig</param>
        /// <returns>long</returns>
        /// 
        public long GlobalConfigCreateOrModify(GlobalConfig pGlobalConfig)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request GlobalConfig");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusGlobalConfig busGlobalConfig = null;
                busGlobalConfig = new BusGlobalConfig(conn, _config);
                busGlobalConfig.Save(pGlobalConfig);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pGlobalConfig.GlobalConfigID;
                _hasError = busGlobalConfig.HasError;
                if (busGlobalConfig.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// GlobalConfigGetList
        /// </summary>
        /// <param name="">pEnumGlobalConfig</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList GlobalConfigGetList(EnumGlobalConfig pEnumGlobalConfig)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request GlobalConfig");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusGlobalConfig busGlobalConfig = null;
                busGlobalConfig = new BusGlobalConfig(conn, _config);
                items = busGlobalConfig.Get(pEnumGlobalConfig);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busGlobalConfig.HasError;
                if (busGlobalConfig.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// GlobalConfigGet
        /// </summary>
        /// <param name="">pLngGlobalConfigID</param>
        /// <returns>GlobalConfig</returns>
        /// 
        public GlobalConfig GlobalConfigGet(long pLngGlobalConfigID)
        {
            ArrayList items = null;
            GlobalConfig globalconfig = null;
            _log("GET", "Received Get Request GlobalConfig");
            EnumGlobalConfig enumGlobalConfig = new EnumGlobalConfig();
            enumGlobalConfig.GlobalConfigID = pLngGlobalConfigID;
            items = GlobalConfigGetList(enumGlobalConfig);
            if ((items != null) && (items.Count == 1))
            {
                globalconfig = (GlobalConfig)items[0];
            }
            return globalconfig;
        }

        /// <summary>
        /// GlobalConfigRemove
        /// </summary>
        /// <param name="">pGlobalConfigID</param>
        /// <returns>void</returns>
        /// 
        public void GlobalConfigRemove(long pGlobalConfigID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request GlobalConfig");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                GlobalConfig globalconfig = new GlobalConfig();
                globalconfig.GlobalConfigID = pGlobalConfigID;
                BusGlobalConfig bus = null;
                bus = new BusGlobalConfig(conn, _config);
                bus.Delete(globalconfig);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// LnkTestTemplateScreenCreateOrModify
        /// </summary>
        /// <param name="">pLnkTestTemplateScreen</param>
        /// <returns>long</returns>
        /// 
        public long LnkTestTemplateScreenCreateOrModify(LnkTestTemplateScreen pLnkTestTemplateScreen)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request LnkTestTemplateScreen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusLnkTestTemplateScreen busLnkTestTemplateScreen = null;
                busLnkTestTemplateScreen = new BusLnkTestTemplateScreen(conn, _config);
                busLnkTestTemplateScreen.Save(pLnkTestTemplateScreen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pLnkTestTemplateScreen.LnkTestTemplateScreenID;
                _hasError = busLnkTestTemplateScreen.HasError;
                if (busLnkTestTemplateScreen.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// LnkTestTemplateScreenGetList
        /// </summary>
        /// <param name="">pEnumLnkTestTemplateScreen</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList LnkTestTemplateScreenGetList(EnumLnkTestTemplateScreen pEnumLnkTestTemplateScreen)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request LnkTestTemplateScreen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusLnkTestTemplateScreen busLnkTestTemplateScreen = null;
                busLnkTestTemplateScreen = new BusLnkTestTemplateScreen(conn, _config);
                items = busLnkTestTemplateScreen.Get(pEnumLnkTestTemplateScreen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busLnkTestTemplateScreen.HasError;
                if (busLnkTestTemplateScreen.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// LnkTestTemplateScreenGet
        /// </summary>
        /// <param name="">pLngLnkTestTemplateScreenID</param>
        /// <returns>LnkTestTemplateScreen</returns>
        /// 
        public LnkTestTemplateScreen LnkTestTemplateScreenGet(long pLngLnkTestTemplateScreenID)
        {
            ArrayList items = null;
            LnkTestTemplateScreen lnktesttemplatescreen = null;
            _log("GET", "Received Get Request LnkTestTemplateScreen");
            EnumLnkTestTemplateScreen enumLnkTestTemplateScreen = new EnumLnkTestTemplateScreen();
            enumLnkTestTemplateScreen.LnkTestTemplateScreenID = pLngLnkTestTemplateScreenID;
            items = LnkTestTemplateScreenGetList(enumLnkTestTemplateScreen);
            if ((items != null) && (items.Count == 1))
            {
                lnktesttemplatescreen = (LnkTestTemplateScreen)items[0];
            }
            return lnktesttemplatescreen;
        }

        /// <summary>
        /// LnkTestTemplateScreenRemove
        /// </summary>
        /// <param name="">pLnkTestTemplateScreenID</param>
        /// <returns>void</returns>
        /// 
        public void LnkTestTemplateScreenRemove(long pLnkTestTemplateScreenID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request LnkTestTemplateScreen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                LnkTestTemplateScreen lnktesttemplatescreen = new LnkTestTemplateScreen();
                lnktesttemplatescreen.LnkTestTemplateScreenID = pLnkTestTemplateScreenID;
                BusLnkTestTemplateScreen bus = null;
                bus = new BusLnkTestTemplateScreen(conn, _config);
                bus.Delete(lnktesttemplatescreen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// ScreenCreateOrModify
        /// </summary>
        /// <param name="">pScreen</param>
        /// <returns>long</returns>
        /// 
        public long ScreenCreateOrModify(Screen pScreen)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Screen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusScreen busScreen = null;
                busScreen = new BusScreen(conn, _config);
                busScreen.Save(pScreen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pScreen.ScreenID;
                _hasError = busScreen.HasError;
                if (busScreen.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// ScreenGetList
        /// </summary>
        /// <param name="">pEnumScreen</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList ScreenGetList(EnumScreen pEnumScreen)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Screen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusScreen busScreen = null;
                busScreen = new BusScreen(conn, _config);
                items = busScreen.Get(pEnumScreen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busScreen.HasError;
                if (busScreen.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// ScreenGet
        /// </summary>
        /// <param name="">pLngScreenID</param>
        /// <returns>Screen</returns>
        /// 
        public Screen ScreenGet(long pLngScreenID)
        {
            ArrayList items = null;
            Screen screen = null;
            _log("GET", "Received Get Request Screen");
            EnumScreen enumScreen = new EnumScreen();
            enumScreen.ScreenID = pLngScreenID;
            items = ScreenGetList(enumScreen);
            if ((items != null) && (items.Count == 1))
            {
                screen = (Screen)items[0];
            }
            return screen;
        }

        /// <summary>
        /// ScreenRemove
        /// </summary>
        /// <param name="">pScreenID</param>
        /// <returns>void</returns>
        /// 
        public void ScreenRemove(long pScreenID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Screen");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Screen screen = new Screen();
                screen.ScreenID = pScreenID;
                BusScreen bus = null;
                bus = new BusScreen(conn, _config);
                bus.Delete(screen);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// ScreenTypeCreateOrModify
        /// </summary>
        /// <param name="">pScreenType</param>
        /// <returns>long</returns>
        /// 
        public long ScreenTypeCreateOrModify(ScreenType pScreenType)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request ScreenType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusScreenType busScreenType = null;
                busScreenType = new BusScreenType(conn, _config);
                busScreenType.Save(pScreenType);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pScreenType.ScreenTypeID;
                _hasError = busScreenType.HasError;
                if (busScreenType.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// ScreenTypeGetList
        /// </summary>
        /// <param name="">pEnumScreenType</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList ScreenTypeGetList(EnumScreenType pEnumScreenType)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request ScreenType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusScreenType busScreenType = null;
                busScreenType = new BusScreenType(conn, _config);
                items = busScreenType.Get(pEnumScreenType);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busScreenType.HasError;
                if (busScreenType.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// ScreenTypeGet
        /// </summary>
        /// <param name="">pLngScreenTypeID</param>
        /// <returns>ScreenType</returns>
        /// 
        public ScreenType ScreenTypeGet(long pLngScreenTypeID)
        {
            ArrayList items = null;
            ScreenType screentype = null;
            _log("GET", "Received Get Request ScreenType");
            EnumScreenType enumScreenType = new EnumScreenType();
            enumScreenType.ScreenTypeID = pLngScreenTypeID;
            items = ScreenTypeGetList(enumScreenType);
            if ((items != null) && (items.Count == 1))
            {
                screentype = (ScreenType)items[0];
            }
            return screentype;
        }

        /// <summary>
        /// ScreenTypeRemove
        /// </summary>
        /// <param name="">pScreenTypeID</param>
        /// <returns>void</returns>
        /// 
        public void ScreenTypeRemove(long pScreenTypeID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request ScreenType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                ScreenType screentype = new ScreenType();
                screentype.ScreenTypeID = pScreenTypeID;
                BusScreenType bus = null;
                bus = new BusScreenType(conn, _config);
                bus.Delete(screentype);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// TestLanguageCreateOrModify
        /// </summary>
        /// <param name="">pTestLanguage</param>
        /// <returns>long</returns>
        /// 
        public long TestLanguageCreateOrModify(TestLanguage pTestLanguage)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request TestLanguage");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestLanguage busTestLanguage = null;
                busTestLanguage = new BusTestLanguage(conn, _config);
                busTestLanguage.Save(pTestLanguage);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pTestLanguage.TestLanguageID;
                _hasError = busTestLanguage.HasError;
                if (busTestLanguage.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// TestLanguageGetList
        /// </summary>
        /// <param name="">pEnumTestLanguage</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList TestLanguageGetList(EnumTestLanguage pEnumTestLanguage)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request TestLanguage");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestLanguage busTestLanguage = null;
                busTestLanguage = new BusTestLanguage(conn, _config);
                items = busTestLanguage.Get(pEnumTestLanguage);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busTestLanguage.HasError;
                if (busTestLanguage.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// TestLanguageGet
        /// </summary>
        /// <param name="">pLngTestLanguageID</param>
        /// <returns>TestLanguage</returns>
        /// 
        public TestLanguage TestLanguageGet(long pLngTestLanguageID)
        {
            ArrayList items = null;
            TestLanguage testlanguage = null;
            _log("GET", "Received Get Request TestLanguage");
            EnumTestLanguage enumTestLanguage = new EnumTestLanguage();
            enumTestLanguage.TestLanguageID = pLngTestLanguageID;
            items = TestLanguageGetList(enumTestLanguage);
            if ((items != null) && (items.Count == 1))
            {
                testlanguage = (TestLanguage)items[0];
            }
            return testlanguage;
        }

        /// <summary>
        /// TestLanguageRemove
        /// </summary>
        /// <param name="">pTestLanguageID</param>
        /// <returns>void</returns>
        /// 
        public void TestLanguageRemove(long pTestLanguageID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request TestLanguage");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                TestLanguage testlanguage = new TestLanguage();
                testlanguage.TestLanguageID = pTestLanguageID;
                BusTestLanguage bus = null;
                bus = new BusTestLanguage(conn, _config);
                bus.Delete(testlanguage);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// TestManagerCreateOrModify
        /// </summary>
        /// <param name="">pTestManager</param>
        /// <returns>long</returns>
        /// 
        public long TestManagerCreateOrModify(TestManager pTestManager)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request TestManager");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestManager busTestManager = null;
                busTestManager = new BusTestManager(conn, _config);
                busTestManager.Save(pTestManager);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pTestManager.TestManagerID;
                _hasError = busTestManager.HasError;
                if (busTestManager.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// TestManagerGetList
        /// </summary>
        /// <param name="">pEnumTestManager</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList TestManagerGetList(EnumTestManager pEnumTestManager)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request TestManager");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestManager busTestManager = null;
                busTestManager = new BusTestManager(conn, _config);
                items = busTestManager.Get(pEnumTestManager);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busTestManager.HasError;
                if (busTestManager.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// TestManagerGet
        /// </summary>
        /// <param name="">pLngTestManagerID</param>
        /// <returns>TestManager</returns>
        /// 
        public TestManager TestManagerGet(long pLngTestManagerID)
        {
            ArrayList items = null;
            TestManager testmanager = null;
            _log("GET", "Received Get Request TestManager");
            EnumTestManager enumTestManager = new EnumTestManager();
            enumTestManager.TestManagerID = pLngTestManagerID;
            items = TestManagerGetList(enumTestManager);
            if ((items != null) && (items.Count == 1))
            {
                testmanager = (TestManager)items[0];
            }
            return testmanager;
        }

        /// <summary>
        /// TestManagerRemove
        /// </summary>
        /// <param name="">pTestManagerID</param>
        /// <returns>void</returns>
        /// 
        public void TestManagerRemove(long pTestManagerID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request TestManager");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                TestManager testmanager = new TestManager();
                testmanager.TestManagerID = pTestManagerID;
                BusTestManager bus = null;
                bus = new BusTestManager(conn, _config);
                bus.Delete(testmanager);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// TestSubjectCreateOrModify
        /// </summary>
        /// <param name="">pTestSubject</param>
        /// <returns>long</returns>
        /// 
        public long TestSubjectCreateOrModify(TestSubject pTestSubject)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request TestSubject");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestSubject busTestSubject = null;
                busTestSubject = new BusTestSubject(conn, _config);
                busTestSubject.Save(pTestSubject);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pTestSubject.TestSubjectID;
                _hasError = busTestSubject.HasError;
                if (busTestSubject.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// TestSubjectGetList
        /// </summary>
        /// <param name="">pEnumTestSubject</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList TestSubjectGetList(EnumTestSubject pEnumTestSubject)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request TestSubject");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestSubject busTestSubject = null;
                busTestSubject = new BusTestSubject(conn, _config);
                items = busTestSubject.Get(pEnumTestSubject);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busTestSubject.HasError;
                if (busTestSubject.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// TestSubjectGet
        /// </summary>
        /// <param name="">pLngTestSubjectID</param>
        /// <returns>TestSubject</returns>
        /// 
        public TestSubject TestSubjectGet(long pLngTestSubjectID)
        {
            ArrayList items = null;
            TestSubject testsubject = null;
            _log("GET", "Received Get Request TestSubject");
            EnumTestSubject enumTestSubject = new EnumTestSubject();
            enumTestSubject.TestSubjectID = pLngTestSubjectID;
            items = TestSubjectGetList(enumTestSubject);
            if ((items != null) && (items.Count == 1))
            {
                testsubject = (TestSubject)items[0];
            }
            return testsubject;
        }

        /// <summary>
        /// TestSubjectRemove
        /// </summary>
        /// <param name="">pTestSubjectID</param>
        /// <returns>void</returns>
        /// 
        public void TestSubjectRemove(long pTestSubjectID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request TestSubject");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                TestSubject testsubject = new TestSubject();
                testsubject.TestSubjectID = pTestSubjectID;
                BusTestSubject bus = null;
                bus = new BusTestSubject(conn, _config);
                bus.Delete(testsubject);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// TestTemplateCreateOrModify
        /// </summary>
        /// <param name="">pTestTemplate</param>
        /// <returns>long</returns>
        /// 
        public long TestTemplateCreateOrModify(TestTemplate pTestTemplate)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request TestTemplate");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestTemplate busTestTemplate = null;
                busTestTemplate = new BusTestTemplate(conn, _config);
                busTestTemplate.Save(pTestTemplate);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pTestTemplate.TestTemplateID;
                _hasError = busTestTemplate.HasError;
                if (busTestTemplate.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// TestTemplateGetList
        /// </summary>
        /// <param name="">pEnumTestTemplate</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList TestTemplateGetList(EnumTestTemplate pEnumTestTemplate)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request TestTemplate");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestTemplate busTestTemplate = null;
                busTestTemplate = new BusTestTemplate(conn, _config);
                items = busTestTemplate.Get(pEnumTestTemplate);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busTestTemplate.HasError;
                if (busTestTemplate.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// TestTemplateGet
        /// </summary>
        /// <param name="">pLngTestTemplateID</param>
        /// <returns>TestTemplate</returns>
        /// 
        public TestTemplate TestTemplateGet(long pLngTestTemplateID)
        {
            ArrayList items = null;
            TestTemplate testtemplate = null;
            _log("GET", "Received Get Request TestTemplate");
            EnumTestTemplate enumTestTemplate = new EnumTestTemplate();
            enumTestTemplate.TestTemplateID = pLngTestTemplateID;
            items = TestTemplateGetList(enumTestTemplate);
            if ((items != null) && (items.Count == 1))
            {
                testtemplate = (TestTemplate)items[0];
            }
            return testtemplate;
        }

        /// <summary>
        /// TestTemplateRemove
        /// </summary>
        /// <param name="">pTestTemplateID</param>
        /// <returns>void</returns>
        /// 
        public void TestTemplateRemove(long pTestTemplateID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request TestTemplate");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                TestTemplate testtemplate = new TestTemplate();
                testtemplate.TestTemplateID = pTestTemplateID;
                BusTestTemplate bus = null;
                bus = new BusTestTemplate(conn, _config);
                bus.Delete(testtemplate);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// TestTypeCreateOrModify
        /// </summary>
        /// <param name="">pTestType</param>
        /// <returns>long</returns>
        /// 
        public long TestTypeCreateOrModify(TestType pTestType)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request TestType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestType busTestType = null;
                busTestType = new BusTestType(conn, _config);
                busTestType.Save(pTestType);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pTestType.TestTypeID;
                _hasError = busTestType.HasError;
                if (busTestType.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// TestTypeGetList
        /// </summary>
        /// <param name="">pEnumTestType</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList TestTypeGetList(EnumTestType pEnumTestType)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request TestType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusTestType busTestType = null;
                busTestType = new BusTestType(conn, _config);
                items = busTestType.Get(pEnumTestType);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busTestType.HasError;
                if (busTestType.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// TestTypeGet
        /// </summary>
        /// <param name="">pLngTestTypeID</param>
        /// <returns>TestType</returns>
        /// 
        public TestType TestTypeGet(long pLngTestTypeID)
        {
            ArrayList items = null;
            TestType testtype = null;
            _log("GET", "Received Get Request TestType");
            EnumTestType enumTestType = new EnumTestType();
            enumTestType.TestTypeID = pLngTestTypeID;
            items = TestTypeGetList(enumTestType);
            if ((items != null) && (items.Count == 1))
            {
                testtype = (TestType)items[0];
            }
            return testtype;
        }

        /// <summary>
        /// TestTypeRemove
        /// </summary>
        /// <param name="">pTestTypeID</param>
        /// <returns>void</returns>
        /// 
        public void TestTypeRemove(long pTestTypeID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request TestType");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                TestType testtype = new TestType();
                testtype.TestTypeID = pTestTypeID;
                BusTestType bus = null;
                bus = new BusTestType(conn, _config);
                bus.Delete(testtype);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }

        /// <summary>
        /// MerchantCreateOrModify
        /// </summary>
        /// <param name="">pMerchant</param>
        /// <returns>long</returns>
        /// 
        public long MerchantCreateOrModify(Merchant pMerchant)
        {
            long lID = 0;
            bool bConn = false;
            _log("CREATE", "Received Create Request Merchant");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusMerchant busMerchant = null;
                busMerchant = new BusMerchant(conn, _config);
                busMerchant.Save(pMerchant);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                lID = pMerchant.MerchantID;
                _hasError = busMerchant.HasError;
                if (busMerchant.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return lID;
        }

        /// <summary>
        /// MerchantGetList
        /// </summary>
        /// <param name="">pEnumMerchant</param>
        /// <returns>ArrayList</returns>
        /// 
        public ArrayList MerchantGetList(EnumMerchant pEnumMerchant)
        {
            ArrayList items = null;
            bool bConn = false;
            _log("CREATE", "Received Get List Request Merchant");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                BusMerchant busMerchant = null;
                busMerchant = new BusMerchant(conn, _config);
                items = busMerchant.Get(pEnumMerchant);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = busMerchant.HasError;
                if (busMerchant.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
            return items;
        }

        /// <summary>
        /// MerchantGet
        /// </summary>
        /// <param name="">pLngMerchantID</param>
        /// <returns>Merchant</returns>
        /// 
        public Merchant MerchantGet(long pLngMerchantID)
        {
            ArrayList items = null;
            Merchant merchant = null;
            _log("GET", "Received Get Request Merchant");
            EnumMerchant enumMerchant = new EnumMerchant();
            enumMerchant.MerchantID = pLngMerchantID;
            items = MerchantGetList(enumMerchant);
            if ((items != null) && (items.Count == 1))
            {
                merchant = (Merchant)items[0];
            }
            return merchant;
        }

        /// <summary>
        /// MerchantRemove
        /// </summary>
        /// <param name="">pMerchantID</param>
        /// <returns>void</returns>
        /// 
        public void MerchantRemove(long pMerchantID)
        {
            bool bConn = false;

            _log("REMOVE", "Received Remove Request Merchant");
            SqlConnection conn = getDBConnection();
            if (conn != null)
            {
                Merchant merchant = new Merchant();
                merchant.MerchantID = pMerchantID;
                BusMerchant bus = null;
                bus = new BusMerchant(conn, _config);
                bus.Delete(merchant);
                // close the db connection
                bConn = _pool.CloseConnection(conn);
                _hasError = bus.HasError;
                if (bus.HasError)
                {
                    // error
                    ErrorCode error = new ErrorCode();
                    _log("ERROR", error.ToString());
                }
            }
        }
    }
}
