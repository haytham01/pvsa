﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Veracity.Client.Proxy;
using System.Drawing.Printing;
using System.IO;
using System.Globalization;



namespace VeracityViewerTest
{
    public partial class VeracityViewerTestForm : Form
    {

        public VeracityViewerTestForm()
        {
            InitializeComponent();

            numericUpDownTSLAdjust.Value = 40;
            numericUpDownTSLAdjust.Maximum = 99;
            numericUpDownTSLAdjust.Minimum = 0;
        }

        
        PrintPreviewDialog objPrev;
        PrintDocument objDoc;
        Pen pen = new Pen(Color.Black);
        Brush brsh = new SolidBrush(Color.Black);
        TestSession globalTestSession = new TestSession ();
        AnalyzerKOTQuestions globalAnaylzerKOT = new AnalyzerKOTQuestions();
        Anaylyzer26Questions globalAnaylzer26Q = new Anaylyzer26Questions();
        Anaylyzer18Questions globalAnaylzer18Q = new Anaylyzer18Questions();
        int globalAutoAdjustSet26Q = 0;
        int globalAutoAdjustSet24Q = 0;
        int globalAutoAdjustSet18Q = 0;
        int globalAutoAdjustSet15Q = 0;
        int globalAutoAdjustSetKOT = 0;
        bool bGlobalFirstSetOfDataLoaded = false;
        bool bGlobalKOTDataParsed = false;
        bool bGlobal15QDataParsed = false;
        bool bGlobal18QDataParsed = false;
        bool bGlobal24QDataParsed = false;
        bool bGlobal26QDataParsed = false;

        // test another another

        //private bool _b24QPrintPreview = true;
        //private bool _b15QPrintPreview = true;
        //private bool _bKOTPrintPreview = true;

        string sWatchFile = "VERACITY-OUTPUT1.CSV";
        string sPath = null;


        private bool _b26QPrintPreview = true;
        public bool b26QPrintPreview
        {
            get { return _b26QPrintPreview; }
            set { _b26QPrintPreview = value; }
        }
        private bool _b24QPrintPreview = true;
        public bool b24QPrintPreview
        {
            get { return _b24QPrintPreview; }
            set { _b24QPrintPreview = value; }
        }
        private bool _b18QPrintPreview = true;
        public bool b18QPrintPreview
        {
            get { return _b18QPrintPreview; }
            set { _b18QPrintPreview = value; }
        }
        private bool _b15QPrintPreview = true;
        public bool b15QPrintPreview
        {
            get { return _b15QPrintPreview; }
            set { _b15QPrintPreview = value; }
        }

        private bool _bKOTPrintPreview = true;
        public bool bKOTPrintPreview
        {
            get { return _bKOTPrintPreview; }
            set { _bKOTPrintPreview = value; }
        }


        private bool Parse15QCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen
                
                


                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                             // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds
                    
                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ( (i <= 1) || (i>92)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {

                                //MessageBox.Show("TEXT=" + TotalQuestion);
                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "ADAPT 1";
                                        screen.ScreenTextShort = "Citizen Of US";
                                        break;
                                    case 2:
                                        screen.ScreenText = "ADAPT 2";
                                        screen.ScreenTextShort = "Reside In US";
                                        break;
                                    case 3:
                                        screen.ScreenText = "BASE 1";
                                        screen.ScreenTextShort = "Born In US";
                                        break;
                                    case 4:
                                        screen.ScreenText = "BASE 2";
                                        screen.ScreenTextShort = "Traveling with Adult";
                                        break;
                                    case 5:
                                        screen.ScreenText = "BASE 3";
                                        screen.ScreenTextShort = "Traveling with Child";
                                        break;
                                    case 6:
                                        screen.ScreenText = "CONTROL 1";
                                        screen.ScreenTextShort = "Weapons in Carry on";
                                        break;
                                    case 7:
                                        screen.ScreenText = "CONTROL 2";
                                        screen.ScreenTextShort = "Weapons in Baggage";
                                        break;
                                    case 8:
                                        screen.ScreenText = "CONTROL 3";
                                        screen.ScreenTextShort = "Weapons on Person";
                                        break;
                                    case 9:
                                        screen.ScreenText = "SECONDARY 1";
                                        screen.ScreenTextShort = "Smugglers Associated with";
                                        break;
                                    case 10:
                                        screen.ScreenText = "SECONDARY 2";
                                        screen.ScreenTextShort = "Smuggling Directly Involved";
                                        break;
                                    case 11:
                                        screen.ScreenText = "SECONDARY 3";
                                        screen.ScreenTextShort = "Smuggling Unknown Issue";
                                        break;
                                    case 12:
                                        screen.ScreenText = "PRIMARY 1";
                                        screen.ScreenTextShort = "Terrorism Associated with";
                                        break;
                                    case 13:
                                        screen.ScreenText = "PRIMARY 2";
                                        screen.ScreenTextShort = "Terrorism Directly Involved";
                                        break;
                                    case 14:
                                        screen.ScreenText = "PRIMARY 3";
                                        screen.ScreenTextShort = "Terrorism Unknown Issue";
                                        break;
                                    case 15:
                                        screen.ScreenText = "EXIT";
                                        screen.ScreenTextShort = "Taken Survey Before";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");
                            

                            switch (col) {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:
                                    
                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}",(startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }              
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch ( Exception ex )
            {
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false ;
            }

            return bFoundFile;
        }


        private void BtnLoadCsv_Click(object sender, EventArgs e)
        {
            textBoxReactionDataAvg6.Text = "";
            textBoxReactionDataAvg7.Text = "";
            textBoxReactionDataAvg8.Text = "";
            textBoxReactionData.Text = "Reaction Data";
            Load15QData();
        }

        private void Load15QData()
        {

            groupBoxAverages.Text = "Reaction Data AVG";
            groupBoxAccidents.Text = "Traveling";
            groupBoxIllegalDrugs.Text = "Weapons";
            groupBoxSexOffenders.Text = "Smuggling";
            groupBoxArrest.Text = "Terrorism";
            groupBoxSmuggling.Text = "Exit";
            

            groupBoxAccidents.Show();
            groupBoxAdopt.Show();
            groupBoxArrest.Show();
            groupBoxDistort.Show();
            groupBoxExit.Hide();
            groupBoxIllegalDrugs.Show();
            groupBoxReadRatio.Show();
            groupBoxSexOffenders.Show();
            groupBoxSmuggling.Show();
            groupBoxTerrorism.Hide();
            groupBoxUnknowns.Hide();
            groupBoxZones.Show();

            // groupBoxSmuggling.Text = "";
            textBoxQuestion16.Text = "";
            textBoxIssues16.Text = "";
            textBoxQuestion17.Text = "";
            textBoxIssues17.Text = "";
            textBoxReactionData16.Text = "";
            textBoxReactionData17.Text = "";
            textBoxAnswers16.Text = "";
            textBoxAnswers17.Text = "";
            textBoxReadTime16.Text = "";
            textBoxReadTime17.Text = "";
            textBoxReaction16.Text = "";
            textBoxReaction17.Text = "";


            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // MessageBox.Show(openFileDialog1.FileName);
                sWatchFile = openFileDialog1.FileName;
            }
            

            TestSession testSession = new TestSession();

            // Populate some data for the DLL file to test with
            Veracity.Client.Proxy.Screen screen = null;


            TestSubject subject = new TestSubject();
            subject.FirstName = "Julious";
            subject.LastName = "Gazman";
            subject.DriverLicenseNumber = "A123456789";
            testSession.objTestSubject = subject;


            if (Parse15QCSV(ref testSession))
            {
                //MessageBox.Show("Finish Loading CSV file...");
            }
            else // if no files found either quit or populate with default data
            {
                MessageBox.Show("File=" + sWatchFile + " not found.  Using default data");



                // Question 1
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 1;
                screen.ScreenText = "ADAPT 1";
                screen.ScreenTextShort = "ADAPT 1";
                screen.Answer = true;
                screen.ReadTimeInSeconds = .210302;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:06.9940544");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:10.2487344");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("3.464982");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 2
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 2;
                screen.ScreenText = "ADAPT 2";
                screen.ScreenTextShort = "ADAPT 2";
                screen.Answer = true;
                screen.ReadTimeInSeconds = .210302;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.623773");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 3
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 3;
                screen.ScreenText = "BASE 1";
                screen.ScreenTextShort = "BASE 1";
                screen.Answer = true;
                screen.ReadTimeInSeconds = 0.200288;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:14.2845376");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:16.5477920");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.463542");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 4
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 4;
                screen.ScreenText = "BASE 2";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.190274;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:17.3489440");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:22.7467056");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.588035");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 5
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 5;
                screen.ScreenText = "BASE 3";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.180259;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:23.5378432");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:28.8254464");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.467862");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 6
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 6;
                screen.ScreenText = "CONTROL 1";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.25036;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:29.6065696");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:42.0544688");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.698259");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 7
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 7;
                screen.ScreenText = "CONTROL 2";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.210302;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:42.9056928");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:56.5152624");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("13.819872");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 8
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 8;
                screen.ScreenText = "CONTROL 3";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.240346;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:57.3264288");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:02.8844208");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.798338");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 9
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 9;
                screen.ScreenText = "SECONDARY 1";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.270389;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010  11:19:08.3322544");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:19:22.8431200");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("14.781254");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 10
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 10;
                screen.ScreenText = "SECONDARY 2";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.190274;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010  11:19:23.7143728");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:19:26.7988080");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("3.274709");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 11
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 11;
                screen.ScreenText = "SECONDARY 3";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.240346;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:27.5899456");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:33.7488016");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("6.399202");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 12
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 12;
                screen.ScreenText = "PRIMARY 1";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.240346;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:39.1966352");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:51.985024");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("13.028734");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 13
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 13;
                screen.ScreenText = "PRIMARY 2";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.220317;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:52.8262336");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:58.7647728");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("6.158856");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 14
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 14;
                screen.ScreenText = "PRIMARY 3";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.190274;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:19:59.5859536");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:20:14.6375968");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("15.241917");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 15
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = 15;
                screen.ScreenText = "EXIT";
                screen.Answer = false;
                screen.ReadTimeInSeconds = 0.170245;
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:20:15.4287344");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:20:20.2156176");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.957128");
                testSession.lstScreenQuestionOnly.Add(screen);
            }


            ////////////////////////////////////////////////////////////////
            ////
            //// CALCULATIONS BEGIN
            ////
            ////////////////////////////////////////////////////////////////
            Analyzer15Questions analyzer = new Analyzer15Questions();
            analyzer.dTSLAdjust15Q = Convert.ToDouble(numericUpDownTSLAdjust.Value);
            
            if (globalAutoAdjustSet15Q == 0)
            {
                analyzer.AnalysisTest15Questions(ref testSession);
            }
            else
            {
                analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                analyzer.AnalysisTest15AutoAdjust(ref testSession);
            }




            //////analyzer.CalculateRDTimeT15(ref testSession);

            ////////for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dRDTime[" + i + "]=" + analyzer.dRDTime[i]);
            ////////}

            //////analyzer.CalculateRDTimeAvgT15(ref testSession);
            ////////for (int i = 0; i <= 7 ; i++)
            ////////{
            ////////    MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            ////////}


            //////analyzer.CalculateE(ref testSession);
            ////////for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dE[" + i + "]=" + analyzer.dE[i]);
            ////////}


            //////analyzer.CalculateI(ref testSession);
            ////////for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dI[" + i + "]=" + analyzer.dI[i]);
            ////////}

            //////analyzer.CalculateJ();
            ////////for (int i = 1; i <= testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dJ[" + i + "]=" + analyzer.dJ[i]);
            ////////}

            //////analyzer.CalculateK();
            ////////for (int i = 1; i <= testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dK[" + i + "]=" + analyzer.dK[i]);
            ////////}

            //////analyzer.CalculateL();
            ////////for (int i = 1; i <= testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dL[" + i + "]=" + analyzer.dL[i]);
            ////////}

            //////analyzer.CalculateM();
            ////////for (int i = 1; i <= testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dM[" + i + "]=" + analyzer.dM[i]);
            ////////}

            //////analyzer.CalculateN();
            ////////for (int i = 1; i <= testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("dN[" + i + "]=" + analyzer.dN[i]);
            ////////}

            //////analyzer.CalculateHiddenG();
            //////analyzer.CalculateHiddenH(); 
            //////analyzer.CalculateHiddenI();
            //////analyzer.CalculateHiddenJ(ref testSession);
            //////analyzer.CalculateHiddenK();
            //////analyzer.CalculateReaction(ref testSession);

            ////////for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            ////////{
            ////////    MessageBox.Show("HIT[" + i + "]=" + testSession.lstScreen[i].IsHit);
            ////////}




            //////analyzer.getStandardDeviation();


            ////////////////////////////////////////////////////////////////
            ////
            //// POPULATE FORMS BEGIN
            ////
            ////////////////////////////////////////////////////////////////

            // Populate the QUESTIONS and NUMBERS
            screen = null;
            string sAnswer = "NO";
            string sReaction = null;
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // Print either a YES or NO for the answer
                if (screen.Answer)
                {
                    sAnswer = "YES";
                }
                else
                {
                    sAnswer = "NO";
                }

                // Print either a HIT, NO, or null
                sReaction = null;
                if (screen.IsHit == true)
                {
                    sReaction = "SPR";
                }
                if (screen.Answer)
                {
                    sReaction = "ADMIT";
                }
                if (screen.IsHit == false)
                {
                    sReaction = "";
                }

                

                
                switch (i)
                {
                    case 0:
                        //
                        // if we are at question 1 and 2, then nothing for the reaction
                        //
                        sReaction = "";
                        textBoxIssues1.Text = screen.ScreenText;
                        textBoxQuestion1.Text = Convert.ToString(i + 1);
                        textBoxAnswers1.Text = sAnswer;
                        textBoxReactionData1.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime1.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction1.Text = sReaction;
                        break;
                    case 1:
                        //
                        // if we are at question 1 and 2, then nothing for the reaction
                        //
                        sReaction = "";
                        textBoxIssues2.Text = screen.ScreenText;
                        textBoxQuestion2.Text = Convert.ToString(i + 1);
                        textBoxAnswers2.Text = sAnswer;
                        textBoxReactionData2.Text = Convert.ToString(analyzer.dRDTime[i]) ;
                        textBoxReadTime2.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction2.Text = sReaction;
                        break;
                    case 2:
                        textBoxIssues3.Text = screen.ScreenText;
                        textBoxQuestion3.Text = Convert.ToString(i + 1);
                        textBoxAnswers3.Text = sAnswer;
                        textBoxReactionData3.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime3.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction3.Text = sReaction;
                        break;
                    case 3:
                        textBoxIssues4.Text = screen.ScreenText;
                        textBoxQuestion4.Text = Convert.ToString(i + 1);
                        textBoxAnswers4.Text = sAnswer;
                        textBoxReactionData4.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime4.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction4.Text = sReaction;
                        break;
                    case 4:
                        textBoxIssues5.Text = screen.ScreenText;
                        textBoxQuestion5.Text = Convert.ToString(i + 1);
                        textBoxAnswers5.Text = sAnswer;
                        textBoxReactionData5.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime5.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction5.Text = sReaction;
                        break;
                    case 5:
                        textBoxIssues6.Text = screen.ScreenText;
                        textBoxQuestion6.Text = Convert.ToString(i + 1);
                        textBoxAnswers6.Text = sAnswer;
                        textBoxReactionData6.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime6.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction6.Text = sReaction;
                        break;
                    case 6:
                        textBoxIssues7.Text = screen.ScreenText;
                        textBoxQuestion7.Text = Convert.ToString(i + 1);
                        textBoxAnswers7.Text = sAnswer;
                        textBoxReactionData7.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime7.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction7.Text = sReaction;
                        break;
                    case 7:
                        textBoxIssues8.Text = screen.ScreenText;
                        textBoxQuestion8.Text = Convert.ToString(i + 1);
                        textBoxAnswers8.Text = sAnswer;
                        textBoxReactionData8.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime8.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction8.Text = sReaction;
                        break;
                    case 8:
                        textBoxIssues9.Text = screen.ScreenText;
                        textBoxQuestion9.Text = Convert.ToString(i + 1);
                        textBoxAnswers9.Text = sAnswer;
                        textBoxReactionData9.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime9.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction9.Text = sReaction;
                        break;
                    case 9:
                        textBoxIssues10.Text = screen.ScreenText;
                        textBoxQuestion10.Text = Convert.ToString(i + 1);
                        textBoxAnswers10.Text = sAnswer;
                        textBoxReactionData10.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime10.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction10.Text = sReaction;
                        break;
                    case 10:
                        textBoxIssues11.Text = screen.ScreenText;
                        textBoxQuestion11.Text = Convert.ToString(i + 1);
                        textBoxAnswers11.Text = sAnswer;
                        textBoxReactionData11.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime11.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction11.Text = sReaction;
                        break;
                    case 11:
                        textBoxIssues12.Text = screen.ScreenText;
                        textBoxQuestion12.Text = Convert.ToString(i + 1);
                        textBoxAnswers12.Text = sAnswer;
                        textBoxReactionData12.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime12.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction12.Text = sReaction;
                        break;
                    case 12:
                        textBoxIssues13.Text = screen.ScreenText;
                        textBoxQuestion13.Text = Convert.ToString(i + 1);
                        textBoxAnswers13.Text = sAnswer;
                        textBoxReactionData13.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime13.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction13.Text = sReaction;
                        break;
                    case 13:
                        textBoxIssues14.Text = screen.ScreenText;
                        textBoxQuestion14.Text = Convert.ToString(i + 1);
                        textBoxAnswers14.Text = sAnswer;
                        textBoxReactionData14.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime14.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction14.Text = sReaction;
                        break;
                    case 14:
                        textBoxIssues15.Text = screen.ScreenText;
                        textBoxQuestion15.Text = Convert.ToString(i + 1);
                        textBoxAnswers15.Text = sAnswer;
                        textBoxReactionData15.Text = Convert.ToString(analyzer.dRDTime[i]);
                        textBoxReadTime15.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction15.Text = sReaction;
                        break;
                    default:
                        MessageBox.Show("Unknown case!");
                        break;

                }
            }

            for (int i = 0; i <= 7; i++)
            {
                switch (i)
                {
                    case 0:
                        textBoxReactionDataAvg1.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 1:
                        textBoxReactionDataAvg2.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 2:
                        textBoxReactionDataAvg3.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 3:
                        textBoxReactionDataAvg4.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 4:
                        textBoxReactionDataAvg5.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 5: // Dont use this - Calculate ALL Average
                        //textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 6:
                        textBoxReactionDataAvg13.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));                   
                        break;
                    case 7:
                        textBoxReactionDataAvg14.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    default:
                        break;
                }
                
                //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            }

            // Standard Deviation
            textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            // Read Reaction and the Distortion
            textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            if (analyzer.Distortion)
            {
                textBoxDistortion1.Text = "Likely";
            }
            else
            {
                textBoxDistortion1.Text = "NO";
            }

            // Setup the zones and update the TSL if needed
            textBoxZone1.Text = Convert.ToString(analyzer.ReactionZone1Counter);
            textBoxZone2.Text = Convert.ToString(analyzer.ReactionZone2Counter);
            textBoxZone3.Text = Convert.ToString(analyzer.ReactionZone3Counter);
            textBoxZone4.Text = Convert.ToString(analyzer.ReactionZone4Counter);
            textBoxZone5.Text = Convert.ToString(analyzer.ReactionZone5Counter);
            textBoxZone6.Text = Convert.ToString(analyzer.ReactionZone6Counter);
            textBoxZone7.Text = Convert.ToString(analyzer.ReactionZone7Counter);
            txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;


            // then formulate the print report
            // then send to printer

            ////////////////////////////////////////////////////////////
            ///
            /// PRINT preview for the 15 questions
            ///
            ////////////////////////////////////////////////////////////
            globalTestSession = testSession; // keep a copy if we need to print it

            // MessageBox.Show("Avg RD Time:  " + analyzer.listRDTime[0]);
            



            // test is going to be populated
            //Veracity.Client.Proxy.Screen screen = new Veracity.Client.Proxy.Screen();
            //screen.ReadTimeInSeconds = 5.0;
            //screen.FlowOrderIndex = 1;
            //testSession.lstScreenQuestionOnly.Add(screen);

            //screen = new Veracity.Client.Proxy.Screen();
            //screen.ReadTimeInSeconds = 15.0;
            //screen.FlowOrderIndex = 2;
            //testSession.lstScreenQuestionOnly.Add(screen);

            //Analyzer analyzer = new Analyzer();
            //analyzer.AnalysisTest15Questions(ref testSession);

            // then formulate the print report
            // then send to printer

            //MessageBox.Show("Avg RD Time:  " + analyzer.lstAvgs[0]);
        }

        private void textBoxIssues1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxReadTime1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxReactionData1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxAnswers1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxQuestion1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxReaction1_TextChanged(object sender, EventArgs e)
        {

        }

        private void VeracityViewerTestForm_Load(object sender, EventArgs e)
        {

        }


        ////////////////////////////////////////////////////////////
        ///
        /// 24 QUESTIONS
        ///
        ////////////////////////////////////////////////////////////

        private bool Parse24QCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen




                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 146)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {


                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "ADAPT 1";
                                        screen.ScreenTextShort = "ADAPT 1";
                                        break;
                                    case 2:
                                        screen.ScreenText = "ADAPT 2";
                                        screen.ScreenTextShort = "ADAPT 2";
                                        break;
                                    case 3:
                                        screen.ScreenText = "Had accident";
                                        screen.ScreenTextShort = "Had accident";
                                        break;
                                    case 4:
                                        screen.ScreenText = "Failed to report accident";
                                        screen.ScreenTextShort = "Failed to report accident";
                                        break;
                                    case 5:
                                        screen.ScreenText = "Caused Hit & Run accident";
                                        screen.ScreenTextShort = "Caused Hit & Run accident";
                                        break;
                                    case 6:
                                        screen.ScreenText = "Used M/J in last 12 months";
                                        screen.ScreenTextShort = "Used M/J in last 12 months";
                                        break;
                                    case 7:
                                        screen.ScreenText = "Used Drugs in last 3 mos.";
                                        screen.ScreenTextShort = "Used Drugs in last 3 mos.";
                                        break;
                                    case 8:
                                        screen.ScreenText = "Used Drugs in last 30 days";
                                        screen.ScreenTextShort = "Used Drugs in last 30 days";
                                        break;
                                    case 9:
                                        screen.ScreenText = "Committed Sex Crime";
                                        screen.ScreenTextShort = "Committed Sex Crime";
                                        break;
                                    case 10:
                                        screen.ScreenText = "Convicted of Sex Crime";
                                        screen.ScreenTextShort = "Convicted of Sex Crime";
                                        break;
                                    case 11:
                                        screen.ScreenText = "Registered Sex Offender";
                                        screen.ScreenTextShort = "Registered Sex Offender";
                                        break;
                                    case 12:
                                        screen.ScreenText = "Arrest or Convict Traffic Vio";
                                        screen.ScreenTextShort = "Arrest or Convict Traffic Vio";
                                        break;
                                    case 13:
                                        screen.ScreenText = "Arrest or Convict Misdem";
                                        screen.ScreenTextShort = "Arrest or Convict Misdem";
                                        break;
                                    case 14:
                                        screen.ScreenText = "Arrest or Convict Felony";
                                        screen.ScreenTextShort = "Arrest or Convict Felony";
                                        break;
                                    case 15:
                                        screen.ScreenText = "Smuggling associated";
                                        screen.ScreenTextShort = "Associated or affiliated with anyone involved in committing acts of smuggling?";
                                        break;
                                    case 16:
                                        screen.ScreenText = "Smuggling directly involved";
                                        screen.ScreenTextShort = "Smuggling directly involved";
                                        break;
                                    case 17:
                                        screen.ScreenText = "Smuggling in any manner";
                                        screen.ScreenTextShort = "Smuggling in any manner";
                                        break;
                                    case 18:
                                        screen.ScreenText = "Terrorism associated";
                                        screen.ScreenTextShort = "Terrorism associated";
                                        break;
                                    case 19:
                                        screen.ScreenText = "Terrorism directly involved";
                                        screen.ScreenTextShort = "Terrorism directly involved";
                                        break;
                                    case 20:
                                        screen.ScreenText = "Terrorism in any manner";
                                        screen.ScreenTextShort = "Terrorism in any manner";
                                        break;
                                    case 21:
                                        screen.ScreenText = "Unknown Issue";
                                        screen.ScreenTextShort = "Unknown Issue";
                                        break;
                                    case 22:
                                        screen.ScreenText = "Is there anything in your background that you are very concerned?";
                                        screen.ScreenTextShort = "Is there anything in your background that you are very concerned?";
                                        break;
                                    case 23:
                                        screen.ScreenText = "Did you lie to any question";
                                        screen.ScreenTextShort = "Did you lie to any question";
                                        break;
                                    case 24:
                                        screen.ScreenText = "Taken Test before";
                                        screen.ScreenTextShort = "Taken Test before";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    // screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:

                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    // screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false;
            }

            return bFoundFile;
        }
        private void btnLoad24Data_Click(object sender, EventArgs e)
        {
            textBoxReactionData.Text = "SUM";
            Load24Data();
            bGlobalFirstSetOfDataLoaded = true;
            bGlobal24QDataParsed = true;
            bGlobal15QDataParsed = false;
            bGlobalKOTDataParsed = false;
            //Calculate24Data();
        }

        private void Update24Data()
        {
            TestSession testSession = globalTestSession;
            int iQuestionNumber = 1;

            // Populate some data for the DLL file to test with
            Veracity.Client.Proxy.Screen screen = null;

            TestSubject subject = new TestSubject();
            subject.FirstName = "Mark";
            subject.LastName = "Anthony";
            subject.DriverLicenseNumber = "A123456789";

            testSession.objTestSubject = subject;

            ////////////////////////////////////////////////////////////////
            ////
            //// CALCULATIONS BEGIN
            ////
            ////////////////////////////////////////////////////////////////
            Anaylyzer24Questions analyzer = new Anaylyzer24Questions();

            if (globalAutoAdjustSet24Q == 0)
            {
                //Anaylyzer24Questions analyzer = new Anaylyzer24Questions();
                analyzer.AnalysisTest24Questions(ref testSession);
            }
            else
            {
                //Anaylyzer24Questions analyzer = new Anaylyzer24Questions();
                analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                analyzer.AnalysisTest24AutoAdjust(ref testSession);
            }


            //for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            //{
            //    MessageBox.Show("DeltaTimeInSeconds" + testSession.lstScreen[i].DeltaClickTimeInSeconds);
            //}



            ////////////////////////////////////////////////////////////////
            ////
            //// POPULATE FORMS BEGIN
            ////
            ////////////////////////////////////////////////////////////////

            // Populate the QUESTIONS and NUMBERS
            screen = null;
            string sAnswer = "NO";
            string sReaction = null;
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // Print either a YES or NO for the answer
                if (screen.Answer)
                {
                    sAnswer = "YES";
                }
                else
                {
                    sAnswer = "NO";
                }

                // Print either a HIT, NO, or null
                sReaction = null;
                if (screen.IsHit == true)
                {
                    sReaction = "SPR";
                }
                if (screen.Answer)
                {
                    sReaction = "ADMIT";
                }
                if (screen.IsHit == false)
                {
                    sReaction = "";
                }

                switch (i)
                {
                    case 0:
                        sReaction = "";
                        //textBoxIssues1.Text = screen.ScreenText;
                        //textBoxQuestion1.Text = Convert.ToString(i + 1);
                        //textBoxAnswers1.Text = sAnswer;
                        textBoxReactionData1.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime1.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction1.Text = sReaction;
                        break;
                    case 1:
                        sReaction = "";
                        //textBoxIssues2.Text = screen.ScreenText;
                        //textBoxQuestion2.Text = Convert.ToString(i + 1);
                        //textBoxAnswers2.Text = sAnswer;
                        textBoxReactionData2.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime2.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction2.Text = sReaction;
                        break;
                    case 2:
                        //textBoxIssues3.Text = screen.ScreenText;
                        //textBoxQuestion3.Text = Convert.ToString(i + 1);
                        //textBoxAnswers3.Text = sAnswer;
                        textBoxReactionData3.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime3.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction3.Text = sReaction;
                        break;
                    case 3:
                        //textBoxIssues4.Text = screen.ScreenText;
                        //textBoxQuestion4.Text = Convert.ToString(i + 1);
                        //textBoxAnswers4.Text = sAnswer;
                        textBoxReactionData4.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime4.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction4.Text = sReaction;
                        break;
                    case 4:
                        //textBoxIssues5.Text = screen.ScreenText;
                        //textBoxQuestion5.Text = Convert.ToString(i + 1);
                        //textBoxAnswers5.Text = sAnswer;
                        textBoxReactionData5.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime5.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction5.Text = sReaction;
                        break;
                    case 5:
                        //textBoxIssues6.Text = screen.ScreenText;
                        //textBoxQuestion6.Text = Convert.ToString(i + 1);
                        //textBoxAnswers6.Text = sAnswer;
                        textBoxReactionData6.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime6.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction6.Text = sReaction;
                        break;
                    case 6:
                        //textBoxIssues7.Text = screen.ScreenText;
                        //textBoxQuestion7.Text = Convert.ToString(i + 1);
                        //textBoxAnswers7.Text = sAnswer;
                        textBoxReactionData7.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime7.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction7.Text = sReaction;
                        break;
                    case 7:
                        //textBoxIssues8.Text = screen.ScreenText;
                        //textBoxQuestion8.Text = Convert.ToString(i + 1);
                        //textBoxAnswers8.Text = sAnswer;
                        textBoxReactionData8.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime8.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction8.Text = sReaction;
                        break;
                    case 8:
                        //textBoxIssues9.Text = screen.ScreenText;
                        //textBoxQuestion9.Text = Convert.ToString(i + 1);
                        //textBoxAnswers9.Text = sAnswer;
                        textBoxReactionData9.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime9.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction9.Text = sReaction;
                        break;
                    case 9:
                        //textBoxIssues10.Text = screen.ScreenText;
                        //textBoxQuestion10.Text = Convert.ToString(i + 1);
                        //textBoxAnswers10.Text = sAnswer;
                        textBoxReactionData10.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime10.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction10.Text = sReaction;
                        break;
                    case 10:
                        //textBoxIssues11.Text = screen.ScreenText;
                        //textBoxQuestion11.Text = Convert.ToString(i + 1);
                        //textBoxAnswers11.Text = sAnswer;
                        textBoxReactionData11.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime11.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction11.Text = sReaction;
                        break;
                    case 11:
                        //textBoxIssues12.Text = screen.ScreenText;
                        //textBoxQuestion12.Text = Convert.ToString(i + 1);
                        //textBoxAnswers12.Text = sAnswer;
                        textBoxReactionData12.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime12.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction12.Text = sReaction;
                        break;
                    case 12:
                        //textBoxIssues13.Text = screen.ScreenText;
                        //textBoxQuestion13.Text = Convert.ToString(i + 1);
                        //textBoxAnswers13.Text = sAnswer;
                        textBoxReactionData13.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime13.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction13.Text = sReaction;
                        break;
                    case 13:
                        //textBoxIssues14.Text = screen.ScreenText;
                        //textBoxQuestion14.Text = Convert.ToString(i + 1);
                        //textBoxAnswers14.Text = sAnswer;
                        textBoxReactionData14.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime14.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction14.Text = sReaction;
                        break;
                    case 14:
                        //textBoxIssues15.Text = screen.ScreenText;
                        //textBoxQuestion15.Text = Convert.ToString(i + 1);
                        //textBoxAnswers15.Text = sAnswer;
                        textBoxReactionData15.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime15.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction15.Text = sReaction;
                        break;
                    case 15:
                        //textBoxIssues16.Text = screen.ScreenText;
                        //textBoxQuestion16.Text = Convert.ToString(i + 1);
                        //textBoxAnswers16.Text = sAnswer;
                        textBoxReactionData16.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime16.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction16.Text = sReaction;
                        break;
                    case 16:
                        //textBoxIssues17.Text = screen.ScreenText;
                        //textBoxQuestion17.Text = Convert.ToString(i + 1);
                        //textBoxAnswers17.Text = sAnswer;
                        textBoxReactionData17.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime17.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction17.Text = sReaction;
                        break;
                    case 17:
                        //textBoxIssues18.Text = screen.ScreenText;
                        //textBoxQuestion18.Text = Convert.ToString(i + 1);
                        //textBoxAnswers18.Text = sAnswer;
                        textBoxReactionData18.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime18.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction18.Text = sReaction;
                        break;
                    case 18:
                        //textBoxIssues19.Text = screen.ScreenText;
                        //textBoxQuestion19.Text = Convert.ToString(i + 1);
                        //textBoxAnswers19.Text = sAnswer;
                        textBoxReactionData19.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime19.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction19.Text = sReaction;
                        break;
                    case 19:
                        //textBoxIssues20.Text = screen.ScreenText;
                        //textBoxQuestion20.Text = Convert.ToString(i + 1);
                        //textBoxAnswers20.Text = sAnswer;
                        textBoxReactionData20.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime20.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction20.Text = sReaction;
                        break;
                    case 20:
                        //textBoxIssues21.Text = screen.ScreenText;
                        //textBoxQuestion21.Text = Convert.ToString(i + 1);
                        //textBoxAnswers21.Text = sAnswer;
                        textBoxReactionData21.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime21.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction21.Text = sReaction;
                        break;
                    case 21:
                        //textBoxIssues22.Text = screen.ScreenText;
                        //textBoxQuestion22.Text = Convert.ToString(i + 1);
                        //textBoxAnswers22.Text = sAnswer;
                        textBoxReactionData22.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime22.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction22.Text = sReaction;
                        break;
                    case 22:
                        //textBoxIssues23.Text = screen.ScreenText;
                        //textBoxQuestion23.Text = Convert.ToString(i + 1);
                        //textBoxAnswers23.Text = sAnswer;
                        textBoxReactionData23.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime23.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction23.Text = sReaction;
                        break;
                    case 23:
                        //textBoxIssues24.Text = screen.ScreenText;
                        //textBoxQuestion24.Text = Convert.ToString(i + 1);
                        //textBoxAnswers24.Text = sAnswer;
                        textBoxReactionData24.Text = Convert.ToString(analyzer.dRDTime[i]) + " E(" + Convert.ToString(analyzer.dSumEI[i]) + ")";
                        //textBoxReadTime24.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction24.Text = sReaction;
                        break;
                    default:
                        MessageBox.Show("Unknown case!");
                        break;

                }
            }

            for (int i = 0; i <= 10; i++)
            {
                switch (i)
                {
                    case 0:
                        textBoxReactionDataAvg1.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 1:
                        textBoxReactionDataAvg2.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 2:
                        textBoxReactionDataAvg3.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 3:
                        textBoxReactionDataAvg4.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 4:
                        textBoxReactionDataAvg5.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 5: // Dont use this - Calculate ALL Average
                        //textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 6:
                        textBoxReactionDataAvg13.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 7:
                        textBoxReactionDataAvg14.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 8:
                        textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 9:
                        textBoxReactionDataAvg7.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 10:
                        textBoxReactionDataAvg8.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    default:
                        break;
                }

                //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            }

            // Standard Deviation
            textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            // Read Reaction and the Distortion
            textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            if (analyzer.Distortion)
            {
                textBoxDistortion1.Text = "Likely";
            }
            else
            {
                textBoxDistortion1.Text = "NO";
            }

            // Setup the zones and update the TSL if needed
            textBoxZone1.Text = "Count(" + Convert.ToString(analyzer.ReactionZone1Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[0]) + ")";
            textBoxZone2.Text = "Count(" + Convert.ToString(analyzer.ReactionZone2Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[1]) + ")";
            textBoxZone3.Text = "Count(" + Convert.ToString(analyzer.ReactionZone3Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[2]) + ")";
            textBoxZone4.Text = "Count(" + Convert.ToString(analyzer.ReactionZone4Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[3]) + ")";
            textBoxZone5.Text = "Count(" + Convert.ToString(analyzer.ReactionZone5Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[4]) + ")";
            textBoxZone6.Text = "Count(" + Convert.ToString(analyzer.ReactionZone6Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[5]) + ")";
            textBoxZone7.Text = "Count(" + Convert.ToString(analyzer.ReactionZone7Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[6]) + ")";
            txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;

            ////////////////////////////////////////////////////////////
            ///
            /// PRINT preview for the 24 questions
            ///
            ////////////////////////////////////////////////////////////
            globalTestSession = testSession; // keep a copy if we need to print it


        }

        private void Load24Data()
        {
            groupBoxAverages.Text = "Summary Averages";
            groupBoxAccidents.Text = "Accidents";
            groupBoxAdopt.Text = "Adopt";
            groupBoxArrest.Text = "Arrest";
            groupBoxDistort.Text = "Distort";
            groupBoxExit.Text = "Exit";
            groupBoxIllegalDrugs.Text = "Illegal Drugs";
            groupBoxReadRatio.Text = "Read Ratio";
            groupBoxSexOffenders.Text = "Sex Offenders";
            groupBoxSmuggling.Text = "Smuggling";
            groupBoxTerrorism.Text = "Terrorism";
            groupBoxUnknowns.Text = "Unknowns";
            groupBoxZones.Text = "Zones";

            groupBoxAccidents.Show();
            groupBoxAdopt.Show();
            groupBoxArrest.Show();
            groupBoxDistort.Show();
            groupBoxExit.Show();
            groupBoxIllegalDrugs.Show();
            groupBoxReadRatio.Show();
            groupBoxSexOffenders.Show();
            groupBoxSmuggling.Show();
            groupBoxTerrorism.Show();
            groupBoxUnknowns.Show();
            groupBoxZones.Show();




                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                // Set filter options and filter index.
                openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
                openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.Multiselect = false;

                // Call the ShowDialog method to show the dialog box.
                DialogResult result = openFileDialog1.ShowDialog();

                // Process input if the user clicked OK.
                if (result == DialogResult.OK)
                {
                    // MessageBox.Show(openFileDialog1.FileName);
                    sWatchFile = openFileDialog1.FileName;
                }


                TestSession testSession = new TestSession();
                int iQuestionNumber = 1;

                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                TestSubject subject = new TestSubject();
                subject.FirstName = "Mark";
                subject.LastName = "Anthony";
                subject.DriverLicenseNumber = "A123456789";

                testSession.objTestSubject = subject;

                //testSession.objSubjectIdentification.FirstName = "Mark";
                //testSession.objSubjectIdentification.LastName = "Anthony";
                //testSession.objSubjectIdentification.DriverLicenseNumber = "A1875590";


                if (Parse24QCSV(ref testSession))
                {
                    //MessageBox.Show("Finish Loading CSV file...");
                }
                else // if no files found either quit or populate with default data
                {
                    MessageBox.Show("File=" + sWatchFile + " not found.  Using default data");

                    // Question 1
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "ADAPT 1";
                    screen.Answer = true;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".0901");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:06.9940544");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:10.2487344");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.3");
                    screen.ZAxisData = Convert.ToDouble("9.00");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 2
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "ADAPT 2";
                    screen.Answer = true;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                    screen.ZAxisData = Convert.ToDouble("11.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.2");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 3
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Had accident";
                    screen.Answer = true;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                    screen.ZAxisData = Convert.ToDouble("20.23");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 4
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Failed to report accident";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                    screen.ZAxisData = Convert.ToDouble("15.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 5
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Caused Hit & Run accident";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                    screen.ZAxisData = Convert.ToDouble("10.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                    testSession.lstScreenQuestionOnly.Add(screen);



                    // Question 6
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Used M/J in last 12 months";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3001");
                    screen.ZAxisData = Convert.ToDouble("14.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("23.9");
                    testSession.lstScreenQuestionOnly.Add(screen);



                    // Question 7
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Used Drugs in last 3 mos.";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                    screen.ZAxisData = Convert.ToDouble("25.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 8
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Used Drugs in last 30 days";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                    screen.ZAxisData = Convert.ToDouble("27.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 9
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Committed Sex Crime";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                    screen.ZAxisData = Convert.ToDouble("27.40");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 10
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Convicted of Sex Crime";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                    screen.ZAxisData = Convert.ToDouble("35.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.0");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 11
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Registered Sex Offender";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                    screen.ZAxisData = Convert.ToDouble("19.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.0");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 12
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Arrest or Convict Traffic Vio";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2903");
                    screen.ZAxisData = Convert.ToDouble("13.34");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("13.0");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 13
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Arrest or Convict Misdem";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2504");
                    screen.ZAxisData = Convert.ToDouble("24.30");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("14.0");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 14
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Arrest or Convict Felony";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2804");
                    screen.ZAxisData = Convert.ToDouble("25.23");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("15.0");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 15
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Smuggling associated";
                    screen.Answer = true;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                    screen.ZAxisData = Convert.ToDouble("20.23");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 16
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Smuggling directly involved";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                    screen.ZAxisData = Convert.ToDouble("15.0");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 17
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Smuggling in any manner";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                    screen.ZAxisData = Convert.ToDouble("10.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 18
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Terrorism associated";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".2501");
                    screen.ZAxisData = Convert.ToDouble("14.0");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("3.9");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 19
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Terrorism directly involved";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                    screen.ZAxisData = Convert.ToDouble("25.0");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 20
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Terrorism in any manner";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                    screen.ZAxisData = Convert.ToDouble("27.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 21
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Unknown Issue";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                    screen.ZAxisData = Convert.ToDouble("27.40");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 22
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    //screen.ScreenText = "Did you withhold information";
                    screen.ScreenText = "Is there anything in your background that you are very concerned?";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                    screen.ZAxisData = Convert.ToDouble("35.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.00");
                    testSession.lstScreenQuestionOnly.Add(screen);

                    // Question 23
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Did you lie to any question";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                    screen.ZAxisData = Convert.ToDouble("19.00");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.00");
                    testSession.lstScreenQuestionOnly.Add(screen);


                    // Question 24
                    screen = new Veracity.Client.Proxy.Screen();
                    screen.FlowOrderIndex = iQuestionNumber++;
                    screen.ScreenText = "Taken Test before";
                    screen.Answer = false;
                    screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                    screen.ZAxisData = Convert.ToDouble("13.34");
                    screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                    screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                    screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.3");
                    testSession.lstScreenQuestionOnly.Add(screen);
                }



            ////////////////////////////////////////////////////////////////
            ////
            //// CALCULATIONS BEGIN
            ////
            ////////////////////////////////////////////////////////////////
            Anaylyzer24Questions analyzer = new Anaylyzer24Questions();
            analyzer.dTSLAdjust24Q = Convert.ToDouble(numericUpDownTSLAdjust.Value);

            //MessageBox.Show("numericUpDownTSLAdjust=" + analyzer.dTSLAdjust24Q.ToString());

            if (globalAutoAdjustSet24Q == 0)
            {
                //Anaylyzer24Questions analyzer = new Anaylyzer24Questions();
                analyzer.AnalysisTest24Questions(ref testSession);
            }
            else
            {
                //Anaylyzer24Questions analyzer = new Anaylyzer24Questions();
                analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                analyzer.AnalysisTest24AutoAdjust(ref testSession);
            }

            
            //for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            //{
            //    MessageBox.Show("DeltaTimeInSeconds" + testSession.lstScreen[i].DeltaClickTimeInSeconds);
            //}



            ////////////////////////////////////////////////////////////////
            ////
            //// POPULATE FORMS BEGIN
            ////
            ////////////////////////////////////////////////////////////////

            // Populate the QUESTIONS and NUMBERS
            screen = null;
            string sAnswer = "NO";
            string sReaction = null;
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // Print either a YES or NO for the answer
                if (screen.Answer)
                {
                    sAnswer = "YES";
                }
                else
                {
                    sAnswer = "NO";
                }

                // Print either a HIT, NO, or null
                sReaction = null;
                if (screen.IsHit == true)
                {
                    sReaction = "SPR";
                }
                if (screen.Answer)
                {
                    sReaction = "ADMIT";
                }
                if (screen.IsHit == false)
                {
                    sReaction = "";
                }

                switch (i)
                {
                    case 0:
                        sReaction = "";
                        textBoxIssues1.Text = screen.ScreenText;
                        textBoxQuestion1.Text = Convert.ToString(i + 1);
                        textBoxAnswers1.Text = sAnswer;
                        textBoxReactionData1.Text = Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime1.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction1.Text = sReaction;
                        break;
                    case 1:
                        sReaction = "";
                        textBoxIssues2.Text = screen.ScreenText;
                        textBoxQuestion2.Text = Convert.ToString(i + 1);
                        textBoxAnswers2.Text = sAnswer;
                        textBoxReactionData2.Text =  Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime2.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction2.Text = sReaction;
                        break;
                    case 2:
                        textBoxIssues3.Text = screen.ScreenText;
                        textBoxQuestion3.Text = Convert.ToString(i + 1);
                        textBoxAnswers3.Text = sAnswer;
                        textBoxReactionData3.Text =  Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime3.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction3.Text = sReaction;
                        break;
                    case 3:
                        textBoxIssues4.Text = screen.ScreenText;
                        textBoxQuestion4.Text = Convert.ToString(i + 1);
                        textBoxAnswers4.Text = sAnswer;
                        textBoxReactionData4.Text =  Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime4.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction4.Text = sReaction;
                        break;
                    case 4:
                        textBoxIssues5.Text = screen.ScreenText;
                        textBoxQuestion5.Text = Convert.ToString(i + 1);
                        textBoxAnswers5.Text = sAnswer;
                        textBoxReactionData5.Text = Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime5.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction5.Text = sReaction;
                        break;
                    case 5:
                        textBoxIssues6.Text = screen.ScreenText;
                        textBoxQuestion6.Text = Convert.ToString(i + 1);
                        textBoxAnswers6.Text = sAnswer;
                        textBoxReactionData6.Text = Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime6.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction6.Text = sReaction;
                        break;
                    case 6:
                        textBoxIssues7.Text = screen.ScreenText;
                        textBoxQuestion7.Text = Convert.ToString(i + 1);
                        textBoxAnswers7.Text = sAnswer;
                        textBoxReactionData7.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime7.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction7.Text = sReaction;
                        break;
                    case 7:
                        textBoxIssues8.Text = screen.ScreenText;
                        textBoxQuestion8.Text = Convert.ToString(i + 1);
                        textBoxAnswers8.Text = sAnswer;
                        textBoxReactionData8.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime8.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction8.Text = sReaction;
                        break;
                    case 8:
                        textBoxIssues9.Text = screen.ScreenText;
                        textBoxQuestion9.Text = Convert.ToString(i + 1);
                        textBoxAnswers9.Text = sAnswer;
                        textBoxReactionData9.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime9.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction9.Text = sReaction;
                        break;
                    case 9:
                        textBoxIssues10.Text = screen.ScreenText;
                        textBoxQuestion10.Text = Convert.ToString(i + 1);
                        textBoxAnswers10.Text = sAnswer;
                        textBoxReactionData10.Text =  Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime10.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction10.Text = sReaction;
                        break;
                    case 10:
                        textBoxIssues11.Text = screen.ScreenText;
                        textBoxQuestion11.Text = Convert.ToString(i + 1);
                        textBoxAnswers11.Text = sAnswer;
                        textBoxReactionData11.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime11.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction11.Text = sReaction;
                        break;
                    case 11:
                        textBoxIssues12.Text = screen.ScreenText;
                        textBoxQuestion12.Text = Convert.ToString(i + 1);
                        textBoxAnswers12.Text = sAnswer;
                        textBoxReactionData12.Text = Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime12.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction12.Text = sReaction;
                        break;
                    case 12:
                        textBoxIssues13.Text = screen.ScreenText;
                        textBoxQuestion13.Text = Convert.ToString(i + 1);
                        textBoxAnswers13.Text = sAnswer;
                        textBoxReactionData13.Text =  Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime13.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction13.Text = sReaction;
                        break;
                    case 13:
                        textBoxIssues14.Text = screen.ScreenText;
                        textBoxQuestion14.Text = Convert.ToString(i + 1);
                        textBoxAnswers14.Text = sAnswer;
                        textBoxReactionData14.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime14.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction14.Text = sReaction;
                        break;
                    case 14:
                        textBoxIssues15.Text = screen.ScreenText;
                        textBoxQuestion15.Text = Convert.ToString(i + 1);
                        textBoxAnswers15.Text = sAnswer;
                        textBoxReactionData15.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime15.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction15.Text = sReaction;
                        break;
                    case 15:
                        textBoxIssues16.Text = screen.ScreenText;
                        textBoxQuestion16.Text = Convert.ToString(i + 1);
                        textBoxAnswers16.Text = sAnswer;
                        textBoxReactionData16.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime16.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction16.Text = sReaction;
                        break;
                    case 16:
                        textBoxIssues17.Text = screen.ScreenText;
                        textBoxQuestion17.Text = Convert.ToString(i + 1);
                        textBoxAnswers17.Text = sAnswer;
                        textBoxReactionData17.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime17.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction17.Text = sReaction;
                        break;
                    case 17:
                        textBoxIssues18.Text = screen.ScreenText;
                        textBoxQuestion18.Text = Convert.ToString(i + 1);
                        textBoxAnswers18.Text = sAnswer;
                        textBoxReactionData18.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime18.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction18.Text = sReaction;
                        break;
                    case 18:
                        textBoxIssues19.Text = screen.ScreenText;
                        textBoxQuestion19.Text = Convert.ToString(i + 1);
                        textBoxAnswers19.Text = sAnswer;
                        textBoxReactionData19.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime19.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction19.Text = sReaction;
                        break;
                    case 19:
                        textBoxIssues20.Text = screen.ScreenText;
                        textBoxQuestion20.Text = Convert.ToString(i + 1);
                        textBoxAnswers20.Text = sAnswer;
                        textBoxReactionData20.Text = Convert.ToString(analyzer.dSumEI[i]);
                        textBoxReadTime20.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction20.Text = sReaction;
                        break;
                    case 20:
                        textBoxIssues21.Text = screen.ScreenText;
                        textBoxQuestion21.Text = Convert.ToString(i + 1);
                        textBoxAnswers21.Text = sAnswer;
                        textBoxReactionData21.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime21.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction21.Text = sReaction;
                        break;
                    case 21:
                        textBoxIssues22.Text = screen.ScreenText;
                        textBoxQuestion22.Text = Convert.ToString(i + 1);
                        textBoxAnswers22.Text = sAnswer;
                        textBoxReactionData22.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime22.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction22.Text = sReaction;
                        break;
                    case 22:
                        textBoxIssues23.Text = screen.ScreenText;
                        textBoxQuestion23.Text = Convert.ToString(i + 1);
                        textBoxAnswers23.Text = sAnswer;
                        textBoxReactionData23.Text = Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime23.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction23.Text = sReaction;
                        break;
                    case 23:
                        textBoxIssues24.Text = screen.ScreenText;
                        textBoxQuestion24.Text = Convert.ToString(i + 1);
                        textBoxAnswers24.Text = sAnswer;
                        textBoxReactionData24.Text =  Convert.ToString(analyzer.dSumEI[i]) ;
                        textBoxReadTime24.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                        textBoxReaction24.Text = sReaction;
                        break;
                    default:
                        MessageBox.Show("Unknown case!");
                        break;

                }
            }

            for (int i = 0; i <= 10; i++)
            {
                switch (i)
                {
                    case 0:
                        textBoxReactionDataAvg1.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 1:
                        textBoxReactionDataAvg2.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 2:
                        textBoxReactionDataAvg3.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 3:
                        textBoxReactionDataAvg4.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 4:
                        textBoxReactionDataAvg5.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 5: // Dont use this - Calculate ALL Average
                        //textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 6:
                        textBoxReactionDataAvg13.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 7:
                        textBoxReactionDataAvg14.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 8:
                        textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 9:
                        textBoxReactionDataAvg7.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    case 10:
                        textBoxReactionDataAvg8.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                        break;
                    default:
                        break;
                }

                //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            }

            // Standard Deviation
            textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            // Read Reaction and the Distortion
            textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            if (analyzer.Distortion)
            {
                textBoxDistortion1.Text = "Likely";
            }
            else
            {
                textBoxDistortion1.Text = "NO";
            }

            // Setup the zones and update the TSL if needed
            textBoxZone1.Text = "Count(" + Convert.ToString(analyzer.ReactionZone1Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[0]) + ")";
            textBoxZone2.Text = "Count(" + Convert.ToString(analyzer.ReactionZone2Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[1]) + ")";
            textBoxZone3.Text = "Count(" + Convert.ToString(analyzer.ReactionZone3Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[2]) + ")";
            textBoxZone4.Text = "Count(" + Convert.ToString(analyzer.ReactionZone4Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[3]) + ")";
            textBoxZone5.Text = "Count(" + Convert.ToString(analyzer.ReactionZone5Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[4]) + ")";
            textBoxZone6.Text = "Count(" + Convert.ToString(analyzer.ReactionZone6Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[5]) + ")";
            textBoxZone7.Text = "Count(" + Convert.ToString(analyzer.ReactionZone7Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[6]) + ")";
            txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;

            ////////////////////////////////////////////////////////////
            ///
            /// PRINT preview for the 24 questions
            ///
            ////////////////////////////////////////////////////////////
            globalTestSession = testSession; // keep a copy if we need to print it
            //if (b24QPrintPreview)
            //{
            //    PrintManager printMgr = new PrintManager();
            //    printMgr.CreateReportType1PrintPreview(testSession);
            //}
            //else
            //{
            //    b24QPrintPreview = true; // make it true for the next time
            //}

        }


        private void btnPrint26QData_Click(object sender, EventArgs e)
        {
            b26QPrintPreview = false;  // dont do a print preview when calling Load24Data
            Load26Data();
            PrintManager printMgr = new PrintManager();
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_1; // print the first report
            printMgr.PrintReportToDefaultPrinter(globalTestSession, globalAnaylzer26Q);

        }
        private void btnPrint26QData_2_Click(object sender, EventArgs e)
        {
            b26QPrintPreview = false;  // dont do a print preview when calling Load24Data
            Load26Data();
            PrintManager printMgr = new PrintManager();
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2; // print the second report
            printMgr.PrintReportToDefaultPrinter(globalTestSession, globalAnaylzer26Q);

        }
        private void btnPrint24QData_Click(object sender, EventArgs e)
        {
            b24QPrintPreview = false;  // dont do a print preview when calling Load24Data
            Load24Data();
            PrintManager printMgr = new PrintManager();
            printMgr.PrintReportToDefaultPrinter(globalTestSession);

        }

        private void btnPrintKOT_Click(object sender, EventArgs e)
        {
            bKOTPrintPreview = false;  // dont do a print preview when calling Load24Data
            LoadKOTData();
            PrintManager printMgr = new PrintManager();
            printMgr.PrintReportToDefaultPrinter(globalTestSession);
        }

        private void btnPrintPreview24Q_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            printMgr.CreateReportType1PrintPreview(globalTestSession);
        }

        private void btnPrintPreview26Q_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_1;
            printMgr.CreateReportType1PrintPreview(globalTestSession, globalAnaylzer26Q);

        }

        private void btnPrintPreview26Q_2_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_26Q_2;
            printMgr.CreateReportType1PrintPreview(globalTestSession, globalAnaylzer26Q);

        }

        private void btnPrintPreview15Q_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            //MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            printMgr.CreateReportType1PrintPreview(globalTestSession);
        }

        private void btnPrintPreviewKOT_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            printMgr.CreateReportType1PrintPreview(globalTestSession, globalAnaylzerKOT);

        }

        private void btnAutoAdjust26_Click(object sender, EventArgs e)
        {
            if (globalAutoAdjustSet26Q == 1)
            {
                globalAutoAdjustSet26Q = 0;
                btnAutoAdjust26.BackColor = Color.AliceBlue;
            }
            else
            {
                globalAutoAdjustSet26Q = 1;
                btnAutoAdjust26.BackColor = Color.Red;
            }
        }

        private void btnAutoAdjust24_Click(object sender, EventArgs e)
        {
            if (globalAutoAdjustSet24Q == 1)
            {
                globalAutoAdjustSet24Q = 0;
                btnAutoAdjust24.BackColor = Color.AliceBlue;
            }
            else
            {
                globalAutoAdjustSet24Q = 1;
                btnAutoAdjust24.BackColor = Color.Red;
            }
        }

        private void btnAutoAdjust15_Click(object sender, EventArgs e)
        {
            if (globalAutoAdjustSet15Q == 1)
            {
                globalAutoAdjustSet15Q = 0;
                btnAutoAdjust15.BackColor = Color.AliceBlue;
            }
            else
            {
                globalAutoAdjustSet15Q = 1;
                btnAutoAdjust15.BackColor = Color.Red;
            }
        }

        private void textBoxZone1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLoadBatch_Click(object sender, EventArgs e)
        {
            Load15QBatchData();
        }



        private bool Parse15QBatchCSV()
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;
            int TotalTests = 0; // total number of tests
            string startDate = null; // start date of the test - used to append to the screen
            string startTime = null; // start time of the test
            string data1 = null;

            // START THE NEW TEST SESSION
            TestSession testSession = new TestSession();
            TestSubject subject = new TestSubject();
            subject.FirstName = "Julious";
            subject.LastName = "Gazman";
            subject.DriverLicenseNumber = "A123456789";
            testSession.objTestSubject = subject;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions


                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                // Read until EOF
                while (readcontents != null)
                {
                    TotalTests++;
                    i = 0;
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 92)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                //MessageBox.Show(startDate);
                            }
                            if (i == 1) // time
                            {
                                startTime = Convert.ToString(data);
                                //string[] formats = { "HH:mm:ss" };
                                //var dt2 = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                //startTime = dt2.ToString("HH:mm:ss");
                            }
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                        
                                    
                                  break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:
                                    data1 = data.Trim();
                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0} {1}", (startDate), (data1)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    data1 = data.Trim();
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0} {1}", (startDate), (data1)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    } // END OF ONE TEST

                    col = 0;
                    row += 1;
                    //MessageBox.Show("TEST #" + TotalTests + " " + startDate + "_" + startTime);

                    ////////////////////////////////////////////////////////////////
                    ////
                    //// CALCULATIONS BEGIN
                    ////
                    ////////////////////////////////////////////////////////////////
                    Analyzer15Questions analyzer = new Analyzer15Questions();
                    if (globalAutoAdjustSet15Q == 0)
                    {
                        analyzer.AnalysisTest15Questions(ref testSession);
                    }
                    else
                    {
                        analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                        analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                        analyzer.AnalysisTest15AutoAdjust(ref testSession);
                    }


                    WriteToDataFile(ref analyzer, ref testSession, startDate, startTime);

                    // START THE NEW TEST SESSION
                    testSession = null;
                    subject = null;
                    testSession = new TestSession();
                    subject = new TestSubject();
                    subject.FirstName = "Julious";
                    subject.LastName = "Gazman";
                    subject.DriverLicenseNumber = "A123456789";
                    testSession.objTestSubject = subject;
                 

                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                MessageBox.Show("Total TEST GENERATED: " + TotalTests + " " + startDate + "_" + startTime);
                bFoundFile = true;
            }

            catch (Exception ex)
            {
              
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                MessageBox.Show("END TEST#" + TotalTests + " " + startDate + "_" + startTime);
                bFoundFile = false;
            }

            return bFoundFile;
        }

        private void Load15QBatchData()
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // MessageBox.Show(openFileDialog1.FileName);
                sWatchFile = openFileDialog1.FileName;
            }

            // TestSession testSession = new TestSession();

            // Populate some data for the DLL file to test with
            // Veracity.Client.Proxy.Screen screen = null;


            //TestSubject subject = new TestSubject();
            //subject.FirstName = "Julious";
            //subject.LastName = "Gazman";
            //subject.DriverLicenseNumber = "A123456789";
            //testSession.objTestSubject = subject;


            //Parse15QBatchCSV(ref testSession);
            Parse15QBatchCSV();
        }

        private void WriteToDataFile(ref Analyzer15Questions analyzer, ref TestSession testSession, string startDate, string startTime)
        {
            string s1 = Convert.ToString(analyzer.StandardDeviation);
            string s2 = Convert.ToString(analyzer.dTSLHIGH); // TSL H Input
            string s3 = Convert.ToString(analyzer.dTSLLow); // TSL Low Input
            string s4 = Convert.ToString(analyzer.TSLPercentage); // TSL Percentage
            string s5 = Convert.ToString(analyzer.iReadReaction);
            string s6 = Convert.ToString(analyzer.ReadReaction1);
            string s7 = Convert.ToString(analyzer.ReadReaction2);
            string s8 = Convert.ToString(analyzer.Distortion);

            string s9 = Convert.ToString(analyzer.dRDTimeAvg[0]); // Average TEST
            string s10 = Convert.ToString(analyzer.dRDTimeAvg[1]); // AVG Q 3/5
            string s11 = Convert.ToString(analyzer.dRDTimeAvg[2]); // AVG Q 6/8
            string s12 = Convert.ToString(analyzer.dRDTimeAvg[3]); // AVG Q 9/11
            string s13 = Convert.ToString(analyzer.dRDTimeAvg[4]); // AVG Q 12/14
            string s14 = Convert.ToString(analyzer.dRDTimeAvg[6]); // TSLH
            string s15 = Convert.ToString(analyzer.dRDTimeAvg[7]); // TSLL

            string s16 = Convert.ToString(analyzer.ReactionZone1Counter); // Weapons
            string s17 = Convert.ToString(analyzer.ReactionZone2Counter); // Smuggling
            string s18 = Convert.ToString(analyzer.ReactionZone3Counter); // Terrorism

            string s19 = "GREEN"; // Zone 1, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            string s19Red = "0";
            string s20 = "GREEN"; // Zone 2, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            string s20Red = "0";
            string s21 = "GREEN"; // Zone 3, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            string s21Red = "0";
            string s22 = "No_Threat"; // If any ZONE is RED then its a RISK

            Veracity.Client.Proxy.Screen screen = null; // Traverse through the testSession


            // Determine the RISK ANALYSIS
            if (Convert.ToDecimal(s16) > 1) // Zone 1 Weapons
            {
                s19 = "RED";
                s19Red = "1";
            }
            if (Convert.ToDecimal(s17) > 1)// Zone 2 Smuggling
            {
                s20 = "RED";
                s20Red = "1";
            }
            if (Convert.ToDecimal(s18) > 1)// Zone 3 Terrorism
            {
                s21 = "RED";
                s21Red = "1";
            }

            // Determine if we have an ADMIT, this will override above
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                switch (i)
                {
                        // SKip first 2 questions
                    case 0:
                    case 1:
                        break;

                    // Zone Travelling
                    case 2:
                    case 3:
                    case 4:
                        break;

                    // Zone 1 Weapons
                    case 5:
                    case 6:
                    case 7:
                        if (screen.Answer)
                        {
                            s19 = "RED_ADMIT";
                        }
                        break;

                    // Zone 2 Smuggling
                    case 8:
                    case 9:
                    case 10:
                        if (screen.Answer)
                        {
                            s20 = "RED_ADMIT";
                        }
                        break;
                    // Zone 3 Terrorism
                    case 11:
                    case 12:
                    case 13:
                        if (screen.Answer)
                        {
                            s21 = "RED_ADMIT";
                        }
                        break;
                    case 14:
                    default:
                        break;


                }
            }


            // Determine the THREAD LEVEL

            if (Convert.ToDecimal(s19Red) > 0)
            {
                s22 = "THREAT";

            }
            if (Convert.ToDecimal(s20Red) > 0)
            {
                s22 = "THREAT";

            }
            if (Convert.ToDecimal(s21Red) > 0)
            {
                s22 = "THREAT";

            }

            //// Standard Deviation
            //textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            //// Read Reaction and the Distortion
            //textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            //textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            //textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            //if (analyzer.Distortion)
            //{
            //    textBoxDistortion1.Text = "Likely";
            //}
            //else
            //{
            //    textBoxDistortion1.Text = "NO";
            //}

            //// Setup the zones and update the TSL if needed
            //textBoxZone1.Text = Convert.ToString(analyzer.ReactionZone1Counter);
            //textBoxZone2.Text = Convert.ToString(analyzer.ReactionZone2Counter);
            //textBoxZone3.Text = Convert.ToString(analyzer.ReactionZone3Counter);
            //textBoxZone4.Text = Convert.ToString(analyzer.ReactionZone4Counter);
            //textBoxZone5.Text = Convert.ToString(analyzer.ReactionZone5Counter);
            //textBoxZone6.Text = Convert.ToString(analyzer.ReactionZone6Counter);
            //textBoxZone7.Text = Convert.ToString(analyzer.ReactionZone7Counter);
            //txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            //txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            //numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;

            using (StreamWriter sw = new StreamWriter("c:\\Veracity\\report\\STATS.CSV", true))  // True to append data to the file; false to overwrite the file
            {
                sw.WriteLine(startDate + "_" + startTime + " " + s1 + " " + s5 + " " + s6 + " " + s7 + " " + s8 + " " + s9 + " " + s10 + " " + s11 + " " + s12 + " " + s13 + " " + s14 + " " + s15 + " " + s2 + " " + s3 + " " + s4 + " " + s16 + " " + s17 + " " + s18 + " " + s19 + " " + s20 + " " + s21 + " " + s22);
            }

            //string s1 = Convert.ToString(analyzer.StandardDeviation);
            //string s2 = Convert.ToString(analyzer.dTSLHIGH); // TSL H Input
            //string s3 = Convert.ToString(analyzer.dTSLLow); // TSL Low Input
            //string s4 = Convert.ToString(analyzer.TSLPercentage); // TSL Percentage
            //string s5 = Convert.ToString(analyzer.iReadReaction);
            //string s6 = Convert.ToString(analyzer.ReadReaction1);
            //string s7 = Convert.ToString(analyzer.ReadReaction2);
            //string s8 = Convert.ToString(analyzer.Distortion);

            //string s9 = Convert.ToString(analyzer.dRDTimeAvg[0]); // Average TEST
            //string s10 = Convert.ToString(analyzer.dRDTimeAvg[1]); // AVG Q 3/5
            //string s11 = Convert.ToString(analyzer.dRDTimeAvg[2]); // AVG Q 6/8
            //string s12 = Convert.ToString(analyzer.dRDTimeAvg[3]); // AVG Q 9/11
            //string s13 = Convert.ToString(analyzer.dRDTimeAvg[4]); // AVG Q 12/14
            //string s14 = Convert.ToString(analyzer.dRDTimeAvg[6]); // TSLH
            //string s15 = Convert.ToString(analyzer.dRDTimeAvg[7]); // TSLL

            //string s16 = Convert.ToString(analyzer.ReactionZone1Counter); // Weapons
            //string s17 = Convert.ToString(analyzer.ReactionZone2Counter); // Smuggling
            //string s18 = Convert.ToString(analyzer.ReactionZone3Counter); // Terrorism

            //string s19 = "GREEN"; // Zone 1, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            //string s19Red = "0";
            //string s20 = "GREEN"; // Zone 2, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            //string s20Red = "0";
            //string s21 = "GREEN"; // Zone 3, Risk Analsys Information GREEN = 0 or 1 SPR, RED > 1 SPR or ADMIT
            //string s21Red = "0";
            //string s22 = "No_Threat"; // If any ZONE is RED then its a RISK
        }

        private void textBoxDistortion1_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// ///////////////////////////////////////////////////////////////////
        /// ///////
        /// ///////
        /// /////// LOAD KOT DATA INFORMATION
        /// ///////
        /// ///////
        /// ///////////////////////////////////////////////////////////////////
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnLoadKOTData_Click(object sender, EventArgs e)
        {
            textBoxReactionData.Text = "Reaction Data";
            LoadKOTData();
        }


        private void LoadKOTData()
        {

            bool bExitOutOfForm = false;

            groupBoxAverages.Text = "Reaction Data AVG";
            groupBoxAccidents.Text = "First Four";
            groupBoxIllegalDrugs.Text = "Second Four";
            groupBoxSexOffenders.Text = "Third Four";
            groupBoxArrest.Text = "Fourth Four";
            groupBoxSmuggling.Text = "Did You Ever Lie...";


            groupBoxAccidents.Show();
            groupBoxAdopt.Show();
            groupBoxArrest.Show();
            groupBoxDistort.Show();
            groupBoxExit.Hide();
            groupBoxIllegalDrugs.Show();
            groupBoxReadRatio.Show();
            groupBoxSexOffenders.Show();
            groupBoxSmuggling.Show();
            groupBoxTerrorism.Hide();
            groupBoxUnknowns.Hide();
            groupBoxZones.Show();

            // groupBoxSmuggling.Text = "";
            textBoxQuestion16.Text = "";
            textBoxIssues16.Text = "";
            textBoxQuestion17.Text = "";
            textBoxIssues17.Text = "";
            textBoxReactionData16.Text = "";
            textBoxReactionData17.Text = "";
            textBoxAnswers16.Text = "";
            textBoxAnswers17.Text = "";
            textBoxReadTime16.Text = "";
            textBoxReadTime17.Text = "";
            textBoxReaction16.Text = "";
            textBoxReaction17.Text = "";


            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // MessageBox.Show(openFileDialog1.FileName);
                sWatchFile = openFileDialog1.FileName;
            }


            TestSession testSession = new TestSession();

            // Populate some data for the DLL file to test with
            Veracity.Client.Proxy.Screen screen = null;


            TestSubject subject = new TestSubject();
            subject.FirstName = "Julious";
            subject.LastName = "Gazman";
            subject.DriverLicenseNumber = "A123456789";
            testSession.objTestSubject = subject;


            if (ParseKOTCSV(ref testSession))
            {
                //MessageBox.Show("Finish Loading CSV file...");
            }
            else // if no files found either quit or populate with default data
            {
                MessageBox.Show("File=" + sWatchFile + " not found.  EXIT!");
                bExitOutOfForm = true;
            }


            if (bExitOutOfForm == false)
            {

                ////////////////////////////////////////////////////////////////
                ////
                //// CALCULATIONS BEGIN
                ////
                ////////////////////////////////////////////////////////////////
                AnalyzerKOTQuestions analyzer = new AnalyzerKOTQuestions();
                if (globalAutoAdjustSetKOT == 0)
                {
                    analyzer.AnalysisTestKOTQuestions(ref testSession);
                }




                ////////////////////////////////////////////////////////////////
                ////
                //// POPULATE FORMS BEGIN
                ////
                ////////////////////////////////////////////////////////////////

                // Populate the QUESTIONS and NUMBERS
                screen = null;
                string sAnswer = "NO";
                string sReaction = null;
                for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
                {
                    screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                    // Print either a YES or NO for the answer
                    if (screen.Answer)
                    {
                        sAnswer = "YES";
                    }
                    else
                    {
                        sAnswer = "NO";
                    }

                    // Print either a HIT, NO, or null
                    sReaction = null;
                    if (screen.IsHit == true)
                    {
                        sReaction = "SPR";
                    }
                    if (screen.Answer)
                    {
                        sReaction = "ADMIT";
                    }
                    if (screen.IsHit == false)
                    {
                        sReaction = "";
                    }




                    switch (i)
                    {
                        case 0:
                            //
                            // if we are at question 1 and 2, then nothing for the reaction
                            //
                            sReaction = "";
                            textBoxIssues1.Text = screen.ScreenText;
                            textBoxQuestion1.Text = Convert.ToString(i + 1);
                            textBoxAnswers1.Text = sAnswer;
                            textBoxReactionData1.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime1.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction1.Text = sReaction;
                            break;
                        case 1:
                            //
                            // if we are at question 1 and 2, then nothing for the reaction
                            //
                            sReaction = "";
                            textBoxIssues2.Text = screen.ScreenText;
                            textBoxQuestion2.Text = Convert.ToString(i + 1);
                            textBoxAnswers2.Text = sAnswer;
                            textBoxReactionData2.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime2.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction2.Text = sReaction;
                            break;
                        case 2:
                            textBoxIssues3.Text = screen.ScreenText;
                            textBoxQuestion3.Text = Convert.ToString(i + 1);
                            textBoxAnswers3.Text = sAnswer;
                            textBoxReactionData3.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime3.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction3.Text = sReaction;
                            break;
                        case 3:
                            textBoxIssues4.Text = screen.ScreenText;
                            textBoxQuestion4.Text = Convert.ToString(i + 1);
                            textBoxAnswers4.Text = sAnswer;
                            textBoxReactionData4.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime4.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction4.Text = sReaction;
                            break;
                        case 4:
                            textBoxIssues5.Text = screen.ScreenText;
                            textBoxQuestion5.Text = Convert.ToString(i + 1);
                            textBoxAnswers5.Text = sAnswer;
                            textBoxReactionData5.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime5.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction5.Text = sReaction;
                            break;
                        case 5:
                            textBoxIssues6.Text = screen.ScreenText;
                            textBoxQuestion6.Text = Convert.ToString(i + 1);
                            textBoxAnswers6.Text = sAnswer;
                            textBoxReactionData6.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime6.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction6.Text = sReaction;
                            break;
                        case 6:
                            textBoxIssues7.Text = screen.ScreenText;
                            textBoxQuestion7.Text = Convert.ToString(i + 1);
                            textBoxAnswers7.Text = sAnswer;
                            textBoxReactionData7.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime7.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction7.Text = sReaction;
                            break;
                        case 7:
                            textBoxIssues8.Text = screen.ScreenText;
                            textBoxQuestion8.Text = Convert.ToString(i + 1);
                            textBoxAnswers8.Text = sAnswer;
                            textBoxReactionData8.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime8.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction8.Text = sReaction;
                            break;
                        case 8:
                            textBoxIssues9.Text = screen.ScreenText;
                            textBoxQuestion9.Text = Convert.ToString(i + 1);
                            textBoxAnswers9.Text = sAnswer;
                            textBoxReactionData9.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime9.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction9.Text = sReaction;
                            break;
                        case 9:
                            textBoxIssues10.Text = screen.ScreenText;
                            textBoxQuestion10.Text = Convert.ToString(i + 1);
                            textBoxAnswers10.Text = sAnswer;
                            textBoxReactionData10.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime10.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction10.Text = sReaction;
                            break;
                        case 10:
                            textBoxIssues11.Text = screen.ScreenText;
                            textBoxQuestion11.Text = Convert.ToString(i + 1);
                            textBoxAnswers11.Text = sAnswer;
                            textBoxReactionData11.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime11.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction11.Text = sReaction;
                            break;
                        case 11:
                            textBoxIssues12.Text = screen.ScreenText;
                            textBoxQuestion12.Text = Convert.ToString(i + 1);
                            textBoxAnswers12.Text = sAnswer;
                            textBoxReactionData12.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime12.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction12.Text = sReaction;
                            break;
                        case 12:
                            textBoxIssues13.Text = screen.ScreenText;
                            textBoxQuestion13.Text = Convert.ToString(i + 1);
                            textBoxAnswers13.Text = sAnswer;
                            textBoxReactionData13.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime13.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction13.Text = sReaction;
                            break;
                        case 13:
                            textBoxIssues14.Text = screen.ScreenText;
                            textBoxQuestion14.Text = Convert.ToString(i + 1);
                            textBoxAnswers14.Text = sAnswer;
                            textBoxReactionData14.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime14.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction14.Text = sReaction;
                            break;
                        case 14:
                            textBoxIssues15.Text = screen.ScreenText;
                            textBoxQuestion15.Text = Convert.ToString(i + 1);
                            textBoxAnswers15.Text = sAnswer;
                            textBoxReactionData15.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime15.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction15.Text = sReaction;
                            break;
                        case 15:
                            textBoxIssues16.Text = screen.ScreenText;
                            textBoxQuestion16.Text = Convert.ToString(i + 1);
                            textBoxAnswers16.Text = sAnswer;
                            textBoxReactionData16.Text = Convert.ToString(analyzer.dRDTime[i]);
                            textBoxReadTime16.Text = Convert.ToString(screen.DeltaClickTimeInSeconds);
                            textBoxReaction16.Text = sReaction;
                            break;
                        default:
                            MessageBox.Show("Unknown case!");
                            break;

                    }
                }

                for (int i = 0; i <= 7; i++)
                {
                    switch (i)
                    {
                        case 0:
                            textBoxReactionDataAvg1.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 1:
                            textBoxReactionDataAvg2.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 2:
                            textBoxReactionDataAvg3.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 3:
                            textBoxReactionDataAvg4.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 4:
                            textBoxReactionDataAvg5.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 5: // Dont use this - Calculate ALL Average
                            //textBoxReactionDataAvg6.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 6:
                            textBoxReactionDataAvg13.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        case 7:
                            textBoxReactionDataAvg14.Text = Convert.ToString(analyzer.dRDTimeAvg[i]);
                            break;
                        default:
                            break;
                    }

                    //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
                }

                // Standard Deviation
                textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);
                textBoxSTDKOT.Text = Convert.ToString(analyzer.StandardDeviation);
                textBoxRDAverageKOT.Text = Convert.ToString(analyzer.dSurveyAvgKOT);
                textBoxRTAverageKOT.Text = Convert.ToString(analyzer.dReadTimeAvgKOT);

                // Read Reaction and the Distortion
                textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
                textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
                textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);


                // UPDATE THE KOT BOX
                textBoxFirst4.Text = "1st 4, " + analyzer.sReaction[0];
                textBoxSecond4.Text = "2nd 4, " + analyzer.sReaction[1];
                textBoxThird4.Text = "3rd 4, " + analyzer.sReaction[2];
                textBoxFourth4.Text = "4th 4, " + analyzer.sReaction[3];
                textBoxQ16.Text = "Q14, " + analyzer.sReaction[4];
                textBoxTSLHiKOT.Text = analyzer.dTSLHIGHKOT.ToString();
                textBoxTSLLowKOT.Text = analyzer.dTSLLowKOT.ToString();

                textBoxFirst4.ForeColor = Color.Black ;
                textBoxSecond4.ForeColor = Color.Black;
                textBoxThird4.ForeColor = Color.Black;
                textBoxFourth4.ForeColor = Color.Black;
                textBoxFirst4.BackColor = Color.Green;
                textBoxSecond4.BackColor = Color.Green;
                textBoxThird4.BackColor = Color.Green;
                textBoxFourth4.BackColor = Color.Green;

                // First 4
                if (analyzer.iHiddenK[0] > 0)
                {
                    textBoxFirst4.BackColor = Color.Red;
                }

                // Second 4
                if (analyzer.iHiddenK[1] > 0)
                {
                    textBoxSecond4.BackColor = Color.Red;
                }

                // Third 4
                if (analyzer.iHiddenK[2] > 0)
                {
                    textBoxThird4.BackColor = Color.Red;
                }

                // Fourth 4
                if (analyzer.iHiddenK[3] > 0)
                {
                    textBoxFourth4.BackColor = Color.Red;
                }

                // Q16 Reaction
                if (analyzer.sReaction[4] == "REACTION")
                {
                    textBoxQ16.BackColor = Color.Red;
                }

                // First 4
                if (analyzer.iHiddenL[3] > 0)
                {
                    textBoxFirst4.BackColor = Color.Yellow;
                }

                // Second 4
                if (analyzer.iHiddenL[6] > 0)
                {
                    textBoxSecond4.BackColor = Color.Yellow;
                }

                // Third 4
                if (analyzer.iHiddenL[9] > 0)
                {
                    textBoxThird4.BackColor = Color.Yellow;
                }

                // Fourth 4
                if (analyzer.iHiddenL[12] > 0)
                {
                    textBoxFourth4.BackColor = Color.Yellow;
                }




                if (analyzer.Distortion)
                {
                    textBoxDistortion1.Text = "Likely";
                }
                else
                {
                    textBoxDistortion1.Text = "NO";
                }

                // Setup the zones and update the TSL if needed
                textBoxZone1.Text = Convert.ToString(analyzer.ReactionZone1Counter);
                textBoxZone2.Text = Convert.ToString(analyzer.ReactionZone2Counter);
                textBoxZone3.Text = Convert.ToString(analyzer.ReactionZone3Counter);
                textBoxZone4.Text = Convert.ToString(analyzer.ReactionZone4Counter);
                textBoxZone5.Text = Convert.ToString(analyzer.ReactionZone5Counter);
                textBoxZone6.Text = Convert.ToString(analyzer.ReactionZone6Counter);
                textBoxZone7.Text = Convert.ToString(analyzer.ReactionZone7Counter);
                txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
                txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
                numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;


                // then formulate the print report
                // then send to printer

                ////////////////////////////////////////////////////////////
                ///
                /// PRINT preview for the 15 questions
                ///
                ////////////////////////////////////////////////////////////
                globalTestSession = testSession; // keep a copy if we need to print it
                globalAnaylzerKOT = analyzer; // keep a copy if we need to print it


            }
        } // EXIT OUT OF FORM bExitOutOfForm == false



        private bool ParseKOTCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen




                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 98)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {

                                //MessageBox.Show("TEXT=" + TotalQuestion);
                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "Is the 1 White";
                                        screen.ScreenTextShort = "Is the 1 White";
                                        break;
                                    case 2:
                                        screen.ScreenText = "Is the 3 White";
                                        screen.ScreenTextShort = "Is the 3 White";
                                        break;
                                    case 3:
                                        screen.ScreenText = "Is the 7 White";
                                        screen.ScreenTextShort = "Is the 7 White";
                                        break;
                                    case 4:
                                        screen.ScreenText = "Is the 4 RED";
                                        screen.ScreenTextShort = "Is the 4 RED";
                                        break;
                                    case 5:
                                        screen.ScreenText = "Is the 6 White";
                                        screen.ScreenTextShort = "Is the 6 White";
                                        break;
                                    case 6:
                                        screen.ScreenText = "Is the 2 white";
                                        screen.ScreenTextShort = "Is the 2 white";
                                        break;
                                    case 7:
                                        screen.ScreenText = "Is the 4 GREEN";
                                        screen.ScreenTextShort = "Is the 4 GREEN";
                                        break;
                                    case 8:
                                        screen.ScreenText = "Is the 1 white";
                                        screen.ScreenTextShort = "Is the 1 white";
                                        break;
                                    case 9:
                                        screen.ScreenText = "Is the 5 white";
                                        screen.ScreenTextShort = "Is the 5 white";
                                        break;
                                    case 10:
                                        screen.ScreenText = "Is the 4 BLUE";
                                        screen.ScreenTextShort = "Is the 4 BLUE";
                                        break;
                                    case 11:
                                        screen.ScreenText = "Is the 3 white";
                                        screen.ScreenTextShort = "Is the 3 white";
                                        break;
                                    case 12:
                                        screen.ScreenText = "Is the 7 white";
                                        screen.ScreenTextShort = "Is the 7 white";
                                        break;
                                    case 13:
                                        screen.ScreenText = "Is the 4 PURPLE";
                                        screen.ScreenTextShort = "Is the 4 PURPLE";
                                        break;
                                    case 14:
                                        screen.ScreenText = "Is the 2 white";
                                        screen.ScreenTextShort = "Is the 2 white";
                                        break;
                                    case 15:
                                        screen.ScreenText = "Is the 6 white";
                                        screen.ScreenTextShort = "Is the 6 white";
                                        break;
                                    case 16:
                                        screen.ScreenText = "Did you ever LIE";
                                        screen.ScreenTextShort = "Did you ever LIE";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    
                                    if (String.Compare(data.Trim(), "YES", true) == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:

                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false;
            }

            return bFoundFile;
        }

        private void numericUpDownTSLAdjust_ValueChanged(object sender, EventArgs e)
        {
            //if ((bGlobalFirstSetOfDataLoaded == true) && ( bGlobal24QDataParsed == true ))
            //{
            //    Update24Data();
            //}
        }




        ////////////////////////////////////////////////////////////
        ///
        /// 18 QUESTIONS
        ///
        ////////////////////////////////////////////////////////////

        private void Load18Data()
        {
            groupBoxAverages.Text = "Summary Averages";
            groupBoxAccidents.Text = "";
            groupBoxAdopt.Text = "";
            groupBoxArrest.Text = "";
            groupBoxDistort.Text = "Distortion";
            groupBoxExit.Text = "";
            groupBoxIllegalDrugs.Text = "";
            groupBoxReadRatio.Text = "Read Ratio";
            groupBoxSexOffenders.Text = "";
            groupBoxSmuggling.Text = "";
            groupBoxTerrorism.Text = "";
            groupBoxUnknowns.Text = "";
            groupBoxZones.Text = "Zones";

            groupBoxAccidents.Show();
            groupBoxAdopt.Show();
            groupBoxArrest.Show();
            groupBoxDistort.Show();
            groupBoxExit.Show();
            groupBoxIllegalDrugs.Show();
            groupBoxReadRatio.Show();
            groupBoxSexOffenders.Show();
            groupBoxSmuggling.Show();
            groupBoxTerrorism.Show();
            groupBoxUnknowns.Show();
            groupBoxZones.Show();

            // not part of the zones
            textBoxAvg1.BackColor = Color.LightGray;
            textBoxAvgTest.BackColor = Color.LightGray;
            textBoxReactionDataAvg1.BackColor = Color.LightGray;
            textBoxReactionDataAvg2.BackColor = Color.LightGray;

            // Modify the color scheme
            //textBoxReactionDataAvg2.BackColor = Color.Aqua;
            textBoxAvg2.BackColor = Color.Aqua;
            textBoxIssues1.BackColor = Color.Aqua;
            textBoxQuestion1.BackColor = Color.Aqua;
            textBoxAnswers1.BackColor = Color.Aqua;
            textBoxReactionData1.BackColor = Color.Aqua;
            textBoxReadTime1.BackColor = Color.Aqua;
            textBoxReaction1.BackColor = Color.Aqua;

            textBoxIssues2.BackColor = Color.Aqua;
            textBoxQuestion2.BackColor = Color.Aqua;
            textBoxAnswers2.BackColor = Color.Aqua;
            textBoxReactionData2.BackColor = Color.Aqua;
            textBoxReadTime2.BackColor = Color.Aqua;
            textBoxReaction2.BackColor = Color.Aqua;

            textBoxIssues3.BackColor = Color.Aqua;
            textBoxQuestion3.BackColor = Color.Aqua;
            textBoxAnswers3.BackColor = Color.Aqua;
            textBoxReactionData3.BackColor = Color.Aqua;
            textBoxReadTime3.BackColor = Color.Aqua;
            textBoxReaction3.BackColor = Color.Aqua;

            textBoxIssues4.BackColor = Color.Aqua;
            textBoxQuestion4.BackColor = Color.Aqua;
            textBoxAnswers4.BackColor = Color.Aqua;
            textBoxReactionData4.BackColor = Color.Aqua;
            textBoxReadTime4.BackColor = Color.Aqua;
            textBoxReaction4.BackColor = Color.Aqua;

            textBoxZone1.BackColor = Color.DarkGoldenrod;
            textBoxReactionDataAvg3.BackColor = Color.DarkGoldenrod;
            textBoxAvg2.BackColor = Color.DarkGoldenrod;
            textBoxIssues5.BackColor = Color.DarkGoldenrod;
            textBoxQuestion5.BackColor = Color.DarkGoldenrod;
            textBoxAnswers5.BackColor = Color.DarkGoldenrod;
            textBoxReactionData5.BackColor = Color.DarkGoldenrod;
            textBoxReadTime5.BackColor = Color.DarkGoldenrod;
            textBoxReaction5.BackColor = Color.DarkGoldenrod;

            textBoxIssues6.BackColor = Color.DarkGoldenrod;
            textBoxQuestion6.BackColor = Color.DarkGoldenrod;
            textBoxAnswers6.BackColor = Color.DarkGoldenrod;
            textBoxReactionData6.BackColor = Color.DarkGoldenrod;
            textBoxReadTime6.BackColor = Color.DarkGoldenrod;
            textBoxReaction6.BackColor = Color.DarkGoldenrod;

            textBoxIssues7.BackColor = Color.DarkGoldenrod;
            textBoxQuestion7.BackColor = Color.DarkGoldenrod;
            textBoxAnswers7.BackColor = Color.DarkGoldenrod;
            textBoxReactionData7.BackColor = Color.DarkGoldenrod;
            textBoxReadTime7.BackColor = Color.DarkGoldenrod;
            textBoxReaction7.BackColor = Color.DarkGoldenrod;

            textBoxIssues8.BackColor = Color.DarkGoldenrod;
            textBoxQuestion8.BackColor = Color.DarkGoldenrod;
            textBoxAnswers8.BackColor = Color.DarkGoldenrod;
            textBoxReactionData8.BackColor = Color.DarkGoldenrod;
            textBoxReadTime8.BackColor = Color.DarkGoldenrod;
            textBoxReaction8.BackColor = Color.DarkGoldenrod;

            textBoxZone2.BackColor = Color.DarkKhaki;
            textBoxReactionDataAvg4.BackColor = Color.DarkKhaki;
            textBoxAvg3.BackColor = Color.DarkKhaki;
            textBoxIssues9.BackColor = Color.DarkKhaki;
            textBoxQuestion9.BackColor = Color.DarkKhaki;
            textBoxAnswers9.BackColor = Color.DarkKhaki;
            textBoxReactionData9.BackColor = Color.DarkKhaki;
            textBoxReadTime9.BackColor = Color.DarkKhaki;
            textBoxReaction9.BackColor = Color.DarkKhaki;

            textBoxIssues10.BackColor = Color.DarkKhaki;
            textBoxQuestion10.BackColor = Color.DarkKhaki;
            textBoxAnswers10.BackColor = Color.DarkKhaki;
            textBoxReactionData10.BackColor = Color.DarkKhaki;
            textBoxReadTime10.BackColor = Color.DarkKhaki;
            textBoxReaction10.BackColor = Color.DarkKhaki;

            textBoxIssues11.BackColor = Color.DarkKhaki;
            textBoxQuestion11.BackColor = Color.DarkKhaki;
            textBoxAnswers11.BackColor = Color.DarkKhaki;
            textBoxReactionData11.BackColor = Color.DarkKhaki;
            textBoxReadTime11.BackColor = Color.DarkKhaki;
            textBoxReaction11.BackColor = Color.DarkKhaki;

            textBoxIssues12.BackColor = Color.DarkKhaki;
            textBoxQuestion12.BackColor = Color.DarkKhaki;
            textBoxAnswers12.BackColor = Color.DarkKhaki;
            textBoxReactionData12.BackColor = Color.DarkKhaki;
            textBoxReadTime12.BackColor = Color.DarkKhaki;
            textBoxReaction12.BackColor = Color.DarkKhaki;

            textBoxZone3.BackColor = Color.LightBlue;
            textBoxReactionDataAvg5.BackColor = Color.LightBlue;
            textBoxAvg4.BackColor = Color.LightBlue;
            textBoxIssues13.BackColor = Color.LightBlue;
            textBoxQuestion13.BackColor = Color.LightBlue;
            textBoxAnswers13.BackColor = Color.LightBlue;
            textBoxReactionData13.BackColor = Color.LightBlue;
            textBoxReadTime13.BackColor = Color.LightBlue;
            textBoxReaction13.BackColor = Color.LightBlue;

            textBoxIssues14.BackColor = Color.LightBlue;
            textBoxQuestion14.BackColor = Color.LightBlue;
            textBoxAnswers14.BackColor = Color.LightBlue;
            textBoxReactionData14.BackColor = Color.LightBlue;
            textBoxReadTime14.BackColor = Color.LightBlue;
            textBoxReaction14.BackColor = Color.LightBlue;

            textBoxIssues15.BackColor = Color.LightBlue;
            textBoxQuestion15.BackColor = Color.LightBlue;
            textBoxAnswers15.BackColor = Color.LightBlue;
            textBoxReactionData15.BackColor = Color.LightBlue;
            textBoxReadTime15.BackColor = Color.LightBlue;
            textBoxReaction15.BackColor = Color.LightBlue;

            textBoxIssues16.BackColor = Color.LightBlue;
            textBoxQuestion16.BackColor = Color.LightBlue;
            textBoxAnswers16.BackColor = Color.LightBlue;
            textBoxReactionData16.BackColor = Color.LightBlue;
            textBoxReadTime16.BackColor = Color.LightBlue;
            textBoxReaction16.BackColor = Color.LightBlue;

            textBoxZone4.BackColor = Color.GreenYellow;
            textBoxReactionDataAvg6.BackColor = Color.GreenYellow;
            textBoxAvg5.BackColor = Color.GreenYellow;
            textBoxIssues17.BackColor = Color.GreenYellow;
            textBoxQuestion17.BackColor = Color.GreenYellow;
            textBoxAnswers17.BackColor = Color.GreenYellow;
            textBoxReactionData17.BackColor = Color.GreenYellow;
            textBoxReadTime17.BackColor = Color.GreenYellow;
            textBoxReaction17.BackColor = Color.GreenYellow;

            textBoxIssues18.BackColor = Color.GreenYellow;
            textBoxQuestion18.BackColor = Color.GreenYellow;
            textBoxAnswers18.BackColor = Color.GreenYellow;
            textBoxReactionData18.BackColor = Color.GreenYellow;
            textBoxReadTime18.BackColor = Color.GreenYellow;
            textBoxReaction18.BackColor = Color.GreenYellow;

            textBoxIssues19.BackColor = Color.GreenYellow;
            textBoxQuestion19.BackColor = Color.GreenYellow;
            textBoxAnswers19.BackColor = Color.GreenYellow;
            textBoxReactionData19.BackColor = Color.GreenYellow;
            textBoxReadTime19.BackColor = Color.GreenYellow;
            textBoxReaction19.BackColor = Color.GreenYellow;

            textBoxIssues20.BackColor = Color.GreenYellow;
            textBoxQuestion20.BackColor = Color.GreenYellow;
            textBoxAnswers20.BackColor = Color.GreenYellow;
            textBoxReactionData20.BackColor = Color.GreenYellow;
            textBoxReadTime20.BackColor = Color.GreenYellow;
            textBoxReaction20.BackColor = Color.GreenYellow;


            textBoxZone5.BackColor = Color.Orange;
            textBoxReactionDataAvg7.BackColor = Color.Orange;
            textBoxAvg6.BackColor = Color.Orange;
            textBoxIssues21.BackColor = Color.Orange;
            textBoxQuestion21.BackColor = Color.Orange;
            textBoxAnswers21.BackColor = Color.Orange;
            textBoxReactionData21.BackColor = Color.Orange;
            textBoxReadTime21.BackColor = Color.Orange;
            textBoxReaction21.BackColor = Color.Orange;

            textBoxIssues22.BackColor = Color.Orange;
            textBoxQuestion22.BackColor = Color.Orange;
            textBoxAnswers22.BackColor = Color.Orange;
            textBoxReactionData22.BackColor = Color.Orange;
            textBoxReadTime22.BackColor = Color.Orange;
            textBoxReaction22.BackColor = Color.Orange;

            textBoxIssues23.BackColor = Color.Orange;
            textBoxQuestion23.BackColor = Color.Orange;
            textBoxAnswers23.BackColor = Color.Orange;
            textBoxReactionData23.BackColor = Color.Orange;
            textBoxReadTime23.BackColor = Color.Orange;
            textBoxReaction23.BackColor = Color.Orange;

            textBoxIssues24.BackColor = Color.Orange;
            textBoxQuestion24.BackColor = Color.Orange;
            textBoxAnswers24.BackColor = Color.Orange;
            textBoxReactionData24.BackColor = Color.Orange;
            textBoxReadTime24.BackColor = Color.Orange;
            textBoxReaction24.BackColor = Color.Orange;


            // The rest of the zones we dont use
            textBoxZone6.BackColor = Color.LightGray;
            textBoxZone7.BackColor = Color.LightGray;
            //textBoxReactionDataAvg9.BackColor = Color.LightGray;
            //textBoxAvg7.BackColor = Color.LightGray;


            // TSL Hi, Low, STD boxes or SPR boxes

            textBoxTSLHiLowSTD1.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD2.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD3.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD4.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD5.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD6.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD7.BackColor = Color.LightGray;



            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // MessageBox.Show(openFileDialog1.FileName);
                sWatchFile = openFileDialog1.FileName;
            }


            TestSession testSession = new TestSession();
            string gResultsFolderPath = ""; // this is not used after all
            int iQuestionNumber = 1;

            // Populate some data for the DLL file to test with
            Veracity.Client.Proxy.Screen screen = null;

            TestSubject subject = new TestSubject();
            subject.FirstName = "Mark";
            subject.LastName = "Anthony";
            subject.DriverLicenseNumber = "A123456789";

            testSession.objTestSubject = subject;

            //testSession.objSubjectIdentification.FirstName = "Mark";
            //testSession.objSubjectIdentification.LastName = "Anthony";
            //testSession.objSubjectIdentification.DriverLicenseNumber = "A1875590";


            if (Parse18QCSV(ref testSession))
            {
                //MessageBox.Show("Finish Loading CSV file...");
            }
            else // if no files found either quit or populate with default data
            {
                MessageBox.Show("File=" + sWatchFile + " not found.  Using default data");

                // Question 1
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "ADAPT 1";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".0901");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:06.9940544");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:10.2487344");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.3");
                screen.ZAxisData = Convert.ToDouble("9.00");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 2
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "ADAPT 2";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("11.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.2");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 3
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Had accident";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("20.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 4
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Failed to report accident";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                screen.ZAxisData = Convert.ToDouble("15.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 5
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Caused Hit & Run accident";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                screen.ZAxisData = Convert.ToDouble("10.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                testSession.lstScreenQuestionOnly.Add(screen);



                // Question 6
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used M/J in last 12 months";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3001");
                screen.ZAxisData = Convert.ToDouble("14.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("23.9");
                testSession.lstScreenQuestionOnly.Add(screen);



                // Question 7
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used Drugs in last 3 mos.";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("25.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 8
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used Drugs in last 30 days";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                screen.ZAxisData = Convert.ToDouble("27.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 9
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Committed Sex Crime";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                screen.ZAxisData = Convert.ToDouble("27.40");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 10
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Convicted of Sex Crime";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("35.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 11
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Registered Sex Offender";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("19.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 12
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Traffic Vio";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2903");
                screen.ZAxisData = Convert.ToDouble("13.34");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("13.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 13
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Misdem";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2504");
                screen.ZAxisData = Convert.ToDouble("24.30");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("14.0");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 14
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Felony";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2804");
                screen.ZAxisData = Convert.ToDouble("25.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("15.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 15
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling associated";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("20.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 16
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling directly involved";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                screen.ZAxisData = Convert.ToDouble("15.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 17
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling in any manner";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                screen.ZAxisData = Convert.ToDouble("10.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 18
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism associated";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2501");
                screen.ZAxisData = Convert.ToDouble("14.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("3.9");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 19
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism directly involved";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("25.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 20
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism in any manner";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                screen.ZAxisData = Convert.ToDouble("27.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 21
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Unknown Issue";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                screen.ZAxisData = Convert.ToDouble("27.40");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 22
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                //screen.ScreenText = "Did you withhold information";
                screen.ScreenText = "Is there anything in your background that you are very concerned?";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("35.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.00");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 23
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Did you lie to any question";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("19.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.00");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 24
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Taken Test before";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("13.34");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.3");
                testSession.lstScreenQuestionOnly.Add(screen);
            }



            ////////////////////////////////////////////////////////////////
            ////
            //// CALCULATIONS BEGIN
            ////
            ////////////////////////////////////////////////////////////////
            Anaylyzer18Questions analyzer = new Anaylyzer18Questions();
            analyzer.dTSLAdjust18Q = Convert.ToDouble(numericUpDownTSLAdjust.Value);
            if (checkBoxSpr.Checked)
            {
                analyzer.bTurnOffExtraSPRWeightCalculation = true;
            }
            if (checkBoxDistortLogic.Checked)
            {
                analyzer.bTurnOnVerifyDistortLogic = true;
            }

            //MessageBox.Show("numericUpDownTSLAdjust=" + analyzer.dTSLAdjust26Q.ToString());

            if (globalAutoAdjustSet18Q == 0)
            {
                //Anaylyzer18Questions analyzer = new Anaylyzer18Questions();
                analyzer.AnalysisTest18Questions(ref testSession, ref gResultsFolderPath);
            }
            else
            {
                //Anaylyzer18Questions analyzer = new Anaylyzer18Questions();
                analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                analyzer.AnalysisTest18AutoAdjust(ref testSession, ref gResultsFolderPath);


            }

            // GET THE RISK ANALYSIS

            analyzer.GetRiskAnalysis18QSummary(ref testSession);
            MessageBox.Show(analyzer.strRiskAnalysis.ToString() + " = " + analyzer.strRiskAnalysisVeracityRecommend.ToString());

            //MessageBox.Show("dRDTime=" + analyzer.dRDTime[13].ToString());
            //MessageBox.Show("dZTime=" + analyzer.dZTime[12].ToString());
            //MessageBox.Show("dZTime=" + analyzer.dSumXZ[13].ToString());
            //MessageBox.Show("dABs=" + analyzer.dAbsXZ[12].ToString());
            //MessageBox.Show("dSUBs=" + analyzer.dSubZX[12].ToString());
            //MessageBox.Show("COUNT=" + testSession.lstScreenQuestionOnly.Count);




            //for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            //{
            //    MessageBox.Show("DeltaTimeInSeconds" + testSession.lstScreen[i].DeltaClickTimeInSeconds);
            //}



            ////////////////////////////////////////////////////////////////
            ////
            //// POPULATE FORMS BEGIN
            ////
            ////////////////////////////////////////////////////////////////

            // Generate the SPR binary numbers, if needed, This is basically the SPR1 to SPR 7 high low
            string[] sReactionSPR = new string[30];
            //string[] sDistortQuestion = new string[30];

            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {



                // there are question that we do NOT want to be set
                switch (i)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        sReactionSPR[i] = null;
                        break;
                    case 5:
                    case 6:
                    case 7:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum18(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //sReactionSPR[i] =
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR3[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR3[i]) + "########";
                        break;
                    case 8:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 9:
                    case 10:
                    case 11:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum18(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //                    sReactionSPR[i] =
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "##" +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR4[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR4[i]) + "######";
                        break;
                    case 12:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 13:
                    case 14:
                    case 15:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum18(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //                    sReactionSPR[i] =
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "####" +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR5[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR5[i]) + "####";
                        break;
                    case 16:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 17:
                    case 18:
                    case 19:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum18(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //                    sReactionSPR[i] =
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "######" +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR6[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR6[i]) + "##";
                        break;
                    case 20:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 21:
                    case 22:
                    case 23:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum18(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //                    sReactionSPR[i] =
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "########" +
                        //Convert.ToString(analyzer.bGreaterTSLHighSPR7[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR7[i]);
                        break;
                    case 24:
                    case 25:
                        sReactionSPR[i] = null;
                        break;


                    default:
                        //sReactionSPR[i] = 
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR3[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR3[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR4[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR4[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR5[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR5[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR6[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR6[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR7[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR7[i]);
                        break;
                }
            }


            // Populate the QUESTIONS and NUMBERS
            screen = null;
            string sAnswer = "NO";
            string sReaction = null;
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // Print either a YES or NO for the answer
                if (screen.Answer)
                {
                    sAnswer = "YES";
                }
                else
                {
                    sAnswer = "NO";
                }

                // Print either a HIT, NO, or null
                sReaction = null;
                if (screen.IsHit == true)
                {
                    sReaction = "SPR" + "," + sReactionSPR[i];
                }
                //else
                //{
                //    sReaction = sReactionSPR[i];
                //}

                if (screen.Answer)
                {
                    sReaction = "ADMIT";
                }
                if (screen.IsHit == false)
                {
                    sReaction = "";
                }





                // there are question that we do NOT want to be set to be ADMIT
                switch (i)
                {
                    // for the spacer question, it has to be YES to be valid
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                    case 20:
                    case 24:
                        if (analyzer.iWeightAnswer[i] == 2)
                        {
                            sReaction = "Invalid Answer";
                        }
                        else
                        {
                            sReaction = "";
                        }

                        break;
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 25:
                    case 17:
                        sReaction = "";
                        break;
                    default:
                        break;
                }

                // Determine if the data was distorted for this question
                // This has to be last since we want to know if the data was distorted
                if (analyzer.QuestionThatWasDistortDuringMultipleCodeLogic18[i] > 0)
                {
                    sReaction += "*";
                }

                switch (i)
                {
                    case 0:
                        sReaction = "";
                        textBoxIssues1.Text = screen.ScreenText;
                        textBoxQuestion1.Text = Convert.ToString(i + 1);
                        textBoxAnswers1.Text = sAnswer;
                        textBoxReactionData1.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime1.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction1.Text = sReaction;
                        break;
                    case 1:
                        sReaction = "";
                        textBoxIssues2.Text = screen.ScreenText;
                        textBoxQuestion2.Text = Convert.ToString(i + 1);
                        textBoxAnswers2.Text = sAnswer;
                        textBoxReactionData2.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime2.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction2.Text = sReaction;
                        break;
                    case 2:
                        textBoxIssues3.Text = screen.ScreenText;
                        textBoxQuestion3.Text = Convert.ToString(i + 1);
                        textBoxAnswers3.Text = sAnswer;
                        textBoxReactionData3.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime3.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction3.Text = sReaction;
                        break;
                    case 3:
                        textBoxIssues4.Text = screen.ScreenText;
                        textBoxQuestion4.Text = Convert.ToString(i + 1);
                        textBoxAnswers4.Text = sAnswer;
                        textBoxReactionData4.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime4.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction4.Text = sReaction;
                        break;
                    case 4:
                        textBoxIssues5.Text = screen.ScreenText;
                        textBoxQuestion5.Text = Convert.ToString(i + 1);
                        textBoxAnswers5.Text = sAnswer;
                        textBoxReactionData5.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime5.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction5.Text = sReaction;
                        break;
                    case 5:
                        textBoxIssues6.Text = screen.ScreenText;
                        textBoxQuestion6.Text = Convert.ToString(i + 1);
                        textBoxAnswers6.Text = sAnswer;
                        textBoxReactionData6.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime6.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction6.Text = sReaction;
                        break;
                    case 6:
                        textBoxIssues7.Text = screen.ScreenText;
                        textBoxQuestion7.Text = Convert.ToString(i + 1);
                        textBoxAnswers7.Text = sAnswer;
                        textBoxReactionData7.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime7.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction7.Text = sReaction;
                        break;
                    case 7:
                        textBoxIssues8.Text = screen.ScreenText;
                        textBoxQuestion8.Text = Convert.ToString(i + 1);
                        textBoxAnswers8.Text = sAnswer;
                        textBoxReactionData8.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime8.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction8.Text = sReaction;
                        break;
                    case 8:
                        textBoxIssues9.Text = screen.ScreenText;
                        textBoxQuestion9.Text = Convert.ToString(i + 1);
                        textBoxAnswers9.Text = sAnswer;
                        textBoxReactionData9.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime9.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction9.Text = sReaction;
                        break;
                    case 9:
                        textBoxIssues10.Text = screen.ScreenText;
                        textBoxQuestion10.Text = Convert.ToString(i + 1);
                        textBoxAnswers10.Text = sAnswer;
                        textBoxReactionData10.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime10.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction10.Text = sReaction;
                        break;
                    case 10:
                        textBoxIssues11.Text = screen.ScreenText;
                        textBoxQuestion11.Text = Convert.ToString(i + 1);
                        textBoxAnswers11.Text = sAnswer;
                        textBoxReactionData11.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime11.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction11.Text = sReaction;
                        break;
                    case 11:
                        textBoxIssues12.Text = screen.ScreenText;
                        textBoxQuestion12.Text = Convert.ToString(i + 1);
                        textBoxAnswers12.Text = sAnswer;
                        textBoxReactionData12.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime12.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction12.Text = sReaction;
                        break;
                    case 12:
                        textBoxIssues13.Text = screen.ScreenText;
                        textBoxQuestion13.Text = Convert.ToString(i + 1);
                        textBoxAnswers13.Text = sAnswer;
                        textBoxReactionData13.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime13.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction13.Text = sReaction;
                        break;
                    case 13:
                        textBoxIssues14.Text = screen.ScreenText;
                        textBoxQuestion14.Text = Convert.ToString(i + 1);
                        textBoxAnswers14.Text = sAnswer;
                        textBoxReactionData14.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime14.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction14.Text = sReaction;
                        break;
                    case 14:
                        textBoxIssues15.Text = screen.ScreenText;
                        textBoxQuestion15.Text = Convert.ToString(i + 1);
                        textBoxAnswers15.Text = sAnswer;
                        textBoxReactionData15.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime15.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction15.Text = sReaction;
                        break;
                    case 15:
                        textBoxIssues16.Text = screen.ScreenText;
                        textBoxQuestion16.Text = Convert.ToString(i + 1);
                        textBoxAnswers16.Text = sAnswer;
                        textBoxReactionData16.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime16.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction16.Text = sReaction;
                        break;
                    case 16:
                        textBoxIssues17.Text = screen.ScreenText;
                        textBoxQuestion17.Text = Convert.ToString(i + 1);
                        textBoxAnswers17.Text = sAnswer;
                        textBoxReactionData17.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime17.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction17.Text = sReaction;
                        break;
                    case 17:
                        textBoxIssues18.Text = screen.ScreenText;
                        textBoxQuestion18.Text = Convert.ToString(i + 1);
                        textBoxAnswers18.Text = sAnswer;
                        textBoxReactionData18.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime18.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction18.Text = sReaction;
                        break;
                    case 18:
                        textBoxIssues19.Text = screen.ScreenText;
                        textBoxQuestion19.Text = Convert.ToString(i + 1);
                        textBoxAnswers19.Text = sAnswer;
                        textBoxReactionData19.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime19.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction19.Text = sReaction;
                        break;
                    case 19:
                        textBoxIssues20.Text = screen.ScreenText;
                        textBoxQuestion20.Text = Convert.ToString(i + 1);
                        textBoxAnswers20.Text = sAnswer;
                        textBoxReactionData20.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime20.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction20.Text = sReaction;
                        break;
                    case 20:
                        textBoxIssues21.Text = screen.ScreenText;
                        textBoxQuestion21.Text = Convert.ToString(i + 1);
                        textBoxAnswers21.Text = sAnswer;
                        textBoxReactionData21.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime21.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction21.Text = sReaction;
                        break;
                    case 21:
                        textBoxIssues22.Text = screen.ScreenText;
                        textBoxQuestion22.Text = Convert.ToString(i + 1);
                        textBoxAnswers22.Text = sAnswer;
                        textBoxReactionData22.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime22.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction22.Text = sReaction;
                        break;
                    case 22:
                        textBoxIssues23.Text = screen.ScreenText;
                        textBoxQuestion23.Text = Convert.ToString(i + 1);
                        textBoxAnswers23.Text = sAnswer;
                        textBoxReactionData23.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime23.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction23.Text = sReaction;
                        break;
                    case 23:
                        textBoxIssues24.Text = screen.ScreenText;
                        textBoxQuestion24.Text = Convert.ToString(i + 1);
                        textBoxAnswers24.Text = sAnswer;
                        textBoxReactionData24.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime24.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction24.Text = sReaction;
                        break;
                    case 24:
                        textBoxIssues25.Text = screen.ScreenText;
                        textBoxQuestion25.Text = Convert.ToString(i + 1);
                        textBoxAnswers25.Text = sAnswer;
                        textBoxReactionData25.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime25.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction25.Text = sReaction;
                        break;
                    case 25:
                        textBoxIssues26.Text = screen.ScreenText;
                        textBoxQuestion26.Text = Convert.ToString(i + 1);
                        textBoxAnswers26.Text = sAnswer;
                        textBoxReactionData26.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i], 2));
                        textBoxReadTime26.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds, 2));
                        textBoxReaction26.Text = sReaction;
                        break;

                    default:
                        MessageBox.Show("Unknown case!");
                        break;

                }
            }

            for (int i = 0; i <= 13; i++)
            {
                switch (i)
                {
                    case 0:
                        textBoxReactionDataAvg1.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 1:
                        textBoxReactionDataAvg2.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 2:
                        textBoxReactionDataAvg3.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 3:
                        textBoxReactionDataAvg4.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 4:
                        textBoxReactionDataAvg5.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 5:
                        textBoxReactionDataAvg6.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 6:
                        textBoxReactionDataAvg7.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 7:
                        textBoxReactionDataAvg8.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 8:
                        textBoxReactionDataAvg9.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 9:
                        textBoxReactionDataAvg10.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 10:
                        textBoxReactionDataAvg11.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 11:
                        textBoxReactionDataAvg12.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 12:
                        textBoxReactionDataAvg13.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    case 13:
                        textBoxReactionDataAvg14.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i], 2)) + ", " + Convert.ToString(Math.Round(analyzer.dReadTimeAvg[i], 2));
                        break;
                    default:
                        break;
                }

                //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            }

            // print out the multiple SPR, Hi and low
            //
            for (int i = 0; i <= 6; i++)
            {
                //MessageBox.Show("dRDHi[" + i + "]=" + analyzer.dTSLHiSPR[i]);
                //MessageBox.Show("dRDLow[" + i + "]=" + analyzer.dTSLLowSPR[i]);
                //MessageBox.Show("dRDSTD[" + i + "]=" + analyzer.dSTDSPR[i]);
                switch (i)
                {
                    case 0:
                        textBoxTSLHiLowSTD1.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 1:
                        textBoxTSLHiLowSTD2.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 2:
                        textBoxTSLHiLowSTD3.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 3:
                        textBoxTSLHiLowSTD4.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 4:
                        textBoxTSLHiLowSTD5.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 5:
                        textBoxTSLHiLowSTD6.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 6:
                        textBoxTSLHiLowSTD7.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    default:
                        break;

                }
            }

            // Standard Deviation
            textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            // Read Reaction and the Distortion
            textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            if (analyzer.Distortion)
            {
                textBoxDistortion1.Text = "Likely";
            }
            else
            {
                textBoxDistortion1.Text = "NO";
            }

            // Setup the zones and update the TSL if needed
            textBoxZone1.Text = "Count(" + Convert.ToString(analyzer.ReactionZone1Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[0]) + ")";
            textBoxZone2.Text = "Count(" + Convert.ToString(analyzer.ReactionZone2Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[1]) + ")";
            textBoxZone3.Text = "Count(" + Convert.ToString(analyzer.ReactionZone3Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[2]) + ")";
            textBoxZone4.Text = "Count(" + Convert.ToString(analyzer.ReactionZone4Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[3]) + ")";
            textBoxZone5.Text = "Count(" + Convert.ToString(analyzer.ReactionZone5Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[4]) + ")";
            textBoxZone6.Text = "Count(" + Convert.ToString(analyzer.ReactionZone6Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[5]) + ")";
            textBoxZone7.Text = "Count(" + Convert.ToString(analyzer.ReactionZone7Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[6]) + ")";
            txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;



            // Setup the Read Time Averages
            textBoxRT1.Text = Convert.ToString(Math.Round(analyzer.dReadTimeTotal18, 2));
            textBoxRT2.Text = Convert.ToString(Math.Round(analyzer.dReadTimeAvg18, 2));
            textBoxRT3.Text = Convert.ToString(Math.Round(analyzer.dReadTimeRelevantAvg18, 2));
            textBoxRT4.Text = Convert.ToString(Math.Round(analyzer.dReadTimeIRAvg18, 2));


            ////////////////////////////////////////////////////////////
            ///
            /// PRINT preview for the 18 questions
            ///
            ////////////////////////////////////////////////////////////
            globalTestSession = testSession; // keep a copy if we need to print it
            globalAnaylzer18Q = analyzer; // keep a copy if we need to print it
            //if (b26QPrintPreview)
            //{
            //    PrintManager printMgr = new PrintManager();
            //    printMgr.CreateReportType1PrintPreview(testSession);
            //}
            //else
            //{
            //    b26QPrintPreview = true; // make it true for the next time
            //}

        }

        private bool Parse18QCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen




                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 110)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {


                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "Adaption Q1";
                                        screen.ScreenTextShort = "Adaption Q1";
                                        break;
                                    case 2:
                                        screen.ScreenText = "ADAPT 2";
                                        screen.ScreenTextShort = "Adaption Q2";
                                        break;
                                    case 6:
                                        screen.ScreenText = "Had accident";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 7:
                                        screen.ScreenText = "Failed to report accident";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 8:
                                        screen.ScreenText = "Caused Hit & Run accident";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 10:
                                        screen.ScreenText = "Used Drugs in last 12 months";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 11:
                                        screen.ScreenText = "Used Drugs in last 3 mos.";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 12:
                                        screen.ScreenText = "Used Drugs in last 30 days";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 16:
                                        screen.ScreenText = "Committed Sex Crime";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 15:
                                        screen.ScreenText = "Convicted of Sex Crime";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 14:
                                        screen.ScreenText = "Registered Sex Offender";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 18:
                                        screen.ScreenText = "Arrest or Convict Traffic Vio";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 19:
                                        screen.ScreenText = "Arrest or Convict Misdem";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 20:
                                        screen.ScreenText = "Arrest or Convict Felony";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 22:
                                        screen.ScreenText = "Smuggling associated";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 23:
                                        screen.ScreenText = "Smuggling directly involved";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 24:
                                        screen.ScreenText = "Smuggling in any manner";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 25:
                                        screen.ScreenText = "Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 3:
                                        screen.ScreenText = "Related Issues";
                                        screen.ScreenTextShort = "Adaption Q3";
                                        break;
                                    case 4:
                                        screen.ScreenText = "Answer Truthfully";
                                        screen.ScreenTextShort = "Answer Truthfully";
                                        break;
                                    case 5:
                                        screen.ScreenText = "Day Of Week";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 9:
                                        screen.ScreenText = "Month of Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 13:
                                        screen.ScreenText = "Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 17:
                                        screen.ScreenText = "Day of Week";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 21:
                                        screen.ScreenText = "Month of Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 26:
                                        screen.ScreenText = "Taken Test before";
                                        screen.ScreenTextShort = "Closing Question";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    // screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:

                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    // screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false;
            }

            return bFoundFile;
        }

        public string GetSPRNumberFromQuestionNum18(int iQuestion, ref Anaylyzer18Questions analyzer)
        {

            string strSPRString = "";
            int[] iSPR1High;
            int[] iSPR1Low;

            iSPR1High = new int[10];

            switch (iQuestion)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                case 5:
                case 6:
                case 7:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR3[iQuestion] + analyzer.bGreaterTSLLowSPR3[iQuestion] > 0)
                    {
                        strSPRString += "3";
                    }
                    break;

                case 9:
                case 10:
                case 11:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR4[iQuestion] + analyzer.bGreaterTSLLowSPR4[iQuestion] > 0)
                    {
                        // XXX strSPRString += "4";
                        strSPRString += "3(4)";
                    }


                    break;

                case 13:
                case 14:
                case 15:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR5[iQuestion] + analyzer.bGreaterTSLLowSPR5[iQuestion] > 0)
                    {
                        // strSPRString += "5";
                        strSPRString += "3(5)";
                    }
                    break;

                case 17:
                case 18:
                case 19:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR6[iQuestion] + analyzer.bGreaterTSLLowSPR6[iQuestion] > 0)
                    {
                        // strSPRString += "6";
                        strSPRString += "3(6)";
                    }
                    break;

                case 21:
                case 22:
                case 23:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR7[iQuestion] + analyzer.bGreaterTSLLowSPR7[iQuestion] > 0)
                    {
                        // strSPRString += "7";
                        strSPRString += "3(7)";
                    }
                    break;
                default:
                    break;
            }

            return (strSPRString);
        }











        ////////////////////////////////////////////////////////////
        ///
        /// 26 QUESTIONS
        ///
        ////////////////////////////////////////////////////////////




        private bool Parse26QCSV(ref TestSession testSession)
        {
            // sWatchFile = txtWatchFile.Text;
            bool bFoundFile = false;

            try
            {
                StreamReader filetoread = File.OpenText(sWatchFile);
                String readcontents;
                int i = 0; // to count the number of inputs
                // int TotalRow = 0;
                int TotalCol = 0;
                int row = 0;
                int col = 0;
                int TotalQuestion = 0; // to count the number of questions
                string startDate = null; // start date of the test - used to append to the screen




                // Populate some data for the DLL file to test with
                Veracity.Client.Proxy.Screen screen = null;

                readcontents = filetoread.ReadLine();

                while (readcontents != null)
                {
                    string[] splitout = readcontents.Split(new char[] { ',' });

                    TotalCol = splitout.Length - 1;
                    col = 0; // 1 = question number, 2 = YES or NO, 3 = TouchData, 4 = Start Watch Time, 5 = End Watch Time, 6 = Delta of 5-4
                    // 1= FlowOrderIndex, 2=Answer, 3=ReadTimeinSeconds, 4=BeginAnsClickDate, 5 = EndAnsClickDate, 6=DeltaClickTimeInSeconds

                    screen = new Veracity.Client.Proxy.Screen();
                    TotalQuestion++;
                    foreach (string data in splitout)
                    {
                        if ((i <= 1) || (i > 158)) // skip the first 2 data and last 2 data, start and end test date
                        {
                            if (i == 0) // date
                            {
                                // Convert the yyyyMMdd to dd/MM/yyyy format to append to the screen DateTime format
                                string[] formats = { "yyyyMMdd" };
                                var dt = DateTime.ParseExact(Convert.ToString(data), formats, new CultureInfo("en-US"), DateTimeStyles.None);
                                startDate = dt.ToString("MM/dd/yyyy");
                                // MessageBox.Show(startDate);
                            }
                            // skip the date and time in the CSV file
                        }
                        else
                        {
                            col++;
                            if (col == 7) // we populated the questions, now lets add it to the lists
                            {


                                switch (TotalQuestion)
                                {
                                    case 1:
                                        screen.ScreenText = "Adaption Q1";
                                        screen.ScreenTextShort = "Adaption Q1";
                                        break;
                                    case 2:
                                        screen.ScreenText = "ADAPT 2";
                                        screen.ScreenTextShort = "Adaption Q2";
                                        break;
                                    case 6:
                                        screen.ScreenText = "Had accident";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 7:
                                        screen.ScreenText = "Failed to report accident";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 8:
                                        screen.ScreenText = "Caused Hit & Run accident";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 10:
                                        screen.ScreenText = "Used Drugs in last 12 months";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 11:
                                        screen.ScreenText = "Used Drugs in last 3 mos.";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 12:
                                        screen.ScreenText = "Used Drugs in last 30 days";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 16:
                                        screen.ScreenText = "Committed Sex Crime";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 15:
                                        screen.ScreenText = "Convicted of Sex Crime";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 14:
                                        screen.ScreenText = "Registered Sex Offender";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 18:
                                        screen.ScreenText = "Arrest or Convict Traffic Vio";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 19:
                                        screen.ScreenText = "Arrest or Convict Misdem";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 20:
                                        screen.ScreenText = "Arrest or Convict Felony";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 22:
                                        screen.ScreenText = "Smuggling associated";
                                        screen.ScreenTextShort = "Setup";
                                        break;
                                    case 23:
                                        screen.ScreenText = "Smuggling directly involved";
                                        screen.ScreenTextShort = "Secondary";
                                        break;
                                    case 24:
                                        screen.ScreenText = "Smuggling in any manner";
                                        screen.ScreenTextShort = "Primary";
                                        break;
                                    case 25:
                                        screen.ScreenText = "Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 3:
                                        screen.ScreenText = "Related Issues";
                                        screen.ScreenTextShort = "Adaption Q3";
                                        break;
                                    case 4:
                                        screen.ScreenText = "Answer Truthfully";
                                        screen.ScreenTextShort = "Answer Truthfully";
                                        break;
                                    case 5:
                                        screen.ScreenText = "Day Of Week";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 9:
                                        screen.ScreenText = "Month of Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 13:
                                        screen.ScreenText = "Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 17:
                                        screen.ScreenText = "Day of Week";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 21:
                                        screen.ScreenText = "Month of Year";
                                        screen.ScreenTextShort = "Baseline";
                                        break;
                                    case 26:
                                        screen.ScreenText = "Taken Test before";
                                        screen.ScreenTextShort = "Closing Question";
                                        break;
                                    default:
                                        break;
                                }

                                //MessageBox.Show("Add: Q[" + screen.FlowOrderIndex + "/" + "] Answer[" + screen.Answer + "] ReadInSec[" + screen.ReadTimeInSeconds + "], Delta[" + screen.DeltaClickTimeInSeconds + "] StartClick[" + Convert.ToString(screen.BeginAnsClickDate) + "] EndClick[" + Convert.ToString(screen.EndAnsClickDate) + "]");

                                testSession.lstScreenQuestionOnly.Add(screen);
                                TotalQuestion++;
                                col = 1;
                                screen = new Veracity.Client.Proxy.Screen();
                            }
                            //MessageBox.Show("i=" + i + ", Q " + TotalQuestion + "[col=" + col + "] <" + data + ">");


                            switch (col)
                            {
                                case 1:
                                    screen.FlowOrderIndex = TotalQuestion;
                                    break;
                                case 2:
                                    if (String.Compare(data, " YES") == 0)
                                    {
                                        screen.Answer = true;
                                    }
                                    else
                                    {
                                        screen.Answer = false;
                                    }
                                    break;
                                case 3:
                                    screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    // screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                case 4:

                                    screen.BeginAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    //MessageBox.Show(Convert.ToString(screen.BeginAnsClickDate));
                                    break;
                                case 5:
                                    screen.EndAnsClickDate = Convert.ToDateTime(String.Format("{0}{1}", (startDate), (data)));
                                    break;
                                case 6:
                                    screen.DeltaClickTimeInSeconds = Convert.ToDouble(data);
                                    // screen.ReadTimeInSeconds = Convert.ToDouble(data);
                                    break;
                                default:
                                    MessageBox.Show("CSV file is unrecognizable!");
                                    break;
                            }
                        }
                        i++; // increment the counter
                    }

                    col = 0;
                    row += 1;
                    readcontents = filetoread.ReadLine();
                }
                filetoread.Close();
                bFoundFile = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show("CSV file is unrecognizable!  Could not import data.");
                bFoundFile = false;
            }

            return bFoundFile;
        }

        public string GetSPRNumberFromQuestionNum26(int iQuestion, ref Anaylyzer26Questions analyzer)
        {

            string strSPRString = "";
            int[] iSPR1High;
            int[] iSPR1Low;

            iSPR1High = new int[10];

            switch (iQuestion)
            {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                case 5:
                case 6:
                case 7:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0 )
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR3[iQuestion] + analyzer.bGreaterTSLLowSPR3[iQuestion] > 0)
                    {
                        strSPRString += "3";
                    }
                    break;

                case 9:
                case 10:
                case 11:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR4[iQuestion] + analyzer.bGreaterTSLLowSPR4[iQuestion] > 0)
                    {
                        strSPRString += "4";
                    }


                    break;

                case 13:
                case 14:
                case 15:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR5[iQuestion] + analyzer.bGreaterTSLLowSPR5[iQuestion] > 0)
                    {
                        strSPRString += "5";
                    }
                    break;

                case 17:
                case 18:
                case 19:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR6[iQuestion] + analyzer.bGreaterTSLLowSPR6[iQuestion] > 0)
                    {
                        strSPRString += "6";
                    }
                    break;

                case 21:
                case 22:
                case 23:

                    if (analyzer.bGreaterTSLHighSPR1[iQuestion] + analyzer.bGreaterTSLLowSPR1[iQuestion] > 0)
                    {
                        strSPRString += "1,";
                    }
                    if (analyzer.bGreaterTSLHighSPR2[iQuestion] + analyzer.bGreaterTSLLowSPR2[iQuestion] > 0)
                    {
                        strSPRString += "2,";
                    }
                    if (analyzer.bGreaterTSLHighSPR7[iQuestion] + analyzer.bGreaterTSLLowSPR7[iQuestion] > 0)
                    {
                        strSPRString += "7";
                    }
                    break;
                default:
                    break;
            }

            return (strSPRString);
        }


        private void Load26Data()
        {
            groupBoxAverages.Text = "Summary Averages";
            groupBoxAccidents.Text = "";
            groupBoxAdopt.Text = "";
            groupBoxArrest.Text = "";
            groupBoxDistort.Text = "Distortion";
            groupBoxExit.Text = "";
            groupBoxIllegalDrugs.Text = "";
            groupBoxReadRatio.Text = "Read Ratio";
            groupBoxSexOffenders.Text = "";
            groupBoxSmuggling.Text = "";
            groupBoxTerrorism.Text = "";
            groupBoxUnknowns.Text = "";
            groupBoxZones.Text = "Zones";

            groupBoxAccidents.Show();
            groupBoxAdopt.Show();
            groupBoxArrest.Show();
            groupBoxDistort.Show();
            groupBoxExit.Show();
            groupBoxIllegalDrugs.Show();
            groupBoxReadRatio.Show();
            groupBoxSexOffenders.Show();
            groupBoxSmuggling.Show();
            groupBoxTerrorism.Show();
            groupBoxUnknowns.Show();
            groupBoxZones.Show();

            // not part of the zones
            textBoxAvg1.BackColor = Color.LightGray;
            textBoxAvgTest.BackColor = Color.LightGray;
            textBoxReactionDataAvg1.BackColor = Color.LightGray;
            textBoxReactionDataAvg2.BackColor = Color.LightGray;

            // Modify the color scheme
            //textBoxReactionDataAvg2.BackColor = Color.Aqua;
            textBoxAvg2.BackColor = Color.Aqua;
            textBoxIssues1.BackColor = Color.Aqua ;
            textBoxQuestion1.BackColor = Color.Aqua;
            textBoxAnswers1.BackColor = Color.Aqua;
            textBoxReactionData1.BackColor = Color.Aqua;
            textBoxReadTime1.BackColor = Color.Aqua;
            textBoxReaction1.BackColor = Color.Aqua;

            textBoxIssues2.BackColor = Color.Aqua;
            textBoxQuestion2.BackColor = Color.Aqua;
            textBoxAnswers2.BackColor = Color.Aqua;
            textBoxReactionData2.BackColor = Color.Aqua;
            textBoxReadTime2.BackColor = Color.Aqua;
            textBoxReaction2.BackColor = Color.Aqua;

            textBoxIssues3.BackColor = Color.Aqua;
            textBoxQuestion3.BackColor = Color.Aqua;
            textBoxAnswers3.BackColor = Color.Aqua;
            textBoxReactionData3.BackColor = Color.Aqua;
            textBoxReadTime3.BackColor = Color.Aqua;
            textBoxReaction3.BackColor = Color.Aqua;

            textBoxIssues4.BackColor = Color.Aqua;
            textBoxQuestion4.BackColor = Color.Aqua;
            textBoxAnswers4.BackColor = Color.Aqua;
            textBoxReactionData4.BackColor = Color.Aqua;
            textBoxReadTime4.BackColor = Color.Aqua;
            textBoxReaction4.BackColor = Color.Aqua;

            textBoxZone1.BackColor = Color.DarkGoldenrod;
            textBoxReactionDataAvg3.BackColor = Color.DarkGoldenrod;
            textBoxAvg2.BackColor = Color.DarkGoldenrod;
            textBoxIssues5.BackColor = Color.DarkGoldenrod;
            textBoxQuestion5.BackColor = Color.DarkGoldenrod;
            textBoxAnswers5.BackColor = Color.DarkGoldenrod;
            textBoxReactionData5.BackColor = Color.DarkGoldenrod;
            textBoxReadTime5.BackColor = Color.DarkGoldenrod;
            textBoxReaction5.BackColor = Color.DarkGoldenrod;

            textBoxIssues6.BackColor = Color.DarkGoldenrod;
            textBoxQuestion6.BackColor = Color.DarkGoldenrod;
            textBoxAnswers6.BackColor = Color.DarkGoldenrod;
            textBoxReactionData6.BackColor = Color.DarkGoldenrod;
            textBoxReadTime6.BackColor = Color.DarkGoldenrod;
            textBoxReaction6.BackColor = Color.DarkGoldenrod;

            textBoxIssues7.BackColor = Color.DarkGoldenrod;
            textBoxQuestion7.BackColor = Color.DarkGoldenrod;
            textBoxAnswers7.BackColor = Color.DarkGoldenrod;
            textBoxReactionData7.BackColor = Color.DarkGoldenrod;
            textBoxReadTime7.BackColor = Color.DarkGoldenrod;
            textBoxReaction7.BackColor = Color.DarkGoldenrod;

            textBoxIssues8.BackColor = Color.DarkGoldenrod;
            textBoxQuestion8.BackColor = Color.DarkGoldenrod;
            textBoxAnswers8.BackColor = Color.DarkGoldenrod;
            textBoxReactionData8.BackColor = Color.DarkGoldenrod;
            textBoxReadTime8.BackColor = Color.DarkGoldenrod;
            textBoxReaction8.BackColor = Color.DarkGoldenrod;

            textBoxZone2.BackColor = Color.DarkKhaki;
            textBoxReactionDataAvg4.BackColor = Color.DarkKhaki;
            textBoxAvg3.BackColor = Color.DarkKhaki;
            textBoxIssues9.BackColor = Color.DarkKhaki;
            textBoxQuestion9.BackColor = Color.DarkKhaki;
            textBoxAnswers9.BackColor = Color.DarkKhaki;
            textBoxReactionData9.BackColor = Color.DarkKhaki;
            textBoxReadTime9.BackColor = Color.DarkKhaki;
            textBoxReaction9.BackColor = Color.DarkKhaki;

            textBoxIssues10.BackColor = Color.DarkKhaki;
            textBoxQuestion10.BackColor = Color.DarkKhaki;
            textBoxAnswers10.BackColor = Color.DarkKhaki;
            textBoxReactionData10.BackColor = Color.DarkKhaki;
            textBoxReadTime10.BackColor = Color.DarkKhaki;
            textBoxReaction10.BackColor = Color.DarkKhaki;

            textBoxIssues11.BackColor = Color.DarkKhaki;
            textBoxQuestion11.BackColor = Color.DarkKhaki;
            textBoxAnswers11.BackColor = Color.DarkKhaki;
            textBoxReactionData11.BackColor = Color.DarkKhaki;
            textBoxReadTime11.BackColor = Color.DarkKhaki;
            textBoxReaction11.BackColor = Color.DarkKhaki;

            textBoxIssues12.BackColor = Color.DarkKhaki;
            textBoxQuestion12.BackColor = Color.DarkKhaki;
            textBoxAnswers12.BackColor = Color.DarkKhaki;
            textBoxReactionData12.BackColor = Color.DarkKhaki;
            textBoxReadTime12.BackColor = Color.DarkKhaki;
            textBoxReaction12.BackColor = Color.DarkKhaki;

            textBoxZone3.BackColor = Color.LightBlue;
            textBoxReactionDataAvg5.BackColor = Color.LightBlue;
            textBoxAvg4.BackColor = Color.LightBlue;
            textBoxIssues13.BackColor = Color.LightBlue;
            textBoxQuestion13.BackColor = Color.LightBlue;
            textBoxAnswers13.BackColor = Color.LightBlue;
            textBoxReactionData13.BackColor = Color.LightBlue;
            textBoxReadTime13.BackColor = Color.LightBlue;
            textBoxReaction13.BackColor = Color.LightBlue;

            textBoxIssues14.BackColor = Color.LightBlue;
            textBoxQuestion14.BackColor = Color.LightBlue;
            textBoxAnswers14.BackColor = Color.LightBlue;
            textBoxReactionData14.BackColor = Color.LightBlue;
            textBoxReadTime14.BackColor = Color.LightBlue;
            textBoxReaction14.BackColor = Color.LightBlue;

            textBoxIssues15.BackColor = Color.LightBlue;
            textBoxQuestion15.BackColor = Color.LightBlue;
            textBoxAnswers15.BackColor = Color.LightBlue;
            textBoxReactionData15.BackColor = Color.LightBlue;
            textBoxReadTime15.BackColor = Color.LightBlue;
            textBoxReaction15.BackColor = Color.LightBlue;

            textBoxIssues16.BackColor = Color.LightBlue;
            textBoxQuestion16.BackColor = Color.LightBlue;
            textBoxAnswers16.BackColor = Color.LightBlue;
            textBoxReactionData16.BackColor = Color.LightBlue;
            textBoxReadTime16.BackColor = Color.LightBlue;
            textBoxReaction16.BackColor = Color.LightBlue;

            textBoxZone4.BackColor = Color.GreenYellow;
            textBoxReactionDataAvg6.BackColor = Color.GreenYellow;
            textBoxAvg5.BackColor = Color.GreenYellow;
            textBoxIssues17.BackColor = Color.GreenYellow;
            textBoxQuestion17.BackColor = Color.GreenYellow;
            textBoxAnswers17.BackColor = Color.GreenYellow;
            textBoxReactionData17.BackColor = Color.GreenYellow;
            textBoxReadTime17.BackColor = Color.GreenYellow;
            textBoxReaction17.BackColor = Color.GreenYellow;

            textBoxIssues18.BackColor = Color.GreenYellow;
            textBoxQuestion18.BackColor = Color.GreenYellow;
            textBoxAnswers18.BackColor = Color.GreenYellow;
            textBoxReactionData18.BackColor = Color.GreenYellow;
            textBoxReadTime18.BackColor = Color.GreenYellow;
            textBoxReaction18.BackColor = Color.GreenYellow;

            textBoxIssues19.BackColor = Color.GreenYellow;
            textBoxQuestion19.BackColor = Color.GreenYellow;
            textBoxAnswers19.BackColor = Color.GreenYellow;
            textBoxReactionData19.BackColor = Color.GreenYellow;
            textBoxReadTime19.BackColor = Color.GreenYellow;
            textBoxReaction19.BackColor = Color.GreenYellow;

            textBoxIssues20.BackColor = Color.GreenYellow;
            textBoxQuestion20.BackColor = Color.GreenYellow;
            textBoxAnswers20.BackColor = Color.GreenYellow;
            textBoxReactionData20.BackColor = Color.GreenYellow;
            textBoxReadTime20.BackColor = Color.GreenYellow;
            textBoxReaction20.BackColor = Color.GreenYellow;
            
            
            textBoxZone5.BackColor = Color.Orange;
            textBoxReactionDataAvg7.BackColor = Color.Orange;
            textBoxAvg6.BackColor = Color.Orange;
            textBoxIssues21.BackColor = Color.Orange;
            textBoxQuestion21.BackColor = Color.Orange;
            textBoxAnswers21.BackColor = Color.Orange;
            textBoxReactionData21.BackColor = Color.Orange;
            textBoxReadTime21.BackColor = Color.Orange;
            textBoxReaction21.BackColor = Color.Orange;

            textBoxIssues22.BackColor = Color.Orange;
            textBoxQuestion22.BackColor = Color.Orange;
            textBoxAnswers22.BackColor = Color.Orange;
            textBoxReactionData22.BackColor = Color.Orange;
            textBoxReadTime22.BackColor = Color.Orange;
            textBoxReaction22.BackColor = Color.Orange;

            textBoxIssues23.BackColor = Color.Orange;
            textBoxQuestion23.BackColor = Color.Orange;
            textBoxAnswers23.BackColor = Color.Orange;
            textBoxReactionData23.BackColor = Color.Orange;
            textBoxReadTime23.BackColor = Color.Orange;
            textBoxReaction23.BackColor = Color.Orange;

            textBoxIssues24.BackColor = Color.Orange;
            textBoxQuestion24.BackColor = Color.Orange;
            textBoxAnswers24.BackColor = Color.Orange;
            textBoxReactionData24.BackColor = Color.Orange;
            textBoxReadTime24.BackColor = Color.Orange;
            textBoxReaction24.BackColor = Color.Orange;


            // The rest of the zones we dont use
            textBoxZone6.BackColor = Color.LightGray;
            textBoxZone7.BackColor = Color.LightGray;
            //textBoxReactionDataAvg9.BackColor = Color.LightGray;
            //textBoxAvg7.BackColor = Color.LightGray;


            // TSL Hi, Low, STD boxes or SPR boxes

            textBoxTSLHiLowSTD1.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD2.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD3.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD4.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD5.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD6.BackColor = Color.LightGray;
            textBoxTSLHiLowSTD7.BackColor = Color.LightGray;



            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog1.InitialDirectory = "c:\\Veracity\\report";
            openFileDialog1.Filter = "CSV Files (.csv)|*.csv|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            // Call the ShowDialog method to show the dialog box.
            DialogResult result = openFileDialog1.ShowDialog();

            // Process input if the user clicked OK.
            if (result == DialogResult.OK)
            {
                // MessageBox.Show(openFileDialog1.FileName);
                sWatchFile = openFileDialog1.FileName;
            }


            TestSession testSession = new TestSession();
            int iQuestionNumber = 1;

            // Populate some data for the DLL file to test with
            Veracity.Client.Proxy.Screen screen = null;

            TestSubject subject = new TestSubject();
            subject.FirstName = "Mark";
            subject.LastName = "Anthony";
            subject.DriverLicenseNumber = "A123456789";

            testSession.objTestSubject = subject;

            //testSession.objSubjectIdentification.FirstName = "Mark";
            //testSession.objSubjectIdentification.LastName = "Anthony";
            //testSession.objSubjectIdentification.DriverLicenseNumber = "A1875590";


            if (Parse26QCSV(ref testSession))
            {
                //MessageBox.Show("Finish Loading CSV file...");
            }
            else // if no files found either quit or populate with default data
            {
                MessageBox.Show("File=" + sWatchFile + " not found.  Using default data");

                // Question 1
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "ADAPT 1";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".0901");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:06.9940544");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010  11:18:10.2487344");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.3");
                screen.ZAxisData = Convert.ToDouble("9.00");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 2
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "ADAPT 2";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("11.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("2.2");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 3
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Had accident";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("20.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 4
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Failed to report accident";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                screen.ZAxisData = Convert.ToDouble("15.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 5
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Caused Hit & Run accident";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                screen.ZAxisData = Convert.ToDouble("10.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                testSession.lstScreenQuestionOnly.Add(screen);



                // Question 6
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used M/J in last 12 months";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3001");
                screen.ZAxisData = Convert.ToDouble("14.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("23.9");
                testSession.lstScreenQuestionOnly.Add(screen);



                // Question 7
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used Drugs in last 3 mos.";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("25.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 8
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Used Drugs in last 30 days";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                screen.ZAxisData = Convert.ToDouble("27.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 9
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Committed Sex Crime";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                screen.ZAxisData = Convert.ToDouble("27.40");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 10
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Convicted of Sex Crime";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("35.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 11
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Registered Sex Offender";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("19.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 12
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Traffic Vio";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2903");
                screen.ZAxisData = Convert.ToDouble("13.34");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("13.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 13
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Misdem";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2504");
                screen.ZAxisData = Convert.ToDouble("24.30");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("14.0");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 14
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Arrest or Convict Felony";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2804");
                screen.ZAxisData = Convert.ToDouble("25.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("15.0");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 15
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling associated";
                screen.Answer = true;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1001");
                screen.ZAxisData = Convert.ToDouble("20.23");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("1.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 16
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling directly involved";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1702");
                screen.ZAxisData = Convert.ToDouble("15.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.8");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 17
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Smuggling in any manner";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2604");
                screen.ZAxisData = Convert.ToDouble("10.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("5.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 18
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism associated";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".2501");
                screen.ZAxisData = Convert.ToDouble("14.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("3.9");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 19
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism directly involved";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("25.0");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("33.8");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 20
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Terrorism in any manner";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3205");
                screen.ZAxisData = Convert.ToDouble("27.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("4.2");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 21
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Unknown Issue";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3505");
                screen.ZAxisData = Convert.ToDouble("27.40");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("8.1");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 22
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                //screen.ScreenText = "Did you withhold information";
                screen.ScreenText = "Is there anything in your background that you are very concerned?";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".3104");
                screen.ZAxisData = Convert.ToDouble("35.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.00");
                testSession.lstScreenQuestionOnly.Add(screen);

                // Question 23
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Did you lie to any question";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("19.00");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("12.00");
                testSession.lstScreenQuestionOnly.Add(screen);


                // Question 24
                screen = new Veracity.Client.Proxy.Screen();
                screen.FlowOrderIndex = iQuestionNumber++;
                screen.ScreenText = "Taken Test before";
                screen.Answer = false;
                screen.ReadTimeInSeconds = Convert.ToDouble(".1903");
                screen.ZAxisData = Convert.ToDouble("13.34");
                screen.BeginAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:11.0599008");
                screen.EndAnsClickDate = Convert.ToDateTime("01/01/2010 11:18:13.4733712");
                screen.DeltaClickTimeInSeconds = Convert.ToDouble("11.3");
                testSession.lstScreenQuestionOnly.Add(screen);
            }



            ////////////////////////////////////////////////////////////////
            ////
            //// CALCULATIONS BEGIN
            ////
            ////////////////////////////////////////////////////////////////
            Anaylyzer26Questions analyzer = new Anaylyzer26Questions();
            analyzer.dTSLAdjust26Q = Convert.ToDouble(numericUpDownTSLAdjust.Value);

            //MessageBox.Show("numericUpDownTSLAdjust=" + analyzer.dTSLAdjust26Q.ToString());

            if (globalAutoAdjustSet26Q == 0)
            {
                //Anaylyzer26Questions analyzer = new Anaylyzer26Questions();
                analyzer.AnalysisTest26Questions(ref testSession);
            }
            else
            {
                //Anaylyzer26Questions analyzer = new Anaylyzer26Questions();
                analyzer.eTotalNumReaction = Convert.ToDouble(numericNumberOfHits.Value);
                analyzer.eTSLMinimum = Convert.ToDouble(numericTSLLow.Value);
                analyzer.AnalysisTest26AutoAdjust(ref testSession);
            }


            //for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            //{
            //    MessageBox.Show("DeltaTimeInSeconds" + testSession.lstScreen[i].DeltaClickTimeInSeconds);
            //}



            ////////////////////////////////////////////////////////////////
            ////
            //// POPULATE FORMS BEGIN
            ////
            ////////////////////////////////////////////////////////////////

            // Generate the SPR binary numbers, if needed, This is basically the SPR1 to SPR 7 high low
            string[] sReactionSPR = new string[30];
            //string[] sDistortQuestion = new string[30];
            
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {



                // there are question that we do NOT want to be set
                switch (i)
                {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        sReactionSPR[i] = null;
                        break;
                    case 5:
                    case 6:
                    case 7:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum26(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        //sReactionSPR[i] =
    //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR3[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR3[i]) + "########";
                        break;
                    case 8:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 9:
                    case 10:
                    case 11:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum26(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
    //                    sReactionSPR[i] =
    //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "##" +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR4[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR4[i]) + "######";
                        break;
                    case 12:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 13:
                    case 14:
                    case 15:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum26(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
    //                    sReactionSPR[i] =
    //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "####" +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR5[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR5[i]) + "####";
                        break;
                    case 16:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 17:
                    case 18:
                    case 19:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum26(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
    //                    sReactionSPR[i] =
    //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "######" +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR6[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR6[i]) + "##";
                        break;
                    case 20:
                        sReactionSPR[i] = null;
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
                        break;
                    case 21:
                    case 22:
                    case 23:
                        sReactionSPR[i] = GetSPRNumberFromQuestionNum26(i, ref analyzer);
                        //sDistortQuestion[i] = DetermineIfDistorted26(i, ref analyzer);
    //                    sReactionSPR[i] =
    //Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) + "########" +
    //Convert.ToString(analyzer.bGreaterTSLHighSPR7[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR7[i]);
                        break;
                    case 24:
                    case 25:
                        sReactionSPR[i] = null;
                        break;

                        
                    default:
                        //sReactionSPR[i] = 
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR1[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR1[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR2[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR2[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR3[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR3[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR4[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR4[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR5[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR5[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR6[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR6[i]) +
                        //    Convert.ToString(analyzer.bGreaterTSLHighSPR7[i]) + Convert.ToString(analyzer.bGreaterTSLLowSPR7[i]);
                        break;
                }
            }


            // Populate the QUESTIONS and NUMBERS
            screen = null;
            string sAnswer = "NO";
            string sReaction = null;
            for (int i = 0; i < testSession.lstScreenQuestionOnly.Count; i++)
            {
                screen = (Veracity.Client.Proxy.Screen)testSession.lstScreenQuestionOnly[i];

                // Print either a YES or NO for the answer
                if (screen.Answer)
                {
                    sAnswer = "YES";
                }
                else
                {
                    sAnswer = "NO";
                }

                // Print either a HIT, NO, or null
                sReaction = null;
                if (screen.IsHit == true)
                {
                    sReaction = "SPR" + "," + sReactionSPR[i];
                }
                //else
                //{
                //    sReaction = sReactionSPR[i];
                //}

                if (screen.Answer)
                {
                    sReaction = "ADMIT";
                }
                if (screen.IsHit == false)
                {
                    sReaction = "";
                }





                // there are question that we do NOT want to be set to be ADMIT
                switch (i)
                {
                    // for the spacer question, it has to be YES to be valid
                    case 4:
                    case 8:
                    case 12:
                    case 16:
                    case 20:
                    case 24:
                        if (analyzer.iWeightAnswer[i] == 2)
                        {
                            sReaction = "Invalid Answer";
                        }
                        else
                        {
                            sReaction = "";
                        }
                        
                        break;
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 25:
                        sReaction = "";
                        break;
                    default:
                        break;
                }

                // Determine if the data was distorted for this question
                // This has to be last since we want to know if the data was distorted
                if (analyzer.QuestionThatWasDistortDuringMultipleCodeLogic26[i] > 0)
                {
                    sReaction += "*";
                }

                switch (i)
                {
                    case 0:
                        sReaction = "";
                        textBoxIssues1.Text = screen.ScreenText;
                        textBoxQuestion1.Text = Convert.ToString(i + 1);
                        textBoxAnswers1.Text = sAnswer;
                        textBoxReactionData1.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime1.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction1.Text = sReaction;
                        break;
                    case 1:
                        sReaction = "";
                        textBoxIssues2.Text = screen.ScreenText;
                        textBoxQuestion2.Text = Convert.ToString(i + 1);
                        textBoxAnswers2.Text = sAnswer;
                        textBoxReactionData2.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime2.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction2.Text = sReaction;
                        break;
                    case 2:
                        textBoxIssues3.Text = screen.ScreenText;
                        textBoxQuestion3.Text = Convert.ToString(i + 1);
                        textBoxAnswers3.Text = sAnswer;
                        textBoxReactionData3.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime3.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction3.Text = sReaction;
                        break;
                    case 3:
                        textBoxIssues4.Text = screen.ScreenText;
                        textBoxQuestion4.Text = Convert.ToString(i + 1);
                        textBoxAnswers4.Text = sAnswer;
                        textBoxReactionData4.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime4.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction4.Text = sReaction;
                        break;
                    case 4:
                        textBoxIssues5.Text = screen.ScreenText;
                        textBoxQuestion5.Text = Convert.ToString(i + 1);
                        textBoxAnswers5.Text = sAnswer;
                        textBoxReactionData5.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime5.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction5.Text = sReaction;
                        break;
                    case 5:
                        textBoxIssues6.Text = screen.ScreenText;
                        textBoxQuestion6.Text = Convert.ToString(i + 1);
                        textBoxAnswers6.Text = sAnswer;
                        textBoxReactionData6.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime6.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction6.Text = sReaction;
                        break;
                    case 6:
                        textBoxIssues7.Text = screen.ScreenText;
                        textBoxQuestion7.Text = Convert.ToString(i + 1);
                        textBoxAnswers7.Text = sAnswer;
                        textBoxReactionData7.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime7.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction7.Text = sReaction;
                        break;
                    case 7:
                        textBoxIssues8.Text = screen.ScreenText;
                        textBoxQuestion8.Text = Convert.ToString(i + 1);
                        textBoxAnswers8.Text = sAnswer;
                        textBoxReactionData8.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime8.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction8.Text = sReaction;
                        break;
                    case 8:
                        textBoxIssues9.Text = screen.ScreenText;
                        textBoxQuestion9.Text = Convert.ToString(i + 1);
                        textBoxAnswers9.Text = sAnswer;
                        textBoxReactionData9.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime9.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction9.Text = sReaction;
                        break;
                    case 9:
                        textBoxIssues10.Text = screen.ScreenText;
                        textBoxQuestion10.Text = Convert.ToString(i + 1);
                        textBoxAnswers10.Text = sAnswer;
                        textBoxReactionData10.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime10.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction10.Text = sReaction;
                        break;
                    case 10:
                        textBoxIssues11.Text = screen.ScreenText;
                        textBoxQuestion11.Text = Convert.ToString(i + 1);
                        textBoxAnswers11.Text = sAnswer;
                        textBoxReactionData11.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime11.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction11.Text = sReaction;
                        break;
                    case 11:
                        textBoxIssues12.Text = screen.ScreenText;
                        textBoxQuestion12.Text = Convert.ToString(i + 1);
                        textBoxAnswers12.Text = sAnswer;
                        textBoxReactionData12.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime12.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction12.Text = sReaction;
                        break;
                    case 12:
                        textBoxIssues13.Text = screen.ScreenText;
                        textBoxQuestion13.Text = Convert.ToString(i + 1);
                        textBoxAnswers13.Text = sAnswer;
                        textBoxReactionData13.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime13.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction13.Text = sReaction;
                        break;
                    case 13:
                        textBoxIssues14.Text = screen.ScreenText;
                        textBoxQuestion14.Text = Convert.ToString(i + 1);
                        textBoxAnswers14.Text = sAnswer;
                        textBoxReactionData14.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime14.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction14.Text = sReaction;
                        break;
                    case 14:
                        textBoxIssues15.Text = screen.ScreenText;
                        textBoxQuestion15.Text = Convert.ToString(i + 1);
                        textBoxAnswers15.Text = sAnswer;
                        textBoxReactionData15.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime15.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction15.Text = sReaction;
                        break;
                    case 15:
                        textBoxIssues16.Text = screen.ScreenText;
                        textBoxQuestion16.Text = Convert.ToString(i + 1);
                        textBoxAnswers16.Text = sAnswer;
                        textBoxReactionData16.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime16.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction16.Text = sReaction;
                        break;
                    case 16:
                        textBoxIssues17.Text = screen.ScreenText;
                        textBoxQuestion17.Text = Convert.ToString(i + 1);
                        textBoxAnswers17.Text = sAnswer;
                        textBoxReactionData17.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime17.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction17.Text = sReaction;
                        break;
                    case 17:
                        textBoxIssues18.Text = screen.ScreenText;
                        textBoxQuestion18.Text = Convert.ToString(i + 1);
                        textBoxAnswers18.Text = sAnswer;
                        textBoxReactionData18.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime18.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction18.Text = sReaction;
                        break;
                    case 18:
                        textBoxIssues19.Text = screen.ScreenText;
                        textBoxQuestion19.Text = Convert.ToString(i + 1);
                        textBoxAnswers19.Text = sAnswer;
                        textBoxReactionData19.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime19.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction19.Text = sReaction;
                        break;
                    case 19:
                        textBoxIssues20.Text = screen.ScreenText;
                        textBoxQuestion20.Text = Convert.ToString(i + 1);
                        textBoxAnswers20.Text = sAnswer;
                        textBoxReactionData20.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime20.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction20.Text = sReaction;
                        break;
                    case 20:
                        textBoxIssues21.Text = screen.ScreenText;
                        textBoxQuestion21.Text = Convert.ToString(i + 1);
                        textBoxAnswers21.Text = sAnswer;
                        textBoxReactionData21.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime21.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction21.Text = sReaction;
                        break;
                    case 21:
                        textBoxIssues22.Text = screen.ScreenText;
                        textBoxQuestion22.Text = Convert.ToString(i + 1);
                        textBoxAnswers22.Text = sAnswer;
                        textBoxReactionData22.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime22.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction22.Text = sReaction;
                        break;
                    case 22:
                        textBoxIssues23.Text = screen.ScreenText;
                        textBoxQuestion23.Text = Convert.ToString(i + 1);
                        textBoxAnswers23.Text = sAnswer;
                        textBoxReactionData23.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime23.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction23.Text = sReaction;
                        break;
                    case 23:
                        textBoxIssues24.Text = screen.ScreenText;
                        textBoxQuestion24.Text = Convert.ToString(i + 1);
                        textBoxAnswers24.Text = sAnswer;
                        textBoxReactionData24.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime24.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction24.Text = sReaction;
                        break;
                    case 24:
                        textBoxIssues25.Text = screen.ScreenText;
                        textBoxQuestion25.Text = Convert.ToString(i + 1);
                        textBoxAnswers25.Text = sAnswer;
                        textBoxReactionData25.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime25.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction25.Text = sReaction;
                        break;
                    case 25:
                        textBoxIssues26.Text = screen.ScreenText;
                        textBoxQuestion26.Text = Convert.ToString(i + 1);
                        textBoxAnswers26.Text = sAnswer;
                        textBoxReactionData26.Text = Convert.ToString(Math.Round(analyzer.dSumEI[i],2));
                        textBoxReadTime26.Text = Convert.ToString(Math.Round(screen.DeltaClickTimeInSeconds,2));
                        textBoxReaction26.Text = sReaction;
                        break;

                    default:
                        MessageBox.Show("Unknown case!");
                        break;

                }
            }

            for (int i = 0; i <= 13; i++)
            {
                switch (i)
                {
                    case 0:
                        textBoxReactionDataAvg1.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 1:
                        textBoxReactionDataAvg2.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 2:
                        textBoxReactionDataAvg3.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 3:
                        textBoxReactionDataAvg4.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 4:
                        textBoxReactionDataAvg5.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 5: 
                        textBoxReactionDataAvg6.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 6:
                        textBoxReactionDataAvg7.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 7:
                        textBoxReactionDataAvg8.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 8:
                        textBoxReactionDataAvg9.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 9:
                        textBoxReactionDataAvg10.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 10:
                        textBoxReactionDataAvg11.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 11:
                        textBoxReactionDataAvg12.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 12:
                        textBoxReactionDataAvg13.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    case 13:
                        textBoxReactionDataAvg14.Text = Convert.ToString(Math.Round(analyzer.dRDTimeAvg[i],2));
                        break;
                    default:
                        break;
                }

                //MessageBox.Show("dRDTimeAvg[" + i + "]=" + analyzer.dRDTimeAvg[i]);
            }

            // print out the multiple SPR, Hi and low
            //
            for (int i = 0; i <= 6; i++)
            {
                //MessageBox.Show("dRDHi[" + i + "]=" + analyzer.dTSLHiSPR[i]);
                //MessageBox.Show("dRDLow[" + i + "]=" + analyzer.dTSLLowSPR[i]);
                //MessageBox.Show("dRDSTD[" + i + "]=" + analyzer.dSTDSPR[i]);
                switch (i)
                {
                    case 0:
                        textBoxTSLHiLowSTD1.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i],2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i],2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i],2));
                        break;
                    case 1:
                        textBoxTSLHiLowSTD2.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 2:
                        textBoxTSLHiLowSTD3.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 3:
                        textBoxTSLHiLowSTD4.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 4:
                        textBoxTSLHiLowSTD5.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 5:
                        textBoxTSLHiLowSTD6.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    case 6:
                        textBoxTSLHiLowSTD7.Text = Convert.ToString(Math.Round(analyzer.dTSLHiSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dTSLLowSPR[i], 2)) + "/" + Convert.ToString(Math.Round(analyzer.dSTDSPR[i], 2));
                        break;
                    default:
                        break;

                }
            }

            // Standard Deviation
            textBoxStandardDeviation.Text = Convert.ToString(analyzer.StandardDeviation);

            // Read Reaction and the Distortion
            textBoxReadRatio1.Text = Convert.ToString(analyzer.iReadReaction);
            textBoxReadRatio2.Text = Convert.ToString(analyzer.ReadReaction1);
            textBoxReadRatio3.Text = Convert.ToString(analyzer.ReadReaction2);
            if (analyzer.Distortion)
            {
                textBoxDistortion1.Text = "Likely";
            }
            else
            {
                textBoxDistortion1.Text = "NO";
            }

            // Setup the zones and update the TSL if needed
            textBoxZone1.Text = "Count(" + Convert.ToString(analyzer.ReactionZone1Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[0]) + ")";
            textBoxZone2.Text = "Count(" + Convert.ToString(analyzer.ReactionZone2Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[1]) + ")";
            textBoxZone3.Text = "Count(" + Convert.ToString(analyzer.ReactionZone3Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[2]) + ")";
            textBoxZone4.Text = "Count(" + Convert.ToString(analyzer.ReactionZone4Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[3]) + ")";
            textBoxZone5.Text = "Count(" + Convert.ToString(analyzer.ReactionZone5Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[4]) + ")";
            textBoxZone6.Text = "Count(" + Convert.ToString(analyzer.ReactionZone6Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[5]) + ")";
            textBoxZone7.Text = "Count(" + Convert.ToString(analyzer.ReactionZone7Counter) + ") Weight(" + Convert.ToString(analyzer.iZoneWeightTotal[6]) + ")";
            txtTSLHi.Text = Convert.ToString(analyzer.dTSLHIGH);
            txtTSLLo.Text = Convert.ToString(analyzer.dTSLLow);
            numericUpDownTSLAdjust.Value = analyzer.TSLPercentage;

            

            // Setup the Read Time Averages
            textBoxRT1.Text = Convert.ToString(Math.Round(analyzer.dReadTimeTotal26,2));
            textBoxRT2.Text = Convert.ToString(Math.Round(analyzer.dReadTimeAvg26,2));
            textBoxRT3.Text = Convert.ToString(Math.Round(analyzer.dReadTimeRelevantAvg26,2));
            textBoxRT4.Text = Convert.ToString(Math.Round(analyzer.dReadTimeIRAvg26, 2));
            

            ////////////////////////////////////////////////////////////
            ///
            /// PRINT preview for the 26 questions
            ///
            ////////////////////////////////////////////////////////////
            globalTestSession = testSession; // keep a copy if we need to print it
            globalAnaylzer26Q = analyzer; // keep a copy if we need to print it
            //if (b26QPrintPreview)
            //{
            //    PrintManager printMgr = new PrintManager();
            //    printMgr.CreateReportType1PrintPreview(testSession);
            //}
            //else
            //{
            //    b26QPrintPreview = true; // make it true for the next time
            //}

        }














        private void btnLoad26Data_Click(object sender, EventArgs e)
        {
            

            textBoxReactionData.Text = "SUM";
            Load26Data();
            bGlobalFirstSetOfDataLoaded = true;
            bGlobal26QDataParsed = true;
            bGlobal24QDataParsed = false;
            bGlobal15QDataParsed = false;
            bGlobalKOTDataParsed = false;

            // groupBoxAdopt;
        }

        private void groupBoxExit_Enter(object sender, EventArgs e)
        {

        }

        private void btnLoad18Data_Click(object sender, EventArgs e)
        {
            textBoxReactionData.Text = "R/D";
            Load18Data();
            bGlobalFirstSetOfDataLoaded = true;
            bGlobal18QDataParsed = true;
            bGlobal24QDataParsed = false;
            bGlobal15QDataParsed = false;
            bGlobalKOTDataParsed = false;
            //Calculate24Data();
        }

        private void btnPrintPreview18Q_Click(object sender, EventArgs e)
        {
            PrintManager printMgr = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_1;
            string gtest = "";
            printMgr.CreateReportType1PrintPreview(globalTestSession, globalAnaylzer18Q, gtest);

            PrintManager printMgr2 = new PrintManager();
            // MessageBox.Show("Count=" + globalTestSession.lstScreenQuestionOnly.Count());
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2;
            printMgr2.CreateReportType1PrintPreview(globalTestSession, globalAnaylzer18Q, gtest);
        }

        private void btnPrint18QData_Click(object sender, EventArgs e)
        {
            b18QPrintPreview = false;  // dont do a print preview when calling Load24Data
            Load18Data();
            PrintManager printMgr = new PrintManager();
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_1; // print the first report
            // XXX printMgr.PrintReportToDefaultPrinter(globalTestSession, globalAnaylzer18Q);


            PrintManager printMgr2 = new PrintManager();
            globalTestSession.ReportType = TestSession.REPORT_TYPE.REPORT_18Q_2; // print the second report
            // XXX printMgr2.PrintReportToDefaultPrinter(globalTestSession, globalAnaylzer18Q);

        }

        private void btnAutoAdjust18_Click(object sender, EventArgs e)
        {
            if (globalAutoAdjustSet18Q == 1)
            {
                globalAutoAdjustSet18Q = 0;
                btnAutoAdjust18.BackColor = Color.AliceBlue;
            }
            else
            {
                globalAutoAdjustSet18Q = 1;
                btnAutoAdjust18.BackColor = Color.Red;
            }
        }

        //private void btnPrint26QData_2_Click(object sender, EventArgs e)
        //{

        //}

        //private void btnPrintPreview26Q_2_Click(object sender, EventArgs e)
        //{

        //}





        //private void textBox13_TextChanged(object sender, EventArgs e)
        //{

        //}


    }
}
