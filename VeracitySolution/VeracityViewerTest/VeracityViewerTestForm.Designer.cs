﻿namespace VeracityViewerTest
{
    partial class VeracityViewerTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VeracityViewerTestForm));
            this.BtnLoadCsv = new System.Windows.Forms.Button();
            this.textBoxIssues1 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion1 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers1 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData1 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime1 = new System.Windows.Forms.TextBox();
            this.textBoxReaction1 = new System.Windows.Forms.TextBox();
            this.textBoxReaction = new System.Windows.Forms.TextBox();
            this.textBoxReadTime = new System.Windows.Forms.TextBox();
            this.textBoxReactionData = new System.Windows.Forms.TextBox();
            this.textBoxAnswers = new System.Windows.Forms.TextBox();
            this.textBoxQuestion = new System.Windows.Forms.TextBox();
            this.textBoxIssues = new System.Windows.Forms.TextBox();
            this.textBoxReaction2 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime2 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData2 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers2 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion2 = new System.Windows.Forms.TextBox();
            this.textBoxIssues2 = new System.Windows.Forms.TextBox();
            this.textBoxReaction3 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime3 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData3 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers3 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion3 = new System.Windows.Forms.TextBox();
            this.textBoxIssues3 = new System.Windows.Forms.TextBox();
            this.textBoxReaction4 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime4 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData4 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers4 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion4 = new System.Windows.Forms.TextBox();
            this.textBoxIssues4 = new System.Windows.Forms.TextBox();
            this.textBoxReaction5 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime5 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData5 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers5 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion5 = new System.Windows.Forms.TextBox();
            this.textBoxIssues5 = new System.Windows.Forms.TextBox();
            this.textBoxReaction6 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime6 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData6 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers6 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion6 = new System.Windows.Forms.TextBox();
            this.textBoxIssues6 = new System.Windows.Forms.TextBox();
            this.textBoxReaction7 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime7 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData7 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers7 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion7 = new System.Windows.Forms.TextBox();
            this.textBoxIssues7 = new System.Windows.Forms.TextBox();
            this.textBoxReaction8 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime8 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData8 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers8 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion8 = new System.Windows.Forms.TextBox();
            this.textBoxIssues8 = new System.Windows.Forms.TextBox();
            this.textBoxReaction9 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime9 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData9 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers9 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion9 = new System.Windows.Forms.TextBox();
            this.textBoxIssues9 = new System.Windows.Forms.TextBox();
            this.textBoxReaction10 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime10 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData10 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers10 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion10 = new System.Windows.Forms.TextBox();
            this.textBoxIssues10 = new System.Windows.Forms.TextBox();
            this.textBoxReaction11 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime11 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData11 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers11 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion11 = new System.Windows.Forms.TextBox();
            this.textBoxIssues11 = new System.Windows.Forms.TextBox();
            this.textBoxReaction12 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime12 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData12 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers12 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion12 = new System.Windows.Forms.TextBox();
            this.textBoxIssues12 = new System.Windows.Forms.TextBox();
            this.textBoxReaction13 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime13 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData13 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers13 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion13 = new System.Windows.Forms.TextBox();
            this.textBoxIssues13 = new System.Windows.Forms.TextBox();
            this.textBoxReaction14 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime14 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData14 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers14 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion14 = new System.Windows.Forms.TextBox();
            this.textBoxIssues14 = new System.Windows.Forms.TextBox();
            this.textBoxReaction15 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime15 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData15 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers15 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion15 = new System.Windows.Forms.TextBox();
            this.textBoxIssues15 = new System.Windows.Forms.TextBox();
            this.txtTSLHi = new System.Windows.Forms.TextBox();
            this.txtTSLLo = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg1 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg2 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg3 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg4 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg5 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg13 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg14 = new System.Windows.Forms.TextBox();
            this.textBoxStandardDeviation = new System.Windows.Forms.TextBox();
            this.textBoxAvgTest = new System.Windows.Forms.TextBox();
            this.textBoxAvg1 = new System.Windows.Forms.TextBox();
            this.textBoxAvg2 = new System.Windows.Forms.TextBox();
            this.textBoxAvg3 = new System.Windows.Forms.TextBox();
            this.textBoxAvg4 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.btnLoad24Data = new System.Windows.Forms.Button();
            this.textBoxReaction16 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime16 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData16 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers16 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion16 = new System.Windows.Forms.TextBox();
            this.textBoxIssues16 = new System.Windows.Forms.TextBox();
            this.textBoxReaction17 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime17 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData17 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers17 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion17 = new System.Windows.Forms.TextBox();
            this.textBoxIssues17 = new System.Windows.Forms.TextBox();
            this.textBoxReaction19 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime19 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData19 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers19 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion19 = new System.Windows.Forms.TextBox();
            this.textBoxIssues19 = new System.Windows.Forms.TextBox();
            this.textBoxReaction18 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime18 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData18 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers18 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion18 = new System.Windows.Forms.TextBox();
            this.textBoxIssues18 = new System.Windows.Forms.TextBox();
            this.textBoxReaction23 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime23 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData23 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers23 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion23 = new System.Windows.Forms.TextBox();
            this.textBoxIssues23 = new System.Windows.Forms.TextBox();
            this.textBoxReaction22 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime22 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData22 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers22 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion22 = new System.Windows.Forms.TextBox();
            this.textBoxIssues22 = new System.Windows.Forms.TextBox();
            this.textBoxReaction21 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime21 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData21 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers21 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion21 = new System.Windows.Forms.TextBox();
            this.textBoxIssues21 = new System.Windows.Forms.TextBox();
            this.textBoxReaction20 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime20 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData20 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers20 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion20 = new System.Windows.Forms.TextBox();
            this.textBoxIssues20 = new System.Windows.Forms.TextBox();
            this.textBoxReaction24 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime24 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData24 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers24 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion24 = new System.Windows.Forms.TextBox();
            this.textBoxIssues24 = new System.Windows.Forms.TextBox();
            this.textBoxReadRatio1 = new System.Windows.Forms.TextBox();
            this.textBoxReadRatio2 = new System.Windows.Forms.TextBox();
            this.textBoxReadRatio3 = new System.Windows.Forms.TextBox();
            this.textBoxDistortion1 = new System.Windows.Forms.TextBox();
            this.textBoxZone1 = new System.Windows.Forms.TextBox();
            this.textBoxZone2 = new System.Windows.Forms.TextBox();
            this.textBoxZone3 = new System.Windows.Forms.TextBox();
            this.textBoxZone4 = new System.Windows.Forms.TextBox();
            this.textBoxZone5 = new System.Windows.Forms.TextBox();
            this.textBoxZone6 = new System.Windows.Forms.TextBox();
            this.textBoxZone7 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHi = new System.Windows.Forms.TextBox();
            this.textBoxTSLLo = new System.Windows.Forms.TextBox();
            this.btnPrint24QData = new System.Windows.Forms.Button();
            this.btnPrint15QData = new System.Windows.Forms.Button();
            this.groupBoxZones = new System.Windows.Forms.GroupBox();
            this.groupBoxExit = new System.Windows.Forms.GroupBox();
            this.textBoxAnswers26 = new System.Windows.Forms.TextBox();
            this.textBoxIssues26 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion26 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData26 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime26 = new System.Windows.Forms.TextBox();
            this.textBoxReaction26 = new System.Windows.Forms.TextBox();
            this.textBoxAnswers25 = new System.Windows.Forms.TextBox();
            this.textBoxIssues25 = new System.Windows.Forms.TextBox();
            this.textBoxQuestion25 = new System.Windows.Forms.TextBox();
            this.textBoxReactionData25 = new System.Windows.Forms.TextBox();
            this.textBoxReadTime25 = new System.Windows.Forms.TextBox();
            this.textBoxReaction25 = new System.Windows.Forms.TextBox();
            this.groupBoxUnknowns = new System.Windows.Forms.GroupBox();
            this.groupBoxTerrorism = new System.Windows.Forms.GroupBox();
            this.groupBoxSexOffenders = new System.Windows.Forms.GroupBox();
            this.groupBoxSmuggling = new System.Windows.Forms.GroupBox();
            this.groupBoxArrest = new System.Windows.Forms.GroupBox();
            this.groupBoxAccidents = new System.Windows.Forms.GroupBox();
            this.groupBoxIllegalDrugs = new System.Windows.Forms.GroupBox();
            this.groupBoxAdopt = new System.Windows.Forms.GroupBox();
            this.groupBoxReadRatio = new System.Windows.Forms.GroupBox();
            this.groupBoxDistort = new System.Windows.Forms.GroupBox();
            this.groupBoxAverages = new System.Windows.Forms.GroupBox();
            this.textBoxReactionDataAvg12 = new System.Windows.Forms.TextBox();
            this.textBoxAvg11 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg11 = new System.Windows.Forms.TextBox();
            this.textBoxAvg10 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg10 = new System.Windows.Forms.TextBox();
            this.textBoxAvg9 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg9 = new System.Windows.Forms.TextBox();
            this.textBoxAvg8 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg8 = new System.Windows.Forms.TextBox();
            this.textBoxAvg7 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg7 = new System.Windows.Forms.TextBox();
            this.textBoxAvg6 = new System.Windows.Forms.TextBox();
            this.textBoxReactionDataAvg6 = new System.Windows.Forms.TextBox();
            this.textBoxAvg5 = new System.Windows.Forms.TextBox();
            this.groupBoxControls = new System.Windows.Forms.GroupBox();
            this.checkBoxDistortLogic = new System.Windows.Forms.CheckBox();
            this.checkBoxSpr = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnAutoAdjust18 = new System.Windows.Forms.Button();
            this.btnLoad18Data = new System.Windows.Forms.Button();
            this.btnPrint18QData = new System.Windows.Forms.Button();
            this.btnPrintPreview18Q = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnPrint26QData_2 = new System.Windows.Forms.Button();
            this.btnPrintPreview26Q_2 = new System.Windows.Forms.Button();
            this.btnAutoAdjust26 = new System.Windows.Forms.Button();
            this.btnLoad26Data = new System.Windows.Forms.Button();
            this.btnPrint26QData = new System.Windows.Forms.Button();
            this.btnPrintPreview26Q = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadKOTData = new System.Windows.Forms.Button();
            this.btnPrintKOT = new System.Windows.Forms.Button();
            this.btnPrintPreviewKOT = new System.Windows.Forms.Button();
            this.groupBoxTSLAdjust = new System.Windows.Forms.GroupBox();
            this.numericNumberOfHits = new System.Windows.Forms.NumericUpDown();
            this.txtNumHits = new System.Windows.Forms.TextBox();
            this.txtTSLLowPercentage = new System.Windows.Forms.TextBox();
            this.numericTSLLow = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownTSLAdjust = new System.Windows.Forms.NumericUpDown();
            this.checkBoxPrintGraph = new System.Windows.Forms.CheckBox();
            this.groupBox15Q = new System.Windows.Forms.GroupBox();
            this.btnLoadBatch = new System.Windows.Forms.Button();
            this.btnAutoAdjust15 = new System.Windows.Forms.Button();
            this.btnPrintPreview15Q = new System.Windows.Forms.Button();
            this.groupBox24Q = new System.Windows.Forms.GroupBox();
            this.btnAutoAdjust24 = new System.Windows.Forms.Button();
            this.btnPrintPreview24Q = new System.Windows.Forms.Button();
            this.textBoxSecond4 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxTSLLowKOT = new System.Windows.Forms.TextBox();
            this.textBoxTSLLowLabelKOT = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiKOT = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLabelKOT = new System.Windows.Forms.TextBox();
            this.textBoxRTAverageKOT = new System.Windows.Forms.TextBox();
            this.textBoxLabelRTAverageKOT = new System.Windows.Forms.TextBox();
            this.textBoxRDAverageKOT = new System.Windows.Forms.TextBox();
            this.textBoxLabelRDAverageKOT = new System.Windows.Forms.TextBox();
            this.textBoxSTDKOT = new System.Windows.Forms.TextBox();
            this.textBoxLabelStd = new System.Windows.Forms.TextBox();
            this.textBoxQ16 = new System.Windows.Forms.TextBox();
            this.textBoxFourth4 = new System.Windows.Forms.TextBox();
            this.textBoxFirst4 = new System.Windows.Forms.TextBox();
            this.textBoxThird4 = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxTSLHiLowSTD1 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD2 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD6 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD7 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD3 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD5 = new System.Windows.Forms.TextBox();
            this.textBoxTSLHiLowSTD4 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxRT4 = new System.Windows.Forms.TextBox();
            this.textBoxRT1 = new System.Windows.Forms.TextBox();
            this.textBoxRT2 = new System.Windows.Forms.TextBox();
            this.textBoxRT3 = new System.Windows.Forms.TextBox();
            this.groupBoxZones.SuspendLayout();
            this.groupBoxExit.SuspendLayout();
            this.groupBoxUnknowns.SuspendLayout();
            this.groupBoxTerrorism.SuspendLayout();
            this.groupBoxSexOffenders.SuspendLayout();
            this.groupBoxSmuggling.SuspendLayout();
            this.groupBoxArrest.SuspendLayout();
            this.groupBoxAccidents.SuspendLayout();
            this.groupBoxIllegalDrugs.SuspendLayout();
            this.groupBoxAdopt.SuspendLayout();
            this.groupBoxReadRatio.SuspendLayout();
            this.groupBoxDistort.SuspendLayout();
            this.groupBoxAverages.SuspendLayout();
            this.groupBoxControls.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxTSLAdjust.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNumberOfHits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTSLLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSLAdjust)).BeginInit();
            this.groupBox15Q.SuspendLayout();
            this.groupBox24Q.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnLoadCsv
            // 
            this.BtnLoadCsv.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.BtnLoadCsv.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnLoadCsv.Location = new System.Drawing.Point(7, 16);
            this.BtnLoadCsv.Name = "BtnLoadCsv";
            this.BtnLoadCsv.Size = new System.Drawing.Size(80, 21);
            this.BtnLoadCsv.TabIndex = 0;
            this.BtnLoadCsv.Text = "Load Data";
            this.BtnLoadCsv.UseVisualStyleBackColor = true;
            this.BtnLoadCsv.Click += new System.EventHandler(this.BtnLoadCsv_Click);
            // 
            // textBoxIssues1
            // 
            this.textBoxIssues1.Location = new System.Drawing.Point(6, 38);
            this.textBoxIssues1.Name = "textBoxIssues1";
            this.textBoxIssues1.ReadOnly = true;
            this.textBoxIssues1.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues1.TabIndex = 1;
            this.textBoxIssues1.TextChanged += new System.EventHandler(this.textBoxIssues1_TextChanged);
            // 
            // textBoxQuestion1
            // 
            this.textBoxQuestion1.Location = new System.Drawing.Point(174, 38);
            this.textBoxQuestion1.Name = "textBoxQuestion1";
            this.textBoxQuestion1.ReadOnly = true;
            this.textBoxQuestion1.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion1.TabIndex = 2;
            this.textBoxQuestion1.TextChanged += new System.EventHandler(this.textBoxQuestion1_TextChanged);
            // 
            // textBoxAnswers1
            // 
            this.textBoxAnswers1.Location = new System.Drawing.Point(206, 38);
            this.textBoxAnswers1.Name = "textBoxAnswers1";
            this.textBoxAnswers1.ReadOnly = true;
            this.textBoxAnswers1.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers1.TabIndex = 3;
            this.textBoxAnswers1.TextChanged += new System.EventHandler(this.textBoxAnswers1_TextChanged);
            // 
            // textBoxReactionData1
            // 
            this.textBoxReactionData1.Location = new System.Drawing.Point(306, 38);
            this.textBoxReactionData1.Name = "textBoxReactionData1";
            this.textBoxReactionData1.ReadOnly = true;
            this.textBoxReactionData1.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData1.TabIndex = 4;
            this.textBoxReactionData1.TextChanged += new System.EventHandler(this.textBoxReactionData1_TextChanged);
            // 
            // textBoxReadTime1
            // 
            this.textBoxReadTime1.Location = new System.Drawing.Point(406, 38);
            this.textBoxReadTime1.Name = "textBoxReadTime1";
            this.textBoxReadTime1.ReadOnly = true;
            this.textBoxReadTime1.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime1.TabIndex = 5;
            this.textBoxReadTime1.TextChanged += new System.EventHandler(this.textBoxReadTime1_TextChanged);
            // 
            // textBoxReaction1
            // 
            this.textBoxReaction1.Location = new System.Drawing.Point(468, 38);
            this.textBoxReaction1.Name = "textBoxReaction1";
            this.textBoxReaction1.ReadOnly = true;
            this.textBoxReaction1.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction1.TabIndex = 6;
            this.textBoxReaction1.TextChanged += new System.EventHandler(this.textBoxReaction1_TextChanged);
            // 
            // textBoxReaction
            // 
            this.textBoxReaction.Location = new System.Drawing.Point(468, 18);
            this.textBoxReaction.Name = "textBoxReaction";
            this.textBoxReaction.ReadOnly = true;
            this.textBoxReaction.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction.TabIndex = 12;
            this.textBoxReaction.Text = "Reaction";
            // 
            // textBoxReadTime
            // 
            this.textBoxReadTime.Location = new System.Drawing.Point(406, 18);
            this.textBoxReadTime.Name = "textBoxReadTime";
            this.textBoxReadTime.ReadOnly = true;
            this.textBoxReadTime.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime.TabIndex = 11;
            this.textBoxReadTime.Text = "Read Time";
            // 
            // textBoxReactionData
            // 
            this.textBoxReactionData.Location = new System.Drawing.Point(306, 18);
            this.textBoxReactionData.Name = "textBoxReactionData";
            this.textBoxReactionData.ReadOnly = true;
            this.textBoxReactionData.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData.TabIndex = 10;
            this.textBoxReactionData.Text = "ReactionData/SUM";
            // 
            // textBoxAnswers
            // 
            this.textBoxAnswers.Location = new System.Drawing.Point(206, 18);
            this.textBoxAnswers.Name = "textBoxAnswers";
            this.textBoxAnswers.ReadOnly = true;
            this.textBoxAnswers.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers.TabIndex = 9;
            this.textBoxAnswers.Text = "Answers";
            // 
            // textBoxQuestion
            // 
            this.textBoxQuestion.Location = new System.Drawing.Point(174, 18);
            this.textBoxQuestion.Name = "textBoxQuestion";
            this.textBoxQuestion.ReadOnly = true;
            this.textBoxQuestion.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion.TabIndex = 8;
            this.textBoxQuestion.Text = "Q #";
            // 
            // textBoxIssues
            // 
            this.textBoxIssues.Location = new System.Drawing.Point(6, 18);
            this.textBoxIssues.Name = "textBoxIssues";
            this.textBoxIssues.ReadOnly = true;
            this.textBoxIssues.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues.TabIndex = 7;
            this.textBoxIssues.Text = "Issues";
            // 
            // textBoxReaction2
            // 
            this.textBoxReaction2.Location = new System.Drawing.Point(468, 58);
            this.textBoxReaction2.Name = "textBoxReaction2";
            this.textBoxReaction2.ReadOnly = true;
            this.textBoxReaction2.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction2.TabIndex = 18;
            // 
            // textBoxReadTime2
            // 
            this.textBoxReadTime2.Location = new System.Drawing.Point(406, 58);
            this.textBoxReadTime2.Name = "textBoxReadTime2";
            this.textBoxReadTime2.ReadOnly = true;
            this.textBoxReadTime2.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime2.TabIndex = 17;
            // 
            // textBoxReactionData2
            // 
            this.textBoxReactionData2.Location = new System.Drawing.Point(306, 58);
            this.textBoxReactionData2.Name = "textBoxReactionData2";
            this.textBoxReactionData2.ReadOnly = true;
            this.textBoxReactionData2.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData2.TabIndex = 16;
            // 
            // textBoxAnswers2
            // 
            this.textBoxAnswers2.Location = new System.Drawing.Point(206, 58);
            this.textBoxAnswers2.Name = "textBoxAnswers2";
            this.textBoxAnswers2.ReadOnly = true;
            this.textBoxAnswers2.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers2.TabIndex = 15;
            // 
            // textBoxQuestion2
            // 
            this.textBoxQuestion2.Location = new System.Drawing.Point(174, 58);
            this.textBoxQuestion2.Name = "textBoxQuestion2";
            this.textBoxQuestion2.ReadOnly = true;
            this.textBoxQuestion2.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion2.TabIndex = 14;
            // 
            // textBoxIssues2
            // 
            this.textBoxIssues2.Location = new System.Drawing.Point(6, 58);
            this.textBoxIssues2.Name = "textBoxIssues2";
            this.textBoxIssues2.ReadOnly = true;
            this.textBoxIssues2.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues2.TabIndex = 13;
            // 
            // textBoxReaction3
            // 
            this.textBoxReaction3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReaction3.Location = new System.Drawing.Point(468, 15);
            this.textBoxReaction3.Name = "textBoxReaction3";
            this.textBoxReaction3.ReadOnly = true;
            this.textBoxReaction3.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction3.TabIndex = 24;
            // 
            // textBoxReadTime3
            // 
            this.textBoxReadTime3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReadTime3.Location = new System.Drawing.Point(406, 15);
            this.textBoxReadTime3.Name = "textBoxReadTime3";
            this.textBoxReadTime3.ReadOnly = true;
            this.textBoxReadTime3.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime3.TabIndex = 23;
            // 
            // textBoxReactionData3
            // 
            this.textBoxReactionData3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReactionData3.Location = new System.Drawing.Point(306, 15);
            this.textBoxReactionData3.Name = "textBoxReactionData3";
            this.textBoxReactionData3.ReadOnly = true;
            this.textBoxReactionData3.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData3.TabIndex = 22;
            // 
            // textBoxAnswers3
            // 
            this.textBoxAnswers3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxAnswers3.Location = new System.Drawing.Point(206, 15);
            this.textBoxAnswers3.Name = "textBoxAnswers3";
            this.textBoxAnswers3.ReadOnly = true;
            this.textBoxAnswers3.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers3.TabIndex = 21;
            // 
            // textBoxQuestion3
            // 
            this.textBoxQuestion3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxQuestion3.Location = new System.Drawing.Point(174, 15);
            this.textBoxQuestion3.Name = "textBoxQuestion3";
            this.textBoxQuestion3.ReadOnly = true;
            this.textBoxQuestion3.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion3.TabIndex = 20;
            // 
            // textBoxIssues3
            // 
            this.textBoxIssues3.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxIssues3.Location = new System.Drawing.Point(6, 15);
            this.textBoxIssues3.Name = "textBoxIssues3";
            this.textBoxIssues3.ReadOnly = true;
            this.textBoxIssues3.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues3.TabIndex = 19;
            // 
            // textBoxReaction4
            // 
            this.textBoxReaction4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReaction4.Location = new System.Drawing.Point(468, 35);
            this.textBoxReaction4.Name = "textBoxReaction4";
            this.textBoxReaction4.ReadOnly = true;
            this.textBoxReaction4.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction4.TabIndex = 30;
            // 
            // textBoxReadTime4
            // 
            this.textBoxReadTime4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReadTime4.Location = new System.Drawing.Point(406, 35);
            this.textBoxReadTime4.Name = "textBoxReadTime4";
            this.textBoxReadTime4.ReadOnly = true;
            this.textBoxReadTime4.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime4.TabIndex = 29;
            // 
            // textBoxReactionData4
            // 
            this.textBoxReactionData4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReactionData4.Location = new System.Drawing.Point(306, 35);
            this.textBoxReactionData4.Name = "textBoxReactionData4";
            this.textBoxReactionData4.ReadOnly = true;
            this.textBoxReactionData4.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData4.TabIndex = 28;
            // 
            // textBoxAnswers4
            // 
            this.textBoxAnswers4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxAnswers4.Location = new System.Drawing.Point(206, 35);
            this.textBoxAnswers4.Name = "textBoxAnswers4";
            this.textBoxAnswers4.ReadOnly = true;
            this.textBoxAnswers4.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers4.TabIndex = 27;
            // 
            // textBoxQuestion4
            // 
            this.textBoxQuestion4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxQuestion4.Location = new System.Drawing.Point(174, 35);
            this.textBoxQuestion4.Name = "textBoxQuestion4";
            this.textBoxQuestion4.ReadOnly = true;
            this.textBoxQuestion4.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion4.TabIndex = 26;
            // 
            // textBoxIssues4
            // 
            this.textBoxIssues4.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxIssues4.Location = new System.Drawing.Point(6, 35);
            this.textBoxIssues4.Name = "textBoxIssues4";
            this.textBoxIssues4.ReadOnly = true;
            this.textBoxIssues4.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues4.TabIndex = 25;
            // 
            // textBoxReaction5
            // 
            this.textBoxReaction5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReaction5.Location = new System.Drawing.Point(468, 55);
            this.textBoxReaction5.Name = "textBoxReaction5";
            this.textBoxReaction5.ReadOnly = true;
            this.textBoxReaction5.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction5.TabIndex = 36;
            // 
            // textBoxReadTime5
            // 
            this.textBoxReadTime5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReadTime5.Location = new System.Drawing.Point(406, 55);
            this.textBoxReadTime5.Name = "textBoxReadTime5";
            this.textBoxReadTime5.ReadOnly = true;
            this.textBoxReadTime5.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime5.TabIndex = 35;
            // 
            // textBoxReactionData5
            // 
            this.textBoxReactionData5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReactionData5.Location = new System.Drawing.Point(306, 55);
            this.textBoxReactionData5.Name = "textBoxReactionData5";
            this.textBoxReactionData5.ReadOnly = true;
            this.textBoxReactionData5.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData5.TabIndex = 34;
            // 
            // textBoxAnswers5
            // 
            this.textBoxAnswers5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxAnswers5.Location = new System.Drawing.Point(206, 55);
            this.textBoxAnswers5.Name = "textBoxAnswers5";
            this.textBoxAnswers5.ReadOnly = true;
            this.textBoxAnswers5.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers5.TabIndex = 33;
            // 
            // textBoxQuestion5
            // 
            this.textBoxQuestion5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxQuestion5.Location = new System.Drawing.Point(174, 55);
            this.textBoxQuestion5.Name = "textBoxQuestion5";
            this.textBoxQuestion5.ReadOnly = true;
            this.textBoxQuestion5.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion5.TabIndex = 32;
            // 
            // textBoxIssues5
            // 
            this.textBoxIssues5.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxIssues5.Location = new System.Drawing.Point(6, 55);
            this.textBoxIssues5.Name = "textBoxIssues5";
            this.textBoxIssues5.ReadOnly = true;
            this.textBoxIssues5.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues5.TabIndex = 31;
            // 
            // textBoxReaction6
            // 
            this.textBoxReaction6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReaction6.Location = new System.Drawing.Point(469, 15);
            this.textBoxReaction6.Name = "textBoxReaction6";
            this.textBoxReaction6.ReadOnly = true;
            this.textBoxReaction6.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction6.TabIndex = 42;
            // 
            // textBoxReadTime6
            // 
            this.textBoxReadTime6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReadTime6.Location = new System.Drawing.Point(406, 15);
            this.textBoxReadTime6.Name = "textBoxReadTime6";
            this.textBoxReadTime6.ReadOnly = true;
            this.textBoxReadTime6.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime6.TabIndex = 41;
            // 
            // textBoxReactionData6
            // 
            this.textBoxReactionData6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReactionData6.Location = new System.Drawing.Point(306, 15);
            this.textBoxReactionData6.Name = "textBoxReactionData6";
            this.textBoxReactionData6.ReadOnly = true;
            this.textBoxReactionData6.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData6.TabIndex = 40;
            // 
            // textBoxAnswers6
            // 
            this.textBoxAnswers6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxAnswers6.Location = new System.Drawing.Point(206, 15);
            this.textBoxAnswers6.Name = "textBoxAnswers6";
            this.textBoxAnswers6.ReadOnly = true;
            this.textBoxAnswers6.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers6.TabIndex = 39;
            // 
            // textBoxQuestion6
            // 
            this.textBoxQuestion6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxQuestion6.Location = new System.Drawing.Point(174, 15);
            this.textBoxQuestion6.Name = "textBoxQuestion6";
            this.textBoxQuestion6.ReadOnly = true;
            this.textBoxQuestion6.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion6.TabIndex = 38;
            // 
            // textBoxIssues6
            // 
            this.textBoxIssues6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxIssues6.Location = new System.Drawing.Point(6, 15);
            this.textBoxIssues6.Name = "textBoxIssues6";
            this.textBoxIssues6.ReadOnly = true;
            this.textBoxIssues6.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues6.TabIndex = 37;
            // 
            // textBoxReaction7
            // 
            this.textBoxReaction7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReaction7.Location = new System.Drawing.Point(469, 35);
            this.textBoxReaction7.Name = "textBoxReaction7";
            this.textBoxReaction7.ReadOnly = true;
            this.textBoxReaction7.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction7.TabIndex = 48;
            // 
            // textBoxReadTime7
            // 
            this.textBoxReadTime7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReadTime7.Location = new System.Drawing.Point(406, 35);
            this.textBoxReadTime7.Name = "textBoxReadTime7";
            this.textBoxReadTime7.ReadOnly = true;
            this.textBoxReadTime7.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime7.TabIndex = 47;
            // 
            // textBoxReactionData7
            // 
            this.textBoxReactionData7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReactionData7.Location = new System.Drawing.Point(306, 35);
            this.textBoxReactionData7.Name = "textBoxReactionData7";
            this.textBoxReactionData7.ReadOnly = true;
            this.textBoxReactionData7.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData7.TabIndex = 46;
            // 
            // textBoxAnswers7
            // 
            this.textBoxAnswers7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxAnswers7.Location = new System.Drawing.Point(206, 35);
            this.textBoxAnswers7.Name = "textBoxAnswers7";
            this.textBoxAnswers7.ReadOnly = true;
            this.textBoxAnswers7.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers7.TabIndex = 45;
            // 
            // textBoxQuestion7
            // 
            this.textBoxQuestion7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxQuestion7.Location = new System.Drawing.Point(174, 35);
            this.textBoxQuestion7.Name = "textBoxQuestion7";
            this.textBoxQuestion7.ReadOnly = true;
            this.textBoxQuestion7.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion7.TabIndex = 44;
            // 
            // textBoxIssues7
            // 
            this.textBoxIssues7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxIssues7.Location = new System.Drawing.Point(6, 35);
            this.textBoxIssues7.Name = "textBoxIssues7";
            this.textBoxIssues7.ReadOnly = true;
            this.textBoxIssues7.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues7.TabIndex = 43;
            // 
            // textBoxReaction8
            // 
            this.textBoxReaction8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReaction8.Location = new System.Drawing.Point(469, 55);
            this.textBoxReaction8.Name = "textBoxReaction8";
            this.textBoxReaction8.ReadOnly = true;
            this.textBoxReaction8.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction8.TabIndex = 54;
            // 
            // textBoxReadTime8
            // 
            this.textBoxReadTime8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReadTime8.Location = new System.Drawing.Point(406, 55);
            this.textBoxReadTime8.Name = "textBoxReadTime8";
            this.textBoxReadTime8.ReadOnly = true;
            this.textBoxReadTime8.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime8.TabIndex = 53;
            // 
            // textBoxReactionData8
            // 
            this.textBoxReactionData8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReactionData8.Location = new System.Drawing.Point(306, 55);
            this.textBoxReactionData8.Name = "textBoxReactionData8";
            this.textBoxReactionData8.ReadOnly = true;
            this.textBoxReactionData8.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData8.TabIndex = 52;
            // 
            // textBoxAnswers8
            // 
            this.textBoxAnswers8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxAnswers8.Location = new System.Drawing.Point(206, 55);
            this.textBoxAnswers8.Name = "textBoxAnswers8";
            this.textBoxAnswers8.ReadOnly = true;
            this.textBoxAnswers8.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers8.TabIndex = 51;
            // 
            // textBoxQuestion8
            // 
            this.textBoxQuestion8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxQuestion8.Location = new System.Drawing.Point(174, 55);
            this.textBoxQuestion8.Name = "textBoxQuestion8";
            this.textBoxQuestion8.ReadOnly = true;
            this.textBoxQuestion8.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion8.TabIndex = 50;
            // 
            // textBoxIssues8
            // 
            this.textBoxIssues8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxIssues8.Location = new System.Drawing.Point(6, 55);
            this.textBoxIssues8.Name = "textBoxIssues8";
            this.textBoxIssues8.ReadOnly = true;
            this.textBoxIssues8.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues8.TabIndex = 49;
            // 
            // textBoxReaction9
            // 
            this.textBoxReaction9.BackColor = System.Drawing.Color.Pink;
            this.textBoxReaction9.Location = new System.Drawing.Point(469, 14);
            this.textBoxReaction9.Name = "textBoxReaction9";
            this.textBoxReaction9.ReadOnly = true;
            this.textBoxReaction9.Size = new System.Drawing.Size(139, 20);
            this.textBoxReaction9.TabIndex = 60;
            // 
            // textBoxReadTime9
            // 
            this.textBoxReadTime9.BackColor = System.Drawing.Color.Pink;
            this.textBoxReadTime9.Location = new System.Drawing.Point(408, 14);
            this.textBoxReadTime9.Name = "textBoxReadTime9";
            this.textBoxReadTime9.ReadOnly = true;
            this.textBoxReadTime9.Size = new System.Drawing.Size(61, 20);
            this.textBoxReadTime9.TabIndex = 59;
            // 
            // textBoxReactionData9
            // 
            this.textBoxReactionData9.BackColor = System.Drawing.Color.Pink;
            this.textBoxReactionData9.Location = new System.Drawing.Point(308, 14);
            this.textBoxReactionData9.Name = "textBoxReactionData9";
            this.textBoxReactionData9.ReadOnly = true;
            this.textBoxReactionData9.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData9.TabIndex = 58;
            // 
            // textBoxAnswers9
            // 
            this.textBoxAnswers9.BackColor = System.Drawing.Color.Pink;
            this.textBoxAnswers9.Location = new System.Drawing.Point(208, 14);
            this.textBoxAnswers9.Name = "textBoxAnswers9";
            this.textBoxAnswers9.ReadOnly = true;
            this.textBoxAnswers9.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers9.TabIndex = 57;
            // 
            // textBoxQuestion9
            // 
            this.textBoxQuestion9.BackColor = System.Drawing.Color.Pink;
            this.textBoxQuestion9.Location = new System.Drawing.Point(176, 14);
            this.textBoxQuestion9.Name = "textBoxQuestion9";
            this.textBoxQuestion9.ReadOnly = true;
            this.textBoxQuestion9.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion9.TabIndex = 56;
            // 
            // textBoxIssues9
            // 
            this.textBoxIssues9.BackColor = System.Drawing.Color.Pink;
            this.textBoxIssues9.Location = new System.Drawing.Point(8, 14);
            this.textBoxIssues9.Name = "textBoxIssues9";
            this.textBoxIssues9.ReadOnly = true;
            this.textBoxIssues9.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues9.TabIndex = 55;
            // 
            // textBoxReaction10
            // 
            this.textBoxReaction10.BackColor = System.Drawing.Color.Pink;
            this.textBoxReaction10.Location = new System.Drawing.Point(469, 34);
            this.textBoxReaction10.Name = "textBoxReaction10";
            this.textBoxReaction10.ReadOnly = true;
            this.textBoxReaction10.Size = new System.Drawing.Size(139, 20);
            this.textBoxReaction10.TabIndex = 66;
            // 
            // textBoxReadTime10
            // 
            this.textBoxReadTime10.BackColor = System.Drawing.Color.Pink;
            this.textBoxReadTime10.Location = new System.Drawing.Point(408, 34);
            this.textBoxReadTime10.Name = "textBoxReadTime10";
            this.textBoxReadTime10.ReadOnly = true;
            this.textBoxReadTime10.Size = new System.Drawing.Size(61, 20);
            this.textBoxReadTime10.TabIndex = 65;
            // 
            // textBoxReactionData10
            // 
            this.textBoxReactionData10.BackColor = System.Drawing.Color.Pink;
            this.textBoxReactionData10.Location = new System.Drawing.Point(308, 34);
            this.textBoxReactionData10.Name = "textBoxReactionData10";
            this.textBoxReactionData10.ReadOnly = true;
            this.textBoxReactionData10.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData10.TabIndex = 64;
            // 
            // textBoxAnswers10
            // 
            this.textBoxAnswers10.BackColor = System.Drawing.Color.Pink;
            this.textBoxAnswers10.Location = new System.Drawing.Point(208, 34);
            this.textBoxAnswers10.Name = "textBoxAnswers10";
            this.textBoxAnswers10.ReadOnly = true;
            this.textBoxAnswers10.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers10.TabIndex = 63;
            // 
            // textBoxQuestion10
            // 
            this.textBoxQuestion10.BackColor = System.Drawing.Color.Pink;
            this.textBoxQuestion10.Location = new System.Drawing.Point(176, 34);
            this.textBoxQuestion10.Name = "textBoxQuestion10";
            this.textBoxQuestion10.ReadOnly = true;
            this.textBoxQuestion10.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion10.TabIndex = 62;
            // 
            // textBoxIssues10
            // 
            this.textBoxIssues10.BackColor = System.Drawing.Color.Pink;
            this.textBoxIssues10.Location = new System.Drawing.Point(8, 34);
            this.textBoxIssues10.Name = "textBoxIssues10";
            this.textBoxIssues10.ReadOnly = true;
            this.textBoxIssues10.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues10.TabIndex = 61;
            // 
            // textBoxReaction11
            // 
            this.textBoxReaction11.BackColor = System.Drawing.Color.Pink;
            this.textBoxReaction11.Location = new System.Drawing.Point(469, 54);
            this.textBoxReaction11.Name = "textBoxReaction11";
            this.textBoxReaction11.ReadOnly = true;
            this.textBoxReaction11.Size = new System.Drawing.Size(139, 20);
            this.textBoxReaction11.TabIndex = 72;
            // 
            // textBoxReadTime11
            // 
            this.textBoxReadTime11.BackColor = System.Drawing.Color.Pink;
            this.textBoxReadTime11.Location = new System.Drawing.Point(408, 54);
            this.textBoxReadTime11.Name = "textBoxReadTime11";
            this.textBoxReadTime11.ReadOnly = true;
            this.textBoxReadTime11.Size = new System.Drawing.Size(61, 20);
            this.textBoxReadTime11.TabIndex = 71;
            // 
            // textBoxReactionData11
            // 
            this.textBoxReactionData11.BackColor = System.Drawing.Color.Pink;
            this.textBoxReactionData11.Location = new System.Drawing.Point(308, 54);
            this.textBoxReactionData11.Name = "textBoxReactionData11";
            this.textBoxReactionData11.ReadOnly = true;
            this.textBoxReactionData11.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData11.TabIndex = 70;
            // 
            // textBoxAnswers11
            // 
            this.textBoxAnswers11.BackColor = System.Drawing.Color.Pink;
            this.textBoxAnswers11.Location = new System.Drawing.Point(208, 54);
            this.textBoxAnswers11.Name = "textBoxAnswers11";
            this.textBoxAnswers11.ReadOnly = true;
            this.textBoxAnswers11.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers11.TabIndex = 69;
            // 
            // textBoxQuestion11
            // 
            this.textBoxQuestion11.BackColor = System.Drawing.Color.Pink;
            this.textBoxQuestion11.Location = new System.Drawing.Point(176, 54);
            this.textBoxQuestion11.Name = "textBoxQuestion11";
            this.textBoxQuestion11.ReadOnly = true;
            this.textBoxQuestion11.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion11.TabIndex = 68;
            // 
            // textBoxIssues11
            // 
            this.textBoxIssues11.BackColor = System.Drawing.Color.Pink;
            this.textBoxIssues11.Location = new System.Drawing.Point(8, 54);
            this.textBoxIssues11.Name = "textBoxIssues11";
            this.textBoxIssues11.ReadOnly = true;
            this.textBoxIssues11.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues11.TabIndex = 67;
            // 
            // textBoxReaction12
            // 
            this.textBoxReaction12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReaction12.Location = new System.Drawing.Point(469, 15);
            this.textBoxReaction12.Name = "textBoxReaction12";
            this.textBoxReaction12.ReadOnly = true;
            this.textBoxReaction12.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction12.TabIndex = 78;
            // 
            // textBoxReadTime12
            // 
            this.textBoxReadTime12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReadTime12.Location = new System.Drawing.Point(406, 15);
            this.textBoxReadTime12.Name = "textBoxReadTime12";
            this.textBoxReadTime12.ReadOnly = true;
            this.textBoxReadTime12.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime12.TabIndex = 77;
            // 
            // textBoxReactionData12
            // 
            this.textBoxReactionData12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReactionData12.Location = new System.Drawing.Point(306, 15);
            this.textBoxReactionData12.Name = "textBoxReactionData12";
            this.textBoxReactionData12.ReadOnly = true;
            this.textBoxReactionData12.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData12.TabIndex = 76;
            // 
            // textBoxAnswers12
            // 
            this.textBoxAnswers12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxAnswers12.Location = new System.Drawing.Point(206, 15);
            this.textBoxAnswers12.Name = "textBoxAnswers12";
            this.textBoxAnswers12.ReadOnly = true;
            this.textBoxAnswers12.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers12.TabIndex = 75;
            // 
            // textBoxQuestion12
            // 
            this.textBoxQuestion12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxQuestion12.Location = new System.Drawing.Point(174, 15);
            this.textBoxQuestion12.Name = "textBoxQuestion12";
            this.textBoxQuestion12.ReadOnly = true;
            this.textBoxQuestion12.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion12.TabIndex = 74;
            // 
            // textBoxIssues12
            // 
            this.textBoxIssues12.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxIssues12.Location = new System.Drawing.Point(6, 15);
            this.textBoxIssues12.Name = "textBoxIssues12";
            this.textBoxIssues12.ReadOnly = true;
            this.textBoxIssues12.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues12.TabIndex = 73;
            // 
            // textBoxReaction13
            // 
            this.textBoxReaction13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReaction13.Location = new System.Drawing.Point(469, 35);
            this.textBoxReaction13.Name = "textBoxReaction13";
            this.textBoxReaction13.ReadOnly = true;
            this.textBoxReaction13.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction13.TabIndex = 84;
            // 
            // textBoxReadTime13
            // 
            this.textBoxReadTime13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReadTime13.Location = new System.Drawing.Point(406, 35);
            this.textBoxReadTime13.Name = "textBoxReadTime13";
            this.textBoxReadTime13.ReadOnly = true;
            this.textBoxReadTime13.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime13.TabIndex = 83;
            // 
            // textBoxReactionData13
            // 
            this.textBoxReactionData13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReactionData13.Location = new System.Drawing.Point(306, 35);
            this.textBoxReactionData13.Name = "textBoxReactionData13";
            this.textBoxReactionData13.ReadOnly = true;
            this.textBoxReactionData13.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData13.TabIndex = 82;
            // 
            // textBoxAnswers13
            // 
            this.textBoxAnswers13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxAnswers13.Location = new System.Drawing.Point(206, 35);
            this.textBoxAnswers13.Name = "textBoxAnswers13";
            this.textBoxAnswers13.ReadOnly = true;
            this.textBoxAnswers13.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers13.TabIndex = 81;
            // 
            // textBoxQuestion13
            // 
            this.textBoxQuestion13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxQuestion13.Location = new System.Drawing.Point(174, 35);
            this.textBoxQuestion13.Name = "textBoxQuestion13";
            this.textBoxQuestion13.ReadOnly = true;
            this.textBoxQuestion13.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion13.TabIndex = 80;
            // 
            // textBoxIssues13
            // 
            this.textBoxIssues13.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxIssues13.Location = new System.Drawing.Point(6, 35);
            this.textBoxIssues13.Name = "textBoxIssues13";
            this.textBoxIssues13.ReadOnly = true;
            this.textBoxIssues13.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues13.TabIndex = 79;
            // 
            // textBoxReaction14
            // 
            this.textBoxReaction14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReaction14.Location = new System.Drawing.Point(469, 55);
            this.textBoxReaction14.Name = "textBoxReaction14";
            this.textBoxReaction14.ReadOnly = true;
            this.textBoxReaction14.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction14.TabIndex = 90;
            // 
            // textBoxReadTime14
            // 
            this.textBoxReadTime14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReadTime14.Location = new System.Drawing.Point(406, 55);
            this.textBoxReadTime14.Name = "textBoxReadTime14";
            this.textBoxReadTime14.ReadOnly = true;
            this.textBoxReadTime14.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime14.TabIndex = 89;
            // 
            // textBoxReactionData14
            // 
            this.textBoxReactionData14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReactionData14.Location = new System.Drawing.Point(306, 55);
            this.textBoxReactionData14.Name = "textBoxReactionData14";
            this.textBoxReactionData14.ReadOnly = true;
            this.textBoxReactionData14.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData14.TabIndex = 88;
            // 
            // textBoxAnswers14
            // 
            this.textBoxAnswers14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxAnswers14.Location = new System.Drawing.Point(206, 55);
            this.textBoxAnswers14.Name = "textBoxAnswers14";
            this.textBoxAnswers14.ReadOnly = true;
            this.textBoxAnswers14.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers14.TabIndex = 87;
            // 
            // textBoxQuestion14
            // 
            this.textBoxQuestion14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxQuestion14.Location = new System.Drawing.Point(174, 55);
            this.textBoxQuestion14.Name = "textBoxQuestion14";
            this.textBoxQuestion14.ReadOnly = true;
            this.textBoxQuestion14.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion14.TabIndex = 86;
            // 
            // textBoxIssues14
            // 
            this.textBoxIssues14.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxIssues14.Location = new System.Drawing.Point(6, 55);
            this.textBoxIssues14.Name = "textBoxIssues14";
            this.textBoxIssues14.ReadOnly = true;
            this.textBoxIssues14.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues14.TabIndex = 85;
            // 
            // textBoxReaction15
            // 
            this.textBoxReaction15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReaction15.Location = new System.Drawing.Point(469, 14);
            this.textBoxReaction15.Name = "textBoxReaction15";
            this.textBoxReaction15.ReadOnly = true;
            this.textBoxReaction15.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction15.TabIndex = 96;
            // 
            // textBoxReadTime15
            // 
            this.textBoxReadTime15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReadTime15.Location = new System.Drawing.Point(406, 14);
            this.textBoxReadTime15.Name = "textBoxReadTime15";
            this.textBoxReadTime15.ReadOnly = true;
            this.textBoxReadTime15.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime15.TabIndex = 95;
            // 
            // textBoxReactionData15
            // 
            this.textBoxReactionData15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReactionData15.Location = new System.Drawing.Point(306, 14);
            this.textBoxReactionData15.Name = "textBoxReactionData15";
            this.textBoxReactionData15.ReadOnly = true;
            this.textBoxReactionData15.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData15.TabIndex = 94;
            // 
            // textBoxAnswers15
            // 
            this.textBoxAnswers15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxAnswers15.Location = new System.Drawing.Point(206, 14);
            this.textBoxAnswers15.Name = "textBoxAnswers15";
            this.textBoxAnswers15.ReadOnly = true;
            this.textBoxAnswers15.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers15.TabIndex = 93;
            // 
            // textBoxQuestion15
            // 
            this.textBoxQuestion15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxQuestion15.Location = new System.Drawing.Point(174, 14);
            this.textBoxQuestion15.Name = "textBoxQuestion15";
            this.textBoxQuestion15.ReadOnly = true;
            this.textBoxQuestion15.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion15.TabIndex = 92;
            // 
            // textBoxIssues15
            // 
            this.textBoxIssues15.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxIssues15.Location = new System.Drawing.Point(6, 14);
            this.textBoxIssues15.Name = "textBoxIssues15";
            this.textBoxIssues15.ReadOnly = true;
            this.textBoxIssues15.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues15.TabIndex = 91;
            // 
            // txtTSLHi
            // 
            this.txtTSLHi.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtTSLHi.Location = new System.Drawing.Point(40, 41);
            this.txtTSLHi.Name = "txtTSLHi";
            this.txtTSLHi.ReadOnly = true;
            this.txtTSLHi.Size = new System.Drawing.Size(46, 20);
            this.txtTSLHi.TabIndex = 97;
            this.txtTSLHi.Text = "1.2";
            // 
            // txtTSLLo
            // 
            this.txtTSLLo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtTSLLo.Location = new System.Drawing.Point(40, 63);
            this.txtTSLLo.Name = "txtTSLLo";
            this.txtTSLLo.ReadOnly = true;
            this.txtTSLLo.Size = new System.Drawing.Size(46, 20);
            this.txtTSLLo.TabIndex = 98;
            this.txtTSLLo.Text = "0.6";
            // 
            // textBoxReactionDataAvg1
            // 
            this.textBoxReactionDataAvg1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBoxReactionDataAvg1.Location = new System.Drawing.Point(112, 21);
            this.textBoxReactionDataAvg1.Name = "textBoxReactionDataAvg1";
            this.textBoxReactionDataAvg1.ReadOnly = true;
            this.textBoxReactionDataAvg1.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg1.TabIndex = 99;
            this.toolTip1.SetToolTip(this.textBoxReactionDataAvg1, "Average from question 5 to 25");
            // 
            // textBoxReactionDataAvg2
            // 
            this.textBoxReactionDataAvg2.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxReactionDataAvg2.Location = new System.Drawing.Point(112, 41);
            this.textBoxReactionDataAvg2.Name = "textBoxReactionDataAvg2";
            this.textBoxReactionDataAvg2.ReadOnly = true;
            this.textBoxReactionDataAvg2.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg2.TabIndex = 100;
            this.toolTip1.SetToolTip(this.textBoxReactionDataAvg2, "Baseline Avg Q5/Q9/Q13/Q17/Q21/Q25");
            // 
            // textBoxReactionDataAvg3
            // 
            this.textBoxReactionDataAvg3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxReactionDataAvg3.Location = new System.Drawing.Point(112, 61);
            this.textBoxReactionDataAvg3.Name = "textBoxReactionDataAvg3";
            this.textBoxReactionDataAvg3.ReadOnly = true;
            this.textBoxReactionDataAvg3.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg3.TabIndex = 101;
            // 
            // textBoxReactionDataAvg4
            // 
            this.textBoxReactionDataAvg4.BackColor = System.Drawing.Color.Pink;
            this.textBoxReactionDataAvg4.Location = new System.Drawing.Point(112, 81);
            this.textBoxReactionDataAvg4.Name = "textBoxReactionDataAvg4";
            this.textBoxReactionDataAvg4.ReadOnly = true;
            this.textBoxReactionDataAvg4.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg4.TabIndex = 102;
            // 
            // textBoxReactionDataAvg5
            // 
            this.textBoxReactionDataAvg5.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxReactionDataAvg5.Location = new System.Drawing.Point(112, 101);
            this.textBoxReactionDataAvg5.Name = "textBoxReactionDataAvg5";
            this.textBoxReactionDataAvg5.ReadOnly = true;
            this.textBoxReactionDataAvg5.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg5.TabIndex = 103;
            // 
            // textBoxReactionDataAvg13
            // 
            this.textBoxReactionDataAvg13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReactionDataAvg13.Location = new System.Drawing.Point(112, 264);
            this.textBoxReactionDataAvg13.Name = "textBoxReactionDataAvg13";
            this.textBoxReactionDataAvg13.ReadOnly = true;
            this.textBoxReactionDataAvg13.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg13.TabIndex = 104;
            // 
            // textBoxReactionDataAvg14
            // 
            this.textBoxReactionDataAvg14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReactionDataAvg14.Location = new System.Drawing.Point(112, 284);
            this.textBoxReactionDataAvg14.Name = "textBoxReactionDataAvg14";
            this.textBoxReactionDataAvg14.ReadOnly = true;
            this.textBoxReactionDataAvg14.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg14.TabIndex = 105;
            // 
            // textBoxStandardDeviation
            // 
            this.textBoxStandardDeviation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxStandardDeviation.Location = new System.Drawing.Point(112, 304);
            this.textBoxStandardDeviation.Name = "textBoxStandardDeviation";
            this.textBoxStandardDeviation.ReadOnly = true;
            this.textBoxStandardDeviation.Size = new System.Drawing.Size(100, 20);
            this.textBoxStandardDeviation.TabIndex = 106;
            // 
            // textBoxAvgTest
            // 
            this.textBoxAvgTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.textBoxAvgTest.Location = new System.Drawing.Point(12, 21);
            this.textBoxAvgTest.Name = "textBoxAvgTest";
            this.textBoxAvgTest.ReadOnly = true;
            this.textBoxAvgTest.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvgTest.TabIndex = 107;
            this.textBoxAvgTest.Text = "Avg Test 5-25";
            this.textBoxAvgTest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxAvg1
            // 
            this.textBoxAvg1.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxAvg1.Location = new System.Drawing.Point(12, 41);
            this.textBoxAvg1.Name = "textBoxAvg1";
            this.textBoxAvg1.ReadOnly = true;
            this.textBoxAvg1.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg1.TabIndex = 108;
            this.textBoxAvg1.Tag = " ";
            this.textBoxAvg1.Text = "Avg 5,9,13,17,21,25";
            this.textBoxAvg1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxAvg2
            // 
            this.textBoxAvg2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxAvg2.Location = new System.Drawing.Point(12, 61);
            this.textBoxAvg2.Name = "textBoxAvg2";
            this.textBoxAvg2.ReadOnly = true;
            this.textBoxAvg2.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg2.TabIndex = 109;
            this.textBoxAvg2.Text = "Base Avg Q 5/8";
            this.textBoxAvg2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxAvg3
            // 
            this.textBoxAvg3.BackColor = System.Drawing.Color.Pink;
            this.textBoxAvg3.Location = new System.Drawing.Point(12, 81);
            this.textBoxAvg3.Name = "textBoxAvg3";
            this.textBoxAvg3.ReadOnly = true;
            this.textBoxAvg3.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg3.TabIndex = 110;
            this.textBoxAvg3.Text = "Base Avg Q 9/12";
            this.textBoxAvg3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxAvg4
            // 
            this.textBoxAvg4.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxAvg4.Location = new System.Drawing.Point(12, 101);
            this.textBoxAvg4.Name = "textBoxAvg4";
            this.textBoxAvg4.ReadOnly = true;
            this.textBoxAvg4.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg4.TabIndex = 111;
            this.textBoxAvg4.Text = "Base Avg Q13/16";
            this.textBoxAvg4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox6.Location = new System.Drawing.Point(12, 264);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 112;
            this.textBox6.Text = "TSL High AVG";
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox7.Location = new System.Drawing.Point(12, 284);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 113;
            this.textBox7.Tag = "";
            this.textBox7.Text = "TSL Low AVG";
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox8.Location = new System.Drawing.Point(12, 304);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 114;
            this.textBox8.Text = "STD AVG";
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnLoad24Data
            // 
            this.btnLoad24Data.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLoad24Data.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoad24Data.Location = new System.Drawing.Point(6, 15);
            this.btnLoad24Data.Name = "btnLoad24Data";
            this.btnLoad24Data.Size = new System.Drawing.Size(79, 23);
            this.btnLoad24Data.TabIndex = 116;
            this.btnLoad24Data.Text = "Load Data";
            this.btnLoad24Data.UseVisualStyleBackColor = true;
            this.btnLoad24Data.Click += new System.EventHandler(this.btnLoad24Data_Click);
            // 
            // textBoxReaction16
            // 
            this.textBoxReaction16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReaction16.Location = new System.Drawing.Point(469, 35);
            this.textBoxReaction16.Name = "textBoxReaction16";
            this.textBoxReaction16.ReadOnly = true;
            this.textBoxReaction16.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction16.TabIndex = 122;
            // 
            // textBoxReadTime16
            // 
            this.textBoxReadTime16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReadTime16.Location = new System.Drawing.Point(406, 35);
            this.textBoxReadTime16.Name = "textBoxReadTime16";
            this.textBoxReadTime16.ReadOnly = true;
            this.textBoxReadTime16.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime16.TabIndex = 121;
            // 
            // textBoxReactionData16
            // 
            this.textBoxReactionData16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReactionData16.Location = new System.Drawing.Point(306, 35);
            this.textBoxReactionData16.Name = "textBoxReactionData16";
            this.textBoxReactionData16.ReadOnly = true;
            this.textBoxReactionData16.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData16.TabIndex = 120;
            // 
            // textBoxAnswers16
            // 
            this.textBoxAnswers16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxAnswers16.Location = new System.Drawing.Point(206, 35);
            this.textBoxAnswers16.Name = "textBoxAnswers16";
            this.textBoxAnswers16.ReadOnly = true;
            this.textBoxAnswers16.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers16.TabIndex = 119;
            // 
            // textBoxQuestion16
            // 
            this.textBoxQuestion16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxQuestion16.Location = new System.Drawing.Point(174, 35);
            this.textBoxQuestion16.Name = "textBoxQuestion16";
            this.textBoxQuestion16.ReadOnly = true;
            this.textBoxQuestion16.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion16.TabIndex = 118;
            // 
            // textBoxIssues16
            // 
            this.textBoxIssues16.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxIssues16.Location = new System.Drawing.Point(6, 35);
            this.textBoxIssues16.Name = "textBoxIssues16";
            this.textBoxIssues16.ReadOnly = true;
            this.textBoxIssues16.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues16.TabIndex = 117;
            // 
            // textBoxReaction17
            // 
            this.textBoxReaction17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReaction17.Location = new System.Drawing.Point(469, 56);
            this.textBoxReaction17.Name = "textBoxReaction17";
            this.textBoxReaction17.ReadOnly = true;
            this.textBoxReaction17.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction17.TabIndex = 128;
            // 
            // textBoxReadTime17
            // 
            this.textBoxReadTime17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReadTime17.Location = new System.Drawing.Point(406, 56);
            this.textBoxReadTime17.Name = "textBoxReadTime17";
            this.textBoxReadTime17.ReadOnly = true;
            this.textBoxReadTime17.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime17.TabIndex = 127;
            // 
            // textBoxReactionData17
            // 
            this.textBoxReactionData17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReactionData17.Location = new System.Drawing.Point(306, 56);
            this.textBoxReactionData17.Name = "textBoxReactionData17";
            this.textBoxReactionData17.ReadOnly = true;
            this.textBoxReactionData17.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData17.TabIndex = 126;
            // 
            // textBoxAnswers17
            // 
            this.textBoxAnswers17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxAnswers17.Location = new System.Drawing.Point(206, 56);
            this.textBoxAnswers17.Name = "textBoxAnswers17";
            this.textBoxAnswers17.ReadOnly = true;
            this.textBoxAnswers17.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers17.TabIndex = 125;
            // 
            // textBoxQuestion17
            // 
            this.textBoxQuestion17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxQuestion17.Location = new System.Drawing.Point(174, 56);
            this.textBoxQuestion17.Name = "textBoxQuestion17";
            this.textBoxQuestion17.ReadOnly = true;
            this.textBoxQuestion17.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion17.TabIndex = 124;
            // 
            // textBoxIssues17
            // 
            this.textBoxIssues17.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxIssues17.Location = new System.Drawing.Point(6, 56);
            this.textBoxIssues17.Name = "textBoxIssues17";
            this.textBoxIssues17.ReadOnly = true;
            this.textBoxIssues17.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues17.TabIndex = 123;
            // 
            // textBoxReaction19
            // 
            this.textBoxReaction19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReaction19.Location = new System.Drawing.Point(469, 36);
            this.textBoxReaction19.Name = "textBoxReaction19";
            this.textBoxReaction19.ReadOnly = true;
            this.textBoxReaction19.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction19.TabIndex = 140;
            // 
            // textBoxReadTime19
            // 
            this.textBoxReadTime19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReadTime19.Location = new System.Drawing.Point(406, 36);
            this.textBoxReadTime19.Name = "textBoxReadTime19";
            this.textBoxReadTime19.ReadOnly = true;
            this.textBoxReadTime19.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime19.TabIndex = 139;
            // 
            // textBoxReactionData19
            // 
            this.textBoxReactionData19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReactionData19.Location = new System.Drawing.Point(306, 36);
            this.textBoxReactionData19.Name = "textBoxReactionData19";
            this.textBoxReactionData19.ReadOnly = true;
            this.textBoxReactionData19.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData19.TabIndex = 138;
            // 
            // textBoxAnswers19
            // 
            this.textBoxAnswers19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxAnswers19.Location = new System.Drawing.Point(206, 36);
            this.textBoxAnswers19.Name = "textBoxAnswers19";
            this.textBoxAnswers19.ReadOnly = true;
            this.textBoxAnswers19.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers19.TabIndex = 137;
            // 
            // textBoxQuestion19
            // 
            this.textBoxQuestion19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxQuestion19.Location = new System.Drawing.Point(174, 36);
            this.textBoxQuestion19.Name = "textBoxQuestion19";
            this.textBoxQuestion19.ReadOnly = true;
            this.textBoxQuestion19.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion19.TabIndex = 136;
            // 
            // textBoxIssues19
            // 
            this.textBoxIssues19.BackColor = System.Drawing.Color.Yellow;
            this.textBoxIssues19.Location = new System.Drawing.Point(6, 36);
            this.textBoxIssues19.Name = "textBoxIssues19";
            this.textBoxIssues19.ReadOnly = true;
            this.textBoxIssues19.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues19.TabIndex = 135;
            // 
            // textBoxReaction18
            // 
            this.textBoxReaction18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReaction18.Location = new System.Drawing.Point(469, 15);
            this.textBoxReaction18.Name = "textBoxReaction18";
            this.textBoxReaction18.ReadOnly = true;
            this.textBoxReaction18.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction18.TabIndex = 134;
            // 
            // textBoxReadTime18
            // 
            this.textBoxReadTime18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReadTime18.Location = new System.Drawing.Point(406, 15);
            this.textBoxReadTime18.Name = "textBoxReadTime18";
            this.textBoxReadTime18.ReadOnly = true;
            this.textBoxReadTime18.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime18.TabIndex = 133;
            // 
            // textBoxReactionData18
            // 
            this.textBoxReactionData18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReactionData18.Location = new System.Drawing.Point(306, 15);
            this.textBoxReactionData18.Name = "textBoxReactionData18";
            this.textBoxReactionData18.ReadOnly = true;
            this.textBoxReactionData18.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData18.TabIndex = 132;
            // 
            // textBoxAnswers18
            // 
            this.textBoxAnswers18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxAnswers18.Location = new System.Drawing.Point(206, 15);
            this.textBoxAnswers18.Name = "textBoxAnswers18";
            this.textBoxAnswers18.ReadOnly = true;
            this.textBoxAnswers18.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers18.TabIndex = 131;
            // 
            // textBoxQuestion18
            // 
            this.textBoxQuestion18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxQuestion18.Location = new System.Drawing.Point(174, 15);
            this.textBoxQuestion18.Name = "textBoxQuestion18";
            this.textBoxQuestion18.ReadOnly = true;
            this.textBoxQuestion18.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion18.TabIndex = 130;
            // 
            // textBoxIssues18
            // 
            this.textBoxIssues18.BackColor = System.Drawing.Color.Yellow;
            this.textBoxIssues18.Location = new System.Drawing.Point(6, 15);
            this.textBoxIssues18.Name = "textBoxIssues18";
            this.textBoxIssues18.ReadOnly = true;
            this.textBoxIssues18.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues18.TabIndex = 129;
            // 
            // textBoxReaction23
            // 
            this.textBoxReaction23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReaction23.Location = new System.Drawing.Point(469, 56);
            this.textBoxReaction23.Name = "textBoxReaction23";
            this.textBoxReaction23.ReadOnly = true;
            this.textBoxReaction23.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction23.TabIndex = 164;
            // 
            // textBoxReadTime23
            // 
            this.textBoxReadTime23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReadTime23.Location = new System.Drawing.Point(406, 56);
            this.textBoxReadTime23.Name = "textBoxReadTime23";
            this.textBoxReadTime23.ReadOnly = true;
            this.textBoxReadTime23.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime23.TabIndex = 163;
            // 
            // textBoxReactionData23
            // 
            this.textBoxReactionData23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionData23.Location = new System.Drawing.Point(306, 56);
            this.textBoxReactionData23.Name = "textBoxReactionData23";
            this.textBoxReactionData23.ReadOnly = true;
            this.textBoxReactionData23.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData23.TabIndex = 162;
            // 
            // textBoxAnswers23
            // 
            this.textBoxAnswers23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAnswers23.Location = new System.Drawing.Point(206, 56);
            this.textBoxAnswers23.Name = "textBoxAnswers23";
            this.textBoxAnswers23.ReadOnly = true;
            this.textBoxAnswers23.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers23.TabIndex = 161;
            // 
            // textBoxQuestion23
            // 
            this.textBoxQuestion23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxQuestion23.Location = new System.Drawing.Point(174, 56);
            this.textBoxQuestion23.Name = "textBoxQuestion23";
            this.textBoxQuestion23.ReadOnly = true;
            this.textBoxQuestion23.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion23.TabIndex = 160;
            // 
            // textBoxIssues23
            // 
            this.textBoxIssues23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxIssues23.Location = new System.Drawing.Point(6, 56);
            this.textBoxIssues23.Name = "textBoxIssues23";
            this.textBoxIssues23.ReadOnly = true;
            this.textBoxIssues23.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues23.TabIndex = 159;
            // 
            // textBoxReaction22
            // 
            this.textBoxReaction22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReaction22.Location = new System.Drawing.Point(469, 35);
            this.textBoxReaction22.Name = "textBoxReaction22";
            this.textBoxReaction22.ReadOnly = true;
            this.textBoxReaction22.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction22.TabIndex = 158;
            // 
            // textBoxReadTime22
            // 
            this.textBoxReadTime22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReadTime22.Location = new System.Drawing.Point(406, 35);
            this.textBoxReadTime22.Name = "textBoxReadTime22";
            this.textBoxReadTime22.ReadOnly = true;
            this.textBoxReadTime22.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime22.TabIndex = 157;
            // 
            // textBoxReactionData22
            // 
            this.textBoxReactionData22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionData22.Location = new System.Drawing.Point(306, 35);
            this.textBoxReactionData22.Name = "textBoxReactionData22";
            this.textBoxReactionData22.ReadOnly = true;
            this.textBoxReactionData22.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData22.TabIndex = 156;
            // 
            // textBoxAnswers22
            // 
            this.textBoxAnswers22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAnswers22.Location = new System.Drawing.Point(206, 35);
            this.textBoxAnswers22.Name = "textBoxAnswers22";
            this.textBoxAnswers22.ReadOnly = true;
            this.textBoxAnswers22.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers22.TabIndex = 155;
            // 
            // textBoxQuestion22
            // 
            this.textBoxQuestion22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxQuestion22.Location = new System.Drawing.Point(174, 35);
            this.textBoxQuestion22.Name = "textBoxQuestion22";
            this.textBoxQuestion22.ReadOnly = true;
            this.textBoxQuestion22.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion22.TabIndex = 154;
            // 
            // textBoxIssues22
            // 
            this.textBoxIssues22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxIssues22.Location = new System.Drawing.Point(6, 35);
            this.textBoxIssues22.Name = "textBoxIssues22";
            this.textBoxIssues22.ReadOnly = true;
            this.textBoxIssues22.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues22.TabIndex = 153;
            // 
            // textBoxReaction21
            // 
            this.textBoxReaction21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReaction21.Location = new System.Drawing.Point(469, 14);
            this.textBoxReaction21.Name = "textBoxReaction21";
            this.textBoxReaction21.ReadOnly = true;
            this.textBoxReaction21.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction21.TabIndex = 152;
            // 
            // textBoxReadTime21
            // 
            this.textBoxReadTime21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReadTime21.Location = new System.Drawing.Point(406, 14);
            this.textBoxReadTime21.Name = "textBoxReadTime21";
            this.textBoxReadTime21.ReadOnly = true;
            this.textBoxReadTime21.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime21.TabIndex = 151;
            // 
            // textBoxReactionData21
            // 
            this.textBoxReactionData21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionData21.Location = new System.Drawing.Point(306, 14);
            this.textBoxReactionData21.Name = "textBoxReactionData21";
            this.textBoxReactionData21.ReadOnly = true;
            this.textBoxReactionData21.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData21.TabIndex = 150;
            // 
            // textBoxAnswers21
            // 
            this.textBoxAnswers21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAnswers21.Location = new System.Drawing.Point(206, 14);
            this.textBoxAnswers21.Name = "textBoxAnswers21";
            this.textBoxAnswers21.ReadOnly = true;
            this.textBoxAnswers21.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers21.TabIndex = 149;
            // 
            // textBoxQuestion21
            // 
            this.textBoxQuestion21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxQuestion21.Location = new System.Drawing.Point(174, 14);
            this.textBoxQuestion21.Name = "textBoxQuestion21";
            this.textBoxQuestion21.ReadOnly = true;
            this.textBoxQuestion21.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion21.TabIndex = 148;
            // 
            // textBoxIssues21
            // 
            this.textBoxIssues21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxIssues21.Location = new System.Drawing.Point(6, 14);
            this.textBoxIssues21.Name = "textBoxIssues21";
            this.textBoxIssues21.ReadOnly = true;
            this.textBoxIssues21.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues21.TabIndex = 147;
            // 
            // textBoxReaction20
            // 
            this.textBoxReaction20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReaction20.Location = new System.Drawing.Point(469, 57);
            this.textBoxReaction20.Name = "textBoxReaction20";
            this.textBoxReaction20.ReadOnly = true;
            this.textBoxReaction20.Size = new System.Drawing.Size(137, 20);
            this.textBoxReaction20.TabIndex = 146;
            // 
            // textBoxReadTime20
            // 
            this.textBoxReadTime20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReadTime20.Location = new System.Drawing.Point(406, 57);
            this.textBoxReadTime20.Name = "textBoxReadTime20";
            this.textBoxReadTime20.ReadOnly = true;
            this.textBoxReadTime20.Size = new System.Drawing.Size(63, 20);
            this.textBoxReadTime20.TabIndex = 145;
            // 
            // textBoxReactionData20
            // 
            this.textBoxReactionData20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReactionData20.Location = new System.Drawing.Point(306, 57);
            this.textBoxReactionData20.Name = "textBoxReactionData20";
            this.textBoxReactionData20.ReadOnly = true;
            this.textBoxReactionData20.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData20.TabIndex = 144;
            // 
            // textBoxAnswers20
            // 
            this.textBoxAnswers20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxAnswers20.Location = new System.Drawing.Point(206, 57);
            this.textBoxAnswers20.Name = "textBoxAnswers20";
            this.textBoxAnswers20.ReadOnly = true;
            this.textBoxAnswers20.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers20.TabIndex = 143;
            // 
            // textBoxQuestion20
            // 
            this.textBoxQuestion20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxQuestion20.Location = new System.Drawing.Point(174, 57);
            this.textBoxQuestion20.Name = "textBoxQuestion20";
            this.textBoxQuestion20.ReadOnly = true;
            this.textBoxQuestion20.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion20.TabIndex = 142;
            // 
            // textBoxIssues20
            // 
            this.textBoxIssues20.BackColor = System.Drawing.Color.Yellow;
            this.textBoxIssues20.Location = new System.Drawing.Point(6, 57);
            this.textBoxIssues20.Name = "textBoxIssues20";
            this.textBoxIssues20.ReadOnly = true;
            this.textBoxIssues20.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues20.TabIndex = 141;
            // 
            // textBoxReaction24
            // 
            this.textBoxReaction24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReaction24.Location = new System.Drawing.Point(469, 14);
            this.textBoxReaction24.Name = "textBoxReaction24";
            this.textBoxReaction24.ReadOnly = true;
            this.textBoxReaction24.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction24.TabIndex = 170;
            // 
            // textBoxReadTime24
            // 
            this.textBoxReadTime24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadTime24.Location = new System.Drawing.Point(407, 14);
            this.textBoxReadTime24.Name = "textBoxReadTime24";
            this.textBoxReadTime24.ReadOnly = true;
            this.textBoxReadTime24.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime24.TabIndex = 169;
            // 
            // textBoxReactionData24
            // 
            this.textBoxReactionData24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReactionData24.Location = new System.Drawing.Point(307, 14);
            this.textBoxReactionData24.Name = "textBoxReactionData24";
            this.textBoxReactionData24.ReadOnly = true;
            this.textBoxReactionData24.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData24.TabIndex = 168;
            // 
            // textBoxAnswers24
            // 
            this.textBoxAnswers24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxAnswers24.Location = new System.Drawing.Point(207, 14);
            this.textBoxAnswers24.Name = "textBoxAnswers24";
            this.textBoxAnswers24.ReadOnly = true;
            this.textBoxAnswers24.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers24.TabIndex = 167;
            // 
            // textBoxQuestion24
            // 
            this.textBoxQuestion24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxQuestion24.Location = new System.Drawing.Point(175, 14);
            this.textBoxQuestion24.Name = "textBoxQuestion24";
            this.textBoxQuestion24.ReadOnly = true;
            this.textBoxQuestion24.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion24.TabIndex = 166;
            // 
            // textBoxIssues24
            // 
            this.textBoxIssues24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxIssues24.Location = new System.Drawing.Point(7, 14);
            this.textBoxIssues24.Name = "textBoxIssues24";
            this.textBoxIssues24.ReadOnly = true;
            this.textBoxIssues24.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues24.TabIndex = 165;
            // 
            // textBoxReadRatio1
            // 
            this.textBoxReadRatio1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadRatio1.Location = new System.Drawing.Point(4, 14);
            this.textBoxReadRatio1.Name = "textBoxReadRatio1";
            this.textBoxReadRatio1.ReadOnly = true;
            this.textBoxReadRatio1.Size = new System.Drawing.Size(100, 20);
            this.textBoxReadRatio1.TabIndex = 172;
            this.textBoxReadRatio1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxReadRatio2
            // 
            this.textBoxReadRatio2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadRatio2.Location = new System.Drawing.Point(4, 34);
            this.textBoxReadRatio2.Name = "textBoxReadRatio2";
            this.textBoxReadRatio2.ReadOnly = true;
            this.textBoxReadRatio2.Size = new System.Drawing.Size(100, 20);
            this.textBoxReadRatio2.TabIndex = 173;
            // 
            // textBoxReadRatio3
            // 
            this.textBoxReadRatio3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadRatio3.Location = new System.Drawing.Point(4, 54);
            this.textBoxReadRatio3.Name = "textBoxReadRatio3";
            this.textBoxReadRatio3.ReadOnly = true;
            this.textBoxReadRatio3.Size = new System.Drawing.Size(100, 20);
            this.textBoxReadRatio3.TabIndex = 174;
            // 
            // textBoxDistortion1
            // 
            this.textBoxDistortion1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxDistortion1.Location = new System.Drawing.Point(4, 14);
            this.textBoxDistortion1.Name = "textBoxDistortion1";
            this.textBoxDistortion1.ReadOnly = true;
            this.textBoxDistortion1.Size = new System.Drawing.Size(100, 20);
            this.textBoxDistortion1.TabIndex = 175;
            this.textBoxDistortion1.TextChanged += new System.EventHandler(this.textBoxDistortion1_TextChanged);
            // 
            // textBoxZone1
            // 
            this.textBoxZone1.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxZone1.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone1.Location = new System.Drawing.Point(5, 14);
            this.textBoxZone1.Name = "textBoxZone1";
            this.textBoxZone1.ReadOnly = true;
            this.textBoxZone1.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone1.TabIndex = 178;
            this.textBoxZone1.TextChanged += new System.EventHandler(this.textBoxZone1_TextChanged);
            // 
            // textBoxZone2
            // 
            this.textBoxZone2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxZone2.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone2.Location = new System.Drawing.Point(5, 34);
            this.textBoxZone2.Name = "textBoxZone2";
            this.textBoxZone2.ReadOnly = true;
            this.textBoxZone2.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone2.TabIndex = 179;
            // 
            // textBoxZone3
            // 
            this.textBoxZone3.BackColor = System.Drawing.Color.Pink;
            this.textBoxZone3.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone3.Location = new System.Drawing.Point(5, 54);
            this.textBoxZone3.Name = "textBoxZone3";
            this.textBoxZone3.ReadOnly = true;
            this.textBoxZone3.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone3.TabIndex = 180;
            // 
            // textBoxZone4
            // 
            this.textBoxZone4.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxZone4.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone4.Location = new System.Drawing.Point(5, 73);
            this.textBoxZone4.Name = "textBoxZone4";
            this.textBoxZone4.ReadOnly = true;
            this.textBoxZone4.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone4.TabIndex = 181;
            // 
            // textBoxZone5
            // 
            this.textBoxZone5.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxZone5.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone5.Location = new System.Drawing.Point(5, 92);
            this.textBoxZone5.Name = "textBoxZone5";
            this.textBoxZone5.ReadOnly = true;
            this.textBoxZone5.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone5.TabIndex = 182;
            // 
            // textBoxZone6
            // 
            this.textBoxZone6.BackColor = System.Drawing.Color.Yellow;
            this.textBoxZone6.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone6.Location = new System.Drawing.Point(5, 111);
            this.textBoxZone6.Name = "textBoxZone6";
            this.textBoxZone6.ReadOnly = true;
            this.textBoxZone6.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone6.TabIndex = 183;
            // 
            // textBoxZone7
            // 
            this.textBoxZone7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxZone7.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxZone7.Location = new System.Drawing.Point(5, 131);
            this.textBoxZone7.Name = "textBoxZone7";
            this.textBoxZone7.ReadOnly = true;
            this.textBoxZone7.Size = new System.Drawing.Size(100, 20);
            this.textBoxZone7.TabIndex = 184;
            // 
            // textBoxTSLHi
            // 
            this.textBoxTSLHi.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxTSLHi.Location = new System.Drawing.Point(5, 41);
            this.textBoxTSLHi.Name = "textBoxTSLHi";
            this.textBoxTSLHi.Size = new System.Drawing.Size(38, 20);
            this.textBoxTSLHi.TabIndex = 185;
            this.textBoxTSLHi.TabStop = false;
            this.textBoxTSLHi.Text = "TSLHi";
            // 
            // textBoxTSLLo
            // 
            this.textBoxTSLLo.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.textBoxTSLLo.Location = new System.Drawing.Point(5, 63);
            this.textBoxTSLLo.Name = "textBoxTSLLo";
            this.textBoxTSLLo.Size = new System.Drawing.Size(38, 20);
            this.textBoxTSLLo.TabIndex = 186;
            this.textBoxTSLLo.TabStop = false;
            this.textBoxTSLLo.Text = "TSLLo";
            // 
            // btnPrint24QData
            // 
            this.btnPrint24QData.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrint24QData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrint24QData.Location = new System.Drawing.Point(46, 37);
            this.btnPrint24QData.Name = "btnPrint24QData";
            this.btnPrint24QData.Size = new System.Drawing.Size(39, 23);
            this.btnPrint24QData.TabIndex = 187;
            this.btnPrint24QData.Text = "Print";
            this.btnPrint24QData.UseVisualStyleBackColor = true;
            this.btnPrint24QData.Click += new System.EventHandler(this.btnPrint24QData_Click);
            // 
            // btnPrint15QData
            // 
            this.btnPrint15QData.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrint15QData.Enabled = false;
            this.btnPrint15QData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrint15QData.Location = new System.Drawing.Point(46, 36);
            this.btnPrint15QData.Name = "btnPrint15QData";
            this.btnPrint15QData.Size = new System.Drawing.Size(40, 23);
            this.btnPrint15QData.TabIndex = 188;
            this.btnPrint15QData.Text = "Print";
            this.btnPrint15QData.UseVisualStyleBackColor = true;
            // 
            // groupBoxZones
            // 
            this.groupBoxZones.Controls.Add(this.textBoxZone1);
            this.groupBoxZones.Controls.Add(this.textBoxZone2);
            this.groupBoxZones.Controls.Add(this.textBoxZone6);
            this.groupBoxZones.Controls.Add(this.textBoxZone7);
            this.groupBoxZones.Controls.Add(this.textBoxZone3);
            this.groupBoxZones.Controls.Add(this.textBoxZone5);
            this.groupBoxZones.Controls.Add(this.textBoxZone4);
            this.groupBoxZones.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxZones.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxZones.Location = new System.Drawing.Point(740, 414);
            this.groupBoxZones.Name = "groupBoxZones";
            this.groupBoxZones.Size = new System.Drawing.Size(109, 156);
            this.groupBoxZones.TabIndex = 189;
            this.groupBoxZones.TabStop = false;
            this.groupBoxZones.Text = "Zones";
            // 
            // groupBoxExit
            // 
            this.groupBoxExit.Controls.Add(this.textBoxAnswers26);
            this.groupBoxExit.Controls.Add(this.textBoxIssues26);
            this.groupBoxExit.Controls.Add(this.textBoxQuestion26);
            this.groupBoxExit.Controls.Add(this.textBoxReactionData26);
            this.groupBoxExit.Controls.Add(this.textBoxReadTime26);
            this.groupBoxExit.Controls.Add(this.textBoxReaction26);
            this.groupBoxExit.Controls.Add(this.textBoxAnswers25);
            this.groupBoxExit.Controls.Add(this.textBoxIssues25);
            this.groupBoxExit.Controls.Add(this.textBoxQuestion25);
            this.groupBoxExit.Controls.Add(this.textBoxReactionData25);
            this.groupBoxExit.Controls.Add(this.textBoxReadTime25);
            this.groupBoxExit.Controls.Add(this.textBoxReaction25);
            this.groupBoxExit.Controls.Add(this.textBoxAnswers24);
            this.groupBoxExit.Controls.Add(this.textBoxIssues24);
            this.groupBoxExit.Controls.Add(this.textBoxQuestion24);
            this.groupBoxExit.Controls.Add(this.textBoxReactionData24);
            this.groupBoxExit.Controls.Add(this.textBoxReadTime24);
            this.groupBoxExit.Controls.Add(this.textBoxReaction24);
            this.groupBoxExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxExit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxExit.Location = new System.Drawing.Point(111, 663);
            this.groupBoxExit.Name = "groupBoxExit";
            this.groupBoxExit.Size = new System.Drawing.Size(614, 80);
            this.groupBoxExit.TabIndex = 190;
            this.groupBoxExit.TabStop = false;
            this.groupBoxExit.Text = "Exit";
            this.groupBoxExit.Enter += new System.EventHandler(this.groupBoxExit_Enter);
            // 
            // textBoxAnswers26
            // 
            this.textBoxAnswers26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxAnswers26.Location = new System.Drawing.Point(207, 56);
            this.textBoxAnswers26.Name = "textBoxAnswers26";
            this.textBoxAnswers26.ReadOnly = true;
            this.textBoxAnswers26.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers26.TabIndex = 179;
            // 
            // textBoxIssues26
            // 
            this.textBoxIssues26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxIssues26.Location = new System.Drawing.Point(7, 56);
            this.textBoxIssues26.Name = "textBoxIssues26";
            this.textBoxIssues26.ReadOnly = true;
            this.textBoxIssues26.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues26.TabIndex = 177;
            // 
            // textBoxQuestion26
            // 
            this.textBoxQuestion26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxQuestion26.Location = new System.Drawing.Point(175, 56);
            this.textBoxQuestion26.Name = "textBoxQuestion26";
            this.textBoxQuestion26.ReadOnly = true;
            this.textBoxQuestion26.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion26.TabIndex = 178;
            // 
            // textBoxReactionData26
            // 
            this.textBoxReactionData26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReactionData26.Location = new System.Drawing.Point(307, 56);
            this.textBoxReactionData26.Name = "textBoxReactionData26";
            this.textBoxReactionData26.ReadOnly = true;
            this.textBoxReactionData26.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData26.TabIndex = 180;
            // 
            // textBoxReadTime26
            // 
            this.textBoxReadTime26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadTime26.Location = new System.Drawing.Point(407, 56);
            this.textBoxReadTime26.Name = "textBoxReadTime26";
            this.textBoxReadTime26.ReadOnly = true;
            this.textBoxReadTime26.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime26.TabIndex = 181;
            // 
            // textBoxReaction26
            // 
            this.textBoxReaction26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReaction26.Location = new System.Drawing.Point(469, 56);
            this.textBoxReaction26.Name = "textBoxReaction26";
            this.textBoxReaction26.ReadOnly = true;
            this.textBoxReaction26.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction26.TabIndex = 182;
            // 
            // textBoxAnswers25
            // 
            this.textBoxAnswers25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxAnswers25.Location = new System.Drawing.Point(207, 35);
            this.textBoxAnswers25.Name = "textBoxAnswers25";
            this.textBoxAnswers25.ReadOnly = true;
            this.textBoxAnswers25.Size = new System.Drawing.Size(100, 20);
            this.textBoxAnswers25.TabIndex = 173;
            // 
            // textBoxIssues25
            // 
            this.textBoxIssues25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxIssues25.Location = new System.Drawing.Point(7, 35);
            this.textBoxIssues25.Name = "textBoxIssues25";
            this.textBoxIssues25.ReadOnly = true;
            this.textBoxIssues25.Size = new System.Drawing.Size(171, 20);
            this.textBoxIssues25.TabIndex = 171;
            // 
            // textBoxQuestion25
            // 
            this.textBoxQuestion25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxQuestion25.Location = new System.Drawing.Point(175, 35);
            this.textBoxQuestion25.Name = "textBoxQuestion25";
            this.textBoxQuestion25.ReadOnly = true;
            this.textBoxQuestion25.Size = new System.Drawing.Size(32, 20);
            this.textBoxQuestion25.TabIndex = 172;
            // 
            // textBoxReactionData25
            // 
            this.textBoxReactionData25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReactionData25.Location = new System.Drawing.Point(307, 35);
            this.textBoxReactionData25.Name = "textBoxReactionData25";
            this.textBoxReactionData25.ReadOnly = true;
            this.textBoxReactionData25.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionData25.TabIndex = 174;
            // 
            // textBoxReadTime25
            // 
            this.textBoxReadTime25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReadTime25.Location = new System.Drawing.Point(407, 35);
            this.textBoxReadTime25.Name = "textBoxReadTime25";
            this.textBoxReadTime25.ReadOnly = true;
            this.textBoxReadTime25.Size = new System.Drawing.Size(62, 20);
            this.textBoxReadTime25.TabIndex = 175;
            // 
            // textBoxReaction25
            // 
            this.textBoxReaction25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxReaction25.Location = new System.Drawing.Point(469, 35);
            this.textBoxReaction25.Name = "textBoxReaction25";
            this.textBoxReaction25.ReadOnly = true;
            this.textBoxReaction25.Size = new System.Drawing.Size(138, 20);
            this.textBoxReaction25.TabIndex = 176;
            // 
            // groupBoxUnknowns
            // 
            this.groupBoxUnknowns.Controls.Add(this.textBoxIssues21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReactionData22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReactionData21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReadTime23);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReaction21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxAnswers23);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReactionData23);
            this.groupBoxUnknowns.Controls.Add(this.textBoxAnswers22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxQuestion23);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReadTime22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReaction22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxQuestion22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxIssues22);
            this.groupBoxUnknowns.Controls.Add(this.textBoxAnswers21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReaction23);
            this.groupBoxUnknowns.Controls.Add(this.textBoxReadTime21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxQuestion21);
            this.groupBoxUnknowns.Controls.Add(this.textBoxIssues23);
            this.groupBoxUnknowns.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxUnknowns.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxUnknowns.Location = new System.Drawing.Point(111, 580);
            this.groupBoxUnknowns.Name = "groupBoxUnknowns";
            this.groupBoxUnknowns.Size = new System.Drawing.Size(614, 82);
            this.groupBoxUnknowns.TabIndex = 191;
            this.groupBoxUnknowns.TabStop = false;
            this.groupBoxUnknowns.Text = "Unknowns";
            // 
            // groupBoxTerrorism
            // 
            this.groupBoxTerrorism.Controls.Add(this.textBoxIssues18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxQuestion18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxAnswers18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReactionData18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReadTime18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReaction18);
            this.groupBoxTerrorism.Controls.Add(this.textBoxIssues19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxQuestion19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxAnswers19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReactionData19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReadTime19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReaction19);
            this.groupBoxTerrorism.Controls.Add(this.textBoxIssues20);
            this.groupBoxTerrorism.Controls.Add(this.textBoxQuestion20);
            this.groupBoxTerrorism.Controls.Add(this.textBoxAnswers20);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReactionData20);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReaction20);
            this.groupBoxTerrorism.Controls.Add(this.textBoxReadTime20);
            this.groupBoxTerrorism.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxTerrorism.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxTerrorism.Location = new System.Drawing.Point(111, 496);
            this.groupBoxTerrorism.Name = "groupBoxTerrorism";
            this.groupBoxTerrorism.Size = new System.Drawing.Size(614, 82);
            this.groupBoxTerrorism.TabIndex = 192;
            this.groupBoxTerrorism.TabStop = false;
            this.groupBoxTerrorism.Text = "Terrorism";
            // 
            // groupBoxSexOffenders
            // 
            this.groupBoxSexOffenders.Controls.Add(this.textBoxIssues9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxQuestion9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxAnswers9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReactionData9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReadTime9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReaction9);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxIssues10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxQuestion10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxAnswers10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReactionData10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReadTime10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReaction10);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxIssues11);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxQuestion11);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxAnswers11);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReactionData11);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReadTime11);
            this.groupBoxSexOffenders.Controls.Add(this.textBoxReaction11);
            this.groupBoxSexOffenders.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxSexOffenders.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxSexOffenders.Location = new System.Drawing.Point(111, 250);
            this.groupBoxSexOffenders.Name = "groupBoxSexOffenders";
            this.groupBoxSexOffenders.Size = new System.Drawing.Size(614, 82);
            this.groupBoxSexOffenders.TabIndex = 193;
            this.groupBoxSexOffenders.TabStop = false;
            this.groupBoxSexOffenders.Text = "Sex Offenders";
            // 
            // groupBoxSmuggling
            // 
            this.groupBoxSmuggling.Controls.Add(this.textBoxIssues15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxQuestion15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxAnswers15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReactionData15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReadTime15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReaction15);
            this.groupBoxSmuggling.Controls.Add(this.textBoxIssues16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxQuestion16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxAnswers16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReactionData16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReadTime16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReaction16);
            this.groupBoxSmuggling.Controls.Add(this.textBoxIssues17);
            this.groupBoxSmuggling.Controls.Add(this.textBoxQuestion17);
            this.groupBoxSmuggling.Controls.Add(this.textBoxAnswers17);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReactionData17);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReadTime17);
            this.groupBoxSmuggling.Controls.Add(this.textBoxReaction17);
            this.groupBoxSmuggling.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxSmuggling.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxSmuggling.Location = new System.Drawing.Point(111, 414);
            this.groupBoxSmuggling.Name = "groupBoxSmuggling";
            this.groupBoxSmuggling.Size = new System.Drawing.Size(614, 82);
            this.groupBoxSmuggling.TabIndex = 194;
            this.groupBoxSmuggling.TabStop = false;
            this.groupBoxSmuggling.Text = "Smuggling";
            // 
            // groupBoxArrest
            // 
            this.groupBoxArrest.Controls.Add(this.textBoxIssues12);
            this.groupBoxArrest.Controls.Add(this.textBoxQuestion12);
            this.groupBoxArrest.Controls.Add(this.textBoxAnswers12);
            this.groupBoxArrest.Controls.Add(this.textBoxReactionData12);
            this.groupBoxArrest.Controls.Add(this.textBoxReadTime12);
            this.groupBoxArrest.Controls.Add(this.textBoxReaction12);
            this.groupBoxArrest.Controls.Add(this.textBoxIssues13);
            this.groupBoxArrest.Controls.Add(this.textBoxQuestion13);
            this.groupBoxArrest.Controls.Add(this.textBoxAnswers13);
            this.groupBoxArrest.Controls.Add(this.textBoxReactionData13);
            this.groupBoxArrest.Controls.Add(this.textBoxReadTime13);
            this.groupBoxArrest.Controls.Add(this.textBoxReaction13);
            this.groupBoxArrest.Controls.Add(this.textBoxIssues14);
            this.groupBoxArrest.Controls.Add(this.textBoxQuestion14);
            this.groupBoxArrest.Controls.Add(this.textBoxAnswers14);
            this.groupBoxArrest.Controls.Add(this.textBoxReactionData14);
            this.groupBoxArrest.Controls.Add(this.textBoxReadTime14);
            this.groupBoxArrest.Controls.Add(this.textBoxReaction14);
            this.groupBoxArrest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxArrest.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxArrest.Location = new System.Drawing.Point(111, 332);
            this.groupBoxArrest.Name = "groupBoxArrest";
            this.groupBoxArrest.Size = new System.Drawing.Size(614, 82);
            this.groupBoxArrest.TabIndex = 194;
            this.groupBoxArrest.TabStop = false;
            this.groupBoxArrest.Text = "Arrest and Convictions";
            // 
            // groupBoxAccidents
            // 
            this.groupBoxAccidents.Controls.Add(this.textBoxIssues3);
            this.groupBoxAccidents.Controls.Add(this.textBoxQuestion3);
            this.groupBoxAccidents.Controls.Add(this.textBoxAnswers3);
            this.groupBoxAccidents.Controls.Add(this.textBoxReactionData3);
            this.groupBoxAccidents.Controls.Add(this.textBoxReadTime3);
            this.groupBoxAccidents.Controls.Add(this.textBoxReaction3);
            this.groupBoxAccidents.Controls.Add(this.textBoxIssues4);
            this.groupBoxAccidents.Controls.Add(this.textBoxQuestion4);
            this.groupBoxAccidents.Controls.Add(this.textBoxAnswers4);
            this.groupBoxAccidents.Controls.Add(this.textBoxReactionData4);
            this.groupBoxAccidents.Controls.Add(this.textBoxReadTime4);
            this.groupBoxAccidents.Controls.Add(this.textBoxReaction4);
            this.groupBoxAccidents.Controls.Add(this.textBoxIssues5);
            this.groupBoxAccidents.Controls.Add(this.textBoxQuestion5);
            this.groupBoxAccidents.Controls.Add(this.textBoxAnswers5);
            this.groupBoxAccidents.Controls.Add(this.textBoxReactionData5);
            this.groupBoxAccidents.Controls.Add(this.textBoxReadTime5);
            this.groupBoxAccidents.Controls.Add(this.textBoxReaction5);
            this.groupBoxAccidents.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxAccidents.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxAccidents.Location = new System.Drawing.Point(112, 86);
            this.groupBoxAccidents.Name = "groupBoxAccidents";
            this.groupBoxAccidents.Size = new System.Drawing.Size(614, 82);
            this.groupBoxAccidents.TabIndex = 194;
            this.groupBoxAccidents.TabStop = false;
            this.groupBoxAccidents.Text = "Accidents";
            // 
            // groupBoxIllegalDrugs
            // 
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxIssues6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxQuestion6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxAnswers6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReactionData6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReadTime6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReaction6);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxIssues7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxQuestion7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxAnswers7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReactionData7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReadTime7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReaction7);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxIssues8);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxQuestion8);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxAnswers8);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReactionData8);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReadTime8);
            this.groupBoxIllegalDrugs.Controls.Add(this.textBoxReaction8);
            this.groupBoxIllegalDrugs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxIllegalDrugs.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxIllegalDrugs.Location = new System.Drawing.Point(111, 168);
            this.groupBoxIllegalDrugs.Name = "groupBoxIllegalDrugs";
            this.groupBoxIllegalDrugs.Size = new System.Drawing.Size(614, 82);
            this.groupBoxIllegalDrugs.TabIndex = 195;
            this.groupBoxIllegalDrugs.TabStop = false;
            this.groupBoxIllegalDrugs.Text = "Illegal Drugs";
            // 
            // groupBoxAdopt
            // 
            this.groupBoxAdopt.Controls.Add(this.textBoxIssues);
            this.groupBoxAdopt.Controls.Add(this.textBoxIssues1);
            this.groupBoxAdopt.Controls.Add(this.textBoxQuestion1);
            this.groupBoxAdopt.Controls.Add(this.textBoxAnswers1);
            this.groupBoxAdopt.Controls.Add(this.textBoxReactionData1);
            this.groupBoxAdopt.Controls.Add(this.textBoxReadTime1);
            this.groupBoxAdopt.Controls.Add(this.textBoxReaction1);
            this.groupBoxAdopt.Controls.Add(this.textBoxQuestion);
            this.groupBoxAdopt.Controls.Add(this.textBoxAnswers);
            this.groupBoxAdopt.Controls.Add(this.textBoxReactionData);
            this.groupBoxAdopt.Controls.Add(this.textBoxReadTime);
            this.groupBoxAdopt.Controls.Add(this.textBoxReaction);
            this.groupBoxAdopt.Controls.Add(this.textBoxIssues2);
            this.groupBoxAdopt.Controls.Add(this.textBoxQuestion2);
            this.groupBoxAdopt.Controls.Add(this.textBoxAnswers2);
            this.groupBoxAdopt.Controls.Add(this.textBoxReactionData2);
            this.groupBoxAdopt.Controls.Add(this.textBoxReadTime2);
            this.groupBoxAdopt.Controls.Add(this.textBoxReaction2);
            this.groupBoxAdopt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxAdopt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxAdopt.Location = new System.Drawing.Point(112, 3);
            this.groupBoxAdopt.Name = "groupBoxAdopt";
            this.groupBoxAdopt.Size = new System.Drawing.Size(614, 82);
            this.groupBoxAdopt.TabIndex = 195;
            this.groupBoxAdopt.TabStop = false;
            this.groupBoxAdopt.Text = "Adopt";
            // 
            // groupBoxReadRatio
            // 
            this.groupBoxReadRatio.Controls.Add(this.textBoxReadRatio1);
            this.groupBoxReadRatio.Controls.Add(this.textBoxReadRatio2);
            this.groupBoxReadRatio.Controls.Add(this.textBoxReadRatio3);
            this.groupBoxReadRatio.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxReadRatio.Location = new System.Drawing.Point(855, 431);
            this.groupBoxReadRatio.Name = "groupBoxReadRatio";
            this.groupBoxReadRatio.Size = new System.Drawing.Size(109, 80);
            this.groupBoxReadRatio.TabIndex = 196;
            this.groupBoxReadRatio.TabStop = false;
            this.groupBoxReadRatio.Text = "Read Ratio";
            // 
            // groupBoxDistort
            // 
            this.groupBoxDistort.Controls.Add(this.textBoxDistortion1);
            this.groupBoxDistort.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxDistort.Location = new System.Drawing.Point(740, 373);
            this.groupBoxDistort.Name = "groupBoxDistort";
            this.groupBoxDistort.Size = new System.Drawing.Size(109, 39);
            this.groupBoxDistort.TabIndex = 197;
            this.groupBoxDistort.TabStop = false;
            this.groupBoxDistort.Text = "Distortion";
            // 
            // groupBoxAverages
            // 
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg12);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg11);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg11);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg10);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg10);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg9);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg9);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg8);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg8);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg7);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg7);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg6);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg6);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg5);
            this.groupBoxAverages.Controls.Add(this.textBoxAvgTest);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg1);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg2);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg3);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg4);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg5);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg13);
            this.groupBoxAverages.Controls.Add(this.textBoxReactionDataAvg14);
            this.groupBoxAverages.Controls.Add(this.textBoxStandardDeviation);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg1);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg2);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg3);
            this.groupBoxAverages.Controls.Add(this.textBoxAvg4);
            this.groupBoxAverages.Controls.Add(this.textBox6);
            this.groupBoxAverages.Controls.Add(this.textBox7);
            this.groupBoxAverages.Controls.Add(this.textBox8);
            this.groupBoxAverages.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxAverages.Location = new System.Drawing.Point(740, -1);
            this.groupBoxAverages.Name = "groupBoxAverages";
            this.groupBoxAverages.Size = new System.Drawing.Size(224, 330);
            this.groupBoxAverages.TabIndex = 197;
            this.groupBoxAverages.TabStop = false;
            this.groupBoxAverages.Text = "Summary Averages";
            // 
            // textBoxReactionDataAvg12
            // 
            this.textBoxReactionDataAvg12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionDataAvg12.Location = new System.Drawing.Point(112, 242);
            this.textBoxReactionDataAvg12.Name = "textBoxReactionDataAvg12";
            this.textBoxReactionDataAvg12.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg12.TabIndex = 127;
            // 
            // textBoxAvg11
            // 
            this.textBoxAvg11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAvg11.Location = new System.Drawing.Point(12, 242);
            this.textBoxAvg11.Name = "textBoxAvg11";
            this.textBoxAvg11.ReadOnly = true;
            this.textBoxAvg11.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg11.TabIndex = 128;
            this.textBoxAvg11.Text = "Smuggling";
            this.textBoxAvg11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg11
            // 
            this.textBoxReactionDataAvg11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionDataAvg11.Location = new System.Drawing.Point(112, 222);
            this.textBoxReactionDataAvg11.Name = "textBoxReactionDataAvg11";
            this.textBoxReactionDataAvg11.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg11.TabIndex = 125;
            // 
            // textBoxAvg10
            // 
            this.textBoxAvg10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAvg10.Location = new System.Drawing.Point(12, 222);
            this.textBoxAvg10.Name = "textBoxAvg10";
            this.textBoxAvg10.ReadOnly = true;
            this.textBoxAvg10.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg10.TabIndex = 126;
            this.textBoxAvg10.Text = "Arrest/Convictions";
            this.textBoxAvg10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg10
            // 
            this.textBoxReactionDataAvg10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionDataAvg10.Location = new System.Drawing.Point(112, 202);
            this.textBoxReactionDataAvg10.Name = "textBoxReactionDataAvg10";
            this.textBoxReactionDataAvg10.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg10.TabIndex = 123;
            // 
            // textBoxAvg9
            // 
            this.textBoxAvg9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAvg9.Location = new System.Drawing.Point(12, 202);
            this.textBoxAvg9.Name = "textBoxAvg9";
            this.textBoxAvg9.ReadOnly = true;
            this.textBoxAvg9.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg9.TabIndex = 124;
            this.textBoxAvg9.Text = "Sex Offender";
            this.textBoxAvg9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg9
            // 
            this.textBoxReactionDataAvg9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionDataAvg9.Location = new System.Drawing.Point(112, 181);
            this.textBoxReactionDataAvg9.Name = "textBoxReactionDataAvg9";
            this.textBoxReactionDataAvg9.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg9.TabIndex = 121;
            // 
            // textBoxAvg8
            // 
            this.textBoxAvg8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAvg8.Location = new System.Drawing.Point(12, 181);
            this.textBoxAvg8.Name = "textBoxAvg8";
            this.textBoxAvg8.ReadOnly = true;
            this.textBoxAvg8.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg8.TabIndex = 122;
            this.textBoxAvg8.Text = "Drugs";
            this.textBoxAvg8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg8
            // 
            this.textBoxReactionDataAvg8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxReactionDataAvg8.Location = new System.Drawing.Point(112, 161);
            this.textBoxReactionDataAvg8.Name = "textBoxReactionDataAvg8";
            this.textBoxReactionDataAvg8.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg8.TabIndex = 0;
            // 
            // textBoxAvg7
            // 
            this.textBoxAvg7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxAvg7.Location = new System.Drawing.Point(12, 161);
            this.textBoxAvg7.Name = "textBoxAvg7";
            this.textBoxAvg7.ReadOnly = true;
            this.textBoxAvg7.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg7.TabIndex = 120;
            this.textBoxAvg7.Text = "Accidents";
            this.textBoxAvg7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg7
            // 
            this.textBoxReactionDataAvg7.BackColor = System.Drawing.Color.Yellow;
            this.textBoxReactionDataAvg7.Location = new System.Drawing.Point(112, 141);
            this.textBoxReactionDataAvg7.Name = "textBoxReactionDataAvg7";
            this.textBoxReactionDataAvg7.ReadOnly = true;
            this.textBoxReactionDataAvg7.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg7.TabIndex = 117;
            // 
            // textBoxAvg6
            // 
            this.textBoxAvg6.BackColor = System.Drawing.Color.Yellow;
            this.textBoxAvg6.Location = new System.Drawing.Point(12, 141);
            this.textBoxAvg6.Name = "textBoxAvg6";
            this.textBoxAvg6.ReadOnly = true;
            this.textBoxAvg6.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg6.TabIndex = 118;
            this.textBoxAvg6.Text = "Base Avg Q21/25";
            this.textBoxAvg6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxReactionDataAvg6
            // 
            this.textBoxReactionDataAvg6.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxReactionDataAvg6.Location = new System.Drawing.Point(112, 121);
            this.textBoxReactionDataAvg6.Name = "textBoxReactionDataAvg6";
            this.textBoxReactionDataAvg6.ReadOnly = true;
            this.textBoxReactionDataAvg6.Size = new System.Drawing.Size(100, 20);
            this.textBoxReactionDataAvg6.TabIndex = 115;
            // 
            // textBoxAvg5
            // 
            this.textBoxAvg5.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxAvg5.Location = new System.Drawing.Point(12, 121);
            this.textBoxAvg5.Name = "textBoxAvg5";
            this.textBoxAvg5.ReadOnly = true;
            this.textBoxAvg5.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvg5.TabIndex = 116;
            this.textBoxAvg5.Text = "Base Avg Q17/20";
            this.textBoxAvg5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBoxControls
            // 
            this.groupBoxControls.Controls.Add(this.checkBoxDistortLogic);
            this.groupBoxControls.Controls.Add(this.checkBoxSpr);
            this.groupBoxControls.Controls.Add(this.groupBox6);
            this.groupBoxControls.Controls.Add(this.groupBox3);
            this.groupBoxControls.Controls.Add(this.label1);
            this.groupBoxControls.Controls.Add(this.groupBox1);
            this.groupBoxControls.Controls.Add(this.groupBoxTSLAdjust);
            this.groupBoxControls.Controls.Add(this.checkBoxPrintGraph);
            this.groupBoxControls.Controls.Add(this.groupBox15Q);
            this.groupBoxControls.Controls.Add(this.groupBox24Q);
            this.groupBoxControls.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxControls.Location = new System.Drawing.Point(3, 5);
            this.groupBoxControls.Name = "groupBoxControls";
            this.groupBoxControls.Size = new System.Drawing.Size(100, 726);
            this.groupBoxControls.TabIndex = 198;
            this.groupBoxControls.TabStop = false;
            this.groupBoxControls.Text = "Controls";
            // 
            // checkBoxDistortLogic
            // 
            this.checkBoxDistortLogic.AutoSize = true;
            this.checkBoxDistortLogic.Location = new System.Drawing.Point(10, 694);
            this.checkBoxDistortLogic.Name = "checkBoxDistortLogic";
            this.checkBoxDistortLogic.Size = new System.Drawing.Size(73, 17);
            this.checkBoxDistortLogic.TabIndex = 204;
            this.checkBoxDistortLogic.Text = "Distort On";
            this.checkBoxDistortLogic.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpr
            // 
            this.checkBoxSpr.AutoSize = true;
            this.checkBoxSpr.Location = new System.Drawing.Point(10, 680);
            this.checkBoxSpr.Name = "checkBoxSpr";
            this.checkBoxSpr.Size = new System.Drawing.Size(77, 17);
            this.checkBoxSpr.TabIndex = 203;
            this.checkBoxSpr.Text = "Spr 2-3 Off";
            this.checkBoxSpr.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnAutoAdjust18);
            this.groupBox6.Controls.Add(this.btnLoad18Data);
            this.groupBox6.Controls.Add(this.btnPrint18QData);
            this.groupBox6.Controls.Add(this.btnPrintPreview18Q);
            this.groupBox6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox6.Location = new System.Drawing.Point(3, 110);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(93, 88);
            this.groupBox6.TabIndex = 202;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "18 Questions";
            // 
            // btnAutoAdjust18
            // 
            this.btnAutoAdjust18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAutoAdjust18.Location = new System.Drawing.Point(5, 61);
            this.btnAutoAdjust18.Name = "btnAutoAdjust18";
            this.btnAutoAdjust18.Size = new System.Drawing.Size(79, 23);
            this.btnAutoAdjust18.TabIndex = 194;
            this.btnAutoAdjust18.Text = "Auto Adjust";
            this.btnAutoAdjust18.UseVisualStyleBackColor = true;
            this.btnAutoAdjust18.Click += new System.EventHandler(this.btnAutoAdjust18_Click);
            // 
            // btnLoad18Data
            // 
            this.btnLoad18Data.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLoad18Data.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoad18Data.Location = new System.Drawing.Point(6, 15);
            this.btnLoad18Data.Name = "btnLoad18Data";
            this.btnLoad18Data.Size = new System.Drawing.Size(79, 23);
            this.btnLoad18Data.TabIndex = 116;
            this.btnLoad18Data.Text = "Load Data";
            this.btnLoad18Data.UseVisualStyleBackColor = true;
            this.btnLoad18Data.Click += new System.EventHandler(this.btnLoad18Data_Click);
            // 
            // btnPrint18QData
            // 
            this.btnPrint18QData.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrint18QData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrint18QData.Location = new System.Drawing.Point(46, 37);
            this.btnPrint18QData.Name = "btnPrint18QData";
            this.btnPrint18QData.Size = new System.Drawing.Size(39, 23);
            this.btnPrint18QData.TabIndex = 187;
            this.btnPrint18QData.Text = "Print";
            this.btnPrint18QData.UseVisualStyleBackColor = true;
            this.btnPrint18QData.Click += new System.EventHandler(this.btnPrint18QData_Click);
            // 
            // btnPrintPreview18Q
            // 
            this.btnPrintPreview18Q.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreview18Q.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreview18Q.Location = new System.Drawing.Point(6, 37);
            this.btnPrintPreview18Q.Name = "btnPrintPreview18Q";
            this.btnPrintPreview18Q.Size = new System.Drawing.Size(39, 23);
            this.btnPrintPreview18Q.TabIndex = 189;
            this.btnPrintPreview18Q.Text = "PP";
            this.btnPrintPreview18Q.UseVisualStyleBackColor = true;
            this.btnPrintPreview18Q.Click += new System.EventHandler(this.btnPrintPreview18Q_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnPrint26QData_2);
            this.groupBox3.Controls.Add(this.btnPrintPreview26Q_2);
            this.groupBox3.Controls.Add(this.btnAutoAdjust26);
            this.groupBox3.Controls.Add(this.btnLoad26Data);
            this.groupBox3.Controls.Add(this.btnPrint26QData);
            this.groupBox3.Controls.Add(this.btnPrintPreview26Q);
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox3.Location = new System.Drawing.Point(4, 327);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(93, 118);
            this.groupBox3.TabIndex = 201;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "26 Questions";
            // 
            // btnPrint26QData_2
            // 
            this.btnPrint26QData_2.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrint26QData_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrint26QData_2.Location = new System.Drawing.Point(47, 65);
            this.btnPrint26QData_2.Name = "btnPrint26QData_2";
            this.btnPrint26QData_2.Size = new System.Drawing.Size(37, 23);
            this.btnPrint26QData_2.TabIndex = 196;
            this.btnPrint26QData_2.Text = "P2";
            this.btnPrint26QData_2.UseVisualStyleBackColor = true;
            this.btnPrint26QData_2.Click += new System.EventHandler(this.btnPrint26QData_2_Click);
            // 
            // btnPrintPreview26Q_2
            // 
            this.btnPrintPreview26Q_2.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreview26Q_2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreview26Q_2.Location = new System.Drawing.Point(47, 40);
            this.btnPrintPreview26Q_2.Name = "btnPrintPreview26Q_2";
            this.btnPrintPreview26Q_2.Size = new System.Drawing.Size(37, 23);
            this.btnPrintPreview26Q_2.TabIndex = 195;
            this.btnPrintPreview26Q_2.Text = "PR2";
            this.btnPrintPreview26Q_2.UseVisualStyleBackColor = true;
            this.btnPrintPreview26Q_2.Click += new System.EventHandler(this.btnPrintPreview26Q_2_Click);
            // 
            // btnAutoAdjust26
            // 
            this.btnAutoAdjust26.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAutoAdjust26.Location = new System.Drawing.Point(6, 89);
            this.btnAutoAdjust26.Name = "btnAutoAdjust26";
            this.btnAutoAdjust26.Size = new System.Drawing.Size(79, 23);
            this.btnAutoAdjust26.TabIndex = 194;
            this.btnAutoAdjust26.Text = "Auto Adjust";
            this.btnAutoAdjust26.UseVisualStyleBackColor = true;
            this.btnAutoAdjust26.Click += new System.EventHandler(this.btnAutoAdjust26_Click);
            // 
            // btnLoad26Data
            // 
            this.btnLoad26Data.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLoad26Data.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoad26Data.Location = new System.Drawing.Point(6, 15);
            this.btnLoad26Data.Name = "btnLoad26Data";
            this.btnLoad26Data.Size = new System.Drawing.Size(79, 23);
            this.btnLoad26Data.TabIndex = 116;
            this.btnLoad26Data.Text = "Load Data";
            this.btnLoad26Data.UseVisualStyleBackColor = true;
            this.btnLoad26Data.Click += new System.EventHandler(this.btnLoad26Data_Click);
            // 
            // btnPrint26QData
            // 
            this.btnPrint26QData.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrint26QData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrint26QData.Location = new System.Drawing.Point(6, 65);
            this.btnPrint26QData.Name = "btnPrint26QData";
            this.btnPrint26QData.Size = new System.Drawing.Size(37, 23);
            this.btnPrint26QData.TabIndex = 187;
            this.btnPrint26QData.Text = "P1";
            this.btnPrint26QData.UseVisualStyleBackColor = true;
            this.btnPrint26QData.Click += new System.EventHandler(this.btnPrint26QData_Click);
            // 
            // btnPrintPreview26Q
            // 
            this.btnPrintPreview26Q.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreview26Q.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreview26Q.Location = new System.Drawing.Point(6, 40);
            this.btnPrintPreview26Q.Name = "btnPrintPreview26Q";
            this.btnPrintPreview26Q.Size = new System.Drawing.Size(37, 23);
            this.btnPrintPreview26Q.TabIndex = 189;
            this.btnPrintPreview26Q.Text = "PR1";
            this.btnPrintPreview26Q.UseVisualStyleBackColor = true;
            this.btnPrintPreview26Q.Click += new System.EventHandler(this.btnPrintPreview26Q_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(3, 710);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 200;
            this.label1.Text = "version 2.93";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnLoadKOTData);
            this.groupBox1.Controls.Add(this.btnPrintKOT);
            this.groupBox1.Controls.Add(this.btnPrintPreviewKOT);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Location = new System.Drawing.Point(4, 448);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(93, 67);
            this.groupBox1.TabIndex = 199;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "KOT Questions";
            // 
            // btnLoadKOTData
            // 
            this.btnLoadKOTData.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLoadKOTData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadKOTData.Location = new System.Drawing.Point(6, 15);
            this.btnLoadKOTData.Name = "btnLoadKOTData";
            this.btnLoadKOTData.Size = new System.Drawing.Size(79, 23);
            this.btnLoadKOTData.TabIndex = 116;
            this.btnLoadKOTData.Text = "Load Data";
            this.btnLoadKOTData.UseVisualStyleBackColor = true;
            this.btnLoadKOTData.Click += new System.EventHandler(this.btnLoadKOTData_Click);
            // 
            // btnPrintKOT
            // 
            this.btnPrintKOT.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintKOT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintKOT.Location = new System.Drawing.Point(47, 38);
            this.btnPrintKOT.Name = "btnPrintKOT";
            this.btnPrintKOT.Size = new System.Drawing.Size(38, 23);
            this.btnPrintKOT.TabIndex = 187;
            this.btnPrintKOT.Text = "Print";
            this.btnPrintKOT.UseVisualStyleBackColor = true;
            this.btnPrintKOT.Click += new System.EventHandler(this.btnPrintKOT_Click);
            // 
            // btnPrintPreviewKOT
            // 
            this.btnPrintPreviewKOT.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreviewKOT.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreviewKOT.Location = new System.Drawing.Point(6, 38);
            this.btnPrintPreviewKOT.Name = "btnPrintPreviewKOT";
            this.btnPrintPreviewKOT.Size = new System.Drawing.Size(37, 23);
            this.btnPrintPreviewKOT.TabIndex = 189;
            this.btnPrintPreviewKOT.Text = "PP";
            this.btnPrintPreviewKOT.UseVisualStyleBackColor = true;
            this.btnPrintPreviewKOT.Click += new System.EventHandler(this.btnPrintPreviewKOT_Click);
            // 
            // groupBoxTSLAdjust
            // 
            this.groupBoxTSLAdjust.Controls.Add(this.numericNumberOfHits);
            this.groupBoxTSLAdjust.Controls.Add(this.txtNumHits);
            this.groupBoxTSLAdjust.Controls.Add(this.txtTSLLowPercentage);
            this.groupBoxTSLAdjust.Controls.Add(this.numericTSLLow);
            this.groupBoxTSLAdjust.Controls.Add(this.numericUpDownTSLAdjust);
            this.groupBoxTSLAdjust.Controls.Add(this.textBoxTSLHi);
            this.groupBoxTSLAdjust.Controls.Add(this.txtTSLHi);
            this.groupBoxTSLAdjust.Controls.Add(this.textBoxTSLLo);
            this.groupBoxTSLAdjust.Controls.Add(this.txtTSLLo);
            this.groupBoxTSLAdjust.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBoxTSLAdjust.Location = new System.Drawing.Point(4, 518);
            this.groupBoxTSLAdjust.Name = "groupBoxTSLAdjust";
            this.groupBoxTSLAdjust.Size = new System.Drawing.Size(91, 147);
            this.groupBoxTSLAdjust.TabIndex = 198;
            this.groupBoxTSLAdjust.TabStop = false;
            this.groupBoxTSLAdjust.Text = "TSL Controls";
            // 
            // numericNumberOfHits
            // 
            this.numericNumberOfHits.Location = new System.Drawing.Point(47, 119);
            this.numericNumberOfHits.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericNumberOfHits.Name = "numericNumberOfHits";
            this.numericNumberOfHits.Size = new System.Drawing.Size(39, 20);
            this.numericNumberOfHits.TabIndex = 196;
            this.numericNumberOfHits.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // txtNumHits
            // 
            this.txtNumHits.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtNumHits.Location = new System.Drawing.Point(4, 119);
            this.txtNumHits.Name = "txtNumHits";
            this.txtNumHits.Size = new System.Drawing.Size(39, 20);
            this.txtNumHits.TabIndex = 195;
            this.txtNumHits.Text = "#SPR";
            // 
            // txtTSLLowPercentage
            // 
            this.txtTSLLowPercentage.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtTSLLowPercentage.Location = new System.Drawing.Point(4, 94);
            this.txtTSLLowPercentage.Name = "txtTSLLowPercentage";
            this.txtTSLLowPercentage.Size = new System.Drawing.Size(39, 20);
            this.txtTSLLowPercentage.TabIndex = 0;
            this.txtTSLLowPercentage.Text = "TslLo%";
            // 
            // numericTSLLow
            // 
            this.numericTSLLow.Location = new System.Drawing.Point(47, 94);
            this.numericTSLLow.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericTSLLow.Name = "numericTSLLow";
            this.numericTSLLow.Size = new System.Drawing.Size(39, 20);
            this.numericTSLLow.TabIndex = 194;
            this.numericTSLLow.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // numericUpDownTSLAdjust
            // 
            this.numericUpDownTSLAdjust.Location = new System.Drawing.Point(5, 19);
            this.numericUpDownTSLAdjust.Name = "numericUpDownTSLAdjust";
            this.numericUpDownTSLAdjust.Size = new System.Drawing.Size(81, 20);
            this.numericUpDownTSLAdjust.TabIndex = 193;
            this.numericUpDownTSLAdjust.ValueChanged += new System.EventHandler(this.numericUpDownTSLAdjust_ValueChanged);
            // 
            // checkBoxPrintGraph
            // 
            this.checkBoxPrintGraph.AutoSize = true;
            this.checkBoxPrintGraph.Enabled = false;
            this.checkBoxPrintGraph.Location = new System.Drawing.Point(10, 666);
            this.checkBoxPrintGraph.Name = "checkBoxPrintGraph";
            this.checkBoxPrintGraph.Size = new System.Drawing.Size(79, 17);
            this.checkBoxPrintGraph.TabIndex = 192;
            this.checkBoxPrintGraph.Text = "Print Graph";
            this.checkBoxPrintGraph.UseVisualStyleBackColor = true;
            // 
            // groupBox15Q
            // 
            this.groupBox15Q.Controls.Add(this.btnLoadBatch);
            this.groupBox15Q.Controls.Add(this.btnAutoAdjust15);
            this.groupBox15Q.Controls.Add(this.btnPrintPreview15Q);
            this.groupBox15Q.Controls.Add(this.btnPrint15QData);
            this.groupBox15Q.Controls.Add(this.BtnLoadCsv);
            this.groupBox15Q.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox15Q.Location = new System.Drawing.Point(3, 14);
            this.groupBox15Q.Name = "groupBox15Q";
            this.groupBox15Q.Size = new System.Drawing.Size(93, 86);
            this.groupBox15Q.TabIndex = 191;
            this.groupBox15Q.TabStop = false;
            this.groupBox15Q.Text = "15 Questions";
            // 
            // btnLoadBatch
            // 
            this.btnLoadBatch.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnLoadBatch.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnLoadBatch.Location = new System.Drawing.Point(46, 58);
            this.btnLoadBatch.Name = "btnLoadBatch";
            this.btnLoadBatch.Size = new System.Drawing.Size(40, 23);
            this.btnLoadBatch.TabIndex = 196;
            this.btnLoadBatch.Text = "Load";
            this.btnLoadBatch.UseVisualStyleBackColor = true;
            this.btnLoadBatch.Click += new System.EventHandler(this.btnLoadBatch_Click);
            // 
            // btnAutoAdjust15
            // 
            this.btnAutoAdjust15.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAutoAdjust15.Location = new System.Drawing.Point(8, 58);
            this.btnAutoAdjust15.Name = "btnAutoAdjust15";
            this.btnAutoAdjust15.Size = new System.Drawing.Size(36, 23);
            this.btnAutoAdjust15.TabIndex = 195;
            this.btnAutoAdjust15.Text = "AA";
            this.btnAutoAdjust15.UseVisualStyleBackColor = true;
            this.btnAutoAdjust15.Click += new System.EventHandler(this.btnAutoAdjust15_Click);
            // 
            // btnPrintPreview15Q
            // 
            this.btnPrintPreview15Q.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreview15Q.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreview15Q.Location = new System.Drawing.Point(7, 36);
            this.btnPrintPreview15Q.Name = "btnPrintPreview15Q";
            this.btnPrintPreview15Q.Size = new System.Drawing.Size(37, 23);
            this.btnPrintPreview15Q.TabIndex = 192;
            this.btnPrintPreview15Q.Text = "PP";
            this.btnPrintPreview15Q.UseVisualStyleBackColor = true;
            this.btnPrintPreview15Q.Click += new System.EventHandler(this.btnPrintPreview15Q_Click);
            // 
            // groupBox24Q
            // 
            this.groupBox24Q.Controls.Add(this.btnAutoAdjust24);
            this.groupBox24Q.Controls.Add(this.btnLoad24Data);
            this.groupBox24Q.Controls.Add(this.btnPrint24QData);
            this.groupBox24Q.Controls.Add(this.btnPrintPreview24Q);
            this.groupBox24Q.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox24Q.Location = new System.Drawing.Point(3, 236);
            this.groupBox24Q.Name = "groupBox24Q";
            this.groupBox24Q.Size = new System.Drawing.Size(93, 88);
            this.groupBox24Q.TabIndex = 190;
            this.groupBox24Q.TabStop = false;
            this.groupBox24Q.Text = "24 Questions";
            // 
            // btnAutoAdjust24
            // 
            this.btnAutoAdjust24.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnAutoAdjust24.Location = new System.Drawing.Point(5, 61);
            this.btnAutoAdjust24.Name = "btnAutoAdjust24";
            this.btnAutoAdjust24.Size = new System.Drawing.Size(79, 23);
            this.btnAutoAdjust24.TabIndex = 194;
            this.btnAutoAdjust24.Text = "Auto Adjust";
            this.btnAutoAdjust24.UseVisualStyleBackColor = true;
            this.btnAutoAdjust24.Click += new System.EventHandler(this.btnAutoAdjust24_Click);
            // 
            // btnPrintPreview24Q
            // 
            this.btnPrintPreview24Q.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnPrintPreview24Q.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPrintPreview24Q.Location = new System.Drawing.Point(6, 37);
            this.btnPrintPreview24Q.Name = "btnPrintPreview24Q";
            this.btnPrintPreview24Q.Size = new System.Drawing.Size(39, 23);
            this.btnPrintPreview24Q.TabIndex = 189;
            this.btnPrintPreview24Q.Text = "PP";
            this.btnPrintPreview24Q.UseVisualStyleBackColor = true;
            this.btnPrintPreview24Q.Click += new System.EventHandler(this.btnPrintPreview24Q_Click);
            // 
            // textBoxSecond4
            // 
            this.textBoxSecond4.BackColor = System.Drawing.Color.Lime;
            this.textBoxSecond4.Location = new System.Drawing.Point(4, 34);
            this.textBoxSecond4.Name = "textBoxSecond4";
            this.textBoxSecond4.ReadOnly = true;
            this.textBoxSecond4.Size = new System.Drawing.Size(100, 20);
            this.textBoxSecond4.TabIndex = 173;
            this.textBoxSecond4.Text = "2nd 4, Not Decep";
            this.textBoxSecond4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxTSLLowKOT);
            this.groupBox2.Controls.Add(this.textBoxTSLLowLabelKOT);
            this.groupBox2.Controls.Add(this.textBoxTSLHiKOT);
            this.groupBox2.Controls.Add(this.textBoxTSLHiLabelKOT);
            this.groupBox2.Controls.Add(this.textBoxRTAverageKOT);
            this.groupBox2.Controls.Add(this.textBoxLabelRTAverageKOT);
            this.groupBox2.Controls.Add(this.textBoxRDAverageKOT);
            this.groupBox2.Controls.Add(this.textBoxLabelRDAverageKOT);
            this.groupBox2.Controls.Add(this.textBoxSTDKOT);
            this.groupBox2.Controls.Add(this.textBoxLabelStd);
            this.groupBox2.Controls.Add(this.textBoxQ16);
            this.groupBox2.Controls.Add(this.textBoxFourth4);
            this.groupBox2.Controls.Add(this.textBoxFirst4);
            this.groupBox2.Controls.Add(this.textBoxSecond4);
            this.groupBox2.Controls.Add(this.textBoxThird4);
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox2.Location = new System.Drawing.Point(855, 511);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(109, 226);
            this.groupBox2.TabIndex = 199;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "KOT DATA";
            // 
            // textBoxTSLLowKOT
            // 
            this.textBoxTSLLowKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTSLLowKOT.Location = new System.Drawing.Point(54, 200);
            this.textBoxTSLLowKOT.Name = "textBoxTSLLowKOT";
            this.textBoxTSLLowKOT.ReadOnly = true;
            this.textBoxTSLLowKOT.Size = new System.Drawing.Size(51, 20);
            this.textBoxTSLLowKOT.TabIndex = 186;
            // 
            // textBoxTSLLowLabelKOT
            // 
            this.textBoxTSLLowLabelKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTSLLowLabelKOT.Location = new System.Drawing.Point(4, 200);
            this.textBoxTSLLowLabelKOT.Name = "textBoxTSLLowLabelKOT";
            this.textBoxTSLLowLabelKOT.ReadOnly = true;
            this.textBoxTSLLowLabelKOT.Size = new System.Drawing.Size(48, 20);
            this.textBoxTSLLowLabelKOT.TabIndex = 185;
            this.textBoxTSLLowLabelKOT.Text = "TSLLo";
            // 
            // textBoxTSLHiKOT
            // 
            this.textBoxTSLHiKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTSLHiKOT.Location = new System.Drawing.Point(54, 180);
            this.textBoxTSLHiKOT.Name = "textBoxTSLHiKOT";
            this.textBoxTSLHiKOT.ReadOnly = true;
            this.textBoxTSLHiKOT.Size = new System.Drawing.Size(51, 20);
            this.textBoxTSLHiKOT.TabIndex = 184;
            // 
            // textBoxTSLHiLabelKOT
            // 
            this.textBoxTSLHiLabelKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxTSLHiLabelKOT.Location = new System.Drawing.Point(4, 180);
            this.textBoxTSLHiLabelKOT.Name = "textBoxTSLHiLabelKOT";
            this.textBoxTSLHiLabelKOT.ReadOnly = true;
            this.textBoxTSLHiLabelKOT.Size = new System.Drawing.Size(48, 20);
            this.textBoxTSLHiLabelKOT.TabIndex = 183;
            this.textBoxTSLHiLabelKOT.Text = "TSLHi";
            // 
            // textBoxRTAverageKOT
            // 
            this.textBoxRTAverageKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRTAverageKOT.Location = new System.Drawing.Point(54, 160);
            this.textBoxRTAverageKOT.Name = "textBoxRTAverageKOT";
            this.textBoxRTAverageKOT.ReadOnly = true;
            this.textBoxRTAverageKOT.Size = new System.Drawing.Size(51, 20);
            this.textBoxRTAverageKOT.TabIndex = 182;
            // 
            // textBoxLabelRTAverageKOT
            // 
            this.textBoxLabelRTAverageKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxLabelRTAverageKOT.Location = new System.Drawing.Point(4, 160);
            this.textBoxLabelRTAverageKOT.Name = "textBoxLabelRTAverageKOT";
            this.textBoxLabelRTAverageKOT.ReadOnly = true;
            this.textBoxLabelRTAverageKOT.Size = new System.Drawing.Size(48, 20);
            this.textBoxLabelRTAverageKOT.TabIndex = 181;
            this.textBoxLabelRTAverageKOT.Text = "R/T Avg";
            // 
            // textBoxRDAverageKOT
            // 
            this.textBoxRDAverageKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRDAverageKOT.Location = new System.Drawing.Point(54, 140);
            this.textBoxRDAverageKOT.Name = "textBoxRDAverageKOT";
            this.textBoxRDAverageKOT.ReadOnly = true;
            this.textBoxRDAverageKOT.Size = new System.Drawing.Size(51, 20);
            this.textBoxRDAverageKOT.TabIndex = 180;
            // 
            // textBoxLabelRDAverageKOT
            // 
            this.textBoxLabelRDAverageKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxLabelRDAverageKOT.Location = new System.Drawing.Point(4, 140);
            this.textBoxLabelRDAverageKOT.Name = "textBoxLabelRDAverageKOT";
            this.textBoxLabelRDAverageKOT.ReadOnly = true;
            this.textBoxLabelRDAverageKOT.Size = new System.Drawing.Size(48, 20);
            this.textBoxLabelRDAverageKOT.TabIndex = 179;
            this.textBoxLabelRDAverageKOT.Text = "R/D Avg";
            // 
            // textBoxSTDKOT
            // 
            this.textBoxSTDKOT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxSTDKOT.Location = new System.Drawing.Point(54, 120);
            this.textBoxSTDKOT.Name = "textBoxSTDKOT";
            this.textBoxSTDKOT.ReadOnly = true;
            this.textBoxSTDKOT.Size = new System.Drawing.Size(51, 20);
            this.textBoxSTDKOT.TabIndex = 178;
            // 
            // textBoxLabelStd
            // 
            this.textBoxLabelStd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxLabelStd.Location = new System.Drawing.Point(4, 120);
            this.textBoxLabelStd.Name = "textBoxLabelStd";
            this.textBoxLabelStd.ReadOnly = true;
            this.textBoxLabelStd.Size = new System.Drawing.Size(48, 20);
            this.textBoxLabelStd.TabIndex = 177;
            this.textBoxLabelStd.Text = "STD";
            // 
            // textBoxQ16
            // 
            this.textBoxQ16.BackColor = System.Drawing.Color.Lime;
            this.textBoxQ16.Location = new System.Drawing.Point(4, 94);
            this.textBoxQ16.Name = "textBoxQ16";
            this.textBoxQ16.ReadOnly = true;
            this.textBoxQ16.Size = new System.Drawing.Size(100, 20);
            this.textBoxQ16.TabIndex = 176;
            this.textBoxQ16.Text = "Q16, No Reaction";
            this.textBoxQ16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxFourth4
            // 
            this.textBoxFourth4.BackColor = System.Drawing.Color.Lime;
            this.textBoxFourth4.Location = new System.Drawing.Point(4, 74);
            this.textBoxFourth4.Name = "textBoxFourth4";
            this.textBoxFourth4.ReadOnly = true;
            this.textBoxFourth4.Size = new System.Drawing.Size(100, 20);
            this.textBoxFourth4.TabIndex = 175;
            this.textBoxFourth4.Text = "4th 4, Not Decep";
            this.textBoxFourth4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxFirst4
            // 
            this.textBoxFirst4.BackColor = System.Drawing.Color.Lime;
            this.textBoxFirst4.Location = new System.Drawing.Point(4, 14);
            this.textBoxFirst4.Name = "textBoxFirst4";
            this.textBoxFirst4.ReadOnly = true;
            this.textBoxFirst4.Size = new System.Drawing.Size(100, 20);
            this.textBoxFirst4.TabIndex = 172;
            this.textBoxFirst4.Text = "1st 4, Not Decep";
            this.textBoxFirst4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxThird4
            // 
            this.textBoxThird4.BackColor = System.Drawing.Color.Lime;
            this.textBoxThird4.Location = new System.Drawing.Point(4, 54);
            this.textBoxThird4.Name = "textBoxThird4";
            this.textBoxThird4.ReadOnly = true;
            this.textBoxThird4.Size = new System.Drawing.Size(100, 20);
            this.textBoxThird4.TabIndex = 174;
            this.textBoxThird4.Text = "3rd 4, Not Decep";
            this.textBoxThird4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD1);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD2);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD6);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD7);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD3);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD5);
            this.groupBox4.Controls.Add(this.textBoxTSLHiLowSTD4);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox4.Location = new System.Drawing.Point(740, 576);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(109, 156);
            this.groupBox4.TabIndex = 200;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "TSL Hi, Low, STD";
            // 
            // textBoxTSLHiLowSTD1
            // 
            this.textBoxTSLHiLowSTD1.BackColor = System.Drawing.Color.LightGreen;
            this.textBoxTSLHiLowSTD1.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD1.Location = new System.Drawing.Point(5, 14);
            this.textBoxTSLHiLowSTD1.Name = "textBoxTSLHiLowSTD1";
            this.textBoxTSLHiLowSTD1.ReadOnly = true;
            this.textBoxTSLHiLowSTD1.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD1.TabIndex = 178;
            // 
            // textBoxTSLHiLowSTD2
            // 
            this.textBoxTSLHiLowSTD2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.textBoxTSLHiLowSTD2.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD2.Location = new System.Drawing.Point(5, 34);
            this.textBoxTSLHiLowSTD2.Name = "textBoxTSLHiLowSTD2";
            this.textBoxTSLHiLowSTD2.ReadOnly = true;
            this.textBoxTSLHiLowSTD2.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD2.TabIndex = 179;
            // 
            // textBoxTSLHiLowSTD6
            // 
            this.textBoxTSLHiLowSTD6.BackColor = System.Drawing.Color.Yellow;
            this.textBoxTSLHiLowSTD6.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD6.Location = new System.Drawing.Point(5, 111);
            this.textBoxTSLHiLowSTD6.Name = "textBoxTSLHiLowSTD6";
            this.textBoxTSLHiLowSTD6.ReadOnly = true;
            this.textBoxTSLHiLowSTD6.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD6.TabIndex = 183;
            // 
            // textBoxTSLHiLowSTD7
            // 
            this.textBoxTSLHiLowSTD7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBoxTSLHiLowSTD7.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD7.Location = new System.Drawing.Point(5, 131);
            this.textBoxTSLHiLowSTD7.Name = "textBoxTSLHiLowSTD7";
            this.textBoxTSLHiLowSTD7.ReadOnly = true;
            this.textBoxTSLHiLowSTD7.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD7.TabIndex = 184;
            // 
            // textBoxTSLHiLowSTD3
            // 
            this.textBoxTSLHiLowSTD3.BackColor = System.Drawing.Color.Pink;
            this.textBoxTSLHiLowSTD3.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD3.Location = new System.Drawing.Point(5, 54);
            this.textBoxTSLHiLowSTD3.Name = "textBoxTSLHiLowSTD3";
            this.textBoxTSLHiLowSTD3.ReadOnly = true;
            this.textBoxTSLHiLowSTD3.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD3.TabIndex = 180;
            // 
            // textBoxTSLHiLowSTD5
            // 
            this.textBoxTSLHiLowSTD5.BackColor = System.Drawing.Color.Fuchsia;
            this.textBoxTSLHiLowSTD5.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD5.Location = new System.Drawing.Point(5, 92);
            this.textBoxTSLHiLowSTD5.Name = "textBoxTSLHiLowSTD5";
            this.textBoxTSLHiLowSTD5.ReadOnly = true;
            this.textBoxTSLHiLowSTD5.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD5.TabIndex = 182;
            // 
            // textBoxTSLHiLowSTD4
            // 
            this.textBoxTSLHiLowSTD4.BackColor = System.Drawing.Color.PeachPuff;
            this.textBoxTSLHiLowSTD4.Cursor = System.Windows.Forms.Cursors.No;
            this.textBoxTSLHiLowSTD4.Location = new System.Drawing.Point(5, 73);
            this.textBoxTSLHiLowSTD4.Name = "textBoxTSLHiLowSTD4";
            this.textBoxTSLHiLowSTD4.ReadOnly = true;
            this.textBoxTSLHiLowSTD4.Size = new System.Drawing.Size(100, 20);
            this.textBoxTSLHiLowSTD4.TabIndex = 181;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxRT4);
            this.groupBox5.Controls.Add(this.textBoxRT1);
            this.groupBox5.Controls.Add(this.textBoxRT2);
            this.groupBox5.Controls.Add(this.textBoxRT3);
            this.groupBox5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox5.Location = new System.Drawing.Point(855, 331);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(109, 94);
            this.groupBox5.TabIndex = 201;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Read Time Avg";
            // 
            // textBoxRT4
            // 
            this.textBoxRT4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRT4.Location = new System.Drawing.Point(4, 72);
            this.textBoxRT4.Name = "textBoxRT4";
            this.textBoxRT4.ReadOnly = true;
            this.textBoxRT4.Size = new System.Drawing.Size(100, 20);
            this.textBoxRT4.TabIndex = 202;
            this.textBoxRT4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxRT1
            // 
            this.textBoxRT1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRT1.Location = new System.Drawing.Point(4, 12);
            this.textBoxRT1.Name = "textBoxRT1";
            this.textBoxRT1.ReadOnly = true;
            this.textBoxRT1.Size = new System.Drawing.Size(100, 20);
            this.textBoxRT1.TabIndex = 172;
            this.textBoxRT1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxRT2
            // 
            this.textBoxRT2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRT2.Location = new System.Drawing.Point(4, 32);
            this.textBoxRT2.Name = "textBoxRT2";
            this.textBoxRT2.ReadOnly = true;
            this.textBoxRT2.Size = new System.Drawing.Size(100, 20);
            this.textBoxRT2.TabIndex = 173;
            this.textBoxRT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxRT3
            // 
            this.textBoxRT3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBoxRT3.Location = new System.Drawing.Point(4, 52);
            this.textBoxRT3.Name = "textBoxRT3";
            this.textBoxRT3.ReadOnly = true;
            this.textBoxRT3.Size = new System.Drawing.Size(100, 20);
            this.textBoxRT3.TabIndex = 174;
            this.textBoxRT3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // VeracityViewerTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(981, 739);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBoxControls);
            this.Controls.Add(this.groupBoxDistort);
            this.Controls.Add(this.groupBoxAverages);
            this.Controls.Add(this.groupBoxReadRatio);
            this.Controls.Add(this.groupBoxAdopt);
            this.Controls.Add(this.groupBoxIllegalDrugs);
            this.Controls.Add(this.groupBoxAccidents);
            this.Controls.Add(this.groupBoxArrest);
            this.Controls.Add(this.groupBoxSmuggling);
            this.Controls.Add(this.groupBoxSexOffenders);
            this.Controls.Add(this.groupBoxTerrorism);
            this.Controls.Add(this.groupBoxUnknowns);
            this.Controls.Add(this.groupBoxExit);
            this.Controls.Add(this.groupBoxZones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VeracityViewerTestForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "VeracityViewerTester";
            this.Load += new System.EventHandler(this.VeracityViewerTestForm_Load);
            this.groupBoxZones.ResumeLayout(false);
            this.groupBoxZones.PerformLayout();
            this.groupBoxExit.ResumeLayout(false);
            this.groupBoxExit.PerformLayout();
            this.groupBoxUnknowns.ResumeLayout(false);
            this.groupBoxUnknowns.PerformLayout();
            this.groupBoxTerrorism.ResumeLayout(false);
            this.groupBoxTerrorism.PerformLayout();
            this.groupBoxSexOffenders.ResumeLayout(false);
            this.groupBoxSexOffenders.PerformLayout();
            this.groupBoxSmuggling.ResumeLayout(false);
            this.groupBoxSmuggling.PerformLayout();
            this.groupBoxArrest.ResumeLayout(false);
            this.groupBoxArrest.PerformLayout();
            this.groupBoxAccidents.ResumeLayout(false);
            this.groupBoxAccidents.PerformLayout();
            this.groupBoxIllegalDrugs.ResumeLayout(false);
            this.groupBoxIllegalDrugs.PerformLayout();
            this.groupBoxAdopt.ResumeLayout(false);
            this.groupBoxAdopt.PerformLayout();
            this.groupBoxReadRatio.ResumeLayout(false);
            this.groupBoxReadRatio.PerformLayout();
            this.groupBoxDistort.ResumeLayout(false);
            this.groupBoxDistort.PerformLayout();
            this.groupBoxAverages.ResumeLayout(false);
            this.groupBoxAverages.PerformLayout();
            this.groupBoxControls.ResumeLayout(false);
            this.groupBoxControls.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBoxTSLAdjust.ResumeLayout(false);
            this.groupBoxTSLAdjust.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNumberOfHits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTSLLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTSLAdjust)).EndInit();
            this.groupBox15Q.ResumeLayout(false);
            this.groupBox24Q.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnLoadCsv;
        private System.Windows.Forms.TextBox textBoxIssues1;
        private System.Windows.Forms.TextBox textBoxQuestion1;
        private System.Windows.Forms.TextBox textBoxAnswers1;
        private System.Windows.Forms.TextBox textBoxReactionData1;
        private System.Windows.Forms.TextBox textBoxReadTime1;
        private System.Windows.Forms.TextBox textBoxReaction1;
        private System.Windows.Forms.TextBox textBoxReaction;
        private System.Windows.Forms.TextBox textBoxReadTime;
        private System.Windows.Forms.TextBox textBoxReactionData;
        private System.Windows.Forms.TextBox textBoxAnswers;
        private System.Windows.Forms.TextBox textBoxQuestion;
        private System.Windows.Forms.TextBox textBoxIssues;
        private System.Windows.Forms.TextBox textBoxReaction2;
        private System.Windows.Forms.TextBox textBoxReadTime2;
        private System.Windows.Forms.TextBox textBoxReactionData2;
        private System.Windows.Forms.TextBox textBoxAnswers2;
        private System.Windows.Forms.TextBox textBoxQuestion2;
        private System.Windows.Forms.TextBox textBoxIssues2;
        private System.Windows.Forms.TextBox textBoxReaction3;
        private System.Windows.Forms.TextBox textBoxReadTime3;
        private System.Windows.Forms.TextBox textBoxReactionData3;
        private System.Windows.Forms.TextBox textBoxAnswers3;
        private System.Windows.Forms.TextBox textBoxQuestion3;
        private System.Windows.Forms.TextBox textBoxIssues3;
        private System.Windows.Forms.TextBox textBoxReaction4;
        private System.Windows.Forms.TextBox textBoxReadTime4;
        private System.Windows.Forms.TextBox textBoxReactionData4;
        private System.Windows.Forms.TextBox textBoxAnswers4;
        private System.Windows.Forms.TextBox textBoxQuestion4;
        private System.Windows.Forms.TextBox textBoxIssues4;
        private System.Windows.Forms.TextBox textBoxReaction5;
        private System.Windows.Forms.TextBox textBoxReadTime5;
        private System.Windows.Forms.TextBox textBoxReactionData5;
        private System.Windows.Forms.TextBox textBoxAnswers5;
        private System.Windows.Forms.TextBox textBoxQuestion5;
        private System.Windows.Forms.TextBox textBoxIssues5;
        private System.Windows.Forms.TextBox textBoxReaction6;
        private System.Windows.Forms.TextBox textBoxReadTime6;
        private System.Windows.Forms.TextBox textBoxReactionData6;
        private System.Windows.Forms.TextBox textBoxAnswers6;
        private System.Windows.Forms.TextBox textBoxQuestion6;
        private System.Windows.Forms.TextBox textBoxIssues6;
        private System.Windows.Forms.TextBox textBoxReaction7;
        private System.Windows.Forms.TextBox textBoxReadTime7;
        private System.Windows.Forms.TextBox textBoxReactionData7;
        private System.Windows.Forms.TextBox textBoxAnswers7;
        private System.Windows.Forms.TextBox textBoxQuestion7;
        private System.Windows.Forms.TextBox textBoxIssues7;
        private System.Windows.Forms.TextBox textBoxReaction8;
        private System.Windows.Forms.TextBox textBoxReadTime8;
        private System.Windows.Forms.TextBox textBoxReactionData8;
        private System.Windows.Forms.TextBox textBoxAnswers8;
        private System.Windows.Forms.TextBox textBoxQuestion8;
        private System.Windows.Forms.TextBox textBoxIssues8;
        private System.Windows.Forms.TextBox textBoxReaction9;
        private System.Windows.Forms.TextBox textBoxReadTime9;
        private System.Windows.Forms.TextBox textBoxReactionData9;
        private System.Windows.Forms.TextBox textBoxAnswers9;
        private System.Windows.Forms.TextBox textBoxQuestion9;
        private System.Windows.Forms.TextBox textBoxIssues9;
        private System.Windows.Forms.TextBox textBoxReaction10;
        private System.Windows.Forms.TextBox textBoxReadTime10;
        private System.Windows.Forms.TextBox textBoxReactionData10;
        private System.Windows.Forms.TextBox textBoxAnswers10;
        private System.Windows.Forms.TextBox textBoxQuestion10;
        private System.Windows.Forms.TextBox textBoxIssues10;
        private System.Windows.Forms.TextBox textBoxReaction11;
        private System.Windows.Forms.TextBox textBoxReadTime11;
        private System.Windows.Forms.TextBox textBoxReactionData11;
        private System.Windows.Forms.TextBox textBoxAnswers11;
        private System.Windows.Forms.TextBox textBoxQuestion11;
        private System.Windows.Forms.TextBox textBoxIssues11;
        private System.Windows.Forms.TextBox textBoxReaction12;
        private System.Windows.Forms.TextBox textBoxReadTime12;
        private System.Windows.Forms.TextBox textBoxReactionData12;
        private System.Windows.Forms.TextBox textBoxAnswers12;
        private System.Windows.Forms.TextBox textBoxQuestion12;
        private System.Windows.Forms.TextBox textBoxIssues12;
        private System.Windows.Forms.TextBox textBoxReaction13;
        private System.Windows.Forms.TextBox textBoxReadTime13;
        private System.Windows.Forms.TextBox textBoxReactionData13;
        private System.Windows.Forms.TextBox textBoxAnswers13;
        private System.Windows.Forms.TextBox textBoxQuestion13;
        private System.Windows.Forms.TextBox textBoxIssues13;
        private System.Windows.Forms.TextBox textBoxReaction14;
        private System.Windows.Forms.TextBox textBoxReadTime14;
        private System.Windows.Forms.TextBox textBoxReactionData14;
        private System.Windows.Forms.TextBox textBoxAnswers14;
        private System.Windows.Forms.TextBox textBoxQuestion14;
        private System.Windows.Forms.TextBox textBoxIssues14;
        private System.Windows.Forms.TextBox textBoxReaction15;
        private System.Windows.Forms.TextBox textBoxReadTime15;
        private System.Windows.Forms.TextBox textBoxReactionData15;
        private System.Windows.Forms.TextBox textBoxAnswers15;
        private System.Windows.Forms.TextBox textBoxQuestion15;
        private System.Windows.Forms.TextBox textBoxIssues15;
        private System.Windows.Forms.TextBox txtTSLHi;
        private System.Windows.Forms.TextBox txtTSLLo;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg1;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg2;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg3;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg4;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg5;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg13;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg14;
        private System.Windows.Forms.TextBox textBoxStandardDeviation;
        private System.Windows.Forms.TextBox textBoxAvgTest;
        private System.Windows.Forms.TextBox textBoxAvg1;
        private System.Windows.Forms.TextBox textBoxAvg2;
        private System.Windows.Forms.TextBox textBoxAvg3;
        private System.Windows.Forms.TextBox textBoxAvg4;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button btnLoad24Data;
        private System.Windows.Forms.TextBox textBoxReaction16;
        private System.Windows.Forms.TextBox textBoxReadTime16;
        private System.Windows.Forms.TextBox textBoxReactionData16;
        private System.Windows.Forms.TextBox textBoxAnswers16;
        private System.Windows.Forms.TextBox textBoxQuestion16;
        private System.Windows.Forms.TextBox textBoxIssues16;
        private System.Windows.Forms.TextBox textBoxReaction17;
        private System.Windows.Forms.TextBox textBoxReadTime17;
        private System.Windows.Forms.TextBox textBoxReactionData17;
        private System.Windows.Forms.TextBox textBoxAnswers17;
        private System.Windows.Forms.TextBox textBoxQuestion17;
        private System.Windows.Forms.TextBox textBoxIssues17;
        private System.Windows.Forms.TextBox textBoxReaction19;
        private System.Windows.Forms.TextBox textBoxReadTime19;
        private System.Windows.Forms.TextBox textBoxReactionData19;
        private System.Windows.Forms.TextBox textBoxAnswers19;
        private System.Windows.Forms.TextBox textBoxQuestion19;
        private System.Windows.Forms.TextBox textBoxIssues19;
        private System.Windows.Forms.TextBox textBoxReaction18;
        private System.Windows.Forms.TextBox textBoxReadTime18;
        private System.Windows.Forms.TextBox textBoxReactionData18;
        private System.Windows.Forms.TextBox textBoxAnswers18;
        private System.Windows.Forms.TextBox textBoxQuestion18;
        private System.Windows.Forms.TextBox textBoxIssues18;
        private System.Windows.Forms.TextBox textBoxReaction23;
        private System.Windows.Forms.TextBox textBoxReadTime23;
        private System.Windows.Forms.TextBox textBoxReactionData23;
        private System.Windows.Forms.TextBox textBoxAnswers23;
        private System.Windows.Forms.TextBox textBoxQuestion23;
        private System.Windows.Forms.TextBox textBoxIssues23;
        private System.Windows.Forms.TextBox textBoxReaction22;
        private System.Windows.Forms.TextBox textBoxReadTime22;
        private System.Windows.Forms.TextBox textBoxReactionData22;
        private System.Windows.Forms.TextBox textBoxAnswers22;
        private System.Windows.Forms.TextBox textBoxQuestion22;
        private System.Windows.Forms.TextBox textBoxIssues22;
        private System.Windows.Forms.TextBox textBoxReaction21;
        private System.Windows.Forms.TextBox textBoxReadTime21;
        private System.Windows.Forms.TextBox textBoxReactionData21;
        private System.Windows.Forms.TextBox textBoxAnswers21;
        private System.Windows.Forms.TextBox textBoxQuestion21;
        private System.Windows.Forms.TextBox textBoxIssues21;
        private System.Windows.Forms.TextBox textBoxReaction20;
        private System.Windows.Forms.TextBox textBoxReadTime20;
        private System.Windows.Forms.TextBox textBoxReactionData20;
        private System.Windows.Forms.TextBox textBoxAnswers20;
        private System.Windows.Forms.TextBox textBoxQuestion20;
        private System.Windows.Forms.TextBox textBoxIssues20;
        private System.Windows.Forms.TextBox textBoxReaction24;
        private System.Windows.Forms.TextBox textBoxReadTime24;
        private System.Windows.Forms.TextBox textBoxReactionData24;
        private System.Windows.Forms.TextBox textBoxAnswers24;
        private System.Windows.Forms.TextBox textBoxQuestion24;
        private System.Windows.Forms.TextBox textBoxIssues24;
        private System.Windows.Forms.TextBox textBoxReadRatio1;
        private System.Windows.Forms.TextBox textBoxReadRatio2;
        private System.Windows.Forms.TextBox textBoxReadRatio3;
        private System.Windows.Forms.TextBox textBoxDistortion1;
        private System.Windows.Forms.TextBox textBoxZone1;
        private System.Windows.Forms.TextBox textBoxZone2;
        private System.Windows.Forms.TextBox textBoxZone3;
        private System.Windows.Forms.TextBox textBoxZone4;
        private System.Windows.Forms.TextBox textBoxZone5;
        private System.Windows.Forms.TextBox textBoxZone6;
        private System.Windows.Forms.TextBox textBoxZone7;
        private System.Windows.Forms.TextBox textBoxTSLHi;
        private System.Windows.Forms.TextBox textBoxTSLLo;
        private System.Windows.Forms.Button btnPrint24QData;
        private System.Windows.Forms.Button btnPrint15QData;
        private System.Windows.Forms.GroupBox groupBoxZones;
        private System.Windows.Forms.GroupBox groupBoxExit;
        private System.Windows.Forms.GroupBox groupBoxUnknowns;
        private System.Windows.Forms.GroupBox groupBoxTerrorism;
        private System.Windows.Forms.GroupBox groupBoxSexOffenders;
        private System.Windows.Forms.GroupBox groupBoxSmuggling;
        private System.Windows.Forms.GroupBox groupBoxArrest;
        private System.Windows.Forms.GroupBox groupBoxAccidents;
        private System.Windows.Forms.GroupBox groupBoxIllegalDrugs;
        private System.Windows.Forms.GroupBox groupBoxAdopt;
        private System.Windows.Forms.GroupBox groupBoxReadRatio;
        private System.Windows.Forms.GroupBox groupBoxDistort;
        private System.Windows.Forms.GroupBox groupBoxAverages;
        private System.Windows.Forms.GroupBox groupBoxControls;
        private System.Windows.Forms.Button btnPrintPreview24Q;
        private System.Windows.Forms.GroupBox groupBox15Q;
        private System.Windows.Forms.Button btnPrintPreview15Q;
        private System.Windows.Forms.GroupBox groupBox24Q;
        private System.Windows.Forms.GroupBox groupBoxTSLAdjust;
        private System.Windows.Forms.Button btnAutoAdjust24;
        private System.Windows.Forms.NumericUpDown numericUpDownTSLAdjust;
        private System.Windows.Forms.CheckBox checkBoxPrintGraph;
        private System.Windows.Forms.Button btnAutoAdjust15;
        private System.Windows.Forms.TextBox txtTSLLowPercentage;
        private System.Windows.Forms.NumericUpDown numericTSLLow;
        private System.Windows.Forms.TextBox txtNumHits;
        private System.Windows.Forms.NumericUpDown numericNumberOfHits;
        private System.Windows.Forms.Button btnLoadBatch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnLoadKOTData;
        private System.Windows.Forms.Button btnPrintKOT;
        private System.Windows.Forms.Button btnPrintPreviewKOT;
        private System.Windows.Forms.TextBox textBoxSecond4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxQ16;
        private System.Windows.Forms.TextBox textBoxFourth4;
        private System.Windows.Forms.TextBox textBoxFirst4;
        private System.Windows.Forms.TextBox textBoxThird4;
        private System.Windows.Forms.TextBox textBoxRDAverageKOT;
        private System.Windows.Forms.TextBox textBoxLabelRDAverageKOT;
        private System.Windows.Forms.TextBox textBoxSTDKOT;
        private System.Windows.Forms.TextBox textBoxLabelStd;
        private System.Windows.Forms.TextBox textBoxRTAverageKOT;
        private System.Windows.Forms.TextBox textBoxLabelRTAverageKOT;
        private System.Windows.Forms.TextBox textBoxTSLLowKOT;
        private System.Windows.Forms.TextBox textBoxTSLLowLabelKOT;
        private System.Windows.Forms.TextBox textBoxTSLHiKOT;
        private System.Windows.Forms.TextBox textBoxTSLHiLabelKOT;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg8;
        private System.Windows.Forms.TextBox textBoxAvg7;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg7;
        private System.Windows.Forms.TextBox textBoxAvg6;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg6;
        private System.Windows.Forms.TextBox textBoxAvg5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAutoAdjust26;
        private System.Windows.Forms.Button btnLoad26Data;
        private System.Windows.Forms.Button btnPrint26QData;
        private System.Windows.Forms.Button btnPrintPreview26Q;
        private System.Windows.Forms.TextBox textBoxAnswers26;
        private System.Windows.Forms.TextBox textBoxIssues26;
        private System.Windows.Forms.TextBox textBoxQuestion26;
        private System.Windows.Forms.TextBox textBoxReactionData26;
        private System.Windows.Forms.TextBox textBoxReadTime26;
        private System.Windows.Forms.TextBox textBoxReaction26;
        private System.Windows.Forms.TextBox textBoxAnswers25;
        private System.Windows.Forms.TextBox textBoxIssues25;
        private System.Windows.Forms.TextBox textBoxQuestion25;
        private System.Windows.Forms.TextBox textBoxReactionData25;
        private System.Windows.Forms.TextBox textBoxReadTime25;
        private System.Windows.Forms.TextBox textBoxReaction25;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg12;
        private System.Windows.Forms.TextBox textBoxAvg11;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg11;
        private System.Windows.Forms.TextBox textBoxAvg10;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg10;
        private System.Windows.Forms.TextBox textBoxAvg9;
        private System.Windows.Forms.TextBox textBoxReactionDataAvg9;
        private System.Windows.Forms.TextBox textBoxAvg8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD1;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD2;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD6;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD7;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD3;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD5;
        private System.Windows.Forms.TextBox textBoxTSLHiLowSTD4;
        private System.Windows.Forms.Button btnPrintPreview26Q_2;
        private System.Windows.Forms.Button btnPrint26QData_2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBoxRT1;
        private System.Windows.Forms.TextBox textBoxRT2;
        private System.Windows.Forms.TextBox textBoxRT3;
        private System.Windows.Forms.TextBox textBoxRT4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnAutoAdjust18;
        private System.Windows.Forms.Button btnLoad18Data;
        private System.Windows.Forms.Button btnPrint18QData;
        private System.Windows.Forms.Button btnPrintPreview18Q;
        private System.Windows.Forms.CheckBox checkBoxDistortLogic;
        private System.Windows.Forms.CheckBox checkBoxSpr;
    }
}

