﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Veracity.Client.SoftwareLocker;

namespace VeracityViewerTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.Run(new VeracityViewerTestForm()); // NOTE!  COMMENT THIS OUT IF USING TrailMaker Code below

        //    string securityFileDirPath = "C:\\Temp";
        //    if (!System.IO.Directory.Exists(securityFileDirPath))
        //    {
        //        System.IO.Directory.CreateDirectory(securityFileDirPath);
        //    }

        //    TrialMaker t = new TrialMaker("VERACITY", securityFileDirPath + System.IO.Path.DirectorySeparatorChar + "VERACITY.reg", 
        //        securityFileDirPath + System.IO.Path.DirectorySeparatorChar + "VERACITY.tra", "Email: psprague@veracitysecurity.com", 180, 500, "518");

        //    byte[] MyOwnKey = { 97, 250, 1, 5, 84, 21, 7, 63, 4, 54, 87, 56, 123, 10, 3, 62, 7, 9, 20, 36, 37, 21, 101, 57 };

        //    t.TripleDESKey = MyOwnKey;

        //    // if you don't call this part the program will
        //    //use default key to encryption
        //    TrialMaker.RunTypes RT = t.ShowDialog();
        //    bool is_trial = false;

        //    if (RT != TrialMaker.RunTypes.Expired)
        //    {
        //        if (RT == TrialMaker.RunTypes.Full)
        //            is_trial = false;
        //        else
        //            is_trial = true;
        //        Application.Run(new VeracityViewerTestForm());
        //    }
        }
    }
}
              
