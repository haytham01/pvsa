using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace KeyboardTextAlpha
{
    public partial class Keyboard : UserControl
    {
        private String _strKeypPressed = "";
        private BoW pvtKeyboardType = BoW.Standard;
        private bool _bShowSpaceButton = true;
        private bool _bSwitchSpaceToEnterKey = false;
        private String _strSpaceKey = " ";

        public bool SwitchSpaceToEnterKey
        {
            get
            {
                return _bSwitchSpaceToEnterKey;
            }
            set
            {
                _bSwitchSpaceToEnterKey = value;
                //if (value)
                //{
                //    _strSpaceKey = "{TAB}";
                //    this.spaceButton.Text = "Next Entry";
                //}
            }
        }
        public bool ShowSpaceButton
        {
            get
            {
                return _bShowSpaceButton;
            }
            set
            {
                _bShowSpaceButton = value;
                this.spaceButton.Visible = value;
            }
        }
        public String KeyPressed
        {
            get { return _strKeypPressed; }
        }

        public BoW KeyboardType
        {
            get
            {
                return pvtKeyboardType;
            }
            set
            {
                pvtKeyboardType = value;
                if (pvtKeyboardType == BoW.Alphabetical)
                {
                    _setKeyboardTextToAlpha();
                }
                else
                {
                    _setKeyboardTextToStandard();
                }
            }
        }

        private void _setKeyboardTextToStandard()
        {
            this.qButton.Text = "Q";
            this.wButton.Text = "W";
            this.eButton.Text = "E";
            this.rButton.Text = "R";
            this.tButton.Text = "T";
            this.yButton.Text = "Y";
            this.uButton.Text = "U";
            this.iButton.Text = "I";
            this.oButton.Text = "O";
            this.pButton.Text = "P";
            this.aButton.Text = "A";
            this.sButton.Text = "S";
            this.dButton.Text = "D";
            this.fButton.Text = "F";
            this.gButton.Text = "G";
            this.hButton.Text = "H";
            this.jButton.Text = "J";
            this.kButton.Text = "K";
            this.lButton.Text = "L";
            this.zButton.Text = "Z";
            this.xButton.Text = "X";
            this.cButton.Text = "C";
            this.vButton.Text = "V";
            this.bButton.Text = "B";
            this.nButton.Text = "N";
            this.mButton.Text = "M";
        }

        private void _setKeyboardTextToAlpha()
        {
            this.qButton.Text = "A";
            this.wButton.Text = "B";
            this.eButton.Text = "C";
            this.rButton.Text = "D";
            this.tButton.Text = "E";
            this.yButton.Text = "F";
            this.uButton.Text = "G";
            this.iButton.Text = "H";
            this.oButton.Text = "I";
            this.pButton.Text = "J";
            this.aButton.Text = "K";
            this.sButton.Text = "L";
            this.dButton.Text = "M";
            this.fButton.Text = "N";
            this.gButton.Text = "O";
            this.hButton.Text = "P";
            this.jButton.Text = "Q";
            this.kButton.Text = "R";
            this.lButton.Text = "S";
            this.zButton.Text = "T";
            this.xButton.Text = "U";
            this.cButton.Text = "V";
            this.vButton.Text = "W";
            this.bButton.Text = "X";
            this.nButton.Text = "Y";
            this.mButton.Text = "Z";
        }

        public Keyboard()
        {
            InitializeComponent();
        }

        public delegate void KeyboardDelegate(object sender, KeyboardEventArgs e);

        [Category("Mouse"), Description("Return value of mouseclicked key")]
        public event KeyboardDelegate UserKeyPressed;
        protected virtual void OnUserKeyPressed(KeyboardEventArgs e)
        {
            if (UserKeyPressed != null)
                UserKeyPressed(this, e);
        }

        private void qButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("Q");
            OnUserKeyPressed(dea);

        }

        private void wButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("W");
            OnUserKeyPressed(dea);
        }

        private void eButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("E");
            OnUserKeyPressed(dea);

        }

        private void rButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("R");
            OnUserKeyPressed(dea);

        }

        private void tButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("T");
            OnUserKeyPressed(dea);

        }

        private void yButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("Y");
            OnUserKeyPressed(dea);

        }

        private void uButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("U");
            OnUserKeyPressed(dea);

        }

        private void iButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("I");
            OnUserKeyPressed(dea);

        }

        private void oButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("O");
            OnUserKeyPressed(dea);

        }

        private void pButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("P");
            OnUserKeyPressed(dea);

        }

        private void aButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("A");
            OnUserKeyPressed(dea);
        }

        private void sButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("S");
            OnUserKeyPressed(dea);

        }

        private void dButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("D");
            OnUserKeyPressed(dea);

        }

        private void fButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("F");
            OnUserKeyPressed(dea);

        }

        private void gButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("G");
            OnUserKeyPressed(dea);

        }

        private void hButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("H");
            OnUserKeyPressed(dea);

        }

        private void jButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("J");
            OnUserKeyPressed(dea);

        }

        private void kButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("K");
            OnUserKeyPressed(dea);

        }

        private void lButton_Click(object sender, EventArgs e)
        {
           KeyboardEventArgs dea = new KeyboardEventArgs("L");
            OnUserKeyPressed(dea);

        }

        private void zButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("Z");
            OnUserKeyPressed(dea);

        }

        private void xButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("X");
            OnUserKeyPressed(dea);

        }

        private void cButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("C");
            OnUserKeyPressed(dea);

        }

        private void vButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("V");
            OnUserKeyPressed(dea);

        }

        private void bButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("B");
            OnUserKeyPressed(dea);

        }

        private void nButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("N");
            OnUserKeyPressed(dea);

        }

        private void mButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs("M");
            OnUserKeyPressed(dea);

        }

        private void spaceButton_Click(object sender, EventArgs e)
        {
            KeyboardEventArgs dea = new KeyboardEventArgs(_strSpaceKey);
            OnUserKeyPressed(dea);

        }

        private void altKeyboardButton_Click(object sender, EventArgs e)
        {
            if (KeyboardType == BoW.Standard)
            {
                KeyboardType = BoW.Alphabetical;
            }
            else
            {
                KeyboardType = BoW.Standard;
            }

        }
    }

    public class KeyboardEventArgs : EventArgs
    {
        private readonly string pvtKeyboardKeyPressed;

        public KeyboardEventArgs(string KeyboardKeyPressed)
        {
            this.pvtKeyboardKeyPressed = KeyboardKeyPressed;
        }

        public string KeyboardKeyPressed
        {
            get
            {
                return pvtKeyboardKeyPressed;
            }
        }
    }

    [Category("Keyboard Type"), Description("Type of keyboard to use")]
    public enum BoW { Standard, Alphabetical };
}