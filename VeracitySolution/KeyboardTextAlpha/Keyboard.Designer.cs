namespace KeyboardTextAlpha
{
    partial class Keyboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Keyboard));
            this.row1TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.uButton = new System.Windows.Forms.Button();
            this.yButton = new System.Windows.Forms.Button();
            this.iButton = new System.Windows.Forms.Button();
            this.pButton = new System.Windows.Forms.Button();
            this.oButton = new System.Windows.Forms.Button();
            this.wButton = new System.Windows.Forms.Button();
            this.qButton = new System.Windows.Forms.Button();
            this.eButton = new System.Windows.Forms.Button();
            this.tButton = new System.Windows.Forms.Button();
            this.rButton = new System.Windows.Forms.Button();
            this.row2LableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.kButton = new System.Windows.Forms.Button();
            this.jButton = new System.Windows.Forms.Button();
            this.hButton = new System.Windows.Forms.Button();
            this.gButton = new System.Windows.Forms.Button();
            this.fButton = new System.Windows.Forms.Button();
            this.dButton = new System.Windows.Forms.Button();
            this.lButton = new System.Windows.Forms.Button();
            this.aButton = new System.Windows.Forms.Button();
            this.sButton = new System.Windows.Forms.Button();
            this.row3TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.mButton = new System.Windows.Forms.Button();
            this.nButton = new System.Windows.Forms.Button();
            this.bButton = new System.Windows.Forms.Button();
            this.vButton = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.xButton = new System.Windows.Forms.Button();
            this.zButton = new System.Windows.Forms.Button();
            this.altKeyboardButton = new System.Windows.Forms.Button();
            this.row4TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.spaceButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.row1TableLayoutPanel.SuspendLayout();
            this.row2LableLayoutPanel.SuspendLayout();
            this.row3TableLayoutPanel.SuspendLayout();
            this.row4TableLayoutPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // row1TableLayoutPanel
            // 
            this.row1TableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.row1TableLayoutPanel.ColumnCount = 10;
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row1TableLayoutPanel.Controls.Add(this.uButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.yButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.iButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.pButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.oButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.wButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.qButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.eButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.tButton, 0, 0);
            this.row1TableLayoutPanel.Controls.Add(this.rButton, 0, 0);
            this.row1TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.row1TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.row1TableLayoutPanel.Name = "row1TableLayoutPanel";
            this.row1TableLayoutPanel.RowCount = 1;
            this.row1TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.row1TableLayoutPanel.Size = new System.Drawing.Size(397, 65);
            this.row1TableLayoutPanel.TabIndex = 0;
            // 
            // uButton
            // 
            this.uButton.AutoSize = true;
            this.uButton.BackColor = System.Drawing.Color.White;
            this.uButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("uButton.BackgroundImage")));
            this.uButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.uButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.uButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uButton.ForeColor = System.Drawing.Color.White;
            this.uButton.Location = new System.Drawing.Point(237, 3);
            this.uButton.Name = "uButton";
            this.uButton.Size = new System.Drawing.Size(33, 59);
            this.uButton.TabIndex = 12;
            this.uButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.uButton.UseVisualStyleBackColor = false;
            this.uButton.Click += new System.EventHandler(this.uButton_Click);
            // 
            // yButton
            // 
            this.yButton.AutoSize = true;
            this.yButton.BackColor = System.Drawing.Color.White;
            this.yButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("yButton.BackgroundImage")));
            this.yButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.yButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.yButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yButton.ForeColor = System.Drawing.Color.White;
            this.yButton.Location = new System.Drawing.Point(198, 3);
            this.yButton.Name = "yButton";
            this.yButton.Size = new System.Drawing.Size(33, 59);
            this.yButton.TabIndex = 11;
            this.yButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.yButton.UseVisualStyleBackColor = false;
            this.yButton.Click += new System.EventHandler(this.yButton_Click);
            // 
            // iButton
            // 
            this.iButton.AutoSize = true;
            this.iButton.BackColor = System.Drawing.Color.White;
            this.iButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("iButton.BackgroundImage")));
            this.iButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.iButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iButton.ForeColor = System.Drawing.Color.White;
            this.iButton.Location = new System.Drawing.Point(276, 3);
            this.iButton.Name = "iButton";
            this.iButton.Size = new System.Drawing.Size(33, 59);
            this.iButton.TabIndex = 10;
            this.iButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.iButton.UseVisualStyleBackColor = false;
            this.iButton.Click += new System.EventHandler(this.iButton_Click);
            // 
            // pButton
            // 
            this.pButton.AutoSize = true;
            this.pButton.BackColor = System.Drawing.Color.White;
            this.pButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pButton.BackgroundImage")));
            this.pButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pButton.ForeColor = System.Drawing.Color.White;
            this.pButton.Location = new System.Drawing.Point(354, 3);
            this.pButton.Name = "pButton";
            this.pButton.Size = new System.Drawing.Size(40, 59);
            this.pButton.TabIndex = 9;
            this.pButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.pButton.UseVisualStyleBackColor = false;
            this.pButton.Click += new System.EventHandler(this.pButton_Click);
            // 
            // oButton
            // 
            this.oButton.AutoSize = true;
            this.oButton.BackColor = System.Drawing.Color.White;
            this.oButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("oButton.BackgroundImage")));
            this.oButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.oButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.oButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oButton.ForeColor = System.Drawing.Color.White;
            this.oButton.Location = new System.Drawing.Point(315, 3);
            this.oButton.Name = "oButton";
            this.oButton.Size = new System.Drawing.Size(33, 59);
            this.oButton.TabIndex = 8;
            this.oButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.oButton.UseVisualStyleBackColor = false;
            this.oButton.Click += new System.EventHandler(this.oButton_Click);
            // 
            // wButton
            // 
            this.wButton.AutoSize = true;
            this.wButton.BackColor = System.Drawing.Color.Transparent;
            this.wButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("wButton.BackgroundImage")));
            this.wButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.wButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.wButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wButton.ForeColor = System.Drawing.Color.White;
            this.wButton.Location = new System.Drawing.Point(42, 3);
            this.wButton.Name = "wButton";
            this.wButton.Size = new System.Drawing.Size(33, 59);
            this.wButton.TabIndex = 7;
            this.wButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.wButton.UseVisualStyleBackColor = false;
            this.wButton.Click += new System.EventHandler(this.wButton_Click);
            // 
            // qButton
            // 
            this.qButton.AutoSize = true;
            this.qButton.BackColor = System.Drawing.Color.Transparent;
            this.qButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qButton.BackgroundImage")));
            this.qButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.qButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.qButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qButton.ForeColor = System.Drawing.Color.White;
            this.qButton.Location = new System.Drawing.Point(3, 3);
            this.qButton.Name = "qButton";
            this.qButton.Size = new System.Drawing.Size(33, 59);
            this.qButton.TabIndex = 6;
            this.qButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.qButton.UseVisualStyleBackColor = false;
            this.qButton.Click += new System.EventHandler(this.qButton_Click);
            // 
            // eButton
            // 
            this.eButton.AutoSize = true;
            this.eButton.BackColor = System.Drawing.Color.White;
            this.eButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("eButton.BackgroundImage")));
            this.eButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.eButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eButton.ForeColor = System.Drawing.Color.White;
            this.eButton.Location = new System.Drawing.Point(81, 3);
            this.eButton.Name = "eButton";
            this.eButton.Size = new System.Drawing.Size(33, 59);
            this.eButton.TabIndex = 5;
            this.eButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.eButton.UseVisualStyleBackColor = false;
            this.eButton.Click += new System.EventHandler(this.eButton_Click);
            // 
            // tButton
            // 
            this.tButton.AutoSize = true;
            this.tButton.BackColor = System.Drawing.Color.White;
            this.tButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tButton.BackgroundImage")));
            this.tButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.tButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tButton.ForeColor = System.Drawing.Color.White;
            this.tButton.Location = new System.Drawing.Point(159, 3);
            this.tButton.Name = "tButton";
            this.tButton.Size = new System.Drawing.Size(33, 59);
            this.tButton.TabIndex = 4;
            this.tButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.tButton.UseVisualStyleBackColor = false;
            this.tButton.Click += new System.EventHandler(this.tButton_Click);
            // 
            // rButton
            // 
            this.rButton.AutoSize = true;
            this.rButton.BackColor = System.Drawing.Color.White;
            this.rButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("rButton.BackgroundImage")));
            this.rButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.rButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rButton.ForeColor = System.Drawing.Color.White;
            this.rButton.Location = new System.Drawing.Point(120, 3);
            this.rButton.Name = "rButton";
            this.rButton.Size = new System.Drawing.Size(33, 59);
            this.rButton.TabIndex = 3;
            this.rButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.rButton.UseVisualStyleBackColor = false;
            this.rButton.Click += new System.EventHandler(this.rButton_Click);
            // 
            // row2LableLayoutPanel
            // 
            this.row2LableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.row2LableLayoutPanel.ColumnCount = 11;
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row2LableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.row2LableLayoutPanel.Controls.Add(this.kButton, 8, 0);
            this.row2LableLayoutPanel.Controls.Add(this.jButton, 7, 0);
            this.row2LableLayoutPanel.Controls.Add(this.hButton, 6, 0);
            this.row2LableLayoutPanel.Controls.Add(this.gButton, 5, 0);
            this.row2LableLayoutPanel.Controls.Add(this.fButton, 4, 0);
            this.row2LableLayoutPanel.Controls.Add(this.dButton, 3, 0);
            this.row2LableLayoutPanel.Controls.Add(this.lButton, 9, 0);
            this.row2LableLayoutPanel.Controls.Add(this.aButton, 1, 0);
            this.row2LableLayoutPanel.Controls.Add(this.sButton, 2, 0);
            this.row2LableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.row2LableLayoutPanel.Location = new System.Drawing.Point(0, 65);
            this.row2LableLayoutPanel.Name = "row2LableLayoutPanel";
            this.row2LableLayoutPanel.RowCount = 1;
            this.row2LableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.row2LableLayoutPanel.Size = new System.Drawing.Size(397, 65);
            this.row2LableLayoutPanel.TabIndex = 1;
            // 
            // kButton
            // 
            this.kButton.AutoSize = true;
            this.kButton.BackColor = System.Drawing.Color.White;
            this.kButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("kButton.BackgroundImage")));
            this.kButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.kButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.kButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kButton.ForeColor = System.Drawing.Color.White;
            this.kButton.Location = new System.Drawing.Point(295, 3);
            this.kButton.Name = "kButton";
            this.kButton.Size = new System.Drawing.Size(33, 59);
            this.kButton.TabIndex = 10;
            this.kButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.kButton.UseVisualStyleBackColor = false;
            this.kButton.Click += new System.EventHandler(this.kButton_Click);
            // 
            // jButton
            // 
            this.jButton.AutoSize = true;
            this.jButton.BackColor = System.Drawing.Color.White;
            this.jButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("jButton.BackgroundImage")));
            this.jButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.jButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jButton.ForeColor = System.Drawing.Color.White;
            this.jButton.Location = new System.Drawing.Point(256, 3);
            this.jButton.Name = "jButton";
            this.jButton.Size = new System.Drawing.Size(33, 59);
            this.jButton.TabIndex = 10;
            this.jButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.jButton.UseVisualStyleBackColor = false;
            this.jButton.Click += new System.EventHandler(this.jButton_Click);
            // 
            // hButton
            // 
            this.hButton.AutoSize = true;
            this.hButton.BackColor = System.Drawing.Color.White;
            this.hButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("hButton.BackgroundImage")));
            this.hButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.hButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hButton.ForeColor = System.Drawing.Color.White;
            this.hButton.Location = new System.Drawing.Point(217, 3);
            this.hButton.Name = "hButton";
            this.hButton.Size = new System.Drawing.Size(33, 59);
            this.hButton.TabIndex = 10;
            this.hButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.hButton.UseVisualStyleBackColor = false;
            this.hButton.Click += new System.EventHandler(this.hButton_Click);
            // 
            // gButton
            // 
            this.gButton.AutoSize = true;
            this.gButton.BackColor = System.Drawing.Color.White;
            this.gButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gButton.BackgroundImage")));
            this.gButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.gButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gButton.ForeColor = System.Drawing.Color.White;
            this.gButton.Location = new System.Drawing.Point(178, 3);
            this.gButton.Name = "gButton";
            this.gButton.Size = new System.Drawing.Size(33, 59);
            this.gButton.TabIndex = 10;
            this.gButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.gButton.UseVisualStyleBackColor = false;
            this.gButton.Click += new System.EventHandler(this.gButton_Click);
            // 
            // fButton
            // 
            this.fButton.AutoSize = true;
            this.fButton.BackColor = System.Drawing.Color.White;
            this.fButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fButton.BackgroundImage")));
            this.fButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.fButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fButton.ForeColor = System.Drawing.Color.White;
            this.fButton.Location = new System.Drawing.Point(139, 3);
            this.fButton.Name = "fButton";
            this.fButton.Size = new System.Drawing.Size(33, 59);
            this.fButton.TabIndex = 10;
            this.fButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.fButton.UseVisualStyleBackColor = false;
            this.fButton.Click += new System.EventHandler(this.fButton_Click);
            // 
            // dButton
            // 
            this.dButton.AutoSize = true;
            this.dButton.BackColor = System.Drawing.Color.White;
            this.dButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dButton.BackgroundImage")));
            this.dButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.dButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.dButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dButton.ForeColor = System.Drawing.Color.White;
            this.dButton.Location = new System.Drawing.Point(100, 3);
            this.dButton.Name = "dButton";
            this.dButton.Size = new System.Drawing.Size(33, 59);
            this.dButton.TabIndex = 10;
            this.dButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.dButton.UseVisualStyleBackColor = false;
            this.dButton.Click += new System.EventHandler(this.dButton_Click);
            // 
            // lButton
            // 
            this.lButton.AutoSize = true;
            this.lButton.BackColor = System.Drawing.Color.White;
            this.lButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lButton.BackgroundImage")));
            this.lButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lButton.ForeColor = System.Drawing.Color.White;
            this.lButton.Location = new System.Drawing.Point(334, 3);
            this.lButton.Name = "lButton";
            this.lButton.Size = new System.Drawing.Size(33, 59);
            this.lButton.TabIndex = 9;
            this.lButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lButton.UseVisualStyleBackColor = false;
            this.lButton.Click += new System.EventHandler(this.lButton_Click);
            // 
            // aButton
            // 
            this.aButton.AutoSize = true;
            this.aButton.BackColor = System.Drawing.Color.White;
            this.aButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("aButton.BackgroundImage")));
            this.aButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.aButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.aButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aButton.ForeColor = System.Drawing.Color.White;
            this.aButton.Location = new System.Drawing.Point(22, 3);
            this.aButton.Name = "aButton";
            this.aButton.Size = new System.Drawing.Size(33, 59);
            this.aButton.TabIndex = 10;
            this.aButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.aButton.UseVisualStyleBackColor = false;
            this.aButton.Click += new System.EventHandler(this.aButton_Click);
            // 
            // sButton
            // 
            this.sButton.AutoSize = true;
            this.sButton.BackColor = System.Drawing.Color.White;
            this.sButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sButton.BackgroundImage")));
            this.sButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.sButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sButton.ForeColor = System.Drawing.Color.White;
            this.sButton.Location = new System.Drawing.Point(61, 3);
            this.sButton.Name = "sButton";
            this.sButton.Size = new System.Drawing.Size(33, 59);
            this.sButton.TabIndex = 11;
            this.sButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.sButton.UseVisualStyleBackColor = false;
            this.sButton.Click += new System.EventHandler(this.sButton_Click);
            // 
            // row3TableLayoutPanel
            // 
            this.row3TableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.row3TableLayoutPanel.ColumnCount = 11;
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.row3TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.row3TableLayoutPanel.Controls.Add(this.bButton, 6, 0);
            this.row3TableLayoutPanel.Controls.Add(this.vButton, 5, 0);
            this.row3TableLayoutPanel.Controls.Add(this.cButton, 4, 0);
            this.row3TableLayoutPanel.Controls.Add(this.xButton, 3, 0);
            this.row3TableLayoutPanel.Controls.Add(this.zButton, 2, 0);
            this.row3TableLayoutPanel.Controls.Add(this.nButton, 7, 0);
            this.row3TableLayoutPanel.Controls.Add(this.mButton, 8, 0);
            this.row3TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.row3TableLayoutPanel.Location = new System.Drawing.Point(0, 130);
            this.row3TableLayoutPanel.Name = "row3TableLayoutPanel";
            this.row3TableLayoutPanel.RowCount = 1;
            this.row3TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.row3TableLayoutPanel.Size = new System.Drawing.Size(397, 65);
            this.row3TableLayoutPanel.TabIndex = 2;
            // 
            // mButton
            // 
            this.mButton.AutoSize = true;
            this.mButton.BackColor = System.Drawing.Color.White;
            this.mButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mButton.BackgroundImage")));
            this.mButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.mButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mButton.ForeColor = System.Drawing.Color.White;
            this.mButton.Location = new System.Drawing.Point(283, 3);
            this.mButton.Name = "mButton";
            this.mButton.Size = new System.Drawing.Size(33, 59);
            this.mButton.TabIndex = 10;
            this.mButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.mButton.UseVisualStyleBackColor = false;
            this.mButton.Click += new System.EventHandler(this.mButton_Click);
            // 
            // nButton
            // 
            this.nButton.AutoSize = true;
            this.nButton.BackColor = System.Drawing.Color.White;
            this.nButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("nButton.BackgroundImage")));
            this.nButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.nButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.nButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nButton.ForeColor = System.Drawing.Color.White;
            this.nButton.Location = new System.Drawing.Point(244, 3);
            this.nButton.Name = "nButton";
            this.nButton.Size = new System.Drawing.Size(33, 59);
            this.nButton.TabIndex = 10;
            this.nButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.nButton.UseVisualStyleBackColor = false;
            this.nButton.Click += new System.EventHandler(this.nButton_Click);
            // 
            // bButton
            // 
            this.bButton.AutoSize = true;
            this.bButton.BackColor = System.Drawing.Color.White;
            this.bButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bButton.BackgroundImage")));
            this.bButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bButton.ForeColor = System.Drawing.Color.White;
            this.bButton.Location = new System.Drawing.Point(205, 3);
            this.bButton.Name = "bButton";
            this.bButton.Size = new System.Drawing.Size(33, 59);
            this.bButton.TabIndex = 10;
            this.bButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.bButton.UseVisualStyleBackColor = false;
            this.bButton.Click += new System.EventHandler(this.bButton_Click);
            // 
            // vButton
            // 
            this.vButton.AutoSize = true;
            this.vButton.BackColor = System.Drawing.Color.White;
            this.vButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("vButton.BackgroundImage")));
            this.vButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.vButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.vButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vButton.ForeColor = System.Drawing.Color.White;
            this.vButton.Location = new System.Drawing.Point(166, 3);
            this.vButton.Name = "vButton";
            this.vButton.Size = new System.Drawing.Size(33, 59);
            this.vButton.TabIndex = 10;
            this.vButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.vButton.UseVisualStyleBackColor = false;
            this.vButton.Click += new System.EventHandler(this.vButton_Click);
            // 
            // cButton
            // 
            this.cButton.AutoSize = true;
            this.cButton.BackColor = System.Drawing.Color.White;
            this.cButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("cButton.BackgroundImage")));
            this.cButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.cButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cButton.ForeColor = System.Drawing.Color.White;
            this.cButton.Location = new System.Drawing.Point(127, 3);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(33, 59);
            this.cButton.TabIndex = 10;
            this.cButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cButton.UseVisualStyleBackColor = false;
            this.cButton.Click += new System.EventHandler(this.cButton_Click);
            // 
            // xButton
            // 
            this.xButton.AutoSize = true;
            this.xButton.BackColor = System.Drawing.Color.White;
            this.xButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("xButton.BackgroundImage")));
            this.xButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.xButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.xButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xButton.ForeColor = System.Drawing.Color.White;
            this.xButton.Location = new System.Drawing.Point(88, 3);
            this.xButton.Name = "xButton";
            this.xButton.Size = new System.Drawing.Size(33, 59);
            this.xButton.TabIndex = 10;
            this.xButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.xButton.UseVisualStyleBackColor = false;
            this.xButton.Click += new System.EventHandler(this.xButton_Click);
            // 
            // zButton
            // 
            this.zButton.AutoSize = true;
            this.zButton.BackColor = System.Drawing.Color.White;
            this.zButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("zButton.BackgroundImage")));
            this.zButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.zButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.zButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zButton.ForeColor = System.Drawing.Color.White;
            this.zButton.Location = new System.Drawing.Point(49, 3);
            this.zButton.Name = "zButton";
            this.zButton.Size = new System.Drawing.Size(33, 59);
            this.zButton.TabIndex = 11;
            this.zButton.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.zButton.UseVisualStyleBackColor = false;
            this.zButton.Click += new System.EventHandler(this.zButton_Click);
            // 
            // altKeyboardButton
            // 
            this.altKeyboardButton.BackColor = System.Drawing.Color.White;
            this.altKeyboardButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("altKeyboardButton.BackgroundImage")));
            this.altKeyboardButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.altKeyboardButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.altKeyboardButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.altKeyboardButton.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.altKeyboardButton.ForeColor = System.Drawing.Color.Black;
            this.altKeyboardButton.Location = new System.Drawing.Point(0, 0);
            this.altKeyboardButton.Name = "altKeyboardButton";
            this.altKeyboardButton.Size = new System.Drawing.Size(113, 50);
            this.altKeyboardButton.TabIndex = 12;
            this.altKeyboardButton.Text = "Switch Keyboard\r\n(ABCD...)";
            this.altKeyboardButton.UseVisualStyleBackColor = false;
            this.altKeyboardButton.Visible = false;
            this.altKeyboardButton.Click += new System.EventHandler(this.altKeyboardButton_Click);
            // 
            // row4TableLayoutPanel
            // 
            this.row4TableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.row4TableLayoutPanel.ColumnCount = 4;
            this.row4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.row4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.row4TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.row4TableLayoutPanel.Controls.Add(this.spaceButton, 1, 0);
            this.row4TableLayoutPanel.Controls.Add(this.panel1, 2, 0);
            this.row4TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.row4TableLayoutPanel.Location = new System.Drawing.Point(0, 195);
            this.row4TableLayoutPanel.Name = "row4TableLayoutPanel";
            this.row4TableLayoutPanel.RowCount = 1;
            this.row4TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.row4TableLayoutPanel.Size = new System.Drawing.Size(397, 56);
            this.row4TableLayoutPanel.TabIndex = 3;
            // 
            // spaceButton
            // 
            this.spaceButton.AutoSize = true;
            this.spaceButton.BackColor = System.Drawing.Color.White;
            this.spaceButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("spaceButton.BackgroundImage")));
            this.spaceButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.spaceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.spaceButton.Font = new System.Drawing.Font("Tahoma", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.spaceButton.ForeColor = System.Drawing.Color.Black;
            this.spaceButton.Location = new System.Drawing.Point(42, 3);
            this.spaceButton.Name = "spaceButton";
            this.spaceButton.Size = new System.Drawing.Size(192, 50);
            this.spaceButton.TabIndex = 11;
            this.spaceButton.Text = "SPACE";
            this.spaceButton.UseVisualStyleBackColor = false;
            this.spaceButton.Visible = false;
            this.spaceButton.Click += new System.EventHandler(this.spaceButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.altKeyboardButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(240, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(113, 50);
            this.panel1.TabIndex = 13;
            // 
            // Keyboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.row4TableLayoutPanel);
            this.Controls.Add(this.row3TableLayoutPanel);
            this.Controls.Add(this.row2LableLayoutPanel);
            this.Controls.Add(this.row1TableLayoutPanel);
            this.Name = "Keyboard";
            this.Size = new System.Drawing.Size(397, 256);
            this.row1TableLayoutPanel.ResumeLayout(false);
            this.row1TableLayoutPanel.PerformLayout();
            this.row2LableLayoutPanel.ResumeLayout(false);
            this.row2LableLayoutPanel.PerformLayout();
            this.row3TableLayoutPanel.ResumeLayout(false);
            this.row3TableLayoutPanel.PerformLayout();
            this.row4TableLayoutPanel.ResumeLayout(false);
            this.row4TableLayoutPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel row1TableLayoutPanel;
        private System.Windows.Forms.Button eButton;
        private System.Windows.Forms.Button tButton;
        private System.Windows.Forms.Button rButton;
        private System.Windows.Forms.Button uButton;
        private System.Windows.Forms.Button yButton;
        private System.Windows.Forms.Button iButton;
        private System.Windows.Forms.Button pButton;
        private System.Windows.Forms.Button oButton;
        private System.Windows.Forms.Button wButton;
        private System.Windows.Forms.Button qButton;
        private System.Windows.Forms.TableLayoutPanel row2LableLayoutPanel;
        private System.Windows.Forms.Button kButton;
        private System.Windows.Forms.Button jButton;
        private System.Windows.Forms.Button hButton;
        private System.Windows.Forms.Button gButton;
        private System.Windows.Forms.Button fButton;
        private System.Windows.Forms.Button dButton;
        private System.Windows.Forms.Button lButton;
        private System.Windows.Forms.Button aButton;
        private System.Windows.Forms.Button sButton;
        private System.Windows.Forms.TableLayoutPanel row3TableLayoutPanel;
        private System.Windows.Forms.Button mButton;
        private System.Windows.Forms.Button nButton;
        private System.Windows.Forms.Button bButton;
        private System.Windows.Forms.Button vButton;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button xButton;
        private System.Windows.Forms.Button zButton;
        private System.Windows.Forms.TableLayoutPanel row4TableLayoutPanel;
        private System.Windows.Forms.Button spaceButton;
        private System.Windows.Forms.Button altKeyboardButton;
        private System.Windows.Forms.Panel panel1;
    }
}
