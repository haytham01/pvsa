﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.Specialized;

using Veracity.Client.Proxy;

namespace VeracityApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region fields
        private Properties.Settings appSettings = Properties.Settings.Default;
        #endregion

        #region menu routed commands

        public static readonly RoutedCommand ChangeSkinCommand = new RoutedCommand("ChangeSkin", typeof(MainWindow));

        #endregion

        public MainWindow()
        {
            InitializeComponent();

            BuildSkinsMenu();

            BuildLanguageButtons();

            SkinsMenu.IsEnabled = true;


            //init();
        }

        private void BuildLanguageButtons()
        {
            //this.b1.ApplyTemplate();
            //this.b1.Visibility = System.Windows.Visibility.Visible;
            //ControlTemplate ct = b1.Template;
            //Border border = (Border)ct.FindName("border", b1);




            //var myImage = (Image) this.b1.FindName("mainImage");

            //this.b2.Visibility = System.Windows.Visibility.Visible;

            for (int i = 0; i < 3; i++)
            {
                Button languageButton = new Button();
                //languageButton.Padding = new Thickness(0, 0, 0, 0);
                //languageButton.Height = 300;
                //languageButton.Width = 400;
                languageButton.Tag = i;
                languageButton.Template = (ControlTemplate)FindResource("LanguageButton");
                languageButton.ApplyTemplate();
                ControlTemplate ct = languageButton.Template;

                Image myImage = (Image)ct.FindName("mainImage", languageButton);
                if (myImage != null)
                {
                    myImage.Source = new BitmapImage(new Uri("/Language/Hebrew/Button/button-hebrew.jpg", UriKind.Relative));
                }
                languageButton.Click += new RoutedEventHandler(languageButtonClick);
                this.mainWrapPanel.Children.Add(languageButton);
            }
            
        }

        //private void init()
        //{
        //    string machineName = Helper.GetMachineName();
        //    this.Title = "Veracity Screen Test - " + machineName;
        //}

        /// <summary>
        /// Builds the Skins Menu
        /// </summary>
        private void BuildSkinsMenu()
        {
            NameValueCollection skins = App.Skins;
            foreach (string skinName in skins.AllKeys)
            {
                MenuItem skin = new MenuItem();
                skin.Header = skinName;
                skin.CommandParameter = skins[skinName];
                skin.Command = ChangeSkinCommand;
                skin.IsEnabled = true;

                SkinsMenu.Items.Add(skin);
            }
        }

        #region command handlers
        /// <summary>
        /// Command handler for ChangeSkinCommand
        /// </summary>
        private void ChangeSkin(object sender, ExecutedRoutedEventArgs e)
        {
            ResourceDictionary rd = new ResourceDictionary();
            rd.MergedDictionaries.Add(Application.LoadComponent(new Uri(e.Parameter as string, UriKind.Relative)) as ResourceDictionary);
            Application.Current.Resources = rd;

            // save the skin setting
            appSettings.Skin = e.Parameter as string;
            appSettings.Save();

            //family.OnContentChanged();
            //PersonInfoControl.OnSkinChanged();
        }

  
        private void languageButtonClick(object sender, EventArgs e)
        {
            Button clicked = (Button)sender;

            IdentificationWindow identificationWindow = new IdentificationWindow();
            identificationWindow.ShowDialog();

            MessageBox.Show("Button's name is: " + clicked.Name);
            QuestionWindow questionWindow = new QuestionWindow();
            questionWindow.ShowDialog();
            //this.Hide();

       
        }
        #endregion

        private void endOfTestButton_Click(object sender, RoutedEventArgs e)
        {
            TestSession testSession = new TestSession();
            // test is going to be populated
            Screen screen = new Screen();
            screen.ReadTimeInSeconds = 5.0;
            screen.FlowOrderIndex = 1;
            testSession.lstScreenQuestionOnly.Add(screen);

            screen = new Screen();
            screen.ReadTimeInSeconds = 15.0;
            screen.FlowOrderIndex = 2;
            testSession.lstScreenQuestionOnly.Add(screen);

            //Analyzer analyzer = new Analyzer();
            //analyzer.AnalysisTest15Questions(ref testSession);

            // then formulate the print report
            // then send to printer

            //MessageBox.Show("Avg RD Time:  " + analyzer.lstAvgs[0]);
        }

    }
}
