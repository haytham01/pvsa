﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Collections.Specialized;
using System.IO;
using System.Globalization;



namespace VeracityApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region fields

        // The name of the application folder.  This folder is used to save the files 
        // for this application such as the photos, stories and family data.
        internal const string ApplicationFolderName = "Veracity.Test";

        #endregion

        #region overrides

        /// <summary>
        /// Occurs when the application starts.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        protected override void OnStartup(StartupEventArgs e)
        {
            Properties.Settings appSettings = VeracityApp.Properties.Settings.Default;

            if (!string.IsNullOrEmpty(appSettings.Skin))
            {
                try
                {
                    ResourceDictionary rd = new ResourceDictionary();
                    rd.MergedDictionaries.Add(Application.LoadComponent(new Uri(appSettings.Skin, UriKind.Relative)) as ResourceDictionary);
                    Application.Current.Resources = rd;
                }
                catch
                {
                }
            }

            base.OnStartup(e);
        }

        /// <summary>
        /// Occurs when the application exits.
        /// </summary>
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }
        #endregion

        #region methods

        /// <summary>
        /// Gets the collection of skins
        /// </summary>
        public static NameValueCollection Skins
        {
            get
            {
                NameValueCollection skins = new NameValueCollection();
                Properties.Settings appSettings = VeracityApp.Properties.Settings.Default;

                foreach (string folder in Directory.GetDirectories(VeracityApp.Properties.Resources.Skins))
                {
                    foreach (string file in Directory.GetFiles(folder))
                    {
                        FileInfo fileInfo = new FileInfo(file);
                        if (string.Compare(fileInfo.Extension, VeracityApp.Properties.Resources.XamlExtension,
                            true, CultureInfo.InvariantCulture) == 0)
                        {
                            // Use the first part of the resource file name for the menu item name.
                            skins.Add(fileInfo.Name.Remove(fileInfo.Name.IndexOf(VeracityApp.Properties.Resources.ResourcesString)),
                                Path.Combine(folder, fileInfo.Name));
                        }
                    }
                }
                return skins;
            }
        }

        #endregion
    }

        
}
