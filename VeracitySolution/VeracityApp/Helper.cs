﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace VeracityApp
{
    public class Helper
    {
        public static string GetMachineName()
        {
            // hello
            string machineName = null;
            try
            {
                machineName = System.Environment.MachineName;
            }
            catch {}
            return machineName;
        }

        /// <summary>
        /// Display the application version.
        /// </summary>
        private string GetVersion()
        {
            string s = null;
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            s += string.Format(CultureInfo.CurrentCulture,
                "{0}.{1}.{2}", version.Major, version.Minor, version.Build);
            return s;
        }

    }
}
