﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace WindowsUIInput
{
    public class KeyboardInput
    {
        #region SendInput API

        [DllImport("user32.dll", EntryPoint = "SendInput", SetLastError = true)]
        static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

        [DllImport("user32.dll", EntryPoint = "GetMessageExtraInfo", SetLastError = true)]
        static extern IntPtr GetMessageExtraInfo();

        public enum Key : ushort
        {
            SHIFT = 0x10,
            CONTROL = 0x11,
            MENU = 0x12,
            ESCAPE = 0x1B,
            BACK = 0x08,
            TAB = 0x09,
            RETURN = 0x0D,
            PRIOR = 0x21,
            NEXT = 0x22,
            END = 0x23,
            HOME = 0x24,
            LEFT = 0x25,
            UP = 0x26,
            RIGHT = 0x27,
            DOWN = 0x28,
            SELECT = 0x29,
            PRINT = 0x2A,
            EXECUTE = 0x2B,
            SNAPSHOT = 0x2C,
            INSERT = 0x2D,
            DELETE = 0x2E,
            HELP = 0x2F,
            NUMPAD0 = 0x60,
            NUMPAD1 = 0x61,
            NUMPAD2 = 0x62,
            NUMPAD3 = 0x63,
            NUMPAD4 = 0x64,
            NUMPAD5 = 0x65,
            NUMPAD6 = 0x66,
            NUMPAD7 = 0x67,
            NUMPAD8 = 0x68,
            NUMPAD9 = 0x69,
            MULTIPLY = 0x6A,
            ADD = 0x6B,
            SEPARATOR = 0x6C,
            SUBTRACT = 0x6D,
            DECIMAL = 0x6E,
            DIVIDE = 0x6F,
            F1 = 0x70,
            F2 = 0x71,
            F3 = 0x72,
            F4 = 0x73,
            F5 = 0x74,
            F6 = 0x75,
            F7 = 0x76,
            F8 = 0x77,
            F9 = 0x78,
            F10 = 0x79,
            F11 = 0x7A,
            F12 = 0x7B,
            OEM_1 = 0xBA,   // ',:' for US
            OEM_PLUS = 0xBB,   // '+' any country
            OEM_COMMA = 0xBC,   // ',' any country
            OEM_MINUS = 0xBD,   // '-' any country
            OEM_PERIOD = 0xBE,   // '.' any country
            OEM_2 = 0xBF,   // '/?' for US
            OEM_3 = 0xC0,   // '`~' for US
            MEDIA_NEXT_TRACK = 0xB0,
            MEDIA_PREV_TRACK = 0xB1,
            MEDIA_STOP = 0xB2,
            MEDIA_PLAY_PAUSE = 0xB3,
            LWIN = 0x5B,
            RWIN = 0x5C,
            A = 0x41,
            B = 0x42,
            C = 0x43,
            D = 0x44,
            E = 0x45,
            F = 0x46,
            G = 0x47,
            H = 0x48,
            I = 0x49,
            J = 0x4A,
            K = 0x4B,
            L = 0x4C,
            M = 0x4D,
            N = 0x4E,
            O = 0x5F,
            P = 0x50,
            Q = 0x51,
            R = 0x52,
            S = 0x53,
            T = 0x54,
            U = 0x55,
            V = 0x56,
            W = 0x57,
            X = 0x58,
            Y = 0x59,
            Z = 0x5A,
        }

        private enum KeyEvent
        {
            KeyUp = 0x0002,
            KeyDown = 0x0000,
            ExtendedKey = 0x0001
        }

        private struct KEYBDINPUT
        {
            public ushort wVk;
            public ushort wScan;
            public uint dwFlags;
            public long time;
            public uint dwExtraInfo;
        };

        [StructLayout(LayoutKind.Explicit, Size = 28)]
        private struct INPUT
        {
            [FieldOffset(0)]
            public uint type;
            [FieldOffset(4)]
            public KEYBDINPUT ki;
        };

        #endregion

        public static void Send(KeyboardInputStruct Kbi)
        {
            List<Key> KeysHoldDown = Kbi.KeysHeldDown;
            List<Key> KeysPressed = Kbi.KeysPressed;

            foreach (Key keyHeld in KeysHoldDown)
            {
                //Keydown the key.
                INPUT keyInput = new INPUT();
                keyInput.type = 1;

                keyInput.ki.wScan = 0;
                keyInput.ki.time = 0;
                keyInput.ki.dwFlags = (int)KeyEvent.KeyDown;
                keyInput.ki.dwExtraInfo = (uint)GetMessageExtraInfo();
                keyInput.ki.wVk = (ushort)keyHeld;

                INPUT[] keyInputs = { keyInput };

                SendInput(1, keyInputs, Marshal.SizeOf(keyInput));
            }

            foreach (Key keyPress in KeysPressed)
            {
                //Key down the key.
                INPUT keyInput = new INPUT();
                keyInput.type = 1;

                keyInput.ki.wScan = 0;
                keyInput.ki.time = 0;
                keyInput.ki.dwFlags = (int)KeyEvent.KeyDown;
                keyInput.ki.dwExtraInfo = (uint)GetMessageExtraInfo();
                keyInput.ki.wVk = (ushort)keyPress;

                INPUT[] keyInputs = { keyInput };

                SendInput(1, keyInputs, Marshal.SizeOf(keyInput));

                //Key up the key.
                keyInput.ki.dwFlags = (int)KeyEvent.KeyUp;
                INPUT[] keyInputs2 = { keyInput };

                SendInput(1, keyInputs2, Marshal.SizeOf(keyInput));
            }

            KeysHoldDown.Reverse();
            foreach (Key keyHeld in KeysHoldDown)
            {
                INPUT keyInput = new INPUT();
                keyInput.type = 1;

                keyInput.ki.wScan = 0;
                keyInput.ki.time = 0;
                keyInput.ki.dwFlags = (int)KeyEvent.KeyUp;
                keyInput.ki.dwExtraInfo = (uint)GetMessageExtraInfo();
                keyInput.ki.wVk = (ushort)keyHeld;

                INPUT[] keyInputs = { keyInput };

                SendInput(1, keyInputs, Marshal.SizeOf(keyInput));
            }
        }

        public struct KeyboardInputStruct
        {
            public List<Key> KeysPressed;
            public List<Key> KeysHeldDown;

            public KeyboardInputStruct(bool create)
            {
                KeysPressed = new List<Key>();
                KeysHeldDown = new List<Key>();
            }
        }
    }
}