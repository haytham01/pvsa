﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Configuration;

namespace REST
{
    public class LocalConfig
    {
        private List<string> _lstIncomingFolder = new List<string>();
        private List<string> _lstIncomingFolderCSV = new List<string>();

        private static readonly string KEY_INCOMING_FOLDER_1 = "IncomingFolder_1";
        private static readonly string KEY_INCOMING_FOLDER_2 = "IncomingFolder_2";
        private static readonly string KEY_INCOMING_FOLDER_3 = "IncomingFolder_3";
        private static readonly string KEY_INCOMING_FOLDER_4 = "IncomingFolder_4";
        private static readonly string KEY_INCOMING_FOLDER_5 = "IncomingFolder_5";
        private static readonly string KEY_INCOMING_FOLDER_CSV_1 = "IncomingFolderCSV_1";
        private static readonly string KEY_INCOMING_FOLDER_CSV_2 = "IncomingFolderCSV_2";
        private static readonly string KEY_INCOMING_FOLDER_CSV_3 = "IncomingFolderCSV_3";
        private static readonly string KEY_INCOMING_FOLDER_CSV_4 = "IncomingFolderCSV_4";
        private static readonly string KEY_INCOMING_FOLDER_CSV_5 = "IncomingFolderCSV_5";

        public List<string> lstIncomingFolder
        {
            get { return _lstIncomingFolder; }
            set { _lstIncomingFolder = value; }
        }
        public List<string> lstIncomingFolderCSV
        {
            get { return _lstIncomingFolderCSV; }
            set { _lstIncomingFolderCSV = value; }
        }
        public LocalConfig()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();

            _lstIncomingFolder.Clear();
            try
            {
                _lstIncomingFolder.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_1, typeof(System.String)));
            }
            catch
            {
            }
            try
            {
                _lstIncomingFolder.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_2, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolder.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_3, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolder.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_4, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolder.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_5, typeof(System.String)));
            }
            catch
            {
            }

            _lstIncomingFolderCSV.Clear();
            try
            {
                _lstIncomingFolderCSV.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_CSV_1, typeof(System.String)));
            }
            catch
            {
            }
            try
            {
                _lstIncomingFolderCSV.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_CSV_2, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolderCSV.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_CSV_3, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolderCSV.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_CSV_4, typeof(System.String)));
            }
            catch
            {
            } try
            {
                _lstIncomingFolderCSV.Add((string)configurationAppSettings.GetValue(KEY_INCOMING_FOLDER_CSV_5, typeof(System.String)));
            }
            catch
            {
            }

        }
    }
}