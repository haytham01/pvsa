﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Collections;
using System.ServiceModel.Web;
using System.Net;

using Veracity.DataAccessLayer.Data;

namespace REST.Resources
{
    public class RESTTestSession
    {
        private string _strData = null;

        private HttpStatusCode _httpStatusCode = HttpStatusCode.BadRequest;
        private string _httpStatusDescription = "Unsuccessful transaction";

        private bool _hasError = false;

        public string httpStatusDescription
        {
            get { return _httpStatusDescription; }
            set { _httpStatusDescription = value; }
        }
        public HttpStatusCode httpStatusCode
        {
            get { return _httpStatusCode; }
            set { _httpStatusCode = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }
        public RESTTestSession(string pStrData)
        {
            _strData = pStrData;
        }
        public RESTTestSession()
        {
        }

        public void Get(string pStrTestSessionID)
        {
            try
            {
                LocalConfig localConfig = new LocalConfig();
                if ((pStrTestSessionID != null) && (pStrTestSessionID.Length > 0) &&
                    (localConfig.lstIncomingFolder != null) && (localConfig.lstIncomingFolder.Count > 0))
                {
                    bool success = true;
                    foreach (string s in localConfig.lstIncomingFolder)
                    {
                        string dirPath = s + System.IO.Path.DirectorySeparatorChar + pStrTestSessionID;
                        if (!Directory.Exists(dirPath))
                        {
                            success = false;
                        }
                    }

                    if (success)
                    {
                        _httpStatusCode = HttpStatusCode.OK;
                        _httpStatusDescription = "Successful transaction.";
                    }

                }

            }
            catch (Exception ex)
            {
                _httpStatusCode = HttpStatusCode.BadRequest;
                _httpStatusDescription = "Error in getting testsession.";
            }
        }

        public void Serialize(string pStrData)
        {
            try
            {
                LocalConfig localConfig = new LocalConfig();
                IncomingTestSession incomingTestSession = null;
                if (RESTUtils.IsXML(pStrData))
                {
                    incomingTestSession = new IncomingTestSession(pStrData);
                }

                if ((incomingTestSession != null) && (!incomingTestSession.HasError) && (incomingTestSession.TestSessionID != null))
                {
                    bool success = true;

                    string filename = null;
                    if (incomingTestSession.IsFinished)
                    {
                        filename = incomingTestSession.TestSessionID + ".xml";
                    }
                    else
                    {
                        filename = incomingTestSession.TestSessionID + "." + incomingTestSession.SessionSerializationIndex + "~";
                    }

                    string filepath = null;
                    string dirPath = null;
                    foreach (string s in localConfig.lstIncomingFolder)
                    {
                        try
                        {
                            dirPath = s + System.IO.Path.DirectorySeparatorChar + incomingTestSession.TestSessionID;
                            if (!Directory.Exists(dirPath))
                            {
                                Directory.CreateDirectory(dirPath);
                            }

                            filepath = dirPath + System.IO.Path.DirectorySeparatorChar + filename;
                            System.IO.File.WriteAllText(filepath, pStrData);

                            if (incomingTestSession.IsFinished)
                            {
                                filepath = s + System.IO.Path.DirectorySeparatorChar + filename;
                                System.IO.File.WriteAllText(filepath, pStrData);
                            }
                        }
                        catch { success = false; }



                    }

                    if ((incomingTestSession.CsvData != null) && (incomingTestSession.IsFinished))
                    {

                        string filenameCSVSingle = "PVSA-OUTPUT1.CSV";
                        string filenameCSVAll = "PVSA-OUTPUT.CSV";
                        string csvDirPath = null;
                        string csvFilePath = null;
                        string csvAllFilePath = null;
                        foreach (string s in localConfig.lstIncomingFolderCSV)
                        {
                            try
                            {
                                csvDirPath = s + System.IO.Path.DirectorySeparatorChar + incomingTestSession.objClient.ClientID;
                                if (!Directory.Exists(csvDirPath))
                                {
                                    Directory.CreateDirectory(csvDirPath);
                                }
                                string csvDataToWrite = incomingTestSession.CsvData;
                                if ((incomingTestSession.objTestSubject != null) && (incomingTestSession.objTestSubject.DriverLicenseNumber != null))
                                {
                                    csvDataToWrite = csvDataToWrite + "," + incomingTestSession.objTestSubject.DriverLicenseNumber;
                                }
                                csvFilePath = csvDirPath + System.IO.Path.DirectorySeparatorChar + filenameCSVSingle;
                                System.IO.File.WriteAllText(csvFilePath, csvDataToWrite);

                                csvAllFilePath = csvDirPath + System.IO.Path.DirectorySeparatorChar + filenameCSVAll;
                                System.IO.File.AppendAllText(csvAllFilePath, csvDataToWrite + Environment.NewLine);
                            }
                            catch { }
                        }

                    }

                    if (success)
                    {
                        _httpStatusCode = HttpStatusCode.OK;
                        _httpStatusDescription = "Successful transaction.";
                    }

                }
                else
                {
                    _httpStatusCode = HttpStatusCode.BadRequest;
                    _httpStatusDescription = "Test session data is not XML.";
                }

            }
            catch (Exception ex)
            {
                _httpStatusCode = HttpStatusCode.BadRequest;
                _httpStatusDescription = "Error in serializing test session.";
            }
        }

    }
}