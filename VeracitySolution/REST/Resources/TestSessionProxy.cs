﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace REST.Resources
{
    public class TestSessionProxy
    {
        public long InternalID { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
    }
}