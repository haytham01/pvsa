﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Collections;
using System.ServiceModel.Web;
using System.Net;

using Veracity.DataAccessLayer.Data;

namespace REST.Resources
{
    public class IncomingTestSession
    {
        private string _strTestSessionID = null;
        private string _strCsvData = null;
        private TestTemplate _objTestTemplate = null;
        private Client _objClient = null;
        private bool _bIsFinished = false;

        private bool _hasError = false;

        public static readonly string ENTITY_NAME = "TestSession";
        public static readonly string TAG_TEST_SESSION_ID = "TestSessionID";
        public static readonly string TAG_CSV_DATA = "CsvData";
        public static readonly string TAG_IS_FINISHED = "IsFinished";

        public Client objClient
        {
            get { return _objClient; }
            set { _objClient = value; }
        }
        public TestTemplate objTestTemplate
        {
            get { return _objTestTemplate; }
            set { _objTestTemplate = value; }
        }
        public string TestSessionID
        {
            get { return _strTestSessionID; }
            set { _strTestSessionID = value; }
        }
        public string CsvData
        {
            get { return _strCsvData; }
            set { _strCsvData = value; }
        }
        public bool IsFinished
        {
            get { return _bIsFinished; }
            set { _bIsFinished = value; }
        }
        public bool HasError
        {
            get { return _hasError; }
        }

        public IncomingTestSession(string pStrData)
        {
            Parse(pStrData);
        }
        public void Parse(string pStrData)
        {
            if (RESTUtils.IsXML(pStrData))
            {
                parseXML(pStrData);
            }
            else
            {
                _hasError = true;
            }
        }

        private void parseXML(string pStrData)
        {
            try
            {
                XmlDocument xmlDoc = null;
                string strXPath = null;
                XmlNodeList xNodes = null;

                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(pStrData);

                // get the element
                strXPath = "//" + ENTITY_NAME;
                xNodes = xmlDoc.SelectNodes(strXPath);
                //XmlNode xNode = xNodes[0];

                XmlNode xResultNode = null;
                string strTmp = null;

                foreach (XmlNode xNode in xNodes)
                {
                    try
                    {
                        xResultNode = xNode.SelectSingleNode(TAG_TEST_SESSION_ID);
                        _strTestSessionID = xResultNode.InnerText;  
                    }
                    catch {}

                    try
                    {
                        xResultNode = xNode.SelectSingleNode(TAG_CSV_DATA);
                        _strCsvData = xResultNode.InnerText;
                    }
                    catch { }

                    try
                    {
                        _objTestTemplate = new TestTemplate();
                        _objTestTemplate.Parse(pStrData);
                    }
                    catch { }

                    try
                    {
                        xResultNode = xNode.SelectSingleNode(TAG_IS_FINISHED);
                        strTmp = xResultNode.InnerText;
                        _bIsFinished = Convert.ToBoolean(strTmp);
                    }
                    catch { _bIsFinished = false; }

                    try
                    {
                        _objClient = new Client();
                        _objClient.Parse(pStrData);
                    }
                    catch { }
                }

            }
            catch
            {
                _hasError = true;
            }
        }


    }
}