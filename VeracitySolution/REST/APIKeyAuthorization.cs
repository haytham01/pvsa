﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Xml;
using System.Xml.Linq;

using Veracity.DataAccessLayer.Data;
using Veracity.BusinessFacadeLayer;

namespace REST
{
    public class APIKeyAuthorization : ServiceAuthorizationManager
    {
        private BusFacCore _busFacCore = new BusFacCore();
        //private Apilog apilog = null;

        public override bool CheckAccess(OperationContext operationContext, ref System.ServiceModel.Channels.Message message)
        {
            if (operationContext.RequestContext.RequestMessage.Headers.To.AbsolutePath.IndexOf("/help") != -1)
            {
                return true;
            }
            else
            {
                //Message message = OperationContext.Current.RequestContext.RequestMessage;
                MessageBuffer requestBuffer = null;
                requestBuffer = message.CreateBufferedCopy(int.MaxValue);
                string key = GetAPIKey(requestBuffer.CreateMessage());
                if (APIKeyRepository.IsValidAPIKey(key))
                {
                    message = requestBuffer.CreateMessage();
                    return true;
                }
                else
                {
                    // Send back an HTML reply
                    // Get the request message
                    //var request = message;
                    //// Get the HTTP Request
                    //var requestProp = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
                    //apilog = fac.LogApi(pLngRefNum, api_key, auth_key, pStrMsgSource, msgText, Convert.ToString(pIntTrace), pBlnIsSuccess,
                    //    pBlnInProgress, statusCodeStr, statusCodeNum, message., pStrResptxt);
                    CreateErrorReply(operationContext, key);
                    return false;
                }


            }
        }

        public string GetAPIKey(Message message)
        {
            string strkey = null;

            // Get the request message
            var request = message;

            // Get the HTTP Request
            var requestProp = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];

            // Get the query string
            NameValueCollection queryParams = HttpUtility.ParseQueryString(requestProp.QueryString);

            // Return the API key (if present, null if not)
            strkey = queryParams[APIKEY];

            if (strkey == null)
            {
                // check if post call
                XmlDictionaryReader postBodyXmlReader = message.GetReaderAtBodyContents();
                postBodyXmlReader.ReadStartElement();
                MemoryStream postBodyStream = new MemoryStream(postBodyXmlReader.ReadContentAsBase64());
                StreamReader postBodyStringReader = new StreamReader(postBodyStream);
                String postBody = postBodyStringReader.ReadToEnd();
                if (postBody != null)
                {
                    queryParams = HttpUtility.ParseQueryString(postBody);
                    strkey = queryParams[APIKEY];
                }
            }

            // decode just in case the key was url encoded decode it
            if (strkey != null)
            {
                strkey = System.Web.HttpUtility.UrlDecode(strkey);
            }

            return strkey;

        }

        private static void CreateErrorReply(OperationContext operationContext, string key)
        {
            // The error message is padded so that IE shows the response by default
            using (var sr = new StringReader("<?xml version=\"1.0\" encoding=\"utf-8\"?>" + APIErrorHTML))
            {
                XElement response = XElement.Load(sr);
                using (Message reply = Message.CreateMessage(MessageVersion.None, null, response))
                {
                    HttpResponseMessageProperty responseProp = new HttpResponseMessageProperty() { StatusCode = HttpStatusCode.Unauthorized, StatusDescription = String.Format("'{0}' is an invalid API key", key) };
                    responseProp.Headers[HttpResponseHeader.ContentType] = "text/html";
                    reply.Properties[HttpResponseMessageProperty.Name] = responseProp;
                    operationContext.RequestContext.Reply(reply);
                    // set the request context to null to terminate processing of this request
                    operationContext.RequestContext = null;
                }
            }
        }

        const string APIKEY = "APIKey";
        const string APIErrorHTML = @"
<html>
<head>
    <title>Request Error - No API Key</title>
    <style type=""text/css"">
        body
        {
            font-family: Verdana;
            font-size: large;
        }
    </style>
</head>
<body>
    <h1>
        Request Error
    </h1>
    <p>
        This key is not authorized to access Veracity TouchScreener services.
    </p>
</body>
</html>
";
    }
}