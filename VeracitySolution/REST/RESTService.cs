﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Web.Services.Description;
using System.ServiceModel.Channels;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using System.Collections.Specialized;
using System.Runtime.Serialization.Json;
using Veracity.DataAccessLayer.Data;
using Veracity.BusinessFacadeLayer;

using REST.Resources;

namespace REST
{
    // Start the service and browse to http://<machine_name>:<port>/Service1/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public class RESTService
    {
        private BusFacCore fac = new BusFacCore();
        private Apilog apilog = null;
        private string _strStatus = null;
        private string _strStatusDesc = null;

        private const string APIKEY_PARAM = @"apikey";
        private const string AUTHKEY_PARAM = @"authkey";

        private const string TEST_SESSION_PARAM = @"testsession";

        private const string RESPONSE_FORMAT_PARAM = @"responseformat";

        [WebGet(UriTemplate = "/")]
        public void ReturnValidKey()
        {
            Apikey apikey = null;
            Client client = null;
            string api_key = getParamValue(WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters, APIKEY_PARAM);
            string auth_key = getParamValue(WebOperationContext.Current.IncomingRequest.UriTemplateMatch.QueryParameters, AUTHKEY_PARAM);

            if (isAuth(api_key, auth_key, ref apikey, ref client))
            {
                CreateValidReply(apikey, client);
            }
            else
            {
                CreateInvalidReply();
            }
        }


        //************************************************************************
        // Testsession management REST calls
        //************************************************************************
        [WebInvoke(UriTemplate = "/testsessions",
        Method = "POST",
        BodyStyle = WebMessageBodyStyle.Bare)]
        public TestSessionProxy TestSessionCreate(Stream streamData)
        {
            TestSessionProxy o = null;
            string strWebData = new StreamReader(streamData).ReadToEnd();
            o = ServiceTestSessionReplyPost(1, strWebData);
            return o;
        }

        [WebInvoke(UriTemplate = "/testsessions?testsessionid={testsessionidentifier}",
        Method = "GET",
        BodyStyle = WebMessageBodyStyle.Bare)]
        public TestSessionProxy TestSessionGet(string testsessionidentifier)
        {
            TestSessionProxy o = null;
            o = ServiceTestSessionReplyGet(WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.Query, testsessionidentifier);
            return o;
        }

        private TestSessionProxy ServiceTestSessionReplyGet( string pStrWebData, string pStrTestSessionIdentifier)
        {
            TestSessionProxy testSessionProxy = null;
            long refnum = 0;
            int trace = 0;
            bool isSuccess = false;
            RESTTestSession resttestsession = null;
            try
            {
                Apikey apikey = null;
                Client client = null;
                if (isAuth(pStrWebData, ref apikey, ref client))
                {
                    resttestsession = new RESTTestSession();
                    resttestsession.Get(pStrTestSessionIdentifier);
                    if (resttestsession.httpStatusCode == HttpStatusCode.OK)
                    {
                        isSuccess = true;
                    }

                    SetResponseReply(resttestsession.httpStatusCode, resttestsession.httpStatusDescription);
                }
                else
                {
                    SetResponseReply(HttpStatusCode.Unauthorized, "Reference ID:  " + Convert.ToString(refnum));
                }

            }
            catch (Exception ex)
            {
                SetResponseReply(HttpStatusCode.BadRequest, null);
            }

            testSessionProxy.Status = _strStatus;
            testSessionProxy.StatusDescription = _strStatusDesc;

            return testSessionProxy;
        }

        private TestSessionProxy ServiceTestSessionReplyPost(int pIntActionToExecute, string pStrWebData)
        {
            TestSessionProxy testSessionProxy = null;
            long refnum = 0;
            int trace = 0;
            bool isSuccess = false;
            RESTTestSession resttestsession = null;
            try
            {
                Apikey apikey = null;
                Client client = null;
                if (isAuth(pStrWebData, ref apikey, ref client))
                {
                    string oData = getParamValue(pStrWebData, TEST_SESSION_PARAM);
                    resttestsession = new RESTTestSession();
                    resttestsession.Serialize(oData);
                    if (resttestsession.httpStatusCode == HttpStatusCode.OK)
                    {
                        isSuccess = true;
                    }

                    SetResponseReply(resttestsession.httpStatusCode, resttestsession.httpStatusDescription);
                }
                else
                {
                    SetResponseReply(HttpStatusCode.Unauthorized, "Reference ID:  " + Convert.ToString(refnum));
                }

            }
            catch (Exception ex)
            {
                SetResponseReply(HttpStatusCode.BadRequest, null);
            }

            testSessionProxy.Status = _strStatus;
            testSessionProxy.StatusDescription = _strStatusDesc;

            return testSessionProxy;
        }


        private void SetResponseReply(HttpStatusCode pHttpStatusCode, string pStatusDesc)
        {
            OutgoingWebResponseContext outgoingResponse = WebOperationContext.Current.OutgoingResponse;
            outgoingResponse.StatusCode = HttpStatusCode.OK;
            string strStatusCode = "OK";
            //string strStatusNum = "200";
            string strStatusDescription = "Success";

            if (pHttpStatusCode != HttpStatusCode.OK)
            {
                strStatusCode = "ERROR";
                //strStatusNum = "400";
            }

            if (pStatusDesc != null)
            {
                strStatusDescription = pStatusDesc;
            }

            _strStatus = strStatusCode;
            _strStatusDesc = strStatusDescription;
         
        }

        private string CreateValidReply(Apikey pApikey, Client pClient)
        {


            string strResponseText = APIValidHTML;
            if (pClient != null)
            {
                if (pClient.ClientID > 0)
                {
                    strResponseText = APIValidHTML.Replace("[[Merchant.MerchantName]]", pClient.VisibleCode);
                }
            }
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = HttpStatusCode.OK;
            response.StatusDescription = "Valid Veracity REST API key.";
            response.ContentType = "text/html";
            HttpContext.Current.Response.Write(strResponseText);
            return null;
        }

        private string CreateInvalidReply()
        {
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            response.StatusCode = HttpStatusCode.OK;
            response.StatusDescription = "Invalid Veracity REST API Authorization key.";
            response.ContentType = "text/html";
            HttpContext.Current.Response.Write(APIInvalidHTML);
            return null;
        }

        private bool isAuth(string pStrRequestData, ref Apikey pRefApikey, ref Client pRefClient)
        {
            bool bReturn = false;
            try
            {
                NameValueCollection queryParams = HttpUtility.ParseQueryString(pStrRequestData);
                string api_key = queryParams[APIKEY_PARAM];
                string auth_key = queryParams[AUTHKEY_PARAM];
                if ((api_key != null) && (auth_key != null))
                {
                    bool bTmp = false;
                    api_key = System.Web.HttpUtility.UrlDecode(api_key);
                    auth_key = System.Web.HttpUtility.UrlDecode(auth_key);

                    bTmp = fac.isAuthAPI(api_key, auth_key, ref pRefApikey);
                    if ((bTmp) && (pRefApikey != null))
                    {
                        if (!((bool)pRefApikey.IsDisabled))
                        {
                            pRefClient = fac.ClientGet(pRefApikey.ClientID);
                            bReturn = true;
                        }
                        else
                        {
                            // this account is disabled
                        }

                    }

                }
            }
            catch (Exception ex)
            {
            }
            return bReturn;
        }

        private bool isAuth(string pStrApiKey, string pStrAuthKey, ref Apikey pRefApikey, ref Client pRefClient)
        {
            bool bReturn = false;
            try
            {
                if ((pStrApiKey != null) && (pStrAuthKey != null))
                {
                    bool bTmp = false;

                    bTmp = fac.isAuthAPI(pStrApiKey, pStrAuthKey, ref pRefApikey);
                    if ((bTmp) && (pRefApikey != null))
                    {
                        if (!((bool)pRefApikey.IsDisabled))
                        {
                            pRefClient = fac.ClientGet(pRefApikey.ClientID);
                            bReturn = true;
                        }
                        else
                        {
                            // this account is disabled
                        }

                    }

                }
            }
            catch (Exception ex)
            {
            }
            return bReturn;
        }


        private string getParamValue(NameValueCollection pQueryParams, string pStrKey)
        {
            string val = null;
            try
            {
                val = System.Web.HttpUtility.UrlDecode(pQueryParams[pStrKey]);
            }
            catch { val = null; }
            return val;
        }

        private string getParamValue(string pStrWebData, string pStrKey)
        {
            string val = null;
            try
            {
                NameValueCollection queryParams = HttpUtility.ParseQueryString(pStrWebData);
                string keyVal = queryParams[pStrKey];
                val = System.Web.HttpUtility.UrlDecode(keyVal);
            }
            catch { val = null; }
            return val;
        }

        private string APIValidHTML = @"
<html>
<head>
    <title>Success - Your Veracity REST API key is valid</title>
    <style type=""text/css"">
        body
        {
            font-family: Verdana;
            font-size: large;
        }
    </style>
</head>
<body>
    <h1>
        Veracity REST API
    </h1>
    <p>
        This Veracity REST API key is valid for merchant [[Merchant.MerchantName]].
    </p>
</body>
</html>
";

        private const string APIInvalidHTML = @"
<html>
<head>
    <title>Authorization Key Failure - Your Veracity REST API authorization key is invalid</title>
    <style type=""text/css"">
        body
        {
            font-family: Verdana;
            font-size: large;
        }
    </style>
</head>
<body>
    <h1>
        Veracity REST API
    </h1>
    <p>
        This Veracity REST API authorization key is invalid.
    </p>
</body>
</html>
";

        //************************************************************************
        // REST calls
        //************************************************************************

        //[OperationContract]
        //[WebGet(UriTemplate = "?url={uri}")]
        //public Stream GetBinary(string uri)
        //{
        //    var binary = new byte[0]; // some sort of query
        //    var mimeType = ""; // some sort of query
        //    WebOperationContext.Current.OutgoingResponse.ContentType = mimeType;
        //    return new MemoryStream(binary);
        //}
    }
}