﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Web.Caching;
using System.ServiceModel;
using System.Xml.Linq;
using System.ServiceModel.Channels;
using System.Collections;

using Veracity.DataAccessLayer.Data;
using Veracity.BusinessFacadeLayer;

namespace REST
{
    public static class APIKeyRepository
    {
        private const string APIKEYLIST = "APIKeyList";

        public static bool IsValidAPIKey(string key)
        {
            // TODO: Implement IsValidAPI Key using your repository

            Guid apiKey;

            // Convert the string into a Guid and validate it
            if (Guid.TryParse(key, out apiKey) && APIKeys.Contains(apiKey))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static List<Guid> APIKeys
        {
            get
            {
                // Get from the cache
                // Could also use AppFabric cache for scalability
                var keys = HttpContext.Current.Cache[APIKEYLIST] as List<Guid>;

                if ( !((keys != null) && (keys.Count > 0)))
                    keys = PopulateAPIKeys();

                return keys;
            }
        }

        private static List<Guid> PopulateAPIKeys()
        {
            List<Guid> keyList = new List<Guid>();
            Guid guid;
            bool bGuid = false;
            int nDtCompare = 0;
            DateTime dtNull = new DateTime();

            ArrayList arApikeys = null;
            Apikey apikey = null;
            BusFacCore busFacCore = new BusFacCore();
            busFacCore.ApikeysGet(ref arApikeys);
            if ((arApikeys != null) && (arApikeys.Count > 0))
            {
                for (int i = 0; i < arApikeys.Count; i++)
                {
                    apikey = (Apikey)arApikeys[i];
                    bGuid = Guid.TryParse(apikey.GuidStr, out guid);
                    if (bGuid)
                    {
                        if (!dtNull.Equals(apikey.DateExpiration))
                        {
                            nDtCompare = DateTime.Compare(apikey.DateExpiration, DateTime.Now);
                            if (nDtCompare < 0)
                            {
                                // has expired.  No need add
                                continue;
                            }

                        }
                        keyList.Add(guid);
                    }
                }
            }
           

            // Save it in the cache
            // Could be saved in AppFabric Cache for scalability across a farm
            System.Web.HttpRuntime.Cache.Insert(
              APIKEYLIST, keyList, null, cacheExpirationDateTime,
              Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, null);

            return keyList;
        }

        private static DateTime cacheExpirationDateTime
        {
            get
            {
                return DateTime.Now.AddMinutes(60);
            }
        }
    }
}