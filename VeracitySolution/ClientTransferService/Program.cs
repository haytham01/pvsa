﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace ClientTransferService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

//#if (DEBUG)
//            mainService srv = new mainService();
//            srv.StartWork();
//#else
//            ServiceBase[] ServicesToRun;
//            ServicesToRun = new ServiceBase[] 
//            { 
//                new mainService() 
//            };
//            Config config = new Config();
//            ServicesToRun[0].ServiceName = config.ServiceName;
//            ServiceBase.Run(ServicesToRun);
//#endif
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new mainService() 
            };
            Config config = new Config();
            ServicesToRun[0].ServiceName = config.ServiceName;
            ServiceBase.Run(ServicesToRun);
        }
    }
}
