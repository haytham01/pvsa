﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;

using Veracity.Client.Proxy;

namespace ClientTransferService
{
    public partial class mainService : ServiceBase
    {
        private volatile bool _bStop = false;
        private static int _nCheckIntervalInMs = 5000;
        private static Thread _thrdMain = null;

        public mainService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _bStop = false;

            _thrdMain = new Thread(new ThreadStart(this.StartWork));
            _thrdMain.Start();
        }

        protected override void OnStop()
        {
            _bStop = true;

            try
            {
                _thrdMain.Abort();
                _thrdMain.Join();
            }
            catch { }
        }

        public void StartWork()
        {
            Config config = new Config();
            _bStop = false;
            _nCheckIntervalInMs = config.CheckIntervalInMs;
            CreateDirs(config);
            while (!_bStop)
            {
                doProcessing(config);
                Thread.Sleep(_nCheckIntervalInMs);
            }
        }

        public void CreateDirs(Config pConfig)
        {
            if (!Directory.Exists(pConfig.ArchiveDirPath))
            {
                DirectoryInfo dr = Directory.CreateDirectory(pConfig.ArchiveDirPath);
            }
            if (!Directory.Exists(pConfig.UnknownDirPath))
            {
                DirectoryInfo dr = Directory.CreateDirectory(pConfig.UnknownDirPath);
            }

        }

        List<string> GetUploadFiles(string path, string filter)
        {
            List<string> uploadFiles = new List<string>();
            DirectoryInfo di = new DirectoryInfo(path);

            string[] filters = filter.Split(new char[] { ',', ';' });

            FileInfo[] fileInfos = filters
                .SelectMany(i => di.GetFiles(i, SearchOption.TopDirectoryOnly))
                .Distinct().ToArray();

            foreach (FileInfo fi in fileInfos)
            {
                uploadFiles.Add(fi.FullName);
            }

            return uploadFiles;
        }

        private void doProcessing(Config pConfig)
        {
            try
            {
                RESTHelper resthelper = null;
                string unknownFilePath = null;
                string archiveFilePath = null;
                int delayIntervalInMinutes = 1;
                List<string> fileNames = GetUploadFiles(pConfig.DataDirPath, pConfig.UploadFilter);
                string logFile = pConfig.DataDirPath + System.IO.Path.DirectorySeparatorChar + "transfer.log";

                if (!File.Exists(logFile))
                {
                    System.IO.File.WriteAllText(logFile, "Data folder " + pConfig.DataDirPath + Environment.NewLine);
                }

                DateTime dtNow = DateTime.Now;

                foreach (string fileName in fileNames)
                {
                    try
                    {
                        if (delayIntervalInMinutes > 0)
                        {
                            DateTime creationTime = File.GetCreationTime(fileName);
                            TimeSpan ts = dtNow - creationTime;
                            if (ts.TotalMinutes < delayIntervalInMinutes)
                            {
                                continue;
                            }
                        }
                        
                        string data = System.IO.File.ReadAllText(fileName);
                        resthelper = new RESTHelper(pConfig.ServiceBaseURL, pConfig.APIKey, pConfig.AuthKey);
                        resthelper.SendPostData(data);
                        if (!resthelper.HasError)
                        {
                            File.Delete(fileName);

                            System.IO.File.WriteAllText(logFile, "Successfully Transferred File:  " + fileName + Environment.NewLine);
                            //filename = System.IO.Path.GetFileName(fileNames[i]);
                            //archiveFilePath = pConfig.ArchiveDirPath + System.IO.Path.DirectorySeparatorChar + filename;
                            //File.Copy(fileNames[i], archiveFilePath, true);
                            //if (File.Exists(archiveFilePath))
                            //{
                            //    File.Delete(fileNames[i]);
                            //}
                        }
                        else
                        {
                            // error happened
                            System.IO.File.WriteAllText(logFile, "REST Error in Transferring File:  " + fileName + Environment.NewLine);
                        }
                    }
                    catch (Exception ex2) 
                    {
                        System.IO.File.WriteAllText(logFile, "Exception on File:  " + fileName + Environment.NewLine);
                        System.IO.File.WriteAllText(logFile, "Exception:  " + ex2.StackTrace + Environment.NewLine);
                    }

                }

                // Move suspicious files?

                  //}
                  //          else
                  //          {
                  //              // suspicious files,  Transfer to unknown
                  //              filename = System.IO.Path.GetFileName(fileNames[i]);
                  //              unknownFilePath = pConfig.UnknownDirPath + System.IO.Path.DirectorySeparatorChar + filename;
                  //              File.Copy(fileNames[i], unknownFilePath, true);
                  //              if (File.Exists(unknownFilePath))
                  //              {
                  //                  File.Delete(fileNames[i]);
                  //              }
                  //          }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
