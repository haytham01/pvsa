﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ClientTransferService
{
    public class Config
    {
        private long _lClientID = 0;
        private int _nCheckIntervalInMs = 5000;
        private string _strServiceName = null;
        private string _strDataDirPath = null;
        private string _strArchiveDirPath = null;
        private string _strUnknownDirPath = null;
        private string _strServiceBaseURL = null;
        private string _strAPIKey = null;
        private string _strAuthKey = null;
        private string _strUploadFilter = null;

        private static readonly string KEY_SERVICE_NAME = "ServiceName";
        private static readonly string KEY_CLIENT_ID = "ClientID";
        private static readonly string KEY_CHECK_INTERVAL_IN_MS = "CheckIntervalInMs";
        private static readonly string KEY_DATA_DIR_PATH = "DataDirPath";
        private static readonly string KEY_ARCHIVE_DIR_PATH = "ArchiveDirPath";
        private static readonly string KEY_UNKNOWN_DIR_PATH = "UnknownDirPath";
        private static readonly string KEY_SERVICE_BASE_URL = "ServiceBaseURL";
        private static readonly string KEY_API_KEY = "APIKey";
        private static readonly string KEY_AUTH_KEY = "AuthKey";
        private static readonly string KEY_UPLOAD_FILTER = "UploadFilter";

        public string UnknownDirPath
        {
            get { return _strUnknownDirPath; }
            set { _strUnknownDirPath = value; }
        }
        public string ArchiveDirPath
        {
            get { return _strArchiveDirPath; }
            set { _strArchiveDirPath = value; }
        }
        public string DataDirPath
        {
            get { return _strDataDirPath; }
            set { _strDataDirPath = value; }
        }
        public string ServiceBaseURL
        {
            get { return _strServiceBaseURL; }
            set { _strServiceBaseURL = value; }
        }
        public string APIKey
        {
            get { return _strAPIKey; }
            set { _strAPIKey = value; }
        }
        public string AuthKey
        {
            get { return _strAuthKey; }
            set { _strAuthKey = value; }
        }
        public long ClientID
        {
            get { return _lClientID; }
            set { _lClientID = value; }
        }
        public string ServiceName
        {
            get { return _strServiceName; }
            set { _strServiceName = value; }
        }
        public int CheckIntervalInMs
        {
            get { return _nCheckIntervalInMs; }
            set { _nCheckIntervalInMs = value; }
        }
        public string UploadFilter
        {
            get { return _strUploadFilter; }
            set { _strUploadFilter = value; }
        }
        public Config()
        {
            AppSettingsReader configurationAppSettings = null;
            configurationAppSettings = new AppSettingsReader();

            try
            {
                _lClientID = (long)Convert.ToInt32((string)configurationAppSettings.GetValue(KEY_CLIENT_ID, typeof(System.String)));
            }
            catch
            {
                _lClientID = 0;
            }

            try
            {
                _nCheckIntervalInMs = Convert.ToInt32((string)configurationAppSettings.GetValue(KEY_CHECK_INTERVAL_IN_MS, typeof(System.String)));
            }
            catch
            {
                _nCheckIntervalInMs = 5000;
            }
            try
            {
                _strServiceName = (string)configurationAppSettings.GetValue(KEY_SERVICE_NAME, typeof(System.String));
            }
            catch
            {
                _strServiceName = null;
            }

            try
            {
                _strDataDirPath = (string)configurationAppSettings.GetValue(KEY_DATA_DIR_PATH, typeof(System.String));
            }
            catch
            {
                _strDataDirPath = null;
            }

            try
            {
                _strArchiveDirPath = (string)configurationAppSettings.GetValue(KEY_ARCHIVE_DIR_PATH, typeof(System.String));
            }
            catch
            {
                _strArchiveDirPath = null;
            }

            try
            {
                _strUnknownDirPath = (string)configurationAppSettings.GetValue(KEY_UNKNOWN_DIR_PATH, typeof(System.String));
            }
            catch
            {
                _strUnknownDirPath = null;
            }

            try
            {
                _strServiceBaseURL = (string)configurationAppSettings.GetValue(KEY_SERVICE_BASE_URL, typeof(System.String));
            }
            catch
            {
                _strServiceBaseURL = null;
            }

            try
            {
                _strAPIKey = (string)configurationAppSettings.GetValue(KEY_API_KEY, typeof(System.String));
            }
            catch
            {
                _strAPIKey = null;
            }

            try
            {
                _strAuthKey = (string)configurationAppSettings.GetValue(KEY_AUTH_KEY, typeof(System.String));
            }
            catch
            {
                _strAuthKey = null;
            }

            try
            {
                _strUploadFilter = (string)configurationAppSettings.GetValue(KEY_UPLOAD_FILTER, typeof(System.String));
            }
            catch
            {
                _strUploadFilter = null;
            }
        }

    }
}
