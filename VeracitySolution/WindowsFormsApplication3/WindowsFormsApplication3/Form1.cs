﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Runtime.InteropServices;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {

        [DllImport("gdi32.dll")]
        public static extern IntPtr CreateDC(
            string strDriver,
            string strDevice,
            string strOutput,
            IntPtr pData);

        [DllImport("gdi32.dll")]
        public static extern int GetPixel(
            IntPtr hdc,
            int nXPos,
            int nYPos);

        private IntPtr hdcScreen;
        private System.Timers.Timer mytimer;
        private System.Windows.Forms.Label label1;


        public Form1()
        {
            InitializeComponent();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();

            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35,13);
            this.label1.TabIndex = 0;
            this.label1.Text = "STARTING";

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(100, 50);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

            hdcScreen = CreateDC("Display", null, null, IntPtr.Zero);
            mytimer = new System.Timers.Timer(10);
            mytimer.Elapsed += new System.Timers.ElapsedEventHandler(mytimer_Elapsed);
            mytimer.Interval = 2000;
            mytimer.Enabled = true;
            Console.WriteLine("Press the enter key");
            Console.ReadLine();
            mytimer.Start();

        }

        private void mytimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Console.WriteLine("ELAPSED");
            
            //bel1.Text = GetPixel(hdcScreen, Cursor.Position.X, Cursor.Position.Y).ToString();
            //MessageBox.Show(GetPixel(hdcScreen, Cursor.Position.X, Cursor.Position.Y).ToString());

            Console.WriteLine(GetPixel(hdcScreen, Cursor.Position.X, Cursor.Position.Y).ToString());
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


    }
}
