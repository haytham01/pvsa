﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace TouchTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Stopwatch _ansTimeStopWatch = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
            //AddHandler(FrameworkElement.MouseDownEvent, new
            //    MouseButtonEventHandler(mainButton_MouseDown), true);
        }

        
        private void mainButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.mainTextBox.AppendText("Mouse down detected\n");
        }

        private void mainButton_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            _ansTimeStopWatch.Start();
            this.mainTextBox.AppendText("Preview mouse down detected\n");
        }

        private void mainButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _ansTimeStopWatch.Stop();
            this.mainTextBox.AppendText("Mouse up detected\n");
        }

        private void mainButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            this.mainTextBox.AppendText("Preview mouse up detected\n");
            doMouseUp();
        }

        private void doMouseUp()
        {
            double DeltaClickTimeInSeconds = _ansTimeStopWatch.Elapsed.TotalSeconds;
            _ansTimeStopWatch.Reset();
            this.mainTextBox.AppendText("Click time in sec:  " + DeltaClickTimeInSeconds + "\n");
            this.mainTextBox.AppendText("--\n");

        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainTextBox.Clear();
        }

        private void mainButton_TouchDown(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Touch down detected\n");
        }

        private void mainButton_TouchEnter(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Touch enter detected\n");
        }

        private void mainButton_TouchLeave(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Touch leave detected\n");
        }

        private void mainButton_TouchUp(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Touch up detected\n");
        }

        private void mainButton_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Preview Touch down detected\n");
        }

        private void mainButton_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Preview touch up detected\n");
        }

        private void mainButton_MouseEnter(object sender, MouseEventArgs e)
        {
            //this.mainTextBox.AppendText("Mouse enter detected\n");
        }

        private void mainButton_KeyUp(object sender, KeyEventArgs e)
        {
            //this.mainTextBox.AppendText("Key up detected\n");
        }

        private void mainButton_KeyDown(object sender, KeyEventArgs e)
        {
            //this.mainTextBox.AppendText("Key down detected\n");
        }

        private void mainButton_Click(object sender, RoutedEventArgs e)
        {
            //this.mainTextBox.AppendText("Click detected\n");
        }

        private void mainButton_StylusEnter(object sender, StylusEventArgs e)
        {
            //this.mainTextBox.AppendText("Stylus enter detected\n");
        }

        private void mainButton_GotTouchCapture(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Got touch capture detected\n");
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //this.mainTextBox.AppendText("Window mouse down detected\n");
        }

        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            //this.mainTextBox.AppendText("Window mouse leave detected\n");
        }

        private void Window_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            //this.mainTextBox.AppendText("Window preview mouse down detected\n");
        }

        private void Window_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            //this.mainTextBox.AppendText("Window preview mouse up detected\n");
        }

        private void Window_TouchDown(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Window touch down detected\n");
        }

        private void Window_TouchLeave(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Window touch leave detected\n");
        }

        private void Window_TouchUp(object sender, TouchEventArgs e)
        {
            //this.mainTextBox.AppendText("Window touch up detected\n");
        }
    }
}
