﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using System.Xml;

namespace RESTWinTester
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            executeAction(1);
        }

        private void executeAction(int pIntAction)
        {
            HttpWebRequest request = null;
            try
            {
                string url = this.urlTextBox.Text.Trim();
                StringBuilder parameters = null;
                EncodeBaseItems(ref parameters);

                switch (pIntAction)
                {
                    case 1:
                        // post
                        EncodeTestSessionItems(ref parameters);
                        url = url + "/testsessions";
                        request = createTestSessionRequest(url, parameters.ToString(), "POST");
                        break;
                    case 2:
                        // validate credentials
                        EncodeAndAddItem(ref parameters, "testsessionid", "1");
                        url = url + "/?" + parameters.ToString();
                        Uri uri = new Uri(url);
                        request = (HttpWebRequest)WebRequest.Create(uri);
                        //url = "http://localhost:6214/RESTService/?apikey=9edeac9c-4172-4f6d-bbec-c010136c168d&authkey=a1&responseformat=json&testsessionid=1";
                        //url = "http://api.treewards.com/RESTRewardsService?apikey=9edeac9c-4172-4f6d-bbec-c010136c168d&authkey=a1";
                        //request.ContentType = "text/html";
                        //request.KeepAlive = false;
                        //request.Method = "GET";
                        //request.ProtocolVersion = HttpVersion.Version10;
                        //System.Net.ServicePointManager.Expect100Continue = false;
                        //request.Credentials = CredentialCache.DefaultCredentials;
                        break;
                    default:
                        break;
                }
                request.Timeout = 1000000000;
                request.KeepAlive = false;
                WebResponse ws = request.GetResponse();

                // Get the response stream  
                StreamReader reader = new StreamReader(ws.GetResponseStream());

                // Read the whole contents and return as a string  
                string text = reader.ReadToEnd();
                this.responseTextBox.Text = text;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                this.statusCodeTextBox.Text = response.StatusCode.ToString();
                this.responseDescTextBox.Text = response.StatusDescription;

            }
            catch (Exception ex)
            {
                this.responseTextBox.Text = "Bad Request";
                this.statusCodeTextBox.Text = String.Empty;
                this.responseDescTextBox.Text = String.Empty;

                this.responseTextBox.Text = ex.ToString() + Environment.NewLine + ex.StackTrace;
            }
        }
        private void EncodeBaseItems(ref StringBuilder baseRequest)
        {
            EncodeAndAddItem(ref baseRequest, "apikey", this.apiKeyTextBox.Text.Trim());
            EncodeAndAddItem(ref baseRequest, "authkey", this.authKeyTextBox.Text.Trim());
            EncodeAndAddItem(ref baseRequest, "responseformat", "json");

        }

        /// <summary>
        /// Encodes an item and ads it to the string.
        /// </summary>
        /// <param name="baseRequest">The previously encoded data.</param>
        /// <param name="dataItem">The data to encode.</param>
        /// <returns>A string containing the old data and the previously encoded data.</returns>
        private void EncodeAndAddItem(ref StringBuilder baseRequest, string key, string dataItem)
        {
            if (baseRequest == null)
            {
                baseRequest = new StringBuilder();
            }
            if (baseRequest.Length != 0)
            {
                baseRequest.Append("&");
            }

            baseRequest.Append(key);
            baseRequest.Append("=");
            baseRequest.Append(System.Web.HttpUtility.UrlEncode(dataItem));
        }

        private void EncodeTestSessionItems(ref StringBuilder baseRequest)
        {
            string objectData = this.dataTextBox.Text;
            EncodeAndAddItem(ref baseRequest, "testsession", objectData);
        }

        private HttpWebRequest createTestSessionRequest(string url, string postData, string method)
        {
            HttpWebRequest request = null;
            Uri uri = new Uri(url);
            request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = method;
            request.ContentType = "application/text";
            request.ContentLength = postData.Length;
            using (Stream writeStream = request.GetRequestStream())
            {
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] bytes = encoding.GetBytes(postData);
                writeStream.Write(bytes, 0, bytes.Length);
            }

            return request;
        }

        private void validateCredentialsButton_Click(object sender, EventArgs e)
        {
            executeAction(2);
        }

        private void clearPostDataButton_Click(object sender, EventArgs e)
        {
            this.dataTextBox.Text = string.Empty;
        }

    }
}
