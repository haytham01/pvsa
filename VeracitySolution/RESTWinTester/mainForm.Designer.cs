﻿namespace RESTWinTester
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.urlTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.authKeyTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.apiKeyTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dataTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.responseDescTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.requestBodyTextBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.statusCodeTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.responseTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.submitButton = new System.Windows.Forms.Button();
            this.validateCredentialsButton = new System.Windows.Forms.Button();
            this.clearPostDataButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // urlTextBox
            // 
            this.urlTextBox.Location = new System.Drawing.Point(98, 12);
            this.urlTextBox.Name = "urlTextBox";
            this.urlTextBox.Size = new System.Drawing.Size(556, 20);
            this.urlTextBox.TabIndex = 3;
            this.urlTextBox.Text = "http://localhost:6214/RESTService";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Base URL";
            // 
            // authKeyTextBox
            // 
            this.authKeyTextBox.Location = new System.Drawing.Point(98, 64);
            this.authKeyTextBox.Name = "authKeyTextBox";
            this.authKeyTextBox.Size = new System.Drawing.Size(556, 20);
            this.authKeyTextBox.TabIndex = 16;
            this.authKeyTextBox.Text = "a1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Auth Key";
            // 
            // apiKeyTextBox
            // 
            this.apiKeyTextBox.Location = new System.Drawing.Point(98, 38);
            this.apiKeyTextBox.Name = "apiKeyTextBox";
            this.apiKeyTextBox.Size = new System.Drawing.Size(556, 20);
            this.apiKeyTextBox.TabIndex = 14;
            this.apiKeyTextBox.Text = "9edeac9c-4172-4f6d-bbec-c010136c168d";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "API Key";
            // 
            // dataTextBox
            // 
            this.dataTextBox.AcceptsReturn = true;
            this.dataTextBox.Location = new System.Drawing.Point(98, 90);
            this.dataTextBox.Multiline = true;
            this.dataTextBox.Name = "dataTextBox";
            this.dataTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataTextBox.Size = new System.Drawing.Size(556, 198);
            this.dataTextBox.TabIndex = 18;
            this.dataTextBox.Text = resources.GetString("dataTextBox.Text");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Data";
            // 
            // responseDescTextBox
            // 
            this.responseDescTextBox.Location = new System.Drawing.Point(98, 547);
            this.responseDescTextBox.Multiline = true;
            this.responseDescTextBox.Name = "responseDescTextBox";
            this.responseDescTextBox.Size = new System.Drawing.Size(539, 40);
            this.responseDescTextBox.TabIndex = 37;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(7, 550);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 37);
            this.label11.TabIndex = 36;
            this.label11.Text = "Response Description";
            // 
            // requestBodyTextBox
            // 
            this.requestBodyTextBox.Location = new System.Drawing.Point(98, 291);
            this.requestBodyTextBox.Multiline = true;
            this.requestBodyTextBox.Name = "requestBodyTextBox";
            this.requestBodyTextBox.Size = new System.Drawing.Size(539, 108);
            this.requestBodyTextBox.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(7, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 39);
            this.label10.TabIndex = 34;
            this.label10.Text = "Request Body";
            // 
            // statusCodeTextBox
            // 
            this.statusCodeTextBox.Location = new System.Drawing.Point(98, 521);
            this.statusCodeTextBox.Name = "statusCodeTextBox";
            this.statusCodeTextBox.Size = new System.Drawing.Size(166, 20);
            this.statusCodeTextBox.TabIndex = 33;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 524);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Response Code:";
            // 
            // responseTextBox
            // 
            this.responseTextBox.Location = new System.Drawing.Point(98, 405);
            this.responseTextBox.Multiline = true;
            this.responseTextBox.Name = "responseTextBox";
            this.responseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.responseTextBox.Size = new System.Drawing.Size(539, 110);
            this.responseTextBox.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 405);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Response Body";
            // 
            // submitButton
            // 
            this.submitButton.Location = new System.Drawing.Point(562, 593);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(75, 23);
            this.submitButton.TabIndex = 29;
            this.submitButton.Text = "Go";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // validateCredentialsButton
            // 
            this.validateCredentialsButton.Location = new System.Drawing.Point(98, 593);
            this.validateCredentialsButton.Name = "validateCredentialsButton";
            this.validateCredentialsButton.Size = new System.Drawing.Size(183, 23);
            this.validateCredentialsButton.TabIndex = 38;
            this.validateCredentialsButton.Text = "Validate Credentials";
            this.validateCredentialsButton.UseVisualStyleBackColor = true;
            this.validateCredentialsButton.Click += new System.EventHandler(this.validateCredentialsButton_Click);
            // 
            // clearPostDataButton
            // 
            this.clearPostDataButton.Location = new System.Drawing.Point(296, 593);
            this.clearPostDataButton.Name = "clearPostDataButton";
            this.clearPostDataButton.Size = new System.Drawing.Size(183, 23);
            this.clearPostDataButton.TabIndex = 39;
            this.clearPostDataButton.Text = "Clear Post Data";
            this.clearPostDataButton.UseVisualStyleBackColor = true;
            this.clearPostDataButton.Click += new System.EventHandler(this.clearPostDataButton_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 639);
            this.Controls.Add(this.clearPostDataButton);
            this.Controls.Add(this.validateCredentialsButton);
            this.Controls.Add(this.responseDescTextBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.requestBodyTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.statusCodeTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.responseTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.dataTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.authKeyTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.apiKeyTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.urlTextBox);
            this.Controls.Add(this.label1);
            this.Name = "mainForm";
            this.Text = "REST Windows Client Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox urlTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox authKeyTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox apiKeyTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox dataTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox responseDescTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox requestBodyTextBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox statusCodeTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox responseTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Button validateCredentialsButton;
        private System.Windows.Forms.Button clearPostDataButton;
    }
}

