/// <summary>
/// Copyright (c) 2013 Veracity.  San Diego, California, USA
/// All Rights Reserved
/// 
/// File:  Config.cs
/// History
/// ----------------------------------------------------
/// 001	HA	3/30/2013	Created
/// 
/// </summary>
/// 

using System;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Veracity.Common
{
	/// <summary>
	/// Copyright (c) 2007 Haytham Allos.  San Diego, California, USA
	/// All Rights Reserved
	/// 
	/// File:  ErrorCode.cs
	/// History
	/// ----------------------------------------------------
	/// 001   HA        6/19/2007          Created
	/// 
	/// ----------------------------------------------------
	/// Abstracts the ErrorCode database table.
	/// </summary>
	public class ErrorCode
	{
		//Attributes
		/// <summary>ErrorCodeID Attribute</summary>
		private long _lErrorCodeID = 0;
		/// <summary>Code Attribute</summary>
		private string _strCode = null;
		/// <summary>Description Attribute</summary>
		private string _strDescription = null;
		/// <summary>VisibleCode Attribute</summary>
		private string _strVisibleCode = null;
		/// <summary>Trace Attribute</summary>
		private string _strTrace = null;

		private Config _config = null;
		private ErrorCode _errorCode = null;
		private Logger _oLog = null;
		 
		private string _strLognameText = "DataAccessLayer-Data-ErrorCode";
		private bool _hasError = false;
        private string _errorMessage = null;
        private string _errorStacktrace = null;

		/// <summary>HasError Property in class ErrorCode and is of type bool</summary>
		public static readonly string ENTITY_NAME = "ErrorCode"; //Table name to abstract

		// DB Field names
		/// <summary>ID Database field</summary>
		public static readonly string DB_FIELD_ID = "error_code_id"; //Table id field name
		/// <summary>code Database field </summary>
		public static readonly string DB_FIELD_CODE = "code"; //Table Code field name
		/// <summary>description Database field </summary>
		public static readonly string DB_FIELD_DESCRIPTION = "description"; //Table Description field name
		/// <summary>visible_code Database field </summary>
		public static readonly string DB_FIELD_VISIBLE_CODE = "visible_code"; //Table VisibleCode field name
		/// <summary>trace Database field </summary>
		public static readonly string DB_FIELD_TRACE = "trace"; //Table Trace field name

		// Attribute variables
		public static readonly string TAG_ID = "ErrorCodeID"; //Attribute id  name
		public static readonly string TAG_CODE = "Code"; //Table Code field name
		public static readonly string TAG_DESCRIPTION = "Description"; //Table Description field name
		public static readonly string TAG_VISIBLE_CODE = "VisibleCode"; //Table VisibleCode field name
		public static readonly string TAG_TRACE = "Trace"; //Table Trace field name

		// Stored procedure names
		private static readonly string SP_INSERT_NAME = "spErrorCodeInsert"; //Insert sp name
		private static readonly string SP_UPDATE_NAME = "spErrorCodeUpdate"; //Update sp name
		private static readonly string SP_DELETE_NAME = "spErrorCodeDelete"; //Delete sp name
		private static readonly string SP_LOAD_NAME = "spErrorCodeLoad"; //Load sp name
		private static readonly string SP_EXIST_NAME = "spErrorCodeExist"; //Exist sp name

		//properties
        /// <summary>
        /// Gets the error stacktrace.
        /// </summary>
        /// <value>
        /// The error stacktrace.
        /// </value>
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }
        /// <summary>ErrorCodeID is a Property in the ErrorCode Class of type long</summary>
		public long ErrorCodeID 
		{
			get{return _lErrorCodeID;}
			set{_lErrorCodeID = value;}
		}
		/// <summary>Code is a Property in the ErrorCode Class of type String</summary>
		public string Code 
		{
			get{return _strCode;}
			set{_strCode = value;}
		}
		/// <summary>Description is a Property in the ErrorCode Class of type String</summary>
		public string Description 
		{
			get{return _strDescription;}
			set{_strDescription = value;}
		}
		/// <summary>VisibleCode is a Property in the ErrorCode Class of type String</summary>
		public string VisibleCode 
		{
			get{return _strVisibleCode;}
			set{_strVisibleCode = value;}
		}
		/// <summary>Trace is a Property in the ErrorCode Class of type String</summary>
		public string Trace 
		{
			get{return _strTrace;}
			set{_strTrace = value;}
		}
		/// <summary>HasError Property in class ErrorCode and is of type bool</summary>
		public  bool HasError 
		{
			get{return _hasError;}
		}
		/// <summary>Error Property in class ErrorCode and is of type ErrorCode</summary>
		public ErrorCode Error 
		{
			get{return _errorCode;}
		}

		//Constructors
		/// <summary>ErrorCode empty constructor</summary>
		public ErrorCode()
		{
		}
		/// <summary>ErrorCode constructor takes a Config</summary>
		public ErrorCode(Config pConfig)
		{
			_config = pConfig;
_oLog = new Logger(_strLognameText);
 
		}
		/// <summary>ErrorCode constructor takes ErrorCodeID and a SqlConnection</summary>
		public ErrorCode(long l, SqlConnection conn) 
		{
			ErrorCodeID = l;
			try
			{
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
 
_log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}

		}
		/// <summary>ErrorCode Constructor takes pStrData and Config</summary>
		public ErrorCode(string pStrData, Config pConfig)
		{
			_config = pConfig;
_oLog = new Logger(_strLognameText);
 
			Parse(pStrData);
		}
		/// <summary>ErrorCode Constructor takes SqlDataReader</summary>
		public ErrorCode(SqlDataReader rd)
		{
			sqlParseResultSet(rd);
		}
		/// <summary>
		///     Dispose of this object's resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(true); // as a service to those who might inherit from us
		}
		/// <summary>
		///                     Free the instance variables of this object.
		/// </summary>
		protected virtual void Dispose(bool disposing)
		{
			if (! disposing)
				return; // we're being collected, so let the GC take care of this object
		}

		// public methods
		/// <summary>ToString is overridden to display all properties of the ErrorCode Class</summary>
		public override string ToString() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();   
			sbReturn.Append(TAG_ID + ":  " + ErrorCodeID.ToString() + "\n");
			sbReturn.Append(TAG_CODE + ":  " + Code + "\n");
			sbReturn.Append(TAG_DESCRIPTION + ":  " + Description + "\n");
			sbReturn.Append(TAG_VISIBLE_CODE + ":  " + VisibleCode + "\n");
			sbReturn.Append(TAG_TRACE + ":  " + Trace + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Creates well formatted XML - includes all properties of ErrorCode</summary>
		public string ToXml() 
		{
			StringBuilder sbReturn = null;

			sbReturn = new StringBuilder();   
			sbReturn.Append("<ErrorCode>\n");
			sbReturn.Append("<" + TAG_ID + ">" + ErrorCodeID + "</" + TAG_ID + ">\n");
			sbReturn.Append("<" + TAG_CODE + ">" + Code + "</" + TAG_CODE + ">\n");
			sbReturn.Append("<" + TAG_DESCRIPTION + ">" + Description + "</" + TAG_DESCRIPTION + ">\n");
			sbReturn.Append("<" + TAG_VISIBLE_CODE + ">" + VisibleCode + "</" + TAG_VISIBLE_CODE + ">\n");
			sbReturn.Append("<" + TAG_TRACE + ">" + Trace + "</" + TAG_TRACE + ">\n");
			sbReturn.Append("</ErrorCode>" + "\n");

			return sbReturn.ToString();
		}
		/// <summary>Parse accepts a string in XML format and parses values</summary>
		public void Parse(string pStrXml)
		{
			try
			{
				XmlDocument xmlDoc = null;
				string strXPath = null;
				XmlNodeList xNodes = null;

				xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(pStrXml);

				// get the element
				strXPath = "//" + ENTITY_NAME;
				xNodes = xmlDoc.SelectNodes(strXPath);
				foreach (XmlNode xNode in xNodes)
				{
					Parse(xNode);
				}
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}
		}                       
		/// <summary>Parse accepts an XmlNode and parses values</summary>
		public void Parse(XmlNode xNode)
		{
			XmlNode xResultNode = null;
			string strTmp = null;

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_ID);
				strTmp = xResultNode.InnerText;
				ErrorCodeID = (long) Convert.ToInt32(strTmp);
			}
			catch  
			{
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_CODE);
				Code = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_DESCRIPTION);
				Description = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_VISIBLE_CODE);
				VisibleCode = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}

			try
			{
				xResultNode = xNode.SelectSingleNode(TAG_TRACE);
				Trace = xResultNode.InnerText;
			}
			catch  
			{
				xResultNode = null;
			}
		}
		/// <summary>Load</summary>
		public void Load(SqlConnection conn)
		{
			try
			{
				_log("LOAD", ToString());
				sqlLoad(conn);
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
			}

		}
		/// <summary>Update</summary>
		public void Update(SqlConnection conn)
		{
			bool bExist = false;
			try
			{
				_log("UPDATE", ToString());
				bExist = Exist(conn);
				if (bExist)
				{
					sqlUpdate(conn);
				}
				else
				{
					_log("NOT_EXIST", ToString());
				}
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}
		}
		/// <summary>Save</summary>
		public void Save(SqlConnection conn)
		{
			try
			{
				bool bExist = false;

				_log("INSERT", ToString());
				bExist = Exist(conn);
				if (!bExist)
				{
					sqlInsert(conn);
				}
				else
				{
					_log("ALREADY_EXISTS", ToString());
					sqlUpdate(conn);
				}
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}

		}
		/// <summary>Delete</summary>
		public void Delete(SqlConnection conn)
		{
			try
			{
				_log("DELETE", ToString());
				sqlDelete(conn);
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}
		}
		/// <summary>Exist</summary>
		public bool Exist(SqlConnection conn)
		{
			bool bReturn = false;
			try
			{
				bReturn = sqlExist(conn);
			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}

			return bReturn;
		}
		/// <summary>Prompt user to enter Property values</summary>
		public void Prompt()
		{
			try 
			{

				Console.WriteLine(ErrorCode.TAG_CODE + ":  ");
				Code = Console.ReadLine();

				Console.WriteLine(ErrorCode.TAG_DESCRIPTION + ":  ");
				Description = Console.ReadLine();

				Console.WriteLine(ErrorCode.TAG_VISIBLE_CODE + ":  ");
				VisibleCode = Console.ReadLine();

				Console.WriteLine(ErrorCode.TAG_TRACE + ":  ");
				Trace = Console.ReadLine();

			}
			catch (Exception e) 
			{
 
 _log("ERROR", e.StackTrace.ToString());
				_hasError = true;
				_errorCode = new ErrorCode();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}
		}
                        
		//protected
		/// <summary>Inserts row of data into the database</summary>
		protected void sqlInsert(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramErrorCodeID = null;
			SqlParameter paramCode = null;
			SqlParameter paramDescription = null;
			SqlParameter paramVisibleCode = null;
			SqlParameter paramTrace = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure    
			cmd = new SqlCommand(SP_INSERT_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
                                    
			// parameters
			paramErrorCodeID = new SqlParameter("@" + TAG_ID, ErrorCodeID);
			paramErrorCodeID.DbType = DbType.Int32;
			paramErrorCodeID.Direction = ParameterDirection.Input;

			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, Description);
			paramDescription.DbType = DbType.String;
			paramDescription.Size = 255;
			paramDescription.Direction = ParameterDirection.Input;

			paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, VisibleCode);
			paramVisibleCode.DbType = DbType.String;
			paramVisibleCode.Size = 255;
			paramVisibleCode.Direction = ParameterDirection.Input;

			paramTrace = new SqlParameter("@" + TAG_TRACE, Trace);
			paramTrace.DbType = DbType.String;
			paramTrace.Size = 1000;
			paramTrace.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramErrorCodeID);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramDescription);
			cmd.Parameters.Add(paramVisibleCode);
			cmd.Parameters.Add(paramTrace);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			// assign the primary kiey
			string strTmp;
			strTmp = cmd.Parameters["@PKID"].Value.ToString();
			ErrorCodeID = long.Parse(strTmp);

			// cleanup to help GC
			paramErrorCodeID = null;
			paramCode = null;
			paramDescription = null;
			paramVisibleCode = null;
			paramTrace = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Check to see if the row exists in database</summary>
		protected bool sqlExist(SqlConnection conn)
		{
			bool bExist = false;

			SqlCommand cmd = null;
			SqlParameter paramErrorCodeID = null;
			SqlParameter paramCount = null;

			cmd = new SqlCommand(SP_EXIST_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;

			paramErrorCodeID = new SqlParameter("@" + TAG_ID, ErrorCodeID);
			paramErrorCodeID.Direction = ParameterDirection.Input;
			paramErrorCodeID.DbType = DbType.Int32;

			paramCount = new SqlParameter();
			paramCount.ParameterName = "@COUNT";
			paramCount.DbType = DbType.Int32;
			paramCount.Direction = ParameterDirection.Output;

			cmd.Parameters.Add(paramErrorCodeID);
			cmd.Parameters.Add(paramCount);
			cmd.ExecuteNonQuery();

			string strTmp;
			int nCount = 0;
			strTmp = cmd.Parameters["@COUNT"].Value.ToString();
			nCount = int.Parse(strTmp);
			if (nCount > 0)
			{
				bExist = true;
			}

			// cleanup
			paramErrorCodeID = null;
			paramCount = null;
			cmd = null;

			return bExist;
		}
		/// <summary>Updates row of data in database</summary>
		protected void sqlUpdate(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramErrorCodeID = null;
			SqlParameter paramCode = null;
			SqlParameter paramDescription = null;
			SqlParameter paramVisibleCode = null;
			SqlParameter paramTrace = null;
			SqlParameter paramPKID = null;

			//Create a command object identifying
			//the stored procedure    
			cmd = new SqlCommand(SP_UPDATE_NAME, conn);

			//Set the command object so it knows
			//to execute a stored procedure
			cmd.CommandType = CommandType.StoredProcedure;
                                    
			// parameters

			paramErrorCodeID = new SqlParameter("@" + TAG_ID, ErrorCodeID);
			paramErrorCodeID.DbType = DbType.Int32;
			paramErrorCodeID.Direction = ParameterDirection.Input;


			paramCode = new SqlParameter("@" + TAG_CODE, Code);
			paramCode.DbType = DbType.String;
			paramCode.Size = 255;
			paramCode.Direction = ParameterDirection.Input;

			paramDescription = new SqlParameter("@" + TAG_DESCRIPTION, Description);
			paramDescription.DbType = DbType.String;
			paramDescription.Size = 255;
			paramDescription.Direction = ParameterDirection.Input;

			paramVisibleCode = new SqlParameter("@" + TAG_VISIBLE_CODE, VisibleCode);
			paramVisibleCode.DbType = DbType.String;
			paramVisibleCode.Size = 255;
			paramVisibleCode.Direction = ParameterDirection.Input;

			paramTrace = new SqlParameter("@" + TAG_TRACE, Trace);
			paramTrace.DbType = DbType.String;
			paramTrace.Size = 1000;
			paramTrace.Direction = ParameterDirection.Input;

			paramPKID = new SqlParameter();
			paramPKID.ParameterName = "@PKID";
			paramPKID.DbType = DbType.Int32;
			paramPKID.Direction = ParameterDirection.Output;

			//Add parameters to command, which
			//will be passed to the stored procedure
			cmd.Parameters.Add(paramErrorCodeID);
			cmd.Parameters.Add(paramCode);
			cmd.Parameters.Add(paramDescription);
			cmd.Parameters.Add(paramVisibleCode);
			cmd.Parameters.Add(paramTrace);
			cmd.Parameters.Add(paramPKID);

			// execute the command
			cmd.ExecuteNonQuery();
			string s;
			s = cmd.Parameters["@PKID"].Value.ToString();
			ErrorCodeID = long.Parse(s);

			// cleanup
			paramErrorCodeID = null;
			paramCode = null;
			paramDescription = null;
			paramVisibleCode = null;
			paramTrace = null;
			paramPKID = null;
			cmd = null;
		}
		/// <summary>Deletes row of data in database</summary>
		protected void sqlDelete(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramErrorCodeID = null;

			cmd = new SqlCommand(SP_DELETE_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramErrorCodeID = new SqlParameter("@" + TAG_ID, ErrorCodeID);
			paramErrorCodeID.DbType = DbType.Int32;
			paramErrorCodeID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramErrorCodeID);
			cmd.ExecuteNonQuery();

			// cleanup to help GC
			paramErrorCodeID = null;
			cmd = null;

		}
		/// <summary>Load row of data from database</summary>
		protected void sqlLoad(SqlConnection conn)
		{
			SqlCommand cmd = null;
			SqlParameter paramErrorCodeID = null;
			SqlDataReader rdr = null;

			cmd = new SqlCommand(SP_LOAD_NAME, conn);
			cmd.CommandType = CommandType.StoredProcedure;
			paramErrorCodeID = new SqlParameter("@" + TAG_ID, ErrorCodeID);
			paramErrorCodeID.DbType = DbType.Int32;
			paramErrorCodeID.Direction = ParameterDirection.Input;
			cmd.Parameters.Add(paramErrorCodeID);
			rdr = cmd.ExecuteReader();
			if (rdr.Read())
			{
				sqlParseResultSet(rdr);
			}
			// cleanup
			rdr.Close();
			rdr = null;
			paramErrorCodeID = null;
			cmd = null;
		}
		/// <summary>Parse result set</summary>
		protected void sqlParseResultSet(SqlDataReader rdr)
		{
			this.ErrorCodeID = long.Parse(rdr[DB_FIELD_ID].ToString());
			this.Code = rdr[DB_FIELD_CODE].ToString().Trim();
			this.Description = rdr[DB_FIELD_DESCRIPTION].ToString().Trim();
			this.VisibleCode = rdr[DB_FIELD_VISIBLE_CODE].ToString().Trim();
			this.Trace = rdr[DB_FIELD_TRACE].ToString().Trim();
		}

        //private
        /// <summary>Log errors</summary>
        private void _log(string pStrAction, string pStrMsgText)
        {
            if (_config != null)
            {
                if (_config.DoLogInfo)
                {
                    _oLog.Log(pStrAction, pStrMsgText);
                }
            }

        }

		/// <summary>Unit Testing: Save, Delete, Update, Exist, Load and ToXml</summary>
		public void Test(SqlConnection conn)
		{
			try 
			{
				Console.WriteLine("What would you like to do?");
				Console.WriteLine("1.  Save.");
				Console.WriteLine("2.  Delete.");
				Console.WriteLine("3.  Update.");
				Console.WriteLine("4.  Exist.");
				Console.WriteLine("5.  Load.");
				Console.WriteLine("6.  ToXml.");
				Console.WriteLine("q.  Quit.");
                                                
				string strAns = "";

				strAns = Console.ReadLine();
				if (strAns != "q")
				{           
					int nAns = 0;
					nAns = int.Parse(strAns);
					switch(nAns)
					{
						case 1:
							// insert
							Console.WriteLine("Save:  ");
							Prompt();
							Save(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 2:
							Console.WriteLine("Delete " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ErrorCodeID = long.Parse(strAns);
							Delete(conn);
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 3:
							Console.WriteLine("Update:  ");
							Prompt();
							Update(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 4:
							Console.WriteLine("Exist " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ErrorCodeID = long.Parse(strAns);
							bool bExist = false;
							bExist = Exist(conn);
							Console.WriteLine("Record id " + ErrorCodeID + " exist:  " + bExist.ToString() );
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 5:
							Console.WriteLine("Load " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ErrorCodeID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToString());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						case 6:
							Console.WriteLine("ToXml " + TAG_ID + ":  ");
							strAns = Console.ReadLine();
							ErrorCodeID = long.Parse(strAns);
							Load(conn);
							Console.WriteLine(ToXml());
							Console.WriteLine(" ");
							Console.WriteLine("Press ENTER to continue...");
							Console.ReadLine();
							break;
						default:
							Console.WriteLine("Undefined option.");
							break;
					}
				}
			}
			catch (Exception e) 
			{
				Console.WriteLine(e.ToString());
				Console.WriteLine(e.StackTrace);
				Console.ReadLine();
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
			}

		}                       
	}
}

//END OF ErrorCode CLASS FILE
