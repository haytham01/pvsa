﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Veracity.Common
{
    static public class Constants
    {
        public enum Severity
        {
            Warning = 1,
            Information = 2,
            Error = 3
        }

        public static readonly DateTime DateTimeEmpty = new DateTime(1, 1, 1);
    }
}
