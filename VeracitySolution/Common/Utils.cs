﻿/// <summary>
/// Copyright (c) 2013 Veracity.  San Diego, California, USA
/// All Rights Reserved
/// 
/// File:  Config.cs
/// History
/// ----------------------------------------------------
/// 001	HA	3/30/2013	Created
/// 
/// </summary>
/// 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Veracity.Common
{
    public class Utils
    {
        private static bool _hasError = false;
        private static string _errorMessage = null;
        private static string _errorStacktrace = null;

        /// <summary>
        /// Gets the error stacktrace.
        /// </summary>
        /// <value>
        /// The error stacktrace.
        /// </value>
        public string ErrorStacktrace
        {
            get { return _errorStacktrace; }
        }
        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage
        {
            get { return _errorMessage; }
        }

        /// <summary>HasError Property in class Column and is of type bool</summary>
        public bool HasError
        {
            get { return _hasError; }
            set { _hasError = value; }
        }

      //  List<string[]> testParse =
      //    parseCSV("C:\\Documents and Settings\\Rich\\My Documents\\TestParse.csv");
      //DataTable newTable = new DataTable();
      //foreach(string column in testParse[0])
      //{
      //  newTable.Columns.Add();
      //}

      //foreach (string[] row in testParse)
      //{
      //  newTable.Rows.Add(row);
      //}
      //dataGridView1.DataSource = newTable;

        public static List<string[]> parseCSV(string path)
        {
            List<string[]> parsedData = new List<string[]>();

            try
            {
                using (StreamReader readFile = new StreamReader(path))
                {
                    string line;
                    string[] row;

                    while ((line = readFile.ReadLine()) != null)
                    {
                        row = line.Split(',');
                        parsedData.Add(row);
                    }
                }
            }
            catch (Exception e)
            {
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
                //MessageBox.Show(e.Message);
            }

            return parsedData;
        }

        public static bool StringToFile(string path, StringBuilder sb)
        {
            bool bSuccess = false;
            try
            {
                // Specify file, instructions, and privelegdes
                FileStream file = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);

                // Create a new stream to write to the file
                StreamWriter sw = new StreamWriter(file);

                // Write a string to the file
                sw.Write(sb.ToString());

                // Close StreamWriter
                sw.Close();

                // Close file
                file.Close();
                file = null;
                bSuccess = true;
            }
            catch (Exception e)
            {
                bSuccess = false;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
            return bSuccess;

        }

        public static StringBuilder FileToString(string path)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader sr = new StreamReader(path))
                {
                    String line;
                    // Read and display lines from the file until the end of 
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.Append(line + "\n\r");
                    }
                }
            }
            catch (Exception e)
            {
                sb = null;
                _hasError = true;
                _errorStacktrace = e.StackTrace.ToString();
                _errorMessage = e.Message;
            }
            return sb;
        }

        public static StringBuilder StripString(string s)
        {
            StringBuilder sb = new StringBuilder();
            if (s != null)
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if ( (s[i] != '\n') && (s[i] != '\r'))
                    {
                        sb.Append(s[i]);
                    }
                }
            }
            return sb;
        }

        public static string ReplaceSpecialXMLCharacters(string s)
        {
            string strReturn = s;
            if (strReturn != null)
            {
                strReturn = strReturn.Replace("&", "&amp;");
                strReturn = strReturn.Replace("<", "&lt;");
                strReturn = strReturn.Replace(">", "&gt;");
                strReturn = strReturn.Replace("\"", "&quot;");
                strReturn = strReturn.Replace("'", "&apos;");
            }

            return strReturn;
        }

        public static string ReverseSpecialXMLCharacters(string s)
        {
            string strReturn = s;
            if (strReturn != null)
            {
                strReturn = strReturn.Replace("&amp;", "&");
                strReturn = strReturn.Replace("&lt;", "<");
                strReturn = strReturn.Replace("&gt;", ">");
                strReturn = strReturn.Replace("&quot;", "\"");
                strReturn = strReturn.Replace("&apos;", "'");
            }

            return strReturn;
        }
    }
}
