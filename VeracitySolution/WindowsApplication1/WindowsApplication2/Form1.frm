VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   4020
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   4020
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtResults 
      Height          =   2055
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   1920
      Width           =   4455
   End
   Begin VB.PictureBox picClicker 
      Height          =   1695
      Left            =   1440
      ScaleHeight     =   1635
      ScaleWidth      =   3075
      TabIndex        =   1
      Top             =   120
      Width           =   3135
   End
   Begin VB.CommandButton cmdClick 
      Caption         =   "Click"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Declare Function ClientToScreen Lib "user32" (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Private Declare Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)

Private Const MOUSEEVENTF_MOVE = &H1          ' mouse move
Private Const MOUSEEVENTF_LEFTDOWN = &H2      ' left button down
Private Const MOUSEEVENTF_LEFTUP = &H4        ' left button up
Private Const MOUSEEVENTF_RIGHTDOWN = &H8     ' right button down
Private Const MOUSEEVENTF_RIGHTUP = &H10      ' right button up
Private Const MOUSEEVENTF_MIDDLEDOWN = &H20   ' middle button down
Private Const MOUSEEVENTF_MIDDLEUP = &H40     ' middle button up
Private Const MOUSEEVENTF_WHEEL = &H800       ' wheel button rolled
Private Const MOUSEEVENTF_ABSOLUTE = &H8000   ' absolute move

Private Type POINTAPI
    X As Long
    Y As Long
End Type

' Simulate moving the mouse to the center of the
' PictureBox and clicking.
Private Sub cmdClick_Click()
Const NUM_MOVES = 3000
Dim pt As POINTAPI
Dim cur_x As Single
Dim cur_y As Single
Dim dest_x As Single
Dim dest_y As Single
Dim dx As Single
Dim dy As Single
Dim i As Integer

    ' Things are easier working in pixels.
    ScaleMode = vbPixels
    picClicker.ScaleMode = vbPixels

    ' mouse_event moves in a coordinate system where
    ' (0, 0) is in the upper left corner and
    ' (65535,65535) is in the lower right corner.

    ' Get the current mouse coordinates and convert
    ' them into this new system.
    GetCursorPos pt
    cur_x = pt.X * 65535 / ScaleX(Screen.Width, vbTwips, vbPixels)
    cur_y = pt.Y * 65535 / ScaleY(Screen.Height, vbTwips, vbPixels)

    ' Convert the coordinates of the center of the
    ' picClicker PictureBox into this new system.
    pt.X = picClicker.ScaleWidth / 2
    pt.Y = picClicker.ScaleHeight / 2
    ClientToScreen picClicker.hwnd, pt
    dest_x = pt.X * 65535 / ScaleX(Screen.Width, vbTwips, vbPixels)
    dest_y = pt.Y * 65535 / ScaleY(Screen.Height, vbTwips, vbPixels)

    ' Move the mouse.
    dx = (dest_x - cur_x) / NUM_MOVES
    dy = (dest_y - cur_y) / NUM_MOVES
    For i = 1 To NUM_MOVES - 1
        cur_x = cur_x + dx
        cur_y = cur_y + dy
        mouse_event _
            MOUSEEVENTF_ABSOLUTE + _
            MOUSEEVENTF_MOVE, _
            cur_x, cur_y, 0, 0
        DoEvents
    Next i

    ' Move the mouse to its final destination and click it.
    mouse_event _
        MOUSEEVENTF_ABSOLUTE + _
        MOUSEEVENTF_MOVE + _
        MOUSEEVENTF_LEFTDOWN + _
        MOUSEEVENTF_LEFTUP, _
        dest_x, dest_y, 0, 0
End Sub
Private Sub picClicker_Click()
    txtResults.Text = txtResults.Text & _
        "MouseClick" & vbCrLf
End Sub

Private Sub picClicker_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    txtResults.Text = txtResults.Text & _
        "MouseDown (" & Format$(X) & _
        ", " & Format$(Y) & ")" & vbCrLf
End Sub


Private Sub picClicker_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    txtResults.Text = txtResults.Text & _
        "MouseUp (" & Format$(X) & _
        ", " & Format$(Y) & ")" & vbCrLf
End Sub


