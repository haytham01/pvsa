﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace VeracityAppLauncher
{
    /// <summary>
    /// Interaction logic for SetupWindow.xaml
    /// </summary>
    public partial class SetupWindow : Window
    {
        public bool bOK = false;

        public SetupWindow()
        {
            InitializeComponent();
        }

        private void keyTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
          
        }

        private void keyTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if ((e.Changes.Any()) && (Helper.IsValidFilepath(this.certFilepathTextBox.Text)))
            {
                this.submitButton.IsEnabled = true;
            }
            else
            {
                this.submitButton.IsEnabled = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            init();
        }

   

        private void init()
        {
            string strColor = null;
            if (Helper.IsConnectedToInternet())
            {
                strColor = "Green";
            }
            else
            {
                strColor = "Red";
            }

            UpdateConnEllipse(strColor);
            //this.connEllipse.Dispatcher.Invoke(
            //  new ConnEventHandler(UpdateConnEllipse),
            //  new object[] { strColor });
                    
        }

        private void UpdateConnEllipse(string pStrColor)
        {
            SolidColorBrush tmpBrush = null;
            if (pStrColor.Equals("Red", StringComparison.CurrentCultureIgnoreCase))
            {
                tmpBrush = new SolidColorBrush(Colors.Red);
            }
            else
            {
                tmpBrush = new SolidColorBrush(Colors.Green);
            }
            this.connEllipse.Fill = tmpBrush;
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            bOK = false;
            string strError = null;
            try
            {
                if (!Helper.IsConnectedToInternet())
                {
                    strError = "You must have an Internet connection to proceed with setup.";
                }
                else
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                bOK = false;
                strError = ex.Message;
            }

            if (!bOK)
            {
                MessageBox.Show(strError);
            }

        }

        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog openFileDialog = new System.Windows.Forms.OpenFileDialog();
            openFileDialog.Filter = "Certificate Files|*.cer";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                this.certFilepathTextBox.Text = filePath;
            }
        }

        //private void StartProcess()
        //{
        //    BackgroundWorker worker = new BackgroundWorker();
        //    //this is where the long running process should go
        //    worker.DoWork += (o, ea) =>
        //    {
        //        //no direct interaction with the UI is allowed from this method
        //        for (int i = 0; i < 100; i++)
        //        {
        //            System.Threading.Thread.Sleep(50);
        //        }
        //    };
        //    worker.RunWorkerCompleted += (o, ea) =>
        //    {
        //        //work has completed. you can now interact with the UI
        //        _busyIndicator.IsBusy = false;
        //    };
        //    //set the IsBusy before you start the thread
        //    _busyIndicator.IsBusy = true;
        //    worker.RunWorkerAsync();
        //}


       
        
    }
}
