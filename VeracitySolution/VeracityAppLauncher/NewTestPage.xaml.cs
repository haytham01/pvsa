﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeracityAppLauncher
{
    /// <summary>
    /// Interaction logic for NewTestPage.xaml
    /// </summary>
    public partial class NewTestPage : Page
    {
        public NewTestPage()
        {
            InitializeComponent();
        }
    }
}
