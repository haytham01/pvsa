﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeracityAppLauncher
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            for (int i = 0; i < 5; i++)
            {
                Grid G = new Grid();
                Button button = new Button();
                TextBlock TB = new TextBlock();
                TB.Text = "Hello World";
                Image IMG = new Image();
                G.Children.Add(IMG);
                G.Children.Add(TB);
                button.Content = G;
                button.Click += new RoutedEventHandler(button_Click);
                this.mainWrapPanel.Children.Add(button);
            }
        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            Type type = (Type)(sender as Button).Tag;
            this.NavigationService.Navigate(new Uri("DetailsPage.xaml", UriKind.Relative));
        }

        private void OnHyperlink(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("NewTestPage.xaml", UriKind.Relative));
        }
    }
}
