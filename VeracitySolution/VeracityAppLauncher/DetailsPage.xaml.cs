﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeracityAppLauncher
{
    /// <summary>
    /// Interaction logic for DetailsPage.xaml
    /// </summary>
    public partial class DetailsPage : Page
    {
        public DetailsPage()
        {
            InitializeComponent();
        }

        private void OnLaunchHyperlink(object sender, RoutedEventArgs e)
        {
            //Hyperlink h = (Hyperlink)sender;
            //MessageBox.Show("Launching Application ...");
            //this.NavigationService.Navigate(new Uri("NewTestPage.xaml", UriKind.Relative));
            bool isMainAppAvailable = Helper.IsMainAppAvailable();
            if (isMainAppAvailable)
            {
                Helper.LaunchApp();

                //if (!Helper.IsAppRunning())
                //{
                //    Helper.LaunchApp();
                //}
            }
        }

        private void OnUninstallHyperlink(object sender, RoutedEventArgs e)
        {
            Hyperlink h = (Hyperlink)sender;
            MessageBox.Show("Uninstalling Application ...");
            //this.NavigationService.Navigate(new Uri("NewTestPage.xaml", UriKind.Relative));
        }
    }
}
