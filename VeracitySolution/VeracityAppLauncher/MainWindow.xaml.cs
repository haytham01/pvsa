﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VeracityAppLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        private System.Windows.Forms.NotifyIcon m_notifyIcon;
        private WindowState m_storedWindowState = WindowState.Normal;

        public MainWindow()
        {
            InitializeComponent();
            setNotifyIcon();
            init();
        }
        public void init()
        {
            this.Navigate(new Uri("MainPage.xaml", UriKind.Relative));  

            //if (isFirstTimeLaunch)
            //{
            //    //SetupWindow setupWindow = new SetupWindow();
            //    //setupWindow.ShowDialog();
            //    // launch the app
            //   //Helper.LaunchApp();
            //}

        }
 

        private void setNotifyIcon()
        {
            m_notifyIcon = new System.Windows.Forms.NotifyIcon();
            m_notifyIcon.BalloonTipText = "The Veracity Touch Screener has been minimized. Click the tray icon to show.";
            m_notifyIcon.BalloonTipTitle = "Veracity Touch Screener";
            m_notifyIcon.Text = "Veracity Touch Screener";
            m_notifyIcon.Icon = new System.Drawing.Icon("Images/Main.ico");
            m_notifyIcon.Click += new EventHandler(m_notifyIcon_Click);
        }

        private void m_notifyIcon_Click(object sender, EventArgs e)
        {
            Show();
            WindowState = m_storedWindowState;
        }
        private void CheckTrayIcon()
        {
            ShowTrayIcon(!IsVisible);
        }

        private void ShowTrayIcon(bool show)
        {
            if (m_notifyIcon != null)
                m_notifyIcon.Visible = show;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Window_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            CheckTrayIcon();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (WindowState == WindowState.Minimized)
            {
                Hide();
                if (m_notifyIcon != null)
                    m_notifyIcon.ShowBalloonTip(2000);
            }
            else
                m_storedWindowState = WindowState;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_notifyIcon.Dispose();
            m_notifyIcon = null;
        }

        private void Window_Closed(object sender, EventArgs e)
        {

        }

 
    }
}
