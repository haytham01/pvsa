﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace VeracityAppLauncher
{
    public class Helper
    {
        public static readonly string PROCESS_NAME = "VeracityApp.exe";

        public static bool IsAppRunning()
        {
            bool bReturn = false;
            try
            {
                Process[] processes = Process.GetProcessesByName(PROCESS_NAME);
                foreach (Process p in Process.GetProcesses("."))
                {
                    if (p.MainModule.ModuleName.IndexOf(PROCESS_NAME) >= 0)
                    {
                        bReturn = true;
                        break;
                    }
                }
             
            }
            catch { }
            return bReturn;
        }

        public static void LaunchApp()
        {
            // For the example
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.CreateNoWindow = false;
            //startInfo.UseShellExecute = false;
            string appExePath = GetAppToLaunchPath();
            startInfo.FileName = appExePath;
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            //startInfo.Arguments = "-f j -o \"" + ex1 + "\" -z 1.0 -s y " + ex2;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    //exeProcess.WaitForExit();
                    //System.Windows.Application.Current.Shutdown();

                }
            }
            catch
            {
                // Log error.
            }
        }

        //Creating the extern function...
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
        //Creating a function that uses the API function...
        public static bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }

        public static bool IsMainAppAvailable()
        {
            bool bReturn = false;
            try
            {
                string appExePath = GetAppToLaunchPath();
                if (File.Exists(appExePath))
                {
                    bReturn = true;
                }
            }
            catch { bReturn = false ; }
            return bReturn;
        }

        public static string GetAppToLaunchPath()
        {
            string appExePath = null;
            try
            {
                string appExeName = PROCESS_NAME;
                string fullExeNameAndPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string dirExePath = System.IO.Path.GetDirectoryName(fullExeNameAndPath);
                appExePath = dirExePath + System.IO.Path.DirectorySeparatorChar + appExeName;
            }
            catch { appExePath = null; }
            return appExePath;
        }

        public static bool DownloadFile(string pStrDownloadURL, string pStrSavePath)
        {
            bool bSuccess = true;
            try
            {
                WebClient wc = new WebClient();
                wc.DownloadFile(pStrDownloadURL, pStrSavePath);
            }
            catch (Exception ex)
            {
                bSuccess = false;
            }
            return bSuccess;
        }

        public static bool UploadFile(string pStrUploadURL, string pStrFilePathToUpload)
        {
            bool bSuccess = true;
            try
            {
                WebClient wc = new WebClient();
                wc.UploadFile(pStrUploadURL, pStrFilePathToUpload);

            }
            catch (Exception ex)
            {
                bSuccess = false;
            }
            return bSuccess;

        }

        public static bool UploadData(string pStrUploadURL, byte[] pByteDataToUpload)
        {
            bool bSuccess = true;
            try
            {
                WebClient wc = new WebClient();
                wc.UploadData(pStrUploadURL, pByteDataToUpload);

            }
            catch (Exception ex)
            {
                bSuccess = false;
            }
            return bSuccess;

        }

        public void CompressFile(string sourceFile, string destinationFile)
        {
            // make sure the source file is there
            if (File.Exists(sourceFile) == false)
                throw new FileNotFoundException();

            // Create the streams and byte arrays needed
            byte[] buffer = null;
            FileStream sourceStream = null;
            FileStream destinationStream = null;
            GZipStream compressedStream = null;

            try
            {
                // Read the bytes from the source file into a byte array
                sourceStream = new FileStream(sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read);

                // Read the source stream values into the buffer
                buffer = new byte[sourceStream.Length];
                int checkCounter = sourceStream.Read(buffer, 0, buffer.Length);

                if (checkCounter != buffer.Length)
                {
                    throw new ApplicationException();
                }

                // Open the FileStream to write to
                destinationStream = new FileStream(destinationFile, FileMode.OpenOrCreate, FileAccess.Write);

                // Create a compression stream pointing to the destiantion stream
                compressedStream = new GZipStream(destinationStream, CompressionMode.Compress, true);

                // Now write the compressed data to the destination file
                compressedStream.Write(buffer, 0, buffer.Length);
            }
            catch (ApplicationException ex)
            {
                //MessageBox.Show(ex.Message, "An Error occured during compression", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Make sure we allways close all streams
                if (sourceStream != null)
                    sourceStream.Close();

                if (compressedStream != null)
                    compressedStream.Close();

                if (destinationStream != null)
                    destinationStream.Close();
            }
        }

        public void DecompressFile(string sourceFile, string destinationFile)
        {
            // make sure the source file is there
            if (File.Exists(sourceFile) == false)
                throw new FileNotFoundException();

            // Create the streams and byte arrays needed
            FileStream sourceStream = null;
            FileStream destinationStream = null;
            GZipStream decompressedStream = null;
            byte[] quartetBuffer = null;

            try
            {
                // Read in the compressed source stream
                sourceStream = new FileStream(sourceFile, FileMode.Open);

                // Create a compression stream pointing to the destiantion stream
                decompressedStream = new GZipStream(sourceStream, CompressionMode.Decompress, true);

                // Read the footer to determine the length of the destiantion file
                quartetBuffer = new byte[4];
                int position = (int)sourceStream.Length - 4;
                sourceStream.Position = position;
                sourceStream.Read(quartetBuffer, 0, 4);
                sourceStream.Position = 0;
                int checkLength = BitConverter.ToInt32(quartetBuffer, 0);

                byte[] buffer = new byte[checkLength + 100];

                int offset = 0;
                int total = 0;

                // Read the compressed data into the buffer
                while (true)
                {
                    int bytesRead = decompressedStream.Read(buffer, offset, 100);

                    if (bytesRead == 0)
                        break;

                    offset += bytesRead;
                    total += bytesRead;
                }

                // Now write everything to the destination file
                destinationStream = new FileStream(destinationFile, FileMode.Create);
                destinationStream.Write(buffer, 0, total);

                // and flush everyhting to clean out the buffer
                destinationStream.Flush();
            }
            catch (ApplicationException ex)
            {
                //MessageBox.Show(ex.Message, "An Error occured during compression", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                // Make sure we allways close all streams
                if (sourceStream != null)
                    sourceStream.Close();

                if (decompressedStream != null)
                    decompressedStream.Close();

                if (destinationStream != null)
                    destinationStream.Close();
            }

        }

        public static bool IsGUID(string expression)
        {
            bool bValid = false;
            if (expression != null)
            {

                Guid newTmpGuid;
                if (Guid.TryParse(expression, out newTmpGuid))
                {
                    bValid = true;
                }
            }
            return bValid;
        }

        public static bool IsValidFilepath(string expression)
        {
            bool bValid = false;
            if (expression != null)
            {
                if (File.Exists(expression))
                {
                    bValid = true;
                }
            }
            return bValid;
        }
    }
}
