﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TouchScreener.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TouchScreener.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Test interrupted.  Contact test administrator..
        /// </summary>
        public static string AlertSubtitle {
            get {
                return ResourceManager.GetString("AlertSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ALERT.
        /// </summary>
        public static string AlertTitle {
            get {
                return ResourceManager.GetString("AlertTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please Choose a Test to Activate.
        /// </summary>
        public static string ChooseTestToActivate {
            get {
                return ResourceManager.GetString("ChooseTestToActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please Enter Identification.
        /// </summary>
        public static string EnterIdentification {
            get {
                return ResourceManager.GetString("EnterIdentification", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to عربي.
        /// </summary>
        public static string LanguageButtonArabicIraqi {
            get {
                return ResourceManager.GetString("LanguageButtonArabicIraqi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to English.
        /// </summary>
        public static string LanguageButtonEnglish {
            get {
                return ResourceManager.GetString("LanguageButtonEnglish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to español.
        /// </summary>
        public static string LanguageButtonSpanishMexico {
            get {
                return ResourceManager.GetString("LanguageButtonSpanishMexico", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PRESS ON YOUR ANSWER.
        /// </summary>
        public static string PressOnYourAnswer {
            get {
                return ResourceManager.GetString("PressOnYourAnswer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        public static string ScreenNoText {
            get {
                return ResourceManager.GetString("ScreenNoText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        public static string ScreenYesText {
            get {
                return ResourceManager.GetString("ScreenYesText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start.
        /// </summary>
        public static string WelcomeScreenStart {
            get {
                return ResourceManager.GetString("WelcomeScreenStart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Application Title.
        /// </summary>
        public static string WelcomeScreenTitle {
            get {
                return ResourceManager.GetString("WelcomeScreenTitle", resourceCulture);
            }
        }
    }
}
